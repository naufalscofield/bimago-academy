$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

function openModalCart(obj) {

var id = $(obj).attr('id');
var url = $(obj).attr('url');
var l = Ladda.create( document.querySelector( '.cart_' + id ));
l.start();
  $.ajax({
      url: url,
      method: "POST",
      data: {id:id},
      success: function(data){
        l.stop();
        var res = $.parseJSON(data);
        if (res["tipe"] == 'warning') {
          toastr.warning(res["pesan"]);
        }else{
          toastr.success(res["pesan"]);
        }
        if (res["url"] != '') {
          window.location = res["url"];
        }
      }
  });
}

function openModalCartHome1(obj) {

var id = $(obj).attr('id');
var url = $(obj).attr('url');
var l = Ladda.create( document.querySelector( '.cart-tw_' + id ));
l.start();
  $.ajax({
      url: url,
      method: "POST",
      data: {id:id},
      success: function(data){
        l.stop();
        var res = $.parseJSON(data);
        if (res["tipe"] == 'warning') {
          toastr.warning(res["pesan"]);
        }else{
          toastr.success(res["pesan"]);
        }
        if (res["url"] != '') {
          window.location = res["url"];
        }
        // $('.cart-body').html(data);
        // $('#cartModal').modal('show');
      }
  });
}

function openModalCartHome2(obj) {

var id = $(obj).attr('id');
var url = $(obj).attr('url');
var l = Ladda.create( document.querySelector( '.cart-bs_' + id ));
l.start();
  $.ajax({
      url: url,
      method: "POST",
      data: {id:id},
      success: function(data){
        l.stop();
        var res = $.parseJSON(data);
        if (res["tipe"] == 'warning') {
          toastr.warning(res["pesan"]);
        }else{
          toastr.success(res["pesan"]);
        }
        if (res["url"] != '') {
          window.location = res["url"];
        }
      }
  });
}

function openModalCartHome3(obj) {

var id = $(obj).attr('id');
var url = $(obj).attr('url');
var l = Ladda.create( document.querySelector( '.cart-upcoming_' + id ));
l.start();
  $.ajax({
      url: url,
      method: "POST",
      data: {id:id},
      success: function(data){
        l.stop();
        var res = $.parseJSON(data);
        if (res["tipe"] == 'warning') {
          toastr.warning(res["pesan"]);
        }else{
          toastr.success(res["pesan"]);
        }
        if (res["url"] != '') {
          window.location = res["url"];
        }
      }
  });
}

function add_cart(form, formData, method, action) {
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "1000",
    "hideDuration": "1000",
    "timeOut": "1000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  $.ajax({
    type: method,
    url: action,
    data: formData,
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'success') {
        toastr.success(hasil["pesan"]);
        $('#courseModal').modal('hide');
        $('#total-count-2').html(hasil["total"]);
        $('#total-count').html(hasil["total"]);
      }else{
        window.location = hasil["url"];
      }
      $('#list-cart').empty();
    },
    error: function (res) {
      // l.stop();
      toastr.error("Gagal memasukan ke keranjang.");
    },
  });
}

function delete_row_cart(obj) {
  var id = $(obj).attr('id');
  var url = $(obj).attr('url');
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "1000",
    "hideDuration": "1000",
    "timeOut": "1000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  $.ajax({
    url: url,
    method: "POST",
    data: {id:id},
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["status"] == true) {
        toastr.success(hasil["pesan"]);
        $('.total-count').html(hasil["total"]);
        $('#row_' + id).remove();
        $('#total-count').html(hasil["total"]);
        $('#total-count-2').html(hasil["total"]);
        $('#total-amount').html(hasil["amount"]);
        $('#total-amount-2').html(hasil["amount"]);
      }else{
        toastr.warning(hasil["pesan"]);
      }
      if (hasil["total"] == 0)
      {
        $("#empty-cart").css("display", "block");
        $("#exist-cart").css("display", "none");
      }
      $('#list-cart').empty();
    },
    error: function (res) {
      toastr.error("Gagal memasukan ke keranjang.");
      result = false;
      return false;
    },
  });
}

// $("#shopping").click(function(){
  function shopping(obj){
    var url = $(obj).attr('url');
    $.ajax({
      'type': 'POST',
      'url': url,
      'dataType': 'html',
      'success': function (data) {
        $('#list-cart').html(data);
      }
    });
  }
  // });

  function logout(obj) {
    var url = $(obj).attr('url');
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "1000",
      "hideDuration": "1000",
      "timeOut": "1000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    $.ajax({
      url: url,
      success: function (res) {
        var hasil = $.parseJSON(res);
        toastr.success(hasil["pesan"]);
        window.location = hasil["url"];
      },
      error: function (res) {
        toastr.error("Tidak bisa logout.");
      },
  });
}

function checkout(obj) {
  var url = $(obj).attr('url');
  var l = Ladda.create( document.querySelector( '.ladda' ) );
  l.start();
  $.ajax({
    url: url,
    method: "POST",
    // data: {id:id},
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'success') {
        l.stop();
        toastr.success(hasil["pesan"]);
        setTimeout(function () {
          window.location = hasil["url"];
        }, 2000);
      }else{
        setTimeout(function () {
          toastr.error(hasil["pesan"]);
        }, 2000);
      }
    },
    error: function (res) {
      toastr.error("Gagal melakukan checkout.");
    },
  });
}

function add_remove_wishlist(obj) {
  var id = $(obj).attr('id');
  var url = $(obj).attr('url');
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "1000",
    "hideDuration": "1000",
    "timeOut": "1000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  var l = Ladda.create( document.querySelector( '.ladda-wish'+id ) );
  l.start();
  $.ajax({
    url: url,
    method: "POST",
    data: {id:id},
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'added') {
        l.stop();
        toastr.success(hasil["pesan"]);
        $('#wish_' + id).attr('class', 'fa fa-heart love');
      }else{
        l.stop();
        toastr.info(hasil["pesan"]);
        $('#wish_' + id).attr('class', 'fa fa-heart');
        $('#mycourse_' + id).remove();
      }
    },
    error: function (res) {
      l.stop();
      toastr.error("Gagal Menambahkan ke Wishlist.");
    },
  });
}

function add_remove_wishlist_home(obj) {
  var id = $(obj).attr('id');
  var url = $(obj).attr('url');
  $.ajax({
    url: url,
    method: "POST",
    data: {id:id},
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'added') {
        toastr.success(hasil["pesan"]);
        $('#wish_' + id).attr('class', 'fa fa-heart love');
        // $('#wish-bs_' + id).attr('class', 'fa fa-heart love');
      }else{
        toastr.info(hasil["pesan"]);
        $('#wish_' + id).attr('class', 'fa fa-heart');
        // $('#wish-bs_' + id).attr('class', 'fa fa-heart');
      }
    },
    error: function (res) {
      toastr.error("Gagal Menambahkan ke Wishlist.");
    },
  });
}

function add_cart_home(obj) {
  var id = $(obj).attr('id');
  var url = $(obj).attr('url');
  $.ajax({
    url: url,
    method: "POST",
    data: {id:id},
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["status"] == true) {
        setTimeout(function () {
          toastr.success(hasil["pesan"]);
        }, 200);
        $('#courseModal').modal('hide');
        $('#total-count').html(hasil["total"]);
      }else{
        setTimeout(function () {
          toastr.info(hasil["pesan"]);
        }, 200);
      }
      $('#list-cart').empty();
    },
    error: function (res) {
      toastr.error("Gagal memasukan ke keranjang.");
      result = false;
      return false;
    },
  });
}

function load_content(id, form, type, url, callback) {
        $(".skeleton").show();
        var data = $('#' + form).serialize();
        if (typeof url === 'undefined') {
            var url = $('#' + id).attr('data-source');
        }
        if (typeof type === 'undefined') {
          var type = 'html';
        }
        if ((typeof form === 'undefined') || (form == '')) {
          form = $('#' + id).attr('data-filter');
          data = $(form).serialize();
        }
        $('#' + id).html('').addClass('loading-img').attr('style', 'min-height:300px; width:100%;');
        $.post(url, data, function (x, textStatus) {
          $('#' + id).removeClass('loading-img').attr('style', '');
          $(".skeleton").hide();
          if (type == 'html') {
                $('#' + id).html(x);
            } else {
                $('#' + id).html(x.content);
            }
            if (typeof (callback) == 'function') {
                callback();
            }
        }, type);

        // $.ajax({
        //   type: "POST",
        //   url: url,
        //   data: data,
        //   success: function (x, textStatus) {
        //     console.log('awww')
        //   $('#' + id).removeClass('loading-img').attr('style', '');
        //   $(".skeleton").hide();
        //   if (type == 'html') {
        //         $('#' + id).html(x);
        //     } else {
        //         $('#' + id).html(x.content);
        //     }
        //     if (typeof (callback) == 'function') {
        //         callback();
        //     }
        //   },
        //   dataType: type
        // });
    }

function show_toast(status, msg)
{
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  Command: toastr[status](msg)

}

function add_free(obj) {
  var id = $(obj).attr('id');
  var url = $(obj).attr('url');
  var l = Ladda.create( document.querySelector( '.ladda'+id ) );
  l.start();
  $.ajax({
    url: url,
    method: "POST",
    data: {id:id},
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'success') {
        l.stop();
        toastr.success(hasil["pesan"]);
        if(hasil["url"]){
          setTimeout(function () {
            window.location = hasil["url"];
          }, 1000);
        }
      }else{
        l.stop();
        setTimeout(function () {
          toastr.info(hasil["pesan"]);
        }, 1000);
      }
    },
    error: function (res) {
      l.stop();
      toastr.error("Tidak bisa melakukan pendaftaran pada pelatihan.");
    },
  });
}

function add_free_home(obj) {
  var id = $(obj).attr('id');
  var url = $(obj).attr('url');
  var l = Ladda.create( document.querySelector( '.ladda'+id ) );
  l.start();
  $.ajax({
    url: url,
    method: "POST",
    data: {id:id},
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'success') {
        l.stop();
        toastr.success(hasil["pesan"]);
        if(hasil["url"]){
          setTimeout(function () {
            window.location = hasil["url"];
          }, 1000);
        }
      }else{
        setTimeout(function () {
          toastr.info(hasil["pesan"]);
        }, 1000);
      }
    },
    error: function (res) {
      toastr.error("Tidak bisa melakukan pendaftaran pada pelatihan.");
    },
  });
}

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function(e) {
        closeAllLists(e.target);
    });
}
