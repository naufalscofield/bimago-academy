<?php
Class Lecturer_account_model extends CI_Model {

    private $table  = 'ls_m_user';
    private $table_detail = 'ls_m_user_detail';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($cond)
    {
        return $this->db->get_where($this->table, $cond);
    }

    public function update($data, $cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }

    public function show_detail($cond)
    {
        return $this->db->get_where($this->table_detail, $cond);
    }

}
