<?php
Class Lecturer_quiz_model extends CI_Model {

    private $table  = 'ls_m_exam';
    private $table_questions  = 'ls_m_questions';
    private $table_course  = 'ls_m_course';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($id_episode)
    {
        // return $this->db->get_where($this->table, $cond);
        return $this->db->query("SELECT *, a.id as id_exam, b.judul as judul_course, (select count(*) from $this->table_questions c where c.id_exam = a.id) as total_soal from $this->table a INNER JOIN $this->table_course b on a.id_course = b.id where a.tipe = 'quiz' and id_episode = $id_episode");

    }

    public function store($data)
    {
        return $this->db->insert($this->table, $data);
    }

}
