<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturer_webinar_model extends MY_Model {

	private $primTable	= 'ls_m_episode';

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';

	public function get_data($table)
	{
		$this->db->select('a.*, c.nama_depan, c.nama_belakang, b.modul');
		$this->db->from($table.' a');
		$this->db->join('ls_m_modul b', 'a.id_modul = b.id', 'LEFT');
		$this->db->join('ls_m_user c', 'a.kontributor = c.id', 'LEFT');
		$this->db->where('a.kontributor', $this->session->userdata('id'));
		if ($this->id != '') $this->db->where('a.id', $this->id);
		$this->db->like('a.type_course', 4);
		return $this->db->get();
	}

	public function get_data_member($table)
	{
		$this->db->select('c.id, c.email, c.nama_depan, c.nama_belakang, b.status, b.tanggal_pembelian');
		$this->db->from($table.' a');
		$this->db->join('ls_t_pembayaran b', 'a.id_pembayaran = b.id', 'LEFT');
		$this->db->join('ls_m_user c', 'a.id_user = c.id', 'LEFT');
		if ($this->id != '') $this->db->where('a.id_course', $this->id);
		return $this->db->get();
	}

}
