<?php
Class Lecturer_withdrawal_model extends CI_Model {

    private $table  = 'ls_t_withdrawal';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($cond)
    {
        return $this->db->get_where($this->table, $cond);
    }

    public function show_with_user($cond)
    {
        $this->db->select("a.*,b.nama_depan,b.nama_belakang");
        $this->db->join("ls_m_user b", "a.id_lecturer = b.id");
        return $this->db->get_where($this->table." a", $cond);
    }

    public function store($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function get()
    {
        $this->db->select("a.*, b.nama_depan, b.nama_belakang");
        $this->db->join("ls_m_user b", "a.id_lecturer = b.id");
        return $this->db->get($this->table.' a');
    }

    public function update($cond, $data)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }
}