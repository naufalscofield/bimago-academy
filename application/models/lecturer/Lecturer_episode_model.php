<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturer_episode_model extends MY_Model {

	private $primTable	= 'ls_m_episode';

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';
	public $id_course = '';

	public function get_data($table)
	{
		$this->db->select('a.*, b.judul as judul_course, c.type_course');
		$this->db->from($table.' a');
		$this->db->join('ls_m_course b', 'a.id_course = b.id', 'LEFT');
		$this->db->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT');
		if ($this->id != '') $this->db->where('a.id', $this->id);
		if ($this->id_course != '') $this->db->where('b.id', $this->id_course);
		// $this->db->where('b.kontributor', $this->session->userdata('id'));
		$this->db->where('b.status', 2);
		return $this->db->get();
	}

	public function create_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update_data($table, $data, $where)
	{
		$this->db->from($table);
		$this->db->set($data);
		$this->db->where($where);
		return $this->db->update();
	}

	public function match($string)
	{
		//Ini comment
		$judul	= str_replace("%20"," ",$string);

		$this->db->like('judul', $judul, 'both');

		return $this->db->get($this->primTable)->result_array();
	}

	public function show($cond)
	{
		return $this->db->get_where("ls_m_tugas_episode", $cond);
	}

}
