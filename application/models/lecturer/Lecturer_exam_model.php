<?php
Class Lecturer_exam_model extends CI_Model {

    private $table              = 'ls_m_exam';
    private $table_course       = 'ls_m_course';
    private $table_type_course  = 'ls_m_type_course';
    private $table_after_exam   = 'ls_t_after_exam';
    private $table_questions    = 'ls_m_questions';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id='')
    {
        // $this->db->select('*,a.id as id_exam');
        // $this->db->join($this->table_course.' b', 'a.id_course = b.id');
        // return $this->db->get($this->table.' a')->result_array();
        if (!empty($id)) {
            return $this->db->query("SELECT *, a.id as id_exam, d.type_course, (select count(*) from $this->table_questions c where c.id_exam = a.id) as total_soal from $this->table a INNER JOIN $this->table_course b on a.id_course = b.id JOIN $this->table_type_course d on a.id_type_course = d.id WHERE a.tipe = 'exam' AND
            b.kontributor = ".$this->session->userdata('id')." AND b.id = ".$id." AND b.status = 2")->result_array();
        }else{   
            return $this->db->query("SELECT *, a.id as id_exam, d.type_course, (select count(*) from $this->table_questions c where c.id_exam = a.id) as total_soal from $this->table a INNER JOIN $this->table_course b on a.id_course = b.id JOIN $this->table_type_course d on a.id_type_course = d.id WHERE a.tipe = 'exam' AND
            b.kontributor = ".$this->session->userdata('id')." AND b.status = 2")->result_array();
        }
    }

    public function show($id)
    {
        $this->db->select('*,a.id as id_exam, c.id as prim_id_type');
        $this->db->join($this->table_course.' b', 'a.id_course = b.id');
        $this->db->join($this->table_type_course.' c', 'a.id_type_course = c.id');
        return $this->db->get_where($this->table.' a', ['a.id' => $id])->row();
    }

    public function where($cond)
    {
        $this->db->select('*,a.id as id_exam');
        $this->db->join($this->table_course.' b', 'a.id_course = b.id');
        return $this->db->get_where($this->table.' a', $cond)->row();
    }

    public function store($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function delete($id)
    {
        $this->db->where('id',$id);
        return $this->db->delete($this->table);
    }

    public function update($data, $cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }

    public function store_after_exam($data)
    {
        return $this->db->insert($this->table_after_exam, $data);
    }

    public function get_after_exam($cond)
    {
        $this->db->select('c.judul, a.score, a.status');
        $this->db->order_by('a.id', 'DESC');
        $this->db->join($this->table.' b', 'a.id_exam = b.id');
        $this->db->join($this->table_course.' c', 'b.id_course = c.id');
        return $this->db->get_where($this->table_after_exam.' a', $cond);
    }

    public function store_question_used($data, $cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }

    public function get_percobaan($cond)
    {
        return $this->db->get_where($this->table_after_exam, $cond);
    }

}
