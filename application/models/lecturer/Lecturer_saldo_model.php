<?php
Class Lecturer_saldo_model extends MY_Model {

    private $table  = 'ls_t_saldo_lecturer';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($cond)
    {
        return $this->db->get_where($this->table, $cond);
    }

    public function store($data)
    {
        return $this->db->insert($this->table, $data);
    }
}