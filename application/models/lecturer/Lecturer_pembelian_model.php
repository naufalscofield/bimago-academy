<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lecturer_pembelian_model extends MY_Model {

	private $primTable	= 'ls_m_episode';

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';

	public function get_data($table)
	{
		$this->db->select('a.id, b.judul as judul_course, b.image, c.modul, d.nama_depan, d.nama_belakang, e.status, e.tanggal_pembelian, a.harga, g.type_course, e.tanggal_pembelian');
		$this->db->from($table.' a');
		$this->db->join('ls_m_course b', 'a.id_course = b.id', 'LEFT');
		$this->db->join('ls_m_modul c', 'b.id_modul = c.id', 'LEFT');
		$this->db->join('ls_m_user d', 'a.id_user = d.id', 'LEFT');
		$this->db->join('ls_t_pembayaran e', 'a.id_pembayaran = e.id', 'LEFT');
		$this->db->join('ls_m_harga_course f', 'a.id_type_course = f.id_type_course and a.id_course = f.id_course', 'LEFT');
		$this->db->join('ls_m_type_course g', 'a.id_type_course = g.id', 'LEFT');
		if ($this->id != '') $this->db->where('a.id', $this->id);
		$this->db->where('b.kontributor', $this->session->userdata('id'));
		return $this->db->get();
	}

	public function create_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update_data($table, $data, $where)
	{
		$this->db->from($table);
		$this->db->set($data);
		$this->db->where($where);
		return $this->db->update();
	}

	public function match($string)
	{
		//Ini comment
		$judul	= str_replace("%20"," ",$string);

		$this->db->like('judul', $judul, 'both');

		return $this->db->get($this->primTable)->result_array();
	}

}
