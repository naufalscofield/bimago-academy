<?php
Class Lecturer_questions_model extends CI_Model {

    private $table              = 'ls_m_questions';
    private $table_answers      = 'ls_m_answers';
    private $table_key          = 'ls_t_answer_key';
    private $table_user_answer  = 'ls_t_user_answer';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($cond, $limit = 0, $offset = 0)
    {
        if ($limit != 0 || $offset)
        {
            return $this->db->get_where($this->table, $cond, $limit, $offset);
        } else
        {
            return $this->db->get_where($this->table, $cond);
        }
    }

    public function check_null_answer($id)
    {
        $this->db->where("id_question", $id);
        $where = '(answer is NULL or answer = "-" or answer = "")';
        $this->db->where($where);
        // $this->db->where("answer", NULL);
        // $this->db->or_where("answer", NULL);
        return $this->db->get($this->table_answers);
    }

    public function store($data)
    {
        $this->db->insert($this->table, $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    public function show_with_user_answer($cond)
    {
        $this->db->select('*,b.id as id_user_answer');
        $this->db->join($this->table_user_answer.' b', 'a.id = b.id_question', 'left');
        return $this->db->get_where($this->table.' a', $cond);
    }

    public function list_qa($cond)
    {
        $this->db->select('*, a.id as id_question_m, b.id as id_answer_m');
        $this->db->join($this->table_answers.' b', 'a.id = b.id_question');
        return $this->db->get_where($this->table.' a', $cond);
    }

    public function show_answers($cond)
    {
        $this->db->select('*,b.id as id_key, a.media as media_answer, a.id as id_answer, b.id_question as id_question_key');
        $this->db->join($this->table_key.' b', 'a.id = b.id_answer', 'left');
        $this->db->join($this->table.' c', 'a.id_question = c.id', 'left');
        $this->db->order_by('a.id', 'ASC');
        return $this->db->get_where($this->table_answers.' a', $cond);
    }

    public function update_master_answer($cond, $data)
    {
        $this->db->where($cond);
        return $this->db->update($this->table_answers, $data);
    }

    public function show_answers_cbx($id_question, $cond)
    {
        $exp        = explode(',',$cond);
        $id_answers = [];

        foreach($exp as $e)
        {
            $opt    = $this->db->get_where($this->table_answers, ['id_question' => $id_question, 'opt' => $e])->row()->id;
            array_push($id_answers,$opt);
            // var_dump($opt->id); die;
        }

        return implode(',', $id_answers);
    }

    public function check_answer($cond)
    {
        $this->db->join('ls_m_user b', 'a.id_user = b.id', 'left');
        $this->db->join('ls_m_exam c', 'a.id_exam = c.id', 'left');
        $this->db->join('ls_m_questions d', 'a.id_question = d.id', 'left');
        $this->db->join('ls_m_answers e', 'a.id_answer = e.id', 'left');

        return $this->db->get_where($this->table_user_answer.' a', $cond);
    }

    public function update_answer($data, $cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table_user_answer, $data);
    }

    public function store_master_answer($data)
    {
        return $this->db->insert($this->table_answers, $data);
    }

    public function store_answer($data, $batch = false)
    {
        if ($batch == false)
        {
            return $this->db->insert($this->table_user_answer, $data);
        } else
        {
            // dd($data);
            return $this->db->insert_batch($this->table_user_answers, $data);
        }
    }

    public function store_answer_batch($data)
    {
        return $this->db->insert($this->table_user_answer, $data);
    }

    public function final_submit($cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table_user_answer, ['status' => 'final']);
    }

    public function check_final_answer($cond)
    {
        $this->db->select('*,c.id as id_ak');
        $this->db->join($this->table.' b', 'a.id_question = b.id', 'left');
        $this->db->join($this->table_key.' c', 'a.id_question = c.id_question and a.id_answer = c.id_answer', 'left');
        // $this->db->join($this->table_key.' d', 'a.id_answer = d.id_answer', 'left');
        $this->db->where('c.id is NOT NULL', NULL, FALSE);
        $this->db->where($cond);
        return $this->db->get($this->table_user_answer.' a');
    }

    public function insert_batch($data)
    {
        return $this->db->insert_batch($this->table_answers, $data);
    }

    public function update_answers_batch($data)
    {
        return $this->db->update_batch($this->table_answers, $data, 'id');
    }

    public function check_kunjaw($id_question, $data = true)
    {
        if ($data){
            return $this->db->get_where($this->table_key, ['id_question' => $id_question])->num_rows();
        } else
        {
            return $this->db->get_where($this->table_key, ['id_question' => $id_question])->row();
        }
    }

    public function update_kunjaw($data, $cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table_key, $data);
    }

    public function store_kunjaw($data)
    {
        return $this->db->insert($this->table_key, $data);
    }

    public function wipe_answers($cond)
    {
        $this->db->where($cond);
        return $this->db->delete($this->table_user_answer);
    }

    public function delete($cond)
    {
        $this->db->where($cond);
        return $this->db->delete($this->table);
    }

    public function delete_answer($cond)
    {
        $this->db->where($cond);
        return $this->db->delete($this->table_answers);
    }

    public function delete_key($cond)
    {
        $this->db->where($cond);
        return $this->db->delete($this->table_key);
    }

    public function get_random($cond, $limit)
    {
        $this->db->where($cond);
        $this->db->limit($limit);
        $this->db->order_by('id', 'RANDOM');
        return $this->db->get($this->table);
    }

    public function store_user_questions($query)
    {
        return $this->db->query($query);
    }

    public function show_user_questions($cond)
    {
        return $this->db->get_where($this->table_user_answer, $cond);
    }

    public function delete_user_answers($cond)
    {
        $this->db->where($cond);
        return $this->db->delete($this->table_user_answer);
    }
    
    public function update_question($cond, $data)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }
}
