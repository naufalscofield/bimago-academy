<?php
Class Certificate_model extends CI_Model {

    private $table  = 'ls_m_certificate';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($id='',$limit='',$offset='',$cari = '')
    {
        $this->db->select('a.*,b.judul');
		$this->db->from($this->table.' a');
		$this->db->join('ls_m_course b', 'a.id_course = b.id', 'LEFT');
		if (!empty($limit)) $this->db->limit($limit, $offset);
        if (!empty($id)) $this->db->where('a.id_course', $id);
		if (!empty($cari)) $this->db->where("LOWER(b.judul) LIKE '%".strtolower($cari)."%'");
		return $this->db->get();
    }
    public function getPreviewCertificate($id_course=''){
        $this->db->select('b.judul,d.certificate_number,c.nama_depan,c.nama_belakang,b.pelaksanaan,b.kontributor,d.signature_name,d.signature_title,d.presented_name1,d.presented_name2,d.presented_title1,d.presented_title2,d.certificate_category');
        $this->db->from('ls_m_course b');
        $this->db->join('ls_m_user c', 'c.id = '.$this->session->userdata('id'), 'LEFT');
        $this->db->join('ls_m_certificate d', 'd.id_course = b.id', 'LEFT');
        $this->db->where('c.id', $this->session->userdata('id'));
        $this->db->where('b.id', $id_course);
        
        return $this->db->get();
    }
}