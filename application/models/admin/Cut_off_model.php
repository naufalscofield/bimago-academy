<?php
Class Cut_off_model extends CI_Model {

    private $table  = 'ls_m_cut_off';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($cond)
    {
        return $this->db->get_where($this->table, $cond);
    }

    public function update($data, $cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }

    public function get()
    {
        return $this->db->get($this->table);
    }
}