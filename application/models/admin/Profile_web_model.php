<?php
Class Profile_web_model extends CI_Model {

    private $table  = 'ls_m_profile_web';

    public function get()
    {
        return $this->db->get($this->table);
    }

    public function update($cond, $data)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }
}