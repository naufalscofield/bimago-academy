<?php
Class Pembayaran_model extends CI_Model {

    private $pembayaran    = 'ls_t_pembayaran';
    private $detail        = 'ls_t_detail_pembelian';
    private $user          = 'ls_m_user';

    public function __construct()
    {
        parent::__construct();
    }

    public function show($cond = '')
    {
      $this->db->select('a.*, b.nama_depan, b.nama_belakang, b.email, b.no_hp, c.id_course, c.harga');
      $this->db->from($this->pembayaran.' a');
      $this->db->join($this->user.' b', 'a.id_user = b.id', 'LEFT');
      $this->db->join($this->detail.' c', 'a.id = c.id_pembayaran', 'LEFT');
      if ($cond != '') $this->db->where($cond);
      $this->db->order_by('a.tanggal_pembelian', 'DESC');
      return $this->db->get();
    }

    public function show_detail($cond = '')
    {
      $this->db->select('a.*, b.nama_depan, b.nama_belakang, b.email, b.no_hp');
      $this->db->from($this->pembayaran.' a');
      $this->db->join($this->user.' b', 'a.id_user = b.id', 'LEFT');
      if ($cond != '') $this->db->where($cond);
      $this->db->order_by('a.tanggal_pembelian', 'DESC');
      return $this->db->get();
    }

    public function store_w_iid($data)
    {
      $this->db->insert($this->pembayaran, $data);
      $insert_id = $this->db->insert_id();

      return  $insert_id;
    }

}
