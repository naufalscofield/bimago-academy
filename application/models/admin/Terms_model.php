<?php
Class Terms_model extends CI_Model {

    private $table  = 'ls_m_terms';

    public function get()
    {
        return $this->db->get($this->table);
    }

    public function update($cond, $data)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }
}