<?php
Class Report_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function show($id='',$limit='',$offset='',$cari = '')
    {
        $this->db->select('a.*,b.judul as judul_episode,c.judul as judul_course,CONCAT(d.nama_depan," ",d.nama_belakang) as nama');
		$this->db->from('ls_t_report a');
        $this->db->join('ls_m_episode b', 'b.id = a.id_episode', 'LEFT');
        $this->db->join('ls_m_course c', 'c.id = a.id_course', 'LEFT');
		$this->db->join('ls_m_user d', 'd.id = a.id_user', 'LEFT');
		if (!empty($limit)) $this->db->limit($limit, $offset);
        if (!empty($id)) $this->db->where('a.id_course', $id);
		if (!empty($cari)) $this->db->where("LOWER(b.judul) LIKE '%".strtolower($cari)."%'");
		return $this->db->get();
    }
}