<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_model extends CI_Model {

	private $primTable	= 'ls_t_user_task';

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    public function show($cond, $limit = 0, $offset = 0)
    {
        // dd($limit.'-'.$offset);
        $this->db->select("a.*, a.id as id_task, b.*, d.nama_depan, d.nama_belakang");
        $this->db->join("ls_m_episode b", "b.id = a.id_episode","right");
        $this->db->join("ls_m_tugas_episode c", "c.id = a.id_tugas","right");
        $this->db->join("ls_m_user d", "d.id = a.id_user","right");
        if ($limit != 0 || $offset != 0)
        {
            return $this->db->get_where($this->primTable.' a', $cond, $limit, $offset);
        } else
        {
            return $this->db->get_where($this->primTable.' a', $cond);
        }
    }

    public function store($data)
    {
        return $this->db->insert($this->primTable, $data);
    }

    public function update($cond, $data)
    {
        $this->db->where($cond);
        return $this->db->update($this->primTable, $data);
    }

}