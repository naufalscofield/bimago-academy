<?php
Class Voucher_model extends CI_Model {

    private $table  = 'ls_m_voucher';

    public function __construct()
    {
        parent::__construct();
    }
    

    public function get()
    {
        return $this->db->get($this->table);
    }

    public function store($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($cond, $data)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }

    public function delete($cond)
    {
        $this->db->where($cond);
        return $this->db->delete($this->table);
    }

    public function show($cond)
    {
        return $this->db->get_where($this->table, $cond);
    }
}