<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_home_model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function topThree(){
		return $this->master_model->data('e.harga,e.discount,a.type_course, a.judul, a.deskripsi_singkat, a.image, a.pelaksanaan, a.id, b.nama_depan, b.nama_belakang, b.institusi, c.modul, d.score', 'ls_m_course a')
					->join('ls_m_user b', 'a.kontributor = b.id', 'LEFT')
					->join('ls_m_modul c', 'a.id_modul = c.id', 'LEFT')
					->join('ls_t_convert_rating d', 'a.id = d.id_course', 'LEFT')
					->join('ls_m_harga_course e', 'a.id = e.id_course', 'LEFT')
					->not_like('a.type_course', '4')
					->where('a.status', 2)
					->order_by('counter','asc')
					->limit(6)->get()->result_array();
	}

	public function getTotWish($id)
	{
		return $this->db->select('count(*) as total_wish')
						->from('ls_t_wishlist')
						->where('id_course', $id)
						->get()
						->row();
	}

	public function getTotCom($id)
	{
		return $this->db->select('count(*) as total_com')
						->from('ls_t_komentar_episode a')
						->join('ls_m_episode b', 'b.id = a.id_episode')
						->where('b.id_course', $id)
						->get()
						->row();
	}

	public function bestSeller(){
		return $this->db->query(
			"SELECT
				c.judul,
				c.image,
				c.pelaksanaan,
				g.harga,
				g.discount,
				c.id,
				c.type_course,
				d.nama_depan,
				d.nama_belakang,
				d.institusi,
				e.modul,
				f.score
			FROM
				ls_t_detail_pembelian a
				LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran
				LEFT JOIN ls_m_course c ON c.id = a.id_course
				LEFT JOIN ls_m_user d ON c.kontributor = d.id
				LEFT JOIN ls_m_modul e ON c.id_modul = e.id
				LEFT JOIN ls_t_convert_rating f ON c.id = f.id_course
				LEFT JOIN ls_m_harga_course g ON c.id = g.id_course
			WHERE
				c.type_course NOT LIKE '%4%' ESCAPE '!' AND
				c.status = 2 AND
				(
					b.STATUS IS NULL
				OR b.STATUS IN ( 1 )
				)
			GROUP BY
				c.judul
			LIMIT 6"
		)->result_array();
	}
}
