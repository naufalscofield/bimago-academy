<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_cart_model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';

	public function get_data($table)
	{
		// $this->db->select('a.*, b.urutan');
		$this->db->select('a.*, b.gambar');
		$this->db->from($table.' a');
		$this->db->join('ls_m_modul b', 'a.id_modul = b.id', 'LEFT');
		if ($this->id != '') $this->db->where('a.id', $this->id);
		// $this->db->order_by('b.urutan', 'ASC');
		return $this->db->get();
	}

	public function get_modul()
	{
		$this->db->select('*');
		$this->db->from('ls_m_modul a');
		return $this->db->get();
	}

	public function get_terkait()
	{
		$this->db->select('a.*, b.gambar');
		$this->db->from('ls_m_course a');
		$this->db->join('ls_m_modul b', 'a.id_modul = b.id', 'LEFT');
		return $this->db->get('', 4);
	}

	public function create_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

}
