<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_checkout_model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';

	function order($no_pesanan = '', $total = '', $metode = ''){
				$free = FALSE;
				$pay = FALSE;
				if ($total == 0)
				{
					$free = TRUE;
				} else
				{
					$pay = TRUE;
				}
				if ($free) {
					$this->order_free($no_pesanan, $total);
				}
				if ($pay) {
					$pembayaran = array(
						'id_user' => $this->session->userdata('id'),
						'no_pesanan' => $no_pesanan,
						'total' => $total,
						'status' => 0,
						'metode_pembayaran' => 'Manual',
						// 'id_voucher'	=> ($this->session->userdata('id_voucher') != NULL) ? $this->session->userdata('id_voucher') : NULL 
						'id_voucher'	=> NULL 
					);

					$this->db->insert('ls_t_pembayaran',$pembayaran);
					$invoice_id = $this->db->insert_id();

					foreach($this->master_model->cart_contents($this->session->userdata('id')) as $item){
						if ($item['price'] != 0) {
							$check = $this->master_model->data('id', 'ls_t_pembelian', ['id_user' => $this->session->userdata('id'), 'id_course' => $item['id_course']])->get()->row_array();
							if (empty($check)) {
								$data = array(

									'id_user' => $this->session->userdata('id'),
									'id_course' => $item['id_course'],
								);
								$this->db->insert('ls_t_pembelian',$data);
								$pembelian_id = $this->db->insert_id();
								$type = json_decode($item['id_type_course']);
								foreach ($type as $key => $value) {
									if ($value != 0) {
										$data_detail = array(
											'id_pembayaran' => $invoice_id,
											'id_user' => $this->session->userdata('id'),
											'id_type_course' => $value,
											'harga' => $item['price'],
											'id_course' => $item['id_course'],
										);
										$this->db->insert('ls_t_detail_pembelian', $data_detail);
									}
								}
							}else{
								$type = json_decode($item['id_type_course']);
								foreach ($type as $key => $value) {
									if ($value != 0) {
										$data_detail = array(
											'id_pembayaran' => $invoice_id,
											'id_user' => $this->session->userdata('id'),
											'id_type_course' => $value,
											'harga' => $item['price'],
											'id_course' => $item['id_course'],
										);
										$this->db->insert('ls_t_detail_pembelian', $data_detail);
									}
								}
							}
						}
					}
				}

        return TRUE;
    }

		public function order_free($no_pesanan = '')
		{
			$pembayaran = array(
			'id_user' => $this->session->userdata('id'),
			'no_pesanan' => $no_pesanan.'2',
			'total' => 0,
			'status' => 1,
			'metode_pembayaran' => 'Manual',
			// 'id_voucher'	=> ($this->session->userdata('id_voucher') != NULL) ? $this->session->userdata('id_voucher') : NULL 
			'id_voucher'	=> NULL 
			);

		$this->db->insert('ls_t_pembayaran',$pembayaran);
		$invoice_id = $this->db->insert_id();

		foreach($this->master_model->cart_contents($this->session->userdata('id')) as $item){
			// if ($item['price'] == 0) {
				$check = $this->master_model->data('id', 'ls_t_pembelian', ['id_user' => $this->session->userdata('id'), 'id_course' => $item['id_course']])->get()->row_array();
				if (empty($check)) {
					$data = array(

						'id_user' => $this->session->userdata('id'),
						'id_course' => $item['id_course'],
					);
					$this->db->insert('ls_t_pembelian',$data);
					$type = json_decode($item['id_type_course']);
					foreach ($type as $key => $value) {
						if ($value != 0) {
							$data_detail = array(
								'id_pembayaran' => $invoice_id,
								'id_user' => $this->session->userdata('id'),
								'id_type_course' => $value,
								'harga' => $item['price'],
								'id_course' => $item['id_course'],
							);
							$this->db->insert('ls_t_detail_pembelian', $data_detail);
							$pembelian = $this->master_model->data('b.pembicara_webinar, b.background_webinar, b.link_zoom_webinar, b.image, a.id_course, a.id_type_course, b.judul, b.id_zoom_webinar, b.pass_zoom_webinar, b.pelaksanaan_webinar, b.link_zoom, b.pass_zoom, b.pelaksanaan_led, b.pelaksanaan_training, b.deskripsi_training, c.email, c.nama_depan', 'ls_t_detail_pembelian a', ['a.id_user' => $this->session->userdata('id'), 'a.id_type_course' => $value, 'a.id_course' => $item['id_course']])
			                    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
			                    ->join('ls_m_user c', 'a.id_user = c.id', 'LEFT')
			                    ->get()->row_array();
							if ($value == 3) {
									send_email_god('training', $pembelian, $pembelian['email']);
		          }elseif ($value == 2) {
								if ($pembelian['pelaksanaan_led'] > date('Y-m-d')) {
									send_email_god('led', $pembelian, $pembelian['email']);
								}
		          }elseif ($value == 4) {
								if ($pembelian['pelaksanaan_webinar'] > date('Y-m-d')) {
									send_email_god('webinar', $pembelian, $pembelian['email']);
								}
		          }
						}
					}
				}else{
					$type = json_decode($item['id_type_course']);
					foreach ($type as $key => $value) {
						if ($value != 0) {
							$data_detail = array(
								'id_pembayaran' => $invoice_id,
								'id_user' => $this->session->userdata('id'),
								'id_type_course' => $value,
								'harga' => $item['price'],
								'id_course' => $item['id_course'],
							);
							$this->db->insert('ls_t_detail_pembelian', $data_detail);
							$pembelian = $this->master_model->data('b.pembicara_webinar, b.background_webinar, b.link_zoom_webinar, b.image, a.id_course, a.id_type_course, b.judul, b.id_zoom_webinar, b.pass_zoom_webinar, b.pelaksanaan_webinar, b.link_zoom, b.pass_zoom, b.pelaksanaan_led, b.pelaksanaan_training, b.deskripsi_training, c.email, c.nama_depan', 'ls_t_detail_pembelian a', ['a.id_user' => $this->session->userdata('id'), 'a.id_type_course' => $value, 'a.id_course' => $item['id_course']])
			                    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
			                    ->join('ls_m_user c', 'a.id_user = c.id', 'LEFT')
			                    ->get()->row_array();
							if ($value == 3) {
								send_email_god('training', $pembelian, $pembelian['email']);
		          }elseif ($value == 2) {
								send_email_god('led', $pembelian, $pembelian['email']);
		          }elseif ($value == 4) {
								send_email_god('webinar', $pembelian, $pembelian['email']);
		          }
						}
					}
				}
			// }
		}
	}

	public function show_voucher_pembayaran($id_voucher)
	{
		return $this->db->get_where("ls_t_pembayaran", ['id_voucher' => $id_voucher]);
	}

}
