<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_contact_model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';

	public function get_data($table)
	{
		$this->db->select('a.*, b.gambar, c.id_user');
		$this->db->from($table.' a');
		$this->db->join('ls_m_modul b', 'a.id_modul = b.id', 'LEFT');
		$this->db->join('ls_t_pembelian c', 'a.id = c.id_course', 'LEFT');
		if ($this->id != '') $this->db->where('a.id', $this->id);
		// if ($this->session->userdata('id') != '') $this->db->where('c.id_user', $this->session->userdata('id'));
		return $this->db->get();
	}

	public function get_data_purchased($id = '')
	{
		$this->db->select('a.id_course');
		$this->db->from('ls_t_pembelian a');
		if ($id != '') $this->db->where('a.id_user', $id);
		return $this->db->get();
	}

	public function get_data_whislist($id = '')
	{
		$this->db->select('a.id_course');
		$this->db->from('ls_t_whislist a');
		if ($id != '') $this->db->where('a.id_user', $id);
		return $this->db->get();
	}

	public function get_modul()
	{
		$this->db->select('*');
		$this->db->from('ls_m_modul a');
		return $this->db->get();
	}

	public function get_terkait()
	{
		$this->db->select('a.*, b.gambar');
		$this->db->from('ls_m_course a');
		$this->db->join('ls_m_modul b', 'a.id_modul = b.id', 'LEFT');
		return $this->db->get('', 4);
	}

	public function create_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

}
