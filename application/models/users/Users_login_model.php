<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_login_model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';

	public function get_data($table)
	{
		$this->db->select('a.*');
		$this->db->from($table.' a');
		if ($this->id != '') $this->db->where('a.id', $this->id);
		return $this->db->get();
	}

	public function create_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function check_token($table, $token)
	{
		$now		= date("Y-m-d H:i:s");
		$get		= $this->db->get_where($table, ['email_verification_token' => $token])->row();
		$expired	= $get->email_verification_expired;

		$is_expired	= ($expired > $now) ? false : true;

		if ($get->verifikasi == 1)
		{
			$statusCode	= 1;
		} else
		{
			if ($is_expired)
			{
				$statusCode	= 3;
			} else
			{
				$statusCode = 2;
			}
		}

		return $statusCode;
	}

	public function verify_email($table, $token)
	{
		$this->db->where('email_verification_token', $token);
		return $this->db->update($table, ['verifikasi' => 1]);
	}

	public function show($table, $cond)
	{
		return $this->db->get_where($table, $cond);
	}

	public function update_email_token($table, $data, $cond)
	{
		$this->db->where($cond);
		return $this->db->update($table, $data);
	}


}
