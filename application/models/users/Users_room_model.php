<?php
Class Users_room_model extends CI_Model {

    private $table  = 'ls_t_room_exam';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function store($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function show($cond)
    {
        $this->db->select("a.*, b.*, a.id as master_id");
        $this->db->join('ls_m_exam b','a.id_exam = b.id');
        return $this->db->get_where($this->table.' a', $cond);
    }

    public function update($data, $cond)
    {
        $this->db->where($cond);
        return $this->db->update($this->table, $data);
    }

    public function delete($cond)
    {
        $this->db->where($cond);
        return $this->db->delete($this->table);
    }
}