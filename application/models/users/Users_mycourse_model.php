<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_mycourse_model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id = '';
	public $kategori = '';
	public $rating = '';
	public $harga_min = '';
	public $harga_max = '';

	public function get_data_purchased($id = '')
	{
		$this->db->select('a.id_course');
		$this->db->from('ls_t_pembelian a');
		if ($id != '') $this->db->where('a.id_user', $id);
		return $this->db->get();
	}

	public function get_data_whislist($id = '')
	{
		$this->db->select('a.id_course');
		$this->db->from('ls_t_whislist a');
		if ($id != '') $this->db->where('a.id_user', $id);
		return $this->db->get();
	}

	public function create_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function wishlisted(){
		$this->db->select('b.*, c.nama_depan, c.nama_belakang, c.institusi, d.score');
		$this->db->from('ls_t_wishlist a');
		$this->db->join('ls_m_course b', 'a.id_course = b.id', 'LEFT');
		$this->db->join('ls_m_user c', 'b.kontributor = c.id', 'LEFT');
		$this->db->join('ls_t_convert_rating d', 'b.id = d.id_course', 'LEFT');
		$this->db->where('a.id_user', $this->session->userdata('id'));
		$this->db->where('b.status', 2);
		return $this->db->get();
	}
	public function getCertificate($id='', $id_user=''){
		$id_user = !empty($id_user) ? $id_user : $this->session->userdata('id');
		$this->db->select('e.created_at as date_grad,a.*,b.judul,a.certificate_number as certificate_number_trans,c.nama_sertifikat,c.nama_depan,c.nama_belakang,b.pelaksanaan,b.pelaksanaan_webinar,b.pelaksanaan_led,b.pelaksanaan_led,b.kontributor,d.signature_name,d.signature_title,d.presented_name1,d.presented_name2,d.presented_title1,d.presented_title2,d.certificate_category,d.signature_name2,d.signature_title2,d.sign_pic1,d.sign_pic2,d.duration,d.institution1,d.institution2,g.type_course,f.id_type_course');
		$this->db->from('ls_t_certificate a');
		$this->db->join('ls_m_course b', 'a.id_course = b.id', 'LEFT');
		$this->db->join('ls_m_user c', 'c.id = a.id_user', 'LEFT');
		$this->db->join('ls_m_certificate d', 'd.id_course = a.id_course', 'LEFT');
		$this->db->join('ls_m_exam f', 'a.id_exam = f.id AND f.tipe = "exam"', 'LEFT');
		$this->db->join('ls_t_after_exam e', "f.id = e.id_exam AND e.status ='passed' AND e.id_user ='". $id_user."'", 'LEFT');
		$this->db->join('ls_m_type_course g', "f.id_type_course = g.id", 'LEFT');
		if(!empty($id)) $this->db->where('a.id', $id);
		$this->db->where('a.id_user', $id_user);
		return $this->db->get();
	}

	public function get_purchase($table = '')
	{
		$this->db->select('e.type_course, a.harga, b.id, b.judul, b.image, b.deskripsi_singkat, c.no_pesanan, d.modul');
		$this->db->from($table.' a');
		$this->db->join('ls_m_course b', 'a.id_course = b.id', 'LEFT');
		$this->db->join('ls_t_pembayaran c', 'a.id_pembayaran = c.id', 'LEFT');
		$this->db->join('ls_m_modul d', 'b.id_modul = d.id', 'LEFT');
		$this->db->join('ls_m_type_course e', 'a.id_type_course = e.id', 'LEFT');
		$this->db->where('a.id_user', $this->session->userdata('id'));
		return $this->db->get();
	}

	public function get_invoice($table = '')
	{
		$this->db->select('a.*');
		$this->db->from($table.' a');
		$this->db->where('a.id_user', $this->session->userdata('id'));
		$this->db->order_by('a.tanggal_pembelian', 'DESC');
		return $this->db->get();
	}

	public function get_data_type_purchased($id = '', $id_course = '')
	{
		$this->db->select('a.id_type_course');
		$this->db->from('ls_t_detail_pembelian a');
		if ($id != '') $this->db->where('a.id_user', $id);
		if ($id != '') $this->db->where('a.id_course', $id_course);
		return $this->db->get();
	}

	public function getCertificateType($id=''){
		$this->db->select('c.type,a.id_exam,a.certificate_number');
		$this->db->from('ls_t_certificate a');
		$this->db->join('ls_m_course b', 'a.id_course = b.id', 'LEFT');
		$this->db->join('ls_m_certificate c', 'c.id_course = b.id', 'LEFT');
		if(!empty($id)) $this->db->where('a.id', $id);

		return $this->db->get();
	}
}
