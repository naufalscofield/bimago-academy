<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_course_model extends MY_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public $id 			= '';
	public $kategori 	= '';
	public $rating 		= '';
	public $harga_min 	= '';
	public $harga_max 	= '';
	public $judul 		= '';
	public $limit		= 0;
	public $offset		= 0;
	public $sort		= '';
	public $like		= '';
	public $type		= '';

	public function get_data($table)
	{
		$this->db->select('e.harga,e.discount,a.created_at, a.url_youtube, a.pelaksanaan_led, a.pelaksanaan_training, a.pelaksanaan_webinar, a.deskripsi_training, a.id, a.id_modul, a.deskripsi, a.image, a.judul, a.deskripsi_singkat, a.type_course, a.pelaksanaan, b.modul, c.nama_depan, c.nama_belakang, c.institusi, d.score');
		$this->db->from($table.' a');
		$this->db->join('ls_m_modul b', 'a.id_modul = b.id', 'LEFT');
		$this->db->join('ls_m_user c', 'a.kontributor = c.id', 'LEFT');
		$this->db->join('ls_t_convert_rating d', 'a.id = d.id_course', 'LEFT');
		$this->db->join('ls_m_harga_course e', 'a.id = e.id_course', 'LEFT');
		$this->db->where('a.status', 2);
		if ($this->id != '') $this->db->where('a.id', $this->id);
		if ($this->rating != '') $this->db->where($this->rating);
		if ($this->kategori != '') $this->db->where_in('a.id_modul', $this->kategori);
		if ($this->judul != '') $this->db->like('LOWER(a.judul)', strtolower($this->judul));
		if ($this->type != '') {
			foreach($this->type as $key => $value) {
			  $this->db->like('a.type_course', $value);
			}
		};

		if ($this->sort == 'name')
		{
			$this->db->order_by('judul');
		} else if ($this->sort == 'price')
		{
			$this->db->order_by('harga');
		} else if ($this->sort == 'date')
		{
			$this->db->where('pelaksanaan IS NOT NULL', NULL, FALSE);
			$this->db->order_by('pelaksanaan', 'DESC');
		}

		if ($this->like != '')
		{
			$this->db->like('judul', $this->like);
		}

		if ($this->limit != 0)
		{
			$this->db->limit($this->limit, $this->offset);
		}

		return $this->db->get();
	}

	public function get_data_purchased($id = '')
	{
		$this->db->select('a.id_course');
		$this->db->from('ls_t_pembelian a');
		if ($id != '') $this->db->where('a.id_user', $id);
		return $this->db->get();
	}

	public function get_data_type_purchased($id = '', $id_course = '')
	{
		$this->db->select('a.id_type_course');
		$this->db->from('ls_t_detail_pembelian a');
		if ($id != '') $this->db->where('a.id_user', $id);
		if ($id != '') $this->db->where('a.id_course', $id_course);
		return $this->db->get();
	}

	public function get_data_wishlisted($id = '')
	{
		$this->db->select('a.id_course');
		$this->db->from('ls_t_wishlist a');
		if ($id != '') $this->db->where('a.id_user', $id);
		return $this->db->get();
	}

	public function get_modul()
	{
		$this->db->select('*');
		$this->db->from('ls_m_modul a');
		return $this->db->get();
	}

	public function get_terkait($id_modul, $id)
	{
		$this->db->select('a.id, a.image, a.judul, a.deskripsi_singkat, a.type_course, a.harga, a.pelaksanaan, b.modul, c.nama_depan, c.nama_belakang, c.institusi, d.score');
		$this->db->from('ls_m_course a');
		$this->db->join('ls_m_modul b', 'a.id_modul = b.id', 'LEFT');
		$this->db->join('ls_m_user c', 'a.kontributor = c.id', 'LEFT');
		$this->db->join('ls_t_convert_rating d', 'a.id = d.id_course', 'LEFT');
		$this->db->where('b.id', $id_modul);
		$this->db->where_not_in('a.id', [$id]);
		return $this->db->get('', 4);
	}

	public function create_data($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function show($cond)
	{
		return $this->db->get_where("ls_m_course", $cond);
	}

}
