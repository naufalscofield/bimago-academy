<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


include APPPATH . "../vendor/autoload.php";

class Video extends MY_Controller {

    public static  $google_client = NULL;
    public static  $CI = NULL;


	public static function __initialize(){
		self::$CI = &get_instance();
		self::$CI->config->load('google');
	    ### GOOGLE API ###
	    self::$google_client = new Google_Client();
	    self::$google_client->setClientId(self::$CI->config->item('OAUTH2_CLIENT_ID')); //Define your ClientID
	    self::$google_client->setClientSecret(self::$CI->config->item('OAUTH2_CLIENT_SECRET')); //Define your Client Secret Key
    	self::$google_client->setScopes([Google_Service_Drive::DRIVE,Google_Service_YouTube::YOUTUBE]);

	    self::$google_client->setAccessType('offline');
	    self::$google_client->setPrompt('select_account consent');
	}

	public static function index(){
		self::__initialize();
		redirect(self::$google_client->createAuthUrl());
	}

	public static function upload_drive($filename, $fileloc, $folder_name=''){
		self::__initialize();

	    $token = self::$CI->master_model->data('response_token', 'ls_t_gdrive_token')->get()->row();
	    if (isset($token->response_token)) {
	        $token = json_decode($token->response_token,TRUE);
	        self::$CI->session->set_userdata('access_token',$token);
	    }

	    // Get your credentials from the console
	    if (isset($_GET['code']) || !empty(self::$CI->session->userdata('access_token'))) {
	        if (isset($_GET['code'])) {
	            self::$google_client->authenticate($_GET['code']);
	            $token = self::$google_client->getAccessToken();
	            self::$CI->session->set_userdata('access_token',$token);
	        } else{
	            self::$google_client->setAccessToken(self::$CI->session->userdata('access_token'));
	        }
	        
	        $folder_id = self::create_folder($folder_name);

	        $success = self::insert_file_to_drive(APPPATH . "../".$fileloc, $filename.'.mov', $folder_id);

	        if( $success['status']){     
	            $file_id = $success['file_id'];
	            // echo $file_id;
	            // echo '<br>';
	            $share_link = self::get_link_share($file_id);
	            return $share_link;
	            // echo '<br>';
	            // echo "file uploaded successfully";
	        } else { 
	            dd ($success);
	            echo "Something went wrong.";
	        }
	    
	   

	    } else {
	        $authUrl = self::$google_client->createAuthUrl();
	        header('Location: ' . $authUrl);
	        exit();
	    }
	}

	public static function create_folder($folder_name, $parent_folder_id=null ){

	    $folder_list = self::check_folder_exists( $folder_name );

	    // if folder does not exists
	    if( count( $folder_list ) == 0 ){
	        $service = new Google_Service_Drive(self::$google_client);
	        $folder = new Google_Service_Drive_DriveFile();
	    
	        $folder->setName( $folder_name );
	        $folder->setMimeType('application/vnd.google-apps.folder');
	        if( !empty( $parent_folder_id ) ){
	            $folder->setParents( [ $parent_folder_id ] );        
	        }

	        $result = $service->files->create( $folder );
	    
	        $folder_id = null;
	        
	        if( isset( $result['id'] ) && !empty( $result['id'] ) ){
	            $folder_id = $result['id'];
	        }
	    
	        return $folder_id;
	    }

	    return $folder_list[0]['id'];
	    
	}

	public static function check_folder_exists($folder_name){
	    $service = new Google_Service_Drive(self::$google_client);

	    $parameters['q'] = "mimeType='application/vnd.google-apps.folder' and name='$folder_name' and trashed=false";
	    $files = $service->files->listFiles($parameters);

	    $op = [];
	    foreach( $files as $k => $file ){
	        $op[] = $file;
	    }

	    return $op;
	}

	public static function insert_file_to_drive( $file_path, $file_name, $parent_file_id = null ){
	    $service = new Google_Service_Drive( self::$google_client );
	    $file = new Google_Service_Drive_DriveFile();

	    $file->setName( $file_name );

	    if( !empty( $parent_file_id ) ){
	        $file->setParents( [ $parent_file_id ] );        
	    }

	    $result = $service->files->create(
	        $file,
	        array(
	            'data' => file_get_contents($file_path),
	            'mimeType' => 'video/quicktime',
	            // 'mimeType' => 'application/octet-stream',
	        )
	    );

	    $is_success['status'] = false;
	    
	    if( isset( $result['name'] ) && !empty( $result['name'] ) ){
	        $is_success['status'] = true;
	        $is_success['file_id'] = $result['id'];
	    }

	    return $is_success;
	}

	public static function get_link_share($file_id){
	    $service = new Google_Service_Drive(self::$google_client);
	    $permissionService = new Google_Service_Drive_Permission();
		$permissionService->role = "reader";
		$permissionService->type = "anyone"; 
		$service->permissions->create($file_id, $permissionService);

	    $link = $service->files->get( $file_id, array( "fields" => "id, webViewLink" ) );
	    $view_link = isset($link['webViewLink']) ? $link['webViewLink'] : '';
	    return $view_link;
	} 

	public static function refresh_token(){
		self::__initialize();

	    self::$CI->load->library('curl');
	    $token = self::$CI->master_model->data('*','ls_t_gdrive_token')->get()->row();

	    $data = array(
	            "client_id" => $token->client_id,
	            "client_secret" => $token->client_secret,
	            "refresh_token" =>  $token->refresh_token,
	            "grant_type" =>  $token->grant_type,
	        );
	    $jsonDataEncoded = json_encode($data);  
	    // dd($jsonDataEncoded);      
	    self::$CI->curl->create('https://www.googleapis.com/oauth2/v4/token'); 

	    // Option
	    self::$CI->curl->option(CURLOPT_HTTPHEADER, array(            
	        'Content-type: application/json; Charset=UTF-8'
	    ));    

	    // Post - If you do not use post, it will just run a GET request
	    self::$CI->curl->post($jsonDataEncoded);  
	          
	    // Execute - returns responce
	    $result = self::$CI->curl->execute();

	    $response_token = json_decode($result,TRUE);
	    $update = self::$CI->master_model->update(['response_token' => json_encode($response_token)], $data,'ls_t_gdrive_token');
	    if ($update) {
	        return 'Success';
	    }else{
	        return 'Failed';
	    }
	}

  
 

}
