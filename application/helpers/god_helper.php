<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

if (!function_exists('get_value')) {
	function get_value($select = '', $from = '', $and_where = array(), $or_where = array(), $like = '', $field_like = '', $db = 'default')
	{
		$CI 						= &get_instance();
		$CI->load->model('master_model');
		$hasil 						= null;
		if ($select != null and $from != null and ($and_where != null or $or_where != null or ($like != null and $field_like != null))) {
			$select 				= $select . ' AS kolom';
			$data 					= $CI->master_model->data($select, $from, $and_where, $or_where, null, null, null, null, null, null, null, $like, $field_like, $db)->get();
			if ($data->num_rows() > 0) {
				$field 				= $data->row_array();
				$hasil 				= $field['kolom'];
			}
		}
		return $hasil;
	}
}
if (!function_exists('rupiah')) {
	function rupiah($angka)
		{
			$hasil_rupiah = "IDR " . number_format($angka,2,',','.');
			return $hasil_rupiah;
		}
}

if (!function_exists('uploadGambar')) {
	function uploadGambar($nama, $path)
		{
			$CI = &get_instance();
			$config['upload_path'] = $path;
			$config['allowed_types'] = 'jpeg|jpg|png';
      $config['max_size'] = 1024 * 100;
      $config['encrypt_name'] = false;

	    $CI->load->library('upload', $config);
			$CI->upload->initialize($config);
	    if (!$CI->upload->do_upload($nama)) {
				$response['pesan'] = FALSE;
				$response['nama_file'] = '';
	    }else{
					$response['pesan'] = TRUE;
					$response['nama_file'] = $path.$CI->upload->data('file_name');
			}
			return $response;
		}
}
if (!function_exists('uploadPdf')) {
	function uploadPdf($nama, $path)
		{
			$CI = &get_instance();
			$config['upload_path'] = $path;
			$config['allowed_types'] = 'pdf';
      $config['max_size'] = 1024 * 100;
      $config['encrypt_name'] = false;

	    $CI->load->library('upload', $config);
			$CI->upload->initialize($config);
	    if (!$CI->upload->do_upload($nama)) {
				$response['pesan'] = FALSE;
				$response['nama_file'] = '';
	    }else{
					$response['pesan'] = TRUE;
					$response['nama_file'] = $path.$CI->upload->data('file_name');
			}
			return $response;
		}
}
if (!function_exists('uploadMultiple')) {
	function uploadMultiple($nama = '', $path = '') {
		$CI = &get_instance();
      $count = count($_FILES[$nama]['name']);
      for($i=0;$i<$count;$i++){
        if(!empty($_FILES[$nama]['name'][$i])){

          $_FILES['file']['name'] = $_FILES[$nama]['name'][$i];
          $_FILES['file']['type'] = $_FILES[$nama]['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES[$nama]['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES[$nama]['error'][$i];
          $_FILES['file']['size'] = $_FILES[$nama]['size'][$i];

          $config['upload_path'] = $path;
          $config['allowed_types'] = '*';
          $config['max_size'] = 1024 * 100;
          $config['file_name'] = $_FILES[$nama]['name'][$i];

          $CI->load->library('upload',$config);
					$CI->upload->initialize($config);
					if (!$CI->upload->do_upload('file')) {
						$response['pesan'][] = FALSE;
						$response['nama_file'][] = '';
			    }else{
							$response['pesan'][] = TRUE;
							$response['nama_file'][] = $path.$CI->upload->data('file_name');
					}
        }
      }
			return $response;
   }
}
if (!function_exists('options')) {
	function options($table = '', $key = array(), $value = '', $label = '', $html = '', $default = '', $def_value = '', $order_by = '', $group_by = '', $db = 'default')
	{
		$CI = &get_instance();
		$db = $CI->load->database($db, TRUE);
		$db->from($table);
		if (!empty($key) || is_array($key)) {
			$db->where($key);
		}
		if ($order_by) {
			if (is_array($order_by)) {
				foreach ($order_by as $key_order => $val_order) {
					$db->order_by($key_order, $val_order);
				}
			} else {
				$db->order_by($order_by);
			}
		}
		if ($group_by) {
			$db->group_by($group_by);
		}
		$query = $db->get();

		$option = '';
		if ($html != '') {
			if ($default != '') {
				$option = '<option value="' . $def_value . '">' . $default . '</option>';
			}
		} else {
			if ($default != '') {
				$option[$def_value] = $default;
			}
		}

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				if (is_array($label)) {
					$label_multiple = array();
					foreach ($label as $key_label => $value_label) {
						$label_multiple[] = str_replace("'", " ", $row->$value_label);
					}

					if ($html != '') {
						$option .= '<option value="' . $row->$value . '">' . str_replace("'", " ", implode(' - ', $label_multiple)) . '</option>';
					} else {
						$option[$row->$value] = str_replace("'", " ", implode(' - ', $label_multiple));
					}
				} else {
					if ($html != '') {
						$option .= '<option value="' . $row->$value . '">' . str_replace("'", " ", $row->$label) . '</option>';
					} else {
						$option[$row->$value] = str_replace("'", " ", $row->$label);
					}
				}
			}
		}
		return $option;
	}
}
function options2($table = '', $key = array(), $value = '', $label = '', $html = '', $default = '', $def_value = '', $order_by = '', $group_by = '', $db = 'default', $wherein = array())
{
	$CI = &get_instance();
	$db = $CI->load->database($db, TRUE);
	$db->from($table);
	if (!empty($key) || is_array($key)) {
		$db->where($key);
	}
	if ($order_by) {
		if (is_array($order_by)) {
			foreach ($order_by as $key_order => $val_order) {
				$db->order_by($key_order, $val_order);
			}
		} else {
			$db->order_by($order_by);
		}
	}
	if ($group_by) {
		$db->group_by($group_by);
	}
	if (!empty($wherein)) {
		$db->where_in($wherein['col'], $wherein['val']);
	}
	$query = $db->get();

	$option = '';
	if ($html != '') {
		if ($default != '') {
			$option = '<option value="' . $def_value . '">' . $default . '</option>';
		}
	} else {
		$option = [];
		if ($default != '') {
			$option[$def_value] = $default;
		}
	}

	if ($query->num_rows() > 0) {
		foreach ($query->result() as $row) {
			if (is_array($label)) {
				$label_multiple = array();
				foreach ($label as $key_label => $value_label) {
					$label_multiple[] = str_replace("'", " ", $row->$value_label);
				}

				if ($html != '') {
					$option .= '<option value="' . $row->$value . '">' . str_replace("'", " ", implode(' - ', $label_multiple)) . '</option>';
				} else {
					$option[$row->$value] = str_replace("'", " ", implode(' - ', $label_multiple));
				}
			} else {
				if ($html != '') {
					$option .= '<option value="' . $row->$value . '">' . str_replace("'", " ", $row->$label) . '</option>';
				} else {
					$option[$row->$value] = str_replace("'", " ", $row->$label);
				}
			}
		}
	}
	return $option;
}

if (!function_exists('get_data')) {
	function get_data($select = '', $from = '', $and_where = array(), $or_where = array(), $like = '', $field_like = '', $order_by = '', $db = 'default')
	{
		$CI 					= &get_instance();
		$CI->load->model('master_model');
		$data 					= $CI->master_model->data($select, $from, $and_where, $or_where, null, null, null, null, null, null, $order_by, $like, $field_like, $db);
		return $data;
	}
}
if (!function_exists('get_count')) {
	function get_count($from = '', $and_where = array(), $or_where = array(), $like = '', $field_like = '', $db = 'default')
	{
		$CI 					= &get_instance();
		$CI->load->model('master_model');
		$data 					= $CI->master_model->data('count(*) as jumlah', $from, $and_where, $or_where, null, null, null, null, null, null, null, $like, $field_like, $db)->get();
		$field 					= $data->row();
		return $field->jumlah;
	}
}
if (!function_exists('cari_array')) {
	function cari_array($array, $search_list)
	{
		$result 			= array();
		foreach ($array as $key => $value) {
			foreach ($search_list as $k => $v) {
				if (!isset($value[$k]) || $value[$k] != $v) {
					continue 2;
				}
			}
			$result[] 		= $value;
		}
		return $result;
	}
}
if (!function_exists('offset')) {
	function offset($limit = 0, $page = 1)
	{
		$hasil = ($page > 1) ? ($page * $limit) - $limit : 0;
		return $hasil;
	}
}
if (!function_exists('jumlah_page')) {
	function jumlah_page($limit = 0, $jumlah_data = 0)
	{
		$hasil = 1;
		if ($limit > 0) {
			$hasil = ceil($jumlah_data / $limit);
		}
		return $hasil;
	}
}

if (!function_exists('start_page')) {
	function start_page($page_aktif = 1, $range_page = 1)
	{
		return ($page_aktif > $range_page) ? $page_aktif - $range_page : 1;
	}
}
if (!function_exists('end_page')) {
	function end_page($page_aktif = 1, $range_page = 1, $jumlah_page = 1)
	{
		return ($page_aktif < ($jumlah_page - $range_page)) ? $page_aktif + $range_page : $jumlah_page;
	}
}
if (!function_exists('boolean_input')) {
	function boolean_input($value = '')
	{
		$hasil 			= 'f';
		if ($value != null) {
			$hasil 		= $value;
		} else {
			$hasil 		= 'f';
		}
		return $hasil;
	}
}

if (!function_exists('text_to_bln_short')) {
	function text_to_bln_short($tgl)
	{
		$xreturn_ = '';
		if (strlen($tgl) == 7) {
			$bln = substr($tgl, 0, 2);
			$thn = substr($tgl, 3, 5);
		}

		if (trim($bln) != '' and $bln != '0') {
			$getbulan = array();
			$getbulan[1] = 'Jan';
			$getbulan[2] = 'Feb';
			$getbulan[3] = 'Mar';
			$getbulan[4] = 'Apr';
			$getbulan[5] = 'Mei';
			$getbulan[6] = 'Jun';
			$getbulan[7] = 'Jul';
			$getbulan[8] = 'Aug';
			$getbulan[9] = 'Sep';
			$getbulan[10] = 'Oct';
			$getbulan[11] = 'Nov';
			$getbulan[12] = 'Dec';
		}

		//return $xreturn_;
		return $getbulan[(int)$bln] . '-' . $thn;
	}
}

function tgl_ind_to_eng($tgl)
{
	$xreturn_ = '';
	if (trim($tgl) != '' && $tgl != '00-00-0000') {
		$tgl_eng = substr($tgl, 6, 4) . "-" . substr($tgl, 3, 2) . "-" . substr($tgl, 0, 2);
		$xreturn_ = $tgl_eng;
	}
	return $xreturn_;
}

if (!function_exists('format_date_ind')) {
	function format_date_ind($tgl, $param = 'short')
	{
		if (trim($tgl) != '' and $tgl != '0000-00-00') {
			$d = substr($tgl, 8, 2);
			$m = substr($tgl, 5, 2);
			$y = substr($tgl, 0, 4);
			$getbulan = array();
			$getbulan[1] = (($param == 'short') ? 'Jan' : 'Januari');
			$getbulan[2] = (($param == 'short') ? 'Feb' : 'Februari');
			$getbulan[3] = (($param == 'short') ? 'Mart' : 'Maret');
			$getbulan[4] = (($param == 'short') ? 'Apr' : 'April');
			$getbulan[5] = (($param == 'short') ? 'Mei' : 'Mei');
			$getbulan[6] = (($param == 'short') ? 'Jun' : 'Juni');
			$getbulan[7] = (($param == 'short') ? 'Jul' : 'Juli');
			$getbulan[8] = (($param == 'short') ? 'Agst' : 'Agustus');
			$getbulan[9] = (($param == 'short') ? 'Sept' : 'September');
			$getbulan[10] = (($param == 'short') ? 'Okt' : 'Oktober');
			$getbulan[11] = (($param == 'short') ? 'Nov' : 'November');
			$getbulan[12] = (($param == 'short') ? 'Des' : 'Desember');
			$tanggal = $d . " " . $getbulan[(int)$m] . " " . $y;
			return $tanggal;
		}
	}
}

function getCount($from, $where, $array = array(), $db = 'default')
{
	$CI = &get_instance();
	$db = $CI->load->database($db, TRUE);
	if (is_array($where)) {
		$db->select('count(*) as jml');
		$db->from($from);
		$db->where($where);
		$query = $db->get();
	} else {
		$sql = "select count(*) as jml from " . $from . " where " . $where;
		$query = $db->query($sql, $array);
	}
	// echo $db->last_query();die;
	$field = $query->row();
	return $field->jml;
}

function posisi($date)
{
	$cekpos = explode('-', $date);
	// dd($cekpos);
	// if (count($cekpos) == 2) {
	// 	$date = $cekpos[1] . '-' . $cekpos[2];
	// }
	// dd($date);

	$data['tgl'] = date('d', strtotime($date));
	$data['bulan'] = date('n', strtotime($date));
	$data['tahun'] = date('Y', strtotime($date));

	return $data;
}

function getValue($select, $from, $where, $array = array(), $db = 'default')
{
	$CI = &get_instance();
	$db = $CI->load->database($db, TRUE);
	if (is_array($where)) {
		$db->distinct();
		$db->select($select . ' AS nm_field');
		$db->from($from);
		$db->where($where);
		$query = $db->get();
	} else {
		$sql = "SELECT DISTINCT " . $select . " AS nm_field FROM " . $from . " WHERE " . $where;
		$query = $db->query($sql, $array);
	}

	$hasil = '';
	if ($query->num_rows() > 0) {
		$field = $query->row_array();
		$hasil = $field['nm_field'];
	}
	return $hasil;
}

function text_area($text = null)
{
	return str_replace(array('\r', '\n'), array(chr(13), chr(10)), str_replace("\\\\", "\\", str_replace("\'", "'", $text)));
}
function text_area_br($text = null)
{
	return str_replace(array(chr(13), chr(10)), '<br>', text_area($text));
}
function tgl_eng_to_ind($tgl)
{
	$xreturn_ = '';
	if (trim($tgl) != '' and $tgl != '0000-00-00') {
		$tgl_ind = substr($tgl, 8, 2) . "-" . substr($tgl, 5, 2) . "-" . substr($tgl, 0, 4);
		$xreturn_ = $tgl_ind;
	}
	return $xreturn_;
}

function get_format($num, $f = 'nominal', $dec = 0, $blk = '.', $kom = ',')
{
    if (is_numeric($num) && $num > 0) {
        if ($f == 'nominal') {
            return number_format($num, $dec, $blk, $kom);
        }

        if ($f == 'number') {
            return $num;
        }
    } else {
        if ($f == 'number') {
            return (($num == 0) ? '' : $num);
        } else {
            return $num;
        }
    }
}

function posisi_triwulan($posisi)
{
    $posBaru = explode('-', $posisi);
    $posBulan[] = date('n', strtotime($posBaru[0]));
    $posBulan[] = date('n', strtotime($posBaru[1]));
    $posBulan[] = $posBaru[2];
    return $posBulan;
}

function init_options($array_data, $key_index = null, $value_index = null, $default = '- Pilih -') {
	$result = null;

	$key_index = $key_index != null ? $key_index : 0;
	$value_index = $value_index != null ? $value_index : 1;
	if ($default) {
		$result .= '<option value="">'.$default.'</option>';
	}
	foreach ($array_data as $i => $row) {
		$result .= '<option value="' . $row[$key_index] . '" >' . $row[$value_index] . '</option>';
		// $result .= '<option value="' . $row[$key_index] . '" ' . ( $i == 0 ? 'selected' : '' ) . '>' . $row[$value_index] . '</option>';
	}

	return $result;
}

function indo_bulan($bulan){
	if ($bulan == 1) {
		return 'Januari';
	}elseif ($bulan == 2) {
		return 'Februari';
	}elseif ($bulan == 3) {
		return 'Maret';
	}elseif ($bulan == 4) {
		return 'April';
	}elseif ($bulan == 5) {
		return 'Mei';
	}elseif ($bulan == 6) {
		return 'Juni';
	}elseif ($bulan == 7) {
		return 'Juli';
	}elseif ($bulan == 8) {
		return 'Agustus';
	}elseif ($bulan == 9) {
		return 'September';
	}elseif ($bulan == 10) {
		return 'Oktober';
	}elseif ($bulan == 11) {
		return 'November';
	}else{
		return 'Desember';
	}
}

function eng_date($date)
{
    if (trim($date) != '' and $date != '0000-00-00') {
        $newdate = new DateTime($date);
        $pcs = explode("-", $date);
        $y = $newdate->format('Y');
        $m = $newdate->format('n');
        $d = $newdate->format('j');
        $wk = $newdate->format('w');

        $getbulan = array();
        $getbulan[1] = 'January';
        $getbulan[2] = 'February';
        $getbulan[3] = 'March';
        $getbulan[4] = 'April';
        $getbulan[5] = 'May';
        $getbulan[6] = 'June';
        $getbulan[7] = 'July';
        $getbulan[8] = 'August';
        $getbulan[9] = 'September';
        $getbulan[10] = 'October';
        $getbulan[11] = 'November';
        $getbulan[12] = 'December';

        return $d . " " . $getbulan[$m] . " " . $y;
    }
}

function indo_date($date)
{
    if (trim($date) != '' and $date != '0000-00-00') {
        $newdate = new DateTime($date);
        $pcs = explode("-", $date);
        $y = $newdate->format('Y');
        $m = $newdate->format('n');
        $d = $newdate->format('j');
        $wk = $newdate->format('w');

        $getbulan = array();
        $getbulan[1] = 'Januari';
        $getbulan[2] = 'Februari';
        $getbulan[3] = 'Maret';
        $getbulan[4] = 'April';
        $getbulan[5] = 'Mei';
        $getbulan[6] = 'Juni';
        $getbulan[7] = 'Juli';
        $getbulan[8] = 'Agustus';
        $getbulan[9] = 'September';
        $getbulan[10] = 'Oktober';
        $getbulan[11] = 'November';
        $getbulan[12] = 'Desember';

        $gethari = array();
        $gethari[0] = 'Minggu';
        $gethari[1] = 'Senin';
        $gethari[2] = 'Selasa';
        $gethari[3] = 'Rabu';
        $gethari[4] = 'Kamis';
        $gethari[5] = 'Jumat';
        $gethari[6] = 'Sabtu';

        return $gethari[$wk] . ", " . $d . " " . $getbulan[$m] . " " . $y;
    }
}

function indo_date_hour($date)
{
    if (trim($date) != '' and $date != '0000-00-00') {
        $newdate = new DateTime($date);
        $datehour = strtotime($date);
        $pcs = explode("-", $date);
        $y = $newdate->format('Y');
        $m = $newdate->format('n');
        $d = $newdate->format('j');
        $wk = $newdate->format('w');

        $getbulan = array();
        $getbulan[1] = 'Januari';
        $getbulan[2] = 'Februari';
        $getbulan[3] = 'Maret';
        $getbulan[4] = 'April';
        $getbulan[5] = 'Mei';
        $getbulan[6] = 'Juni';
        $getbulan[7] = 'Juli';
        $getbulan[8] = 'Agustus';
        $getbulan[9] = 'September';
        $getbulan[10] = 'Oktober';
        $getbulan[11] = 'November';
        $getbulan[12] = 'Desember';

        $gethari = array();
        $gethari[0] = 'Minggu';
        $gethari[1] = 'Senin';
        $gethari[2] = 'Selasa';
        $gethari[3] = 'Rabu';
        $gethari[4] = 'Kamis';
        $gethari[5] = 'Jumat';
        $gethari[6] = 'Sabtu';

        return $gethari[$wk] . " - " . $d . " " . $getbulan[$m] . " " . $y . " - " . date('H:i', $datehour);
    }
}

function indo_date_noday($date)
{
    if (trim($date) != '' and $date != '0000-00-00') {
        $newdate = new DateTime($date);
        $pcs = explode("-", $date);
        $y = $newdate->format('Y');
        $m = $newdate->format('n');
        $d = $newdate->format('j');
        $wk = $newdate->format('w');

        $getbulan = array();
        $getbulan[1] = 'Januari';
        $getbulan[2] = 'Februari';
        $getbulan[3] = 'Maret';
        $getbulan[4] = 'April';
        $getbulan[5] = 'Mei';
        $getbulan[6] = 'Juni';
        $getbulan[7] = 'Juli';
        $getbulan[8] = 'Agustus';
        $getbulan[9] = 'September';
        $getbulan[10] = 'Oktober';
        $getbulan[11] = 'November';
        $getbulan[12] = 'Desember';

        return $d . " " . $getbulan[$m] . " " . $y;
    }
}

function options_status($swit = '')
{

    if ($swit == 'color') {
        $data = array(
            '' => '',
            'D' => 'primary',
            'R' => 'warning',
            'S' => 'success',
            'X' => 'danger',
            'T' => 'warning',
            'P' => 'primary',
            'TL' => 'primary',
            'N' => 'danger',
            'BTL' => 'danger',
            'DTL' => 'warning',
            'STL' => 'success',
            'TDTL' => 'danger',
        );
    } else {
        $data = array(
            '' => '',
            'D' => 'Draft',
            'SD' => 'Draft',
            'R' => 'Review',
            'S' => 'Selesai',
            'X' => 'Ditolak',
            'T' => 'Ditolak',
            'P' => 'Assessment process',
            'TL' => 'Sedang ditindak lanjuti',
            'N' => 'Belum di set',
            'BTL' => 'Belum Ditindaklanjuti',
            'DTL' => 'Dalam Proses Tindak Lanjut',
            'STL' => 'Sudah Ditindaklanjuti',
            'TDTL' => 'Tidak Dapat Ditindaklanjuti',
            'nearmiss' => 'Near Miss',
            'softloss' => 'Soft Loss',
            'lossevent' => 'Loss Event',
            'potensialrisk' => 'Potential Risk',
        );
    }

    return $data;
}

function status_value($status = '', $data = '')
{
    $options_status = options_status($data);
    if ($status != '') {
        $status = (isset($options_status[$status]) ? $options_status[$status] : '');
    }
    return $status;
}

function options_group($where = '', $tabel = '', $id = '', $nama = '', $parent = '', $default = '', $key = '', $db = 'default')
{
    if ($where == '' || $where == null) {
        $where['(' . $parent . ' IS NULL or ' . $parent . ' = 0)'] = null;
    }

    $query = get_data($where, '', '', $tabel, $db);

    $data = array();
    $data1 = array();

    if ($default != '') {
        $data[$key] = $default;
    }

    if ($query->num_rows() > 0) {
        foreach ($query->result() as $rp) {
            unset($where['(' . $parent . ' IS NULL or ' . $parent . ' = 0)']);
            $where[$parent] = $rp->$id;

            $cek_child = getCount($tabel, $where, null, $db);
            if ($cek_child > 0) {
                $query2 = get_data($where, '', '', $tabel, $db);
                if ($query2->num_rows() > 0) {
                    foreach ($query2->result() as $rp2) {
                        $data1[$rp->$nama][$rp2->$id] = $rp2->$nama;
                    }
                }
            } else {
                $data[$rp->$id] = $rp->$nama;
            }
        }
    }

    return $data + $data1;
}

function updateNotifChat($data, $cond)
{
		$CI = &get_instance();
		$CI->load->model('master_model');
		$update = $CI->master_model->update($data, $cond, 't_notification');

		if ($update) {
				return true;
		}
		return false;
}

function setStatusNotif($idNotif, $statusNotif)
{
	$CI = &get_instance();
	$CI->load->model('master_model');
	$update = $CI->master_model->update(['st_viewed' => $statusNotif], ['id' => $idNotif], 't_notification');
}

function seenNotif($idNotif, $urlRedirect)
{
	setStatusNotif($idNotif, 1);

	echo json_encode(site_url($urlRedirect));
}

function closeNotif($idNotif, $urlRedirect)
{
	setStatusNotif($idNotif, 2);

	echo json_encode(site_url($urlRedirect));
}


function convert_rating($array){
	$CI = &get_instance();
	if (is_array($array)) {
		$return = [];
		foreach ($array as $key => $value) {
				$value['reviewer'] = reviewer($value['id'])->reviewer;
		    $value['rating'] = $value['score'];
		    $split = explode('.', $value['rating']);
		    $value['rating_depan'] = $split[0];
		    $value['rating_belakang'] = '';
		    if (isset($split[1])) {
	        	$value['rating_belakang'] = $split[1];
		    }
      	$return[]=$value;
		}
		return $return;
	}
}

function reviewer($id){
	$CI = &get_instance();
	$CI->load->model('master_model');
	$reviewer = $CI->master_model->data('count(id_user) as reviewer', 'ls_t_rating', ['id_course' => $id])->get()->row();
	return $reviewer;
}

function owned_course(){
	$CI = &get_instance();
	$CI->load->model('master_model');
	$check_unique = $CI->master_model->data('b.pembicara_webinar, b.id_zoom_webinar, b.pass_zoom_webinar, b.id, b.pelaksanaan_led, b.pelaksanaan_training, b.pelaksanaan_webinar, b.judul, b.kontributor, b.image, b.pelaksanaan, b.harga, c.nama_depan, c.nama_belakang, c.institusi, d.type_course as nama_type, d.id as id_type, e.tanggal_pembelian', 'ls_t_detail_pembelian a', ['a.id_user' => $CI->session->userdata('id')])
						->join('ls_m_course b', 'a.id_course = b.id', 'INNER')
						->join('ls_m_user c', 'b.kontributor = c.id', 'LEFT')
						->join('ls_m_type_course d', 'a.id_type_course = d.id', 'LEFT')
						->join('ls_t_pembayaran e', 'a.id_pembayaran = e.id', 'LEFT')
						->get()->result_array();
	$owned = [];
	foreach ($check_unique as $key => $value) {
		$get_owned = $CI->master_model->owned($value['id'], $CI->session->userdata('id'));
		$value['type_course'] = $get_owned;
		$owned[]=$value;
	}
	return $owned;
}

function getYoutubeEmbedUrl($url)
{
     $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
     $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

    if (preg_match($longUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    if (preg_match($shortUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }
    return 'https://www.youtube.com/embed/' . $youtube_id ;
}
function getYoutubeEmbedId($url)
{
     $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
     $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

    if (preg_match($longUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    if (preg_match($shortUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }
    return $youtube_id ;
}
function getGDriveEmbedUrl($url)
{
	$arr = explode("/", $url);
	$id = $arr[5];
    $key = 'AIzaSyDgIkkyJLkyFYE1CKjJxcd7cBhvbJ-BSF4';
    // return 'https://drive.google.com/u/6/uc?id='.$id.'&export=download';
    return 'https://www.googleapis.com/drive/v3/files/'.$id.'?alt=media&key='.$key;
}

function generateCertificate($id, $id_user=''){
	// font directory
	$CI = &get_instance();
	$CI->load->model('users/users_mycourse_model','model');
	$type = $CI->model->getCertificateType($id)->row();
	$dc = $CI->model->getCertificate($id,$id_user)->row();
	$filename = str_replace(' ', '-', strtolower($dc->judul). '-'. strtolower($dc->type_course) .'-'.$CI->session->userdata('id'));
	// if (file_exists(BASEPATH."../assets/certificate/master/".$CI->session->userdata('id').".png")) {
	// 	unlink(BASEPATH."../assets/certificate/master/".$CI->session->userdata('id').".png");
	// }
	generateQRcode(base_url('users/users_sertification?no='.$type->certificate_number));
	if($type->type == 'webinar'){
		$filename = str_replace(' ', '-', strtolower($dc->judul). '-webinar-'.$CI->session->userdata('id'));
		if (!empty($dc->signature_title2) && !empty($dc->signature_name2) ) {
		    generateCertWebinar2sign($dc,$filename);
		}else{
		    generateCertWebinar1sign($dc, $filename);
		}
	} else{
	    if (!empty($dc->signature_title2) && !empty($dc->signature_name2) ) {
		    generateCertCourse2sign($dc,$filename);
		}else{
		    generateCertCourse1sign($dc, $filename);
		}
	}
	return $filename;
}

function generateCertWebinar1sign($dc, $filename){
	$CI = &get_instance();
	$drFont = BASEPATH."../assets/certificate/font/Metropolis-Medium.otf";
	$drFont1 = BASEPATH."../assets/certificate/font/Metropolis-Bold.otf";

	$image = BASEPATH."../assets/certificate/master/webinar-cert-1.png";

	$CI->load->model('users/users_mycourse_model','model');

	$createimage = imagecreatefrompng($image);
	//this is going to be created once the generate button is clicked
	$output = BASEPATH."../assets/certificate/temp/".$filename.".png";

	//then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
	$white = imagecolorallocate($createimage, 255, 255, 255);
	$black = imagecolorallocate($createimage, 0, 0, 0);

	// function to display name on certificate picture
	if ($dc->nama_sertifikat != '') {
		$nameParticipant = imagettftext($createimage, 120, 0, 200, 1100, $black,$drFont1, $dc->nama_sertifikat);
	}else{
		$nameParticipant = imagettftext($createimage, 120, 0, 200, 1100, $black,$drFont1, $dc->nama_depan .' '. $dc->nama_belakang);
	}

	// $underParticipant = imagettftext($createimage, 50, 0, 200, 1250, $black,$drFont1, 'as a participants');

	// $typeWebinar = imagettftext($createimage, 40, 0, 200, 1450, $white, $drFont1, $dc->certificate_category);

	if ($dc->id_type_course == null || $dc->id_type_course == '') {
		$date = strtotime($dc->pelaksanaan_webinar);
		$date_cert =  date('F d, Y', $date);
	}else{
		$date = strtotime($dc->date_grad);
		$date_cert =  date('F d, Y', $date);
	}

	$timeWebinar = imagettftext($createimage, 40, 0, 2490, 1500, $white, $drFont, $date_cert . (!empty($dc->duration) ? ' - '. $dc->duration: ''));

	$nameWebinar = imagettftext($createimage, 60, 0, 200, 1600, $white, $drFont1, wordwrap($dc->judul, 50, "\n"));

	$typeWebinar = imagettftext($createimage, 40, 0, 200, 1850, $white, $drFont1, 'Presented By');


	$presentedName = imagettftext($createimage, 40, 0, 200, 1910, $white, $drFont1, wordwrap($dc->presented_name1, 50, "\n"));

	if (strlen($dc->presented_name1) > 50) {
		$titleName = imagettftext($createimage, 35, 0, 200, 2030, $white, $drFont, wordwrap($dc->presented_title1, 75, "\n"));
	}else{
		$titleName = imagettftext($createimage, 35, 0, 200, 1970, $white, $drFont, wordwrap($dc->presented_title1, 75, "\n"));
	}

	if (strlen($dc->presented_title1) > 120) {
		$prenseted2_addjust = 50;
	}else{
		$prenseted2_addjust = 0;
	}

	$presentedName = imagettftext($createimage, 40, 0, 200, 2100 + $prenseted2_addjust , $white, $drFont1, wordwrap($dc->presented_name2, 50, "\n"));
	if (strlen($dc->presented_name2) > 50) {
		$titleName = imagettftext($createimage, 35, 0, 200, 2220 + $prenseted2_addjust, $white, $drFont, wordwrap($dc->presented_title2, 75, "\n"));
	}else{
		$titleName = imagettftext($createimage, 35, 0, 200, 2160 + $prenseted2_addjust, $white, $drFont, wordwrap($dc->presented_title2, 75, "\n"));
	}

	$institution1 = imagettftext($createimage, 35, 0, 2600 , 1700, $white, $drFont, $dc->institution1);

	$institution2 = imagettftext($createimage, 35, 0, 1800, 1700, $white, $drFont, $dc->institution2);

	$presentedName = imagettftext($createimage, 35, 0, 2550, 2200, $white, $drFont1, wordwrap($dc->signature_name, 40, "\n"));

	$certNumber = imagettftext($createimage, 35, 0, 890, 2305, $white, $drFont, $dc->certificate_number_trans);

	$titleSign = imagettftext($createimage, 35, 0, 2550, 2290, $white, $drFont, $dc->signature_title);

	imagepng($createimage,$output,3);

	$jpeg = imagecreatefrompng($output);


	if (!empty($dc->sign_pic1))
		$sign1 = imagecreatefrompng(BASEPATH."../".$dc->sign_pic1);

	$logo = imagecreatefrompng(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");
	list($logowidth, $logoheight) = getimagesize(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");

	list($width, $height) = getimagesize($output);

	if (!empty($dc->sign_pic1))
		list($newwidthsign, $newheightsign) = getimagesize(BASEPATH."../".$dc->sign_pic1);

	$out = imagecreatetruecolor($width, $height);
	imagecopyresampled($out, $jpeg, 0, 0, 0, 0, $width, $height, $width, $height);

	if (!empty($dc->sign_pic1))
		imagecopyresampled($out, $sign1, 2600, 1800, 0, 0, 300, 300, $newwidthsign, $newheightsign);

	imagecopyresampled($out, $logo, 210, 2265, 0, 0, 135, 135, $logowidth, $logoheight);
	// imagecopyresampled($out, $logo, 400, 55, 0, 0, 105, 65, $logowidth, $logoheight);
	return imagejpeg($out, $output, 100);
}

function generateCertWebinar2sign($dc, $filename){

	$CI = &get_instance();
	$drFont = BASEPATH."../assets/certificate/font/Metropolis-Medium.otf";
	$drFont1 = BASEPATH."../assets/certificate/font/Metropolis-Bold.otf";

	$image = BASEPATH."../assets/certificate/master/webinar-cert-2.png";

	$CI->load->model('users/users_mycourse_model','model');

	$createimage = imagecreatefrompng($image);

	//this is going to be created once the generate button is clicked
	$output = BASEPATH."../assets/certificate/temp/".$filename.".png";

	//then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
	$white = imagecolorallocate($createimage, 255, 255, 255);
	$black = imagecolorallocate($createimage, 0, 0, 0);

	// function to display name on certificate picture
	if ($dc->nama_sertifikat != '') {
		$nameParticipant = imagettftext($createimage, 120, 0, 200, 1100, $black,$drFont1, $dc->nama_sertifikat);
	}else{
		$nameParticipant = imagettftext($createimage, 120, 0, 200, 1100, $black,$drFont1, $dc->nama_depan .' '. $dc->nama_belakang);
	}
	// $underParticipant = imagettftext($createimage, 50, 0, 200, 1250, $black,$drFont1, 'as a participants');

	// $typeWebinar = imagettftext($createimage, 40, 0, 200, 1450, $white, $drFont1, $dc->certificate_category);

	if ($dc->id_type_course == null || $dc->id_type_course == '') {
		$date = strtotime($dc->pelaksanaan_webinar);
		$date_cert =  date('F d, Y', $date);
	}else{
		$date = strtotime($dc->date_grad);
		$date_cert =  date('F d, Y', $date);
	}

	$timeWebinar = imagettftext($createimage, 40, 0, 2490, 1500, $white, $drFont, $date_cert . (!empty($dc->duration) ? ' - '. $dc->duration: ''));

	$nameWebinar = imagettftext($createimage, 60, 0, 200, 1600, $white, $drFont1, wordwrap($dc->judul, 50, "\n"));

	$typeWebinar = imagettftext($createimage, 40, 0, 200, 1850, $white, $drFont1, 'Presented By');

	$presentedName = imagettftext($createimage, 40, 0, 200, 1910, $white, $drFont1, wordwrap($dc->presented_name1, 50, "\n"));

	if (strlen($dc->presented_name1) > 50) {
			$titleName = imagettftext($createimage, 35, 0, 200, 2030, $white, $drFont, wordwrap($dc->presented_title1, 60, "\n"));
	}else{
		$titleName = imagettftext($createimage, 35, 0, 200, 1970, $white, $drFont, wordwrap($dc->presented_title1, 60, "\n"));
	}
	if (strlen($dc->presented_title1) > 120) {
		$prenseted2_addjust = 50;
	}else{
		$prenseted2_addjust = 0;
	}

	$presentedName = imagettftext($createimage, 40, 0, 200, 2100 + $prenseted2_addjust, $white, $drFont1, wordwrap($dc->presented_name2, 50, "\n"));
	if (strlen($dc->presented_name2) > 50) {
		$titleName = imagettftext($createimage, 35, 0, 200, 2220 + $prenseted2_addjust, $white, $drFont, wordwrap($dc->presented_title2, 60, "\n"));
	}else{
		$titleName = imagettftext($createimage, 35, 0, 200, 2160 + $prenseted2_addjust, $white, $drFont, wordwrap($dc->presented_title2, 60, "\n"));
	}

	$institution1 = imagettftext($createimage, 35, 0, 2600 , 1700, $white, $drFont, $dc->institution1);

	$institution2 = imagettftext($createimage, 35, 0, 1800, 1700, $white, $drFont, $dc->institution2);

	$presentedName = imagettftext($createimage, 35, 0, 2550, 2200, $white, $drFont1, wordwrap($dc->signature_name, 40, "\n"));

	$presentedName = imagettftext($createimage, 35, 0, 1800, 2200, $white, $drFont1, wordwrap($dc->signature_name2, 40, "\n"));


	$certNumber = imagettftext($createimage, 35, 0, 660, 2318, $white, $drFont, $dc->certificate_number_trans);

	$titleSign = imagettftext($createimage, 35, 0, 2550, 2290, $white, $drFont, $dc->signature_title);
	$titleSign = imagettftext($createimage, 35, 0, 1800, 2290, $white, $drFont,  $dc->signature_title2);

	imagepng($createimage,$output,3);

	$jpeg = imagecreatefrompng($output);


	if (!empty($dc->sign_pic1))
		$sign1 = imagecreatefrompng(BASEPATH."../".$dc->sign_pic1);
	if (!empty($dc->sign_pic2))
		$sign2 = imagecreatefrompng(BASEPATH."../".$dc->sign_pic2);

	$qr = imagecreatefrompng(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");
	list($qrwidth, $qrheight) = getimagesize(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");

	list($width, $height) = getimagesize($output);

	if (!empty($dc->sign_pic1))
		list($newwidthsign, $newheightsign) = getimagesize(BASEPATH."../".$dc->sign_pic1);
	if (!empty($dc->sign_pic2))
		list($newwidthsign2, $newheightsign2) = getimagesize(BASEPATH."../".$dc->sign_pic2);

	$out = imagecreatetruecolor($width, $height);
	imagecopyresampled($out, $jpeg, 0, 0, 0, 0, $width, $height, $width, $height);

	if (!empty($dc->sign_pic1))
		imagecopyresampled($out, $sign1, 2600, 1800, 0, 0, 300, 300, $newwidthsign, $newheightsign);
	if (!empty($dc->sign_pic2))
		imagecopyresampled($out, $sign2, 1800, 1800, 0, 0, 300, 300, $newwidthsign2, $newheightsign2);


	imagecopyresampled($out, $qr, 210, 2265, 0, 0, 135, 135, $qrwidth, $qrheight);

	return imagejpeg($out, $output, 100);
}
function generateCertCourse2sign($dc, $filename){

	$CI = &get_instance();
	$drFont = BASEPATH."../assets/certificate/font/Verdana-Bold.ttf";
	$drFont3 = BASEPATH."../assets/certificate/font/Verdana.ttf";
	$drFont1 = BASEPATH."../assets/certificate/font/Monotype-Corsiva.ttf";
	if ($dc->id_type_course == 1) {
		$image = BASEPATH."../assets/certificate/master/course-cert-sf-2.png";
	}else{
		$image = BASEPATH."../assets/certificate/master/course-cert-2.png";
	}

	$CI->load->model('users/users_mycourse_model','model');

	$createimage = imagecreatefrompng($image);

	//this is going to be created once the generate button is clicked

	$output = BASEPATH."../assets/certificate/temp/".$filename.".png";

	//then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
	$white = imagecolorallocate($createimage, 255, 255, 255);
	$black = imagecolorallocate($createimage, 0, 0, 0);

	// function to display name on certificate picture

	if ($dc->nama_sertifikat != '') {
		// determine the size of the text so we can center it
		$box = imagettfbbox(130, 0, $drFont1, $dc->nama_sertifikat);
		$text_width = abs($box[2]) - abs($box[0]);
		$text_height = abs($box[5]) - abs($box[3]);
		$image_width = imagesx($createimage);
		$image_height = imagesy($createimage);
		$x = ($image_width - $text_width) / 2;

		// add text
		imagettftext($createimage, 130, 0, $x, 1040, $black, $drFont1, $dc->nama_sertifikat);

		// $nameParticipant = imagettftext($createimage, 130, 0, $x_name, 1040, $black,$drFont1, $dc->nama_sertifikat);
	}else{
		// determine the size of the text so we can center it
		$box = imagettfbbox(130, 0, $drFont1, $dc->nama_depan .' '. $dc->nama_belakang);
		$text_width = abs($box[2]) - abs($box[0]);
		$text_height = abs($box[5]) - abs($box[3]);
		$image_width = imagesx($createimage);
		$image_height = imagesy($createimage);
		$x = ($image_width - $text_width) / 2;

		// add text
		imagettftext($createimage, 130, 0, $x, 1040, $black, $drFont1, $dc->nama_depan .' '. $dc->nama_belakang);
	}

	$date = strtotime($dc->date_grad);
	$date_cert =  date('F d, Y', $date);

	$date = strtotime($dc->pelaksanaan_led);
	$date_held =  date('F d, Y', $date);


	$lines = explode("\n", wordwrap($dc->judul, 43, "\n"));

	$box_judul = imagettfbbox(80, 0, $drFont, $lines[0]);
	$text_width = abs($box_judul[2]) - abs($box_judul[0]);
	$image_width = imagesx($createimage);
	$x = ($image_width - $text_width) / 2;

	imagettftext($createimage, 80, 0, $x, 1290, $black, $drFont, $lines[0]);


	if (isset($lines[1])) {
		$box_judul2 = imagettfbbox(80, 0, $drFont, $lines[1]);
		$text_width2 = abs($box_judul2[2]) - abs($box_judul2[0]);
		$image_width2 = imagesx($createimage);
		$x = ($image_width2 - $text_width2) / 2;

		imagettftext($createimage, 80, 0, $x, 1400, $black, $drFont, $lines[1]);
	}



	$signname = imagettftext($createimage, 35, 0, 2190, 2090, $black, $drFont, wordwrap($dc->signature_name, 40, "\n"));
	$signname2 = imagettftext($createimage, 35, 0, 690, 2090, $black, $drFont, wordwrap($dc->signature_name2, 40, "\n"));

	$certNumber = imagettftext($createimage, 30, 0, 670, 1580, $black, $drFont3, $dc->certificate_number_trans);
	$timeWebinar = imagettftext($createimage, 30, 0, 2160, 1580, $black, $drFont3, $date_cert);

	// $heldon = imagettftext($createimage, 30, 0, 1590, 1464, $black, $drFont3, 'Held on '.$date_held);

	$titleSign = imagettftext($createimage, 35, 0, 2190, 2170, $black, $drFont3, $dc->signature_title);
	$titleSign = imagettftext($createimage, 35, 0, 690, 2170, $black, $drFont3, $dc->signature_title2);

	imagepng($createimage,$output,3);

	$jpeg = imagecreatefrompng($output);


	if (!empty($dc->sign_pic1))
		$sign1 = imagecreatefrompng(BASEPATH."../".$dc->sign_pic1);
	if (!empty($dc->sign_pic2))
		$sign2 = imagecreatefrompng(BASEPATH."../".$dc->sign_pic2);

	$qr = imagecreatefrompng(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");
	list($qrwidth, $qrheight) = getimagesize(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");

	list($width, $height) = getimagesize($output);

	if (!empty($dc->sign_pic1))
		list($newwidthsign, $newheightsign) = getimagesize(BASEPATH."../".$dc->sign_pic1);
	if (!empty($dc->sign_pic2))
		list($newwidthsign2, $newheightsign2) = getimagesize(BASEPATH."../".$dc->sign_pic2);

	$out = imagecreatetruecolor($width, $height);
	imagecopyresampled($out, $jpeg, 0, 0, 0, 0, $width, $height, $width, $height);

	if (!empty($dc->sign_pic1))
		imagecopyresampled($out, $sign1, 2300, 1700, 0, 0, 300, 300, $newwidthsign, $newheightsign);
	if (!empty($dc->sign_pic2))
		imagecopyresampled($out, $sign2, 800, 1700, 0, 0, 300, 300, $newwidthsign2, $newheightsign2);

	imagecopyresampled($out, $qr, 500, 1535, 0, 0, 150, 150, $qrwidth, $qrheight);

	return imagejpeg($out, $output, 100);
}
function generateCertCourse1sign($dc, $filename){

	$CI = &get_instance();
	$drFont = BASEPATH."../assets/certificate/font/Verdana-Bold.ttf";
	$drFont3 = BASEPATH."../assets/certificate/font/Verdana.ttf";
	$drFont1 = BASEPATH."../assets/certificate/font/Monotype-Corsiva.ttf";

	if ($dc->id_type_course == 1) {
		$image = BASEPATH."../assets/certificate/master/course-cert-sf-1.png";
	}else{
		$image = BASEPATH."../assets/certificate/master/course-cert-1.png";
	}

	$CI->load->model('users/users_mycourse_model','model');

	$createimage = imagecreatefrompng($image);

	//this is going to be created once the generate button is clicked
	$output = BASEPATH."../assets/certificate/temp/".$filename.".png";

	//then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
	$white = imagecolorallocate($createimage, 255, 255, 255);
	$black = imagecolorallocate($createimage, 0, 0, 0);

	// function to display name on certificate picture
	if ($dc->nama_sertifikat != '') {
		// determine the size of the text so we can center it
		$box = imagettfbbox(130, 0, $drFont1, $dc->nama_sertifikat);
		$text_width = abs($box[2]) - abs($box[0]);
		$text_height = abs($box[5]) - abs($box[3]);
		$image_width = imagesx($createimage);
		$image_height = imagesy($createimage);
		$x = ($image_width - $text_width) / 2;

		// add text
		imagettftext($createimage, 130, 0, $x, 1040, $black, $drFont1, $dc->nama_sertifikat);

		// $nameParticipant = imagettftext($createimage, 130, 0, $x_name, 1040, $black,$drFont1, $dc->nama_sertifikat);
	}else{
		// determine the size of the text so we can center it
		$box = imagettfbbox(130, 0, $drFont1, $dc->nama_depan .' '. $dc->nama_belakang);
		$text_width = abs($box[2]) - abs($box[0]);
		$text_height = abs($box[5]) - abs($box[3]);
		$image_width = imagesx($createimage);
		$image_height = imagesy($createimage);
		$x = ($image_width - $text_width) / 2;

		// add text
		imagettftext($createimage, 130, 0, $x, 1040, $black, $drFont1, $dc->nama_depan .' '. $dc->nama_belakang);
	}

	$date = strtotime($dc->date_grad);
	$date_cert =  date('F d, Y', $date);

	$date = strtotime($dc->pelaksanaan_led);
	$date_held =  date('F d, Y', $date);


	$lines = explode("\n", wordwrap($dc->judul, 43, "\n"));

	$box_judul = imagettfbbox(80, 0, $drFont, $lines[0]);
	$text_width = abs($box_judul[2]) - abs($box_judul[0]);
	$image_width = imagesx($createimage);
	$x = ($image_width - $text_width) / 2;

	imagettftext($createimage, 80, 0, $x, 1290, $black, $drFont, $lines[0]);


	if (isset($lines[1])) {
		$box_judul2 = imagettfbbox(80, 0, $drFont, $lines[1]);
		$text_width2 = abs($box_judul2[2]) - abs($box_judul2[0]);
		$image_width2 = imagesx($createimage);
		$x = ($image_width2 - $text_width2) / 2;

		imagettftext($createimage, 80, 0, $x, 1400, $black, $drFont, $lines[1]);
	}


	$certNumber = imagettftext($createimage, 30, 0, 670, 1580, $black, $drFont3, $dc->certificate_number_trans);
	$timeWebinar = imagettftext($createimage, 30, 0, 2160, 1580, $black, $drFont3, $date_cert);

	// $heldon = imagettftext($createimage, 30, 0, 1510, 1464, $black, $drFont3, 'Held on '.$date_held);

	$signname = imagettftext($createimage, 35, 0, 1450, 2090, $black, $drFont, wordwrap($dc->signature_name, 40, "\n"));
	$titleSign = imagettftext($createimage, 35, 0, 1450, 2170, $black, $drFont3, $dc->signature_title);

	imagepng($createimage,$output,3);

	$jpeg = imagecreatefrompng($output);


	if (!empty($dc->sign_pic1))
		$sign1 = imagecreatefrompng(BASEPATH."../".$dc->sign_pic1);

	$qr = imagecreatefrompng(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");
	list($qrwidth, $qrheight) = getimagesize(BASEPATH."../assets/certificate/qrcode/qr-".$CI->session->userdata('id').".png");

	list($width, $height) = getimagesize($output);

	if (!empty($dc->sign_pic1))
		list($newwidthsign, $newheightsign) = getimagesize(BASEPATH."../".$dc->sign_pic1);

	$out = imagecreatetruecolor($width, $height);
	imagecopyresampled($out, $jpeg, 0, 0, 0, 0, $width, $height, $width, $height);

	if (!empty($dc->sign_pic1))
		imagecopyresampled($out, $sign1, 1590, 1700, 0, 0, 300, 300, $newwidthsign, $newheightsign);

	imagecopyresampled($out, $qr, 500, 1535, 0, 0, 150, 150, $qrwidth, $qrheight);
	// imagecopyresampled($out, $logo, 400, 55, 0, 0, 105, 65, $logowidth, $logoheight);
	return imagejpeg($out, $output, 100);
}

function generatePreviewCertificate($id){
	// font directory
	$drFont = BASEPATH."../assets/certificate/font/Metropolis-Medium.otf";
	$drFont1 = BASEPATH."../assets/certificate/font/Metropolis-Bold.otf";
	$image = BASEPATH."../assets/certificate/master/cert-solmit.png";

	$CI = &get_instance();
	$CI->load->model('users/users_mycourse_model','model');

	$dc = $CI->model->getPreviewCertificate($id)->row();

	$createimage = imagecreatefrompng($image);

	//this is going to be created once the generate button is clicked
	$output = BASEPATH."../assets/certificate/certificatepreview.png";

	//then we make use of the imagecolorallocate inbuilt php function which i used to set color to the text we are displaying on the image in RGB format
	$white = imagecolorallocate($createimage, 255, 255, 255);
	$black = imagecolorallocate($createimage, 0, 0, 0);

	// function to display name on certificate picture
	$nameParticipant = imagettftext($createimage, 40, 0, 80, 400, $black,$drFont1, $dc->nama_depan .' '. $dc->nama_belakang);

	$underParticipant = imagettftext($createimage, 14, 0, 80, 450, $black,$drFont1, 'as a participant');

	$typeWebinar = imagettftext($createimage, 14, 0, 80, 535, $white, $drFont1, $dc->certificate_category);

	$date = strtotime($dc->pelaksanaan);
	$date_cert =  date('F d, Y', $date);

	$timeWebinar = imagettftext($createimage, 13, 0, 905, 555, $white, $drFont, $date_cert);

	if(strlen($dc->judul) <= 40){
		$font_size = 70;
		$x_judul = 820;
	} elseif(strlen($dc->judul) <= 50){
		$x_judul   = 780;
		$font_size = 60;
	} elseif(strlen($dc->judul) <= 60){
		$x_judul = 600;
		$font_size = 55;
	} elseif(strlen($dc->judul) <= 70){
		$x_judul = 360;
		$font_size = 55;
	} elseif(strlen($dc->judul) <= 80){
		$font_size =50;
		$x_judul = 300;
	} else {
		$x_judul = 820;
		$font_size =50;
	}

	$nameWebinar = imagettftext($createimage, $font_size, 0, $x_judul, 1290, $black, $drFont, $dc->judul);

	$typeWebinar = imagettftext($createimage, 14, 0, 80, 675, $white, $drFont1, 'Presented By');
	$presentedName = imagettftext($createimage, 14, 0, 80, 700, $white, $drFont1, $dc->presented_name1);
	$titleName = imagettftext($createimage, 12, 0, 80, 720, $white, $drFont, $dc->presented_title1);

	$presentedName = imagettftext($createimage, 14, 0, 80, 740, $white, $drFont1, $dc->presented_name2);

	$presentedName = imagettftext($createimage, 10, 0, 900, 735, $white, $drFont1, wordwrap($dc->signature_name, 40, "\n"));

	$titleName = imagettftext($createimage, 12, 0, 80, 760, $white, $drFont, $dc->presented_title2);

	$certNumber = imagettftext($createimage, 12, 0, 250, 815, $white, $drFont, $dc->certificate_number);

	$titleSign = imagettftext($createimage, 12, 0, 1010, 805, $white, $drFont, $dc->signature_title);

	return imagepng($createimage,$output,3);
}

function encrypt_string($string)
{

	$ciphering = "AES-128-CTR";

	$iv_length = openssl_cipher_iv_length($ciphering);
	$options = 0;

	$encryption_iv = '1234567891011121';

	$encryption_key = "BimagoAcademy";

	$encryption = openssl_encrypt($string, $ciphering, $encryption_key, $options, $encryption_iv);

	// $encryption	= str_replace("+","",$encryption);
	// return str_replace("=","",$encryption);
	$fix = base64_encode($encryption);
	return $fix;
}

function decrypt_string($encryption)
{
	$ciphering = "AES-128-CTR";

	$options = 0;

	$decryption_iv = '1234567891011121';

	$decryption_key = "BimagoAcademy";

	$fix = base64_decode($encryption);
	$decryption=openssl_decrypt ($fix, $ciphering,
			$decryption_key, $options, $decryption_iv);

	return $decryption;
}
function encode_url($string, $key="", $url_safe=TRUE)
{
 	$CI =& get_instance();
    $ret = $CI->encryption->encrypt($string);

    if ($url_safe)
    {
        $ret = strtr(
                $ret,
                array(
                    '+' => '.',
                    '=' => '-',
                    '/' => '~'
                )
            );
    }

    return $ret;
}
function decode_url($string, $key="")
{
    $CI =& get_instance();
    $string = strtr(
            $string,
            array(
                '.' => '+',
                '-' => '=',
                '~' => '/'
            )
        );

    return $CI->encryption->decrypt($string);
}

function encode_url_share($id)
{
	$CI =& get_instance();
	 $name = $CI->master_model->data('judul', 'ls_m_course', ['id' =>$id])->get()->row();
	 $string = str_replace(" ", "-", $name->judul);
	 $string = str_replace(" ", "-", $name->judul);
	 return $string;
	}
	
function get_link_youtube_home_page()
{
	$CI =& get_instance();
	$video = $CI->master_model->data('link_youtube', 'ls_m_lms_config', ['id' => 1])->get()->row();
	return $video->link_youtube;;
}


function encode_url_share_id($id)
{
 	$CI =& get_instance();
    $name = $CI->master_model->data('id', 'ls_m_course', ['id' =>$id])->get()->row();
		$string = str_replace(" ", "-", $name->id);
    return $string;
}

function decode_url_share($id)
{
 	$CI =& get_instance();
	$string = str_replace("-", " ", $id);
	$name = $CI->master_model->data('id', 'ls_m_course', ['judul' => $string])->get()->row();
	if(empty($name)){
		redirect(base_url().'users/users_course?id='.$string);
	}else{
		return $name->id;
	}
}

function decode_url_share_id($id)
{
 	$CI =& get_instance();
	$string = str_replace("-", " ", $id);
	$name = $CI->master_model->data('id', 'ls_m_course', ['id' => $string])->get()->row();
	if(empty($name)){
		redirect(base_url().'users/users_course?id='.$string);
	}else{
		return $name->id;
	}
}

function label_course($array = '')
{
	// dd($array);
    $CI =& get_instance();
		if (is_array($array)) {
			$final=[];
			foreach ($array as $key => $value) {
				$value['have_free'] = FALSE;
				if (is_array($value['type_course'])) {
					$type = $value['type_course'];
					foreach ($type as $key2 => $value2) {
						$check = $CI->master_model->check_data(['id_course' => $value['id'], 'id_type_course' => $value2['id_type_course'], 'harga' => 0], 'ls_m_harga_course');
						if ($check) {
							$value['have_free'] = TRUE;
						}
					}
				}else{
					$type = json_decode($value['type_course']);
					foreach ($type as $key2 => $value2) {
						$check = $CI->master_model->check_data(['id_course' => $value['id'], 'id_type_course' => $value2, 'harga' => 0], 'ls_m_harga_course');
						if ($check) {
							$value['have_free'] = TRUE;
						}
					}
				}
				$final[] = $value;
			}
		}else{
			$final = [];
		}
    return $final;
}

function send_email_god($type, $data, $email) {
	$CI =& get_instance();
	$message = template_email_god($type, $data);
	if ($type == 'led') {
		$subject = 'Instructur Led Class -'.env('APP_NAME');
	}elseif ($type == 'training') {
		$subject = 'Schedule List Training Online -'.env('APP_NAME');
	}elseif ($type == 'webinar') {
		$subject = 'Webinar -'.env('APP_NAME');
	} elseif ($type == 'payment') {
		$subject = 'Payment Success -'.env('APP_NAME');
	}
	$CI->load->library('email', $CI->config->item('email_config'));
	$CI->email->from('info@solmit.academy', env('APP_NAME'));
	$CI->email->to($email);
	$CI->email->subject($subject);
	$CI->email->message($message);
	if ($CI->email->send())
	{
		return true;
	} else
	{
		dd($CI->email->print_debugger());
	}
}

function template_email_god($type, $dataset){
	$CI =& get_instance();
		$data = [
			'name'  => $dataset['nama_depan'],
			'data'  => $dataset,
			'base'  => base_url(),
			'type'  => $type
		];
	return $CI->load->view('admin/pembayaran/template_email',$data,true);
}

function get_max($select = '', $table = '', $cond = '', $db = 'default'){
	$CI =& get_instance();
	$db = $CI->load->database($db, TRUE);
	$db->select_max($select);
	$db->where($cond);
	$result = $db->get($table)->row_array();
	return $result['order'];
}

function get_min($select = '', $table = '', $cond = '', $db = 'default'){
	$CI =& get_instance();
	$db = $CI->load->database($db, TRUE);
	$db->select_min($select);
	$db->where($cond);
	$result = $db->get($table)->row_array();
	return $result['order'];
}

function makeInitials($string){
	$acronym = $word = '';
    $words = preg_split("/(\s|\-|\.)/", $string);
    foreach($words as $w) {
        $acronym .= substr($w,0,1);
    }
    $word = $word . $acronym ;
    return($word);
}
function makeTypes($string){
	$word = '';
	$arr = json_decode($string);
	$count = count($arr) - 1;
	foreach ($arr as $key => $value) {
    	if ($value != 0) {
	        $word .=  $value . (($count != $key ) ? '|' : '');
    	}
    }
    return($word);
}
function send_email_cert($data, $email) {
	$CI =& get_instance();
	$message = template_email_cert($type, $data);
	$subject = 'Congratulations, Certificate Already Exist';
	$CI->email->from('info@solmit.academy', 'Bimago Academy');
	$CI->email->to($email);
	$CI->email->subject($subject);
	$CI->email->message($message);
	if ($CI->email->send())
	{
		return true;
	} else
	{
		dd($CI->email->print_debugger());
	}
}

function template_email_cert($type, $dataset){
	$CI =& get_instance();
		$data = [
			'data'  => $dataset,
			'base'  => base_url(),
			'type'  => $type
		];
	return $CI->load->view('admin/pembayaran/template_email',$data,true);
}
function generateQRcode($str){
	$CI =& get_instance();
	$result = Builder::create()
    ->writer(new PngWriter())
    ->writerOptions([])
    ->data($str)
    ->encoding(new Encoding('UTF-8'))
    ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
    ->size(300)
    ->margin(10)
    ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
    // ->logoPath(FCPATH.'/solmit.png')
    ->labelText('')
    ->labelFont(new NotoSans(20))
    ->labelAlignment(new LabelAlignmentCenter())
    ->build();

    $result->saveToFile(FCPATH.'assets/certificate/qrcode/qr-'.$CI->session->userdata('id').'.png');
}

if (!function_exists('sendNotification')) {
	/***
	 * Params string url
	 * Params string to_user_id OR to_role_id
	 * Params string title
	 * Params string message
	 * Params string module
	 */
	function sendNotification($data)
	{
		try {
			$CI =& get_instance();

			$storeData = [
				'user_id' => isset($data['user_id']) ? $data['user_id'] : $CI->session->userdata('id'),
				'to_user_id' => isset($data['to_user_id']) ? $data['to_user_id'] : null,
				'to_role_id' => isset($data['to_role_id']) ? $data['to_role_id'] : null,
				'url' => isset($data['url']) ? $data['url'] : null,
				'title' => isset($data['title']) ? $data['title'] : 'Notification',
				'message' => isset($data['message']) ? $data['message'] : null,
				'module' => isset($data['module']) ? $data['module'] : 'system',
				'st_viewed' => 0,
				'created_at' => date('Y-m-d H:i:s'),
			];
			$CI->db->insert('ls_t_notification', $storeData);
		} catch (Exception $e) {}
	}
}

if (!function_exists('readNotification')) {
	function readNotification($notif_id)
	{
		try {
			$CI =& get_instance();
			$CI->db->update('ls_t_notification', ['st_viewed' => 1], array('id' => $notif_id));

			$data_notif = $CI->master_model->data('*', 'ls_t_notification', ['id' => $notif_id])->get()->row();
			redirect($data_notif->url);
		} catch (Exception $e) {}
	}
}

if (!function_exists('countUnreadNotification')) {
	function countUnreadNotification()
	{
		$CI =& get_instance();
		$countUnreadNotif = $this->master_model->data('COUNT(*) as jml_notif', 'ls_t_notification', ['to_role_id' => $this->session->userdata('role_id'), 'st_viewed' => 0])->get()->row()->jml_notif;

		return $countUnreadNotif;
	}
}

if (!function_exists('getUnreadNotification')) {
	function getUnreadNotification()
	{
		$CI =& get_instance();
		$getUnreadNotif = $CI->db->query("SELECT * FROM ls_t_notification WHERE st_viewed = 0 AND (to_role_id = '" . $CI->session->userdata('role_id') . "' OR to_user_id = '"  . $CI->session->userdata('id') . "') ORDER BY created_at DESC")->result();

		return $getUnreadNotif;
	}
}

function checkJWTValidation() {
	$CI =& get_instance();
	$jwt	= $CI->session->userdata('jwt');
	if (empty($jwt)){
		redirect('login');
	}
	$privateKey = new \MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
	$publicKey  = new \MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

	$signer     = new \MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer($privateKey);
	$verifier   = new \MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier($publicKey);

	// Parse the token
	$parser = new Parser($verifier);
	$claims = $parser->parse($jwt);

	return $claims;
}

if (!function_exists('checkAuthJWT')) {
	function checkAuthJWT()
	{
		$CI =& get_instance();

		// Check JWT Validation

		$check_jwt = checkJWTValidation();
		if ($check_jwt != NULL )
		{
			if ($check_jwt['id_role'] != 1 && $CI->session->userdata('role_id') != '1')
			{
				$data['base_url'] = base_url();
				redirect('handling/not-authorized');
			}
		} else
			redirect('login');
			

		// if ($CI->session->userdata('id')) {
		// 	// Check role is admin
		// 	if ($CI->session->userdata('role_id') != '1') {
		// 		$data['base_url'] = base_url();
		// 		redirect('handling/not-authorized');
		// 	}
		// } else {
		// 	redirect('login');
		// }
	}
}

if (!function_exists('checkApiKey')) {
	function checkApiKey($api_key)
	{
		if ($api_key != env("APP_API_KEY"))
		{
			return false;
		} else
		{
			return true;
		}
	}
}
if (!function_exists('checkAuthJWTLec')) {
	function checkAuthJWTLec()
	{
		$CI =& get_instance();

		// Check JWT Validation
		$check_jwt = checkJWTValidation();
		if ($check_jwt != NULL )
		{
			if ($check_jwt['id_role'] != 2 && $CI->session->userdata('role_id') != '2')
			{
				$data['base_url'] = base_url();
				redirect('handling/not-authorized');
			}
		} else
			redirect('login');
	}
}
if (!function_exists('checkAuthJWTStudent')) {
	function checkAuthJWTStudent()
	{
		$CI =& get_instance();

		// Check JWT Validation
		$check_jwt = checkJWTValidation();
		if ($check_jwt != NULL )
		{
			if ($check_jwt['id_role'] != 3 && $CI->session->userdata('role_id') != '3')
			{
				$data['base_url'] = base_url();
				redirect('handling/not-authorized');
			}
		} else
			redirect('login');
	}
}
if (!function_exists('uploadVideo')) {
	function uploadVideo($nama, $path)
		{
			$CI = &get_instance();
			$config['upload_path'] = $path;
			$config['allowed_types'] = 'mov|mp4';
			$config['max_size'] = 1024 * 450;
			$config['encrypt_name'] = false;

	    $CI->load->library('upload', $config);
			$CI->upload->initialize($config);
	    if (!$CI->upload->do_upload($nama)) {
				$response['pesan'] = FALSE;
				$response['nama_file'] = '';
	    }else{
					$response['pesan'] = TRUE;
					$response['nama_file'] = $CI->upload->data('file_name');
					$response['nama_loc'] = $path.$CI->upload->data('file_name');
					$response['ext'] = $CI->upload->data('file_ext');
			}
			return $response;
		}
}

if (!function_exists('getMainLogo')) {
	function getMainLogo()
		{
			$CI	= &get_instance();
			$CI->load->model('master_model');
			$main_logo = $CI->master_model->data('logo_utama', 'ls_m_lms_config')->get()->row()->logo_utama;
			$path_logo = base_url().'uploads/lms_config/'.$main_logo;
			return $path_logo;
		}
}

if (!function_exists('getIco')) {
	function getIco()
		{
			$CI	= &get_instance();
			$CI->load->model('master_model');
			$main_logo = $CI->master_model->data('icon', 'ls_m_lms_config')->get()->row()->icon;
			$path_logo = base_url().'uploads/lms_config/'.$main_logo;
			return $path_logo;
		}
}

if (!function_exists('linkUserManual')) {
	function linkUserManual()
		{
			$CI	= &get_instance();
			$CI->load->model('master_model');
			$main_logo = $CI->master_model->data('icon', 'ls_m_lms_config')->get()->row()->icon;
			$path_logo = base_url().'uploads/lms_config/'.$main_logo;
			return $path_logo;
		}
}