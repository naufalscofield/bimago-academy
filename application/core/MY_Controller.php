<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $class, $method, $is_print, $db, $ndb;
    var $template_data = array();
    public function __construct()
    {
        parent::__construct();
        ini_set('max_execution_time', 36000000000000);
        ini_set('memory_limit', '-1');
        $this->ndb    = 'default';
        $this->method = $this->router->fetch_method();
        $this->class  = $this->router->fetch_class();

        $this->db = $this->load->database('default', true);
        $this->load->model('master_model');
        $this->load->library(['user_agent']);
        $this->counter();
    }
    public function counter(){
        $ip    = $this->input->ip_address(); // Mendapatkan IP user
        $date  = date("Y-m-d"); // Mendapatkan tanggal sekarang
        $waktu = time(); //
        $timeinsert = date("Y-m-d H:i:s");
        if ($this->agent->is_browser()) {
                $agent = $this->agent->browser().' '.$this->agent->version();
        } elseif ($this->agent->is_robot()) {
                $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
                $agent = $this->agent->mobile();
        } else {
                $agent = 'Unidentified User Agent';
        }


        $visitor = $this->master_model->data("*","ls_t_visitor",['ip'=> $ip, 'date'=>$date])->get()->num_rows();

        if(empty($visitor)){
          $this->master_model->save(['browser'=>$agent,'ip'=>$ip,'date'=>$date,'online'=>$waktu,'hits'=>1,'time'=>$timeinsert],'ls_t_visitor');
        }else{
          $this->db->where(['ip'=>$ip,'date'=>$date]);
          $this->db->set('hits', '`hits`+ 1', FALSE);
          $this->db->set('online', $waktu);
          $this->db->set('browser', $agent);
          $this->db->update('ls_t_visitor');
        }
    }

    public function load_template($template = '', $view = '', $view_data = array(), $menu = '', $sub_menu = '')
    {
      $this->template_data['info'] = $this->master_model->data('*', 'ls_m_lms_config',['id' => 1])->get()->row();
      !empty($view_data) ? $this->set('content', $this->load->view($view, $view_data, TRUE)) : $this->set('content', $this->load->view($view, '', TRUE));
      $header = $this->master_model->data('*', 'm_menu', ['id_parent' => NULL])->get()->result();
      $this->template_data['list_menu'] = $this->child($header, 'admin');
      $this->template_data['menu'] = $menu;
      $this->template_data['sub_menu'] = $sub_menu;
      $this->template_data['controller'] = $this;
      $this->template_data['dashboard'] = base_url().'admin';
      $this->template_data['nama'] = $this->master_model->data('nama_depan, nama_belakang, avatar, institusi, jk', 'ls_m_user', ['id' => $this->session->userdata('id')])->get()->row_array();
      $this->template_data['logout'] = base_url().'admin/dashboard/logout';
      $this->set('config_menu', base_url().'admin/menu');
      return $this->load->view($template, $this->template_data);
    }

    public function load_template_lecturer($template = '', $view = '', $view_data = array(), $menu = '', $sub_menu = '')
    {
      $this->template_data['info'] = $this->master_model->data('*', 'ls_m_lms_config',['id' => 1])->get()->row();
      !empty($view_data) ? $this->set('content', $this->load->view($view, $view_data, TRUE)) : $this->set('content', $this->load->view($view, '', TRUE));
      $header = $this->master_model->data('*', 'm_menu', ['id_parent' => NULL, 'lecturer' => 2])->get()->result();
      $this->template_data['list_menu'] = $this->child($header, 'lecturer');
      $this->template_data['menu'] = $menu;
      $this->template_data['sub_menu'] = $sub_menu;
      $this->template_data['controller'] = $this;
      $this->template_data['profil'] = base_url().'lecturer/account';
      $this->template_data['dashboard'] = base_url().'lecturer';
      $this->template_data['nama'] = $this->master_model->data('nama_depan, nama_belakang, avatar, institusi, jk', 'ls_m_user', ['id' => $this->session->userdata('id')])->get()->row_array();
      $this->template_data['logout'] = base_url().'lecturer/lecturer_account/logout';
      return $this->load->view($template, $this->template_data);
    }

    public function load_template_users($template = '', $view = '', $view_data = array(), $menu = '')
    {
      $this->template_data['info'] = $this->master_model->data('*', 'ls_m_lms_config',['id' => 1])->get()->row();
      !empty($view_data) ? $this->set('content', $this->load->view($view, $view_data, TRUE)) : $this->set('content', $this->load->view($view, '', TRUE));
      $this->template_data['kategori'] = $this->master_model->data('*', 'ls_m_modul')->get()->result();
      $this->template_data['user'] = $this->master_model->data('*', 'ls_m_user', ['id'=> $this->session->userdata('id')])->get()->row();
      $this->template_data['menu'] = $menu;
      $this->template_data['home'] = base_url();
      $this->template_data['search'] = base_url().'users/users_course';
      $this->template_data['checkout'] = base_url().'users/users_checkout';
      $this->template_data['invoice'] = base_url().'users/users_invoice';
      $this->template_data['wishlist'] = base_url().'users/users_mycourse/index/wishlist';
      $this->template_data['login_page'] = base_url().'users/users_login';
      $this->template_data['register_page'] = base_url().'users/users_login/registrasi';
      $this->template_data['contact'] = base_url().'users/users_contact';
      $this->template_data['profile_web'] = base_url().'users/users_profile_web';
      $this->template_data['mycourse'] = base_url().'users/users_mycourse';
      $this->template_data['logout'] = base_url().'welcome/logout';
      $this->template_data['controller'] = $this;
      $this->template_data['show_cart'] = base_url().'users/users_cart/get_modal';
      $this->template_data['count_cart'] = base_url().'users/users_cart/count_cart';
      $this->template_data['cart_page'] = base_url().'users/users_cart';
      $this->template_data['my_account'] = base_url().'users/myaccount';
      $this->template_data['free_webinar'] = base_url().'users/users_course/free_webinar';
      $this->template_data['paid_webinar'] = base_url().'users/users_course/paid_webinar';
      return $this->load->view($template, $this->template_data);
    }

    function set($name, $value)
		{
			$this->template_data[$name] = $value;
		}

    public function child($header, $type){
      $return = [];
      if (!empty($header)) {
        foreach ($header as $key => $value) {
          if ($type == 'admin') {
            $child = $this->master_model->data('*', 'm_menu', ['id_parent' => $value->id, 'admin' => 2])->get()->result();
          }else{
            $child = $this->master_model->data('*', 'm_menu', ['id_parent' => $value->id, 'lecturer' => 2])->get()->result();
          }
          $value->child = $child;
          $return[]=$value;
          if ($this->grand_child($child)) {
            $this->child($child,$type);
          }
        }
      }
      return $return;
    }

    public function child_users($id){
      $li = '';
      if (!empty($id)) {
        $child = $this->master_model->data('*', 'm_menu_users', ['id_parent' => $id])->get()->result();
        foreach ($child as $key => $value) {
          $li .= '<li><a href="'.$value->url.'">'.$value->nama_menu.'</a></li>';
        }
      }
      return $li;
    }

    public function grand_child($child){
      $return = FALSE;
      if (!empty($child)) {
        if (count($child) > 0) {
          $return = TRUE;
        }else{
          $return = FALSE;
        }
      }
      return $return;
    }
    public function count_course_visitor($id_course = 11){
      $this->db->set('counter', 'counter+1', FALSE);
      $this->db->where(array('id' => $id_course));
      return $this->db->update('ls_m_course');
    }

    public function changelang(){
        $lang = $this->input->post('lang');
        $this->session->set_userdata('site_lang', $lang);
        redirect('','refresh');

    }
    public function pagenotfound(){
        $this->load->view('utilities/404-certificate');
    }
}
