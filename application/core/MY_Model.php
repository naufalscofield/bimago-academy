<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Model extends CI_Model
{
	public $db;

	function __construct()
	{
		parent::__construct();
		ini_set('max_execution_time', '-1');
		ini_set('memory_limit', '-1');
		$this->db = $this->load->database('default', true);
	}

	function data($select = '*', $tabel = '', $and_where = array(), $or_where = array(), $and_where_in = array(), $or_where_in = array(), $having = array(), $or_having = array(), $limit = 0, $offset = 0, $order = '', $like = '', $field_like = '', $usedb = 'default')
	{
		$usedb = $this->load->database($usedb, true);

		if ($select != null && $select != '' && $select != '*') {
			$usedb->select($select);
		}

		$usedb->from($tabel);

		if (is_array($and_where)) {
			$usedb->where($and_where);
		}

		if (is_array($or_where)) {
			$usedb->or_where($or_where);
		}

		if (is_array($and_where_in)) {
			if (count($and_where_in) == 2) {
				if (isset($and_where_in[0]) and isset($and_where_in[1])) {
					if (is_string($and_where_in[0]) and is_array($and_where_in[1])) {
						$usedb->where_in($and_where_in[0], $and_where_in[1]);
					}
				}
			}
		}

		if (is_array($or_where_in)) {
			if (count($or_where_in) == 2) {
				if (isset($or_where_in[0]) and isset($or_where_in[1])) {
					if (is_string($or_where_in[0]) and is_array($or_where_in[1])) {
						$usedb->or_where_in($or_where_in[0], $or_where_in[1]);
					}
				}
			}
		}

		if (is_array($having)) {
			$usedb->having($having);
		}

		if (is_array($or_having)) {
			$usedb->or_having($or_having);
		}

		if ($like) {
			if (is_array($field_like)) {
				$usedb->group_start();
				$i = 0;
				foreach ($field_like as $key => $value) {
					if (is_numeric($like)) {
						if ($i == 0) {
							$usedb->like("CAST(" . $value . " as TEXT)", $like);
						} else {
							$usedb->or_like("CAST(" . $value . " as TEXT)", $like);
						}
					} else {
						if ($i == 0) {
							$usedb->like("LOWER(CAST(" . $value . " as TEXT))", strtolower($like));
						} else {
							$usedb->or_like("LOWER(CAST(" . $value . " as TEXT))", strtolower($like));
						}
					}
					$i++;
				}
				$usedb->group_end();
			} else {
				if (is_numeric($like)) {
					$usedb->like("CAST(" . $field_like . " as TEXT)", $like);
				} else {
					$usedb->like("LOWER(CAST(" . $field_like . ") as TEXT)", strtolower($like));
				}
			}
		}

		if ($order) {
			$usedb->order_by($order);
		}

		if ($limit > 0) {
			$usedb->limit($limit, $offset);
		}

		return $usedb;
	}

	function save($data = array(), $table = '', $usedb = 'default')
	{
		$usedb 				= $this->load->database($usedb, true);
		return $usedb->insert($table, $data);
	}

	function save_batch($data, $table = '', $usedb = 'default')
	{
		$usedb 				= $this->load->database($usedb, true);
		return $usedb->insert_batch($table, $data);
	}

	function update($set = array(), $where = array(), $tabel = '', $usedb = 'default')
	{
		$usedb 				= $this->load->database($usedb, true);
		$update 			= $usedb->set($set);
		$update 			= $usedb->where($where);
		$update 			= $usedb->update($tabel);
		return $update;
	}

	function delete($where = array(), $tabel = '', $usedb = 'default')
	{
		$usedb 				= $this->load->database($usedb, true);
		$hasil 				= FALSE;
		if (!empty($where) and $tabel != NULL) {
			$hasil 			= $usedb->where($where);
			$hasil 			= $usedb->delete($tabel);
		}
		return $hasil;
	}

	function reset($tabel = '', $usedb = 'default')
	{
		$usedb 				= $this->load->database($usedb, true);
		return $usedb->truncate($tabel);
	}

	public function coltoarray($table = '', $column_key = '', $column_value = '', $cond = '')
	{

		if ($cond) {
			$this->db->where($cond);
		}
		$q = $this->db->get($table);
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $row) {
				if ($column_key) {
					$data[$row[$column_key]] = $row[$column_value];
				} else {
					$data[] = $row[$column_value];
				}
			}
			return $data;
		}
		return array();
	}

	function data_list($key = '', $limit = 0, $offset = 0, $table = '', $select = '', $order = '', $like = '', $field_like = '', $usedb = 'default', $distinct = '')
	{
		$usedb = $this->load->database($usedb, true);

		if ($distinct)
			$usedb->distinct($distinct);
		if ($select != null && $select != '' && $select != '*') {
			$usedb->select($select);
		}


		//$usedb->select($select);
		$usedb->from($table);

		if (is_array($key)) {
			$usedb->where($key);
		}

		if ($like) {
			if (is_array($field_like)) {
				// $usedb->group_start();
				$i = 0;
				foreach ($field_like as $key => $value) {
					if ($i == 0) {
						$usedb->like($value, $like);
					} else {
						$usedb->or_like($value, $like);
					}
					$i++;
				}
				// $usedb->group_end();
			} else {
				$usedb->like($field_like, $like);
			}
		}

		if ($order) {
			$usedb->order_by($order);
		}

		if ($limit > 0) {
			$usedb->limit($limit, $offset);
		}

		return $usedb;
	}

	function selectFiles($id, $modul)
	{
		$this->db->from('m_files');
		$this->db->where('trans_id', $id);
		$this->db->where('url', $modul);

		$data = $this->db->get();
		return $data;
	}

	// public function find($table = '', $cond)
	// {
	// 	$q = $this->db->get_where($table, $cond);
	// 	return $q;
	// }

	function check_data($conditions = array(), $table = '')
	{
		$this->db->where($conditions);
		$this->db->limit(1);
		if ($table == '') {
			$table = $this->_table1;
		}
		$q     = $this->db->get($table);
		if ($q->num_rows() > 0) {
			return TRUE;
		}
		return FALSE;
	}

	// public function findfirst($table = '', $cond)
	// {
	// 	$q = $this->find($table, $cond);
	// 	if ($q->num_rows() > 0) {
	// 		$q = $q->result();
	// 		return $q[0];
	// 	}
	// 	return array();
	// }

	function selectFilesByclass($id, $class)
	{
		$this->db->from('m_files');
		$this->db->where('trans_id', $id);
		$this->db->where('modul', $class);

		$data = $this->db->get();
		return $data;
	}

	public function owned($id_course = '', $id_user = '')
	{
		return $this->db->query(
			"SELECT
			d.id,
			d.type_course
		FROM
			ls_t_detail_pembelian a
			LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran
			LEFT JOIN ls_m_type_course d ON d.id = a.id_type_course
		WHERE
			a.id_user = ".$id_user." and a.id_course = ".$id_course." and (
			b.status is null OR b.status IN (1) )"
		)->result_array();
	}

	public function owned_exam($id_user)
	{
		return $this->db->query(
			"SELECT
			b.status,
			c.judul,
			c.id,
			d.id as id_exam,
			d.jumlah_kesempatan,
			-- e.status as status_lulus,
			d.aksi_waktu_habis,
			d.passing_grade,
			d.waktu,
			-- g.id as id_type_m,
			g.type_course,
			(select count(*) from ls_t_room_exam f where f.id_exam = d.id and f.id_user = $id_user) as is_running,
			(select start_minute from ls_t_room_exam f where f.id_exam = d.id and f.id_user = $id_user) as time_remaining,
			(select count(*) from ls_t_after_exam h where h.id_exam = d.id and h.id_user = $id_user) as percobaan,
			(select status from ls_t_after_exam e where e.id_exam = d.id and e.id_user = $id_user order by e.id desc limit 1) as status_lulus_terakhir
		FROM
			ls_t_detail_pembelian a
			LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran
			LEFT JOIN ls_m_course c ON a.id_course = c.id
			LEFT JOIN ls_m_exam d ON a.id_course = d.id_course and a.id_type_course = d.id_type_course
			-- LEFT JOIN ls_t_after_exam e ON e.id_exam = d.id and e.id_user = $id_user ORDER BY e.id desc limit 1
			INNER JOIN ls_m_type_course g ON d.id_type_course = g.id
		WHERE
			d.jumlah_soal_dipakai > 0 AND
			a.id_user = ".$id_user." AND
			(b.status is null OR b.status IN (1) ) AND
			d.tipe = 'exam'"
		)->result_array();
	}

	public function owned_exam_detail($id_user, $id_course, $id_type_course)
	{
		return $this->db->query(
			"SELECT
			b.status,
			c.judul,
			c.id,
			d.id as id_exam,
			d.jumlah_kesempatan,
			-- e.status as status_lulus,
			d.aksi_waktu_habis,
			d.passing_grade,
			d.waktu,
			-- g.id as id_type_m,
			g.type_course,
			(select count(*) from ls_t_room_exam f where f.id_exam = d.id and f.id_user = $id_user) as is_running,
			(select start_minute from ls_t_room_exam f where f.id_exam = d.id and f.id_user = $id_user) as time_remaining,
			(select count(*) from ls_t_after_exam h where h.id_exam = d.id and h.id_user = $id_user) as percobaan,
			(select status from ls_t_after_exam e where e.id_exam = d.id and e.id_user = $id_user order by e.id desc limit 1) as status_lulus_terakhir
		FROM
			ls_t_detail_pembelian a
			LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran
			LEFT JOIN ls_m_course c ON a.id_course = c.id
			LEFT JOIN ls_m_exam d ON a.id_course = d.id_course and a.id_type_course = d.id_type_course
			-- LEFT JOIN ls_t_after_exam e ON e.id_exam = d.id and e.id_user = $id_user ORDER BY e.id desc limit 1
			INNER JOIN ls_m_type_course g ON d.id_type_course = g.id
		WHERE
			d.id_course = ".$id_course." AND
			d.id_type_course = ".$id_type_course." AND
			d.jumlah_soal_dipakai > 0 AND
			a.id_user = ".$id_user." AND
			(b.status is null OR b.status IN (1) ) AND
			d.tipe = 'exam'"
		);
	}

	public function owned_quiz($id_user, $id_episode)
	{
		// dd($id_user.' '.$id_episode);
		return $this->db->query(
			"SELECT
			c.judul,
			c.id,
			d.id as id_exam,
			d.jumlah_kesempatan,
			d.aksi_waktu_habis,
			d.passing_grade,
			d.waktu,
			d.id_episode,
			g.type_course,
			h.judul as judul_episode,
			(select count(*) from ls_t_room_exam f where f.id_exam = d.id and f.id_user = $id_user) as is_running,
			(select start_minute from ls_t_room_exam f where f.id_exam = d.id and f.id_user = $id_user) as time_remaining,
			(select count(*) from ls_t_after_exam h where h.id_exam = d.id and h.id_user = $id_user) as percobaan,
			(select status from ls_t_after_exam e where e.id_exam = d.id and e.id_user = $id_user order by e.id desc limit 1) as status_lulus_terakhir
		FROM
			ls_t_detail_pembelian a
			LEFT JOIN ls_m_course c ON a.id_course = c.id
			LEFT JOIN ls_m_exam d ON a.id_course = d.id_course and a.id_type_course = d.id_type_course
			INNER JOIN ls_m_type_course g ON d.id_type_course = g.id
			INNER JOIN ls_m_episode h ON d.id_episode = h.id
		WHERE
			a.id_user = ".$id_user." AND d.tipe = 'quiz' and d.id_episode = ".$id_episode.""
		)->result_array();
	}

	public function owned_id($id_user)
	{
		return $this->db->query(
			"SELECT
			c.id
		FROM
			ls_t_detail_pembelian a
			LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran
			LEFT JOIN ls_m_course c ON c.id = a.id_course
			LEFT JOIN ls_m_exam d ON d.id_course = a.id_course
		WHERE
			a.id_user = ".$id_user." and (
			b.status is null OR b.status IN (1) )"
		)->result_array();
	}

	public function owned_product($id_user, $id_course)
	{
		return $this->db->query(
			"SELECT
			c.id
		FROM
			ls_t_detail_pembelian a
			LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran
			LEFT JOIN ls_m_course c ON c.id = a.id_course
		WHERE
			a.id_user = ".$id_user." and a.id_course = ".$id_course." and (
			b.status is null OR b.status IN (1) )"
		)->result_array();
	}

	public function db_query($query, $type = '')
	{
		if ($type == 'row') {
			return $this->db->query($query)->row();
		}elseif ($type == 'row_array') {
			return $this->db->query($query)->row_array();
		}elseif ($type == 'result') {
			return $this->db->query($query)->result();
		}elseif ($type == 'result_array') {
			return $this->db->query($query)->result_array();
		}
	}

	public function saveTransCertificate($id_course,$id_user,$id_exam = '')
	{
		// $cn = $this->data('certificate_number','ls_t_certificate', ['id_course' => $id_course])->order_by('id','desc')->get()->row();
		// $cn_m = $this->data('certificate_number','ls_m_certificate', ['id_course' => $id_course])->order_by('id','asc')->get()->row();
		$cn = $this->data('certificate_number','ls_t_certificate')->like('certificate_number', date('Y'))->order_by('id','desc')->get()->row();
		if (isset($cn->certificate_number)) {
			$serial = (int)substr($cn->certificate_number, -4) + 1;
		}else{
			if (date('Y') == 2021) {
				$serial = '261';
			}else{
				$serial = '1';
			}
		}
		// if (isset($cn_m->certificate_number)) {
			$insert = [
				'id_course' 			=> $id_course,
				'id_exam' 				=> $id_exam,
				'id_user' 				=> $id_user,
				'certificate_number' 	=> 'BA-'.date('Y') . '-'.sprintf("%04s", $serial),
				// 'certificate_number' => $cn_m->certificate_number . '-'.sprintf("%04s", $serial),
			];
        	$insert =  $this->master_model->save($insert, 'ls_t_certificate');
		// }
        	if ($insert) {
        		return true;
        	}else{
				return false;
        	}
	}

	function save_cart($data = '')
	{
		$save = $this->save($data, 'ls_t_cart');
		return $save;
	}

	function update_cart($data = '', $id_course = '', $id_user = '')
	{
		$save = $this->update($data, ['id_course' => $id_course, 'id_user' => $id_user], 'ls_t_cart');
		return $save;
	}

	function cart_contents($id_user = '')
	{
		$data = $this->data('*', 'ls_t_cart', ['id_user' => $id_user])->get()->result_array();
		return $data;
	}

	function cart_total($id_user = '')
	{
		$data = $this->data('sum(price) as total', 'ls_t_cart', ['id_user' => $id_user])->get()->row_array();
		return $data['total'];
	}

	function cart_delete_row($id_user = '', $id_course = '')
	{
		$data = $this->delete(['id_user' => $id_user, 'id_course' => $id_course] ,'ls_t_cart');
		return $data;
	}

	function cart_destroy($id_user = '')
	{
		$data = $this->delete(['id_user' => $id_user] ,'ls_t_cart');
		return $data;
	}

	function cart_ready($id_user = '', $id_course = '')
	{
		$get = $this->data('id_type_course', 'ls_t_cart', ['id_user' => $id_user, 'id_course' => $id_course])->get()->result_array();
		$data = [];
		foreach ($get as $key => $value) {
			$type = json_decode($value['id_type_course']);
			if (is_array($type) && count($type) > 0) {
				foreach ($type as $key2 => $value2) {
					$data[]=$value2;
				}
			}
		}
		return $data;
	}

	function cron_show_room_exam()
	{
		date_default_timezone_set("Asia/Jakarta");
		$hour_ago 	= date('Y-m-d H:i:s', time() - 3600);
		return $this->db->query("select * from ls_t_room_exam where start_from < '$hour_ago'")->result_array();
	}

	function cron_room_exam()
	{
		date_default_timezone_set("Asia/Jakarta");
		$hour_ago 	= date('Y-m-d H:i:s', time() - 3600);
		return $this->db->query("delete from ls_t_room_exam where start_from < '$hour_ago'");
	}

	function cron_delete_user_answer($cond)
	{
		$this->db->where($cond);
		return $this->db->delete("ls_t_user_answer");
	}

	function cron_insert_after_exam($data)
	{
		return $this->db->insert('ls_t_after_exam', $data);
	}

	function show_data($table, $cond)
	{
		return $this->db->get_where($table, $cond);
	}

}
