<?php
Class Handling extends CI_Controller {

    public function not_authorized()
    {
        $data['base_url']   = base_url();
        $this->load->view('utilities/exam-forbidden', $data);
    }
}