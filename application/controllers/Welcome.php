<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
  public $view    = 'users/users_home/';
	public function __construct()
	{
		parent::__construct();

    ini_set('allow_url_fopen', 1);
    ini_set('allow_url_include', 1);

    if ($this->session->userdata('id')) {
      if ($this->session->userdata('role') == 'admin') {
        redirect('admin');
      }elseif ($this->session->userdata('role') == 'lecturer') {
        if ($this->session->userdata('status') == 2) {
          redirect('lecturer');
        }else{
          redirect('lecturer/account');
        }
      }
    }
    $this->load->database();
		$this->load->model('users/users_home_model','model');
		$this->load->model('users/users_course_model','model_course');
	}

  public function changelang_dash(){
    $lang = $this->input->post('lang');
    $this->session->set_userdata('site_lang', $lang);
    redirect('','refresh');

}

  public function cron_room_exam()
  {
    $data     = $this->master_model->cron_show_room_exam();
    $delete   = $this->master_model->cron_room_exam();

    foreach ($data as $dt)
    {
      $sup  = [
        'id_user' => $dt['id_user'],
        'id_exam' => $dt['id_exam'],
        'score'   => 0,
        'status'  => 'timeout'
      ];

      $cond  = [
        'id_user' => $dt['id_user'],
        'id_exam' => $dt['id_exam'],
      ];

      $this->master_model->cron_insert_after_exam($sup);
      $this->master_model->cron_delete_user_answer($cond);
    }

  }

	public function index(){

    $quotes = [
      "Terlalu memanjakan anak itu akan menghambat masa depannya, sebab nanti mereka gak bisa mandiri, gak paham agama, gak ngerti Qur'an, gak punya akhlaq, ujung-ujungnya gak bisa jadi amal jariyahmu kelak, kalau kamu telah tiada.",
      "Ketika anak mau masuk pondok apalagi menghafal Qur'an gak usah ditangisi. Itu rezeki, kamu harus
      bersyukur.",
      "Coba bayangkan jika anak-anakmu hidup di luar sekarang, apa iya kamu tega setiap jam 4 pagi
      memaksa mereka untuk bangun Tahajud? Apa iya setiap hari kamu ada waktu menyimak
      setoran hafalan mereka?",
      "Coba kamu lihat dirimu sekarang sudah yakinkah kira-kira dengan sholatmu, puasamu, bisa membuat
      kamu masuk surga-Nya Allah?",
      "Kamu hanya dititipi mereka, nanti kamu akan
      dimintai pertanggung jawaban atas mereka. Kira-kira kalau anakmu lebih bangga kenal artis,
      lebih bangga dengan barang ber-merk, lebih
      seneng menghafal lagu ora genah, gak kenal Gusti Allah, gak kenal kanjeng Nabi, gak bisa baca dan paham Qur'an gak ngerti budi pekerti. Lha kamu mau jawab apa kelak
      dihadapan Gusti Allah?",
      "Jika kamu yakin amalmu bisa menjamin kamu masuk surga yo sak karepmu. Urusen anakmu dengan budaya barat yang sekarang lagi trend
      di luar sana.",
      "Anak-anak kecil wes podo pinter dolanan hape buka situs apa saja bisa, bangga punya ini itu: baju, sepatu, tas ber-merk, lha pas di suruh ngaji
      blekak blekuk. Ditanya tentang agama
      prengas-prengep, arep dadi opo?"
    ];

    // $rand = rand(0,6);
    // $result['quote'] = $quotes[$rand];
    $result['quotes'] = $quotes;

		$top_viewed = convert_rating($this->model->topThree());
    $top_viewed = label_course($top_viewed);
    foreach ($top_viewed as $idx => $tw)
    {
      // print_r($tw['id']);
      $totWish = $this->model->getTotWish($tw['id']);
      $top_viewed[$idx]['tot_wish'] = $totWish->total_wish;
      
      $totCom = $this->model->getTotCom($tw['id']);
      $top_viewed[$idx]['tot_com'] = $totCom->total_com;
    }
		$best_seller = convert_rating($this->model->bestSeller());
		$best_seller = label_course($best_seller);
    $webinar = convert_rating($this->webinar());
    $webinar = label_course($webinar);
		$result['course'] = base_url().'users/users_course';
		$result['top_viewed'] = $top_viewed;
		$result['best_seller'] = $best_seller;
		$result['upcoming_webinar'] = $webinar;
		$result['class'] = 'users_course';
    if ($this->session->userdata('id') != ''){
      $wishlist = $this->wishlisted($this->session->userdata('id'));
      $result['wishlisted'] = $this->convert_array($wishlist['data']);
    }
    $result['wishlist'] = base_url().'users/users_mycourse/add_remove_wishlist';
    $result['login_page'] = base_url().'users/users_login';
    $result['detail'] = base_url().'users/users_course/detail/';
    $result['detail_cart'] = base_url('users/users_cart/add_to_cart_single/');
    $result['add_free'] = base_url('users/users_checkout/add_to_free/');
    $result['best_modul'] = $this->best_modul();
    $result['gallery'] = $this->master_model->data('file', 'ls_m_gallery')->get()->result_array();
		$this->load_template_users('users/template', $this->view.'display', $result, 'home');
	}

  public function best_modul()
  {
    $modul = $this->master_model->data('count(c.modul) as total, b.judul, c.modul, c.gambar, c.id as id_modul', 'ls_t_pembelian a')
    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
    ->join('ls_m_modul c', 'b.id_modul = c.id', 'LEFT')
    ->group_by('c.modul')
    ->order_by('total', 'DESC')
    ->limit(2)
    ->get()->result_array();
    return $modul;
  }

  public function webinar()
  {
    $webinar = $this->master_model->data('e.harga,e.discount,a.type_course, a.judul, a.image, a.pelaksanaan, a.id, b.nama_depan, b.nama_belakang, b.institusi, c.modul, d.score', 'ls_m_course a', ['a.pelaksanaan_webinar >' => date('Y-m-d')])
    ->join('ls_m_user b', 'a.kontributor = b.id', 'LEFT')
    ->join('ls_m_modul c', 'a.id_modul = c.id', 'LEFT')
    ->join('ls_t_convert_rating d', 'a.id = d.id_course', 'LEFT')
    ->join('ls_m_harga_course e', 'a.id = e.id_course', 'LEFT')
    ->like('type_course', '4')
    ->order_by('pelaksanaan_webinar', 'DESC')
    ->get()->result_array();
    return $webinar;
  }

  public function wishlisted($id = '')
	{
		$get = $this->model_course->get_data_wishlisted($id);
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function convert_array($array) {
    if (!is_array($array)) {
      return [];
    }
    $result = array();
    foreach ($array as $key => $value) {
      $result[$key] = $value['id_course'];
    }
    return $result;
  }

  public function zoom_web(){
    $url = 'http://bimago.academy/welcome/zoom_web';
    $data = array('meetingNumber' => '76084454789', 'role' => '0');
    // use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) { /* Handle error */ }

    $resAr = json_decode($result, true);
    $signature = $resAr['signature'];
    echo '
    <div class="iframe-container" style="overflow: hidden; padding-top: 56.25%; position: relative;">
      <iframe allow="microphone; camera" style="border: 0; height: 100%; left: 0; position: absolute; top: 0; width: 100%;" src="https://success.zoom.us/wc/join/76084454789" sandbox="allow-same-origin allow-forms allow-scripts" allowfullscreen="allowfullscreen" frameborder="0"></iframe>
    </div>
    ';
  }

  public function handling_spp()
  {
    $this->load->view('utilities/spp-error');
  }

}
