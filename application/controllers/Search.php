<?php

class Search extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/course_model', 'model');
    }
    
    public function query($string)
    {
        $data   = $this->model->match($string);
        echo json_encode($data);
    }
}