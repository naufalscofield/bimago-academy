<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Example extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
	}
	
	public function youtube() {
		
		$this->load->library('Youtube');
		///echo $this->rahul->my_function();
		//exit;
		$video= "uploads/episode_videos/Jakarta_gak_pernah_tidur4.mp4";
		$title= "Tester Upload Video lagi";
		$desc= "this is a final test youtube video desc";
		$tags=["solmitacademy","beelms"];
		$privacy_status="unlisted";
		$youtube = $this->youtube->youtube_upload($video,$title,$desc,$tags,$privacy_status);
		print_r($youtube);
		//echo "hi";
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
