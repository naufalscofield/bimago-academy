<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;
include APPPATH . "../vendor/autoload.php";

Class Api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('users/users_login_model', 'model');
        $this->load->model('lecturer/lecturer_withdrawal_model', 'withdrawal_model');
        $this->load->model('admin/cut_off_model', 'cut_off_model');
        $this->load->model('admin/course_model', 'course_model');
        $this->load->model('admin/pembayaran_model', 'pembayaran_model');
        $this->load->model('admin/pembelian_model', 'pembelian_model');
        $this->load->library('form_validation');
    }

    public function check_valid_email($email) {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false && $find2 > $find1);
     }

    public function sso()
    {
		date_default_timezone_set("Asia/Jakarta");

        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {
            // $json       = file_get_contents('php://input');
            // $_POST['  = json_decode($json);
            $_POST = json_decode(file_get_contents("php://input"), true);

            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('no_hp', 'No Hp', 'required');
            $this->form_validation->set_rules('nama_depan', 'Nama Depan', 'required');
            $this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'required');
            $this->form_validation->set_rules('api_key', 'API Key', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $response_code = 400;
                $response_msg = [
                    'status_code'   => 400,
                    'msg'           => 'Parameter input tidak lengkap'
                ];
            }
            else
            {
                $email        = $_POST['email'];
                $no_hp        = $_POST['no_hp'];
                $nama_depan   = $_POST['nama_depan'];
                $nama_belakang= $_POST['nama_belakang'];
                $api_key      = $_POST['api_key'];

                if (!is_string($email) || !is_string($no_hp) || !is_string($nama_depan) || !is_string($nama_belakang) || !is_string($api_key))
                {
                    $response_code = 400;
                    $response_msg = [
                        'status_code'   => 400,
                        'msg'           => 'Tipe parameter input tidak sesuai'
                    ];
                } else
                {
                    // if ($this->check_valid_email($email))
                    // {

                        if (strlen($no_hp) >= 10 && strlen($no_hp) <= 13)
                        {
                            $check_mitra    = $this->master_model->data('id, kode, nama_mitra, api_key', 'ls_m_mitra', ['api_key' => $api_key])->get();
                            
                            if ($check_mitra->num_rows() == 0)
                            {
                                $response_code = 400;
                                $response_msg = [
                                    'status_code'   => 400,
                                    'msg'           => 'Mitra tidak terdaftar'
                                ];
                            } else
                            {
                                $data_mitra     = $check_mitra->row();
                                $check_data     = $this->master_model->data('a.id, a.verifikasi, a.login_via, a.status, b.role', 'ls_m_user a', ['email' => $email, 'kode_mitra' => $data_mitra->kode])
                                                ->join('ls_m_role b', 'a.role = b.id', 'LEFT')
                                                ->join('ls_m_mitra c', 'a.kode_mitra = c.kode', 'LEFT')
                                                ->get();
            
                                $token = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($email))))))))));
                                if ($check_data->num_rows() > 0)
                                {
                                    $data   = $check_data->row();
                                    // JWT Part
                                    $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
                                    $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

                                    $signer = new RS256Signer($privateKey);
                                    $verifier = new RS256Verifier($publicKey);

                                    $data_jwt   = [
                                        'id' => (int)$data->id,
                                        'role' => $data->role,
                                        'token' => $token,
                                        'access' => 'YES',
                                        'login_via' => $data_mitra->nama_mitra,
                                        'status' => 2,
                                        'id_role' => 3,
                                    ];

                                    // if(!empty($_POST["stat_spp"]))
                                    // {
                                    //     $data_jwt['stat_spp'] = $_POST["stat_spp"];
                                    // }

                                    $generator  = new Generator($signer);
                                    $jwt        = $generator->generate($data_jwt);
                                    
                                    $response_code = 200;
                                    $response_msg = [
                                        'status_code'   => 200,
                                        'msg'           => 'Login sebagai user terdaftar',
                                        'token'         => $jwt,
                                        'redirect_link' => 'https://bimago.academy/redirect-sso?Authorization='.$jwt
                                    ];
                                } else 
                                {
                                    $dataInsert = [
                                        'email' => $email,
                                        'password'  => NULL,
                                        'no_hp' => $no_hp,
                                        'institusi' => $data_mitra->nama_mitra,
                                        'role'  => 3,
                                        'nama_depan' => $nama_depan,
                                        'nama_belakang' => $nama_belakang,
                                        'verifikasi' => 1,
                                        'email_verification_token' => NULL,
                                        'email_verification_expired'  => NULL,
                                        'jk'    => NULL,
                                        'avatar' => NULL,
                                        'login_via' => $data_mitra->nama_mitra,
                                        'profil'    => NULL,
                                        'nama_sertifikat'   => NULL,
                                        'change_password_token' => NULL,
                                        'status'    => 2,
                                        'kode_mitra'     => $data_mitra->kode
                                    ];
                    
                                    $insert = $this->model->create_data('ls_m_user', $dataInsert);

                                    //Check apakah SSO dari Portal Santri Bimago
                                        // if(!empty($_POST["register"]))
                                        // {
                                        //     $courses    = $this->course_model->get_data('ls_m_course')->result_array();

                                        //     foreach ($courses as $c)
                                        //     {
                                        //         //Insert Tabel Pembayaran
                                        //         $data_pembayaran = [
                                        //             'id_user'       => (int)$insert,
                                        //             'no_pesanan'    => rand(000000000000001, 999999999999999),
                                        //             'total'         => 0,
                                        //             'status'        => 1,
                                        //             'tanggal_pembelian' => date('Y-m-d H:i:s'),
                                        //             'metode_pembayaran' => 'Bimago'
                                        //         ];

                                        //         $insert_pembayaran = $this->pembayaran_model->store_w_iid($data_pembayaran);

                                        //         //Insert Tabel Detail Pembelian
                                        //         $tipe_course_id = $c['type_course'];
                                        //         $tipe_course_id = str_replace("[","",$tipe_course_id);
                                        //         $tipe_course_id = str_replace("]","",$tipe_course_id);
                                        //         $tipe_course_id = str_replace('"',"",$tipe_course_id);

                                        //         $data_detail = [
                                        //             'id_user'       => (int)$insert,
                                        //             'id_course'     => $c['id'],
                                        //             'id_pembayaran' => $insert_pembayaran,
                                        //             'id_type_course'=> $tipe_course_id,
                                        //             'harga'         => 0
                                        //         ];

                                        //         $insert_detail = $this->pembelian_model->store($data_detail,'ls_t_detail_pembelian');

                                        //         //Insert Tabel Pembelian
                                        //         $data_pembelian = [
                                        //             'id_user'       => (int)$insert,
                                        //             'id_course'     => $c['id'],
                                        //         ];

                                        //         $insert_pembelian = $this->pembelian_model->store($data_pembelian,'ls_t_pembelian');
                                        //     }

                                        // }
                                    //

                                    // JWT Part
                                    $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
                                    $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

                                    $signer     = new RS256Signer($privateKey);
                                    $verifier   = new RS256Verifier($publicKey);
                                    
                                    $data_jwt   = [
                                        'id' => (int)$insert,
                                        'role' => 'user',
                                        'id_role' => 3,
                                        'token' => $token,
                                        'access' => 'YES',
                                        'login_via' => $data_mitra->nama_mitra,
                                        'status' => 2,
                                    ];

                                    // if(!empty($_POST["stat_spp"]))
                                    // {
                                    //     $data_jwt['stat_spp'] = $_POST["stat_spp"];
                                    // }

                                    $generator = new Generator($signer);
                                    $jwt        = $generator->generate($data_jwt);
            
                                    $response_code = 200;
                                    $response_msg = [
                                        'status_code'   => 200,
                                        'msg'           => 'Login sebagai user baru',
                                        'token'         => $jwt,
                                        'redirect_link' => 'https://bimago.academy/redirect-sso?Authorization='.$jwt
                                    ];
                                }
                            }
                        } else
                        {
                            $response_code = 400;
                            $response_msg = [
                                'status_code'   => 400,
                                'msg'           => 'No hp tidak valid',
                            ];
                        }
                    // } else 
                    // {
                    //     $response_code = 400;
                    //     $response_msg = [
                    //         'status_code'   => 400,
                    //         'msg'           => 'Email tidak valid',
                    //     ];
                    // }
                }
            }
        } else
        {
            $response_code = 405;
            $response_msg = [
                'status_code'   => 405,
                'msg'           => 'Request method tidak sesuai',
            ];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header($response_code)
            ->set_output(json_encode($response_msg));
    }

    public function redirect_sso()
    {

        $headers = $this->input->get();
        if (isset($headers['Authorization']))
        {
            $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
            $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

            $signer     = new RS256Signer($privateKey);
            $verifier   = new RS256Verifier($publicKey);

            // Parse the token
            $parser = new Parser($verifier);
            $claims = $parser->parse($headers['Authorization']);

            $data_session = array(
                'id' => $claims['id'],
                'role' => $claims['role'],
                'role_id' => '3',
                'token' => $claims['token'],
                'access' => $claims['access'],
                'login_via' => $claims['login_via'],
                'status' => $claims['status'],
                'jwt' => $headers['Authorization'],
                // 'spp'  => $claims['stat_spp']
                );
            $this->session->set_userdata($data_session);
        }

        redirect('/');
    }

    public function callback_payment()
    {
        $id_withdrawal      = $this->input->post('id_withdrawal');
        $file_name          = $this->input->post('file_name');
        $api_key            = $this->input->post('api_key');
        
        $check              = checkApiKey($api_key);

        if ($check)
        {
            $config['upload_path']          = './uploads/withdrawal_receipt';
            $config['allowed_types']        = 'jpeg|jpg|png';
            $config['max_size']             = 1024;
            $config['file_name']            = $file_name;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file'))
            {
                $this->output->set_status_header('500');
                $this->data['message'] = 'Internal server '.env('APP_NAME').' error uploading receipt';
                $this->data['message'] = 'Error while';
            } else
            {
                $update = $this->withdrawal_model->update(['id' => $id_withdrawal], ['status' => 3, 'receipt' => $file_name]);
                
                if (!$update)
                {
                    $this->output->set_status_header('500');
                    $this->data['message'] = 'Internal server '.env('APP_NAME').' error';
                    $this->data['status_code'] = 500;
                } else
                {
                    $this->output->set_status_header('200');
                    $this->data['message'] = 'Update payment status in LMS success';
                    $this->data['status_code'] = 200;
                }
            }

        } else
        {
            $this->output->set_status_header('400');
            $this->data['status_code'] = 400;
            $this->data['message'] = 'Invalid credential key';
        }

        echo json_encode($this->data);
    }

    public function callback_cut_off()
    {
        $api_key            = $this->input->post('api_key');
        
        $check              = checkApiKey($api_key);

        if ($check)
        {
            $data   = [
                'keterangan'    => $this->input->post('keterangan'),
                'kode'          => $this->input->post('kode'),
                'potongan'      => $this->input->post('potongan'),
                'jenis'         => $this->input->post('jenis'),
            ];

            $update = $this->cut_off_model->update($data, ['kode' => $this->input->post('kode')]);
            
            if (!$update)
            {
                $this->output->set_status_header('500');
                $this->data['message'] = 'Internal server '.env('APP_NAME').' error';
                $this->data['status_code'] = 500;
            } else
            {
                $this->output->set_status_header('200');
                $this->data['message'] = 'Update cut off in LMS success';
                $this->data['status_code'] = 200;
            }
        } else
        {
            $this->output->set_status_header('400');
            $this->data['status_code'] = 400;
            $this->data['message'] = 'Invalid credential key';
        }

        echo json_encode($this->data);
    }
}