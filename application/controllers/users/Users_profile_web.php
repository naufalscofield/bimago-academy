<?php

class Users_profile_web extends MY_Controller {
  public $view    = 'users/users_profile_web/';
	public function __construct(){
		parent::__construct();
    // if ($this->session->userdata('id')) {
    //   if ($this->session->userdata('role') == 'admin') {
    //     redirect('admin');
    //   }elseif ($this->session->userdata('role') == 'lecturer') {
    //     redirect('admin');
    //   }
    // }


    $this->load->database();
    $this->load->model("admin/profile_web_model", "model");
	}

	public function index(){
    $data['class']  = 'Users_profile_web';
    $data['data']   = $this->model->get()->row();
    // $result['simpan'] = base_url().'users/users_contact/simpan';
    // $result['data'] = $this->master_model->data('*', 'ls_m_user', ['id' => $this->session->userdata('id')])->get()->row();
		$this->load_template_users('users/template', $this->view.'display', $data);
	}

}
