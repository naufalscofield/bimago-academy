<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Users_cart extends MY_Controller {
  public $view    = 'users/users_cart/';
	public function __construct(){
		parent::__construct();
    // if ($this->session->userdata('id')) {
    //   if ($this->session->userdata('role') == 'admin') {
    //     redirect('admin');
    //   }elseif ($this->session->userdata('role') == 'lecturer') {
    //     redirect('admin');
    //   }
    // }else{
    //   redirect('login');
    // }

    // $signer = new HS256('12345678901234567890123456789012');
    // $parser = new Parser($signer);

    // if ($this->session->userdata('id'))
    // {
    //   if ($this->session->userdata('jwt'))
    //   {
    //     $claims = $parser->parse(($this->session->userdata('jwt')));

    //     if ($claims['role'] != 'user')
    //     {
    //       $data['base_url']           = base_url();
    //       redirect('handling/not-authorized');
    //     }
    //   }
    // } else
    // {
    //     redirect('login');
    // }

    $this->load->database();
		$this->load->model('users/users_cart_model','model');
    $this->load->model('users/users_course_model','modelcourse');
	}

	public function index(){
    $result['class'] = $this->class;
    $result['cart'] = $this->master_model->cart_contents($this->session->userdata('id'));
    $result['cart_total'] = rupiah($this->master_model->cart_total($this->session->userdata('id')));
    // $result['detail'] = base_url('users/'.$this->class.'/detail/');
    $result['delete_row_cart'] = base_url().'users/users_cart/delete_row_cart';
    $result['checkout'] = base_url().'users/users_checkout';
    $result['course'] = base_url().'users/users_course';
		$this->load_template_users('users/template', $this->view.'display', $result);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_m_course');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal()
	{
    $response['cart'] = $this->master_model->cart_contents($this->session->userdata('id'));
    $response['cart_total'] = rupiah($this->master_model->cart_total($this->session->userdata('id')));
    $response['cart_page'] = base_url().'users/users_cart';
    $response['delete_row_cart'] = base_url().'users/users_cart/delete_row_cart';
    $response['checkout'] = base_url().'users/users_checkout';
    return $this->load->view($this->view.'modal_cart', $response);
	}

  public function empty(){
    $result['course'] = base_url().'users/users_course';
    echo $this->load->view($this->view.'empty', $result, TRUE);
  }

  public function add_to_cart(){
    if (!$this->session->userdata('id')) {
      $hasil['proses'] = 'failed';
      $hasil['url'] = base_url().'login';
    }else{
      $course_empty = [];
      // dd($this->input->post('select_type'));
      foreach ($this->input->post('select_type') as $key => $value) {
        if (count($value) == 1) {
          array_push($course_empty, $key);
        }
        $course = $this->get($key);
        $price = 0;
        $type_course = '';
        $i = 0;
        $set_type = '';
        $numItems = count($value);
          foreach ($value as $key2 => $value2) {
            $detail_type = $this->master_model->data('type_course', 'ls_m_type_course', ['id' => $value2])->get()->row_array();
            if(++$i === $numItems) {
              if ($value2 == 0) {
                $set_type .= '"'.$value2.'"'.',';
                $type_course = '';
              }else{
                $set_type .= '"'.$value2.'"';
                $type_course .= '('.$detail_type['type_course'].')';
              }
            }else{
              if ($value2 == 0) {
                $set_type .= '"'.$value2.'"'.',';
                $type_course = '';
              }else{
                $type_course .= '('.$detail_type['type_course'].') - ';
                $set_type .= '"'.$value2.'"'.',';
              }
            }
            $id_type_course = '['.$set_type.']';

            $detail_price = $this->master_model->data('harga, discount', 'ls_m_harga_course', ['id_course' => $key, 'id_type_course' => $value2])->get()->row_array();
            
            if($detail_price['discount'] != NULL || $detail_price['discount'] != 0)
            {
              $discount     = ($detail_price['discount']/100) * $detail_price['harga'];
              $harga_final  = $detail_price['harga'] - $discount;
              if ($harga_final < 0)
              {
                $price        += 0;
              } else
              {
                $price        += $detail_price['harga'] - $discount;
              }
            } else
            {
              $price += $detail_price['harga'];
            }


          }
        $data = array(
          'id_course' => $course['data'][0]['id'],
          'id_user' => $this->session->userdata('id'),
          'id_type_course' => $id_type_course,
          'name' => preg_replace('/[^a-zA-Z0-9_ -]/s','', $course['data'][0]['judul']),
          'price' => $price,
          'type_course' => $type_course,
          'image' => $course['data'][0]['image'],
          'deskripsi_singkat' => $course['data'][0]['deskripsi_singkat'],
          'qty' => $numItems,
        );
        $check_cart = $this->master_model->check_data(['id_course' => $course['data'][0]['id'], 'id_user' => $this->session->userdata('id')], 'ls_t_cart');
        if ($check_cart) {
          if ($numItems > 0) {
            $this->master_model->update_cart($data, $course['data'][0]['id'], $this->session->userdata('id'));
          }else{
            $this->master_model->cart_delete_row($this->session->userdata('id'), $data, $course['data'][0]['id']);
          }
        }else{
          $this->master_model->save_cart($data);
        }
      }
        if (count($course_empty) > 0) {
            $this->db->where(['id_user'=>$this->session->userdata('id')]);
            $this->db->where_in('id_course', $course_empty);
            $this->db->delete('ls_t_cart');
        }

      $hasil['proses'] = 'success';
      $hasil['data'] = $data;
      $hasil['total'] = count($this->master_model->cart_contents($this->session->userdata('id')));
      $hasil['pesan'] = 'Cart updated!';
    }
    echo json_encode($hasil);
  }

  public function add_to_cart_single(){
    if (!$this->session->userdata('id')) {
      $hasil['proses'] = 'failed';
      $hasil['pesan'] = 'Please Login First';
      $hasil['tipe'] = 'warning';
      $hasil['url'] = base_url().'login';
    }else{
      $id_course = $this->input->post('id');
      $owned_course = $this->master_model->owned($id_course, $this->session->userdata('id'));
      if (empty($owned_course)) {        
        $hasil['url'] = base_url().'users/users_checkout';
        $this->modelcourse->id = $id_course;
        $get = $this->modelcourse->get_data('ls_m_course');
        $data = $get->result_array();
        $type_course=[];
        $type = json_decode($data[0]['type_course']);
        foreach ($type as $key2 => $value2) {
          $type_course[$id_course][0] = 0;
          $type_course[$id_course][$value2] = $value2;
        }
        // dd($type_course);
        $course_empty = [];
        // dd($this->input->post('select_type'));
        foreach ($type_course as $key => $value) {
          if (count($value) == 1) {
            array_push($course_empty, $key);
          }
          $course = $this->get($key);
          $price = 0;
          $type_course = '';
          $i = 0;
          $set_type = '';
          $numItems = count($value);
            foreach ($value as $key2 => $value2) {
              $detail_type = $this->master_model->data('type_course', 'ls_m_type_course', ['id' => $value2])->get()->row_array();
              if(++$i === $numItems) {
                if ($value2 == 0) {
                  $set_type .= '"'.$value2.'"'.',';
                  $type_course = '';
                }else{
                  $set_type .= '"'.$value2.'"';
                  $type_course .= '('.$detail_type['type_course'].')';
                }
              }else{
                if ($value2 == 0) {
                  $set_type .= '"'.$value2.'"'.',';
                  $type_course = '';
                }else{
                  $type_course .= '('.$detail_type['type_course'].') - ';
                  $set_type .= '"'.$value2.'"'.',';
                }
              }
              $id_type_course = '['.$set_type.']';

              $detail_price = $this->master_model->data('harga, discount', 'ls_m_harga_course', ['id_course' => $key, 'id_type_course' => $value2])->get()->row_array();
              
              if($detail_price['discount'] != NULL || $detail_price['discount'] != 0)
              {
                $discount     = ($detail_price['discount']/100) * $detail_price['harga'];
                $harga_final  = $detail_price['harga'] - $discount;
                if ($harga_final < 0)
                {
                  $price        += 0;
                } else
                {
                  $price        += $detail_price['harga'] - $discount;
                }
              } else
              {
                $price += $detail_price['harga'];
              }


            }
          $data = array(
            'id_course' => $course['data'][0]['id'],
            'id_user' => $this->session->userdata('id'),
            'id_type_course' => $id_type_course,
            'name' => preg_replace('/[^a-zA-Z0-9_ -]/s','', $course['data'][0]['judul']),
            'price' => $price,
            'type_course' => $type_course,
            'image' => $course['data'][0]['image'],
            'deskripsi_singkat' => $course['data'][0]['deskripsi_singkat'],
            'qty' => $numItems,
          );
          $check_cart = $this->master_model->check_data(['id_course' => $course['data'][0]['id'], 'id_user' => $this->session->userdata('id')], 'ls_t_cart');
          if ($check_cart) {
            if ($numItems > 0) {
              $this->master_model->update_cart($data, $course['data'][0]['id'], $this->session->userdata('id'));
            }else{
              $this->master_model->cart_delete_row($this->session->userdata('id'), $data, $course['data'][0]['id']);
            }
          }else{
            $this->master_model->save_cart($data);
          }
        }
          if (count($course_empty) > 0) {
              $this->db->where(['id_user'=>$this->session->userdata('id')]);
              $this->db->where_in('id_course', $course_empty);
              $this->db->delete('ls_t_cart');
          }

        $hasil['proses'] = 'success';
        $hasil['data'] = $data;
        $hasil['total'] = count($this->master_model->cart_contents($this->session->userdata('id')));
        $hasil['pesan'] = 'Cart updated!';
      }
      else{
        $hasil['pesan'] = 'Already Have The Course';
        $hasil['url'] = '';
      }
    }
    echo json_encode($hasil);
  }

  function delete_row_cart(){
    $id = $this->input->post('id');
    $check = false;
    if ($id != '' || !empty($id)) {
      foreach ($this->master_model->cart_contents($this->session->userdata('id')) as $key => $value) {
        if ($value['id_course'] == $id) {
          $check = true;
        }
      }
    }
    if ($check) {
      $this->master_model->cart_delete_row($this->session->userdata('id'), $id);
      $hasil['status'] = true;
      $hasil['pesan'] ='Course is removed from the cart!';
      $hasil['total'] =count($this->master_model->cart_contents($this->session->userdata('id')));
      $hasil['amount'] = rupiah($this->master_model->cart_total($this->session->userdata('id')));
    }else{
      $hasil['status'] = false;
      $hasil['pesan'] ='Course failed to be removed from the cart!';
    }
    echo json_encode($hasil);
  }

  function count_cart(){
        $rows = count($this->master_model->cart_contents($this->session->userdata('id')));
        echo $rows;
  }

}
