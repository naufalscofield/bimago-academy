<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";
class Users_sertification extends MY_Controller {

  public  $view = 'users/users_sertification/';
  
  public function __construct(){
		parent::__construct();
  }

  public function index()
  {
    $result['data'] = [];
    $result['class'] = 'users_sertification';

    $name = $this->input->get('name');
    $no_certificate = $this->input->get('no');

    if (isset($name) || isset($no_certificate)) {
      $query = '
        SELECT
          CONCAT(b.nama_depan, " ", b.nama_belakang) as nama,
          a.certificate_number,
          c.judul as course_name
        FROM
          ls_t_certificate a
        INNER JOIN ls_m_user b ON b.id = a.id_user
        INNER JOIN ls_m_course c ON c.id = a.id_course
        WHERE CONCAT(b.nama_depan, " ", b.nama_belakang) LIKE \'%' . strtolower($name) . '%\'';

      if ($no_certificate != null || $no_certificate != '')
        $query .= 'AND a.certificate_number = \'' . $no_certificate . '\'';

      $get_data = $this->db->query($query);
      $result['data'] = $get_data->result();
    }

    $this->load_template_users('users/template', $this->view.'display', $result);
  }

  public function find_certificate()
  {
    $name = $this->input->post('name');
    $no_certificate = $this->input->post('no_certificate');

    $query = '
      SELECT
        CONCAT(b.nama_depan, " ", b.nama_belakang) as nama,
        a.certificate_number,
        c.judul as course_name
      FROM
        ls_t_certificate a
      INNER JOIN ls_m_user b ON b.id = a.id_user
      INNER JOIN ls_m_course c ON c.id = a.id_course
      WHERE CONCAT(b.nama_depan, " ", b.nama_belakang) LIKE \'%' . strtolower($name) . '%\'';

    if ($no_certificate != null || $no_certificate != '')
      $query .= 'AND a.certificate_number = \'' . $no_certificate . '\'';

    $get_data = $this->db->query($query);

    echo json_encode($get_data->result());
  }
}