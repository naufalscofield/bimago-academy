<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Users_mycourse extends MY_Controller {
  public $view    = 'users/users_mycourse/';
	public function __construct(){
		parent::__construct();
    checkAuthJWTStudent();

    // if (!empty($this->session->userdata('spp')) && $this->session->userdata('spp') == 'belum')
    // {
    //   redirect('handlingspp');
    // }
    // if ($this->session->userdata('id')) {
    //   if ($this->session->userdata('role') == 'admin') {
    //     redirect('admin');
    //   }elseif ($this->session->userdata('role') == 'lecturer') {
    //     redirect('admin');
    //   }
    // }else{
    //   redirect('login');
    // }
    $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
            $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

            $signer     = new RS256Signer($privateKey);
            $verifier   = new RS256Verifier($publicKey);

            // Parse the token
            $parser = new Parser($verifier);

    if ($this->session->userdata('id'))
    {
      if ($this->session->userdata('jwt'))
      {
        $claims = $parser->parse(($this->session->userdata('jwt')));

        if ($claims['role'] != 'user')
        {
          $data['base_url']           = base_url();
          redirect('handling/not-authorized');
        }
      }
    } else
    {
        $this->session->set_flashdata('curent_url', current_url());
        redirect('login');
    }

    $this->load->database();
		$this->load->model('users/users_mycourse_model','model');
    $this->load->model('users/users_room_model', 'room_model');

	}

	public function index($name = ''){
		$result['course'] = base_url().'users/users_course';
		$result['link'] = base_url().'users/users_mycourse/tab';
		$result['list'] = 'all';
		$result['class'] = 'users_course';
    if ($name != '') $result['list'] = $name;
    if ($name == 'certificate_exam') $result['list'] = 'certificate';
		$this->load_template_users('users/template', $this->view.'display', $result, 'home');
	}

	public function tab(){
		$tab = $this->input->post('id');
        $data = [];
        $html ='';
        switch ($tab) {
            case 'all':
                $owned = owned_course();
                $final_owned = [];
                foreach ($owned as $key => $value) {
                  $value['exam'] = $this->master_model->owned_exam_detail($this->session->userdata('id'), $value['id'], $value['id_type']);
                  $final_owned[] = $value;
                }
                $getRoom                = $this->room_model->show(['a.id_user' => $this->session->userdata('id'), 'b.tipe' => 'exam']);
                $totalInRoom            = $getRoom->num_rows();
                $data['room']           = ($totalInRoom > 0) ? $getRoom->row()->id_exam : 'none';
                $data['owned'] = $final_owned;
                $data['course'] = base_url().'users/users_learn/learn/';
                $data['detail'] = base_url().'users/users_course/detail/';
                $html = $this->load->view($this->view.'all', $data,true);
                break;
            case 'wishlist':
                $purchased = $this->purchased($this->session->userdata('id'));
                $data['purchased'] = $this->convert_array($purchased['data']);
                $data['wishlist'] = base_url().'users/users_mycourse/add_remove_wishlist';
                $data['wishlisted'] = convert_rating($this->model->wishlisted()->result_array());
                $data['detail_cart'] = base_url('users/users_course/detail_cart/');
                $data['add_cart'] = base_url('users/users_cart/add_to_cart/');
                $data['add_free'] = base_url('users/users_checkout/add_to_free/');
                $data['course'] = base_url().'users/users_course';
                $data['detail'] = base_url().'users/users_course/detail/';
                $html = $this->load->view($this->view.'wishlist', $data,true);
                break;
            case 'history':
                $data['purchase'] = $this->get_purchase();
                $data['invoice'] = $this->get_invoice();
                $data['controller_page'] = $this;
                $data['detail'] = base_url().'users/users_course/detail/';
                $data['payment'] = base_url().'users/users_mycourse/payment/';
                $data['course'] = base_url().'users/users_course';
                $html = $this->load->view($this->view.'invoice', $data,true);
                break;
            case 'exam':
                $idUser                 = $this->session->userdata('id');
                $data['courses']        = $this->master_model->owned_exam($idUser);
                $data['class']          = "Users_course";
                $getRoom                = $this->room_model->show(['a.id_user' => $this->session->userdata('id'), 'b.tipe' => 'exam']);
                $totalInRoom            = $getRoom->num_rows();
                $data['room']           = ($totalInRoom > 0) ? $getRoom->row()->id_exam : 'none';

                $html = $this->load->view('users/exam/display', $data,true);
                break;
            case 'certificate':
                $data['list_cert'] = $this->master_model->data('a.*,b.judul,d.type_course,c.id_type_course', 'ls_t_certificate a',['a.id_user' => $this->session->userdata('id')])
                ->join('ls_m_course b', 'a.id_course = b.id','INNER')
                ->join('ls_m_exam c', 'c.id = a.id_exam', 'LEFT')
                ->join('ls_m_type_course d', 'c.id_type_course = d.id', 'LEFT')
                ->get();
                $html = $this->load->view('users/users_mycourse/list_cert', $data,true);
                break;
        }
        echo json_encode($html);
	}

  public function payment()
  {
    echo $this->load->view($this->view.'payment_method', [], true);
  }

  public function purchased($id = '')
	{
		$get = $this->model->get_data_purchased($id);
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  // public function wishlisted($value='')
  // {
  //   $data = convert_rating($this->model->wishlisted()->result_array());
  //   $data_final = [];
  //   foreach ($data as $key => $value) {
  //     if (!empty($value['type_course']) || $value['type_course'] != null) {
  //       $type_course=[];
  //       $type = json_decode($value['type_course']);
  //       foreach ($type as $key2 => $value2) {
  //         $detail = $this->master_model->data('a.id, a.id_type_course, a.harga, b.type_course', 'ls_m_harga_course a', ['a.id_course' => $value['id'], 'a.id_type_course' => $value2])
  //         ->join('ls_m_type_course b', 'a.id_type_course = b.id', 'LEFT')->get()->row_array();
  //         $type_course[] = $detail;
  //       }
  //       if ($this->session->userdata('id') != '') {
  //         $value['purchased'] = $this->type_purchased($value['id']);
  //       }else{
  //         $value['purchased'] = [];
  //       }
  //       $value['type_course'] = $type_course;
  //     }
  //     if ($this->session->userdata('id') != '') {
  //       $value['in_cart'] = $this->master_model->cart_ready($this->session->userdata('id'), $value['id']);
  //     }else{
  //       $value['in_cart'] = [];
  //     }
  //     $data_final[] = $value;
  //   }
  //   return $data_final;
  // }

  public function type_purchased($id_course = '')
	{
		$get = $this->model->get_data_type_purchased($this->session->userdata('id'), $id_course)->result_array();
    $data=[];
    foreach ($get as $key => $value) {
      foreach ($value as $key2 => $value2) {
        array_push($data, $value2);
      }
    }
    return $data;
	}

  public function convert_array($array) {
    if (!is_array($array)) {
      return [];
    }
    $result = array();
    foreach ($array as $key => $value) {
      $result[$key] = $value['id_course'];
    }
    return $result;
  }

  public function get_purchase()
	{
		$get = $this->model->get_purchase('ls_t_detail_pembelian');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_invoice()
	{
		$get = $this->model->get_invoice('ls_t_pembayaran');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function certificate(){
      $id = $this->input->post('id');
      $msg = ['status'=> FALSE, 'msg'=>'Gagal Generate Certificate', 'url'=> ''];

      if (!empty($id)) {
          $data = $this->master_model->data('a.id_course','ls_t_certificate a',['a.id'=>$id])
                  ->join('ls_m_certificate b', 'a.id_course = b.id_course','INNER')->get();
          if ($data->num_rows() > 0) {
            $cert_upload = $this->master_model->data('a.url', 'ls_t_certificate_upload a',['a.id_certificate'=> $id])->get();
            if ($cert_upload->num_rows() > 0) {
                $cert_upload = $cert_upload->row('url');
                $filename = base_url($cert_upload);
            }else{
                $cert = generateCertificate($id);
                $filename = base_url('assets/certificate/temp/'.$cert.'.png');
            }
            // $this->load->helper('download');
            // force_download(BASEPATH."../assets/certificate/temp/".$filename.".png", NULL);
            $msg = ['status'=> TRUE, 'msg'=>'Succes Generate Certificate',
             'url'=> $filename
           ];
          }else{
            $msg = ['msg'=>'Succes Generate Certificate', 'url'=> base_url('pagenotfound')];
          }
      }

      echo json_encode($msg);
  }

  public function add_remove_wishlist(){
    $id = $this->input->post('id');
    $data = array(
      'id_course' => $id,
      'id_user' => $this->session->userdata('id')
    );
    $check = $this->master_model->check_data($data, 'ls_t_wishlist');
    if ($check) {
      $this->master_model->delete($data, 'ls_t_wishlist');
      $hasil['pesan'] = 'Berhasil Dihapus Dari Wishlist.';
      $hasil['proses'] = 'deleted';
    }else{
      $this->master_model->save($data, 'ls_t_wishlist');
      $hasil['proses'] = 'added';
      $hasil['pesan'] = 'Berhasil Ditambahakan Ke Wishlist.';
    }
    echo json_encode($hasil);
  }
  public function desc(){
    $id_course = $this->input->post('id_course');
    $data = $this->master_model->data('a.*, b.nama_depan, b.nama_belakang, b.institusi','ls_m_course a' ,['a.id' => $id_course])
            ->join('ls_m_user b', 'a.kontributor = b.id')->get()->row();
    $result = [
      'course' => $data
    ];

    $html = $this->load->view($this->view.'desc', $result,true);

    echo json_encode($html);
  }
  public function certpdf($id){
    $id = decode_url($id);
    if (!empty($id)) {
        $data = $this->master_model->data('a.id_course','ls_t_certificate a',['a.id'=>$id])
                ->join('ls_m_certificate b', 'a.id_course = b.id_course','INNER')->get();
        if ($data->num_rows() > 0) {
          $cert_upload = $this->master_model->data('a.url', 'ls_t_certificate_upload a',['a.id_certificate'=> $id])->get();
          if ($cert_upload->num_rows() > 0) {
              $cert_upload = $cert_upload->row('url');
              $filesource = BASEPATH.'../'.$cert_upload;
          }else{
              $cert = generateCertificate($id);
              $filesource = BASEPATH.'../assets/certificate/temp/'.$cert.'.png';
          }
          $mpdf = new \Mpdf\Mpdf([
            'mode'                  => 'utf-8',
            'format'                => 'A4',
            'img_dpi'               => 300,
            'dpi'                   => 300,
            'orientation'           => 'L',
          ]);

          $image  = '<div style="position: absolute; top: 0mm; left: 0mm; width: 100%; height: 100%; overflow:hidden; margin:0;"><img src="'.$filesource.'" style="width:100%; margin: 0;" /></div>';

          $mpdf->SetTitle('Certificate');
          $mpdf->WriteHTML( $image, 2 );
          $mpdf->Output();
        }else{
          $msg = ['msg'=>'Succes Generate Certificate', 'url'=> base_url('pagenotfound')];
        }
    }
  }
  public function interest(){
    $result['link'] = base_url().'users/users_mycourse/save_interest';
    $result['class'] = 'users_course';
    $result['data'] = $this->master_model->data('a.*,b.id_kategori as id_exist','ls_m_kategori a')->join('ls_t_kategori b', 'a.id = b.id_kategori  and b.id_user = '.$this->session->userdata('id'),'left')->order_by('a.id')->get();
    $this->load_template_users('users/template', $this->view.'interest', $result, 'home');
  }
  public function save_interest(){
    $hasil['pesan']  = 'Failed to save Interest.';
    $hasil['status'] = 'failed';
    $input =  $this->input->post('interest');
    $this->master_model->delete(['id_user'=> $this->session->userdata('id')], 'ls_t_kategori');
    if (is_array($input) && count($input) > 0 ) {
      foreach ($input as $value) {
        $insert = $this->master_model->save(['id_kategori'=>$value, 'id_user'=> $this->session->userdata('id')], 'ls_t_kategori');
      }
      if ($insert) {
        $hasil['status'] = 'success';
        $hasil['pesan'] = 'Success Save Interest.';
      }
    }else{
      $hasil['status'] = 'warning';
      $hasil['pesan'] = 'Please Choose your Interest.';
    }

    echo json_encode($hasil);

  }

}
