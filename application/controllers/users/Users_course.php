<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_course extends MY_Controller {
  public $view    = 'users/users_course/';
  public $view_learn    = 'users/users_learn/';
	public function __construct(){
		parent::__construct();
    if ($this->session->userdata('id')) {
      if ($this->session->userdata('role') == 'admin') {
        redirect('admin');
      }elseif ($this->session->userdata('role') == 'lecturer') {
        redirect('admin');
      }
    }
    $this->load->database();
		$this->load->model('users/users_course_model','model');
	}

	public function index(){
    $result['class'] = $this->class;
    $result['get_data_url'] = base_url('users/'.$this->class.'/data');
    $result['data_source'] = base_url('users/'.$this->class.'/data');
    $result['data_modal'] = base_url('users/'.$this->class.'/get_modal');
    $result['modul'] = $this->get_modul();
    $result['type_course'] = $this->master_model->data('*', 'ls_m_type_course')->get()->result_array();
		$this->load_template_users('users/template', $this->view.'display', $result);
	}

  public function detail($id){
    $id = decode_url_share_id($id);
    $this->count_course_visitor($id);
    $data = $this->get($id);
    $result['id_course'] = $id;
    $result['data'] = $data;
    $result['class'] = $this->class;
    $result['dinamic_meta'] = 'on';
    $result['modul'] = $this->model->get_modul()->result_array();
    $result['terkait'] = label_course(convert_rating($this->model->get_terkait($data['data'][0]['id_modul'], $id)->result_array()));
    $result['detail'] = base_url('users/'.$this->class.'/detail/');
    $result['detail_cart'] = base_url('users/users_cart/add_to_cart_single/');
    // $result['detail_cart'] = base_url('users/'.$this->class.'/detail_cart/');
    $result['add_cart'] = base_url('users/users_cart/add_to_cart/');
    if ($this->session->userdata('id') != ''){
      $wishlist = $this->wishlisted($this->session->userdata('id'));
      $result['wishlisted'] = $this->convert_array($wishlist['data']);

      $result['check_owned'] = $this->master_model->owned_product($this->session->userdata('id'), $id);
    }
    $result['login_page'] = base_url().'users/users_login';
    $result['wishlist'] = base_url().'users/users_mycourse/add_remove_wishlist';
    $result['save_rating'] = base_url().'users/users_course/save_rating';
    $result['comment'] = $this->comment($id);
    if ($this->session->userdata('token') != '') {
      $result['type_comment'] = $this->type_comment($id);
    }
    $result['add_free'] = base_url('users/users_checkout/add_to_free/');

		$this->load_template_users('users/template', $this->view.'detail', $result);
	}

  public function data()
  {
    $limit  = $this->input->post('limit');
    $offset = $this->input->post('pagenum') - 1;
    $sort  = $this->input->post('sort');
    $like  = $this->input->post('keyword');

    $result['data'] = $this->get('', $limit, $offset, $sort, $like);
    if ($this->session->userdata('id') != ''){
      $wishlist = $this->wishlisted($this->session->userdata('id'));
      $result['wishlisted'] = $this->convert_array($wishlist['data']);
    }
    $result['wishlist'] = base_url().'users/users_mycourse/add_remove_wishlist';
    $result['login_page'] = base_url().'users/users_login';
    $result['detail'] = base_url('users/'.$this->class.'/detail/');
    $result['add_cart'] = base_url('users/users_cart/add_to_cart/');
    $result['add_free'] = base_url('users/users_checkout/add_to_free/');
    $result['show_tutor'] = base_url('users/'.$this->class.'/tutor/');
    $result['detail_cart'] = base_url('users/users_cart/add_to_cart_single/');
    $result['class'] = $this->class;
    $result['pagenum'] = $this->input->post('pagenum');
    $result['course'] = base_url().'users/users_course';

    $return_data = $this->load->view('users/'.$this->class.'/table',$result,true);
    echo $return_data;
  }

	public function get($id = '', $limit = 0, $offset = 0, $sort = '', $like = '')
	{
    $rating = $this->input->post('rating');
    $final_rating = '';
    if ($rating) {
      $no = 0;
      $jumlah = count($rating);
      foreach ($rating as $key => $value) {
        if ($value == 5) {
          if ($no == $jumlah - 1) {
            $final_rating .= 'd.score >= 5';
          }else{
            $final_rating .= 'd.score >= 5 OR ';
          }
        }elseif ($value == 4) {
          if ($no == $jumlah - 1) {
            $final_rating .= 'd.score >= 4';
          }else{
            $final_rating .= 'd.score >= 4 OR ';
          }
        }elseif ($value == 3) {
          if ($no == $jumlah - 1) {
            $final_rating .= 'd.score >= 3';
          }else{
            $final_rating .= 'd.score >= 3 OR ';
          }
        }elseif ($value == 2) {
          if ($no == $jumlah - 1) {
            $final_rating .= 'd.score >= 2';
          }else{
            $final_rating .= 'd.score >= 2 OR ';
          }
        }
        elseif ($value == 1) {
          $final_rating .= 'd.score >= 1';
        }
        $no++;
      }
    }
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$this->model->kategori = $this->input->post('modul') != '' ? $this->input->post('modul') : '';
		$this->model->type = $this->input->post('type') != '' ? $this->input->post('type') : '';
		$this->model->rating = $this->input->post('rating') != '' ? $final_rating : '';
		$this->model->offset = $offset * $limit;
    $this->model->sort  = $sort;
    $this->model->like  = ($like != '') ? $like : '';
		$totaldata = $this->model->get_data('ls_m_course');
		$this->model->limit = $limit;
		$get = $this->model->get_data('ls_m_course');
    $data = convert_rating($get->result_array());
    // dd($data);
    $data_final = [];
    foreach ($data as $key => $value) {
      if (!empty($value['type_course']) || $value['type_course'] != null) {
        $type_course=[];
        $type = json_decode($value['type_course']);
        foreach ($type as $key2 => $value2) {
          $detail = $this->master_model->data('a.id, a.id_type_course, a.harga, b.type_course', 'ls_m_harga_course a', ['a.id_course' => $value['id'], 'a.id_type_course' => $value2])
          ->join('ls_m_type_course b', 'a.id_type_course = b.id', 'LEFT')->get()->row_array();
          $type_course[] = $detail;
        }
        if ($this->session->userdata('id') != '') {
          $value['purchased'] = $this->type_purchased($value['id']);
        }else{
          $value['purchased'] = [];
        }
        $value['type_course'] = $type_course;
      }
      if ($this->session->userdata('id') != '') {
        $value['in_cart'] = $this->master_model->cart_ready($this->session->userdata('id'), $value['id']);
      }else{
        $value['in_cart'] = [];
      }
      $data_final[] = $value;
    }
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = label_course($data_final);
    }
    if ($limit != 0)
    {
      $response['total_page'] = ceil($totaldata->num_rows() / $limit);
    }
    return $response;
	}

  public function wishlisted($id = '')
	{
		$get = $this->model->get_data_wishlisted($id);
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function type_purchased($id_course = '')
	{
		$get = $this->model->get_data_type_purchased($this->session->userdata('id'), $id_course)->result_array();
    $data=[];
    foreach ($get as $key => $value) {
      foreach ($value as $key2 => $value2) {
        array_push($data, $value2);
      }
    }
    return $data;
	}

  public function comment($id){
    $comment = $this->master_model->data('c.login_via, c.avatar, c.jk, a.id_user, a.id_course, a.score, a.comment, a.tanggal_komentar, c.nama_belakang, c.nama_depan', 'ls_t_rating a', ['a.id_course' => $id])
    ->join('ls_m_user c', 'a.id_user = c.id', 'LEFT')
    ->get()->result_array();
    return $comment;
  }

  public function type_comment($id){
    $check = $this->master_model->owned_product($this->session->userdata('id'), $id);
    $return = FALSE;
    if (count($check) > 0) {
      $return = TRUE;
      if ($this->master_model->check_data(['id_user' => $this->session->userdata('id'), 'id_course' => $id], 'ls_t_rating')) {
        $return = FALSE;
      }
    }
    return $return;
  }

  public function get_modul($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->master_model->data('*','ls_m_modul')->get();
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal()
	{
    $this->model->id = $this->input->post('id');
    $get = $this->model->get_data('ls_m_course');
    $response['pesan'] = 'data tidak ada';
    $response['status'] = 404;
    $response['data'] = array();
    $response['disable'] = 'disabled';
    $response['add_cart'] = base_url('users/users_cart/add_to_cart/');
    if ($get -> num_rows() > 0) {
      $response['pesan'] = 'data ada';
      $response['status'] = 200;
      $response['data'] = $get->row_array();
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
	}

  public function convert_array($array) {
    if (!is_array($array)) {
      return [];
    }
    $result = array();
    foreach ($array as $key => $value) {
      $result[$key] = $value['id_course'];
    }
    return $result;
  }

  public function save_rating(){
    $conf = array(
            array('field' => 'rating', 'label' => 'Rating', 'rules' => 'trim|required'),
            array('field' => 'message', 'label' => 'Message', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s can not be empty.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      $data_rating = array(
        'id_course' => $this->input->post('id_course'),
        'id_user' => $this->session->userdata('id'),
        'score' => $this->input->post('rating'),
        'comment' => $this->input->post('message'),
      );
      $rating = $this->master_model->save($data_rating, 'ls_t_rating');
      $check_rating = $this->master_model->check_data(['id_course' => $this->input->post('id_course')], 'ls_t_convert_rating');
      if ($check_rating) {
        $get_rating = $this->master_model->data('AVG(score) as convert_nilai', 'ls_t_rating', ['id_course' => $this->input->post('id_course')])->get()->row();
        $avg = round($get_rating->convert_nilai, 2);
        $this->master_model->update(['score' => $avg], ['id_course' => $this->input->post('id_course')], 'ls_t_convert_rating');
      }else{
        $rating_convert = array(
          'id_course' => $this->input->post('id_course'),
          'score' => $this->input->post('rating'),
        );
        $this->master_model->save($rating_convert, 'ls_t_convert_rating');
      }
      if ($rating) {
        $response['pesan'] = 'Thank you for giving your comments and ratings.';
        $response['proses'] = 'success';
      }else{
        $response['pesan'] = 'Comments and ratings cannot be saved.';
        $response['proses'] = 'failed';
      }
    }
      echo json_encode($response);
  }

  public function detail_cart()
  {
    $id_course = $this->input->post('id');
    $this->model->id = $id_course;
    $get = $this->model->get_data('ls_m_course');
    $data = $get->result_array();
    $type_course=[];
    $type = json_decode($data[0]['type_course']);
    foreach ($type as $key2 => $value2) {
      $detail = $this->master_model->data('a.id, a.id_type_course, a.harga, a.discount, b.type_course', 'ls_m_harga_course a', ['a.id_course' => $id_course, 'a.id_type_course' => $value2])
      ->join('ls_m_type_course b', 'a.id_type_course = b.id', 'LEFT')->get()->row_array();
      $type_course[] = $detail;
    }
    $result['type_course'] = $type_course;
    if ($this->session->userdata('id') != '') {
      $result['purchased'] = $this->type_purchased($id_course);
    }else{
      $result['purchased'] = [];
    }
    if ($this->session->userdata('id') != '') {
      $result['in_cart'] = $this->master_model->cart_ready($this->session->userdata('id'), $id_course);
    }else{
      $result['in_cart'] = [];
    }
    $result['id'] = $id_course;
    $result['add_cart'] = base_url('users/users_cart/add_to_cart/');
    $result['judul'] = $data[0]['judul'];
    $result['pelaksanaan_led'] = $data[0]['pelaksanaan_led'];
    $result['pelaksanaan_training'] = $data[0]['pelaksanaan_training'];
    $result['pelaksanaan_webinar'] = $data[0]['pelaksanaan_webinar'];
    echo $this->load->view($this->view.'detail_cart', $result, true);
  }

  public function tab(){
      $tab = $this->input->post('id');
      $id_course = $this->input->post('id_course');
      $data = [];
      $html ='';
      switch ($tab) {
          case 'overview':
              $course = $this->master_model->data('deskripsi,deskripsi_singkat','ls_m_course',['id' => $id_course])->order_by('id','asc')->get()->row();
              $data['overview'] = isset($course->deskripsi) ? $course->deskripsi : '';
              $data['deskripsi_singkat'] = isset($course->deskripsi_singkat) ? $course->deskripsi_singkat : '';
              $html = $this->load->view($this->view_learn.'overview', $data,true);
              break;
          case 'tutor':
              $data['tutor'] = $this->master_model->data('b.no_hp, b.institusi, b.nama_depan, b.nama_belakang, b.avatar, b.profil', 'ls_m_course a', ['a.id' => $id_course])
                                ->join('ls_m_user b', 'a.kontributor = b.id', 'LEFT')
                                ->get()->row_array();
              $html = $this->load->view($this->view_learn.'tutor', $data,true);
              break;
          case 'pricing':
              $judul = $this->master_model->data('judul', 'ls_m_course', ['id' => $id_course])
                                ->get()->row_array();
              $data['judul'] = $judul['judul'];
              $data['pricing'] = $this->master_model->data('a.harga, b.type_course', 'ls_m_harga_course a', ['id_course' => $id_course])
                                ->join('ls_m_type_course b', 'a.id_type_course = b.id')
                                ->get()->result_array();
              $html = $this->load->view($this->view_learn.'pricing', $data,true);
              break;
      }
      echo json_encode($html);
  }

}
