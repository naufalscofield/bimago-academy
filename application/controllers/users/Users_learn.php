<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Users_learn extends MY_Controller {
  public $view    = 'users/users_learn/';
	public function __construct(){
		parent::__construct();
    
    // if ($this->session->userdata('id')) {
    //   if ($this->session->userdata('role') == 'admin') {
    //     redirect('admin');
    //   }elseif ($this->session->userdata('role') == 'lecturer') {
    //     redirect('admin');
    //   }
    // }else{
    //   redirect('login');
    // }

    $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
    $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

    $signer     = new RS256Signer($privateKey);
    $verifier   = new RS256Verifier($publicKey);

    // Parse the token
    $parser = new Parser($verifier);

    if ($this->session->userdata('id'))
    {
      if ($this->session->userdata('jwt'))
      {
        $claims = $parser->parse(($this->session->userdata('jwt')));

        if ($claims['role'] != 'user')
        {
          $data['base_url']           = base_url();
          redirect('handling/not-authorized');
        }
      }
    } else
    {
        redirect('login');
    }

    // if (!empty($this->session->userdata('spp')) && $this->session->userdata('spp') == 'belum')
    // {
    //   redirect('handlingspp');
    // }

    $this->load->model('users/users_room_model', 'room_model');
    $this->load->model('admin/exam_model', 'exam_model');
    $this->load->model('admin/questions_model', 'questions_model');
	}

    public function learn($id = '',$id_type_course='', $id_episode=''){
      $id = decode_url($id);
      $related = $this->relatedcourse($id);
      $result =[];
      $result['info'] = $this->master_model->data('*', 'ls_m_lms_config',['id' => 1])->get()->row();
      $id_type_course = decode_url($id_type_course);
      $check_room = $this->master_model->check_data(['id_user' => $this->session->userdata('id')], 'ls_t_room_exam');
      if ($check_room)
      {
        $check_type = $this->master_model->data('a.id_course, b.id_episode, b.tipe', 'ls_t_room_exam a', ['id_user' => $this->session->userdata('id')])
        ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')->get()->row();
        if ($check_type->tipe == 'quiz') {
          redirect ("create-room-quiz/".$check_type->id_course."/y/".$check_type->id_episode);
        }else{
          redirect ("create-room/".$check_type->id_course."/y");
        }
      } else {
      $check_video = $this->master_model->check_data(['id_course' => $id, 'id_type_course' => $id_type_course, 'status' => 2], 'ls_m_episode');
      if ($check_video) {
        if (empty($id_episode) || $id_episode == '') {
          $min_order = get_min('order', 'ls_m_episode', ['id_course' => $id, 'id_type_course' => $id_type_course, 'status' => 2]);
          $get_id_episode = $this->master_model->data('id', 'ls_m_episode', ['order' => $min_order, 'id_course' => $id, 'id_type_course' => $id_type_course])->get()->row();
          $id_episode = $get_id_episode->id;
        }else{
          $id_episode = decode_url($id_episode);
        }
        $owned = $this->master_model->owned_id($this->session->userdata('id'));
        $owned = $this->convert_array($owned);
        $progres = $this->master_model->data('persentase, waktu', 'ls_t_progress_watch', ['id_user' =>$this->session->userdata('id'), 'id_episode' => $id_episode])->get()->row();

        // =======================================EPISODE
        $get_episode = $this->master_model->data('a.*, b.nilai','ls_m_episode a',['id_course'=>$id, 'id_type_course' => $id_type_course, 'status' => 2])
        ->join('ls_t_rating_episode_convert b', 'a.id = b.id_episode', 'LEFT')
        ->order_by('a.order','ASC')
        ->get();
        $total_episode = count($get_episode->result());
        $list_episode = [];
        $video = 0;
        foreach ($get_episode->result_array() as $key => $value) {
          $progres_vid = $this->master_model->data('persentase', 'ls_t_progress_watch', ['id_user' => $this->session->userdata('id'), 'id_episode' => $value['id']])->get()->row();
          $value['progres_vid'] = isset($progres_vid->persentase) ? $progres_vid->persentase : 0;
          if (isset($progres_vid->persentase)) {
            $video += $progres_vid->persentase;
          }
          $jumlah = $this->master_model->data('COUNT(id) as jumlah', 'ls_t_rating_episode', ['id_episode' => $value['id']])->get()->row();
          $get_quiz = $this->master_model->data('a.status', 'ls_t_after_exam a', ['b.id_episode' => $value['id'] ,'a.id_user' => $this->session->userdata('id'), 'b.id_type_course' => $id_type_course, 'b.id_course' => $id])
          ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')->get()->row();
          if (!empty($get_quiz)) {
            $value['quiz'] = $get_quiz->status;
          }else{
            $value['quiz'] = '';
          }
          $value['reviewer'] = $jumlah->jumlah;
          $value['rating_depan'] = '';
          $value['rating_belakang'] = '';
          if (!empty($value['nilai'])) {
            $split = explode('.', $value['nilai']);
            $value['rating_depan'] = $split[0];
            if ($split[1] != 0) {
              $value['rating_belakang'] = $split[1];
            }
          }
          $list_episode[] = $value;
        }
        if (empty($id_episode)) {
            $id_episode = $this->master_model->data('id','ls_m_episode')->order_by('id','asc')->get()->row('id');
        }
        $exam = $this->master_model->owned_exam_detail($this->session->userdata('id'), $id, $id_type_course);
        $idUser                 = $this->session->userdata('id');
        $getRoom                = $this->room_model->show(['a.id_user' => $idUser, 'b.tipe' => 'quiz']);
        $totalInRoom            = $getRoom->num_rows();
        $room                   = ($totalInRoom > 0) ? $getRoom->row()->id_exam : 'none';
        // $quiz = $this->master_model->data('count(a.id) as progres_quiz', 'ls_t_after_exam a', ['a.status' => 'passed','a.id_user' => $this->session->userdata('id'), 'b.id_type_course' => $id_type_course, 'b.id_course' => $id, 'tipe' => 'quiz'])
        // ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')->get()->row();
        // $persen_quiz = round(($quiz->progres_quiz/$total_episode)*100);
        // $persen_vid = round(($video/$total_episode));
        $progres_all = round(($video/$total_episode));
        // $progres_all = ($persen_quiz+$persen_vid)/2;
        // =======================================

        if (in_array($id, $owned)) {
          if ($id != '') {
            $urlPag = $this->getPagination($id, $id_type_course, $id_episode);
            if ($id_episode != '') {
              $episode = $this->master_model->check_data(['id_course' => $id, 'id_type_course' => $id_type_course, 'id' => $id_episode, 'status' => 2], 'ls_m_episode');
              if ($episode) {
                $course = $this->master_model->data('*','ls_m_course',['id'=>$id])->get()->row();
                if (empty($id_episode)) {
                  $episode = $this->master_model->data('url,type,judul,video','ls_m_episode',['id_course'=>$id, 'id_type_course' => $id_type_course, 'status' => 2])->order_by('id','asc')->get()->row();
                  $url_video = isset($episode->url) ? $episode->url : '';
                  $type      =  isset($episode->type) ? $episode->type : '';
                  $judul     =  isset($episode->judul) ? $episode->judul : '';
                  $video = isset($episode->video) ? $episode->video : '';

                }else{
                  $episode   = $this->master_model->data('url,type,judul,video','ls_m_episode',['id_course'=>$id,'id'=>$id_episode, 'id_type_course' => $id_type_course, 'status' => 2])->get()->row();
                  $url_video = isset($episode->url) ? $episode->url : '';
                  $type      =  isset($episode->type) ? $episode->type : '';
                  $judul     =  isset($episode->judul) ? $episode->judul : '';
                  $video = isset($episode->video) ? $episode->video : '';

                }
                $result = [
                  'course'          => $course,
                  'id_episode'      => $id_episode,
                  'url_video'       => $url_video,
                  'video'           => $video,
                  'type'            => $type,
                  'judul'           => $judul,
                  'urlPag'          => $urlPag,
                  'id'              => $id,
                  'id_type_course'  => $id_type_course,
                  'waktu'              => isset($progres->waktu) ? $progres->waktu : 0 ,
                  'judul_course'   => $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row()->judul,
                  'num_episode'   => $get_episode->num_rows(),
                  'episode'   => $list_episode,
                  'id_episode'=> $id_episode,
                  'id_course' => $id,
                  'id_type_course' => $id_type_course,
                  'exam' => $exam,
                  'room' => $room,
                  'progres_all' => $progres_all,
                  // 'progres_vid'     => $progres_vid,
                  // 'progres_quiz'    => $progres_quiz,
                  // 'progres_all'     => $progres_all,
                  // 'length_bar'      => $length_bar,
                  // 'length'          => $length,
                  ];
                  $result['info'] = $this->master_model->data('*', 'ls_m_lms_config',['id' => 1])->get()->row();
                  $result['recomedations'] = $this->master_model->data('*', 'ls_m_course')->where_in('id',$related['ids'])->get()->result_array();
                  $result['status_recommend'] = $related['st'];
                  $this->load->view($this->view.'display', $result);
                }else{
                  redirect('users/users_course/detail/'.encode_url($id));
                }
              }else{
                $course = $this->master_model->data('*','ls_m_course',['id'=>$id])->get()->row();
                if (empty($id_episode)) {
                  $episode = $this->master_model->data('url,type,judul,video','ls_m_episode',['id_course'=>$id, 'id_type_course' => $id_type_course, 'status' => 2])->order_by('id','asc')->get()->row();
                  $url_video = isset($episode->url) ? $episode->url : '';
                  $type =  isset($episode->type) ? $episode->type : '';
                  $judul =  isset($episode->judul) ? $episode->judul : '';
                  $video = isset($episode->video) ? $episode->video : '';

                }else{
                  $episode   = $this->master_model->data('url,type,judul,video','ls_m_episode',['id_course'=>$id,'id'=>$id_episode, 'id_type_course' => $id_type_course, 'status' => 2])->get()->row();
                  $url_video = isset($episode->url) ? $episode->url : '';
                  $type =  isset($episode->type) ? $episode->type : '';
                  $judul =  isset($episode->judul) ? $episode->judul : '';
                  $video = isset($episode->video) ? $episode->video : '';

                }
                $result = [
                'course'            => $course,
                'id_episode'        => $id_episode,
                'url_video'         => $url_video,
                'video'           => $video,
                'type'              => $type,
                'judul'             => $judul,
                'id'                => $id,
                'id_type_course'    => $id_type_course,
                'urlPag'            => $urlPag,
                'prev'              => null,
                'waktu'              => isset($progres->waktu) ? $progres->waktu : 0 ,
                'judul_course'   => $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row()->judul,
                'num_episode'   => $get_episode->num_rows(),
                'episode'   => $list_episode,
                'id_episode'=> $id_episode,
                'id_course' => $id,
                'id_type_course' => $id_type_course,
                'exam' => $exam,
                'room' => $room,
                'progres_all' => $progres_all,
                // 'progres_vid'       => $progres_vid,
                // 'progres_quiz'      => $progres_quiz,
                // 'progres_all'       => $progres_all,
                // 'length'            => $length,
                // 'length_bar'        => $length_bar,
                ];
                $result['recomedations'] = $this->master_model->data('*', 'ls_m_course')->where_in('id',[1,2,3])->get()->result_array();

                $this->load->view($this->view.'display', $result);
              }
            }else{
              redirect('welcome');
            }
          }else{
            redirect('welcome');
          }
      }else{
        $result['course'] = $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row()->judul;
        $result['type'] = $this->master_model->data('type_course', 'ls_m_type_course', ['id' => $id_type_course])->get()->row()->type_course;
        $this->load->view($this->view.'display_none', $result);
      }
    }


    }
    public function tab(){
        $tab = $this->input->post('id');
        $id_course = $this->input->post('id_course');
        $id_type_course = $this->input->post('id_type_course');
        $id_episode      = $this->input->post('id_episode');
        $data = [];
        $html ='';
        switch ($tab) {
            case 'search':
                $html = $this->load->view($this->view.'search', $data,true);
                break;
            case 'content':
                $get_episode = $this->master_model->data('a.*, b.nilai','ls_m_episode a',['id_course'=>$id_course, 'id_type_course' => $id_type_course, 'status' => 2])
                ->join('ls_t_rating_episode_convert b', 'a.id = b.id_episode', 'LEFT')
                ->order_by('a.order','ASC')
                ->get();
                $waktu = $this->master_model->data('waktu', 'ls_t_progress_watch', ['id_user' =>$this->session->userdata('id'), 'id_episode' => $id_episode])->get()->row();
                $total_episode = count($get_episode->result());
                $episode = [];
                $video = 0;
                foreach ($get_episode->result_array() as $key => $value) {
                  $progres_vid = $this->master_model->data('persentase', 'ls_t_progress_watch', ['id_user' => $this->session->userdata('id'), 'id_episode' => $value['id']])->get()->row();
                  $value['progres_vid'] = isset($progres_vid->persentase) ? $progres_vid->persentase : 0;
                  if (isset($progres_vid->persentase)) {
                    $video += $progres_vid->persentase;
                  }
                  $jumlah = $this->master_model->data('COUNT(id) as jumlah', 'ls_t_rating_episode', ['id_episode' => $value['id']])->get()->row();
                  $get_quiz = $this->master_model->data('a.status', 'ls_t_after_exam a', ['b.id_episode' => $value['id'] ,'a.id_user' => $this->session->userdata('id'), 'b.id_type_course' => $id_type_course, 'b.id_course' => $id_course])
                  ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')->get()->row();
                  if (!empty($get_quiz)) {
                    $value['quiz'] = $get_quiz->status;
                  }else{
                    $value['quiz'] = '';
                  }
                  $value['reviewer'] = $jumlah->jumlah;
                  $value['rating_depan'] = '';
                  $value['rating_belakang'] = '';
                  if (!empty($value['nilai'])) {
                    $split = explode('.', $value['nilai']);
                    $value['rating_depan'] = $split[0];
                    if ($split[1] != 0) {
                      $value['rating_belakang'] = $split[1];
                    }
                  }
                  $episode[] = $value;
                }
                if (empty($id_episode)) {
                    $id_episode = $this->master_model->data('id','ls_m_episode')->order_by('id','asc')->get()->row('id');
                }
                $exam = $this->master_model->owned_exam_detail($this->session->userdata('id'), $id_course, $id_type_course);
                $idUser                 = $this->session->userdata('id');
                $getRoom                = $this->room_model->show(['a.id_user' => $idUser, 'b.tipe' => 'quiz']);
                $totalInRoom            = $getRoom->num_rows();
                $room                   = ($totalInRoom > 0) ? $getRoom->row()->id_exam : 'none';
                // $quiz = $this->master_model->data('count(a.id) as progres_quiz', 'ls_t_after_exam a', ['a.status' => 'passed','a.id_user' => $this->session->userdata('id'), 'b.id_type_course' => $id_type_course, 'b.id_course' => $id_course, 'tipe' => 'quiz'])
                // ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')->get()->row();
                // $persen_quiz = round(($quiz->progres_quiz/$total_episode)*100);
                // $persen_vid = round(($video/$total_episode));
                $progres_all = round($video/$total_episode);
                // $progres_all = ($persen_quiz+$persen_vid)/2;
                $data = [
                    'judul_course'   => $this->master_model->data('judul', 'ls_m_course', ['id' => $id_course])->get()->row()->judul,
                    'num_episode'   => $get_episode->num_rows(),
                    'episode'   => $episode,
                    'id_episode'=> $id_episode,
                    'id_course' => $id_course,
                    'id_type_course' => $id_type_course,
                    'exam' => $exam,
                    'room' => $room,
                    'progres_all' => $progres_all,
                    'waktu'       => isset($waktu->waktu) ? $waktu->waktu : 0 ,

                ];

                $html = $this->load->view($this->view.'content', $data,true);
                break;
            case 'overview':
                $course = $this->master_model->data('deskripsi,deskripsi_singkat','ls_m_course',['id' => $id_course])->order_by('id','asc')->get()->row();
                $data['overview'] = isset($course->deskripsi) ? $course->deskripsi : '';
                $data['deskripsi_singkat'] = isset($course->deskripsi_singkat) ? $course->deskripsi_singkat : '';
                $html = $this->load->view($this->view.'overview', $data,true);
                break;
            case 'overview_episode':
                $course = $this->master_model->data('deskripsi, judul','ls_m_episode',['id' => $id_episode])->get()->row();
                $data['overview'] = isset($course->deskripsi) ? $course->deskripsi : 'Deskripsi Episode Belum Di Isi.';
                $data['judul'] = isset($course->judul) ? $course->judul : 'Judul Belum Di Isi.';

                $html = $this->load->view($this->view.'overview_episode', $data,true);
                break;
            case 'tutor':
                $data['tutor'] = $this->master_model->data('b.no_hp, b.institusi, b.nama_depan, b.nama_belakang, b.avatar, b.profil', 'ls_m_course a', ['a.id' => $id_course])
                                  ->join('ls_m_user b', 'a.kontributor = b.id', 'LEFT')
                                  ->get()->row_array();
                $html = $this->load->view($this->view.'tutor', $data,true);
                break;
            case 'tutor_video':
                $data['tutor'] = $this->master_model->data('b.no_hp, b.institusi, b.nama_depan, b.nama_belakang, b.avatar, b.profil', 'ls_m_course a', ['a.id' => $id_course])
                                  ->join('ls_m_user b', 'a.kontributor = b.id', 'LEFT')
                                  ->get()->row_array();
                $html = $this->load->view($this->view.'tutor_video', $data,true);
                break;
            case 'resource':
                $data['resource'] = $this->master_model->data('a.*','ls_m_sumber_episode a', ['b.id_course' => $id_course, 'b.id_type_course' => $id_type_course])
                ->join('ls_m_episode b', 'a.id_episode = b.id', 'LEFT')->get();
                $data['get_pdf_preview'] = base_url('users/'.$this->class.'/preview_materi');
                $html = $this->load->view($this->view.'resource', $data,true);
                break;
            case 'task':
                $data['task'] = $this->master_model->data('a.*, c.nilai, c.id as id_user_task','ls_m_tugas_episode a', ['a.id_episode' => $id_episode, 'b.id_course' => $id_course, 'b.id_type_course' => $id_type_course])
                ->join('ls_m_episode b', 'a.id_episode = b.id', 'LEFT')
                ->join('ls_t_user_task c', 'a.id = c.id_tugas AND c.id_user = '.$this->session->userdata('id'), 'LEFT')
                ->get();
                // dd($data->result());
                $data['id_course']  = $id_course;
                $data['id_type_course']  = $id_type_course;
                $data['id_episode']  = $id_episode;
                $data['get_pdf_preview'] = base_url('users/'.$this->class.'/preview_tugas');
                $data['get_pdf_preview_user'] = base_url('users/'.$this->class.'/preview_user_task');
                $html = $this->load->view($this->view.'task', $data,true);
                break;
            case 'discuss':
                $data['komentar'] = $this->master_model->data('a.*, b.nama_depan, b.nama_belakang, b.avatar, b.jk, c.nilai','ls_t_komentar_episode a', ['a.id_episode' => $id_episode])
                ->join('ls_m_user b', 'a.id_user = b.id', 'LEFT')
                ->join('ls_t_rating_episode c', 'a.id_user = c.id_user and a.id_episode = c.id_episode', 'LEFT')
                ->order_by('a.tanggal', 'DESC')->get()->result_array();
                $data['id_episode'] = $id_episode;
                $data['save_rating'] = base_url().'users/'.$this->class.'/save_rating';
                $data['akses_komen'] = $this->master_model->check_data(['id_user' => $this->session->userdata('id'), 'id_episode' => $id_episode], 'ls_t_rating_episode');
                $html = $this->load->view($this->view.'discuss2', $data,true);
                break;
            case 'announcement':
                $html = $this->load->view($this->view.'announcement', $data,true);
                break;
            case 'pricing':
                $judul = $this->master_model->data('judul', 'ls_m_course', ['id' => $id_course])
                                  ->get()->row_array();
                $data['judul'] = $judul['judul'];
                $data['pricing'] = $this->master_model->data('a.harga, b.type_course', 'ls_m_harga_course a', ['id_course' => $id_course])
                                  ->join('ls_m_type_course b', 'a.id_type_course = b.id')
                                  ->get()->result_array();
                $html = $this->load->view($this->view.'pricing', $data,true);
                break;
            case 'quiz':
                $id_episode             = $this->input->post('id_episode');
                $idUser                 = $this->session->userdata('id');
                $data['courses']        = $this->master_model->owned_quiz($idUser, $id_episode);
                $getRoom                = $this->room_model->show(['a.id_user' => $this->session->userdata('id'), 'b.tipe' => 'quiz']);
                $totalInRoom            = $getRoom->num_rows();
                $data['room']           = ($totalInRoom > 0) ? $getRoom->row()->id_exam : 'none';
                $data['href']           = base_url().'users/users_learn/learn/'.encode_url($id_course).'/'.encode_url($id_type_course).'/'.encode_url($id_episode);
                $html = $this->load->view($this->view.'quiz', $data, true);
              break;
            default:
                break;
        }
        echo json_encode($html);
    }

    public function preview_materi(){
        $id = $this->input->post('id');
        $data = $this->master_model->data('file', 'ls_m_sumber_episode', ['id' => $id])->get()->row();
        $msg = ['status'=> FALSE, 'msg'=>'Gagal membuka pdf', 'url'=> ''];
        if (!empty($data)) {
            $msg = ['status'=> TRUE, 'msg'=>'Berhasil membuka pdf', 'url'=> base_url().$data->file];
        }

        echo json_encode($msg);
    }

    public function preview_tugas(){
        $id = $this->input->post('id');
        $data = $this->master_model->data('file', 'ls_m_tugas_episode', ['id' => $id])->get()->row();
        $msg = ['status'=> FALSE, 'msg'=>'Gagal membuka pdf', 'url'=> ''];
        if (!empty($data)) {
            $msg = ['status'=> TRUE, 'msg'=>'Berhasil membuka pdf', 'url'=> base_url().$data->file];
        }

        echo json_encode($msg);
    }

    public function preview_user_task(){
        $id = $this->input->post('id');
        $data = $this->master_model->data('file', 'ls_t_user_task', ['id' => $id])->get()->row();
        $msg = ['status'=> FALSE, 'msg'=>'Gagal membuka pdf', 'url'=> ''];
        if (!empty($data)) {
            $msg = ['status'=> TRUE, 'msg'=>'Berhasil membuka pdf', 'url'=> base_url().'uploads/user_task/'.$data->file];
        }

        echo json_encode($msg);
    }

    public function user_upload_task()
    {
      $this->load->helper('url', 'form');
      $this->load->model('admin/Task_model');
    
      $id_course        = $this->input->post('id_course');
      $id_tugas         = $this->input->post('id_tugas');
      $id_type_course   = $this->input->post('id_type_course');
      $id_episode       = $this->input->post('id_episode');
      $no               = $this->input->post('no');
      $id_user          = $this->session->userdata("id");
      $name_file        = 'User_Task_'.$id_user.'_'.$no.'_'.$id_course.'_'.$id_type_course.'_'.$id_episode;

      $config['upload_path']          = './uploads/user_task';
      $config['allowed_types']        = 'pdf|doc|docx|xls|xlsx';
      $config['max_size']             = 1024;
      $config['file_name']            = $name_file;

      $this->load->library('upload', $config);

      if (!$this->upload->do_upload('file'))
      {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata("error",$error['error']);
        redirect($_SERVER['HTTP_REFERER']);
      }
      else
      {
        $ext  = $this->upload->data('file_ext'); 
        $data = array('upload_data' => $this->upload->data());
        $data_store = [
          'id_user' => $this->session->userdata('id'),
          'id_course'  => $this->input->post('id_course'),
          'id_type_course'  => $this->input->post('id_type_course'),
          'id_episode'  => $this->input->post('id_episode'),
          'file'  => $name_file.$ext,
          'catatan' => '-',
          'id_tugas'  => $id_tugas
        ];

        $store  = $this->Task_model->store($data_store);
        $this->session->set_flashdata("success","Task assignment has been sent!");
        redirect($_SERVER['HTTP_REFERER']);
      }
    }

    public function save_rating(){
      $conf = array(
              array('field' => 'msg', 'label' => 'Message', 'rules' => 'trim|required'),
          );

      $this->form_validation->set_rules($conf);
      $this->form_validation->set_message('required', '%s can not be empty.');

      if ($this->form_validation->run() === FALSE) {
        $respones['status'] = 404;
        $response['pesan']  = validation_errors();
      }else {
        if (!empty($this->input->post('rating'))) {
          $rating = array(
            'id_user' => $this->session->userdata('id'),
            'id_episode' => $this->input->post('id_episode'),
            'nilai' => $this->input->post('rating'),
          );
          $this->master_model->save($rating, 'ls_t_rating_episode');
          $check_rating = $this->master_model->check_data(['id_episode' => $this->input->post('id_episode')], 'ls_t_rating_episode_convert');
          if ($check_rating) {
            $get_rating = $this->master_model->data('AVG(nilai) as convert_nilai', 'ls_t_rating_episode', ['id_episode' => $this->input->post('id_episode')])->get()->row();
            $avg = round($get_rating->convert_nilai, 2);
            $this->master_model->update(['nilai' => $avg], ['id_episode' => $this->input->post('id_episode')], 'ls_t_rating_episode_convert');
          }else{
            $rating_convert = array(
              'id_episode' => $this->input->post('id_episode'),
              'nilai' => $this->input->post('rating'),
            );
            $this->master_model->save($rating_convert, 'ls_t_rating_episode_convert');
          }
        }
        $komen = array(
          'id_user' => $this->session->userdata('id'),
          'id_episode' => $this->input->post('id_episode'),
          'komentar' => $this->input->post('msg'),
        );
        $rating = $this->master_model->save($komen, 'ls_t_komentar_episode');
        if ($rating) {
          $response['pesan'] = 'Thank you for giving your comments and ratings.';
          $response['proses'] = 'success';
        }else{
          $response['pesan'] = 'Comments and ratings cannot be saved.';
          $response['proses'] = 'failed';
        }
      }
        echo json_encode($response);
    }

    public function convert_array($array) {
      if (!is_array($array)) {
        return [];
      }
      $result = array();
      foreach ($array as $key => $value) {
        $result[$key] = $value['id'];
      }
      return $result;
    }

    public function getPagination($id = '',$id_type_course='', $id_episode=''){
      $id_encrypt = encode_url($id);
      $id_type_encrypt = encode_url($id_type_course);
      $next_url = $prev_url = $next_judul = $prev_judul = '';
      if ($id_episode=='') {
        $min_order = get_min('order', 'ls_m_episode', ['id_course' => $id, 'id_type_course' => $id_type_course, 'status' => 2]);
        $get_id_episode = $this->master_model->data('id', 'ls_m_episode', ['order' => $min_order, 'id_course' => $id, 'id_type_course' => $id_type_course])->get()->row();
        $id_episode = $get_id_episode->id;
        $cond = ['id_course'=>$id,'id_type_course' => $id_type_course, 'id'=>$id_episode, 'status' => 2];
      }else{
        $cond = ['id_course'=>$id, 'id_type_course' => $id_type_course, 'id'=>$id_episode, 'status' => 2];
      }
      $order   = $this->master_model->data('order','ls_m_episode',$cond)->get()->row();
      if (!empty($order)) {
        $prev_order = get_max('order', 'ls_m_episode', ['id_course' => $id, 'id_type_course' => $id_type_course, 'status' => 2, 'order <'=> $order->order]);
        $next_order = get_min('order', 'ls_m_episode', ['id_course' => $id, 'id_type_course' => $id_type_course, 'status' => 2, 'order >'=> $order->order]);
        $prev = $this->master_model->data('id,judul','ls_m_episode',['id_course'=>$id,'id_type_course' => $id_type_course,'order'=> $prev_order, 'status' => 2])->get()->row();
        $next = $this->master_model->data('id,judul','ls_m_episode',['id_course'=>$id,'id_type_course' => $id_type_course,'order'=> $next_order, 'status' => 2])->get()->row();
        if(isset($prev->id) && !empty($prev->id)){
          $idprev_encript = encode_url($prev->id);
          $prev_judul = $prev->judul;
          $prev_url = base_url('users/users_learn/learn/'.$id_encrypt.'/'.$id_type_encrypt.'/'.$idprev_encript);
        }
        if(isset($next->id) && !empty($next->id)){
          $idnext_encript = encode_url($next->id);
          $next_judul = $next->judul;
          $next_url = base_url('users/users_learn/learn/'.$id_encrypt.'/'.$id_type_encrypt.'/'.$idnext_encript);
        }
      }else{
        $next_url = '';
        $prev_url = '';
        $next_judul = '';
        $prev_judul = '';
      }
      return array('nextUrl' => $next_url,'prevUrl'=> $prev_url,'prevTitle'=> $prev_judul,'nextTitle'=> $next_judul);
    }

    public function create_room($idCourse, $status, $idEpisode = '')
    {
        if ($status == 'n')
        {
            $getExam                    = $this->exam_model->where(['id_course' => $idCourse, 'tipe' => 'quiz', 'id_episode' => $idEpisode]);
            $dataRoom                   = [
                'id_user'               => $this->session->userdata('id'),
                'id_exam'               => $getExam->id_exam,
                'id_course'             => $idCourse,
                'start_from'            => NULL,
                'start_minute'          => $getExam->waktu
            ];
            $storeRoom                  = $this->room_model->store($dataRoom);

            $getRandomQuestions         = $this->questions_model->get_random(['id_exam' => $getExam->id_exam], $getExam->jumlah_soal_dipakai);
            $stringQuery                = "INSERT INTO ls_t_user_answer (id_user, id_exam, id_question, id_answer, status, tipe) VALUES ";
            $length                     = $getRandomQuestions->num_rows();
            $dataQuestions              = $getRandomQuestions->result_array();

            for($i=0; $i<$length; $i++)
            {
                if ($i == $length-1)
                {
                    $stringQuery            .= "(".$this->session->userdata('id').",".$getExam->id_exam.",".$dataQuestions[$i]['id'].",0,'draft','quiz');";
                } else
                {
                    $stringQuery            .= "(".$this->session->userdata('id').",".$getExam->id_exam.",".$dataQuestions[$i]['id'].",0,'draft','quiz'),";
                }
            }
            $insertUserQuestions        = $this->questions_model->store_user_questions($stringQuery);
        }

        $token                          = encrypt_string('strting_exm_slmt-'.$idCourse.'-'.$this->session->userdata('id').'-'.$idEpisode);

        redirect ('start-exam/'.$token.'/quiz');
    }

    public function progres_watch()
    {
      $id_course = $this->input->post('id_course');
      $id_type_course = $this->input->post('id_type_course');
      $id_episode = $this->input->post('id_episode');
      $data_nonton = array(
        'id_episode' => $id_episode,
        'status_watch' => 2,
        'id_user' => $this->session->userdata('id'),
      );
      $check_watch = $this->master_model->check_data($data_nonton, 'ls_t_progress_watch');
      if ($check_watch == FALSE) {
        $this->master_model->save($data_nonton, 'ls_t_progress_watch');
      }

      $total_vid = $this->master_model->data('count(id) as total_vid', 'ls_m_episode', ['id_course' => $id_course, 'id_type_course' => $id_type_course, 'status' => 2])->get()->row();
      $total_watch_vid = $this->master_model->data('count(a.id_episode) as total_watch_vid', 'ls_t_progress_watch a', ['a.id_user' => $this->session->userdata('id'), 'b.id_type_course' => $id_type_course, 'b.id_course' => $id_course, 'b.status' => 2])
                        ->join('ls_m_episode b', 'b.id = a.id_episode', 'LEFT')->get()->row();
      $progres_vid = ($total_watch_vid->total_watch_vid/$total_vid->total_vid)*100;
      $progres_vid = round($progres_vid,1);

      $quiz = $this->master_model->data('count(a.id) as progres_exam', 'ls_t_after_exam a', ['a.status' => 'passed','a.id_user' => $this->session->userdata('id'), 'b.id_type_course' => $id_type_course, 'b.id_course' => $id_course])
              ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')->get()->row();
      $progres_quiz = ($quiz->progres_exam/$total_vid->total_vid)*100;
      $progres_quiz = round($progres_quiz,1);

      $progres_all = (($progres_vid+$progres_quiz)/2);
      $progres_all = round($progres_all, 1);

      $progres = [
        'total_progres' => $progres_all,
        'total_watch' => $progres_vid,
      ];

      echo json_encode($progres);
    }

    public function persen_video()
    {
      $check = $this->master_model->data('persentase, waktu', 'ls_t_progress_watch', ['id_episode' => $this->input->post('id_episode'), 'id_user' => $this->session->userdata('id')])->get()->row();
      if (!empty($check)) {
        if ($this->input->post('persentase') > $check->persentase) {
          $this->master_model->update(['persentase' => $this->input->post('persentase'), 'waktu' => $this->input->post('waktu')], ['id_episode' => $this->input->post('id_episode'), 'id_user' => $this->session->userdata('id')], 'ls_t_progress_watch');
        }
      }else{
        $this->master_model->save(['id_episode' => $this->input->post('id_episode'), 'id_user' => $this->session->userdata('id'), 'persentase' => $this->input->post('persentase'), 'waktu' => $this->input->post('waktu')], 'ls_t_progress_watch');
      }
      $data = $this->master_model->data('persentase', 'ls_t_progress_watch', ['id_episode' => $this->input->post('id_episode'), 'id_user' => $this->session->userdata('id')])->get()->row();
      $video = $this->master_model->data('sum(persentase) as persentase', 'ls_t_progress_watch a', ['b.id_course' => $this->input->post('id_course'), 'a.id_user' => $this->session->userdata('id')])
      ->join('ls_m_episode b', 'a.id_episode = b.id', 'LEFT')->get()->row();
      $get_episode = $this->master_model->data('id','ls_m_episode',['id_course'=>$this->input->post('id_course'), 'id_type_course' => $this->input->post('id_type_course'), 'status' => 2])
      ->get()->result();
      // $quiz = $this->master_model->data('count(a.id) as progres_quiz', 'ls_t_after_exam a', ['a.status' => 'passed','a.id_user' => $this->session->userdata('id'), 'b.id_type_course' => $id_type_course, 'b.id_course' => $id_course, 'tipe' => 'quiz'])
      // ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')->get()->row();
      // $persen_quiz = round(($quiz->progres_quiz/$total_episode)*100);
      $persen_vid = round($video->persentase/count($get_episode));
      // $progres_all = ($persen_quiz+$persen_vid)/2;
      $response = array(
        'persentase' => $data->persentase,
        'persentase_all' => $persen_vid,
      );
      echo json_encode($response);
    }
    function report(){
      $id_user    = $this->session->userdata('id');
      $id_episode = $this->input->post('id_episode');
      $id_course  = $this->input->post('id_course');
      $report     = $this->input->post('report');
      $report_desc  = $this->input->post('report_desc');
      $data = [
          'id_user'    => $id_user,
          'id_episode' => $id_episode,
          'id_course'  => $id_course,
          'report'     => json_encode($report),
          'report_desc' => $report_desc,
          'created_at' => date("Y-m-d H:i:s"),
      ];
      $this->master_model->delete(['id_user'=>$id_user,'id_course'=>$id_course,'id_course'=>$id_course],'ls_t_report');
      $insert = $this->master_model->save($data,'ls_t_report');
      if ($insert) {
        $data_modul = $this->master_model->data('a.judul as judul_episode ,b.judul as judul_course', 'ls_m_episode a', ['a.id' => $id_episode])->join('ls_m_course b', 'a.id_course = b.id and b.id="'.$id_course.'"', 'LEFT')
        ->get()->row();

        sendNotification([
          'url' => base_url('admin/course'),
          'to_role_id' => 1, // Role Admin
          'message' => 'Reported :  Course ' . $data_modul->judul_course . ' for Module ' . $data_modul->judul_episode,
          'module' => $this->class,
        ]);

        $response['pesan'] = 'Thank you, report has been sent .';
        $response['proses'] = 'success';
      }else{
        $response['pesan'] = 'Report Failed to sent.';
        $response['proses'] = 'failed';
      }
      echo json_encode($response);
    }

    public function relatedcourse($id){
      $this->load->library('curl');
      
      // $jsonDataEncoded = json_encode($data);  
      // dd($jsonDataEncoded);      
      $this->curl->create('http://localhost:8888/main?id_judul='.$id.'&kode='.env('APP_ALIAS')); 

      // Option
      $this->curl->option(CURLOPT_HTTPHEADER, array(            
          'Content-type: application/json; Charset=UTF-8'
      ));    

      // Post - If you do not use post, it will just run a GET request
      // $this->curl->post($jsonDataEncoded);  
            
      // Execute - returns responce
      
      $result = $this->curl->execute();
      $res = json_decode($result);
      if (isset($res))
      {
        $get_owned = $this->master_model->owned_id($this->session->userdata('id'));
        $array = array_unique(array_column($get_owned, 'id'));
        $diff  = array_diff($res->response, $array);
        $status = 'cb';
        
      } else
      {
        $get_kategori = $this->master_model->data('id_kategori', 'ls_t_course_kategori_detail', ['id_course' => $id])->get()->result_array();
        if(count($get_kategori) > 0){
          $diff =  array_column($get_kategori, 'id_kategori');
        }else{
          $diff = [];
        }
        $status = 'ct';
      }
      return ['ids' => $diff, 'st' => $status];
    }

}
