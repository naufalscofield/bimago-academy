<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Users_checkout extends MY_Controller {
  public $view    = 'users/users_checkout/';
	public function __construct(){
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET,POST,PUT, OPTIONS');
    header('Access-Control-Allow-Headers: X-Requested-With');
		parent::__construct();
    // if ($this->session->userdata('id')) {
    //   if ($this->session->userdata('role') == 'admin') {
    //     redirect('admin');
    //   }elseif ($this->session->userdata('role') == 'lecturer') {
    //     redirect('admin');
    //   }
    // }else{
    //   redirect('login');
    // }

    $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
    $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

    $signer     = new RS256Signer($privateKey);
    $verifier   = new RS256Verifier($publicKey);

    // Parse the token
    $parser = new Parser($verifier);

    if ($this->session->userdata('id'))
    {
      if ($this->session->userdata('jwt'))
      {
        $claims = $parser->parse(($this->session->userdata('jwt')));

        if ($claims['role'] != 'user')
        {
          $data['base_url']           = base_url();
          redirect('handling/not-authorized');
        }
      }
    } else
    {
        redirect('login');
    }

    $this->load->database();
	  $this->load->model('users/users_checkout_model','model');
	  $this->load->model('users/users_cart_model','cart_model');
	  $this->load->model('users/users_course_model','course_model');
	  $this->load->model('lecturer/lecturer_saldo_model','saldo_model');
	  $this->load->model('admin/cut_off_model','cut_off_model');
	  $this->load->model('admin/voucher_model','voucher_model');

	}

	public function index(){
    $total = $this->master_model->cart_total($this->session->userdata('id'));
    $voucher  = $this->session->userdata('voucher');
    
    if ($voucher != NULL)
    {
      $discount = $voucher/100;
      $cut      = $total*$discount;
      $total    = $total-$cut;
      $total    = ($total < 0) ? 0 : $total;
    }

    $result['cart'] = $this->master_model->cart_contents($this->session->userdata('id'));
    $result['total_label'] = rupiah($total);
    $result['total']       = $total;
    $result['checkout'] = base_url().'users/users_checkout/order';
    $result['class'] = $this->class;
		$this->load_template_users('users/template', $this->view.'display', $result);
    $total = $this->master_model->cart_total($this->session->userdata('id'));
	}

  public function order(){
    if (empty($this->master_model->cart_contents($this->session->userdata('id')))) {
      $response["pesan"] = 'Cannot checkout, shopping cart is still empty!';
      $response["proses"] = 'failed';
    }else{
      $date = new DateTime();
      $date = $date->format(date('d-m-Y'));
      $no = $this->random_number(3).'0'.$this->session->userdata('id').$date;
      $no = str_replace('-', '', $no);

      if ($this->session->userdata('voucher')) 
      {
        $voucher = $this->session->userdata('voucher') / 100;
        $cut      = $voucher *  $this->master_model->cart_total($this->session->userdata('id'));
        $cart_total =  $this->master_model->cart_total($this->session->userdata('id')) - $cut;
      } else
      {
        $cart_total =  $this->master_model->cart_total($this->session->userdata('id'));
      }

      // $is_processed = $this->model->order($no, $this->master_model->cart_total($this->session->userdata('id')));
      $is_processed = $this->model->order($no,$cart_total);
      if($is_processed){
        $this->master_model->cart_destroy($this->session->userdata('id'));
        $response["pesan"] =  'The order has been successfully processed, please make a payment.';
        $response["proses"] = 'success';
        $response["url"] = base_url().'users/users_mycourse/index/history';
      }else{
        $response["pesan"] = 'Failed to process your order, please try again!';
        $response["proses"] = 'failed';
      }
    }
    echo json_encode($response);
  }

  public function add_to_free(){
    $data = $this->master_model->data('*', 'ls_m_course', ['id' => $this->input->post('id')])->get()->row();
    $email = $this->master_model->data('email', 'ls_m_user', ['id' => $this->session->userdata('id')])->get()->row();
    $pembelian = array(
      'id_user' => $this->session->userdata('id'),
      'id_course' => $this->input->post('id'),
    );
    $check_pembelian = $this->master_model->check_data(['id_user' => $this->session->userdata('id'), 'id_course' => $this->input->post('id')], 'ls_t_pembelian');
    if ($check_pembelian) {
      if ($data->pelaksanaan > date('Y-m-d')) {
        $response["pesan"] =  'You have registered, wait for the course to be implemented.';
        $response["proses"] = 'success';
      }else{
        $response["pesan"] =  'The training has entered your shopping history.';
        $response["proses"] = 'success';
        $response["url"] = base_url().'users/users_learn/learn/'.encode_url($this->input->post('id'));
      }
    }else{
      $processed = $this->master_model->save($pembelian, 'ls_t_pembelian');
      $detail_processed = $this->master_model->save($pembelian, 'ls_t_detail_pembelian');
      if ($data->pelaksanaan > date('Y-m-d')) {
        $this->email($data, $email->email);
        if($processed && $detail_processed){
          $response["pesan"] =  'Thank you for joining, we have emailed the zoom link and password. Attendance is expected on the stated schedule.';
          $response["proses"] = 'success';
        }else{
          $response["pesan"] = 'Failed to register, please try again!';
          $response["proses"] = 'failed';
        }
      }else{
        if($processed && $detail_processed){
          $response["pesan"] =  'The training is entered into your shopping history.';
          $response["proses"] = 'success';
          $response["url"] = base_url().'users/users_learn/learn/'.$this->input->post('id');
        }else{
          $response["pesan"] = 'The training failed to be entered into your shopping history.';
          $response["proses"] = 'failed';
        }
      }
    }
    echo json_encode($response);
  }

  function random_number($length){
    $str        = "";
    $characters = '123456789';
    $max        = strlen($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
  }

  public function email($data, $email) {
    $message = $this->template_email($data);
    $this->load->library('email', $this->config->item('email_config'));
    $this->email->from('info@solmit.academy', env('APP_NAME'));
    $this->email->to($email);
    $this->email->subject('Link dan password Zoom Pelatihan '.$data->judul);
    $this->email->message($message);
    if ($this->email->send())
    {
      return true;
    } else
    {
      return false;
    }
  }

  public function template_email($dataset){
    $biodata = $this->master_model->data('*', 'ls_m_user', ['id'=>$this->session->userdata('id')])->get()->row();
    $data = [
      'judul'  => $dataset->judul,
      'pelaksanaan'  => $dataset->pelaksanaan,
      'link_zoom'  => $dataset->link_zoom,
      'pass_zoom'  => $dataset->pass_zoom,
      'name'  => $biodata->nama_depan,
      'base'  => base_url()
    ];

    return $this->load->view($this->view.'template_email',$data,true);
  }

  public function token()
  {
    $cart = $this->master_model->cart_contents($this->session->userdata('id'));
    $total = $this->master_model->cart_total($this->session->userdata('id'));
    $voucher  = $this->session->userdata('voucher');

    if ($voucher != NULL)
    {
      $discount = $voucher/100;
      $cut      = $total*$discount;
      $total    = $total-$cut;
      $total    = ($total < 0) ? 0 : $total;
    }

    if (!empty($cart)) {
      $date = new DateTime();
      $date = $date->format(date('d-m-Y'));
      $no = $this->random_number(3).'0'.$this->session->userdata('id').$date;
      $no = str_replace('-', '', $no);

      $params = array('server_key' => 'Mid-server-Zv30B2eTAF20Ilp8IPIVg9k0', 'production' => true);
      $this->load->library('midtrans');
      $this->midtrans->config($params);
      $this->load->helper('url');

      // Required
     $transaction_details = array(
      'order_id' => $no,
      'gross_amount' => $total, // no decimal allowed for creditcard
    );

    foreach ($cart as $value) {
       $item_details[] =  array(
          'id' => 'a1',
          'price' => $value['price'],
          'quantity' => 1,
          'name' => makeInitials($value['name']) . ' - ' . makeTypes($value['id_type_course'])
        );
    }

    if ($voucher != NULL)
    {
      $item_details[] =  array(
        'id' => 'V1',
        'price' => -$cut,
        'quantity' => 1,
        'name' => 'Kode Voucher '.$this->session->userdata('kode_voucher')
      );
    }
    
    $user_data =  $this->master_model->data('*','ls_m_user', ['id' => $this->session->userdata('id')])->get()->row();
    $customer_details = array(
      'first_name'    => $user_data->nama_depan,
      'last_name'     => $user_data->nama_belakang,
      'email'         => $user_data->email,
      'phone'         => $user_data->no_hp,
      // 'billing_address'  => $billing_address,
      // 'shipping_address' => $shipping_address
    );

    // Data yang akan dikirim untuk request redirect_url.
        $credit_card['secure'] = true;
        //ser save_card true to enable oneclick or 2click
        //$credit_card['save_card'] = true;

        $time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'minute',
            'duration'  => 120
        );

        $transaction_data = array(
            'transaction_details'=> $transaction_details,
            'item_details'       => $item_details,
            'customer_details'   => $customer_details,
            'credit_card'        => $credit_card,
            'expiry'             => $custom_expiry
        );

      error_log(json_encode($transaction_data));
      $snapToken = $this->midtrans->getSnapToken($transaction_data);
      error_log($snapToken);
      echo $snapToken;

      }
    }

    public function finish()
    {
      $result = json_decode($this->input->post('result_data'));
      $pembayaran = array(
        'id_user' => $this->session->userdata('id'),
        'no_pesanan' => $result->order_id,
        'total' => $result->gross_amount,
        'status' => 0,
        'metode_pembayaran' => $result->payment_type,
        'id_voucher'	=> NULL 
        // 'id_voucher'	=> ($this->session->userdata('id_voucher') != NULL) ? $this->session->userdata('id_voucher') : NULL 
      );

      $this->db->insert('ls_t_pembayaran',$pembayaran);
      $invoice_id = $this->db->insert_id();

      foreach($this->master_model->cart_contents($this->session->userdata('id')) as $item){
        if ($item['price'] != 0) {
          $check = $this->master_model->data('id', 'ls_t_pembelian', ['id_user' => $this->session->userdata('id'), 'id_course' => $item['id_course']])->get()->row_array();
          if (empty($check)) {
            $data = array(
              'id_user' => $this->session->userdata('id'),
              'id_course' => $item['id_course'],
            );
            $this->db->insert('ls_t_pembelian',$data);
            $pembelian_id = $this->db->insert_id();
            $type = json_decode($item['id_type_course']);
            foreach ($type as $key => $value) {
              if ($value != 0) {
                $data_detail = array(
                  'id_pembayaran' => $invoice_id,
                  'id_user' => $this->session->userdata('id'),
                  'id_type_course' => $value,
                  'harga' => $item['price'],
                  'id_course' => $item['id_course'],
                );
                $this->db->insert('ls_t_detail_pembelian', $data_detail);

                //Tambahan insert saldo
                // $get_potongan_platform  = $this->cut_off_model->show(['kode' => 'bee_lms'])->row();
                // if ($get_potongan_platform->jenis == 'persentase')
                // {
                //   $potongan_platform  = ($get_potongan_platform->potongan / 100) * $item['price'];
                // } else
                // {
                //   $potongan_platform  = $get_potongan_platform->potongan;
                // }

                // $get_potongan_payment_gateway  = $this->cut_off_model->show(['kode' => $result->payment_type])->row();
                // if ($get_potongan_payment_gateway->jenis == 'persentase')
                // {
                //   $potongan_payment_gateway  = ($get_potongan_payment_gateway->potongan / 100) * $item['price'];
                // } else
                // {
                //   $potongan_payment_gateway  = $get_potongan_payment_gateway->potongan;
                // }

                // $total_potongan = (int)$potongan_platform + (int)$potongan_payment_gateway;

                // $id_lecturer = $this->course_model->show(['id' => $item['id_course']])->row()->kontributor;
                // $check_saldo = $this->saldo_model->show(['id_lecturer' => $id_lecturer])->row();

                // if ($check_saldo == NULL)
                // {
                //   $data_saldo = [
                //     'id_lecturer' => $id_lecturer,
                //     'saldo'       => (int)$item['price'] - (int)$total_potongan
                //   ];

                //   $insert_saldo = $this->saldo_model->store($data_saldo);
                // } else
                // {
                //   $curr_saldo = $check_saldo->saldo;

                //   $data_saldo = [
                //     'id_lecturer' => $id_lecturer,
                //     'saldo'       => (int)$curr_saldo + ((int)$item['price'] - (int)$total_potongan),
                //   ];

                //   $update_saldo = $this->saldo_model->update(['id_lecturer' => $id_lecturer],$data_saldo);
                // }
                //Akhir insert saldo

              }
            }
          }else{
            $type = json_decode($item['id_type_course']);
            foreach ($type as $key => $value) {
              if ($value != 0) {
                $data_detail = array(
                  'id_pembayaran' => $invoice_id,
                  'id_user' => $this->session->userdata('id'),
                  'id_type_course' => $value,
                  'harga' => $item['price'],
                  'id_course' => $item['id_course'],
                );
                $this->db->insert('ls_t_detail_pembelian', $data_detail);
                
                //Tambahan insert saldo
                // $get_potongan_platform  = $this->cut_off_model->show(['kode' => 'bee_lms'])->row();
                // if ($get_potongan_platform->jenis == 'persentase')
                // {
                //   $potongan_platform  = ($get_potongan_platform->potongan / 100) * $item['price'];
                // } else
                // {
                //   $potongan_platform  = $get_potongan_platform->potongan;
                // }

                // $get_potongan_payment_gateway  = $this->cut_off_model->show(['kode' => $result->payment_type])->row();
                // if ($get_potongan_payment_gateway->jenis == 'persentase')
                // {
                //   $potongan_payment_gateway  = ($get_potongan_payment_gateway->potongan / 100) * $item['price'];
                // } else
                // {
                //   $potongan_payment_gateway  = $get_potongan_payment_gateway->potongan;
                // }

                // $total_potongan = (int)$potongan_platform + (int)$potongan_payment_gateway;

                // $id_lecturer = $this->course_model->show(['id' => $item['id_course']])->row()->kontributor;
                // $check_saldo = $this->saldo_model->show(['id_lecturer' => $id_lecturer])->row();

                // if ($check_saldo == NULL)
                // {
                //   $data_saldo = [
                //     'id_lecturer' => $id_lecturer,
                //     'saldo'       => (int)$item['price'] - (int)$total_potongan
                //   ];

                //   $insert_saldo = $this->saldo_model->store($data_saldo);
                // } else
                // {
                //   $curr_saldo = $check_saldo->saldo;

                //   $data_saldo = [
                //     'id_lecturer' => $id_lecturer,
                //     'saldo'       => (int)$curr_saldo + ((int)$item['price'] - (int)$total_potongan),
                //   ];

                //   $update_saldo = $this->saldo_model->update(['id_lecturer' => $id_lecturer],$data_saldo);
                // }
                //Akhir insert saldo
              }
            }
          }
        }
      }
      $this->master_model->cart_destroy($this->session->userdata('id'));
      $response["pesan"] =  'The order has been successfully processed, please make a payment.';
      $response["proses"] = 'success';
      redirect(base_url().'users/users_mycourse/index/history','refresh') ;
    }

    public function voucher()
    {
      $id_user  = $this->session->userdata('id');
      $kode     = $this->input->post("kode_voucher");

      $check    = $this->voucher_model->show(['kode' => $kode]);
      
      if ($check->num_rows() > 0)
      {
          $data_check = $check->row();

          //Check expired
          $expired    = $data_check->expired;
          $now        = date("Y-m-d");

          if ($expired < $now)
          {
            $this->session->set_flashdata("error", "Voucher expired");
          } 

          //Check kuota
          $kuota      = $data_check->kuota;
          $terpakai   = $this->model->show_voucher_pembayaran($data_check->id);

          if ($kuota > $terpakai->num_rows())
          {
            $this->session->set_flashdata("error", "Voucher limit quota");
          }
          
          //Valid voucher
          $discount   = $data_check->discount;
          $this->session->set_userdata("id_voucher", $data_check->id);
          $this->session->set_userdata("voucher", $discount);
          $this->session->set_userdata("kode_voucher", $kode);
          $this->session->set_flashdata("success", "Success get voucher ".$discount.'%');

      } else
      {
        $this->session->set_flashdata("error", "Voucher not found");
      }
    }

    public function voucher_remove()
    {
      $this->session->unset_userdata('voucher');
      $this->session->unset_userdata('id_voucher');
      $this->session->unset_userdata('kode_voucher');

      $this->session->set_flashdata("success", "Voucher removed");
      redirect ("users/users_checkout");
    }


}
