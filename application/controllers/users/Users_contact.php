<?php

class Users_contact extends MY_Controller {
  public $view    = 'users/users_contact/';
	public function __construct(){
		parent::__construct();
    // if ($this->session->userdata('id')) {
    //   if ($this->session->userdata('role') == 'admin') {
    //     redirect('admin');
    //   }elseif ($this->session->userdata('role') == 'lecturer') {
    //     redirect('admin');
    //   }
    // }


    $this->load->database();
		$this->load->model('users/users_course_model','model');
	}

	public function index(){
    // $result['simpan'] = base_url().'users/users_contact/simpan';
    // $result['data'] = $this->master_model->data('*', 'ls_m_user', ['id' => $this->session->userdata('id')])->get()->row();
    $data['info'] = $this->master_model->data('*', 'ls_m_lms_config',['id' => 1])->get()->row();
    $data['class']  = 'Users_course';

		$this->load_template_users('users/template', $this->view.'display', $data);
	}

  public function simpan()
	{
    $conf = array(
            array('field' => 'nama', 'label' => 'Name', 'rules' => 'trim|required'),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
            array('field' => 'no_hp', 'label' => 'No Handphone', 'rules' => 'trim|required'),
            array('field' => 'pesan', 'label' => 'Message', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');

    if ($this->form_validation->run() === FALSE) {
      $respones['proses'] = 'failed';
      $response['pesan']  = validation_errors();
    }else {
      $data = array(
        'nama' => $this->input->post('nama'),
        'subjek' => $this->input->post('subjek'),
        'email' => $this->input->post('email'),
        'no_hp' => $this->input->post('no_hp'),
        'pesan' => $this->input->post('pesan'),
      );
      $update = $this->master_model->save($data, 'ls_t_message');
      $response['pesan'] = 'Gagal mengirimkan pertanyaan atau masukan.';
      $response['proses'] = 'failed';
      if ($update) {
        $response['pesan'] = 'Berhasil mengirimkan pertanyaan atau masukan, Terimakasih.';
        $response['proses'] = 'success';
      }
    }
      echo json_encode($response);
	}


}
