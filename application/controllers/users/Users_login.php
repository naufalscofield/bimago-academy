<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";
class Users_login extends MY_Controller {

  public  $google_client = NULL;
  public  $linkedin_client = NULL;
  public  $view          = 'users/users_login/';

	public function __construct(){
		parent::__construct();
    if ($this->session->userdata('id')) {
      if ($this->session->userdata('role') == 'admin') {
        redirect('admin');
      }elseif ($this->session->userdata('role') == 'lecturer') {
        if ($this->session->userdata('status') == 2) {
          redirect('lecturer');
        }else{
          redirect('lecturer/account');
        }
      }elseif ($this->session->userdata('role') == 'user') {
        redirect('welcome');
      }
    }
    ### GOOGLE API ###

    $this->google_client = new Google_Client();
    $this->google_client->setClientId('291214281997-2ua505bbq8aacv6lidoqf6pqnb7c8i1v.apps.googleusercontent.com'); //Define your ClientID
    $this->google_client->setClientSecret('F3jg4Q-PH9NnFUUPaJD4MQPU'); //Define your Client Secret Key
    $this->google_client->setRedirectUri(base_url('users/users_login/loginGoogle')); //Define your Redirect Uri
    $this->google_client->addScope('email');
    $this->google_client->addScope('profile');

    ### LINKEDIN API ###
    $this->linkedin_client = array(
       'appKey'       => '788pql4gz0tp37',
       'appSecret'    => 'uM2SWlD4rUHHEBRa',
       'callbackUrl'  => base_url().'users/users_login/loginLinkedin'
    );

		$this->load->model('users/users_login_model','model');
		$this->load->model('admin/terms_model','model_terms');
	}

	public function index(){
    ### LINKEDIN API ###
    $this->load->library('linkedin', $this->linkedin_client);

    $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
    $token = $this->linkedin->retrieveTokenRequest();
    // $this->session->set_flashdata('oauth_request_token_secret',$token['linkedin']['oauth_token_secret']);
    // $this->session->set_flashdata('oauth_request_token',$token['linkedin']['oauth_token']);

    $link = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=". $token['oauth']['string'];

    $result['check_login'] = base_url('users/users_login/check_login');
    $result['registrasi'] = base_url('users/users_login/registrasi');
    $result['login_page'] = base_url('users/users_login');
    ### GOOGLE API ###
    $result['loginGoogle'] = $this->google_client->createAuthUrl();
    $result['loginLinkedin'] = $link;
    $result['class']          = $this->class;
    $result['current_url']    = $this->session->flashdata('curent_url');

		$this->load_template_users('users/template', $this->view.'display', $result);
	}

  public function registrasi(){
    $result['registrasi'] = base_url('users/users_login/save');
    $result['login_page'] = base_url('users/users_login');
    $result['register_page'] = base_url('users/users_login/registrasi');
    $result['loginGoogle'] = $this->google_client->createAuthUrl();
    // $result['role'] = $this->master_model->data('*', 'ls_m_role', ['id' => 3])->get()->result_array();
    $result['role'] = $this->master_model->data('*', 'ls_m_role', ['id !=' => 1])->get()->result_array();
    $result['class']    = $this->class;
    $result['terms']    = $this->model_terms->get()->row();
		$this->load_template_users('users/template', $this->view.'registrasi', $result);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_m_course');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function save()
	{
    $conf = array(
        array('field' => 'nama_depan', 'label' => 'First Name', 'rules' => 'trim|required'),
        array('field' => 'nama_belakang', 'label' => 'Last Name', 'rules' => 'trim|required'),
        array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email|xss_clean|callback_unique'),
        array('field' => 'password', 'label' => 'Password', 'rules' => 'trim|required|min_length[6]|max_length[15]|callback_check_pass')
      );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s can not be empty.');

    if ($this->form_validation->run() === FALSE) {
      $respones['proses'] = 'failed';
      $response['pesan']  = strip_tags(validation_errors());
    }else{
      if ($this->input->post('password') != $this->input->post('confirm_password')) {
        $respones['proses'] = 'wrong';
        $response['pesan']  = 'Double check the password and confirm the password !';
      }else{
        $role_id = $this->input->post('role');
        if ($role_id == 2) {
          $status = 1;
        }else{
          $status = 2;
        }
        $data = array(
          'nama_depan' => $this->input->post('nama_depan') != '' ? $this->input->post('nama_depan') : NULL,
          'nama_belakang' => $this->input->post('nama_belakang') != '' ? $this->input->post('nama_belakang') : NULL,
          'email' => $this->input->post('email'),
          'no_hp' => $this->input->post('no_hp') != '' ? $this->input->post('no_hp') : NULL,
          'password' => md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($this->input->post('password'))))))))))),
          'role' => $role_id,
          'verifikasi' => 0,
          'login_via' => 'solmit',
          'status' => $status,
        );

        $stringToken  = $data['nama_depan'].$data['email'].rand(0000000001, 9999999999);
        $tokenEmail   = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($stringToken))))))))));

        $data['email_verification_token']   = $tokenEmail;
        $data['email_verification_expired'] = date("Y-m-d H:i:s", strtotime("+1 hours"));

        $this->email($this->input->post('email'), $data);
        $create = $this->model->create_data('ls_m_user', $data);
        if ($create) {

          // Jika user yang daftar ada lecturer, maka kirim notifikasi ke admin
          if ($role_id == 2) {
            $user_data = $this->master_model->data('*', 'ls_m_user', ['email' => $this->input->post('email')])->get()->row();
            sendNotification([
              'user_id' => $user_data->id,
              'url' => base_url('admin/exam'),
              'to_role_id' => 1, // Role Admin
              'message' => 'New Lecturer Registered!',
              'module' => $this->class,
            ]);
          }

          $response['pesan'] = 'Successfully registered, please verify the account in your email.';
          $response['proses'] = 'success';
          $response['url'] = base_url().'users/users_login';
        }else{
          $response['pesan'] = 'Failed to register.';
          $response['proses'] = 'failed';
        }
      }
    }
    echo json_encode($response);
  }

  public function check_pass(){
    if (1 !== preg_match("/^.*(?=.{6,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $this->input->post('password'))){
      $this->form_validation->set_message('check_pass', '%s must be at least 6 characters and must contain lower case letter, one upper case letter and one digit');
      return FALSE;
    }else{
      return TRUE;
    }
  }

  public function unique(){
    $email = $this->input->post('email');
    $check = true;
    if ($this->master_model->check_data(['email' => $email],'ls_m_user')) {
      $this->form_validation->set_message('unique', 'Email already used !');
      $check = false;
    }
    return $check;
	}

  public function sso()
  {
        $json = file_get_contents('php://input');
        $dataReq = json_decode($json);

        $email        = $dataReq->email;
        $no_hp        = $dataReq->no_hp;
        $nama_depan   = $dataReq->nama_depan;
        $nama_belakang= $dataReq->nama_belakang;
        $kode         = $dataReq->kode;

        $check = $this->master_model->data('a.id, a.verifikasi, a.login_via, a.status, b.role', 'ls_m_user a', ['email' => $email, 'kode_mitra' => $kode])
                ->join('ls_m_role b', 'a.role = b.id', 'LEFT')
                ->join('ls_m_mitra c', 'a.kode_mitra = c.kode', 'LEFT')
                ->get();

        $token = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($email))))))))));
        if ($check->num_rows() > 0)
        {
            $data   = $check->row();
            // JWT Part
            $signer = new HS256('12345678901234567890123456789012');
            $generator = new Generator($signer);
            $jwt = $generator->generate(['id' => (int)$data->id, 'role' => $data->role]);

            $data_session = array(
            'id' => $data->id,
            'role' => $data->role,
            'token' => $token,
            'access' => 'YES',
            'login_via' => $data->nama_mitra,
            'status' => $data->status,
            'jwt' => $jwt
            );

            $this->session->set_userdata($data_session);
        } else
        {
            $getMitra = $this->master_model->data('nama_mitra', 'ls_m_mitra', ['kode' => $kode])
            ->get()->row();
            $dataInsert = [
                'email' => $email,
                'password'  => NULL,
                'no_hp' => $no_hp,
                'institusi' => $getMitra->nama_mitra,
                'role'  => 3,
                'nama_depan' => $nama_depan,
                'nama_belakang' => $nama_belakang,
                'verifikasi' => 1,
                'email_verification_token' => NULL,
                'email_verification_expired'  => NULL,
                'jk'    => NULL,
                'avatar' => NULL,
                'login_via' => $getMitra->nama_mitra,
                'profil'    => NULL,
                'nama_sertifikat'   => NULL,
                'change_password_token' => NULL,
                'status'    => 2,
                'kode_mitra'     => $kode
            ];

            $insert = $this->model->create_data('ls_m_user', $dataInsert);
            // JWT Part
            $signer = new HS256('12345678901234567890123456789012');
            $generator = new Generator($signer);
            $jwt = $generator->generate(['id' => (int)$insert, 'role' => 'user']);

            $data_session = array(
            'id' => $insert,
            'role' => 'user',
            'token' => $token,
            'access' => 'YES',
            'login_via' => $getMitra->nama_mitra,
            'status' => 2,
            'jwt' => $jwt
            );

            $this->session->set_userdata($data_session);
        }
        redirect ('/');
  }

  public function check_login() {
    $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
    $userIp=$this->input->ip_address();

    $secret='6Le2G_caAAAAALlO-I0g_uodMKvBSI7cCr_UYbvc';

    $credential = array(
        'secret' => $secret,
        'response' => $this->input->post('g-recaptcha-response')
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($verify);

    $status= json_decode($res, true);
    if($status['success']){
      $email = $this->input->post('email');
      $passwordInput = $this->input->post('password');
      $password = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($passwordInput))))))))));
      $token = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($email))))))))));
      $check = $this->master_model->data('a.id, a.verifikasi, a.login_via, a.status, b.role, a.role as role_id', 'ls_m_user a', ['email' => $email, 'password' => $password])
              ->join('ls_m_role b', 'a.role = b.id', 'LEFT')->get();

      if ($check->num_rows() > 0)  {
        $data = $check->row();
        if ($data->verifikasi == 1) {
          $this->check_cart($data->id);

          // // JWT Part
          $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
          $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

          $signer     = new RS256Signer($privateKey);
          $verifier   = new RS256Verifier($publicKey);

          $generator = new Generator($signer);
          $jwt = $generator->generate(['id' => (int)$data->id, 'role' => $data->role, 'id_role' => $data->role_id]);
          // // $parser = new Parser($signer);
          // // $claims = $parser->parse($jwt);

          $data_session = array(
            'id' => $data->id,
            'role' => $data->role,
            'role_id' => $data->role_id,
            'token' => $token,
            'access' => 'YES',
            'login_via' => $data->login_via,
            'status' => $data->status,
            'jwt' => $jwt
          );

          $this->session->set_userdata($data_session);

          $response['pesan'] = 'Successfully Login.';
          $response['proses'] = 'success';
          if ($data->role == 'admin') {
            $response['url'] = base_url().'admin';
          }elseif ($data->role == 'user') {

            if ($this->input->post("redirect") != '') {
              $response['url'] = $this->input->post("redirect");
            }else{
              $response['url'] = base_url();
            }
          }elseif ($data->role == 'lecturer') {
            if ($data->status == 2) {
              $response['url'] = base_url().'lecturer';
            }else{
              $response['url'] = base_url().'lecturer/account';
            }
          }
        }else{
          $response['pesan'] = 'Your account has not been verified by email.';
          $response['proses'] = 'failed';
        }
      }else{
        $response['pesan'] = 'Wrong username or password, please check again.';
        $response['proses'] = 'failed';
      }
    }else{
      $response['pesan'] = 'Captcha Failed !';
      $response['proses'] = 'failed';
    }

    echo json_encode($response);
  }

  public function email($email, $data) {
    $message = $this->template_email($data);

    $this->load->library('email', $this->config->item('email_config'));
    $this->email->from('info@solmit.academy', env('APP_NAME'));
    $this->email->to($email);
    $this->email->subject('Konfirmasi email pendaftaran akun Bimago Academy.');
    $this->email->message($message);
    if ($this->email->send())
    {
      return true;
    } else
    {
      return false;
    }
  }

  public function template_email($dataset){
    $data = [
      'name'  => $dataset['nama_depan'],
      'link'  => base_url().'verify-email/'.$dataset['email_verification_token'],
      'base'  => base_url()
    ];

    return $this->load->view($this->view.'template_email',$data,true);
  }

  public function check_cart($id){
    $get_pembelian = $this->master_model->data('id_course', 'ls_t_pembelian', ['id_user' => $id])->get()->result_array();
    $cart = $this->cart->contents();
    if (!empty($get_pembelian) && !empty($cart)) {
      $pembelian = [];
      foreach ($get_pembelian as $key => $value) {
        $pembelian[]=$value['id_course'];
      }
      foreach ($cart as $key => $value) {
        if (in_array($value['id'], $pembelian) ) {
          $data = [
            'rowid' => $value['rowid'],
            'qty' => 0,
          ];
          $this->cart->update($data);
        }
      }
    }
  }

  public function verify_email($token)
  {
    $checkToken = $this->model->check_token('ls_m_user', $token);
    if ($checkToken == 1)
    {
      redirect ('users/home');
    } else if ($checkToken == 2)
    {
      $verifyEmail  = $this->model->verify_email('ls_m_user', $token);
      $this->load->view('utilities/redirect', ['base_url' => base_url()]);
    } else
    {
      $this->load->view('utilities/email-expired', ['token' => $token, 'base_url' => base_url()]);
    }
  }

  public function resend_email_verification()
  {
    $token      = $this->input->post('token');
    $getByToken = $this->model->show('ls_m_user', ['email_verification_token' => $token]);
    $user       = $getByToken->row();
    $data       = [
      'nama_depan'                => $user->nama_depan,
      'email_verification_token'  => $token,
      'email'                     => $user->email
    ];

    $this->update_email_token($data);

    $send       = $this->email($user->email, $data);

    if ($send)
    {
      $this->load->view('utilities/email-sent', ['base_url' => base_url()]);
    } else
    {
      $this->load->view('utilities/internal-error', ['base_url' => base_url()]);
    }
  }

  public function update_email_token($dataset)
  {
    $stringToken  = $dataset['nama_depan'].$dataset['email'].rand(0000000001, 9999999999);
    $tokenEmail   = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($stringToken))))))))));

    $data['email_verification_token']   = $tokenEmail;
    $data['email_verification_expired'] = date("Y-m-d H:i:s", strtotime("+1 hours"));

    $this->model->update_email_token('ls_m_user', $data, ['email_verification_token' => $dataset['email_verification_token']]);
  }
  function loginGoogle()
  {
    if(isset($_GET["code"]))
    {
     $token = $this->google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

     if(!isset($token["error"]))
     {
      $this->google_client->setAccessToken($token['access_token']);
      $this->session->set_userdata('access_token', $token['access_token']);
      $google_service = new Google_Service_Oauth2($this->google_client);
      $data = $google_service->userinfo->get();

      $check_email = $this->master_model->data('a.id, a.login_via, a.status, b.role, b.id as role_id' ,'ls_m_user a', ['email' => $data['email']])
                     ->join('ls_m_role b', 'a.role = b.id', 'LEFT')->get();
      if ($check_email->num_rows() > 0) {
        $row_data = $check_email->row();
        if ($row_data->role_id != 3)
        {
          $this->session->set_flashdata('pesan', $this->lang->line('gmail_only_student'));
          $this->session->set_flashdata('proses', 'error');
          redirect('users/users_login');
        }
        if ($row_data->login_via != 'solmit') {
          $this->check_cart($row_data->id);
          $token_email = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($data['email']))))))))));
          $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
          $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

          $signer     = new RS256Signer($privateKey);
          $verifier   = new RS256Verifier($publicKey);

          $generator = new Generator($signer);
          $jwt = $generator->generate(['id' => (int)$row_data->id, 'role' => $row_data->role, 'id_role' => $row_data->role_id]);
          $data_session = array(
            'id' => $row_data->id,
            'role' => $row_data->role,
            'role_id' => $row_data->role_id,
            'token' => $token_email,
            'access' => 'YES',
            'login_via' => $row_data->login_via,
            'status' => $row_data->status,
            'jwt' => $jwt
          );
          $this->session->set_userdata($data_session);
          $this->session->set_flashdata('pesan', 'Successfully Login.');
          $this->session->set_flashdata('proses', 'success');
          redirect('welcome');
        }else{
          $this->session->set_flashdata('pesan', 'You have registered at '.env('APP_NAME').', use your username and password.');
          $this->session->set_flashdata('proses', 'error');
          redirect('users/users_login');
        }
      }else{
        $data_gmail = array(
          'nama_depan' => $data['givenName'],
          'nama_belakang' => $data['familyName'],
          'email' => $data['email'],
          'no_hp' => NULL,
          'role' => 3,
          'verifikasi' => 1,
          'avatar' => $data['picture'],
          'login_via' => 'gmail',
          'status' => 2,
        );
        $simpan_gmail = $this->master_model->save($data_gmail, 'ls_m_user');
        $get_after = $this->master_model->data('a.id, a.login_via, b.role, b.id as role_id', 'ls_m_user a', ['email' => $data['email']]);
        $token_email = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($data['email']))))))))));
        $privateKey = new RsaPrivateKey(FCPATH.'/key_sec/private_key.pem');
        $publicKey  = new RsaPublicKey(FCPATH.'./key_sec/public_key.pem');

        $signer     = new RS256Signer($privateKey);
        $verifier   = new RS256Verifier($publicKey);

        $generator = new Generator($signer);
        $jwt = $generator->generate(['id' => (int)$get_after->id, 'role' => $get_after->role, 'id_role' => $get_after->role_id]);
        $data_session = array(
          'id' => $get_after->id,
          'role' => $get_after->role,
          'role_id' => $get_after->role_id,
          'token' => $token_email,
          'access' => 'YES',
          'login_via' => $get_after->login_via,
          'jwt' => $jwt
        );
        $this->session->set_userdata($data_session);
        $this->session->set_flashdata('pesan', 'Successfully Login.');
        $this->session->set_flashdata('proses', 'success');
        redirect('welcome');
      }
     }
    }
   }
  function loginLinkedin()
  {

  }

  public function forgot_password() {
    $email  = $this->input->post('forgot_email');
    $data   = $this->model->show_data('ls_m_user', ['email' => $email])->row();
    $rand   = rand(00001, 99999);
    $link   = encrypt_string("slmtacdmy-chgpwd-".$data->email."-".$rand);
    $addToken = $this->model->update_email_token('ls_m_user', ['change_password_token' => $rand], ['email' => $data->email]);
    $message = $this->template_email_forgot_password($data, $link);
    $this->load->library('email', $this->config->item('email_config'));
    $this->email->from('info@solmit.academy', env('APP_NAME'));
    $this->email->to($email);
    $this->email->subject('Konfirmasi Pergantian Password');
    $this->email->message($message);
    if ($this->email->send())
    {
      $this->session->set_flashdata('pesan', 'Please check your email. Change password link is already send to your email.');
      $this->session->set_flashdata('proses', 'success');
      redirect('login');
    } else
    {
      $this->session->set_flashdata('pesan', 'Failed to send change password link to your email, please input email again!');
      $this->session->set_flashdata('proses', 'error');
      redirect('login');
    }
  }

  public function template_email_forgot_password($dataset, $link){

    $data = [
      'data'  => $dataset,
      'link'  => base_url().'forgot-change-password/'.$link,
      'base'  => base_url()
    ];

    return $this->load->view($this->view.'template_email_forgot_password',$data,true);
  }

  public function forgot_change_password($token)
  {
      $decrypt  = decrypt_string($token);
      $exp      = explode('-', $decrypt);
      $exp3     = (isset($exp[3])) ? (int)$exp[3] : 0;
      if ( (!isset($exp[0]) || $exp[0] != 'slmtacdmy') || (!isset($exp[1]) || $exp[1] != 'chgpwd') || (!isset($exp[2]) || strpos($exp[2], '@') == false) || $exp3 == 0)
      // if ( (!isset($exp[0]) || $exp[0] != 'slmtacdmy') || (!isset($exp[1]) || exp[1] != 'chgpwd') || (!isset($exp[2]) || strpos($exp[2], '@') == false) || $exp3 == 0)
      {
        $data['base_url'] = base_url();
        return $this->load->view('utilities/404', $data);
      } else
      {
        $cekToken = $this->model->show('ls_m_user', ['email' => $exp[2], 'change_password_token' => $exp[3]]);
        if ($cekToken->num_rows() == 0)
        {
          $data['base_url'] = base_url();
          return $this->load->view('utilities/404', $data);
        } else
        {
          $data['data'] = $cekToken->row();
          $data['link'] = $token;
          $this->load->view($this->view.'template_change_password',$data);
        }
      }
  }

  public function store_forgot_change_password()
  {
    $decrypt  = decrypt_string($this->input->post('token'));
    $exp      = explode('-', $decrypt);

    $cond     = [
      'email' => $exp[2]
    ];

    $data     = [
      'password'   => md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($this->input->post('password')))))))))))
    ];

    $update_token   = $this->model->update_email_token('ls_m_user', ['change_password_token' => NULL], $cond);
    $update         = $this->model->update_email_token('ls_m_user', $data, $cond);

    if ($update && $update_token)
    {
      redirect('/');
    } else
    {
      $this->load->view('utilities/internal-error');
    }
  }

}
