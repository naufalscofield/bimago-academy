<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Exam extends MY_Controller {
    public $view    = 'users/exam/';
    public function __construct()
    {
        parent::__construct();
        checkAuthJWTStudent();

        $this->load->model('admin/exam_model', 'exam_model');
        $this->load->model('admin/questions_model', 'questions_model');
        $this->load->model('admin/episode_model', 'episode_model');
        $this->load->model('users/users_room_model', 'room_model');
    }

    public function update_checkpoint()
    {
        date_default_timezone_set("Asia/Jakarta");
        ignore_user_abort(true);
        $cond   = [
            'id_user'   => $this->input->post('id_user'),
            'id_exam'   => $this->input->post('id_exam')
        ];

        $data   = [
            'start_minute'  => $this->input->post('minute'),
        ];

        if ($this->input->post('status') == 'ul')
        {
            $data['start_from'] = date('Y-m-d H:i:s');
        }

        $this->room_model->update($data, $cond);
    }

    public function index()
    {
        $idUser                 = $this->session->userdata('id');
        $data['courses']        = $this->master_model->owned_exam($idUser);
        $data['class']          = "Users_course";
        $getRoom                = $this->room_model->show(['a.id_user' => $this->session->userdata('id'), 'b.tipe' => 'exam']);
        $totalInRoom            = $getRoom->num_rows();
        $data['room']           = ($totalInRoom > 0) ? $getRoom->row()->id_exam : 'none';

		$this->load_template_users('users/template', $this->view.'display', $data);
    }

    public function create_room($idCourse, $status)
    {
        if ($status == 'n')
        {
            date_default_timezone_set("Asia/Jakarta");
            $getExam                    = $this->exam_model->where(['id_course' => $idCourse, 'tipe' => 'exam']);
            $dataRoom                   = [
                'id_user'               => $this->session->userdata('id'),
                'id_exam'               => $getExam->id_exam,
                'id_course'             => $idCourse,
                'first_start'           => date('Y-m-d H:i:s'),
                'start_from'            => NULL,
                'start_minute'          => $getExam->waktu
            ];
            $storeRoom                  = $this->room_model->store($dataRoom);

            $getRandomQuestions         = $this->questions_model->get_random(['id_exam' => $getExam->id_exam], $getExam->jumlah_soal_dipakai);
            $stringQuery                = "INSERT INTO ls_t_user_answer (id_user, id_exam, id_question, id_answer, status, tipe) VALUES ";
            $length                     = $getRandomQuestions->num_rows();
            $dataQuestions              = $getRandomQuestions->result_array();

            for($i=0; $i<$length; $i++)
            {
                if ($i == $length-1)
                {
                    $stringQuery            .= "(".$this->session->userdata('id').",".$getExam->id_exam.",".$dataQuestions[$i]['id'].",0,'draft','exam');";
                } else
                {
                    $stringQuery            .= "(".$this->session->userdata('id').",".$getExam->id_exam.",".$dataQuestions[$i]['id'].",0,'draft','exam'),";
                }
            }

            $insertUserQuestions        = $this->questions_model->store_user_questions($stringQuery);
        }

        $token                          = encrypt_string('strting_exm_slmt-'.$idCourse.'-'.$this->session->userdata('id'));

        redirect ('start-exam/'.$token.'/exam');
    }

    public function start($token, $tipe)
    {
        $dtoken     = decrypt_string($token);
        $extoken    = explode('-', $dtoken);
        $idCourse   = (isset($extoken[1])) ? $extoken[1] : 'none';
        $idEpisode   = (isset($extoken[3])) ? $extoken[3] : 'none';
        if (strpos($dtoken, '-') == FALSE || $extoken[0] != 'strting_exm_slmt' || (int)$extoken[1] == 0 || (int)$extoken[2] == 0)
        {
            $this->load->view('utilities/404', ['base_url' => base_url()]);
        } else
        {
            if ($idCourse != 'none')
            {
                $checkRoom  = $this->room_model->show(['id_user' => $this->session->userdata('id'), 'a.id_course' => $idCourse, 'b.tipe' => $tipe]);

                if ($checkRoom->num_rows() > 0)
                {
                    $update_start_from = $this->room_model->update(['start_from' => NULL], ['id' => $checkRoom->row()->master_id]);
                    $this->session->unset_userdata('starting_exam');
                    $this->session->set_userdata('starting_exam', $this->session->userdata('id').'-'.$idCourse);
                    if ($tipe == 'quiz') {
                      $getExam                    = $this->exam_model->where(['id_course' => $idCourse, 'tipe' => $tipe, 'id_episode' => $idEpisode]);
                    }else{
                      $getExam                    = $this->exam_model->where(['id_course' => $idCourse, 'tipe' => $tipe]);
                    }
                    $questions                  = $this->questions_model->show_user_questions(['id_exam' => $getExam->id_exam, 'id_user' => $this->session->userdata('id')]);
                    $data['class']              = "Users_course";
                    $data['questions']          = $questions->result_array();
                    $data['total_question']     = $questions->num_rows();
                    $data['url_single_question']= base_url().'exam-single-question';
                    $data['id_exam']            = $getExam->id_exam;
                    $data['id_user']            = $this->session->userdata('id');
                    $data['url_get_answered']   = base_url().'exam-get-answered';
                    $data['data_exam']          = $getExam;
                    $data['data_room']          = $checkRoom->row()->start_minute;
                    $data['tipe']               = $tipe;

                    if ($tipe == 'quiz')
                    {
                        $this->episode_model->id = $getExam->id_episode;
                        $data['episode']    = $this->episode_model->get_data('ls_m_episode')->row();
                    }
                    $this->load->view('users/exam/exam', $data);
                } else
                {
                    $data['base_url']           = base_url();
                    $this->load->view('utilities/exam-forbidden', $data);
                }
            } else
            {
                $data['base_url']           = base_url();
                $this->load->view('utilities/exam-forbidden', $data);
            }
        }
    }

    public function single_question()
    {
        $idQuestion = $this->input->post('id');
        $getQuestion= $this->questions_model->show(['id' => $idQuestion]);
        $getAnswers = $this->questions_model->show_answers(['a.id_question' => $idQuestion]);
        $isAnswer   = $this->questions_model->check_answer(['a.id_user' => $this->session->userdata('id'), 'a.id_question' => $idQuestion]);

        $data       = [
            "question"      => $getQuestion->row(),
            "answers"       => $getAnswers->result_array(),
            "isAnswer"      => $isAnswer->num_rows(),
            "userAnswer"    => ($isAnswer->num_rows() > 0) ? $isAnswer->row() : ''
        ];

        $data['kunjaw'] = new stdClass();

        if ($data['question']->tipe == 'checkbox')
        {
            $data['kunjaw'] = $this->questions_model->check_kunjaw($idQuestion, false);
        }

        $response   = [
            "status"    => 200,
            "data"      => $data
        ];

        echo json_encode($response);
    }

    public function store_user_answer()
    {
        $condAnswer         = [
            'a.id_user'       => $this->session->userdata('id'),
            'a.id_exam'       => $this->input->post('id_exam'),
            'a.id_question'   => $this->input->post('id_question')
        ];

        $checkAnswerExist   = $this->questions_model->check_answer($condAnswer);
        // $nextAnswer         = $this->questions_model->next_answer(['id >' => $checkAnswerExist->row()->id_primary, 'id_user' => $this->session->userdata('id')])->result();
        // print_r($nextAnswer->result()); die;
        // var_dump($nextAnswer->row()); die;
        // var_dump($nextAnswer->row()); die;
        // var_dump($checkAnswerExist->row()); die;
        // if ($checkAnswerExist->row()->id_question_primary != NULL)
        // {
        //     $idNext             = $checkAnswerExist->row()->id_primary + 2;
        // } else
        // {
        //     $checkAnswerExist2   = $this->questions_model->check_answer(['a.id_user' => $this->session->userdata('id'),'a.id_exam'=> $this->input->post('id_exam')])->row();
        //     $idNext             = $checkAnswerExist2->id_primary;
        // }
        // $nextAnswer         = $this->questions_model->check_answer(['a.id' => $idNext]);

        $condAnswer['id_user'] = $condAnswer['a.id_user'];
        unset($condAnswer['a.id_user']);

        $condAnswer['id_exam'] = $condAnswer['a.id_exam'];
        unset($condAnswer['a.id_exam']);

        $condAnswer['id_question'] = $condAnswer['a.id_question'];
        unset($condAnswer['a.id_question']);
        
        if ($this->input->post('tipe_exam') == 'checkbox')
        {
            if ($this->input->post('id_answer') != NULL)
            {
                $dataIdAnswer   = implode(',',$this->input->post('id_answer'));
            }
            else
            {
                $dataIdAnswer   = 0;
            }
        } else
        {
            if ($this->input->post('id_answer') != NULL)
            {
                $dataIdAnswer   = $this->input->post('id_answer');
            }
            else
            {
                $dataIdAnswer   = 0;
            }
        }
        // $dataIdAnswer = ($this->input->post('tipe_exam') == 'checkbox') ? implode(',',$this->input->post('id_answer')) : $this->input->post('id_answer');

        if($checkAnswerExist->num_rows() > 0)
        {
            $dataUpdate['id_answer']    = $dataIdAnswer;

            $updateAnswer               = $this->questions_model->update_answer($dataUpdate, $condAnswer);

            $response                   = [
                'act'   => 'update',
            ];
            
            if ($updateAnswer){

                $condCheckAll         = [
                    'id_user'       => $this->session->userdata('id'),
                    'id_exam'       => $this->input->post('id_exam'),
                ];

                $checkAllIsAnswer   = $this->questions_model->check_answer_all($condCheckAll);

                $response['all_answered'] = ($checkAllIsAnswer > 0) ? 'false' : 'true';
                // var_dump($response); die;
                $response['status'] = 200;
                $response['msg']    = 'Answer updated';
                $response['is_answer'] = ($dataIdAnswer == 0) ? 'false' : 'true';
                // $response['id_question_next']    = (isset($nextAnswer[1]->id_question)) ? $nextAnswer[1]->id_question : null;
            } else
            {
                $response['status'] = 500;
                $response['msg']    = 'Failed update answer';
            }
        } else
        {
            $dataInsert                 = $condAnswer;
            $dataInsert['id_answer']    = $dataIdAnswer;
            $dataInsert['status']       = 'draft';

            $insertAnswer               = $this->questions_model->store_answer($dataInsert);

            $response                   = [
                'act'   => 'insert',
            ];

            if ($insertAnswer){
                $response['status'] = 200;
                $response['msg']    = 'Answer saved';
            } else
            {
                $response['status'] = 500;
                $response['msg']    = 'Failed save answer';
            }
        }

        echo json_encode($response);
    }

    public function finish_exam()
    {
        $id_exam        = $this->input->post('finish_id_exam');
        $status_timeout = $this->input->post('finish_status');
        $tipe_exam      = $this->input->post('tipe_exam');
        $id_user        = $this->session->userdata('id');

        if ($status_timeout == 'timeout')
        {
            $wipeUserAnswer = $this->questions_model->wipe_answers(['id_exam' => $id_exam, 'id_user' => $id_user, 'tipe' => $tipe_exam]);
        }

        $condCheck      = [
            'id_exam'   => $id_exam,
            'id_user'   => $id_user,
        ];

        $condCheck2      = [
            'id_exam'   => $id_exam,
            'id_user'   => $id_user,
        ];

        $updateStatus       = $this->questions_model->final_submit($condCheck);

        //Tambahan total waktu pengerjaan
        date_default_timezone_set("Asia/Jakarta");
        $getRoom            = $this->room_model->show($condCheck)->row();
        $waktuMulai         = $getRoom->first_start;
        $now                = date("Y-m-d H:i:s");
        $diff               = (strtotime($now) - strtotime($waktuMulai)) / 60;

        $deleteRoom         = $this->room_model->delete($condCheck);

        $condCheck['a.id_exam'] = $condCheck['id_exam'];
        unset($condCheck['id_exam']);

        $condCheck['a.id_user'] = $condCheck['id_user'];
        unset($condCheck['id_user']);

        $checkAnswer    = $this->questions_model->check_final_answer($condCheck);
        $score          = array_sum(array_column($checkAnswer->result_array(), 'score'));

        $getPassingGrade= $this->exam_model->show($id_exam);
        $passingGrade   = $getPassingGrade->passing_grade;
        $idCourse       = $getPassingGrade->id_course;

        if ($status_timeout == 'timeout')
        {
            $isPassed   = 'timeout';
        } else
        {
            $isPassed       = ((int)$score >= (int)$passingGrade) ? 'passed' : 'failed';
        }

        if ($isPassed == 'passed' && $tipe_exam == 'exam')
        {
            $this->master_model->saveTransCertificate($idCourse,$id_user,$id_exam);
        }

        $getPercobaan   = $this->exam_model->get_percobaan(['id_user' => $id_user, 'id_exam' => $id_exam])->num_rows();

        $dataAfterExam  = [
            'id_user'   => $id_user,
            'id_exam'   => $id_exam,
            'score'     => $score,
            'status'    => $isPassed,
            'percobaan' => $getPercobaan + 1,
            'total_menit'   => floor($diff)
        ];

        $storeAfterExam = $this->exam_model->store_after_exam($dataAfterExam);
        $deleteUserAnswer   = $this->questions_model->delete_user_answers($condCheck2);

        if($storeAfterExam)
        {
            if ($status_timeout == 'timeout')
            {
                $this->session->set_flashdata('status_timeout', true);
            } else
            {
                $this->session->set_flashdata('status_timeout', false);
            }

            $this->session->set_flashdata('after_exam', $id_exam);

            redirect ('after-exam');
        }
    }

    public function after_exam()
    {
        $idExam         = $this->session->flashdata('after_exam');
        $getExam        = $this->exam_model->show($idExam);
        if ($idExam)
        {
            $cond           = [
                'a.id_exam'   => $idExam,
                'a.id_user'   => $this->session->userdata('id')
            ];

            $data['data']   = $this->exam_model->get_after_exam($cond)->row();

            if ($getExam->tipe == 'quiz')
            {
                $this->episode_model->id = $getExam->id_episode;
                $data['episode'] = $this->episode_model->get_data('ls_m_episode')->row();
                $data['exam'] = $getExam;
                $this->load->view('users/exam/after_quiz',$data);
            } else if ($getExam->tipe == 'exam')
            {
                $this->load->view('users/exam/after_exam',$data);
            }

        } else
        {
            redirect('/');
        }
    }

    public function get_answered()
    {
        $idExam = $this->input->post('id_exam');
        $idUser = $this->input->post('id_user');

        $data   = $this->questions_model->check_answer(['a.id_exam' => $idExam, 'a.id_user' => $idUser])->result_array();

        $condCheckAll         = [
            'id_exam'       => $idExam,
            'id_user'       => $idUser,
        ];

        $checkAllIsAnswer   = $this->questions_model->check_answer_all($condCheckAll);
        
        // print_r($response); die;
        $response   = [
            'data'      => $data,
            'status'    => 200
        ];
        $response['all_answered'] = ($checkAllIsAnswer > 0) ? 'false' : 'true';

        echo json_encode($response);


    }

}
