<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Profileweb extends MY_Controller {
  public $view    = 'admin/profile_web/';
	public function __construct(){
		parent::__construct();
    checkAuthJWT();
    
    $this->load->database();
		$this->load->model('admin/profile_web_model','model');
	}

	public function index(){
    $result['data']         = $this->model->get()->row();
    $result['breadcrumbs']  = [
      [
        'text'  => 'Profile Web ',
        'link'  => '#'
      ]
    ];
    $result['sub_menu'] = 'profileweb';
		$this->load_template('admin/template', $this->view.'display', $result);
	}

  public function update()
  {
    $cond = [
      'id'  => 1
    ];

    
    $config['upload_path']          = './uploads/profile-web';
    $config['allowed_types']        = 'jpg|jpeg|png';
    $config['max_size']             = 1024;
    
    $this->load->library('upload', $config);
    
    if (file_exists($_FILES['image']['tmp_name']))
    {
      if (!$this->upload->do_upload('image'))
      {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata("error",$error['error']);
        redirect($_SERVER['HTTP_REFERER']);
      } else
      {
        $data = [
          'description' => $this->input->post('description'),
          'image'       => $this->upload->data('file_name')
        ];
    
        $update = $this->model->update($cond, $data);
      }
    } else
    {
       $data = [
        'description' => $this->input->post('description')
      ];
  
      $update = $this->model->update($cond, $data);
    }

    if ($update)
    {
      $this->session->set_flashdata("success", "Profile Web update");
    } else
    {
      $this->session->set_flashdata("error", "Profile Web failed update");
    }

    redirect ("admin/profile-web");
  }

	
}
