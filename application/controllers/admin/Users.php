<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Users extends MY_Controller {
  public $view    = 'admin/users/';
	public function __construct(){
		parent::__construct();
    checkAuthJWT();

    $this->load->database();
		$this->load->model('admin/users_model','model');
	}

	public function index(){
    $result['data'] = $this->get();
    $result['judul'] = 'Users';
    $result['get_data_edit'] = base_url('admin/'.$this->class.'/get_modal');
    $result['get_data_delete'] = base_url('admin/'.$this->class.'/delete');
    $result['get_reset_password'] = base_url('admin/'.$this->class.'/reset_password');
    $result['update_status'] = base_url('admin/'.$this->class.'/update_status');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template('admin/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_m_user');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal()
	{
    $id = $this->input->post('id');
    $this->model->id = $id;
    $get = $this->model->get_data('ls_m_user');
    $response['role'] = $this->master_model->data('*', 'ls_m_role', ["id !=" => 1])->get()->result_array();
    $response['pesan'] = 'data tidak ada';
    $response['status'] = 404;
    $response['data'] = array();
    $response['simpan'] = base_url('admin/'.$this->class.'/save');
    $response['disable'] = 'disabled';
    if ($get -> num_rows() > 0) {
      $response['pesan'] = 'data ada';
      $response['status'] = 200;
      $response['data'] = $get->row_array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
	}

	public function save()
	{
    $conf = array(
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|callback_unique'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      $where['id'] = $this->input->post('id');
      if ($this->input->post('role') == 2) {
        $data = array(
          'nama_depan' => $this->input->post('nama_depan'),
          'nama_belakang' => $this->input->post('nama_belakang'),
          'email' => $this->input->post('email'),
          'no_hp' => $this->input->post('no_hp'),
          'institusi' => $this->input->post('institusi'),
          'jk' => $this->input->post('jk'),
          'profil' => $this->input->post('profil'),
        );
      }elseif ($this->input->post('role') == 3) {
        if ($this->input->post('login_via') == 'solmit') {
          if ($this->input->post('avatar') == 0) {
            $avatar = $this->input->post('avatar_old');
          }else{
            $avatar = null;
          }
          $data = array(
            'nama_depan' => $this->input->post('nama_depan'),
            'nama_belakang' => $this->input->post('nama_belakang'),
            'email' => $this->input->post('email'),
            'no_hp' => $this->input->post('no_hp'),
            'institusi' => $this->input->post('institusi'),
            'jk' => $this->input->post('jk'),
            'nama_sertifikat' => $this->input->post('nama_sertifikat'),
            'avatar' => $avatar,
          );
        }else{
          $data = array(
            'nama_depan' => $this->input->post('nama_depan'),
            'nama_belakang' => $this->input->post('nama_belakang'),
            'no_hp' => $this->input->post('no_hp'),
            'institusi' => $this->input->post('institusi'),
            'jk' => $this->input->post('jk'),
            'nama_sertifikat' => $this->input->post('nama_sertifikat'),
          );
        }
      }
      $update = $this->model->update_data('ls_m_user', $data, $where);
      $response['pesan'] = 'Gagal Melakukan Update Users';
      $response['status'] = 404;
      if ($update) {
        $response['pesan'] = 'Berhasil Melakukan Update Users';
        $response['status'] = 200;
      }
    }
    echo json_encode($response);
	}

  public function unique(){
		$id 				= $this->input->post('id');
		$email 		= $this->input->post('email');
    if ($this->master_model->check_data(['id !='=> $id, 'email' => $email],'ls_m_user')) {
			$this->form_validation->set_message('unique', 'Email Sudah Ada !');
			return false;
		}
		return true;
	}

  public function update_list(){
    $posisi = $this->input->post('position');
    $i = 0;
    foreach ($posisi as $key => $value) {
      $i++;
      $this->master_model->update(['urutan' => $i], ['id_user' => $value], 'urutan_biodata');
    }
  }

  public function update_status()
  {
    $id = $this->input->post('id');
    $data = $this->master_model->data('status', 'ls_m_user', ['id' => $id])->get()->row();
    if ($data->status == 1) {
      $update = $this->master_model->update(['status' => 2], ['id' => $id], 'ls_m_user');
    }else{
      $update = $this->master_model->update(['status' => 1], ['id' => $id], 'ls_m_user');
    }
    if ($update) {
      sendNotification([
        'url' => base_url('lecturer'),
        'to_user_id' => $id, // Role Admin
        'message' => 'Submission Lecturer was ' . ($data->status == 1 ? 'approved' : 'declined') . ' by Admin',
        'module' => $this->class,
      ]);

      $response['pesan'] = 'Berhasil melakukan verifikasi user lecturer';
      $response['status'] = 200;
    }else{
      $response['pesan'] = 'Gagal melakukan verifikasi user lecturer';
      $response['status'] = 500;
    }
    echo json_encode($response);
  }

  public function reset_password(){
    $get_data = $this->master_model->data('email', 'ls_m_user', ['id' => $this->input->post('id')])->get()->row();
    $where['id'] = $this->input->post('id');
    $new_password = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($get_data->email))))))))));
    $update = $this->master_model->update(['password' => $new_password], $where, 'ls_m_user');
    if ($update) {
      $response['pesan'] = "Password berhasil di reset !";
      $response['status'] = 200;
    }else{
      $response['pesan'] = "Password gagal di reset !";
      $response['status'] = 404;
    }
    echo json_encode($response);
  }

	public function delete()
  {
    $where['id'] = $this->input->post('id');
    $status = $this->master_model->delete($where, 'ls_m_user');
    // $urutan = $this->master_model->delete(['id_user' => $this->input->post('id')], 'urutan_biodata');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($status) {
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    // if ($status && $urutan) {
    //   $response['pesan'] = 'Data Berhasil Dihapus';
    //   $response['status'] = 200;
    // }
    echo json_encode($response);
  }

}
