<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;

Class Questions extends MY_Controller {

    public $view    = 'admin/questions/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWT();

        $this->load->model('admin/questions_model', 'model');
        $this->load->model('admin/exam_model', 'model_exam');
        $this->load->model('admin/episode_model', 'model_episode');
    }

    public function index($id_exam)
    {
        $data['title']          = 'Questions & Answers';
        $data['id_exam']        = $id_exam;
        $data['data_exam']      = $this->model_exam->show($id_exam);
        if ($data['data_exam']->tipe == 'quiz'){
            $this->model_episode->id = $data['data_exam']->id_episode;
            $data['episode']    = $this->model_episode->get_data('ls_m_episode')->row();
        }
        $data['url_data']       = base_url().'admin/data-questions/'.$id_exam;
        $data['url_delete']     = base_url().'admin/delete-question';
        $data['url_answers']    = base_url().'admin/data-answers';
        $data['total_question'] = $this->model->show(['id_exam' => $id_exam]);
        // $sub_menu           = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
        // $menu               = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $this->load_template('admin/template', $this->view.'display', $data, '', '');
    }

    public function data($id_exam)
    {
        $search  = $this->input->post('search');
        $limit  = $this->input->post('length');
        $offset = $this->input->post('start');

        if (!empty($search['value']))
        {
            $list   = $this->model->show(['id_exam' => $id_exam, "pertanyaan LIKE'%".$search['value']."%'" => NULL], $limit, $offset);
            $data_list   = $list->result_array();
            $total     = $list->num_rows();
        } else
        {
            $list   = $this->model->show(['id_exam' => $id_exam], $limit, $offset);
            $data_list   = $list->result_array();
            $total     = $list->num_rows();
        }

        $filtered = $this->model->show(['id_exam' => $id_exam])->num_rows();

        $data = array();
        $no = $offset;
        
        foreach ($data_list as $field) {
            $null_answer    = $this->model->check_null_answer($field['id'])->num_rows();
            $total_answer   = $this->model->show_answers(["c.id" => $field['id']])->num_rows();
            $check_kunjaw   = $this->model->check_kunjaw($field['id'], false);

            $no++;
            $row = array();
            $row[] = $no;
            $pertanyaan = $field['pertanyaan'];

            if ($null_answer != 0)
            {
                $pertanyaan .= "<div class='blink_me'>
                <span class='badge badge-warning'>Ada pilihan jawaban yang masih kosong</span>
                </div>";
            } 
            if ($total_answer == 0)
            {
                $pertanyaan .= "<div class='blink_me'>
                <span class='badge badge-danger'>Soal belum memiliki jawaban</span>
                </div>";
            }  
            if ($check_kunjaw == NULL)
            {
                $pertanyaan .= "<div class='blink_me'>
                <span class='badge badge-info'>Soal belum memiliki kunci jawaban</span>
                </div>";
            }  
            
            $row[] = $pertanyaan;
            $row[] = $field['score'];
            $row[] = $field['id'];
            $row[] = $field['tipe'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $total,
            "recordsFiltered" => $filtered,
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function data_answers()
    {
        $id_question    = $this->input->post("id");
        $answers        = $this->model->show_answers(['a.id_question' => $id_question])->result_array();

        $kunjaw         = new stdClass();
        if ($answers[0]['tipe'] == 'checkbox')
        {
            $kunjaw     = $this->model->check_kunjaw($id_question, false);
        }

        if ($answers)
        {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Get answers success!',
                'data'      => $answers,
                'kunjaw'    => $kunjaw
            ];
        } else
        {
            $res    = [
                'status'    => 500,
                'pesan'     => 'Oops, something wrong. Get answers failed!'
            ];
        }

        echo json_encode($res);
    }

    public function store_question()
    {
        $data   = [
            'pertanyaan'=> $this->input->post('question'),
            'score'     => $this->input->post('score'),
            'id_exam'   => $this->input->post('id_exam'),
            'tipe'      => $this->input->post('type'),
            'number_option'   => ($this->input->post('number_option') == 0) ? 0 : $this->input->post('number_option')
        ];

        $store  = $this->model->store($data);

        if ($this->input->post('type') != 'boolean')
        {
            $dataAnswer = [];
            $opt    = 'a';
            for($i=0; $i<$data['number_option']; $i++){
                $push   = [
                    'id_question'   => $store,
                    'opt'           => $opt,
                    'answer'        => '-'
                ];
                array_push($dataAnswer, $push);
                $opt++;
            }
        } else
        {
            $dataAnswer = [
                [
                    'id_question'   => $store,
                    'opt'           => 'a',
                    'answer'        => 'True'
                ],
                [
                    'id_question'   => $store,
                    'opt'           => 'b',
                    'answer'        => 'False'
                ]
            ];
        }
        $storeAnswer    = $this->model->insert_batch($dataAnswer);

        if ($store && $storeAnswer)
        {
            $this->session->set_flashdata('status_store_question', 'success');
            $this->session->set_flashdata('msg_store_question', 'Store question success!');
        } else
        {
            $this->session->set_flashdata('status_store_question', 'error');
            $this->session->set_flashdata('msg_store_question', 'Oops something wrong happen, Store question failed!');
        }

        if ($this->input->post('tipe_exam') == 'exam')
        {
            redirect ('admin/exam-questions/'.$data['id_exam']);
        } else
        {
            redirect ('admin/quiz-questions/'.$data['id_exam']);
        }
    }

    public function store_answers()
    {
        $totalData  = count($this->input->post('ans_id_answer'));

        $data   = [];
        for ($i=0; $i<$totalData; $i++)
        {
            $data[] =
            [
                    'id' => $this->input->post('ans_id_answer')[$i],
                    'answer' => $this->input->post('ans_answer')[$i],
            ];
        }

       $updateBatch = $this->model->update_answers_batch($data);

       $checkKunjaw = $this->model->check_kunjaw($this->input->post('ans_id_question'));

        $data_id_answer  = ($this->input->post('ans_tipe_question') == 'checkbox') ? implode(',', $this->input->post('opt')) : $this->input->post('opt');
        if ($checkKunjaw > 0)
        {
            $dataKunjaw  = [
                'id_answer' => $data_id_answer
            ];

            $condKunjaw  = [
                'id_question'    => $this->input->post('ans_id_question')
            ];

            $updateKunjaw    = $this->model->update_kunjaw($dataKunjaw, $condKunjaw);
        } else
        {
            $dataKunjaw  = [
                'id_question'   => $this->input->post('ans_id_question'),
                'id_answer'     => $data_id_answer
            ];

            $storeKunjaw    = $this->model->store_kunjaw($dataKunjaw);
        }

        if ($updateKunjaw || $storeKunjaw)
        {
            $this->session->set_flashdata('status_update_answer', 'success');
            $this->session->set_flashdata('msg_update_answer', 'Answers saved!');
        } else
        {
            $this->session->set_flashdata('status_update_answer', 'error');
            $this->session->set_flashdata('msg_update_answer', 'Oops something wrong happen, update answer failed!');
        }

        redirect('admin/exam-questions/'.$this->input->post('ans_id_exam'));
    }

    public function store_question_used()
    {
        $idExam = $this->input->post('id_exam');
        $data   = [
            'jumlah_soal_dipakai' => $this->input->post('jumlah_soal_dipakai')
        ];

        $cond   = [
            'id' => $idExam
        ];

        $update = $this->model_exam->store_question_used($data, $cond);

        redirect ('admin/exam-questions/'.$idExam);
    }

    public function delete_question()
    {
        $delete = $this->model->delete(['id' => $this->input->post('id')]);
        $delete2 = $this->model->delete_answer(['id_question' => $this->input->post('id')]);
        $delete3 = $this->model->delete_key(['id_question' => $this->input->post('id')]);

        if ($delete)
        {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Delete question success!'
            ];
        } else
        {
            $res    = [
                'status'    => 500,
                'pesan'     => 'Oops, something wrong. Delete question failed!'
            ];
        }

        echo json_encode($res);
    }

    public function import_questions()
    {
        $filename   ='excel_questions_'.rand(00001,99999).'_'.$this->session->userdata('id');
        $config['upload_path'] = './assets/excel_questions';
        $config['max_size']=2048;
        $config['allowed_types']="xlsx|xls";
        $config['remove_spaces']=TRUE;
        // $config['encrypt_name']=TRUE;
        $config['file_name']= $filename;


        $a = $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file_excel')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error); die;
        } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

            $spreadsheet    = $reader->load("./assets/excel_questions/".$filename.$this->upload->data('file_ext'));
            $datas          = $spreadsheet->getSheet(0)->toArray();
            unset($datas[0]);

            foreach ($datas as $data)
            {
                if (empty($data[1])) {
                    continue;
                }
                $dataStoreQuestion = [
                    'id_exam'       => $this->input->post('id_exam'),
                    'pertanyaan'    => $data[1],
                    'score'         => $data[8],
                    'tipe'          => ($data[2] == 'true-false') ? 'boolean' : $data[2]
                ];
                $storeQuestion = $this->model->store($dataStoreQuestion);

                // $dataAnswers    = [
                //     [
                //         'id_question'   => $storeQuestion,
                //         'opt'           => 'a',
                //         'answer'        => $data[2]
                //     ],
                //     [
                //         'id_question'   => $storeQuestion,
                //         'opt'           => 'b',
                //         'answer'        => $data[3]
                //     ],
                //     [
                //         'id_question'   => $storeQuestion,
                //         'opt'           => 'c',
                //         'answer'        => $data[4]
                //     ],
                //   ]

                $dataAnswers    = [];

                $alpha          = 'a';

                for ($i=3; $i<=6; $i++)
                {
                    if ($data[$i] !== NULL)
                    {
                        if ($data[2] == 'option' || $data[2] == 'checkbox')
                        {
                            $addAnswer      = [
                                'id_question'   => $storeQuestion,
                                'opt'           => $alpha,
                                'answer'        => $data[$i]
                            ];
                        } else if ($data[2] == 'true-false') {
                            $addAnswer      = [
                                'id_question'   => $storeQuestion,
                                'opt'           => $alpha,
                                'answer'        => ($data[$i] === true) ? 'TRUE' : 'FALSE'
                            ];
                        }

                        array_push($dataAnswers, $addAnswer);
                        $alpha++;
                    }
                }

                $storeAnswer    = $this->model->store_answer($dataAnswers, true);

                if ($data[2] == 'option')
                {
                    $getIdAnswer    = $this->model->show_answers(['a.id_question' => $storeQuestion, 'opt' => strtolower($data[7])])->row();
                    $dataKunjaw     = [
                        'id_question'   => $storeQuestion,
                        'id_answer'     => $getIdAnswer->id_answer
                    ];

                    // $storeAnswer    = $this->model->store_answer($dataAnswers, true);
                    // $getIdAnswer    = $this->model->show_answers(['a.id_question' => $storeQuestion, 'opt' => strtolower($data[6])])->row();

                } else if ($data[2] == 'true-false')
                {
                    $getIdAnswer    = $this->model->show_answers(['a.id_question' => $storeQuestion, 'answer' => $addAnswer['answer']])->row();
                    $dataKunjaw     = [
                        'id_question'   => $storeQuestion,
                        'id_answer'     => $getIdAnswer->id_answer
                    ];

            //         $storeKunjaw    = $this->model->store_kunjaw($dataKunjaw);
            //     }
            //
            //
            // }
                } else if ($data[2] == 'checkbox')
                {
                    $getIdAnswer    = $this->model->show_answers_cbx($storeQuestion, strtolower($data[7]));
                    $dataKunjaw     = [
                        'id_question'   => $storeQuestion,
                        'id_answer'     => $getIdAnswer
                    ];
                }


                $storeKunjaw    = $this->model->store_kunjaw($dataKunjaw);

            }


        }
            redirect ('admin/exam-questions/'.$this->input->post('id_exam'));
    }
}
