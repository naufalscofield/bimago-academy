<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;
use GuzzleHttp\Psr7\Request;

defined('BASEPATH') OR exit('No direct script access allowed');

// include APPPATH . "../vendor/autoload.php";
require 'vendor/autoload.php';

class Withdrawal extends MY_Controller {
    public $view    = 'admin/withdrawal/';

    public function __construct(){
        parent::__construct();
        checkAuthJWT(); 

        $this->load->database();
        $this->load->model('lecturer/lecturer_withdrawal_model','model');
        // $this->load->model('lecturer/lecturer_saldo_model','model_saldo');
        // $this->load->model('lecturer/lecturer_account_model','model_account');
        // $this->load->model('admin/cut_off_model','model_cut_off');
    }

    public function index()
    {
        $data['withdrawal'] = $this->model->get()->result_array();
        $data['menu'] = 'Withdrawal Request';
        $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => 'withdrawal-request'])->get()->row();
        $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $data['breadcrumbs']  =
        [
            [
                'title' => 'Transaksi',
                'link'  => NULL
            ],
            [
                'title' => 'Withdrawal Request',
                'link'  => '',
                'akhir' => true
            ]
        ];

        $this->load_template('admin/template', $this->view.'display', $data, $menu->nama_menu, $sub_menu->nama_menu);
    }

    public function verify($id)
    {
        $cond = [
            'id' => $id
        ];

        $data = [
            'status' => 2
        ];

        $verify     = $this->model->update($cond, $data);
        $withdrawal = $this->model->show_with_user(['a.id' => $id])->row();

        if ($verify)
        {
            $client     = new GuzzleHttp\Client(['base_uri' => 'http://beelms.solmit.com/api/']);
            $response   = $client->request('POST', 'auth/token',[
                'form_params'   => [
                    'api_key'       => env('APP_API_KEY'),
                ]
            ]);

            if ($response != NULL)
            {
                if ($response->getStatusCode() == 200)
                {
                    $response   = json_decode($response->getBody());
                    $client2     = new GuzzleHttp\Client(['base_uri' => 'http://beelms.solmit.com/api/']);
                    $headers = [
                        'Authorization' => 'Bearer ' . $response->token,        
                        'Accept'        => 'application/json',
                    ];
                    $response2   = $client2->request('POST', 'withdrawal-request',[
                        'headers' => $headers,
                        'form_params'   => [
                            'lms_code'      => $response->lms_code,
                            'id_withdrawal' => $id,
                            'id_lecturer'   => $withdrawal->id_lecturer,
                            'name'          => $withdrawal->nama_depan.' '.$withdrawal->nama_belakang,
                            'total'         => $withdrawal->total
                        ]
                    ]);

                    if ($response2 != NULL)
                    {
                        if ($response2->getStatusCode() == 200)
                        {
                            $this->output->set_status_header(200);
                            echo json_encode($this->lang->line('withdrawal_request_success'));
                        }
                    } else
                    {
                        $this->output->set_status_header(500);
                        echo json_encode('Server error, withdrawal request failed.');
                    }
                } else
                {
                    $this->output->set_status_header(500);
                    echo json_encode('Server error, withdrawal request failed.');
                }

            } else
            {
                $this->output->set_status_header(500);
                echo json_encode('Server error, withdrawal request failed.');
            }
        } else{
            $this->output->set_status_header(500);
            echo json_encode($this->lang->line('internal_server_error'));
        }
    }

    public function show($id)
    {
        $data  = $this->model->show(['id' => $id])->row();

        if ($data)
        {
            $this->output->set_status_header('200');
            echo json_encode(['data' => $data]);
        } else{
            $this->output->set_status_header('500');
            echo json_encode($this->lang->line('internal_server_error'));
        }
    }

    public function callback_payment()
    {
        
    }
    
}