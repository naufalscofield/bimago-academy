<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Kategori extends MY_Controller {
  public $view    = 'admin/kategori/';
	public function __construct(){
		parent::__construct();
    checkAuthJWT();
    
    $this->load->database();
		$this->load->model('admin/kategori_model','model');
	}

	public function index(){
    $result['data'] = $this->get();
    $result['judul'] = 'Kategori List';
    $result['get_data_edit'] = base_url('admin/'.$this->class.'/get_modal');
    $result['get_data_delete'] = base_url('admin/'.$this->class.'/delete');
    // $result['update_list'] = base_url('admin/'.$this->class.'/update_list');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template('admin/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_m_kategori');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal()
	{
    $id = $this->input->post('id');
    if ($id != '') {
      $this->model->id = $id;
      $get = $this->model->get_data('ls_m_kategori');
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save');
      $response['disable'] = 'disabled';
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['simpan'] = base_url('admin/'.$this->class.'/save');
        $response['disable'] = '';
      }
    }else{
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
	}

	public function save()
	{
    $conf = array(
            array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'trim|required|callback_unique'),
            // array('field' => 'gambar', 'label' => 'Gambar', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');
    $this->form_validation->set_message('numeric', '%s harus berisi nomor.');
    $this->form_validation->set_message('matches', '%s tidak cocok.');
    $this->form_validation->set_message('min_length', '%s minimal %s digit.');
    $this->form_validation->set_message('valid_email', '%s harus valid.');
    $this->form_validation->set_message('is_unique', '%s tidak boleh sama.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
        $data = array(
          'nama_kategori' => $this->input->post('kategori'),
        );
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $update = $this->model->update_data('ls_m_kategori', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update kategori';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update kategori';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_kategori', $data);
        $response['pesan'] = 'Data kategori Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data kategori Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
	}

  public function unique(){
		$id 				= $this->input->post('id');
    $kategori 		= strtolower($this->input->post('kategori'));
		if (!empty($id)) {
			if ($this->master_model->check_data(['id !='=> $id, 'lower(nama_kategori)' => $kategori],'ls_m_kategori')) {
				$this->form_validation->set_message('unique', 'Kategori Sudah Ada !');
				return false;
			}
		}else{
			if ($this->master_model->check_data(['nama_kategori' => $kategori],'ls_m_kategori')) {
				$this->form_validation->set_message('unique', 'Kategori Sudah Ada !');
				return false;
			}
		}

		return true;
	}

	public function delete()
  {
    $where['id'] = $this->input->post('id');
    $status = $this->master_model->delete($where, 'ls_m_kategori');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($status) {
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    // if ($status && $urutan) {
    //   $response['pesan'] = 'Data Berhasil Dihapus';
    //   $response['status'] = 200;
    // }
    echo json_encode($response);
  }

}
