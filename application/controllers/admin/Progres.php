<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Progres extends MY_Controller {
  public $view    = 'admin/progres/';
    public function __construct(){
        parent::__construct();
        checkAuthJWT();

        $this->load->model('admin/Progres_model','cert');
        $this->load->model('admin/Course_model','model_course');
        $this->load->model('admin/Episode_model','model_episode');
        $this->load->model('admin/Task_model','model_task');
        $this->load->model('admin/Exam_model','model_exam');
    }

    public function index(){
    $result['data'] = $this->get();
    $result['judul'] = 'Monitoring Webinar';
    $result['video'] = base_url('admin/'.$this->class.'/detail_video/');
    $result['quiz'] = base_url('admin/'.$this->class.'/detail_quiz/');
    $result['exam'] = base_url('admin/'.$this->class.'/detail_exam/');
    $result['get_data_edit'] = base_url('admin/'.$this->class.'/data_type/');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
    $this->load_template('admin/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
    }

    public function get($id = '')
    {
      $this->cert->id = $id == '' ? $this->input->post('id') : $id;
      $get = $this->cert->get_data('ls_m_course');
      $response['pesan'] = 'Data Tidak Ditemukan';
      $response['status'] = 404;
      $response['data'] = array();
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
      }
      return $response;
    }

    public function data_type()
    {
      $id_course = $this->input->post('id');
      $type = $this->master_model->data('type_course', 'ls_m_course', ['id' => $id_course])->get()->row_array()['type_course'];
      $type = json_decode($type);
      $final_type = [];
      foreach ($type as $key => $value) {
        $get = $this->master_model->data('type_course', 'ls_m_type_course', ['id' => $value])->get()->row_array();
        $final['id'] = $value;
        $final['type_course'] = $get['type_course'];
        $final_type[] = $final;
      }
      $data['id_course'] = $id_course;
      $data['type'] = $final_type;
      $data['link'] = $this->input->post('link');
      $this->load->view($this->view.'type', $data);
    }

  public function detail_video($id, $id_type){
    $result['data'] = $this->get_detail_video($id,$id_type);
    $data = $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row();
    $result['judul'] = 'Monitoring Video -> '.$data->judul;
    $result['get_data'] = base_url('admin/'.$this->class.'/get_spesifik_video/');
    $result['id_course'] = $id;
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
    $this->load_template('admin/template', $this->view.'display_video', $result, $menu->nama_menu, $sub_menu->nama_menu);
    }

  public function get_detail_video($id, $id_type)
  {
    $get = $this->master_model->data('a.id_user, a.id_type_course, c.nama_depan, c.nama_belakang, d.type_course, (SELECT COUNT(id) from ls_m_episode e WHERE a.id_course = e.id_course AND a.id_type_course = e.id_type_course) as total_video', 'ls_t_detail_pembelian a', ['a.id_course' => $id, 'a.id_type_course' => $id_type, 'b.status' => 1])
            ->join('ls_t_pembayaran b', 'a.id_pembayaran = b.id', 'LEFT')
            ->join('ls_m_user c', 'a.id_user = c.id', 'LEFT')
            ->join('ls_m_type_course d', 'a.id_type_course = d.id', 'LEFT')
            ->get()->result_array();
    $get_final = [];
    foreach ($get as $key => $value) {
      $persentase = $this->master_model->data('sum(b.persentase) as persentase', 'ls_m_episode a', ['a.id_course' => $id, 'a.id_type_course' => $value['id_type_course'], 'b.id_user' => $value['id_user'], 'a.status' => 2])
      ->join('ls_t_progress_watch b', 'a.id = b.id_episode', 'LEFT')->get()->row_array();
      $value['persentase'] = 0;
      if (!empty($persentase['persentase']) || $persentase['persentase'] > 0) {
        $value['persentase'] = round($persentase['persentase']/$value['total_video'],1);
      }
      $get_final[] = $value;
    }
    $response['pesan'] = 'data tidak ada';
    $response['status'] = 404;
    $response['data'] = array();
    if (count($get_final) > 0) {
      $response['pesan'] = 'data ada';
      $response['status'] = 200;
      $response['data'] = $get_final;
    }
    return $response;
  }

  public function get_spesifik_video()
  {
    $persentase = $this->master_model->data('judul, (SELECT persentase FROM ls_t_progress_watch b WHERE b.id_user = '.$this->input->post('id_user').' AND a.id = b.id_episode) as persentase' , 'ls_m_episode a', ['a.id_course' => $this->input->post('id_course'), 'a.id_type_course' => $this->input->post('id_type_course'), 'a.status' => 2])
    ->get()->result_array();
    $result = array(
      'data' => $persentase,
      'nama' => $this->input->post('nama_depan').' '.$this->input->post('nama_belakang'),
    );
    return $this->load->view($this->view.'data_modal_video', $result);
  }

  public function detail_exam($id, $id_type){
    $result['data'] = $this->get_detail_exam($id, $id_type);
    $data = $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row();
    $result['judul'] = 'Monitoring Exam -> '.$data->judul;
    $result['get_data'] = base_url('admin/'.$this->class.'/get_spesifik_exam/');
    $result['id_course'] = $id;
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
    $this->load_template('admin/template', $this->view.'display_exam', $result, $menu->nama_menu, $sub_menu->nama_menu);
    }

  public function get_detail_exam($id, $id_type)
  {
    $user = $this->master_model->data('b.id, b.nama_depan, b.nama_belakang','ls_t_detail_pembelian a', ['a.id_course' => $id, 'a.id_type_course' => $id_type])
            ->join('ls_m_user b', 'a.id_user = b.id', 'LEFT')->get()->result_array();
    $quiz = [];
    foreach ($user as $key => $value) {
      $sub  = $this->db->query("SELECT (select sub.score from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as score_final, (select sub.status from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as status FROM `ls_m_exam` as a WHERE id_course=".$id." and id_type_course=".$id_type." and tipe='exam'")->result_array();

      $res  = [
        'id_user' => $value['id'],
        'nama_depan'  => $value['nama_depan'],
        'nama_belakang'  => $value['nama_belakang'],
        'data'  => $sub
      ];
      $quiz[] = $res;

    }
    // dd($quiz);
    $response['pesan'] = 'data tidak ada';
    $response['status'] = 404;
    $response['data'] = array();
    if (count($quiz) > 0) {
      $response['pesan'] = 'data ada';
      $response['status'] = 200;
      $response['data'] = $quiz;
    }
    return $response;
  }

  public function detail_quiz($id, $id_type){
    $result['data'] = $this->get_detail_quiz($id, $id_type);
    $data = $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row();
    $result['judul'] = 'Monitoring Quiz -> '.$data->judul;
    $result['get_data'] = base_url('admin/'.$this->class.'/get_spesifik_exam/');
    $result['id_course'] = $id;
    $result['total_quiz'] = $this->master_model->data('id', 'ls_m_episode', ['id_course' => $id, 'id_type_course' => $id_type])->get()->result_array();
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
    $this->load_template('admin/template', $this->view.'display_quiz', $result, $menu->nama_menu, $sub_menu->nama_menu);
    }

  public function get_detail_quiz($id, $id_type)
  {
    $user = $this->master_model->data('b.id, b.nama_depan, b.nama_belakang','ls_t_detail_pembelian a', ['a.id_course' => $id, 'a.id_type_course' => $id_type])
            ->join('ls_m_user b', 'a.id_user = b.id', 'LEFT')->get()->result_array();
    // dd($user);
    $quiz = [];
    foreach ($user as $key => $value) {
      // $value['quiz'] = $this->master_model->data('a.score, b.id_episode', 'ls_t_after_exam a', ['a.id_user' => $value['id'], 'b.id_course' => $id, 'b.id_type_course' => $id_type, 'b.tipe' => 'quiz'])
      //                 ->join('ls_m_exam b', 'a.id_exam = b.id', 'LEFT')
      //                 // ->group_by('b.id_episode')
      //                 // ->order_by('a.score', 'DESC')
      //                 ->get()
      //                 ->result_array();

      $sub  = $this->db->query("SELECT a.id_episode, (select sub.score from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as score_final, (select sub.status from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as status FROM `ls_m_exam` as a WHERE id_course=".$id." and id_type_course=".$id_type." and tipe='quiz'")->result_array();

      $res  = [
        'id_user' => $value['id'],
        'nama_depan'  => $value['nama_depan'],
        'nama_belakang'  => $value['nama_belakang'],
        'data'  => $sub
      ];
      $quiz[] = $res;

    }
    // dd($quiz);
    $response['pesan'] = 'data tidak ada';
    $response['status'] = 404;
    $response['data'] = array();
    if (count($quiz) > 0) {
      $response['pesan'] = 'data ada';
      $response['status'] = 200;
      $response['data'] = $quiz;
    }
    // dd($response);
    return $response;
  }

  public function rekap_nilai($id_course)
  {
    $data_course  = $this->model_course->show(['id' => $id_course])->row();
    $type  = str_replace('"', "", $data_course->type_course);
    $type  = str_replace('[', "", $type);
    $type  = str_replace(']', "", $type);
    
    $query        = "SELECT * from ls_m_type_course where id in ($type)";
    $data['type'] = $this->model_course->get_type($query);
    $data['url_header']          = base_url()."admin/Progres/get-total-episode/".$id_course;
    $data['url']   = base_url()."admin/Progres/get-rekap-nilai/".$id_course;
    $data['breadcrumbs']  = [
      [
        'name'  => 'Monitoring (Progress)',
        'link'  => base_url()."admin/Progres"
      ],
      [
        'name'  => 'Rekap Nilai '. $data_course->judul,
        'link'  => "#"
      ],
    ];

    $this->load_template('admin/template', $this->view.'rekap_nilai', $data);
  }

  public function get_total_episode($id, $id_type)
  {
    // $total_episode  = $this->model_episode->show(['id_course' => $id, 'id_type_course' => $id_type])->num_rows();
    $get_exam     = $this->model_exam->cond(['id_course' => $id, 'id_type_course' => $id_type, 'tipe' => 'exam']);
    $total_quiz   = $this->model_exam->cond(['id_course' => $id, 'id_type_course' => $id_type, 'tipe' => 'quiz']);
    $total_tugas  = $this->model_exam->get_total_tugas_per_episode();
    // print_r($total_tugas->result_array()); die;
    if ($get_exam->num_rows() != 0)
    {
      $is_exam = TRUE;
    } else
    {
      $is_exam = FALSE;
    }

    $data = [
      'is_exam'     => $is_exam,
      'total_quiz'  => $total_quiz->num_rows(),
      'total_tugas'  => $total_tugas->result_array()
    ];

    echo json_encode($data);
  }

  public function get_rekap_nilai($id, $id_type)
  {
    $search  = $this->input->post('search');
    $limit  = $this->input->post('length');
    $offset = $this->input->post('start');

    if (!empty($search['value']))
    {
      $user         = $this->master_model->data('b.id, b.nama_depan, b.nama_belakang','ls_t_detail_pembelian a', ['a.id_course' => $id, 'a.id_type_course' => $id_type, "b.nama_depan LIKE" => '%'.$search['value'].'%'], [], [], [], [], [], $limit, $offset)
                    ->join('ls_m_user b', 'a.id_user = b.id', 'LEFT')->get();
      $data_user    = $user->result_array();
      $total        = $user->num_rows();
    } else
    {
      $user         = $this->master_model->data('b.id, b.nama_depan, b.nama_belakang','ls_t_detail_pembelian a', ['a.id_course' => $id, 'a.id_type_course' => $id_type], [], [], [], [], [], $limit, $offset)
                    ->join('ls_m_user b', 'a.id_user = b.id', 'LEFT')->get();
      $data_user    = $user->result_array();
      $total        = $user->num_rows();
    }

    $filtered         = $this->master_model->data('b.id, b.nama_depan, b.nama_belakang','ls_t_detail_pembelian a', ['a.id_course' => $id, 'a.id_type_course' => $id_type])
                        ->join('ls_m_user b', 'a.id_user = b.id', 'LEFT')->get()->num_rows();
            
    $result = [];

    if ($total != 0)
    {
      foreach ($data_user as $key => $value) {

        $subtask  = $this->db->query("SELECT a.id as id_tugas_episode, a.id_episode, b.nilai as score_final from ls_m_tugas_episode as a left join ls_t_user_task as b on a.id = b.id_tugas and b.id_user = ".$value['id']." and b.id_course=".$id." and b.id_type_course=".$id_type."")->result_array();

        $subquiz  = $this->db->query("SELECT (select sub.score from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as score_final, (select sub.status from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as status FROM `ls_m_exam` as a WHERE id_course=".$id." and id_type_course=".$id_type." and tipe='quiz' order by id_episode")->result_array();
        
        $subexam  = $this->db->query("SELECT (select sub.score from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as score_final, (select sub.created_at from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as tanggal_kumpul, (select sub.total_menit from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as total_menit, (select sub.percobaan from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as percobaan, (select sub.status from ls_t_after_exam as sub where a.id = sub.id_exam and sub.id_user=".$value['id']." order by sub.score DESC limit 1) as status FROM `ls_m_exam` as a WHERE id_course=".$id." and id_type_course=".$id_type." and tipe='exam'")->result_array();

        $res_exam = [
          'nama'  => $value['nama_depan'].' '.$value['nama_belakang'],
          'task'  => $subtask,
          'quiz'  => $subquiz,
          'exam'  => (isset($subexam[0]['score_final']) &&  $subexam[0]['score_final'] != NULL) ? (int)$subexam[0]['score_final'] : 0,
          'status'  => (isset($subexam[0]['status']) &&  $subexam[0]['status'] != NULL) ? $subexam[0]['status'] : '-',
          'percobaan'  => (isset($subexam[0]['percobaan']) &&  $subexam[0]['percobaan'] != NULL) ? $subexam[0]['percobaan'] : 0,
          'tanggal_kumpul_exam'  => (isset($subexam[0]['tanggal_kumpul']) &&  $subexam[0]['tanggal_kumpul'] != NULL) ? $subexam[0]['tanggal_kumpul'] : '-',
          'total_menit'  => (isset($subexam[0]['total_menit']) &&  $subexam[0]['total_menit'] != NULL) ? $subexam[0]['total_menit'] : '-',
        ];

        $result[] = $res_exam;
      }
    }

    $data = array();
    $no = $_POST['start'];
    foreach ($result as $e) {
      $no++;
      $row = array();
      $row[] = $e['nama'];
      $row[] = $e['exam'];
      $row[] = $e['percobaan'];
      $row[] = $e['total_menit']. " Menit";
      $row[] = $e['tanggal_kumpul_exam'];
      $to = count($e['task']);
      $i  = 0;

      $arr = array();

      foreach ($e['task'] as $key => $item) {
        $arr[$item['id_episode']][$key] = $item;
      }
      
      ksort($arr, SORT_NUMERIC);

      if ($to != NULL)
      {
        foreach($arr as $ar)
        {
          $nilai  = '';
          $idx = 0;
          foreach ($ar as $t)
          {
            if ($t['score_final'] != NULL)
            {
              $nilai .= "<li> Tugas #".($idx+1)." : ".$t['score_final']."</li>";
            } else
            {
                $nilai .= "<li> Tugas #".($idx+1)." : -</li>";
            }
          }
          $row[] = $nilai;
        }
      }

      if ($id_type != 4)
      {
        foreach ($e['quiz'] as $idx => $sq)
        {
          $row[] = ($sq['score_final'] != NULL) ? $sq['score_final'].' ('.$sq['status'].')' : '-';
        }
      }

      $data[] = $row;
    }
    usort($data, function($a, $b) {
        return $a[1] <= $b[1];
    });

    foreach ($data as $idx => $d)
    {
      array_unshift($data[$idx], $idx+1);
    }
    
    $length = $_REQUEST['length'];
    $start  = $_REQUEST['start'];

    $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $total,
        "recordsFiltered" => $filtered,
        "data" => $data,
    );
    echo json_encode($output);
  }


}
