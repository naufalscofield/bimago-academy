<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Lms_config extends MY_Controller {
  public $view    = 'admin/lms_config/';
	public function __construct(){
		parent::__construct();
    checkAuthJWT();
    
    $this->load->database();
		// $this->load->model('admin/profile_web_model','model');
	}

	public function index(){
    // $result['data']         = $this->model->get()->row();
    $result['data']         = $this->master_model->data('*', 'ls_m_lms_config')->get()->row();
		$this->load_template('admin/template', $this->view.'display', $result);
	}

  public function update()
  {
    $cond = [
      'id'  => 1
    ];

    $data = [
      'link_youtube'=> $this->input->post('link_youtube'),
      'footer'      => $this->input->post('footer'),
      'alamat'      => $this->input->post('alamat'),
      'email'       => $this->input->post('email'),
      'facebook'    => $this->input->post('facebook'),
      'linkedin'    => $this->input->post('linkedin'),
      'telepon'     => $this->input->post('telepon'),
      'hari_kerja'  => $this->input->post('hari_kerja'),
      'jam_kerja'   => $this->input->post('jam_kerja'),
      'link_google_maps'   => $this->input->post('link_google_map'),
    ];

    $curr = $this->master_model->data('logo_utama, logo_putih, icon', 'ls_m_lms_config', ['id' => 1])->get()->row();

    $config['upload_path']          = './uploads/lms_config';
    $config['allowed_types']        = '*';
    $config['max_size']             = 34505;
    
    $this->load->library('upload', $config);

    if (!empty($_FILES['logo_utama']['name']))
    {
      unlink(APPPATH.'../uploads/lms_config/'.$curr->logo_utama);
      if (!$this->upload->do_upload('logo_utama'))
      {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata("error",$error['error']);
        redirect($_SERVER['HTTP_REFERER']);
      } else
      {
        $logo_utama = $this->upload->data('file_name');
        $data['logo_utama'] = $logo_utama;
      }
    }

    if (!empty($_FILES['logo_putih']['name']))
    {
      unlink(APPPATH.'../uploads/lms_config/'.$curr->logo_putih);
      if (!$this->upload->do_upload('logo_putih'))
      {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata("error",$error['error']);
        redirect($_SERVER['HTTP_REFERER']);
      } else
      {
        $logo_putih = $this->upload->data('file_name');
        $data['logo_putih'] = $logo_putih;
      }
    }

    if (!empty($_FILES['icon']['name']))
    {
      // unlink(APPPATH.'../uploads/lms_config/'.$curr->icon);
      if (!$this->upload->do_upload('icon'))
      {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata("error",$error['error']);
        redirect($_SERVER['HTTP_REFERER']);
      } else
      {
        $icon = $this->upload->data('file_name');
        $data['icon'] = $icon;
      }
    }
    // dd($data);
    $update = $this->master_model->update($data, $cond, 'ls_m_lms_config');

    if ($update)
    {
      $this->session->set_flashdata("success", "Config updated");
    } else
    {
      $this->session->set_flashdata("error", "Config failed update");
    }

    redirect ("admin/lms-config");
  }

  public function howto()
  {
    $this->load->view('admin/lms_config/howto');
  }

	
}
