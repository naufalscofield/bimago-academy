<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Gallery_photo extends MY_Controller {
  public $view    = 'admin/gallery_photo/';
	public function __construct(){
		parent::__construct();
    checkAuthJWT();
    
    $this->load->database();
	}

	public function index(){
    $result['gallery']         = $this->master_model->data('*', 'ls_m_gallery')->get()->result_array();
		$this->load_template('admin/template', $this->view.'display', $result);
	}

  public function store()
  {
    $config['upload_path']          = './uploads/gallery';
    $config['allowed_types']        = 'jpeg|png|jpg';
    $config['max_size']             = 34505;
    
    $this->load->library('upload', $config);

    if (!empty($_FILES['file']['name']))
    {
      if (!$this->upload->do_upload('file'))
      {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata("error",$error['error']);
        redirect($_SERVER['HTTP_REFERER']);
      } else
      {
        $file_name = $this->upload->data('file_name');
        $data['file'] = $file_name;
      }
    }

    $store =  $this->master_model->save($data, 'ls_m_gallery');

    if ($store)
    {
      $this->session->set_flashdata("success", "Photo saved");
    } else
    {
      $this->session->set_flashdata("error", "Photo failed saved");
    }

    redirect ("admin/gallery-photo");
  }

  public function delete($id)
  {
    $curr = $this->master_model->data('file', 'ls_m_gallery', ['id' => $id])->get()->row();
    unlink(APPPATH.'../uploads/gallery/'.$curr->file);

    $delete = $this->master_model->delete(['id' => $id], 'ls_m_gallery');

    if ($delete)
    {
      $data = ['code' => 200, 'msg' => 'success'];
      $code = 200;
    } else
    {
      $data = ['code' => 400, 'msg' => 'error'];
      $code = 400;
    }

    echo json_encode($data, $code);

  }
}
