<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Dashboard extends MY_Controller {
    public $view    = 'admin/dashboard/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWT();
    }

    public function index()
    {
        $data['title']            = 'Dashboard';
        $data['sum_student']      = $this->master_model->data('COUNT(id) as sum_student', 'ls_m_user',['role' => 3])->get()->row();
        $data['sum_lecturer']     = $this->master_model->data('COUNT(id) as sum_lecturer', 'ls_m_user',['role' => 2])->get()->row();
        $data['sum_course']       = $this->master_model->data('COUNT(id) as sum_course', 'ls_m_course')->get()->row();
        $data['sum_total']        = $this->master_model->data('SUM(total) as sum_total', 'ls_t_pembayaran', ['status'=> 1])->get()->row();
        // $data['sum_month_total']  = $this->master_model->data('SUM(total) as sum_month_total', 'ls_t_pembayaran', ['MONTH(tanggal_pembelian)=' . date('m') => NULL,'YEAR(tanggal_pembelian) ='. date('Y')=> NULL,'status'=> 1])->get()->row();


        $posdata = array();
        $bulan_data = array();
        $earning_data = array();
        $chart_course = array();
        $selled_course = array();

        $posisi['bulan'] = date('m');
        $posisi['tahun'] = date('Y');
          for ($x = 1; $x <= 12; $x++) {
            $posdata['tahun'] = date('Y', mktime(0, 0, 0, ($posisi['bulan'] - 12) + $x, 1, $posisi['tahun']));
            $posdata['bulan'] = date('m', mktime(0, 0, 0, ($posisi['bulan'] - 12) + $x, 1, $posisi['tahun']));
            $bulan            = date('M-Y', mktime(0, 0, 0, ($posisi['bulan'] - 12) + $x, 1, $posisi['tahun']));
            $data_earning = $this->master_model->data('SUM(total) as sum_month_total', 'ls_t_pembayaran', ['MONTH(tanggal_pembelian)=' . $posdata['bulan'] => NULL,'YEAR(tanggal_pembelian) ='. $posdata['tahun'] => NULL,'status'=> 1])->get()->row();
           
                $bulan_data[] = $bulan;
                $earning_data[] = isset($data_earning->sum_month_total) ? $data_earning->sum_month_total : 0;
        }

        $data['bulan_data']     = $bulan_data;
        $data['earning_data']   = $earning_data;


        $all_course = $this->master_model->data('*', 'ls_m_course',['status' => 2])->get();
        $total_selled= 0;
        foreach ($all_course->result() as $value) {
            $chart_course[]  = $value->judul;

            $count_selled   = $this->master_model->data('count(a.id_course) as count', 'ls_t_detail_pembelian a',['a.id_course' => $value->id])
            ->join('ls_t_pembayaran b', 'b.id = a.id_pembayaran AND b.status="1"', 'INNER')
            ->get()->row();
            $selled_course[] = isset($count_selled) ? $count_selled->count : 0;
            if ($count_selled->count != 0){
                $total_selled++;
            }
        }
        $total_course = $all_course->num_rows();
        $unselled_course = $total_course - $total_selled;
        $data['total_unselled']   = $unselled_course;
        $data['total_selled']     = $total_selled;
        $data['chart_course']   = $chart_course;
        $data['selled_course']   = $selled_course;

        $counter_visitor = [];
        $lebel_visitor = [];

        for ($i = 6; $i >= 0 ; $i--) {
            $date = date('Y-m-d');

            $minusdate = Date('Y-m-d', strtotime('-'.$i.' days'));
            $visitor = $this->master_model->data('','ls_t_visitor',['date'=>$minusdate])->group_by('ip')->get()->num_rows();
            $counter_visitor[] = $visitor; 
            $label_date = Date('Y M d', strtotime('-'.$i.' days'));
            $lebel_visitor[] = $label_date;

        }
        // $pengunjungonline  = $this->db->query("SELECT * FROM visitor WHERE online > '".( time() - 300)."'")->num_rows(); // hitung pengunjung online
          
        $data['lebel_visitor']=$lebel_visitor;
        $data['counter_visitor']=$counter_visitor;

        $this->load_template('admin/template', $this->view.'display', $data);
    }

    public function logout() {
      $this->cart->destroy();
      $this->session->sess_destroy();
      redirect('login');
    //   $response['pesan'] = 'Successfully Logout';
    //   $response['url'] = base_url();
    //   echo json_encode($response);
    }
    function rand_color() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

}
