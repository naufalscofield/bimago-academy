<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";
class Video extends MY_Controller {

  public  $google_client = NULL;
  public  $linkedin_client = NULL;

	public function __construct(){
		parent::__construct();
        ### GOOGLE API ###
        $this->config->load('google');

        $this->google_client = new Google_Client();
        $this->google_client->setClientId($this->config->item('OAUTH2_CLIENT_ID')); //Define your ClientID
        $this->google_client->setClientSecret($this->config->item('OAUTH2_CLIENT_SECRET')); //Define your Client Secret Key
        $this->google_client->setRedirectUri(base_url('admin/video/drive'));
        
        $this->google_client->setScopes([Google_Service_Drive::DRIVE,Google_Service_YouTube::YOUTUBE]);

        $this->google_client->setAccessType('offline');
        $this->google_client->setPrompt('select_account consent');
	}
  public function index(){
    redirect($this->google_client->createAuthUrl());
  }

	public function drive(){
    // $token = $this->master_model->data('response_token', 'ls_t_gdrive_token')->get()->row();
    // if (isset($token->response_token)) {
    //     $token = json_decode($token->response_token,TRUE);
    //     $this->session->set_userdata('access_token',$token);
    // }

    // Get your credentials from the console
        if (isset($_GET['code']) || !empty($this->session->userdata('access_token'))) {
            if (isset($_GET['code'])) {
                $this->google_client->authenticate($_GET['code']);
                $token = $this->google_client->getAccessToken();
                
                $data =array(
                    "client_id"     => $this->config->item('OAUTH2_CLIENT_ID'),
                    "client_secret" => $this->config->item('OAUTH2_CLIENT_SECRET'),
                    "grant_type"    => 'refresh_token',
                );

                $update = $this->master_model->update(['refresh_token' => $token['refresh_token']], $data,'ls_t_gdrive_token');
                if ($update) {
                    echo 'Success => ' . $token['refresh_token'];
                }else{
                    echo 'Failed';
                }

            } else{
                $this->google_client->setAccessToken($this->session->userdata('access_token'));
            }


        } else {
            $authUrl = $this->google_client->createAuthUrl();
            header('Location: ' . $authUrl);
            exit();
        }
	}

}
