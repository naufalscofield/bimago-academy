<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Episodev2 extends MY_Controller {
  public $view    = 'admin/episode/';
  public function __construct(){
    parent::__construct();
    checkAuthJWT();

    ini_set('max_execution_time', 36000000000000);
    ini_set('memory_limit', '-1');
    ini_set('post_max_size', '200M');
    ini_set('upload_max_filesize', '200M');
    
    $this->load->model('admin/episode_model','model');
    $this->load->model('admin/course_model','course_model');
    $this->load->model('admin/task_model','task_model');
    $this->load->helper('video');    
  }

  public function index($id=""){
    $result['data'] = $this->get('',$id);
    $result['judul'] = get_value('judul','ls_m_course',['id' => $id ]);
    $result['get_data_edit'] = base_url('admin/'.$this->class.'/get_modal'.(!empty($id) ? "/$id" :''));
    $result['get_data_delete'] = base_url('admin/'.$this->class.'/delete');
    $result['update_status'] = base_url('admin/'.$this->class.'/update_status');
    $result['get_video'] = base_url('admin/'.$this->class.'/video');
    $result['materi'] = base_url('admin/'.$this->class.'/materi/');
    $result['tugas'] = base_url('admin/'.$this->class.'/tugas/');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    // $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
    // $this->load_template('admin/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
    $this->load_template('admin/template', $this->view.'display', $result);
  }

  public function video()
  {
    $id = $this->input->post('id');
    if ($id != '') {
      $get = $this->master_model->data('*', 'ls_m_episode', ['id' => $id])->get();
      $response['data'] = array();
      if ($get -> num_rows() > 0) {
        $response['data'] = $get->row_array();
      }
    }else{
      $response['data'] = array();
    }
    return $this->load->view($this->view.'video', $response);
  }

  public function get($id = '',$id_ep='')
  {
    $this->model->id = $id == '' ? $this->input->post('id') : $id;
    $this->model->id_ep = $id_ep == '' ? '' : $id_ep;
    $get = $this->model->get_data('ls_m_episode');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
    if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
  }
  public function get_modal($id_course ='')
  {
    $id = $this->input->post('id');
    $where = ['status' => 2];
    if (!empty($id_course)) { $where['id']= $id_course; }
    $response['opt_modul'] = options2('ls_m_course', $where, 'id', 'judul', '', '- Pilih -', '', array('id' => 'ASC'));
    $response['url_get_tipe_course'] = base_url('admin/'.$this->class.'/get_tipe_course');
    if ($id != '') {
      $this->model->id = $id;
      $get = $this->model->get_data('ls_m_episode');
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save');
      $response['disable'] = 'disabled';
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['disable'] = '';
      }
    }else{
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modalv2', $response);
  }

  public function save()
  {
    $conf = array(
            array('field' => 'id_course', 'label' => 'Course', 'rules' => 'trim|required'),
            array('field' => 'judul', 'label' => 'Judul', 'rules' => 'trim|required'),
            // array('field' => 'url', 'label' => 'Url', 'rules' => 'trim|required'),
            array('field' => 'id_type_course', 'label' => 'Type course', 'rules' => 'trim|required'),
            array('field' => 'deskripsi', 'label' => 'Deskripsi', 'rules' => 'trim|required'),
            array('field' => 'order', 'label' => 'Urutan video', 'rules' => 'trim|required|callback_unique'),
            array('field' => 'video', 'label' => 'Video', 'rules' => 'callback_check_video'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');
    $this->form_validation->set_message('numeric', '%s harus berisi nomor.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      $where['id'] = $this->input->post('id');
      $data = array(
        'id_course' => $this->input->post('id_course'),
        'judul'     => $this->input->post('judul'),
        'url'       => $this->input->post('url'),
        'type'      => (strpos($this->input->post('url'), 'youtu') !== false ? 'youtube': 'gdrive'),
        'id_type_course'      => $this->input->post('id_type_course'),
        'deskripsi' => $this->input->post('deskripsi'),
        'order' => $this->input->post('order'),
        'status' => 2
      );

      $user  = $this->master_model->data('concat(a.nama_depan, " ",a.nama_belakang) as nama_user','ls_m_user a',['a.id'=>$this->session->userdata('id')])->get()->row();
      
      $folder_name = str_replace(' ', '-',strtolower($user->nama_user));
      $dir = "uploads/episode_videos/". $folder_name . '/';

      ## BEGIN UPLOAD VIDEO
      if ( !file_exists($dir ) && !is_dir($dir ) ) {
          mkdir($dir , 0757);       
      } 
      $video = uploadVideo('video',$dir);
      $video_name  = $video['pesan'] ? trim($this->input->post('judul')) : '';
      $video_loc = $video['pesan'] ? $video['nama_loc'] : '';

      if (!empty($video_name) && !empty($video_loc)) {
        ## HANDLING HAPUS FILE JIKA MASIH ADA
        if (!empty($this->input->post('video_old'))) {
        if (file_exists(APPPATH . "../".$this->input->post('video_old'))) {
           unlink(APPPATH . "../".$this->input->post('video_old'));
         }
        }
       $data['url'] = '';
       $data['video'] = $video_loc;

        // if ( strtolower($video['ext']) == '.mov' || strtolower($video['ext']) == '.mp4') {
          // $type_upload = $this->input->post('type');
          // if ($type_upload == 'gdrive') { 
          //   ## CREATE FOLDER NAME
          //   $judul = get_value('judul','ls_m_course', ['id'=> $this->input->post('id_course')]);
          //   $nama  = $this->master_model->data('a.nama_depan,a.nama_belakang','ls_m_user a',['a.id'=>$this->session->userdata('id')])->get()->row();
          //   $folder_name = '('.env('APP_NAME').' - '.$nama->nama_depan .' '.$nama->nama_belakang.') '.trim($judul);

          //   ## REFRESH TOKEN AKUN GOOGLE
          //   video::refresh_token();
          //   ## UPLOAD GDRIVE && GET URL
          //   $url = video::upload_drive($video_name, $video_loc,$folder_name);

          // }else{
          //   ## UPLOAD YOUTUBE
          //   $this->load->library('Youtube');
            
          //   $video= $video_loc;
          //   $title= $video_name;
          //   $desc= strip_tags($data['deskripsi']);
          //   $tags=["solmitacademy","beelms"];
          //   $privacy_status="unlisted";
          //   $youtube = $this->youtube->youtube_upload($video,$video_name,$desc,$tags,$privacy_status);
          //   $url = isset($youtube['url']) ? $youtube['url'] : '';
          // }

          ## DELETE FILE AFTER UPLOAD
          // if (!empty($url)) {
          //    $data['url'] = $url;
          //    $data['type'] = $type_upload;
          //    $data['video'] = '';
          //    if (file_exists(APPPATH . "../".$video_loc)) {
          //     unlink(APPPATH . "../".$video_loc);
          //    }
          // }else{
          //   $this->output->set_status_header('403');
          //   $this->data['message'] = 'Gagal Upload Video ke Cloud';
          //   echo json_encode($this->data);
          //   die;
          // }

        // }else{
        //      $data['url'] = '';
        //      $data['video'] = $video_loc;
        // }

      }
      if ($where['id'] != '') {
        $update = $this->model->update_data('ls_m_episode', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Episode';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Episode';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_episode', $data);
        $response['pesan'] = 'Data Episode Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Episode Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
  }

  public function unique(){
    $id        = $this->input->post('id');
    $id_course       = $this->input->post('id_course');
    $id_type_course  = $this->input->post('id_type_course');
    $order           = $this->input->post('order');
    if (!empty($id)) {
      if ($this->master_model->check_data(['id !='=> $id, 'id_type_course' => $id_type_course, 'id_course' => $id_course, 'order' => $order],'ls_m_episode')) {
        $this->form_validation->set_message('unique', 'Urutan sudah ada !');
        return false;
      }
    }else{
      if ($this->master_model->check_data(['id_type_course' => $id_type_course, 'id_course' => $id_course, 'order' => $order],'ls_m_episode')) {
        $this->form_validation->set_message('unique', 'Urutan sudah ada !');
        return false;
      }
    }

    return true;
  }

  public function check_video(){
    if (isset($_FILES['video']['size']) &&  $_FILES['video']['size'] > 400 * 1000000) {
        $this->form_validation->set_message('check_video', 'Ukuran Video Harus Kurang dari 300MB !');
        return false;
    }

    return true;
  }



  public function get_tipe_course()
  {
    $id = $this->input->post('id');
    $id_type = $this->input->post('id_type_course');
    if (!empty($id)) {
      $list_tipe      = $this->course_model->show(['id' => $id])->row()->type_course;
      $list_tipe      = str_replace("[", "", $list_tipe);
      $list_tipe      = str_replace("]", "", $list_tipe);
      $string_query   = "SELECT * FROM ls_m_type_course where id in ($list_tipe)";
      $get            = $this->course_model->get_type($string_query);
    }else{
      $get = [];
    }
    $lists = "<option value=''>-- Pilih --</option>";

    foreach($get as $data){
      $selected = '';
      if (!empty($id_type)) {
        if ($data['id'] == $id_type) {
          $selected = 'selected';
        }
      }
      $lists .= "<option value='".$data['id']."' ".$selected.">".$data['type_course']."</option>";
    }

    $callback = [
      'type' => $lists,
      'status' => 200,
      'pesan' => 'Get data success!',
    ];
    echo json_encode($callback);
  }

  public function update_status()
  {
    $id = $this->input->post('id');
    $data = $this->master_model->data('a.status, a.judul, b.kontributor', 'ls_m_episode a', ['a.id' => $id])
      ->join('ls_m_course b', 'b.id = a.id_course')
      ->get()->row();

    if ($data->status == 1) {
      $update = $this->master_model->update(['status' => 2], ['id' => $id], 'ls_m_episode');
    }else{
      $update = $this->master_model->update(['status' => 1], ['id' => $id], 'ls_m_episode');
    }
    if ($update) {
      sendNotification([
        'url' => base_url('lecturer/Lecturer_episode'),
        'to_user_id' => $data->kontributor, // Role Admin
        'message' => 'Submission Episode ' . $data->judul . ' was ' . ($data->status == 1 ? 'approved' : 'declined') . ' by Admin',
        'module' => $this->class,
      ]);

      $response['pesan'] = 'Berhasil melakukan verifikasi episode video';
      $response['status'] = 200;
    }else{
      $response['pesan'] = 'Gagal melakukan verifikasi episode video';
      $response['status'] = 500;
    }
    echo json_encode($response);
  }

  public function delete()
  {
    $where['id'] = $this->input->post('id');
    $episode = $this->master_model->delete($where, 'ls_m_episode');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($episode) {
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

  // ============================================================= MATERI

  public function materi($id){
    $get = $this->master_model->data('a.judul as judul_episode, b.judul as judul_course, c.type_course', 'ls_m_episode a', ['a.id' => $id])
    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
    ->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT')
    ->get()->row_array();
    $result['data'] = $this->get_materi($id);
    $result['id_episode'] = $id;
    $result['judul'] = '(Materi) '.$get['judul_course'].' -> '.$get['type_course'].' -> '.$get['judul_episode'];
    $result['part'] = 'Materi / Resource';
    $result['get_data_edit'] = base_url('admin/'.$this->class.'/get_modal_materi');
    $result['get_data_delete'] = base_url('admin/'.$this->class.'/delete_materi');
    $result['get_pdf_preview'] = base_url('admin/'.$this->class.'/preview_materi');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
    $this->load_template('admin/template', $this->view.'display_materi', $result, $menu->nama_menu, $sub_menu->nama_menu);
  }



  public function get_materi($id = '')
  {
    $get = $this->master_model->data('a.*', 'ls_m_sumber_episode a', ['a.id_episode' => $id])->get();
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
    if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
  }

  public function get_modal_materi()
  {
    $id = $this->input->post('id');
    $id_episode = $this->input->post('id_episode');
    if ($id != '') {
      $get = $this->master_model->data('a.*', 'ls_m_sumber_episode a', ['a.id' => $id])->get();
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save_materi');
      $response['disable'] = 'disabled';
      $response['id_episode'] = $id_episode;
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['disable'] = '';
      }
    }else{
      $response['id_episode'] = $id_episode;
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save_materi');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal_materi', $response);
  }

  public function save_materi()
  {
    $conf = array(
            array('field' => 'nama_file', 'label' => 'Judul materi', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      if ($_FILES['materi']['name'] == '') {
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
        );
      }else{
        $pdf = uploadPdf('materi', 'uploads/course_episode_resource/');
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
          'file'     => $pdf['nama_file'],
        );
        if (file_exists($this->input->post('materi_old'))) {
          unlink($this->input->post('materi_old'));
        }
      }
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $file = $this->master_model->data('file', 'ls_m_sumber_episode', ['id' => $this->input->post('id')])->get()->row();
        $update = $this->model->update_data('ls_m_sumber_episode', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Materi';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Materi';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_sumber_episode', $data);
        $response['pesan'] = 'Data Materi Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Materi Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
  }

  public function preview_materi(){
      $id = $this->input->post('id');
      $data = $this->master_model->data('file', 'ls_m_sumber_episode', ['id' => $id])->get()->row();
      $msg = ['status'=> FALSE, 'msg'=>'Gagal membuka pdf', 'url'=> ''];
      if (!empty($data)) {
          $msg = ['status'=> TRUE, 'msg'=>'Berhasil membuka pdf', 'url'=> base_url().$data->file];
      }

      echo json_encode($msg);
  }

  public function delete_materi()
  {
    $file = $this->master_model->data('file', 'ls_m_sumber_episode', ['id' => $this->input->post('id')])->get()->row();
    $where['id'] = $this->input->post('id');
    $materi = $this->master_model->delete($where, 'ls_m_sumber_episode');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($materi) {
      if (file_exists($file->file)) {
        unlink($file->file);
      }
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

  // ============================================================= TUGAS

  public function tugas($id){
    $get = $this->master_model->data('a.judul as judul_episode, b.judul as judul_course, c.type_course', 'ls_m_episode a', ['a.id' => $id])
    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
    ->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT')
    ->get()->row_array();
    $result['data'] = $this->get_tugas($id);
    $result['id_episode'] = $id;
    $result['judul'] = '(Tugas) '.$get['judul_course'].' -> '.$get['type_course'].' -> '.$get['judul_episode'];
    $result['get_data_edit'] = base_url('admin/'.$this->class.'/get_modal_tugas');
    $result['get_data_delete'] = base_url('admin/'.$this->class.'/delete_tugas');
    $result['get_pdf_preview'] = base_url('admin/'.$this->class.'/preview_tugas');
    $result['part'] = 'Tugas / Tasks';
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
    $this->load_template('admin/template', $this->view.'display_tugas', $result, $menu->nama_menu, $sub_menu->nama_menu);
  }

  public function get_tugas($id = '')
  {
    $get = $this->master_model->data('a.*', 'ls_m_tugas_episode a', ['a.id_episode' => $id])->get();
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
    if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
  }

  public function get_modal_tugas()
  {
    $id = $this->input->post('id');
    $id_episode = $this->input->post('id_episode');
    if ($id != '') {
      $get = $this->master_model->data('a.*', 'ls_m_tugas_episode a', ['a.id' => $id])->get();
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save_tugas');
      $response['disable'] = 'disabled';
      $response['id_episode'] = $id_episode;
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['disable'] = '';
      }
    }else{
      $response['id_episode'] = $id_episode;
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save_tugas');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal_tugas', $response);
  }

  public function save_tugas()
  {
    $conf = array(
            array('field' => 'nama_file', 'label' => 'Judul tugas', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      if ($_FILES['tugas']['name'] == '') {
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
        );
      }else{
        $pdf = uploadPdf('tugas', 'uploads/course_episode_tugas/');
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
          'file'     => $pdf['nama_file'],
        );
        if (file_exists($this->input->post('tugas_old'))) {
          unlink($this->input->post('tugas_old'));
        }
      }
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $file = $this->master_model->data('file', 'ls_m_tugas_episode', ['id' => $this->input->post('id')])->get()->row();
        $update = $this->model->update_data('ls_m_tugas_episode', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Materi';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Materi';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_tugas_episode', $data);
        $response['pesan'] = 'Data Materi Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Materi Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
  }

  public function preview_tugas(){
      $id = $this->input->post('id');
      $data = $this->master_model->data('file', 'ls_m_tugas_episode', ['id' => $id])->get()->row();
      $msg = ['status'=> FALSE, 'msg'=>'Gagal membuka pdf', 'url'=> ''];
      if (!empty($data)) {
          $msg = ['status'=> TRUE, 'msg'=>'Berhasil membuka pdf', 'url'=> base_url().$data->file];
      }

      echo json_encode($msg);
  }

  public function delete_tugas()
  {
    $file = $this->master_model->data('file', 'ls_m_tugas_episode', ['id' => $this->input->post('id')])->get()->row();
    $where['id'] = $this->input->post('id');
    $materi = $this->master_model->delete($where, 'ls_m_tugas_episode');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($materi) {
      if (file_exists($file->file)) {
        unlink($file->file);
      }
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

  public function monitoring_tugas($id_tugas)
  {
    $get_episode  = $this->model->show_tugas(['id' => $id_tugas])->row();

    $data['url']  = base_url()."admin/data-monitoring-tugas/".$id_tugas;
    $data['breadcrumbs']  = [
      [
        'name'  => 'Episode',
        'link'  => base_url()."admin/episode"
      ],
      [
        'name'  => 'List Tugas',
        'link'  => base_url()."admin/admin/tugas/".$get_episode->id_episode
      ],
      [
        'name'  => 'Monitoring Tugas '.$get_episode->nama_file,
        'link'  => "#"
      ],
    ];

    $this->load_template('admin/template', $this->view.'monitoring_tugas', $data);
  }

  public function data_monitoring_tugas($id_tugas)
  {
    $search  = $this->input->post('search');
    $limit  = $this->input->post('length');
    $offset = $this->input->post('start');
    if (!empty($search['value']))
    {
      $get_tasks    = $this->task_model->show(["a.id_tugas" => $id_tugas, "d.nama_depan LIKE'%".$search['value']."%'" => NULL], $limit, $offset);
      $data_tasks   = $get_tasks->result_array();
      $total     = $get_tasks->num_rows();
    } else
    {
      $get_tasks    = $this->task_model->show(['a.id_tugas' => $id_tugas], $limit, $offset);
      $data_tasks   = $get_tasks->result_array();
      $total     = $get_tasks->num_rows();
    }

    $filtered          = $this->task_model->show(['a.id_tugas' => $id_tugas])->num_rows();
    $data = array();
    $no = $_POST['start'];
    foreach ($data_tasks as $e) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $e['nama_depan'].' '.$e['nama_belakang'];
      $row[] = '<a target="_blank" href="'.base_url().'uploads/user_task/'.$e['file'].'">Link</a>';
      $row[] = ($e['nilai'] != NULL) ? $e['nilai'] : 'Belum ada nilai';
      $row[] = $e['created_at'];

      $disabled = ($e['nilai']!=NULL) ? 'none' : 'block';

      $row[] = '<button type="button" style="display:'.$disabled.'" title="Rate" class="btn btn-primary btn-nilai" data-id="'.$e['id_task'].'" data-toggle="tooltip" data-placement="top" title="Beri Nilai">
                <i class="fas fa-pen"></i></button>';
      
      $data[] = $row;
    }

    $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $total,
        "recordsFiltered" => $filtered,
        "data" => $data,
    );
    echo json_encode($output);
  }

  public function input_nilai_tugas()
  {
    $cond = 
    [
      'id'  => $this->input->post("id")
    ];

    $data = [
      'nilai' => $this->input->post("nilai"),
      'catatan' => $this->input->post("catatan"),
    ];

    $store  = $this->task_model->update($cond, $data);

    if ($store)
    {
      $this->session->set_flashdata("success", "Nilai berhasil diinput");
    } else
    {
      $this->session->set_flashdata("error", "Terjadi masalah, gagal input nilai");
    }

    redirect($_SERVER['HTTP_REFERER']);
  }

}
