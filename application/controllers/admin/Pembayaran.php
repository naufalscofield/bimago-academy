<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Pembayaran extends MY_Controller {
    public $view    = 'admin/pembayaran/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWT();
        
        $this->load->model('admin/pembayaran_model', 'model');
    }

    public function index()
    {
        $data['title']      = 'Pembayaran';
        $data['data']   = $this->data();
        $data['update'] = base_url('admin/'.$this->class.'/update_data');
        $sub_menu           = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
        $menu               = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $this->load_template('admin/template', $this->view.'display', $data, $menu->nama_menu, $sub_menu->nama_menu);
    }

    public function data()
    {
        return $this->model->show()->result_array();
    }

    public function update_data()
    {
      $id = $this->input->post('id');
      $data = $this->master_model->data('status', 'ls_t_pembayaran', ['id' => $id])->get()->row();
      if ($data->status == 1) {
        $update = $this->master_model->update(['status' => 0], ['id' => $id], 'ls_t_pembayaran');
      }else{
        $data_pembayaran = $this->master_model->data('a.id as id_pembayaran, a.*, b.*','ls_t_pembayaran a', ['a.id'=> $id])
        ->join('ls_m_user b', 'a.id_user=b.id', 'LEFT')->get()->row_array();
        $data_pembayaran['detail_pembelian'] = $this->master_model->data('a.harga, b.judul, c.type_course' ,'ls_t_detail_pembelian a',['a.id_pembayaran' => $data_pembayaran['id_pembayaran']])
        ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
        ->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT')
        ->get()->result_array();
        $this->email('payment', $data_pembayaran, $data_pembayaran['email']);
        $update = $this->master_model->update(['status' => 1], ['id' => $id], 'ls_t_pembayaran');
        ##### SEND NOTIF EMAIL
        $pembelian = $this->master_model->data('b.pembicara_webinar, b.background_webinar, b.link_zoom_webinar, b.image, a.id_course, a.id_type_course, b.judul, b.link_zoom, b.pass_zoom, b.id_zoom_webinar, b.pass_zoom_webinar, b.pelaksanaan_webinar, b.pelaksanaan_led, b.pelaksanaan_training, b.deskripsi_training, c.email, c.nama_depan', 'ls_t_detail_pembelian a', ['id_pembayaran' => $id])
                    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
                    ->join('ls_m_user c', 'a.id_user = c.id', 'LEFT')
                    ->get()->result_array();
        foreach ($pembelian as $key => $value) {
          if ($value['id_type_course'] == 3) {
            $this->email('training', $value, $value['email']);
          }elseif ($value['id_type_course'] == 2) {
            $this->email('led', $value, $value['email']);
          }elseif ($value['id_type_course'] == 4) {
            $this->email('webinar', $value, $value['email']);
          }
        }
        // $led = $this->master_model->data('deskripsi_training, judul');
      }
      if ($update) {
        $response['pesan'] = 'Berhasil melakukan update pembayaran';
        $response['status'] = 200;
      }else{
        $response['pesan'] = 'Gagal melakukan update pembayaran';
        $response['status'] = 500;
      }
      echo json_encode($response);
    }

      public function email($type, $data, $email) {
        $message = $this->template_email($type, $data);
        if ($type == 'payment') {
          $subject = 'Payment Already Confirm - '.env('APP_NAME').'';
        }elseif ($type == 'training') {
          $subject = 'Schedule List Training Online - '.env('APP_NAME').'';
        }elseif ($type == 'led') {
          $subject = 'Instructur Led Class - '.env('APP_NAME').'';
        }elseif ($type == 'webinar') {
          $subject = 'Webinar - '.env('APP_NAME').'';
        }
        $this->load->library('email', $this->config->item('email_config'));
        $this->email->from('info@solmit.academy', env('APP_NAME'));
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send())
        {
          return true;
        } else
        {
          return false;
        }
    }

    public function template_email($type, $dataset){
        $data = [
          'name'  => $dataset['nama_depan'],
          'data'  => $dataset,
          'base'  => base_url(),
          'type'  => $type
        ];
      return $this->load->view($this->view.'template_email',$data,true);
    }

}
