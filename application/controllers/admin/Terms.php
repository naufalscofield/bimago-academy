<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Terms extends MY_Controller {
  public $view    = 'admin/terms/';
	public function __construct(){
		parent::__construct();
    checkAuthJWT();
    
    $this->load->database();
		$this->load->model('admin/terms_model','model');
	}

	public function index(){
    $result['data']         = $this->model->get()->row();
    $result['breadcrumbs']  = [
      [
        'text'  => 'Terms & Conditions',
        'link'  => '#'
      ]
    ];
    $result['sub_menu'] = 'terms';
		$this->load_template('admin/template', $this->view.'display', $result);
	}

  public function update()
  {
    $cond = [
      'id'  => 1
    ];

    $data = [
      'terms' => $this->input->post('terms')
    ];

    $update = $this->model->update($cond, $data);

    if ($update)
    {
      $this->session->set_flashdata("success", "Terms condition update");
    } else
    {
      $this->session->set_flashdata("error", "Terms condition failed update");
    }

    redirect ("admin/terms");
  }

	
}
