<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . "../vendor/autoload.php";

Class Quiz extends MY_Controller {
    private $view    = 'admin/quiz/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWT();

        $this->load->model('admin/questions_model', 'model_questions');
        $this->load->model('admin/episode_model', 'model_episode');
        $this->load->model('admin/quiz_model', 'model');
    }

    public function index($id_episode)
    {
        // var_dump($id_episode); die;
        $result['data']                 = $this->model->show($id_episode);
        // var_dump($result['data']->result()); die;
        $this->model_episode->id        = $id_episode;
        $result['episode']              = $this->model_episode->get_data('ls_m_episode')->row();
        $result['judul']                = 'Quiz Course - Episode : '.$result['episode']->judul;
        $result['url_delete']           = base_url('admin/exam/delete');
        $result['url_edit']             = base_url('admin/exam/edit');
        $result['url_get_tipe_course']  = base_url('admin/exam/get_tipe_course');

        // $sub_menu                       = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
        // $menu                           = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();

        $this->load_template('admin/template', $this->view.'display', $result, 'Episode', 'Quiz');
    }

    public function store()
    {
        $data   = [
            'id_course'             => $this->input->post('id_course'),
            'id_type_course'        => $this->input->post('id_type_course'),
            'waktu'                 => $this->input->post('waktu'),
            'passing_grade'         => $this->input->post('passing_grade'),
            'aksi_waktu_habis'      => $this->input->post('aksi_waktu_habis'),
            'jumlah_kesempatan'     => $this->input->post('jumlah_kesempatan'),
            'jumlah_soal_dipakai'   => 0,
            'id_episode'            => $this->input->post('id_episode'),
            'tipe'                  => 'quiz'
        ];

        $store  = $this->model->store($data);

        if ($store)
        {
            $this->session->set_flashdata('status_store_quiz', 'success');
            $this->session->set_flashdata('msg_store_quiz', 'Store quiz success!');
        } else
        {
            $this->session->set_flashdata('status_store_quiz', 'error');
            $this->session->set_flashdata('msg_store_quiz', 'Oops, something went wrong, Store quiz failed');
        }

        redirect('admin/quiz/'.$this->input->post('id_episode'));
    }

    public function update()
    {
        $data   = [
            'id'                => $this->input->post('edit_id'),
            'id_course'         => $this->input->post('edit_id_course'),
            'waktu'             => $this->input->post('edit_waktu'),
            'passing_grade'     => $this->input->post('edit_passing_grade'),
            'aksi_waktu_habis'  => $this->input->post('edit_aksi_waktu_habis'),
            'jumlah_kesempatan' => $this->input->post('edit_jumlah_kesempatan')
        ];

        $update = $this->model->update($data, ['id' => $data['id']]);

        if ($update)
        {
            $this->session->set_flashdata('status_update_exam', 'success');
            $this->session->set_flashdata('msg_update_exam', 'Update exam success!');
        } else
        {
            $this->session->set_flashdata('status_update_exam', 'error');
            $this->session->set_flashdata('msg_update_exam', 'Oops, something went wrong, Update exam failed');
        }

        redirect('admin/quiz/'.$this->input->post('edit_id_course'));
    }
}
