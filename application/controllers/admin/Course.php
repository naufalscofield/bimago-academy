<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Course extends MY_Controller {
  public $view    = 'admin/course/';
	public function __construct(){
		parent::__construct();
    checkAuthJWT();

    $this->load->database();
		$this->load->model('admin/course_model','model');
	}

	public function index(){
    $result['data'] = $this->get();
    $result['judul'] = 'Course List';
    $result['get_data_edit'] = base_url('admin/'.$this->class.'/get_modal');
    $result['get_data_delete'] = base_url('admin/'.$this->class.'/delete');
    $result['update_status'] = base_url('admin/'.$this->class.'/update_status');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template('admin/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_m_course');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal()
	{
    $id = $this->input->post('id');
    $response['opt_kategori'] = options2('ls_m_kategori', '', 'id', 'nama_kategori', '', '- Pilih -', '', array('id' => 'ASC'));
    if ($id != '') {
      $this->model->id = $id;
      $get = $this->model->get_data('ls_m_course');
      $get_final = [];
      foreach ($get->result_array() as $key => $value) {
        $webinar_time = new DateTime($value['pelaksanaan_webinar']);
        $webinar_date = $webinar_time->format('Y-m-d');
        $webinar_time = $webinar_time->format('H:i');
        $value['pelaksanaan_webinar'] = $webinar_date.'T'.$webinar_time;
        $type = json_decode($value['type_course']);
        $value['type_course'] = $type;
        $harga = [];
        foreach ($type as $key2 => $value2) {
          $get_harga = $this->master_model->data('id_type_course, harga, discount', 'ls_m_harga_course', ['id_course' => $value['id'], 'id_type_course' => $value2])->get()->row_array();
          $harga[$value2] = $get_harga;
        }
        $value['harga'] = $harga;
        $get_final=$value;
      }
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save');
      $response['disable'] = 'disabled';
      $response['opt_modul'] = options2('ls_m_modul', '', 'id', 'modul', '',null, '', array('id' => 'ASC'));
      $response['opt_lecturer'] = options2('ls_m_user', ['role' => 2], 'id', ['nama_depan','nama_belakang', 'institusi'], '', '- Pilih -', '', array('id' => 'ASC'));
      $response['type'] = $this->master_model->data('*', 'ls_m_type_course')->get()->result_array();
      $response['kategori'] = $this->getkategori($id);
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get_final;
        $response['disable'] = '';
      }
    }else{
      $response['kategori'] = [];
      $response['type'] = $this->master_model->data('*', 'ls_m_type_course')->get()->result_array();
      $response['opt_lecturer'] = options2('ls_m_user', ['role' => 2], 'id', ['nama_depan','nama_belakang', 'institusi'], '', '- Pilih -', '', array('id' => 'ASC'));
      $response['opt_modul'] = options2('ls_m_modul', '', 'id', 'modul', '', '- Pilih -', '', array('id' => 'ASC'));
      $response['data'] = array();
      $response['simpan'] = base_url('admin/'.$this->class.'/save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
	}
  public function getkategori($id){
    $kategori_selected = [];
    $kategori = $this->master_model->data('*', 'ls_t_course_kategori_detail')->where('id_course', $id)->get()->result_array();
    if (count($kategori) > 0) {
      foreach ($kategori as $value) {
        $kategori_selected[] = $value['id'];  
      }      
    }
    return $kategori_selected;
  }

	public function save()
	{
    $conf = array(
            array('field' => 'id_modul', 'label' => 'Modul', 'rules' => 'trim|required|callback_unique'),
            array('field' => 'judul', 'label' => 'Judul', 'rules' => 'trim|required'),
            array('field' => 'deskripsi', 'label' => 'Deskripsi', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');
    $this->form_validation->set_message('numeric', '%s harus berisi nomor.');
    $this->form_validation->set_message('matches', '%s tidak cocok.');
    $this->form_validation->set_message('min_length', '%s minimal %s digit.');
    $this->form_validation->set_message('valid_email', '%s harus valid.');
    $this->form_validation->set_message('is_unique', '%s tidak boleh sama.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      if ($_FILES['image']['name'] != '') {
        if (file_exists($this->input->post('image_old'))) {
          unlink($this->input->post('image_old'));
        }
        $cek_uploads = uploadGambar('image', 'uploads/course_gambar/');
        $image = $cek_uploads['nama_file'];
      }else{
        $image = $this->input->post('image_old');
      }

      $pelaksanaan_led = null;
      $pelaksanaan_training = null;
      $deskripsi_training = null;
      $link_zoom = null;
      $pass_zoom = null;
      $id_zoom_webinar = null;
      $pass_zoom_webinar = null;
      $pelaksanaan_webinar = null;
      $pembicara_webinar = null;
      $background = null;
      foreach ($this->input->post('type_course') as $key => $value) {
        if ($value == 2) {
          $pelaksanaan_led = $this->input->post('pelaksanaan_led');
          $link_zoom = $this->input->post('link_zoom');
          $pass_zoom = $this->input->post('pass_zoom');
        }elseif ($value == 3) {
          $pelaksanaan_training = $this->input->post('pelaksanaan_training');
          $deskripsi_training = $this->input->post('deskripsi_training');
        }elseif ($value == 4) {
          $id_zoom_webinar = $this->input->post('id_zoom_webinar');
          $pass_zoom_webinar = $this->input->post('pass_zoom_webinar');
          $pelaksanaan_webinar = $this->input->post('pelaksanaan_webinar');
          $pembicara_webinar = $this->input->post('pembicara_webinar');
          if ($_FILES['background_webinar']['name'] != '') {
            if (file_exists($this->input->post('background_webinar_old'))) {
              unlink($this->input->post('background_webinar_old'));
            }
            $cek_uploads = uploadGambar('background_webinar', 'uploads/background_webinar/');
            $background = $cek_uploads['nama_file'];
          }else{
            $background = $this->input->post('image_old');
          }
        }else{

        }
      }
      $where['id'] = $this->input->post('id');
      $data = array(
        'id_modul' => $this->input->post('id_modul'),
        'judul' => $this->input->post('judul'),
        'deskripsi_singkat' => $this->input->post('deskripsi_singkat'),
        'deskripsi' => $this->input->post('deskripsi'),
        'image' => $image,
        'kontributor' => $this->input->post('kontributor'),
        'link_zoom' => $link_zoom,
        'pass_zoom' => $pass_zoom,
        'url_youtube' => $this->input->post('url_youtube'),
        'type_course' => json_encode($this->input->post('type_course')),
        'pelaksanaan_led' => $pelaksanaan_led,
        'pelaksanaan_training' => $pelaksanaan_training,
        'deskripsi_training' => $deskripsi_training,
        'id_zoom_webinar' => $id_zoom_webinar,
        'pass_zoom_webinar' => $pass_zoom_webinar,
        'pelaksanaan_webinar' => $pelaksanaan_webinar,
        'pembicara_webinar' => $pembicara_webinar,
        'background_webinar' => $background,
        'harga' => 1,
        'status' => 2,
      );
      if ($where['id'] != '') {
        $update = $this->model->update_data('ls_m_course', $data, $where);
        $this->master_model->delete(['id_course'=> $this->input->post('id')],'ls_m_harga_course');
        foreach ($this->input->post('type_course') as $key => $value) {
          $check_harga = $this->master_model->check_data(['id_course' => $this->input->post('id'), 'id_type_course' => $value], 'ls_m_harga_course');
          if ($value == 2) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_2'), 'discount' => $this->input->post('discount_2')], ['id_course' => $this->input->post('id'), 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $this->input->post('id'), 'id_type_course' => $value, 'harga' => $this->input->post('harga_2'), 'discount' => $this->input->post('discount_2')], 'ls_m_harga_course');
            }
          }elseif ($value == 3) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_3'), 'discount' => $this->input->post('discount_3')], ['id_course' => $this->input->post('id'), 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $this->input->post('id'), 'id_type_course' => $value, 'harga' => $this->input->post('harga_3'), 'discount' => $this->input->post('discount_3')], 'ls_m_harga_course');
            }
          }elseif ($value == 4) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_4'), 'discount' => $this->input->post('discount_4')], ['id_course' => $this->input->post('id'), 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $this->input->post('id'), 'id_type_course' => $value, 'harga' => $this->input->post('harga_4'), 'discount' => $this->input->post('discount_4')], 'ls_m_harga_course');
            }
          }elseif ($value == 1) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_1'), 'discount' => $this->input->post('discount_1')], ['id_course' => $this->input->post('id'), 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $this->input->post('id'), 'id_type_course' => $value, 'harga' => $this->input->post('harga_1'), 'discount' => $this->input->post('discount_1')], 'ls_m_harga_course');
            }
          }
        }
        $response['pesan'] = 'Gagal Melakukan Update Course';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Course';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_course', $data);
        $where['id'] = $this->db->insert_id();
        foreach ($this->input->post('type_course') as $key => $value) {
          $check_harga = $this->master_model->check_data(['id_course' => $create, 'id_type_course' => $value], 'ls_m_harga_course');
          if ($value == 2) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_2'), 'discount' => $this->input->post('discount_2')], ['id_course' => $create, 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $create, 'id_type_course' => $value, 'harga' => $this->input->post('harga_2'), 'discount' => $this->input->post('discount_2')], 'ls_m_harga_course');
            }
          }elseif ($value == 3) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_3'), 'discount' => $this->input->post('discount_3')], ['id_course' => $create, 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $create, 'id_type_course' => $value, 'harga' => $this->input->post('harga_3'), 'discount' => $this->input->post('discount_3')], 'ls_m_harga_course');
            }
          }elseif ($value == 4) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_4'), 'discount' => $this->input->post('discount_4')], ['id_course' => $create, 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $create, 'id_type_course' => $value, 'harga' => $this->input->post('harga_4'), 'discount' => $this->input->post('discount_4')], 'ls_m_harga_course');
            }
          }elseif ($value == 1) {
            if ($check_harga) {
              $this->master_model->update(['harga' => $this->input->post('harga_1'), 'discount' => $this->input->post('discount_1')], ['id_course' => $create, 'id_type_course' => $value], 'ls_m_harga_course');
            }else{
              $this->master_model->save(['id_course' => $create, 'id_type_course' => $value, 'harga' => $this->input->post('harga_1'), 'discount' => $this->input->post('discount_1')], 'ls_m_harga_course');
            }
          }
        }
        $response['pesan'] = 'Data Course Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Course Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
      ### INPUT KATEGORI
      $kategori = $this->input->post('kategori');
      if (is_array($kategori) && count($kategori) > 0) {
        $this->master_model->delete($where,'ls_t_course_kategori_detail');
        foreach ($kategori as $value) {
          $data_kategori = ['id_kategori' => $value , 'id_course'=> $where['id']];
          $this->master_model->save($data_kategori, 'ls_t_course_kategori_detail');
        }
      }
    }
      echo json_encode($response);
	}

  public function unique(){
		$id 				= $this->input->post('id');
    $judul 		= strtolower($this->input->post('judul'));
    $type 		= $this->input->post('type_course');
		if (!empty($id)) {
			if ($this->master_model->check_data(['id !='=> $id, 'lower(judul)' => $judul],'ls_m_course')) {
				$this->form_validation->set_message('unique', 'Judul Sudah Ada !');
				return false;
			}
		}else{
			if ($this->master_model->check_data(['judul' => $judul],'ls_m_course')) {
				$this->form_validation->set_message('unique', 'Judul Sudah Ada !');
				return false;
			}
		}
    if (is_array($type)) {
      if (count($type) < 1) {
        $this->form_validation->set_message('unique', 'Type course harus di pilih minimal 1 !');
        return false;
      }
    }else{
      $this->form_validation->set_message('unique', 'Type course harus di pilih minimal 1 !');
      return false;
    }

		return true;
	}

  public function update_status()
  {
    $id = $this->input->post('id');
    $data = $this->master_model->data('*', 'ls_m_course', ['id' => $id])->get()->row();
    if ($data->status == 1) {
      $update = $this->master_model->update(['status' => 2], ['id' => $id], 'ls_m_course');
    }else{
      $update = $this->master_model->update(['status' => 1], ['id' => $id], 'ls_m_course');
    }
    if ($update) {
      sendNotification([
        'url' => base_url('lecturer/Lecturer_course'),
        'to_user_id' => $data->kontributor, // Role Admin
        'message' => 'Submission Course ' . $data->judul . ' was ' . ($data->status == 1 ? 'approved' : 'declined') . ' by Admin',
        'module' => $this->class,
      ]);

      $response['pesan'] = 'Berhasil melakukan verifikasi pelatihan';
      $response['status'] = 200;
    }else{
      $response['pesan'] = 'Gagal melakukan verifikasi pelatihan';
      $response['status'] = 500;
    }
    echo json_encode($response);
  }

	public function delete()
  {
    $file = $this->master_model->data('image', 'ls_m_course', ['id' => $this->input->post('id')])->get()->row();
    $where_course['id'] = $this->input->post('id');
    $course = $this->master_model->delete($where_course, 'ls_m_course');
    $where_harga['id_course'] = $this->input->post('id');
    $harga = $this->master_model->delete($where_harga, 'ls_m_harga_course');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($course && $harga) {
      if (file_exists($file->image)) {
        unlink($file->image);
      }
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

}
