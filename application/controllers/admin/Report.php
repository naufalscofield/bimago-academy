<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Report extends MY_Controller {
    public $view    = 'admin/report/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWT();

        $this->load->model('admin/report_model', 'model');
    }

    public function index($id='')
    {
        $data['title']      = 'Report';
        $data['url_data']   = base_url().$this->view.'data'.(!empty($id) ? '/'.$id :'');
        $data['get_data_delete'] = base_url($this->view.'/delete');

        $sub_menu           = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
        $menu               = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $this->load_template('admin/template', $this->view.'display', $data, $menu->nama_menu, $sub_menu->nama_menu);
    }

    public function data($id='',$offset='', $limit='')
    {
        $search  = $this->input->post('search');
        $limit  = $this->input->post('length');
        $offset = $this->input->post('start');
        $data   = $this->model->show($id,$limit,$offset,$search['value'])->result_array();
        $list = [];
        foreach ($data as $key => $value) {
            $value['no'] = $key+1;
            $list[] = $value;
        }
        $count  = $this->model->show()->num_rows();
        $data = array();
        $no = 0;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $count,
            "recordsFiltered" => $this->model->show($id,$limit,$offset,$search['value'])->num_rows(),
            "data" => $list,
        );

        echo json_encode($output);
    }
   
    public function delete()
    {
      $where['id'] = $this->input->post('id');
      $status = $this->master_model->update(['status' => 1],$where, 'ls_m_course');
      $response['pesan'] = 'Gagal Menonaktifkan Course';
      $response['status'] = 404;
      if ($status) {
        $response['pesan'] = 'Berhasil Menonaktifkan Course';
        $response['status'] = 200;
      }
      echo json_encode($response);
    }
}
