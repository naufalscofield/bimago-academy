<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
    {
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET,POST,PUT, OPTIONS');
		header('Access-Control-Allow-Headers: X-Requested-With');
        parent::__construct();
	    $params = array('server_key' => 'Mid-server-Zv30B2eTAF20Ilp8IPIVg9k0', 'production' => true);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->helper('url');
		$this->load->model('users/users_course_model','course_model');
		$this->load->model('lecturer/lecturer_saldo_model','saldo_model');
		$this->load->model('admin/cut_off_model','cut_off_model');
		$this->load->model('admin/pembayaran_model','model_pembayaran');
    }

	public function index()
	{
		// echo 'test notification handler';
		$json_result = file_get_contents('php://input');
		$result = json_decode($json_result);
		if($result){
			$notif = $this->veritrans->status($result->order_id);
		}

		error_log(print_r($result,TRUE));

		//notification handler sample


		$transaction = $notif->transaction_status;
		$type = $notif->payment_type;
		$order_id = $notif->order_id;
		$fraud = $notif->fraud_status;
		if ($transaction == 'capture') {

		  // For credit card transaction, we need to check whether transaction is challenge by FDS or not
		  if ($type == 'credit_card'){
		    if($fraud == 'challenge'){
		      // TODO set payment status in merchant's database to 'Challenge by FDS'
		      // TODO merchant should decide whether this transaction is authorized or not in MAP
		      // echo "Transaction order_id: " . $order_id ." is challenged by FDS";
		      }
		      else {
		      // TODO set payment status in merchant's database to 'Success'
		      echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
		      }
		    }
		  }
		else if ($transaction == 'settlement'){	

				$this->db->where(['no_pesanan' => $order_id]);
				$this->db->update('ls_t_pembayaran', ['status' => 1]);
	
				if ($this->db->affected_rows() > 0) {

					$data_pembayaran = $this->master_model->data('a.id as id_pembayaran, a.*,b.*','ls_t_pembayaran a', ['a.no_pesanan'=> $order_id])->join('ls_m_user b', 'a.id_user=b.id', 'LEFT')->get()->row_array();
					$data_pembayaran['detail_pembelian'] = $this->master_model->data('a.harga, b.judul, c.type_course' ,'ls_t_detail_pembelian a',['a.id_pembayaran' => $data_pembayaran['id_pembayaran']])
					->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
					->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT')
					->get()->result_array();

			       	send_email_god('payment', $data_pembayaran, $data_pembayaran['email']);
			        // $pembelian = $this->master_model->data('a.id_course, a.id_type_course, b.judul, b.link_zoom, b.pass_zoom, b.pelaksanaan_led, b.pelaksanaan_training, b.deskripsi_training, c.email, c.nama_depan', 'ls_t_detail_pembelian a', ['id_pembayaran' => $id])
			        //             ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
			        //             ->join('ls_m_user c', 'a.id_user = c.id', 'LEFT')
			        //             ->get()->result_array();
			        // foreach ($pembelian as $key => $value) {
			        //   if ($value['id_type_course'] == 3) {
			        //     send_email_god('training', $value, $value['email']);
			        //   }elseif ($value['id_type_course'] == 2) {
			        //     send_email_god('led', $value, $value['email']);
			        //   }
			        // }
					$data_pembayaran 	= $this->model_pembayaran->show(['a.no_pesanan' => $order_id])->result_array();
					foreach ($data_pembayaran as $idx => $dp)
					{
						if ($dp['status'] == 1)
						{
							//Tambahan insert saldo
							$get_potongan_platform  = $this->cut_off_model->show(['kode' => 'bee_lms'])->row();
							if ($get_potongan_platform->jenis == 'persentase')
							{
								$potongan_platform  = ($get_potongan_platform->potongan / 100) * $dp['harga'];
							} else
							{
								$potongan_platform  = $get_potongan_platform->potongan;
							}
			
							$get_potongan_payment_gateway  = $this->cut_off_model->show(['kode' => $dp['metode_pembayaran']])->row();
							if ($get_potongan_payment_gateway->jenis == 'persentase')
							{
								$potongan_payment_gateway  = ($get_potongan_payment_gateway->potongan / 100) * $dp['harga'];
							} else
							{
								$potongan_payment_gateway  = $get_potongan_payment_gateway->potongan;
							}
			
							$total_potongan = (int)$potongan_platform + (int)$potongan_payment_gateway;
			
							$id_lecturer = $this->course_model->show(['id' => $dp['id_course']])->row()->kontributor;
							$check_saldo = $this->saldo_model->show(['id_lecturer' => $id_lecturer])->row();
			
							if ($check_saldo == NULL)
							{
								$data_saldo = [
								'id_lecturer' => $id_lecturer,
								'saldo'       => (int)$dp['harga'] - (int)$total_potongan
								];
			
								$insert_saldo = $this->saldo_model->store($data_saldo);
							} else
							{
								$curr_saldo = $check_saldo->saldo;
			
								$data_saldo = [
								'id_lecturer' => $id_lecturer,
								'saldo'       => (int)$curr_saldo + ((int)$dp['harga'] - (int)$total_potongan),
								];
			
								$update_saldo = $this->master_model->update($data_saldo, ['id_lecturer' => $id_lecturer], 'ls_t_saldo_lecturer');
							}
							//Akhir insert saldo
						}
					}

				   return $this->output
		            ->set_content_type('application/json')
		            ->set_status_header(200)
		            ->set_output(json_encode(array(
		                    'text' => 'SUCCES UPDATE STATUS PAYMENT',
		                    'type' => 'success'
	           		)));

				} else {
			        return $this->output
		            ->set_content_type('application/json')
		            ->set_status_header(503)
		            ->set_output(json_encode(array(
		                    'text' => 'Failed !',
		                    'query' => $this->db->last_query(),
		                    'type' => 'danger'
	           		)));
				}

		  // echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
		  }
		  else if($transaction == 'pending'){
		  // TODO set payment status in merchant's database to 'Pending'
		  // echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
		  }
		  else if ($transaction == 'deny') {
		  // TODO set payment status in merchant's database to 'Denied'
		  // echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
		}

	}

}
