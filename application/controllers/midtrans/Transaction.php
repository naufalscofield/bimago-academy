<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
    {
        parent::__construct();
        $params = array('server_key' => 'Mid-server-Zv30B2eTAF20Ilp8IPIVg9k0', 'production' => true);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->helper('url');
		$this->load->model('admin/Pembayaran_model', 'model_pembayaran');
		$this->load->model('users/users_course_model','course_model');
		$this->load->model('lecturer/lecturer_saldo_model','saldo_model');
		$this->load->model('admin/cut_off_model','cut_off_model');
    }

    public function index()
    {
    	$this->load->view('midtrans/transaction');
    }

    public function process()
    {
    	$order_id = $this->input->post('order_id');
    	$action = $this->input->post('action');
    	switch ($action) {
		    case 'status':
		        $this->status($order_id);
		        break;
		    case 'approve':
		        $this->approve($order_id);
		        break;
		    case 'expire':
		        $this->expire($order_id);
		        break;
		   	case 'cancel':
		        $this->cancel($order_id);
		        break;
		}

    }

	public function status($order_id)
	{
		// echo 'test get status </br>';
		$response['pesan'] 	= 'Gagal update status pembayaran';
		$data_pembayaran 	= $this->model_pembayaran->show(['a.no_pesanan' => $order_id])->result_array();

		$data = ($this->veritrans->status($order_id) );
		if ($data) {
			switch ($data->transaction_status) {
		    case 'expire':
        		$update = $this->master_model->update(['status' => 2], ['no_pesanan' => $order_id], 'ls_t_pembayaran');
        		$response['pesan'] = 'Berhasil melakukan update pembayaran';
		        break;
		    case 'settlement':
        		$update = $this->master_model->update(['status' => 1], ['no_pesanan' => $order_id], 'ls_t_pembayaran');
        		$response['pesan'] = 'Berhasil melakukan update pembayaran';

				foreach ($data_pembayaran as $idx => $dp)
				{
					if ($dp['status'] == 0)
					{
						//Tambahan insert saldo
						$get_potongan_platform  = $this->cut_off_model->show(['kode' => 'bee_lms'])->row();
						if ($get_potongan_platform->jenis == 'persentase')
						{
						  $potongan_platform  = ($get_potongan_platform->potongan / 100) * $dp['harga'];
						} else
						{
						  $potongan_platform  = $get_potongan_platform->potongan;
						}
		
						$get_potongan_payment_gateway  = $this->cut_off_model->show(['kode' => $dp['metode_pembayaran']])->row();
						if ($get_potongan_payment_gateway->jenis == 'persentase')
						{
						  $potongan_payment_gateway  = ($get_potongan_payment_gateway->potongan / 100) * $dp['harga'];
						} else
						{
						  $potongan_payment_gateway  = $get_potongan_payment_gateway->potongan;
						}
		
						$total_potongan = (int)$potongan_platform + (int)$potongan_payment_gateway;
		
						$id_lecturer = $this->course_model->show(['id' => $dp['id_course']])->row()->kontributor;
						$check_saldo = $this->saldo_model->show(['id_lecturer' => $id_lecturer])->row();
		
						if ($check_saldo == NULL)
						{
						  $data_saldo = [
							'id_lecturer' => $id_lecturer,
							'saldo'       => (int)$dp['harga'] - (int)$total_potongan
						  ];
		
						  $insert_saldo = $this->saldo_model->store($data_saldo);
						} else
						{
						  $curr_saldo = $check_saldo->saldo;
		
						  $data_saldo = [
							'id_lecturer' => $id_lecturer,
							'saldo'       => (int)$curr_saldo + ((int)$dp['harga'] - (int)$total_potongan),
						  ];
		
						  $update_saldo = $this->master_model->update($data_saldo, ['id_lecturer' => $id_lecturer], 'ls_t_saldo_lecturer');
						}
						//Akhir insert saldo
					}
				}

		        break;
			}
		}

		
		echo json_encode($response);
	}

	public function cancel($order_id)
	{
		// echo 'test cancel trx </br>';
		echo $this->veritrans->cancel($order_id);
	}

	public function approve($order_id)
	{
		// echo 'test get approve </br>';
		return ($this->veritrans->approve($order_id) );
	}

	public function expire($order_id)
	{
		// echo 'test get expire </br>';
		return ($this->veritrans->expire($order_id) );
	}
}
