<?php

include APPPATH . "../vendor/autoload.php";

Class Common extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function read_notif($notif_id) {
    readNotification($notif_id);
  }
}