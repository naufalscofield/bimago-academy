<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Lecturer_Voucher extends MY_Controller {
  public $view    = 'lecturer/voucher/';
	public function __construct(){
		parent::__construct();
    checkAuthJWTLec();
    
    $this->load->database();
		$this->load->model('admin/voucher_model','model');
	}

	public function index(){
    $result['data'] = $this->get();
    $result['judul'] = 'Voucher List';
    $result['get_data_edit'] = base_url('lecturer/'.$this->class.'/get_modal');
    $result['get_data_delete'] = base_url('lecturer/'.$this->class.'/delete');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get();
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function show($id)
  {
    $cond = [
      'id'  => $id
    ];

    $get  = $this->model->show($cond)->row();

    echo json_encode($get);
  }

  public function store()
  {
    $data = [
      'kode'  => $this->input->post('kode'),
      'discount'  => $this->input->post('discount'),
      'expired'  => $this->input->post('expired'),
      'kuota'  => $this->input->post('kuota'),
    ];

    $store  = $this->model->store($data);

    if ($store)
    {
      $this->session->set_flashdata("success", "Voucher baru berhasil ditambahkan");
    } else
    {
      $this->session->set_flashdata("error", "Terjadi kesalahan,voucher baru gagal ditambahkan");
    }

    redirect ('lecturer/voucher');  
  }

  public function update()
  {
    $data = [
      'kode'  => $this->input->post('edit_kode'),
      'discount'  => $this->input->post('edit_discount'),
      'expired'  => $this->input->post('edit_expired'),
      'kuota'  => $this->input->post('edit_kuota'),
    ];

    $cond = [
      'id'  => $this->input->post('edit_id')
    ];

    $store  = $this->model->update($cond, $data);

    if ($store)
    {
      $this->session->set_flashdata("success", "Voucher berhasil diperbarui");
    } else
    {
      $this->session->set_flashdata("error", "Terjadi kesalahan,voucher gagal diperbarui");
    }

    redirect ('lecturer/voucher');  
  }
  
  public function delete($id)
  {
    $cond = [
      'id'  => $id
    ];
    
    $delete = $this->model->delete($cond);

    if ($delete)
    {
      $this->session->set_flashdata("success", "Voucher berhasil dihapus");
    } else
    {
      $this->session->set_flashdata("error", "Terjadi kesalahan,voucher gagal dihapus");
    }

    $data = [
      'status'  => 200
    ];

    echo json_decode($data);
  }

  public function get_modal()
	{
    $id = $this->input->post('id');
    if ($id != '') {
      $this->model->id = $id;
      $get = $this->model->get_data('ls_m_modul');
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
      $response['disable'] = 'disabled';
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
        $response['disable'] = '';
      }
    }else{
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
	}

	public function save()
	{
    $conf = array(
            array('field' => 'modul', 'label' => 'Modul', 'rules' => 'trim|required|callback_unique'),
            // array('field' => 'gambar', 'label' => 'Gambar', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');
    $this->form_validation->set_message('numeric', '%s harus berisi nomor.');
    $this->form_validation->set_message('matches', '%s tidak cocok.');
    $this->form_validation->set_message('min_length', '%s minimal %s digit.');
    $this->form_validation->set_message('valid_email', '%s harus valid.');
    $this->form_validation->set_message('is_unique', '%s tidak boleh sama.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      if ($_FILES['gambar']['name'] == '') {
        $gambar = $this->input->post('gambar_old');
        $data = array(
          'modul' => $this->input->post('modul'),
          'gambar' => $gambar,
        );
      }else{
        if (file_exists($this->input->post('gambar_old'))) {
          unlink($this->input->post('gambar_old'));
        }
        $cek_uploads = uploadGambar('gambar', 'uploads/main_course_gambar/');
        $gambar = $cek_uploads['nama_file'];
        $data = array(
          'modul' => $this->input->post('modul'),
          'gambar' => $gambar,
        );
      }
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $update = $this->model->update_data('ls_m_modul', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Main Course';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Main Course';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_modul', $data);
        $response['pesan'] = 'Data Main Course Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Main Course Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
	}

  public function unique(){
		$id 				= $this->input->post('id');
    $modul 		= strtolower($this->input->post('modul'));
		if (!empty($id)) {
			if ($this->master_model->check_data(['id !='=> $id, 'lower(modul)' => $modul],'ls_m_modul')) {
				$this->form_validation->set_message('unique', 'Modul Sudah Ada !');
				return false;
			}
		}else{
			if ($this->master_model->check_data(['modul' => $modul],'ls_m_modul')) {
				$this->form_validation->set_message('unique', 'Modul Sudah Ada !');
				return false;
			}
		}

		return true;
	}

}
