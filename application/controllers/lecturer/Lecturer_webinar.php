<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Lecturer_webinar extends MY_Controller {
  public $view    = 'lecturer/webinar/';
	public function __construct(){
		parent::__construct();
    checkAuthJWTLec();

		$this->load->model('lecturer/lecturer_webinar_model','model');
	}

	public function index(){
    $result['data'] = $this->get();
    // dd($result['data']);
    $result['judul'] = 'Monitoring Webinar';
    $result['member'] = base_url('lecturer/'.$this->class.'/detail/');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

  public function detail($id){
    $result['data'] = $this->get_detail($id);
    $data = $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row();
    // dd($result['data']);
    $result['judul'] = $data->judul;
    $result['email'] = base_url('lecturer/'.$this->class.'/email');
    $result['id_course'] = $id;
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display_member', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_m_course');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_detail($id)
  {
    $this->model->id = $id;
    $get = $this->model->get_data_member('ls_t_detail_pembelian');
    $response['pesan'] = 'data tidak ada';
    $response['status'] = 404;
    $response['data'] = array();
    if ($get -> num_rows() > 0) {
      $response['pesan'] = 'data ada';
      $response['status'] = 200;
      $response['data'] = $get->result_array();
    }
    return $response;
  }

  public function email()
  {
    $id = $this->input->post('id_course');
    $email = $this->input->post('email');
    // $cek = [
    //   'm.syahidnurrohman@gmail.com',
    //   'mer.neu.min.e.z@gmail.com',
    //   'rahmathidayat.ie@gmail.com',
    //   'dewianidj@gmail.com',
    //   'selmarizky26@gmail.com',
    //   'zaenabandy@gmail.com',
    //   'ahmadyakin80@gmail.com',
    //   'randu.arthayudha@gmail.com',
    //   'yusakil@yahoo.com',
    //   'ratihafrianti1985@gmail.com',
    //   'hanoof.a.s@hotmail.com',
    //   'ahfafaeza@gmail.com',
    //   'oktaviani.119140014@student.itera.ac.id',
    //   'suhendri2012210002@gmail.com',
    //   'robi.119140146@student.itera.ac.id',
    //   'markus.118140037@student.itera.ac.id',
    //   'bintangbgs16@gmail.com',
    //   'atikanovitawe@gmail.com',
    //   'm.ern.eu.m.i.ne.z@gmail.com',
    //   'nuki.bambang@bppt.go.id',
    //   'yarisnuryana93@gmail.com',
    //   'muhammad.119140212@student.itera.ac.id',
    //   'medalona10@gmai.com',
    //   'yeilinarizka@gmail.com',
    //   'misbahuddin.nsn@gmail.com',
    //   'andina@amikompurwokerto.ac.id',
    //   'royana@unram.ac.id',
    //   'valendeandra@gmail.com',
    //   'marc@sniukas.com',
    //   'ekofajararianto@me.com',
    //   'oksanasuxanova@mail.com',
    //   'tabitha.dutoit@cobaltconsulting.co.za',
    //   'zdn2ali@yahoo.com',
    //   'johaniip@gmail.com',
    //   'satria.u@gmail.com',
    //   'muhammad.arif@moratelindo.co.id',
    //   'adenarrasyid@gmail.com',
    // ];
    //
    // $terkirim = [];
    //
    // $webinar = $this->master_model->data('b.judul, b.pembicara_webinar, b.background_webinar, b.link_zoom_webinar, b.image, b.id_zoom_webinar, b.pass_zoom_webinar, b.pelaksanaan_webinar', 'ls_m_course b', ['b.id' => $id])
    //   ->get()->row_array();
    //
    // $no = 0;
    // foreach ($cek as $value) {
    //   $send = send_email_god('webinar', $webinar, $value);
    //   if ($send) {
    //     $no += 1;
    //     $terkirim[] = $value;
    //   }
    // }
    // $result = [
    //   'no' => $no,
    //   'terkirim' => $terkirim,
    // ];
    // echo json_encode($result);

    if ($this->input->post('member')) {
      $member = $this->input->post('member');
      $webinar = $this->master_model->data('b.judul, b.pembicara_webinar, b.background_webinar, b.link_zoom_webinar, b.image, b.id_zoom_webinar, b.pass_zoom_webinar, b.pelaksanaan_webinar', 'ls_m_course b', ['b.id' => $id])
      ->get()->row_array();
      $no = 0;
      foreach ($member as $key => $value) {
        $send = send_email_god('webinar', $webinar, $value);
        if ($send) {
          $no += 1;
        }
      }
      echo $no;
    }
  }

}
