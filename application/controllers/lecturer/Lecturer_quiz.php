<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . "../vendor/autoload.php";

Class Lecturer_quiz extends MY_Controller {
    private $view    = 'lecturer/quiz/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWTLec();

        $this->load->model('lecturer/lecturer_questions_model', 'model_questions');
        $this->load->model('lecturer/lecturer_episode_model', 'model_episode');
        $this->load->model('lecturer/lecturer_quiz_model', 'model');
    }

    public function index($id_episode)
    {
        $result['data']                 = $this->model->show($id_episode);
        $this->model_episode->id        = $id_episode;
        $result['episode']              = $this->model_episode->get_data('ls_m_episode')->row();
        $result['course']               = $this->master_model->data('judul', 'ls_m_course', ['id' => $result['episode']->id_course])->get()->row();
        $result['judul']                = 'Quiz Course - Episode : '.$result['episode']->judul;
        $result['url_delete']           = base_url('lecturer/lecturer_exam/delete');
        $result['url_edit']             = base_url('lecturer/lecturer_exam/edit');
        $result['url_get_tipe_course']  = base_url('lecturer/lecturer_exam/get_tipe_course');

        // $sub_menu                       = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => 'lecturer_episode'])->get()->row();
        // $menu                           = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();

        $this->load_template_lecturer('lecturer/template', $this->view.'display', $result);
    }

    public function store()
    {
        $id_course = $this->input->post('id_course');
        $id_type_course = $this->input->post('id_type_course');
        $id_episode = $this->input->post('id_episode');
        $data   = [
            'id_course'             => $id_course,
            'id_type_course'        => $id_type_course,
            'waktu'                 => $this->input->post('waktu'),
            'passing_grade'         => $this->input->post('passing_grade'),
            'aksi_waktu_habis'      => $this->input->post('aksi_waktu_habis'),
            'jumlah_kesempatan'     => $this->input->post('jumlah_kesempatan'),
            'jumlah_soal_dipakai'   => 0,
            'id_episode'            => $id_episode,
            'tipe'                  => 'quiz'
        ];

        $store  = $this->model->store($data);

        if ($store)
        {
            $data_course = $this->master_model->data('*', 'ls_m_course', ['id' => $id_course])->get()->row();
            $data_type_course = $this->master_model->data('*', 'ls_m_type_course', ['id' => $id_type_course])->get()->row();

            sendNotification([
                'url' => base_url('admin/quiz/' . $id_episode),
                'to_role_id' => 1, // Role Admin
                'message' => 'Created new Quiz in Course ' . $data_course->judul . ' (' . $data_type_course->type_course . ')',
                'module' => $this->class,
            ]);

            $this->session->set_flashdata('status_store_quiz', 'success');
            $this->session->set_flashdata('msg_store_quiz', 'Store quiz success!');
        } else
        {
            $this->session->set_flashdata('status_store_quiz', 'error');
            $this->session->set_flashdata('msg_store_quiz', 'Oops, something went wrong, Store quiz failed');
        }

        redirect('lecturer/quiz/'.$this->input->post('id_episode'));
    }
}
