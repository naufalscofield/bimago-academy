<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Lecturer_exam extends MY_Controller {
    public $view    = 'lecturer/exam/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWTLec();
        
        $this->load->model('lecturer/lecturer_exam_model', 'model');
        $this->load->model('lecturer/lecturer_course_model', 'course_model');
    }

    public function index($id='')
    {
        $result['data']                 = $this->model->get();
        $result['judul']                = get_value('judul','ls_m_course',['id' => $id ]);
        $result['url_delete']           = base_url('lecturer/'.$this->class.'/delete');
        $result['url_edit']             = base_url('lecturer/'.$this->class.'/edit');
        $result['url_get_tipe_course']  = base_url('lecturer/'.$this->class.'/get_tipe_course');
        $result['opt_course']           = options2('ls_m_course', ['status' => 2, 'kontributor' => $this->session->userdata('id'),'id' => $id], 'id', 'judul', '', '- Pilih -', '', array('id' => 'ASC'));
        $sub_menu                       = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
        // $menu                           = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();

        $this->load_template_lecturer('lecturer/template', $this->view.'display', $result);
        // $this->load_template_lecturer('lecturer/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
    }

    public function store()
    {
        $id_course = $this->input->post('id_course');
        $id_type_course = $this->input->post('id_type_course');

        $check_exam = $this->master_model->check_data(['id_course' => $id_course, 'id_type_course' => $id_type_course, 'tipe' => 'exam'], 'ls_m_exam');
        if ($check_exam) {
            $this->session->set_flashdata('status_store_exam', 'error');
            $this->session->set_flashdata('msg_store_exam', 'Oops, exam with that course and type course already exist!');
        }else{
            $data   = [
                'id_course'             => $this->input->post('id_course'),
                'id_type_course'        => $this->input->post('id_type_course'),
                'waktu'                 => $this->input->post('waktu'),
                'passing_grade'         => $this->input->post('passing_grade'),
                'aksi_waktu_habis'      => $this->input->post('aksi_waktu_habis'),
                'jumlah_kesempatan'     => $this->input->post('jumlah_kesempatan'),
                'jumlah_soal_dipakai'   => 0,
                'id_episode'            => NULL,
                'tipe'                  => 'exam',
              ];

            $store  = $this->model->store($data);

            if ($store)
            {
                $this->session->set_flashdata('status_store_exam', 'success');
                $this->session->set_flashdata('msg_store_exam', 'Store exam success!');

                $data_course = $this->master_model->data('*', 'ls_m_course', ['id' => $id_course])->get()->row();
                $data_type_course = $this->master_model->data('*', 'ls_m_type_course', ['id' => $id_type_course])->get()->row();

                sendNotification([
                    'url' => base_url('admin/exam'),
                    'to_role_id' => 1, // Role Admin
                    'message' => 'Created new Exam in Course ' . $data_course->judul . ' (' . $data_type_course->type_course . ')',
                    'module' => $this->class,
                ]);
            } else
            {
                $this->session->set_flashdata('status_store_exam', 'error');
                $this->session->set_flashdata('msg_store_exam', 'Oops, something went wrong, Store exam failed');
            }
        }

        redirect('lecturer/exam');
    }

    public function delete()
    {
        $delete = $this->model->delete($this->input->post('id'));

        if ($delete)
        {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Delete exam success!'
            ];
        } else
        {
            $res    = [
                'status'    => 500,
                'pesan'     => 'Oops, something wrong. Delete exam failed!'
            ];
        }

        echo json_encode($res);
    }

    public function edit()
    {
        $edit = $this->model->show($this->input->post('id'));

        if ($edit)
        {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Get data success!',
                'data'      => $edit
            ];
        } else
        {
            $res    = [
                'status'    => 500,
                'pesan'     => 'Oops, something wrong. Get data failed!'
            ];
        }

        echo json_encode($res);
    }

    public function update()
    {
        $data   = [
            'id'                => $this->input->post('edit_id'),
            'id_course'         => $this->input->post('edit_id_course'),
            'waktu'             => $this->input->post('edit_waktu'),
            'passing_grade'     => $this->input->post('edit_passing_grade'),
            'aksi_waktu_habis'  => $this->input->post('edit_aksi_waktu_habis'),
            'jumlah_kesempatan' => $this->input->post('edit_jumlah_kesempatan')
        ];

        $update = $this->model->update($data, ['id' => $data['id']]);

        if ($update)
        {
            $this->session->set_flashdata('status_update_exam', 'success');
            $this->session->set_flashdata('msg_update_exam', 'Update exam success!');
        } else
        {
            $this->session->set_flashdata('status_update_exam', 'error');
            $this->session->set_flashdata('msg_update_exam', 'Oops, something went wrong, Update exam failed');
        }

        redirect('lecturer/exam');
    }

    public function get_tipe_course()
    {
        $list_tipe      = $this->course_model->show(['id' => $this->input->post('id')])->row()->type_course;
        $list_tipe      = str_replace("[", "", $list_tipe);
        $list_tipe      = str_replace("]", "", $list_tipe);
        $string_query   = "SELECT * FROM ls_m_type_course where id in ($list_tipe)";
        $get            = $this->course_model->get_type($string_query);

        if ($get)
        {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Get data success!',
                'data'      => $get
            ];
        } else
        {
            $res    = [
                'status'    => 500,
                'pesan'     => 'Oops, something wrong. Get data failed!'
            ];
        }

        echo json_encode($res);
    }

}
