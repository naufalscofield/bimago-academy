<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Lecturer_account extends MY_Controller {

    public $view    = 'lecturer/account/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWTLec();

        $this->load->model('lecturer/lecturer_account_model', 'model');
    }

    public function index()
    {
        $result['data']   = $this->master_model->data('a.*, b.role as nama_role', 'ls_m_user a', ['a.id' => $this->session->userdata('id')])
        ->join('ls_m_role b', 'a.role = b.id', 'LEFT')
        ->get()->row();
        $result['data_detail']   = $this->master_model->data('*','ls_m_user_detail',['id_user' => $this->session->userdata('id')])->get()->row();
    		$this->load_template_lecturer('lecturer/template', $this->view.'display', $result, '', '');
    }

    public function save()
    {
      $conf = array(
              array('field' => 'nama_depan', 'label' => 'First Name', 'rules' => 'trim|required'),
              array('field' => 'nama_belakang', 'label' => 'Last Name', 'rules' => 'trim|required'),
              array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required|valid_email|xss_clean|callback_unique'),
              array('field' => 'no_hp', 'label' => 'No Handphone / WA', 'rules' => 'trim|required|min_length[11]|max_length[13]|callback_check_no_hp'),
              array('field' => 'profil', 'label' => 'Profil', 'rules' => 'trim|required'),
          );

      $this->form_validation->set_rules($conf);
      $this->form_validation->set_message('required', '%s can not be empty.');

      if ($this->form_validation->run() === FALSE) {
        $respones['proses'] = 'failed';
        $response['pesan']  = strip_tags(validation_errors());
      }else{
        $data = array(
          'nama_depan' => $this->input->post('nama_depan') != '' ? $this->input->post('nama_depan') : null,
          'nama_belakang' => $this->input->post('nama_belakang') != '' ? $this->input->post('nama_belakang') : null,
          'institusi' => $this->input->post('institusi') != '' ? $this->input->post('institusi') : null,
          'email' => $this->input->post('email'),
          'no_hp' => $this->input->post('no_hp') != '' ? $this->input->post('no_hp') : null,
          'jk' => $this->input->post('jk') != '' ? $this->input->post('jk') : null,
          'profil' => $this->input->post('profil') != '' ? $this->input->post('profil') : null,
        );

        $cond = array(
          'id' => $this->session->userdata('id'),
        );

        $update = $this->model->update($data, $cond);
        if ($update) {
          $this->update_detail();
          $response['pesan'] = 'Update account profile success!';
          $response['proses'] = 'success';
        }else{
          $response['pesan'] = 'Oops, something went wrong, update account profile failed';
          $response['proses'] = 'failed';
        }
      }
      echo json_encode($response);
    }
    public function update_detail(){
 
     $data_detail['no_rek']   = $this->input->post('no_rek');
     $data_detail['nama_bank']   = $this->input->post('nama_bank');
     $data_detail['atas_nama_rek']   = $this->input->post('atas_nama_rek');
     if ($this->master_model->check_data(array('id_user'=> $this->session->userdata('id')),'ls_m_user_detail')) {
      $update = $this->master_model->update($data_detail,array('id_user'=> $this->session->userdata('id')),'ls_m_user_detail');
     }else{
      $data_detail['id_user']   = $this->session->userdata('id');
      $update = $this->master_model->save($data_detail,'ls_m_user_detail');
     }
  
    }

    public function change_password()
    {
      $old_password = md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($this->input->post('old_password')))))))))));
      $old = $this->master_model->check_data(['password' => $old_password, 'id' => $this->session->userdata('id')], 'ls_m_user');
      if ($old) {
        $conf = array(
          array('field' => 'new_password', 'label' => 'New Password', 'rules' => 'trim|required|min_length[6]|max_length[15]|callback_check_pass'),
          array('field' => 'old_password', 'label' => 'Old Password', 'rules' => 'trim|required|min_length[6]|max_length[15]')
        );

        $this->form_validation->set_rules($conf);
        $this->form_validation->set_message('required', '%s can not be empty.');

        if ($this->form_validation->run() === FALSE){
          $respones['proses'] = 'failed';
          $response['pesan']  = strip_tags(validation_errors());
        }else{
          $data   =   [
            'password' =>   md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($this->input->post('new_password')))))))))))
          ];

          $cond   =   [
            'id'        => $this->session->userdata('id'),
            'password'  => md5(sha1(md5(sha1(sha1(md5(md5(sha1(md5(md5($this->input->post('old_password'))))))))))),
          ];

          $change = $this->model->update($data, $cond);

          if ($change){
            $response['proses'] = 'success';
            $response['pesan']  = 'Change password success!';
          }else{
            $response['proses'] = 'failed';
            $response['pesan']  = 'Oops, something went wrong, change password failed';
          }
        }
      }else{
        $response['proses'] = 'wrongpass';
        $response['pesan']  = 'Old password not match, change password failed!';
      }
    echo json_encode($response);
  }

  public function change_picture()
  {

    $config['upload_path'] = 'assets/avatar/';
    $config['max_size']=2048;
		$config['allowed_types']="png|jpg|jpeg|gif";
		$config['remove_spaces']=TRUE;
		$config['encrypt_name']=TRUE;

    $a = $this->load->library('upload', $config);

    if (!$this->upload->do_upload('avatar')) {
        $error = array('error' => $this->upload->display_errors());
    } else {
        $data = array('image_metadata' => $this->upload->data());
        $cond = ['id' => $this->session->userdata('id')];
        $file = $this->master_model->data('avatar', 'ls_m_user', $cond)->get()->row()->avatar;
        $dataUpdate = ['avatar' => $data['image_metadata']['file_name']];
        $update = $this->master_model->update($dataUpdate, $cond, 'ls_m_user');
        if ($update) {
          if (file_exists('assets/avatar/'.$file)) {
            unlink('assets/avatar/'.$file);
          }
        }
    }

    redirect ('lecturer/myaccount');
  }

  public function check_pass(){
    if (1 !== preg_match("/^.*(?=.{6,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $this->input->post('new_password'))){
      $this->form_validation->set_message('check_pass', '%s must be at least 6 characters and must contain lower case letter, one upper case letter and one digit');
      return FALSE;
    }
    return TRUE;
  }

  public function check_no_hp(){
    if (!is_numeric($this->input->post('no_hp'))){
      $this->form_validation->set_message('check_no_hp', '%s phone number invalid');
      return FALSE;
    }else{
      return TRUE;
    }
  }

  public function unique(){
    $email = $this->input->post('email');
    $check = true;
    if ($this->master_model->check_data(['email' => $email, 'id !=' => $this->session->userdata('id')],'ls_m_user')) {
      $this->form_validation->set_message('unique', 'Email already used !');
      $check = false;
    }
    return $check;
	}

  public function logout() {
    $this->cart->destroy();
    $this->session->sess_destroy();
    redirect('login');
    // $response['pesan'] = 'Successfully Logout';
    // $response['url'] = base_url();
    // echo json_encode($response);
  }

}
