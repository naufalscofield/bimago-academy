<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Lecturer_dashboard extends MY_Controller {
    public $view    = 'lecturer/dashboard/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWTLec();
    }

    public function index()
    {
        $posdata = array();
        $bulan_data = array();
        $earning_data = array();
        $chart_course = array();
        $selled_course = array();

        $data['title']            = 'Lecturer Dashboard';
        $data['sum_course']       = $this->master_model->data('COUNT(id) as sum_course', 'ls_m_course', ['kontributor' => $this->session->userdata('id')])->get()->row();
        $own_course = $this->master_model->data('id,judul', 'ls_m_course', ['kontributor' => $this->session->userdata('id')])->get()->result_array();
        $all_course = $own_course;
        if (!empty($own_course)) {
          $own_course = $this->convert_array($own_course);
          $own_course = json_encode($own_course);
          $own_course      = str_replace("[", "", $own_course);
          $own_course      = str_replace("]", "", $own_course);

          $sum_user = 'SELECT COUNT(DISTINCT a.id_user) as sum_user FROM ls_t_detail_pembelian a LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran WHERE b.status = 1 and a.id_course IN ('.$own_course.') ';

          $sum_total = 'SELECT SUM(a.harga) as sum_total FROM ls_t_detail_pembelian a LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran WHERE b.status = 1 and a.id_course IN ('.$own_course.') ';

          $sum_month = 'SELECT SUM(a.harga) as sum_month_total FROM ls_t_detail_pembelian a LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran WHERE MONTH ( b.tanggal_pembelian ) = '.date('m').' AND YEAR ( b.tanggal_pembelian ) = '.date('Y').' AND  b.status = 1 and a.id_course IN ('.$own_course.') ';

          $data['sum_user']  = $this->master_model->db_query($sum_user, 'row');
          $data['sum_total'] = $this->master_model->db_query($sum_total, 'row');
          $data['sum_month_total'] = $this->master_model->db_query($sum_month, 'row');


          
          $posisi['bulan'] = date('m');
          $posisi['tahun'] = date('Y');
            for ($x = 1; $x <= 12; $x++) {
              $posdata['tahun'] = date('Y', mktime(0, 0, 0, ($posisi['bulan'] - 12) + $x, 1, $posisi['tahun']));
              $posdata['bulan'] = date('m', mktime(0, 0, 0, ($posisi['bulan'] - 12) + $x, 1, $posisi['tahun']));
              $bulan            = date('M-Y', mktime(0, 0, 0, ($posisi['bulan'] - 12) + $x, 1, $posisi['tahun']));
              $sum_month = 'SELECT SUM(a.harga) as sum_month_total FROM ls_t_detail_pembelian a LEFT JOIN ls_t_pembayaran b ON b.id = a.id_pembayaran WHERE MONTH ( b.tanggal_pembelian ) = '.$posdata['bulan'].' AND YEAR ( b.tanggal_pembelian ) = '.$posdata['tahun'].' AND  b.status = 1 and a.id_course IN ('.$own_course.') ';
                $data_earning = $this->master_model->db_query($sum_month, 'row');
                  $bulan_data[] = $bulan;
                  $earning_data[] = isset($data_earning->sum_month_total) ? $data_earning->sum_month_total : 0;
          }

          $data['bulan_data']     = $bulan_data;
          $data['earning_data']   = $earning_data;

        }else{
          $data['sum_user']  = [];
          $data['sum_total'] = [];
          $data['sum_month_total'] = [];
        }
        $all_course = $this->master_model->data('*', 'ls_m_course',['status' => 2,'kontributor' => $this->session->userdata('id')])->get();
        $total_selled= 0;
        foreach ($all_course->result() as $value) {
            $chart_course[]  = $value->judul;

            $count_selled   = $this->master_model->data('count(a.id_course) as count', 'ls_t_detail_pembelian a',['a.id_course' => $value->id])
            ->join('ls_t_pembayaran b', 'b.id = a.id_pembayaran AND b.status="1"', 'INNER')
            ->get()->row();
            $selled_course[] = isset($count_selled) ? $count_selled->count : 0;
            if ($count_selled->count != 0){
                $total_selled++;
            }
        }

        $total_course = isset($data['sum_course']->sum_course) ? $data['sum_course']->sum_course: 0;
        $unselled_course = $total_course - $total_selled;
        $data['total_unselled']   = $unselled_course ;
        $data['total_selled']     = $total_selled;
        $data['chart_course']   = $chart_course;
        $data['selled_course']   = $selled_course;
        $this->load_template_lecturer('lecturer/template', $this->view.'display', $data);
    }

    public function convert_array($array) {
    	if (!is_array($array)) {
    		return [];
    	}
    	$result = array();
    	foreach ($array as $key => $value) {
    		$result[$key] = $value['id'];
    	}
    	return $result;
    }

    public function logout() {
      $this->cart->destroy();
      $this->session->sess_destroy();
      $response['pesan'] = 'Successfully Logout';
      $response['url'] = base_url();
      echo json_encode($response);
    }

}
