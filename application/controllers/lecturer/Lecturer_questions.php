<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

require 'vendor/autoload.php';

// header('Content-Type: text/html; charset=utf-8');

Class Lecturer_questions extends MY_Controller {

    public $view    = 'lecturer/questions/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWTLec();
        
        $this->load->model('lecturer/lecturer_questions_model', 'model');
        $this->load->model('lecturer/lecturer_exam_model', 'model_exam');
        $this->load->model('lecturer/lecturer_episode_model', 'model_episode');
    }

    public function index($id_exam)
    {
        $data['title']          = 'Questions & Answers';
        $data['id_exam']        = $id_exam;
        $data['data_exam']      = $this->model_exam->show($id_exam);
        $data['course']         = $this->master_model->data('id, judul', 'ls_m_course', ['id' => $data['data_exam']->id_course])->get()->row();
        if ($data['data_exam']->tipe == 'quiz'){
            $this->model_episode->id = $data['data_exam']->id_episode;
            $data['episode']    = $this->model_episode->get_data('ls_m_episode')->row();
        }
        $data['url_data']       = base_url().'lecturer/data-questions/'.$id_exam;
        $data['url_delete']     = base_url().'lecturer/delete-question';
        $data['url_answers']    = base_url().'lecturer/data-answers';
        $data['total_question'] = $this->model->show(['id_exam' => $id_exam]);
        // $sub_menu           = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => $this->class])->get()->row();
        // $menu               = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $this->load_template_lecturer('lecturer/template', $this->view.'display', $data, '', '');
    }

    public function data($id_exam)
    {
        $search  = $this->input->post('search');
        $limit  = $this->input->post('length');
        $offset = $this->input->post('start');

        if (!empty($search['value']))
        {
            $list   = $this->model->show(['id_exam' => $id_exam, "pertanyaan LIKE'%".$search['value']."%'" => NULL], $limit, $offset);
            $data_list   = $list->result_array();
            $total     = $list->num_rows();
        } else
        {
            $list   = $this->model->show(['id_exam' => $id_exam], $limit, $offset);
            $data_list   = $list->result_array();
            $total     = $list->num_rows();
        }

        $filtered = $this->model->show(['id_exam' => $id_exam])->num_rows();

        $data = array();
        $no = $offset;
        
        foreach ($data_list as $field) {
            $null_answer    = $this->model->check_null_answer($field['id'])->num_rows();
            $total_answer   = $this->model->show_answers(["c.id" => $field['id']])->num_rows();
            $check_kunjaw   = $this->model->check_kunjaw($field['id'], false);

            $no++;
            $row = array();
            $row[] = $no;
            $pertanyaan = $field['pertanyaan'];

            if ($null_answer != 0)
            {
                $pertanyaan .= "<div class='blink_me'>
                <span class='badge badge-warning'>Ada pilihan jawaban yang masih kosong</span>
                </div>";
            } 
            if ($total_answer == 0)
            {
                $pertanyaan .= "<div class='blink_me'>
                <span class='badge badge-danger'>Soal belum memiliki jawaban</span>
                </div>";
            }  
            if ($check_kunjaw == NULL)
            {
                $pertanyaan .= "<div class='blink_me'>
                <span class='badge badge-info'>Soal belum memiliki kunci jawaban</span>
                </div>";
            }  
            
            $row[] = $pertanyaan;
            $row[] = $field['score'];
            $row[] = $field['id'];
            $row[] = $field['tipe'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $total,
            "recordsFiltered" => $filtered,
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function data_answers()
    {
        $id_question    = $this->input->post("id");
        $answers        = $this->model->show_answers(['a.id_question' => $id_question])->result_array();

        $kunjaw         = new stdClass();
        if (count($answers) != 0)
        {
            if ($answers[0]['tipe'] == 'checkbox')
            {
                $kunjaw     = $this->model->check_kunjaw($id_question, false);
            }
        }

        // if ($answers)
        // {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Get answers success!',
                'data'      => $answers,
                'kunjaw'    => $kunjaw
            ];
        // } else
        // {
            // $res    = [
            //     'status'    => 500,
            //     'pesan'     => 'Oops, something wrong. Get answers failed!'
            // ];
        // }

        echo json_encode($res);
    }

    public function store_question()
    {
        //Store media
        $media  = NULL;
        if (!empty($_FILES['media']['name']))
        {
            $media = str_replace(" ", "_", $_FILES['media']['name']);
            $config['upload_path']          = './uploads/media_question';
            $config['allowed_types']        = 'jpg|jpeg|png|mp4|mov|mp3';
            $config['max_size']             = 10240;
    
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('media'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata("error",ss($error['error']));
                redirect($_SERVER['HTTP_REFERER']);
            }
        }

        $data   = [
            'pertanyaan'=> $this->input->post('question'),
            'score'     => $this->input->post('score'),
            'id_exam'   => $this->input->post('id_exam'),
            'tipe'      => $this->input->post('type'),
            'number_option'   => ($this->input->post('number_option') == 0) ? 0 : $this->input->post('number_option'),
            'media'     => $media
        ];

        $store  = $this->model->store($data);

        if ($this->input->post('type') != 'boolean')
        {
            $dataAnswer = [];
            $opt    = 'a';
            for($i=0; $i<$data['number_option']; $i++){
                $push   = [
                    'id_question'   => $store,
                    'opt'           => $opt,
                    'answer'        => '-'
                ];
                array_push($dataAnswer, $push);
                $opt++;
            }
        } else
        {
            $dataAnswer = [
                [
                    'id_question'   => $store,
                    'opt'           => 'a',
                    'answer'        => 'True'
                ],
                [
                    'id_question'   => $store,
                    'opt'           => 'b',
                    'answer'        => 'False'
                ]
            ];
        }
        $storeAnswer    = $this->model->insert_batch($dataAnswer);

        if ($store && $storeAnswer)
        {
            $this->session->set_flashdata('status_store_question', 'success');
            $this->session->set_flashdata('msg_store_question', 'Store question success!');
        } else
        {
            $this->session->set_flashdata('status_store_question', 'error');
            $this->session->set_flashdata('msg_store_question', 'Oops something wrong happen, Store question failed!');
        }

        if ($this->input->post('tipe_exam') == 'exam')
        {
            redirect ('lecturer/exam-questions/'.$data['id_exam']);
        } else
        {
            redirect ('lecturer/quiz-questions/'.$data['id_exam']);
        }
    }

    public function store_answers()
    {
        $totalData  = count($this->input->post('ans_id_answer'));

        $data   = [];
        for ($i=0; $i<$totalData; $i++)
        {
            $data[] =
            [
                    'id' => $this->input->post('ans_id_answer')[$i],
                    'answer' => $this->input->post('ans_answer')[$i],
            ];
        }

       $updateBatch = $this->model->update_answers_batch($data);

       $checkKunjaw = $this->model->check_kunjaw($this->input->post('ans_id_question'));

        $data_id_answer  = ($this->input->post('ans_tipe_question') == 'checkbox') ? implode(',', $this->input->post('opt')) : $this->input->post('opt');
        if ($checkKunjaw > 0)
        {
            $dataKunjaw  = [
                'id_answer' => $data_id_answer
            ];

            $condKunjaw  = [
                'id_question'    => $this->input->post('ans_id_question')
            ];

            $updateKunjaw    = $this->model->update_kunjaw($dataKunjaw, $condKunjaw);
        } else
        {
            $dataKunjaw  = [
                'id_question'   => $this->input->post('ans_id_question'),
                'id_answer'     => $data_id_answer
            ];

            $storeKunjaw    = $this->model->store_kunjaw($dataKunjaw);
        }

        if ($updateKunjaw || $storeKunjaw)
        {
            $this->session->set_flashdata('status_update_answer', 'success');
            $this->session->set_flashdata('msg_update_answer', 'Answers saved!');
        } else
        {
            $this->session->set_flashdata('status_update_answer', 'error');
            $this->session->set_flashdata('msg_update_answer', 'Oops something wrong happen, update answer failed!');
        }

        redirect('lecturer/exam-questions/'.$this->input->post('ans_id_exam'));
    }

    public function store_question_used()
    {
        $idExam = $this->input->post('id_exam');
        $data   = [
            'jumlah_soal_dipakai' => $this->input->post('jumlah_soal_dipakai')
        ];

        $cond   = [
            'id' => $idExam
        ];

        $update = $this->model_exam->store_question_used($data, $cond);

        redirect ('lecturer/exam-questions/'.$idExam);
    }

    public function delete_question()
    {
        $delete = $this->model->delete(['id' => $this->input->post('id')]);
        $delete2 = $this->model->delete_answer(['id_question' => $this->input->post('id')]);
        $delete3 = $this->model->delete_key(['id_question' => $this->input->post('id')]);

        if ($delete)
        {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Delete question success!'
            ];
        } else
        {
            $res    = [
                'status'    => 500,
                'pesan'     => 'Oops, something wrong. Delete question failed!'
            ];
        }

        echo json_encode($res);
    }

    public function import_questions()
    {
        $filename   ='excel_questions_'.rand(00001,99999).'_'.$this->session->userdata('id');
        $config['upload_path'] = './assets/excel_questions';
        $config['max_size']=2048;
        $config['allowed_types']="xlsx|xls";
        $config['remove_spaces']=TRUE;
        $config['file_name']= $filename;


        $a = $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file_excel')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

            $spreadsheet    = $reader->load("./assets/excel_questions/".$filename.$this->upload->data('file_ext'));
            $datas          = $spreadsheet->getSheet(0)->toArray();
            unset($datas[0]);
            foreach ($datas as $data)
            {
                if (empty($data[0]) || empty($data[1]) )
                {
                    continue;
                }

                $dataStoreQuestion = [
                    'id_exam'       => $this->input->post('id_exam'),
                    'pertanyaan'    => $data[1],
                    // 'score'         => $data[8],
                    'score'         => $data[4],
                    'tipe'          => ($data[2] == 'true-false') ? 'boolean' : $data[2]
                ];
                $storeQuestion = $this->model->store($dataStoreQuestion);

                $dataAnswers    = [];

                $alpha          = 'a';

                // for ($i=5; $i<=6; $i++)
                foreach ($data as $i => $d)
                {
                    if ($i < 5) 
                    {
                        continue;
                    }
                    else
                    {
                        if ($data[$i] !== NULL)
                        {
                            if ($data[2] == 'option' || $data[2] == 'checkbox')
                            {
                                $addAnswer      = [
                                    'id_question'   => $storeQuestion,
                                    'opt'           => $alpha,
                                    'answer'        => $data[$i]
                                ];
                            } else if ($data[2] == 'true-false') {
                                $addAnswer      = [
                                    'id_question'   => $storeQuestion,
                                    'opt'           => $alpha,
                                    'answer'        => ($data[$i] === true) ? 'TRUE' : 'FALSE'
                                ];
                            }
    
                            array_push($dataAnswers, $addAnswer);
                            $alpha++;
                        }
                    }

                }

                $storeAnswer    = $this->model->store_answer($dataAnswers, true);

                if ($data[2] == 'option')
                {
                    $getIdAnswer    = $this->model->show_answers(['a.id_question' => $storeQuestion, 'opt' => strtolower($data[3])])->row();
                    $dataKunjaw     = [
                        'id_question'   => $storeQuestion,
                        'id_answer'     => $getIdAnswer->id_answer
                    ];

                } else if ($data[2] == 'true-false')
                {
                    $jaw = (string)$data[3];
                    $jaw = ($jaw == 1) ? 'TRUE' : 'FALSE';

                    $getIdAnswer    = $this->model->show_answers(['a.id_question' => $storeQuestion, 'answer' => $jaw])->row();
                    $dataKunjaw     = [
                        'id_question'   => $storeQuestion,
                        'id_answer'     => $getIdAnswer->id_answer
                    ];

                } else if ($data[2] == 'checkbox')
                {
                    $getIdAnswer    = $this->model->show_answers_cbx($storeQuestion, strtolower($data[3]));
                    $dataKunjaw     = [
                        'id_question'   => $storeQuestion,
                        'id_answer'     => $getIdAnswer
                    ];
                }


                $storeKunjaw    = $this->model->store_kunjaw($dataKunjaw);

            }


        }
            unlink(APPPATH.'../assets/excel_questions/'.$filename.'.xlsx');
            redirect ('lecturer/exam-questions/'.$this->input->post('id_exam'));
    }

    public function delete_option($id,$id_question)
    {
        $delete = $this->model->delete_answer(['id' => $id]);
        $get_answer = $this->model->show_answers(['a.id_question' => $id_question])->result_array();
        
        //Update option jawaban
        $opt    = "a";
        foreach ($get_answer as $ga)
        {
            $cond   = [
                'id' => $ga['id_answer']
            ];

            $data = [
                'opt'   => $opt
            ];

            $update = $this->model->update_master_answer($cond, $data);
            $opt++;
        }

        //Delete kunjaw
        $delete_kunjaw  = $this->model->delete_key(['id_question' => $id_question]);

        //Delete answer
        if ($delete)
        {
            $res    = [
                'status'    => 200,
                'pesan'     => 'Delete answer success!'
            ];
        } else
        {
            $res    = [
                'status'    => 500,
                'pesan'     => 'Oops, something wrong. Delete answer failed!'
            ];
        }

        echo json_encode($res);
    }

    public function store_option()
    {
        $id_question    = $this->input->post("id_question");
        $answer         = $this->input->post("answer");

        $get_answer = $this->model->show_answers(['a.id_question' => $id_question])->num_rows();
        $alphabet = range('a', 'z');

        $data = [
            'id_question'   => $id_question,
            'opt'           => $alphabet[$get_answer],
            'answer'        => $answer
        ];

        $store  = $this->model->store_master_answer($data);

        redirect ('lecturer/exam-questions/'.$this->input->post('id_exam'));
    }

    public function edit_question($id)
    {
        $get    = $this->model->show(['id' => $id])->row();

        echo json_encode($get);
    }

    public function media_question($id)
    {
        $get    = $this->model->show(['id' => $id])->row();
        if ($get->media != NULL)
        {
            $ext    = substr($get->media, -3);
            $file   = $get->media;
            $source = base_url()."uploads/media_question/".$get->media;

            if (in_array($ext, ['mp4', 'mov']))
            {
                $type = "video";
            } else if (in_array($ext, ['mp3']))
            {
                $type = "sound";
            } else
            {
                $type = "image";
            }
        } else
        {
            $source = "null";
            $file = "null";
            $type = "null";
        }

        $data = [
            'source'  => $source,
            'file'  => $file,
            'type'  => $type
        ];

        if ($type == "video")
        {
            $data['ext']    = $ext;
        }

        echo json_encode($data);
    }

    public function store_media()
    {
        $id_question    = $this->input->post("inp_id_question");

        $media = str_replace(" ", "_", $_FILES['inp_media']['name']);
        $config['upload_path']          = './uploads/media_question';
        $config['allowed_types']        = 'jpg|jpeg|png|mp4|mov|mp3';
        $config['max_size']             = 10240;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('inp_media'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata("error",strip_tags($error['error']));
            redirect($_SERVER['HTTP_REFERER']);
        }

        $update         = $this->model->update_question(['id' => $id_question], ['media' => $media]);

        if ($update)
        {
            $this->session->set_flashdata("success","Media updated");
        } else
        {
            $this->session->set_flashdata("error","Something wrong, media fail update");
        }
        
        redirect ('lecturer/exam-questions/'.$this->input->post('inp_id_exam'));
    }

    public function delete_media($id, $id_exam)
    {
        $get    = $this->model->show(['id' => $id])->row();
        unlink(APPPATH.'../uploads/media_question/'.$get->media);

        $update         = $this->model->update_question(['id' => $id], ['media' => NULL]);
        
        if ($update)
        {
            $this->data['message'] = 'success';
        } else
        {
            $this->output->set_status_header('500');
            $this->data['message'] = validation_errors();
        }
        echo json_encode($this->data);
    }

    public function delete_media_answer($id, $id_exam)
    {
        $get    = $this->model->show_answers(['a.id' => $id])->row();
        // print_r($get); die;
        unlink(APPPATH.'../uploads/media_answer/'.$get->media_answer);

        $update         = $this->model->update_master_answer(['id' => $id], ['media' => NULL]);

        if ($update)
        {
            $this->data['message'] = 'success';
            $this->data['id_question'] = $get->id_question;
            $this->data['tipe'] = $get->tipe;
        } else
        {
            $this->output->set_status_header('500');
            $this->data['message'] = validation_errors();
        }
        echo json_encode($this->data);
    }

    public function update_question()
    {
        $data = [
            'pertanyaan'    => $this->input->post("edit_question"),
            'score'    => $this->input->post("edit_score"),
        ];

        //Store media
        // dd($_FILES['edit_media']['name']);
        // if (isset($_FILES['edit_media']))
        if (!empty($_FILES['edit_media']['name']))
        {
            $media = str_replace(" ", "_", $_FILES['edit_media']['name']);
            $config['upload_path']          = './uploads/media_question';
            $config['allowed_types']        = 'jpg|jpeg|png|mp4|mov|mp3';
            $config['max_size']             = 10240;
    
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('edit_media'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata("error",ss($error['error']));
                redirect($_SERVER['HTTP_REFERER']);
            }

            $data['media']  = $media;
        }

        $cond = [
            'id'    => $this->input->post("edit_id_question")
        ];

        $update = $this->model->update_question($cond, $data);
        if ($update)
        {
            $this->session->set_flashdata("success","Question updated");
        } else
        {
            $this->session->set_flashdata("error","Something wrong, question fail update");
        }
        
        redirect ('lecturer/exam-questions/'.$this->input->post('edit_id_exam'));
    }

    public function upload_media_answer()
    {
        
        $config['upload_path']          = './uploads/media_answer';
        $config['allowed_types']        = 'jpg|jpeg|png|mp4|mov|mp3';
        $config['max_size']             = 10240;

        $this->load->library('upload', $config);

        $upload_data = $this->upload->data();

        if (!$this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->output->set_status_header('400');
            echo json_encode(strip_tags($error['error']));
        } else
        {
            $data = [
                'media'       => $this->upload->data('file_name')
            ];

            $cond = [
                'id'          => $this->input->post("id")
            ];
            
            $update = $this->model->update_master_answer($cond, $data);

            if ($update)
            {
                $this->data['message'] = "Success";
                echo json_encode($this->data);

            } else
            {
                $this->output->set_status_header('500');
                $this->data['message'] = "Error updating media";
                
                echo json_encode($this->data);
            }
        }
    }
}
