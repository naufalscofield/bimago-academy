<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

Class Lecturer_certificate extends MY_Controller {
    public $view    = 'lecturer/certificate/';

    public function __construct()
    {
        parent::__construct();
        checkAuthJWTLec();
        
        $this->load->model('lecturer/lecturer_certificate_model', 'model');
    }

    public function index($id='')
    {
        $data['title']      = get_value('judul','ls_m_course',['id' => $id ]);
        $data['url_data']   = base_url().'lecturer/lecturer_certificate/data'.(!empty($id) ? '/'.$id :'');
        $data['get_data_edit'] = base_url('lecturer/'.$this->class.'/get_modal'.(!empty($id) ? '/'.$id :''));
        $data['get_data_delete'] = base_url('lecturer/'.$this->class.'/delete');
        $data['get_data_preview'] = base_url('lecturer/'.$this->class.'/preview_certificate');

        $sub_menu           = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
        // $menu               = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        // $this->load_template_lecturer('lecturer/template', $this->view.'display', $data, $menu->nama_menu, $sub_menu->nama_menu);
        $this->load_template_lecturer('lecturer/template', $this->view.'display', $data);
    }

    public function data($id='',$offset='', $limit='')
    {
        $search  = $this->input->post('search');
        $limit  = $this->input->post('length');
        $offset = $this->input->post('start');
        $data   = $this->model->show($id,$limit,$offset,$search['value'])->result_array();
        $list = [];
        foreach ($data as $key => $value) {
            $value['no'] = $key+1;
            $list[] = $value;
        }
        $count  = $this->model->show()->num_rows();
        $data = array();
        $no = 0;

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $count,
            "recordsFiltered" => $this->model->show($id,$limit,$offset,$search['value'])->num_rows(),
            "data" => $list,
        );

        echo json_encode($output);
    }
    public function get_modal($id='')
    {
        if (!empty($this->input->post('id'))) {
            $id = $this->input->post('id');
        }
        $response['opt_modul'] = options2('ls_m_course', ['status' => 2,'id' => $id], 'id', 'judul', '', '- Pilih -', '', array('id' => 'ASC'));
        if ($id != '') {
          $get = $this->model->show($id);
          $response['pesan'] = 'data tidak ada';
          $response['status'] = 404;
          $response['data'] = array();
          $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
          $response['disable'] = 'disabled';
          if ($get -> num_rows() > 0) {
            $response['pesan'] = 'data ada';
            $response['status'] = 200;
            $response['data'] = $get->row_array();
            $response['disable'] = '';
          }
        }else{
          $response['data'] = array();
          $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
          $response['disable'] = '';
        }
        return $this->load->view($this->view.'data_modal', $response);
    }
    public function save()
    {
    $conf = array(
            array('field' => 'id_course', 'label' => 'Course', 'rules' => 'trim|required'),
    );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');
    $this->form_validation->set_message('numeric', '%s harus berisi nomor.');
    $this->form_validation->set_message('matches', '%s tidak cocok.');
    $this->form_validation->set_message('min_length', '%s minimal %s digit.');
    $this->form_validation->set_message('valid_email', '%s harus valid.');
    $this->form_validation->set_message('is_unique', '%s tidak boleh sama.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      $ttd1 = uploadGambar('sign_pic1','assets/certificate/signature/');
      $ttd_name1  = $ttd1['pesan'] ? $ttd1['nama_file'] : '';
      $ttd2 = uploadGambar('sign_pic2','assets/certificate/signature/');
      $ttd_name2  = $ttd2['pesan'] ? $ttd2['nama_file'] : '';

      $type = $this->input->post('type');

      $data = array(
        'id_course' => $this->input->post('id_course'),
        'certificate_number'   => $this->input->post('certificate_number'),
        'signature_name'       => $this->input->post('signature_name'),
        'signature_title'      => $this->input->post('signature_title'),
        'presented_name1'      => $this->input->post('presented_name1'),
        'presented_title1'     => $this->input->post('presented_title1'),
        'presented_name2'      => $this->input->post('presented_name2'),
        'presented_title2'     => $this->input->post('presented_title2'),
        'certificate_category' => $this->input->post('certificate_category'),
        'signature_name2'      => $this->input->post('signature_name2'),
        'signature_title2'     => $this->input->post('signature_title2'),
        'duration'             => $this->input->post('duration'),
        'institution1'         => $this->input->post('institution1'),
        'institution2'         => $this->input->post('institution2'),
        'type'                 => $this->input->post('type'),
        'sign_pic1'            => ($ttd_name1 != '' ? $ttd_name1 : $this->input->post('sign_pic1_old')),
        'sign_pic2'            => ($ttd_name2 != '' ? $ttd_name2 : $this->input->post('sign_pic2_old')),
      );

      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $update = $this->master_model->update($data, $where,'ls_m_certificate');
        $response['pesan'] = 'Failed To Save Certificate Configurations';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Success Update Certificate Configurations';
          $response['status'] = 200;
        }
      }else{
        $create = $this->master_model->save($data, 'ls_m_certificate');
        $response['pesan'] = 'Failed To Save';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Success To Save';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
    }

    public function delete()
    {
      $where['id'] = $this->input->post('id');
      $status = $this->master_model->delete($where, 'ls_m_certificate');
      $response['pesan'] = 'Failed to Delete';
      $response['status'] = 404;
      if ($status) {
        $response['pesan'] = 'Success to Delete';
        $response['status'] = 200;
      }
      echo json_encode($response);
    }

    public function preview_certificate(){
        $id_course = $this->input->post('id');
        $msg = ['status'=> FALSE, 'msg'=>'Failed to Generate Certificate', 'url'=> ''];
        if (!empty($id_course)) {
            $status = generatePreviewCertificate($id_course);
            $msg = ['status'=> $status, 'msg'=>'Success Generate Certificate', 'url'=> base_url('assets/certificate/certificatepreview.png')];
        }

        echo json_encode($msg);
    }
    public function generate_certificate(){
        $id_course = $this->input->post('id');
        $msg = ['status'=> FALSE, 'msg'=>'failed to generate certificate', 'url'=> ''];
        ## AMBIL SEMUA DATA USER YANG BELI COURSE DAN SUDAH BAYAR
        $data = $this->master_model->data('a.*', 'ls_t_detail_pembelian a',['a.id_course' => $id_course])
        ->join('ls_t_pembayaran b', 'b.id = a.id_pembayaran AND b.status="1"', 'INNER')
        ->get();
        if ($data->num_rows() > 0) {
            ## MULAI INPUT TRAS CERTIFICATE
            foreach ($data->result() as $value) {
                ## CEK JIKA JENIS WEBINAR
                if ($value->id_type_course == 4) {
                    ## CEK DULU APAKAH USER SUDAH ADA TRANS CERTIFICATE
                    if ($this->master_model->check_data(['id_user'=> $value->id_user,'id_course'=> $value->id_course,'id_exam'=> 0],'ls_t_certificate') == false) {
                        $this->master_model->saveTransCertificate($value->id_course,$value->id_user);
                    }
                }else{
                        $after_exam = $this->master_model->data('b.id', 'ls_t_after_exam a', ['a.id_user' => $value->id_user ,'a.status' => 'passed'])
                        ->join('ls_m_exam b', 'b.id = a.id_exam AND b.tipe="exam" AND b.id_course="'.$value->id_course.'" AND b.id_type_course="'.$value->id_type_course.'"', 'INNER')
                        ->get()->row();
                        if(isset($after_exam->id)){
                            if ($this->master_model->check_data(['id_user'=> $value->id_user,'id_exam'=> $after_exam->id,'id_course'=> $value->id_course],'ls_t_certificate') == false) {
                                ## CEK DULU APAKAH USER SUDAH ADA TRANS CERTIFICATE
                                $this->master_model->saveTransCertificate($value->id_course,$value->id_user,$after_exam->id);
                            }
                        }
                }
            }
            $msg = ['status'=> TRUE, 'msg'=>'All user certificate has been generated', 'url'=> ''];
        }else{
            $msg = ['status'=> FALSE, 'msg'=>'No one user have this course', 'url'=> ''];
        }

        echo json_encode($msg);
    }
}
