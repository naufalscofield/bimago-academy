<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Lecturer_pembelian extends MY_Controller {
  public $view    = 'lecturer/pembelian/';
	public function __construct(){
		parent::__construct();
    checkAuthJWTLec();
    
		$this->load->model('lecturer/lecturer_pembelian_model','model');
	}

	public function index(){
    $result['data'] = $this->get();
    $result['judul'] = 'Pembelian';
    $result['get_data_edit'] = base_url('lecturer/'.$this->class.'/get_modal');
    $result['get_data_delete'] = base_url('lecturer/'.$this->class.'/delete');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

	public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_t_detail_pembelian');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}
  public function get_modal()
  {
    $id = $this->input->post('id');
    $response['opt_modul'] = options2('ls_m_course', '', 'id', 'judul', '', '- Pilih -', '', array('id' => 'ASC'));
    if ($id != '') {
      $this->model->id = $id;
      $get = $this->model->get_data('ls_t_pembayaran');
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
      $response['disable'] = 'disabled';
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['disable'] = '';
      }
    }else{
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
  }
  public function save()
  {
    $conf = array(
            array('field' => 'id_course', 'label' => 'Course', 'rules' => 'trim|required'),
            array('field' => 'judul', 'label' => 'Judul', 'rules' => 'trim|required'),
            array('field' => 'url', 'label' => 'Url', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');
    $this->form_validation->set_message('numeric', '%s harus berisi nomor.');
    $this->form_validation->set_message('matches', '%s tidak cocok.');
    $this->form_validation->set_message('min_length', '%s minimal %s digit.');
    $this->form_validation->set_message('valid_email', '%s harus valid.');
    $this->form_validation->set_message('is_unique', '%s tidak boleh sama.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      // $gambar = $this->input->post('thumbnail');
      $data = array(
        'id_course' => $this->input->post('id_course'),
        'judul'     => $this->input->post('judul'),
        'url'       => $this->input->post('url'),
        'type'      => $this->input->post('type'),
        // 'length' => $this->input->post('length'),
        // 'gambar' => $gambar,
      );
      // if ($_FILES['gambar']['name'] == '') {

      // }else{
      //   if (file_exists($this->input->post('thumbnail'))) {
      //     unlink($this->input->post('thumbnail'));
      //   }
      // }
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $update = $this->model->update_data('ls_t_pembayaran', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Episode';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Episode';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_t_pembayaran', $data);
        $id_user = $this->db->insert_id();
        $response['pesan'] = 'Data Episode Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Episode Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
  }

}
