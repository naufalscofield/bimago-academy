<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Lecturer_episode extends MY_Controller {
  public $view    = 'lecturer/episode/';
	public function __construct(){
		parent::__construct();
    checkAuthJWTLec();

		$this->load->model('lecturer/lecturer_episode_model','model');
		$this->load->model('lecturer/lecturer_course_model','course_model');
		$this->load->model('admin/task_model','task_model');
	}

	public function index(){
    $result['data'] = $this->get();
    $result['judul'] = 'Episode List';
    $result['get_data_edit'] = base_url('lecturer/'.$this->class.'/get_modal');
    $result['get_data_delete'] = base_url('lecturer/'.$this->class.'/delete');
    $result['get_video'] = base_url('lecturer/'.$this->class.'/video');
    $result['materi'] = base_url('lecturer/'.$this->class.'/materi/');
    $result['tugas'] = base_url('lecturer/'.$this->class.'/tugas/');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

  public function get($id = '')
	{
		$this->model->id = $id == '' ? $this->input->post('id') : $id;
		$get = $this->model->get_data('ls_m_episode');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal()
  {
    $id = $this->input->post('id');
    $response['opt_modul'] = options2('ls_m_course', ['status' => 2, 'kontributor' => $this->session->userdata('id')], 'id', 'judul', '', '- Pilih -', '', array('id' => 'ASC'));
    $response['url_get_tipe_course'] = base_url('lecturer/'.$this->class.'/get_tipe_course');
    if ($id != '') {
      $this->model->id = $id;
      $get = $this->model->get_data('ls_m_episode');
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
      $response['disable'] = 'disabled';
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['disable'] = '';
      }
    }else{
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal', $response);
  }

  public function video()
  {
    $id = $this->input->post('id');
    if ($id != '') {
      $get = $this->master_model->data('*', 'ls_m_episode', ['id' => $id])->get();
      $response['data'] = array();
      if ($get -> num_rows() > 0) {
        $response['data'] = $get->row_array();
      }
    }else{
      $response['data'] = array();
    }
    return $this->load->view($this->view.'video', $response);
  }

  public function save()
  {
    $conf = array(
            array('field' => 'id_course', 'label' => 'Course', 'rules' => 'trim|required'),
            array('field' => 'judul', 'label' => 'Judul', 'rules' => 'trim|required'),
            array('field' => 'url', 'label' => 'Url', 'rules' => 'trim|required'),
            array('field' => 'id_type_course', 'label' => 'Type course', 'rules' => 'trim|required'),
            array('field' => 'deskripsi', 'label' => 'Deskripsi', 'rules' => 'trim|required'),
            array('field' => 'order', 'label' => 'Urutan video', 'rules' => 'trim|required|callback_unique'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');
    $this->form_validation->set_message('numeric', '%s harus berisi nomor.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $data = array(
          'id_course' => $this->input->post('id_course'),
          'judul'     => $this->input->post('judul'),
          'url'       => $this->input->post('url'),
          'type'      => 'gdrive',
          'id_type_course'      => $this->input->post('id_type_course'),
          'deskripsi' => $this->input->post('deskripsi'),
          'order' => $this->input->post('order'),
        );
        $update = $this->model->update_data('ls_m_episode', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Episode';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Episode';
          $response['status'] = 200;
        }
      }else{
        $data = array(
          'id_course' => $this->input->post('id_course'),
          'judul'     => $this->input->post('judul'),
          'url'       => $this->input->post('url'),
          'type'      => $this->input->post('type'),
          'id_type_course'      => $this->input->post('id_type_course'),
          'order'      => $this->input->post('order'),
          'status' => 1,
          'deskripsi' => $this->input->post('deskripsi'),
          'length' => $this->input->post('length'),
        );
        $create = $this->model->create_data('ls_m_episode', $data);
        $response['pesan'] = 'Data Episode Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $data_course = $this->master_model->data('*', 'ls_m_course', ['id' => $this->input->post('id_course')])->get()->row();
          sendNotification([
            'url' => base_url('admin/episode'),
            'to_role_id' => 1, // Role Admin
            'message' => 'New Episode ' . $this->input->post('judul') . ' from Course ' . $data_course->judul,
            'module' => $this->class,
          ]);

          $response['pesan'] = 'Data Episode Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
    
    echo json_encode($response);
  }

  public function unique(){
		$id 			 = $this->input->post('id');
		$id_course 			 = $this->input->post('id_course');
		$id_type_course  = $this->input->post('id_type_course');
    $order 		       = $this->input->post('order');
		if (!empty($id)) {
			if ($this->master_model->check_data(['id !='=> $id, 'id_type_course' => $id_type_course, 'id_course' => $id_course, 'order' => $order],'ls_m_episode')) {
				$this->form_validation->set_message('unique', 'Urutan sudah ada !');
				return false;
			}
		}else{
			if ($this->master_model->check_data(['id_type_course' => $id_type_course, 'id_course' => $id_course, 'order' => $order],'ls_m_episode')) {
				$this->form_validation->set_message('unique', 'Urutan sudah ada !');
				return false;
			}
		}

		return true;
	}

  public function get_tipe_course()
  {
    $id = $this->input->post('id');
    $id_type = $this->input->post('id_type_course');
    if (!empty($id)) {
      $list_tipe      = $this->course_model->show(['id' => $id])->row()->type_course;
      $list_tipe      = str_replace("[", "", $list_tipe);
      $list_tipe      = str_replace("]", "", $list_tipe);
      $string_query   = "SELECT * FROM ls_m_type_course where id in ($list_tipe)";
      $get            = $this->course_model->get_type($string_query);
    }else{
      $get = [];
    }
    $lists = "<option value=''>-- Pilih --</option>";

    foreach($get as $data){
      $selected = '';
      if (!empty($id_type)) {
        if ($data['id'] == $id_type) {
          $selected = 'selected';
        }
      }
      $lists .= "<option value='".$data['id']."' ".$selected.">".$data['type_course']."</option>";
    }

    $callback = [
      'type' => $lists,
      'status' => 200,
      'pesan' => 'Get data success!',
    ];
    echo json_encode($callback);
  }

  public function delete()
  {
    $where['id'] = $this->input->post('id');
    $episode = $this->master_model->delete($where, 'ls_m_episode');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($episode) {
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

  // ============================================================= MATERI

  public function materi($id){
    $get = $this->master_model->data('a.judul as judul_episode, b.judul as judul_course, c.type_course', 'ls_m_episode a', ['a.id' => $id])
    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
    ->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT')
    ->get()->row_array();
    $result['data'] = $this->get_materi($id);
    $result['id_episode'] = $id;
    $result['judul'] = '(Materi) '.$get['judul_course'].' -> '.$get['type_course'].' -> '.$get['judul_episode'];
    $result['part'] = 'Materi / Resource';
    $result['get_data_edit'] = base_url('lecturer/'.$this->class.'/get_modal_materi');
    $result['get_data_delete'] = base_url('lecturer/'.$this->class.'/delete_materi');
    $result['get_pdf_preview'] = base_url('lecturer/'.$this->class.'/preview_materi');
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display_materi', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}



  public function get_materi($id = '')
	{
		$get = $this->master_model->data('a.*', 'ls_m_sumber_episode a', ['a.id_episode' => $id])->get();
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
		if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
	}

  public function get_modal_materi()
  {
    $id = $this->input->post('id');
    $id_episode = $this->input->post('id_episode');
    if ($id != '') {
      $get = $this->master_model->data('a.*', 'ls_m_sumber_episode a', ['a.id' => $id])->get();
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save_materi');
      $response['disable'] = 'disabled';
      $response['id_episode'] = $id_episode;
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['disable'] = '';
      }
    }else{
      $response['id_episode'] = $id_episode;
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save_materi');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal_materi', $response);
  }

  public function save_materi()
  {
    $conf = array(
            array('field' => 'nama_file', 'label' => 'Judul materi', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      if ($_FILES['materi']['name'] == '') {
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
        );
      }else{
        $pdf = uploadPdf('materi', 'uploads/course_episode_resource/');
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
          'file'     => $pdf['nama_file'],
        );
        if (file_exists($this->input->post('materi_old'))) {
          unlink($this->input->post('materi_old'));
        }
      }
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $file = $this->master_model->data('file', 'ls_m_sumber_episode', ['id' => $this->input->post('id')])->get()->row();
        $update = $this->model->update_data('ls_m_sumber_episode', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Materi';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Materi';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_sumber_episode', $data);
        $response['pesan'] = 'Data Materi Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Materi Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
  }

  public function preview_materi(){
      $id = $this->input->post('id');
      $data = $this->master_model->data('file', 'ls_m_sumber_episode', ['id' => $id])->get()->row();
      $msg = ['status'=> FALSE, 'msg'=>'Gagal membuka pdf', 'url'=> ''];
      if (!empty($data)) {
          $msg = ['status'=> TRUE, 'msg'=>'Berhasil membuka pdf', 'url'=> base_url().$data->file];
      }

      echo json_encode($msg);
  }

  public function delete_materi()
  {
    $file = $this->master_model->data('file', 'ls_m_sumber_episode', ['id' => $this->input->post('id')])->get()->row();
    $where['id'] = $this->input->post('id');
    $materi = $this->master_model->delete($where, 'ls_m_sumber_episode');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($materi) {
      if (file_exists($file->file)) {
        unlink($file->file);
      }
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

  // ============================================================= TUGAS

  public function tugas($id){
    $get = $this->master_model->data('a.judul as judul_episode, b.judul as judul_course, c.type_course', 'ls_m_episode a', ['a.id' => $id])
    ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
    ->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT')
    ->get()->row_array();
    $result['data'] = $this->get_tugas($id);
    $result['id_episode'] = $id;
    $result['judul'] = '(Tugas) '.$get['judul_course'].' -> '.$get['type_course'].' -> '.$get['judul_episode'];
    $result['get_data_edit'] = base_url('lecturer/'.$this->class.'/get_modal_tugas');
    $result['get_data_delete'] = base_url('lecturer/'.$this->class.'/delete_tugas');
    $result['get_pdf_preview'] = base_url('lecturer/'.$this->class.'/preview_tugas');
    $result['part'] = 'Tugas / Tasks';
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display_tugas', $result, $menu->nama_menu, $sub_menu->nama_menu);
	}

  public function get_tugas($id = '')
  {
    $get = $this->master_model->data('a.*', 'ls_m_tugas_episode a', ['a.id_episode' => $id])->get();
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
    if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
  }

  public function get_modal_tugas()
  {
    $id = $this->input->post('id');
    $id_episode = $this->input->post('id_episode');
    if ($id != '') {
      $get = $this->master_model->data('a.*', 'ls_m_tugas_episode a', ['a.id' => $id])->get();
      $response['pesan'] = 'data tidak ada';
      $response['status'] = 404;
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save_tugas');
      $response['disable'] = 'disabled';
      $response['id_episode'] = $id_episode;
      if ($get -> num_rows() > 0) {
        $response['pesan'] = 'data ada';
        $response['status'] = 200;
        $response['data'] = $get->row_array();
        $response['disable'] = '';
      }
    }else{
      $response['id_episode'] = $id_episode;
      $response['data'] = array();
      $response['simpan'] = base_url('lecturer/'.$this->class.'/save_tugas');
      $response['disable'] = '';
    }
    return $this->load->view($this->view.'data_modal_tugas', $response);
  }

  public function save_tugas()
  {
    $conf = array(
            array('field' => 'nama_file', 'label' => 'Judul tugas', 'rules' => 'trim|required'),
        );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      if ($_FILES['tugas']['name'] == '') {
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
        );
      }else{
        $pdf = uploadPdf('tugas', 'uploads/course_episode_tugas/');
        $data = array(
          'id_episode' => $this->input->post('id_episode'),
          'nama_file' => $this->input->post('nama_file'),
          'file'     => $pdf['nama_file'],
        );
        if (file_exists($this->input->post('tugas_old'))) {
          unlink($this->input->post('tugas_old'));
        }
      }
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $file = $this->master_model->data('file', 'ls_m_tugas_episode', ['id' => $this->input->post('id')])->get()->row();
        $update = $this->model->update_data('ls_m_tugas_episode', $data, $where);
        $response['pesan'] = 'Gagal Melakukan Update Materi';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Berhasil Melakukan Update Materi';
          $response['status'] = 200;
        }
      }else{
        $create = $this->model->create_data('ls_m_tugas_episode', $data);
        $response['pesan'] = 'Data Materi Tidak Berhasil Ditambahkan';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Data Materi Berhasil Ditambahkan';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
  }

  public function preview_tugas(){
      $id = $this->input->post('id');
      $data = $this->master_model->data('file', 'ls_m_tugas_episode', ['id' => $id])->get()->row();
      $msg = ['status'=> FALSE, 'msg'=>'Gagal membuka pdf', 'url'=> ''];
      if (!empty($data)) {
          $msg = ['status'=> TRUE, 'msg'=>'Berhasil membuka pdf', 'url'=> base_url().$data->file];
      }

      echo json_encode($msg);
  }

  public function delete_tugas()
  {
    $file = $this->master_model->data('file', 'ls_m_tugas_episode', ['id' => $this->input->post('id')])->get()->row();
    $where['id'] = $this->input->post('id');
    $materi = $this->master_model->delete($where, 'ls_m_tugas_episode');
    $response['pesan'] = 'Data Gagal Dihapus';
    $response['status'] = 404;
    if ($materi) {
      if (file_exists($file->file)) {
        unlink($file->file);
      }
      $response['pesan'] = 'Data Berhasil Dihapus';
      $response['status'] = 200;
    }
    echo json_encode($response);
  }

  public function monitoring_tugas($id_tugas)
  {
    $get_episode  = $this->model->show(['id' => $id_tugas])->row();
    // dd($get_episode);
    $data['url']  = base_url()."lecturer/data-monitoring-tugas/".$id_tugas;

    $this->load_template_lecturer('lecturer/template', $this->view.'monitoring_tugas', $data);
  }

  public function data_monitoring_tugas($id_tugas)
  {
    $get_tasks    = $this->task_model->show(['a.id_tugas' => $id_tugas])->result_array();

    $data = array();
    $no = $_POST['start'];
    foreach ($get_tasks as $e) {
      $no++;
      $row = array();
      $row[] = $no;
      $row[] = $e['nama_depan'].' '.$e['nama_belakang'];
      $row[] = '<a target="_blank" href="'.base_url().'uploads/user_task/'.$e['file'].'">Link</a>';
      $row[] = ($e['nilai'] != NULL) ? $e['nilai'] : 'Belum ada nilai';
      $row[] = $e['created_at'];

      $disabled = ($e['nilai']!=NULL) ? 'disabled' : '';

      $row[] = '<button type="button" '.$disabled.' class="btn btn-primary btn-nilai" data-id="'.$e['id_task'].'" data-toggle="tooltip" data-placement="top" title="Beri Nilai">
                <i class="fas fa-pen"></i></button>';
      
      $data[] = $row;
    }

    $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => count($data),
        "recordsFiltered" => count($data),
        "data" => $data,
    );
    echo json_encode($output);
  }

  public function input_nilai_tugas()
  {
    $cond = 
    [
      'id'  => $this->input->post("id")
    ];

    $data = [
      'nilai' => $this->input->post("nilai"),
      'catatan' => $this->input->post("catatan"),
    ];

    $store  = $this->task_model->update($cond, $data);

    if ($store)
    {
      $this->session->set_flashdata("success", "Nilai berhasil diinput");
    } else
    {
      $this->session->set_flashdata("error", "Terjadi masalah, gagal input nilai");
    }

    redirect($_SERVER['HTTP_REFERER']);
  }

}
