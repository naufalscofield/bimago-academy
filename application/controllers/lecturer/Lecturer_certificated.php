<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Lecturer_certificated extends MY_Controller {
  public $view    = 'lecturer/certificated/';
    public function __construct(){
      parent::__construct();
      checkAuthJWTLec();

      $this->load->model('lecturer/Lecturer_certificated_model','cert');
    }

    public function index(){
    $result['data'] = $this->get();
    // dd($result['data']);
    $result['judul']  = 'Monitoring Webinar';
    $result['member'] = base_url('lecturer/'.$this->class.'/detail/');
    $result['get_data_generate'] = base_url('lecturer/lecturer_certificated/generate_certificate');

    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $this->load_template_lecturer('lecturer/template', $this->view.'display', $result, $menu->nama_menu, $sub_menu->nama_menu);
    }

  public function detail($id){
    $result['data'] = $this->get_detail($id);
    $data = $this->master_model->data('judul', 'ls_m_course', ['id' => $id])->get()->row();
    // dd($result['data']);
    $result['judul'] = 'Monitoring Webinar ->'.$data->judul;
    $result['email'] = base_url('lecturer/'.$this->class.'/email');
    $result['id_course'] = $id;
    $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['menu_lecturer' => $this->class])->get()->row();
    $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $this->load_template_lecturer('lecturer/template', $this->view.'display_member', $result, $menu->nama_menu, $sub_menu->nama_menu);
    }

    public function get($id = '')
    {
        $this->cert->id = $id == '' ? $this->input->post('id') : $id;
        $get = $this->cert->get_data('ls_m_course');
    $response['pesan'] = 'Data Tidak Ditemukan';
    $response['status'] = 404;
    $response['data'] = array();
        if ($get -> num_rows() > 0) {
        $response['pesan'] = 'Data Ditemukan';
        $response['status'] = 200;
        $response['data'] = $get->result_array();
    }
    return $response;
    }

  public function get_detail($id)
  {
    $this->cert->id = $id;
    $get = $this->cert->get_data_member('ls_t_certificate');
    $response['pesan'] = 'data tidak ada';
    $response['status'] = 404;
    $response['data'] = array();
    if ($get -> num_rows() > 0) {
      $response['pesan'] = 'data ada';
      $response['status'] = 200;
      $response['data'] = $get->result_array();
    }
    return $response;
  }
  public function certificate(){
      $id = $this->input->post('id');
      $id_user = $this->input->post('id_user');
      $msg = ['status'=> FALSE, 'msg'=>'Gagal Generate Certificate', 'url'=> ''];

      if (!empty($id)) {
          $data = $this->master_model->data('a.id_course','ls_t_certificate a',['a.id'=>$id])
                  ->join('ls_m_certificate b', 'a.id_course = b.id_course','INNER')->get();
          if ($data->num_rows() > 0) {
            $filename = generateCertificate($id, $id_user);
            // $this->load->helper('download');
            // force_download(BASEPATH."../assets/certificate/temp/".$filename.".png", NULL);
            $msg = ['status'=> TRUE, 'msg'=>'Succes Generate Certificate',
             'url'=> base_url('assets/certificate/temp/'.$filename.'.png')
           ];
          }else{
            $msg = ['msg'=>'Succes Generate Certificate', 'url'=> base_url('pagenotfound')];
          }
      }

      echo json_encode($msg);
  }

  public function uploadcertificate(){
      $id_certificate = $this->input->post('id');
      $id_user        = $this->input->post('id_user');

      $response['id_certificate'] = $id_certificate;
      $response['id_user'] = $id_user;

      if ($id_certificate != '') {
        $get = $this->master_model->data('a.*','ls_t_certificate_upload a',['id_certificate'=> $id_certificate, 'id_user' => $id_user])->get();
        $response['pesan'] = 'data tidak ada';
        $response['status'] = 404;
        $response['data'] = array();
        $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
        $response['disable'] = 'disabled';
        if ($get -> num_rows() > 0) {
          $response['pesan'] = 'data ada';
          $response['status'] = 200;
          $response['data'] = $get->row_array();
          $response['disable'] = '';
        }
      }else{
        $response['data'] = array();
        $response['simpan'] = base_url('lecturer/'.$this->class.'/save');
        $response['disable'] = '';
      }
      return $this->load->view($this->view.'data_modal', $response);
  }

  public function save(){
    $conf = array(
        array('field' => 'id_certificate', 'label' => 'Certificate', 'rules' => 'trim|required'),
        array('field' => 'id_user', 'label' => 'User', 'rules' => 'trim|required'),
    );

    $this->form_validation->set_rules($conf);
    $this->form_validation->set_message('required', '%s tidak boleh kosong.');

    if ($this->form_validation->run() === FALSE) {
      $respones['status'] = 404;
      $response['pesan']  = validation_errors();
    }else {
      $url       = uploadGambar('url','assets/certificate/temp_upload/');
      $url_name  = $url['pesan'] ? $url['nama_file'] : '';
      $id_user        = $this->input->post('id_user');
      $id_certificate = $this->input->post('id_certificate');
      $url_old        = $this->input->post('url_old');

      $data = array(
        'id_user'          => $this->input->post('id_user'),
        'id_certificate'   => $this->input->post('id_certificate'),
        'url'              => ($url_name != '' ? $url_name : $url_old),
      );
      $where['id'] = $this->input->post('id');
      if ($where['id'] != '') {
        $update = $this->master_model->update($data, $where,'ls_t_certificate_upload');
        $response['pesan'] = 'Failed To Upload Certificate';
        $response['status'] = 404;
        if ($update) {
          $response['pesan'] = 'Success Upload Certificate';
          $response['status'] = 200;
        }
      }else{
        $create = $this->master_model->save($data, 'ls_t_certificate_upload');
        $response['pesan'] = 'Failed To Upload';
        $response['status'] = 404;
        if ($create != 0) {
          $response['pesan'] = 'Success To Upload';
          $response['status'] = 200;
        }
      }
    }
      echo json_encode($response);
  }

  public function delete(){
      $response['pesan'] = 'Failed To Delete';
      $response['status'] = 404;
      $this->load->helper("file");
      $id   = $this->input->post('id');
      $cert_upload = $this->master_model->data('a.*', 'ls_t_certificate_upload a',['a.id'=> $id])->get();
      if ($cert_upload->num_rows() > 0) {
        $loc = $cert_upload->row('url');
        $delete  = $this->master_model->delete(["id" => $id], "ls_t_certificate_upload");
        if ($delete) {
          if (is_readable(BASEPATH."../".$loc)) {
             unlink(BASEPATH."../".$loc);
          }

          $response['pesan'] = 'Success To Delete';
          $response['status'] = 200;
        }
      }
      echo json_encode($response);
  }

  public function email(){
    $response['pesan'] = 'Failed Sent Email';
    $response['status'] = 404;

    if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $learn = $this->master_model->data('a.*,b.judul,c.email', 'ls_t_certificate a')
        ->join('ls_m_course b', 'a.id_course = b.id','LEFT')
        ->join('ls_m_user c', 'a.id_user = c.id','LEFT')
        ->where_in('a.id', $id)
        ->get()->result();

        foreach ($learn as $value) {
          $data = [
            'base'  => base_url(),
            'judul' => $value->judul,
            'cert_url' => base_url('certificate_exam'),
          ];

          $message = $this->load->view('lecturer/certificate_monitoring/template_email',$data,true);
          $subject = 'Congratulations, Your Certificate is Available';
          $this->load->library('email', $this->config->item('email_config'));
          $this->email->from('info@solmit.academy',  env('APP_NAME'));
          $this->email->to($value->email);
          $this->email->subject($subject);
          $this->email->message($message);
          if ($this->email->send()) {
            $response['pesan'] = 'Email Has Been Sent';
            $response['status'] = 200;
          } else {
            dd($this->email->print_debugger());
          }
        }
    }
    echo json_encode($response);
  }
   public function generate_certificate(){
        $id_course = $this->input->post('id');
        $msg = ['status'=> FALSE, 'msg'=>'failed to generate certificate', 'url'=> ''];
        ## AMBIL SEMUA DATA USER YANG BELI COURSE DAN SUDAH BAYAR
        $data = $this->master_model->data('a.*', 'ls_t_detail_pembelian a',['a.id_course' => $id_course])
        ->join('ls_t_pembayaran b', 'b.id = a.id_pembayaran AND b.status="1"', 'INNER')
        ->get();
        if ($data->num_rows() > 0) {
            ## MULAI INPUT TRAS CERTIFICATE
            foreach ($data->result() as $value) {
                ## CEK JIKA JENIS WEBINAR
                if ($value->id_type_course == 4) {
                    ## CEK DULU APAKAH USER SUDAH ADA TRANS CERTIFICATE
                    if ($this->master_model->check_data(['id_user'=> $value->id_user,'id_course'=> $value->id_course,'id_exam'=> 0],'ls_t_certificate') == false) {
                        $this->master_model->saveTransCertificate($value->id_course,$value->id_user);
                    }
                }else{
                        $after_exam = $this->master_model->data('b.id', 'ls_t_after_exam a', ['a.id_user' => $value->id_user ,'a.status' => 'passed'])
                        ->join('ls_m_exam b', 'b.id = a.id_exam AND b.tipe="exam" AND b.id_course="'.$value->id_course.'" AND b.id_type_course="'.$value->id_type_course.'"', 'INNER')
                        ->get()->row();
                        if(isset($after_exam->id)){
                            if ($this->master_model->check_data(['id_user'=> $value->id_user,'id_exam'=> $after_exam->id,'id_course'=> $value->id_course],'ls_t_certificate') == false) {
                                ## CEK DULU APAKAH USER SUDAH ADA TRANS CERTIFICATE
                                $this->master_model->saveTransCertificate($value->id_course,$value->id_user,$after_exam->id);
                            }
                        }
                }
            }
            $msg = ['status'=> TRUE, 'msg'=>'All user certificate has been generated', 'url'=> ''];
        }else{
            $msg = ['status'=> FALSE, 'msg'=>'No one user have this course', 'url'=> ''];
        }

        echo json_encode($msg);
    }
}
