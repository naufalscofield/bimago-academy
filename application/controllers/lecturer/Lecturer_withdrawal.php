<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;

defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . "../vendor/autoload.php";

class Lecturer_withdrawal extends MY_Controller {
    public $view    = 'lecturer/withdrawal/';

    public function __construct(){
        parent::__construct();
        checkAuthJWTLec();

        $this->load->database();
        $this->load->model('lecturer/lecturer_withdrawal_model','model');
        $this->load->model('lecturer/lecturer_saldo_model','model_saldo');
        $this->load->model('lecturer/lecturer_account_model','model_account');
        $this->load->model('admin/cut_off_model','model_cut_off');
    }

    public function convert_array($array) 
    {
    	if (!is_array($array)) {
    		return [];
    	}
    	$result = array();
    	foreach ($array as $key => $value) {
    		$result[$key] = $value['id'];
    	}
    	return $result;
    }

    public function index()
    {
        $get_detail_user      = $this->model_account->show_detail(['id_user' => $this->session->userdata('id')])->row();
        if ($get_detail_user != NULL)
        {
            if ($get_detail_user->nama_bank == 'mandiri')
            {
                $admin_bank = 0;
            } else
            {
                $admin_bank = $this->model_cut_off->show(['kode' => 'admin_bank'])->row()->potongan;
            }
        } else
        {
            $admin_bank = $this->model_cut_off->show(['kode' => 'admin_bank'])->row()->potongan;
        }

        $data['withdrawal']   = $this->model->show(['id_lecturer' => $this->session->userdata('id')])->result_array();
        $data['saldo']        = $this->model_saldo->show(['id_lecturer' => $this->session->userdata('id')])->row();
        if ($data['saldo'] != NULL)
        {
            $data['withdrawalable'] = (($data['saldo']->saldo - $admin_bank) < 500000) ? 0 : ($data['saldo']->saldo - $admin_bank);
        } else
        {
            $data['withdrawalable'] = 0;
        }
        $data['breadcrumbs']  =
        [
            [
                'title' => 'Transaksi',
                'link'  => NULL
            ],
            [
                'title' => 'Withdrawal',
                'link'  => '',
                'akhir' => true
            ]
        ];
        $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => 'withdrawal'])->get()->row();
        $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
		$this->load_template_lecturer('lecturer/template', $this->view.'display', $data, $menu->nama_menu, $sub_menu->nama_menu);
    }

    public function request()
    {
        $get_detail_user      = $this->model_account->show_detail(['id_user' => $this->session->userdata('id')])->row();
        if ($get_detail_user != NULL)
        {
            if ($get_detail_user->nama_bank == 'mandiri')
            {
                $admin_bank = 0;
            } else
            {
                $admin_bank = $this->model_cut_off->show(['kode' => 'admin_bank'])->row()->potongan;
            }
        } else
        {
            $admin_bank = $this->model_cut_off->show(['kode' => 'admin_bank'])->row()->potongan;
        }

        $check      = $this->model->show(['id_lecturer' => $this->session->userdata('id'), 'status' => 1])->num_rows();
        $saldo       = $this->model_saldo->show(['id_lecturer' => $this->session->userdata('id')])->row();
        $withdrawalable = (($saldo->saldo - $admin_bank) < 500000) ? 0 : ($saldo->saldo - $admin_bank);

        if ($check == 0)
        {
            if ($withdrawalable <= 0)
            {
                $this->output->set_status_header('400');
                echo json_encode($this->lang->line('zero_balance'));
            } else
            {
                $data = [
                    'id_lecturer'   => $this->session->userdata("id"),
                    'total'         => $withdrawalable,
                    'status'        => 1
                ];
    
                $insert = $this->model->store($data);
    
                if ($insert)
                {
                    $this->output->set_status_header('200');
                    echo json_encode($this->lang->line('withdrawal_request_success'));
                } else{
                    $this->output->set_status_header('500');
                    echo json_encode($this->lang->line('internal_server_error'));
                }
            }
        } else
        {
            $this->output->set_status_header('400');
            echo json_encode($this->lang->line('pending_withdrawal'));
        }
    }
    
}