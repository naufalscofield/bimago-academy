<?php
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Signer;
use MiladRahimi\Jwt\Cryptography\Algorithms\Rsa\RS256Verifier;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPrivateKey;
use MiladRahimi\Jwt\Cryptography\Keys\RsaPublicKey;
use MiladRahimi\Jwt\Generator;
use MiladRahimi\Jwt\Parser;
use GuzzleHttp\Psr7\Request;

defined('BASEPATH') OR exit('No direct script access allowed');

// include APPPATH . "../vendor/autoload.php";
require 'vendor/autoload.php';

class Lecturer_CutOff extends MY_Controller {
    public $view    = 'lecturer/cutoff/';

    public function __construct(){
        parent::__construct();
        checkAuthJWTLec(); 

        $this->load->database();
        $this->load->model('admin/cut_off_model','model');
    }

    public function index()
    {
        $data['cutoff'] = $this->model->get()->result_array();
        $data['menu'] = 'Cut Off';
        $data['sub_menu'] = 'cutoff';
        // $sub_menu = $this->master_model->data('id_parent, nama_menu', 'm_menu', ['url' => 'cutoff'])->get()->row();
        // $menu = $this->master_model->data('nama_menu', 'm_menu', ['id' => $sub_menu->id_parent])->get()->row();
        $data['breadcrumbs']  =
        [
            [
                'text' => $this->lang->line('cut_off'),
                'link'  => '',
                'akhir' => true
            ]
        ];

        $this->load_template_lecturer('lecturer/template', $this->view.'display', $data);
    }

    
}