<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {
 
  public function __construct()
  {
    parent::__construct();
    $params = array('server_key' => 'Mid-server-Zv30B2eTAF20Ilp8IPIVg9k0', 'production' => true);
    $this->load->library('veritrans');
    $this->veritrans->config($params);
    $this->load->helper('url');

  }

  public function cron_payment(){
      $data = $this->master_model->data('*','ls_t_pembayaran',['status'=> 0])->get();
      if ($data->num_rows() > 0) {
          foreach ($data->result() as $value) {
            $status_midtrans = $this->status($value->no_pesanan);
            if ($status_midtrans) {
              if ($status_midtrans) {
                switch ($status_midtrans->transaction_status) {
                  case 'expire':
                      $update = $this->master_model->update(['status' => 2], ['no_pesanan' => $value->no_pesanan], 'ls_t_pembayaran');
                      sendNotification([
                        'url' => base_url('admin/Pembayaran'),
                        'to_role_id' => 1, // Role Admin
                        'message' => 'Pembayaran untuk no pesanan ' .  $value->no_pesanan . ' Expire',
                        'module' => $this->class,
                      ]);
                      break;
                  case 'settlement':
                      $update = $this->master_model->update(['status' => 1], ['no_pesanan' => $value->no_pesanan], 'ls_t_pembayaran');
                      sendNotification([
                        'url' => base_url('admin/Pembayaran'),
                        'to_role_id' => 1, // Role Admin
                        'user_id' => 0, 
                        'message' => 'Pembayaran untuk no pesanan ' .  $value->no_pesanan . ' Diterima',
                        'module' => $this->class,
                      ]);
                      $data_pembayaran = $this->master_model->data('a.id as id_pembayaran, a.*, b.*','ls_t_pembayaran a', ['a.no_pesanan'=> $value->no_pesanan])
                      ->join('ls_m_user b', 'a.id_user=b.id', 'LEFT')->get()->row_array();
                      $data_pembayaran['detail_pembelian'] = $this->master_model->data('a.harga, b.judul, c.type_course' ,'ls_t_detail_pembelian a',['a.id_pembayaran' => $data_pembayaran['id_pembayaran']])
                      ->join('ls_m_course b', 'a.id_course = b.id', 'LEFT')
                      ->join('ls_m_type_course c', 'a.id_type_course = c.id', 'LEFT')
                      ->get()->result_array();

                      $this->email('payment', $data_pembayaran, $data_pembayaran['email']);

                  		$data_pembayaran 	= $this->model_pembayaran->show(['a.no_pesanan' => $$value->no_pesanan])->result_array();

                      foreach ($data_pembayaran as $idx => $dp)
                      {
                        if ($dp['status'] == 0)
                        {
                          //Tambahan insert saldo
                          $get_potongan_platform  = $this->cut_off_model->show(['kode' => 'bee_lms'])->row();
                          if ($get_potongan_platform->jenis == 'persentase')
                          {
                            $potongan_platform  = ($get_potongan_platform->potongan / 100) * $dp['harga'];
                          } else
                          {
                            $potongan_platform  = $get_potongan_platform->potongan;
                          }
                  
                          $get_potongan_payment_gateway  = $this->cut_off_model->show(['kode' => $dp['metode_pembayaran']])->row();
                          if ($get_potongan_payment_gateway->jenis == 'persentase')
                          {
                            $potongan_payment_gateway  = ($get_potongan_payment_gateway->potongan / 100) * $dp['harga'];
                          } else
                          {
                            $potongan_payment_gateway  = $get_potongan_payment_gateway->potongan;
                          }
                  
                          $total_potongan = (int)$potongan_platform + (int)$potongan_payment_gateway;
                  
                          $id_lecturer = $this->course_model->show(['id' => $dp['id_course']])->row()->kontributor;
                          $check_saldo = $this->saldo_model->show(['id_lecturer' => $id_lecturer])->row();
                  
                          if ($check_saldo == NULL)
                          {
                            $data_saldo = [
                            'id_lecturer' => $id_lecturer,
                            'saldo'       => (int)$dp['harga'] - (int)$total_potongan
                            ];
                  
                            $insert_saldo = $this->saldo_model->store($data_saldo);
                          } else
                          {
                            $curr_saldo = $check_saldo->saldo;
                  
                            $data_saldo = [
                            'id_lecturer' => $id_lecturer,
                            'saldo'       => (int)$curr_saldo + ((int)$dp['harga'] - (int)$total_potongan),
                            ];
                  
                            $update_saldo = $this->master_model->update($data_saldo, ['id_lecturer' => $id_lecturer], 'ls_t_saldo_lecturer');
                          }
                          //Akhir insert saldo
                        }
                      }
                      break;
                }
              }
            }
          }
          echo 'Berhasil melakukan update pembayaran';
      }
  } 
  public function status($order_id)
  {
    return ($this->veritrans->status($order_id));
  }
  public function email($type, $data, $email) {
        $message = $this->template_email($type, $data);
        if ($type == 'payment') {
          $subject = 'Payment Already Confirm - '.env('APP_NAME').'';
        }elseif ($type == 'training') {
          $subject = 'Schedule List Training Online - '.env('APP_NAME').'';
        }elseif ($type == 'led') {
          $subject = 'Instructur Led Class - '.env('APP_NAME').'';
        }elseif ($type == 'webinar') {
          $subject = 'Webinar - '.env('APP_NAME').'';
        }
        $this->load->library('email', $this->config->item('email_config'));
        $this->email->from('info@solmit.academy', env('APP_NAME'));
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send())
        {
          return true;
        } else
        {
          return false;
        }
    }

    public function template_email($type, $dataset){
        $data = [
          'name'  => $dataset['nama_depan'],
          'data'  => $dataset,
          'base'  => base_url(),
          'type'  => $type
        ];
      return $this->load->view('admin/pembayaran/template_email',$data,true);
    }

}
