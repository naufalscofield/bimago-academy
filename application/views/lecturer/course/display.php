<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        Master <i class="fas fa-angle-double-right"></i>
        <a href="#">
          <?=$this->lang->line('courses');?>
        </a>
      </h6>
    </div>
    <div class="card-body">
      <div align="right">
        <a href="#" data-style="zoom-out" id="" class="btn ladda-button btn-success btn-icon-split getModal ladda-update_" style="align:right;">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text"><?= $this->lang->line('add');?></span>
        </a>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="15%">Modul</th>
              <th width="20%"><?= $this->lang->line('title');?></th>
              <th width="20%"><?= $this->lang->line('image');?></th>
              <th width="10%">Status</th>
              <th width="5%">Episode</th>
              <th width="5%"><?= $this->lang->line('exam');?></th>
              <th width="5%"><?= $this->lang->line('certificate');?></th>
              <th width="20%"><i class="fas fa-cogs"></i></th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
              <?php if ($data['status'] == 200) { ?>
                  <tr id="<?php echo $value['id'] ?>">
                    <td><?= $no;?>.</td>
                    <td><?= $value['modul'];?></td>
                    <td><?= $value['judul'];?></td>
                    <td><img src="<?=base_url().$value['image'];?>" style="width:240px;height:165px;" alt=""></td>
                    <?php if ($value['status'] == 2) { ?>
                      <td style="background-color:#cbf5cf;">
                        <span>Ya</span>
                      </td>
                    <?php }else{ ?>
                      <td style="background-color:#f5a2a7;">
                        <span>Tidak</span>
                      </td>
                    <?php } ?>
                    <td>
                      <a href="<?=base_url("lecturer/lecturer_episodev2/index/".$value['id']);?>" data-style="zoom-out" class="btn ladda-button btn-success text-light">
                        <i style="color:white" class="fas fa-video"></i>
                      </a>
                    </td>
                    <td>
                      <a href="<?=base_url("lecturer/lecturer_exam/index/".$value['id']);?>" data-style="zoom-out" class="btn ladda-button btn-success text-light">
                        <i style="color:white" class="fas fa-book"></i>
                      </a>
                    </td>
                    <td>
                      <a href="<?=base_url("lecturer/lecturer_certificate/index/".$value['id']);?>" data-style="zoom-out" class="btn ladda-button btn-success text-light">
                        <i style="color:white" class="fas fa-certificate"></i>
                      </a>
                    </td>
                    <td>
                      <a id="<?=$value['id'];?>" data-style="zoom-out" title="Detail/Edit" class="btn ladda-button btn-info getModal ladda-update_<?=$value['id'];?>">
                        <i style="color:white" class="fas fa-info-circle"></i>
                      </a>
                      <a id="<?=$value['id'];?>" data-style="zoom-out" title="Delete" class="btn ladda-button btn-danger deleteData ladda-delete_<?=$value['id'];?>">
                        <i style="color:white" class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
              <?php }else{ ?>
                <td>Tidak Ada Data !</td>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){

    $(".getModal").click(function(event){
      var id = $(this).attr('id');
      var l = Ladda.create( document.querySelector( '.ladda-update_' + id));
      l.start();
        $.ajax({
            url: "<?=$get_data_edit;?>",
            method: "POST",
            data: {id:id},
            success: function(data){
                l.stop();
                $('#dataModalLarge').html(data);
                $('#ModalLarge').modal('show');
            },
            error: function (data) {
              l.stop();
              toastr.error("Data tidak dapat dibuka, coba kembali.");
            },
        });
      });

    $(".deleteData").click(function(event){
      var id = $(this).attr('id');
      var l = Ladda.create( document.querySelector( '.ladda-delete_' + id));
  		l.start();
      bootbox.confirm("Apakah anda yakin akan menghapus data ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_data_delete;?>",
              method: "POST",
              data: {id:id},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                   l.stop();
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   l.stop();
                   toastr.error(hasil["pesan"]);
                 }
              },
              error: function (res) {
                l.stop();
                toastr.error("Data tidak dapat dihapus.");
              },
          });
        }else{
  				l.stop();
  			}
      });
    });

  });
</script>
