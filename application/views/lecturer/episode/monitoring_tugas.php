<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">
        Master <i class="fas fa-angle-double-right"></i>
        <a href="<?= base_url('lecturer/lecturer_course'); ?>"><?= $this->lang->line('courses').' : '.$judul; ?> <i class="fas fa-angle-double-right"></i></a> 
        <a href="<?= base_url('lecturer/lecturer_episodev2/index/'.$id_episode); ?>">Episode : <?=$judul_episode;?> <i class="fas fa-angle-double-right"></i></a> 
        <a href="<?= base_url('lecturer/lecturer_episodev2/tugas/'.$id_episode); ?>">
          <?= $this->lang->line('task'); ?> : <?=$judul_task;?> <i class="fas fa-angle-double-right"></i>
        </a>
        <a href="#">
          <?= $this->lang->line('score'); ?> / Monitoring
        </a>
      </h6>
    </div>
    <div class="card-body">
    </div>
    <br> 
    <br> 
    <div class="table-responsive">
      <div class="container" style="display:block" id="tableDiv">
        <table class="table table-striped table-bordered" id="table" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th><?= $this->lang->line('name_'); ?></th>
                    <th>File</th>
                    <th><?= $this->lang->line('score'); ?></th>
                    <th><?= $this->lang->line('date_assign'); ?></th>
                    <th><i class="fa fa-cogs"></i></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="modal_nilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Input Nilai Tugas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>lecturer/input-nilai-tugas" method="POST">
          <input type="hidden" name="id" value="" id="inp_id">
          <div class="form-group">
            <label for="">Nilai</label>
            <input required type="number" name="nilai" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Catatan</label>
            <textarea name="catatan" class="form-control" id="" cols="30" rows="10"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
      var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
      var flashdata_error	= '<?php echo $this->session->flashdata("error");?>'
      if (flashdata_success)
      {
        toastr.success(flashdata_success);
      } else if (flashdata_error)
      {
        toastr.error(flashdata_error);
      }
      var url = '<?php echo $url;?>'

        $('#table').DataTable().destroy()
        table = $('#table').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "order": [], 
          
        "ajax": {
            "url": url,
            "type": "POST"
        },

          
        "columnDefs": [
          { 
              "targets": [ 0 ], 
              "orderable": false, 
          },
        ],

        });
  })

  $(document).on("click", ".btn-nilai", function(){
    var id_task = $(this).data("id")
    $("#inp_id").val(id_task)
    $("#modal_nilai").modal("show")
  })
</script>
