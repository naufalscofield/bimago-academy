<link rel="stylesheet" href="<?= base_url();?>assets/videojs/css/video-js.css">
<script src="<?= base_url();?>assets/videojs/js/videojs-ie8.min.js"></script>
<script src="<?= base_url();?>assets/videojs/js/video.js"></script>
  <div class="modal-header">
    <h5 class="modal-title" id="ModalLabelLarge"><b>Video - <?=$data['judul'];?></b></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="trailer col-lg-12 col-md-12 col-12">
      <?php if (!empty($data['url'])): ?>
        <?php if ($data['type'] == 'gdrive'): ?>
          <div class="wrapper">
          <div style="max-width: 950px;">
            <video id='my-video' class='video-js vjs-default-skin vjs-big-play-centered' width='740' height='460'
            poster="" data-setup='{"controls" : true, "preload" : "auto" ,"fluid": true}'>
            <source src='<?= getGDriveEmbedUrl($data['url']);?>' type='video/mp4'>
              <p class='vjs-no-js'>
                To view this video please enable JavaScript, and consider upgrading to a web browser that
                <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
              </p>
            </video>
          </div>
          </div>
        <?php else: ?>
          <div>
            <iframe src="<?= getYoutubeEmbedUrl($data['url']); ?>?showinfo=0&rel=0&iv_load_policy=3&disablekb=1" width="740" height="460" allowfullscreen="allowfullscreen" frameborder="0" scrolling="no" seamless=""></iframe>
          </div>
        <?php endif ?>
     <?php elseif (!empty($data['video'])): ?>
      <div style="max-width: 950px;">
            <video id='my-video' class='video-js vjs-default-skin vjs-big-play-centered' width='740' height='460'
        poster="" data-setup='{"controls" : true, "preload" : "auto" ,"fluid": true}'>
        <source src='<?= base_url($data['video']);?>' type='video/mp4'>
          <p class='vjs-no-js'>
            To view this video please enable JavaScript, and consider upgrading to a web browser that
            <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
          </p>
        </video>
      </div>
      </div>
        <!-- <h6>Video format mp4.</h6> -->
      <?php else: ?>
        <h6>Video tidak dapat diputar.</h6>
      <?php endif ?>
    </div>
  </div>
