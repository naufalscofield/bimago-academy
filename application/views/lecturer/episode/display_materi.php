<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        Master <i class="fas fa-angle-double-right"></i>
        <a href="<?= base_url('lecturer/lecturer_course'); ?>"><?= $this->lang->line('courses').' : '.$judul; ?> <i class="fas fa-angle-double-right"></i></a> 
        <a href="<?= base_url('lecturer/lecturer_episodev2/index/'.$id_episode); ?>">Episode : <?=$judul_episode;?> <i class="fas fa-angle-double-right"></i></a> 
        <a href="#">
          <?= $this->lang->line('resource'); ?>
        </a>
      </h6>
    </div>
    <div class="card-body">
      <!-- <div align="left">
        <h3><?=$part;?></h3>
      </div> -->
      <div align="right">
        <a href="#" id_episode="<?=$id_episode;?>" class="btn btn-success btn-icon-split getModal" style="align:right;">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text"><?= $this->lang->line('add');?></span>
        </a>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="75%"><?= $this->lang->line('resource');?></th>
              <th width="20%"><i class="fas fa-cogs"></i></th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
              <?php if ($data['status'] == 200) { ?>
                  <tr id="<?php echo $value['id'] ?>">
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_file'];?></td>
                    <td>
                      <a style="color:white" id="<?=$value['id'];?>" title="Detail/Edit" id_episode="<?=$id_episode;?>" class="btn btn-info btn-sm getModal">
                        <i class="fas fa-info-circle"></i>
                      </a>
                       <a style="color:white" id="<?=$value['id'];?>" title="See" class="btn btn-sm btn-primary onPreview">
                         <i class="fa fa-eye"></i>
                       </a>
                      <a style="color:white" id="<?=$value['id'];?>" title="Delete" class="btn btn-danger btn-sm deleteData">
                        <i class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
              <?php }else{ ?>
                <td>Tidak Ada Data !</td>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){

    $(".onPreview").click(function(event){
      var id = $(this).attr('id');
        $.ajax({
            url: "<?=$get_pdf_preview;?>",
            method: "POST",
            data: {id:id},
            success: function(data){
              var hasil = $.parseJSON(data);
                window.open(hasil["url"], '_blank');
            }
        });
      });

    $(".getModal").click(function(event){
      var id = $(this).attr('id');
      var id_episode = $(this).attr('id_episode');
        $.ajax({
            url: "<?=$get_data_edit;?>",
            method: "POST",
            data: {id:id, id_episode:id_episode},
            success: function(data){
                $('#dataModalLarge').html(data);
                $('#ModalLarge').modal('show');
            }
        });
      });

    $(".deleteData").click(function(event){
      var id = $(this).attr('id');
      bootbox.confirm("Apakah anda yakin akan menghapus data ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_data_delete;?>",
              method: "POST",
              data: {id:id},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   toastr.error(hasil["pesan"]);
                 }
              },
              error: function (res) {
                toastr.error("Data tidak dapat dihapus.");
              },
          });
        }
      });
    });

  });
</script>
