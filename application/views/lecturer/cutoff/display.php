<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        <?= $this->lang->line('fee'); ?>
      </h6>
    </div>
    <div class="card-body">
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="10%">No.</th>
              <th width="60%"><?= $this->lang->line('description');?></th>
              <th width="35%"><?= $this->lang->line('cut_off'); ?></th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php
              foreach($cutoff as $idx => $co)
              {
            ?>
              <tr>
                <td><?= $idx+1; ?></td>
                <td>
                  <button class="btn btn-primary btn-xs" title="info" id="btn_detail_<?= $idx+1;?>"><i class="fa fa-info-circle"></i></button>
                  <?= $co['keterangan'];?>
                </td>
                <td>
                  <?php
                    $cut = ($co['jenis'] == 'persentase') ? $co['potongan'].'%' : "Rp " . number_format($co['potongan'],2,',','.');
                    echo $cut;
                  ?>
                </td>
              </tr>
              <?php
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="modal_detail_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><?= $this->lang->line('detail_jasa_bee_lms');?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_detail_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><?= $this->lang->line('detail_pg_bank');?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_detail_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><?= $this->lang->line('detail_pg_ewallet');?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="modal_detail_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><?= $this->lang->line('detail_admin_bank');?></p>
        </div>
      </div>
    </div>
  </div>

<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
    var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	  = '<?php echo $this->session->flashdata("error");?>'
    
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}

  })

  $(document).on("click", "#btn_detail_1", function(){
    $("#modal_detail_1").modal("show")
  })

  $(document).on("click", "#btn_detail_2", function(){
    $("#modal_detail_2").modal("show")
  })

  $(document).on("click", "#btn_detail_3", function(){
    $("#modal_detail_3").modal("show")
  })

  $(document).on("click", "#btn_detail_4", function(){
    $("#modal_detail_4").modal("show")
  })
</script>
