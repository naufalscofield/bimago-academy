<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        Monitoring <i class="fas fa-angle-double-right"></i>
        <a href="<?= base_url();?>lecturer/Lecturer_progres">
          <?= $this->lang->line('progress').' : '.$course->judul;?>
        </a> <i class="fas fa-angle-double-right"></i>
        <a href="#">
          <?= $this->lang->line('grade_recap')?>
        </a>
      </h6>
    </div>
    <div class="card-body">
      <div class="row">
        <select name="type_course" id="type_course" class="form-control" style="width:50%;margin-left:300px">
          <option value="">--Pilih Tipe Course--</option>
          <?php 
          foreach ($type as $t)
          {
            ?>
            <option value="<?=$t['id'];?>"><?= $t['type_course'];?></option>
            <?php
          }
          ?>
      </select>
      <button id="btn_tampil" class="btn btn-primary">Tampilkan</button>
    </div>
    <br> 
    <br> 
    <div class="row">
      <div class="spinner-border text-primary" style="display:none;margin-left:50%" id="spinner" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <div class="table-responsive">
      <div class="container" style="display:display" id="tableDiv">
        <table class="table table-striped table-bordered" id="table" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).on('click', '#btn_tampil', function(){
    var id_type = $("#type_course").val()
    if (id_type != "")
    {
      $('#table tbody').empty();
      $('#table thead').empty();
      // $('#table').dataTable().fnDestroy();
      $("#spinner").show()
      var url_header     = '<?php echo $url_header;?>'+'/'+id_type
      var url     = '<?php echo $url;?>'+'/'+id_type

      $.get(url_header, function(value, status){
        var val = JSON.parse(value)
        
          $("#table thead").append('<tr></tr>')
          $("#table thead tr").append('<th>No</th>')
          $("#table thead tr").append('<th>Nama</th>')
          // console.log(val.total_tugas[1].total)
          if (val.is_exam)
        {
          $("#table thead tr").append('<th>Exam</th>')
          $("#table thead tr").append('<th>Total Percobaan</th>')
          $("#table thead tr").append('<th>Total Menit Pengerjaan Exam</th>')
          $("#table thead tr").append('<th>Tanggal Kumpul Exam</th>')
        }
        console.log(val.total_quiz)
        for(i=1; i<=val.total_quiz; i++)
        {
          var idx = i-1
          // if (typeof val.total_tugas[idx] !== 'undefined')
          // {
            $("#table thead tr").append('<th>Tugas Modul '+i+'</th>')
            // $("#table thead tr").append('<th>Tugas Modul '+i+'</th>')
          // } 
        }
        
        if (id_type != 4)
        {
          for(i=1; i<=val.total_quiz; i++)
          {
            $("#table thead tr").append('<th> Quiz Modul '+i+'</th>')
          }
        }

        

      })
        .done(function() {
          $('#table').DataTable().destroy()
              table = $('#table').DataTable({ 
                "processing": true, 
                "serverSide": true, 
                
                // "sortable" : false,
                // "paging": true,
                // "pageLength": 10,
                "order": [], 
                  
                "ajax": {
                    "url": url,
                    "type": "POST"
                },

                  
                "columnDefs": [
                  { 
                      "targets": [ 0 ], 
                      "orderable": false, 
                  },
                ],

              });
                $("#spinner").hide()
                // $(".col").attr("colspan", 2)
                $("#tableDiv").show()
          // setTimeout(
          //   function() 
          //   {

          //   }, 1000);
        })
    }

  })
</script>
