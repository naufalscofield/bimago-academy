<div class="modal-header">
  <h5 class="modal-title" id="ModalLabelSmall"><b>Type Course</b></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <center>
    <?php foreach ($type as $key => $value): ?>
      <a href="<?=$link.'/'.$value['id'];?>" style="width:250px" class="btn btn-info"><?=$value['type_course'];?></a><br><br>
    <?php endforeach; ?>
  </center>
</div>
<div class="modal-footer">
</div>
