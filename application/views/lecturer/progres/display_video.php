<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        Monitoring <i class="fas fa-angle-double-right"></i>
        <a href="<?= base_url();?>lecturer/Lecturer_progres">
          <?= $this->lang->line('progress').' : '.$judul;?>
        </a> <i class="fas fa-angle-double-right"></i>
        <a href="#">
          <?= $this->lang->line('video_progress')?>
        </a>
      </h6>
    </div>
    <div class="card-body">
      <input type="hidden" name="id_course" value="<?=$id_course;?>">
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No.</th>
              <th width="25%">Nama Pelajar </th>
              <th width="30%">Tipe Kursus</th>
              <th width="30%">Persentase </th>
              <th width="5%"><i class="fas fa-cogs"></i></th>
            </tr>
          </thead>
          <tbody class="row_position">
              <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
                <?php if ($data['status'] == 200) { ?>
                  <tr>
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_depan'].' '.$value['nama_belakang'];?></td>
                    <td><?= $value['type_course'];?></td>
                    <td>
                      <?= $value['persentase'];?>%<br>
                      <div class="progress">
                        <div class="progress-bar" id="progress-bar-video" role="progressbar" style="width: <?=$value['persentase'];?>%;" aria-valuenow="<?=$value['persentase'];?>" aria-valuemin="0" aria-valuemax="100"><span id="progres-all-view"></div>
                      </div>
                    </td>
                    <td>
                      <a title="Edit/Info" id="<?=$no;?>" nama-depan="<?=$value['nama_depan'];?>" nama-belakang="<?=$value['nama_belakang'];?>" id-type-course="<?=$value['id_type_course'];?>" id-user="<?=$value['id_user'];?>" data-style="zoom-out" class="btn ladda-button btn-primary getModal ladda-update_<?=$no;?>">
                        <i style="color:white" class="fas fa-info-circle"></i>
                      </a>
                    </td>
                  </tr>
                <?php }else{ ?>
                  <td>Tidak Ada Data !</td>
                <?php } ?>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(".getModal").click(function(event){
    var id = $(this).attr('id');
    var id_type_course = $(this).attr('id-type-course');
    var id_user = $(this).attr('id-user');
    var nama_depan = $(this).attr('nama-depan');
    var nama_belakang = $(this).attr('nama-belakang');
    var l = Ladda.create( document.querySelector( '.ladda-update_' + id));
    l.start();
    $.ajax({
        url: "<?=$get_data;?>",
        method: "POST",
        data: {id_course:'<?=$id_course;?>', id_type_course : id_type_course, id_user : id_user, nama_depan : nama_depan, nama_belakang : nama_belakang},
        success: function(data){
          l.stop();
          $('#dataModalLarge').html(data);
          $('#ModalLarge').modal('show');
        },
        error: function (data) {
          l.stop();
          toastr.error("Data tidak dapat dibuka, coba kembali.");
        },
    });
  });
</script>
