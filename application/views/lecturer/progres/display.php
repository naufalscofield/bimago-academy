<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        Monitoring <i class="fas fa-angle-double-right"></i>

        <a href="#">
          <?= $this->lang->line('progress');?>
        </a>
      </h6>
    </div>
    <div class="card-body">
      <!-- <div align="right">
        <a href="#" class="btn btn-success btn-icon-split getModal" style="align:right;">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
      </div>
      <br> -->
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="15%">Pengajar</th>
              <th width="25%">Judul Kursus</th>
              <th width="10%">Kategori</th>
              <th width="10%">Video</th>
              <!-- <th width="10%">Quiz</th>
              <th width="10%">Exam</th> -->
              <th width="10%">Rekap Nilai</th> 
            </tr>
          </thead>
          <tbody class="row_position">
            <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
              <?php if ($data['status'] == 200) { ?>
                  <tr id="<?php echo $value['id'] ?>">
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_depan'].' '.$value['nama_belakang'];?></td>
                    <td><?= $value['judul'];?></td>
                    <td><?= $value['modul'];?></td>
                    <td>
                      <center>
                        <a style="color:white; cursor:pointer;" title="Video" class="btn btn-info getModal" id="<?=$value['id'];?>" link="<?=$video.$value['id'];?>">
      										 <i class="fas fa-video"></i>
      									</a>
                      </center>
                    </td>
                    <!-- <td>
                      <center>
                        <a style="color:white; cursor:pointer;" class="btn btn-info getModal" id="<?=$value['id'];?>" link="<?=$quiz.$value['id'];?>">
      										 <i class="fas fa-tasks"></i>
      									</a>
                      </center>
                    </td>
                    <td>
                      <center>
                        <a style="color:white; cursor:pointer;" class="btn btn-info getModal" id="<?=$value['id'];?>" link="<?=$exam.$value['id'];?>">
      										 <i class="fas fa-pencil-ruler"></i>
      									</a>
                      </center>
                    </td> -->
                    <td>
                      <center>
                        <a style="color:white; cursor:pointer;" title="Rekap" class="btn btn-warning" href="<?= base_url();?>lecturer/Progres/rekap-nilai/<?=$value['id'];?>">
      										 <i class="fas fa-file"></i>
      									</a>
                      </center>
                    </td> 
                  </tr>
              <?php }else{ ?>
                <td>Tidak Ada Data !</td>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<script>
   $(document).ready(function(){

     $(".getModal").click(function(event){
       var id = $(this).attr('id');
       var link = $(this).attr('link');
         $.ajax({
             url: "<?=$get_data_edit;?>",
             method: "POST",
             data: {id : id, link : link},
             success: function(data){
                 $('#dataModalSmall').html(data);
                 $('#ModalSmall').modal('show');
             },
             error: function (data) {
               toastr.error("Data tidak dapat dibuka, coba kembali.");
             },
         });
       });
   });
</script>
