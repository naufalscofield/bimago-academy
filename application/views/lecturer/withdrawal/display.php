<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <!-- <?php 
        foreach ($breadcrumbs as $idx => $br)
        {
      ?>
        <a href="<?= base_url();?>lecturer/<?= $br['link'];?>">
          <?= $br['title'];?> <?= (isset($br['akhir'])) ? '' : '<i class="fas fa fa-angle-double-right"></i>';?>
        </a>
      <?php
        }
      ?> -->
      <h6 class="m-0 font-weight-bold text-primary">
        <?=$this->lang->line('transaction');?> <i class="fas fa-angle-double-right"></i>
        <a href="">
          <?=$this->lang->line('withdrawal');?>
        </a>
      </h6>
    </div>
    <div class="card-body">

    <div class="row">
      <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-2">
              <div class="card-body">
                  <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                              <?= $this->lang->line('current_balance'); ?></div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <?php
                            if ($saldo !== NULL)
                            {
                              echo "Rp " . number_format($saldo->saldo,2,',','.');
                            } else
                            {
                              echo "Rp " . number_format(0,2,',','.');
                            }
                            ?>
                          </div>
                      </div>
                      <div class="col-auto">
                          <i class="fas fa-money-bill-wave fa-2x text-gray-300"></i>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-danger shadow h-100 py-2">
              <div class="card-body">
                  <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                          <?= $this->lang->line('withdrawable_money'); ?>
                          
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp " . number_format($withdrawalable,2,',','.'); ?></div>
                        </div>
                        <div class="col-auto">
                          <i class="fas fa-wallet fa-2x text-gray-300"></i>
                        </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-success shadow h-100 py-2">
              <div class="card-body">
                  <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                          <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                        </div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800">
                            <button id="btn_withdraw" <?= ($withdrawalable < 500000) ? 'disabled' : '';?> class="btn btn-success" style="width:100%"><?= $this->lang->line('withdrawal_request'); ?></button>
                            <?php
                              if($withdrawalable < 500000)
                              {
                            ?>
                                <p style="color:red;font-size:13px" for="">*<?= $this->lang->line('minimum_withdrawal'); ?></p>
                            <?php
                              }
                            ?>
                          </div>
                        </div>
                        <div class="col-auto">
                          <i class="fas fa-hand-holding-usd fa-2x text-gray-300"></i>
                        </div>
                  </div>
              </div>
          </div>
      </div>
    </div>

      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="20%"><?= $this->lang->line('date');?></th>
              <th width="10%">Nominal (Rp.)</th>
              <th width="10%">Status</th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php
              foreach($withdrawal as $idx => $d)
              {
            ?>
              <tr>
                <td><?= $idx+1 ;?></td>
                <td><?= date("d F Y - H:i:s", strtotime($d['created_at']));?></td>
                <td><?= "Rp " . number_format($d['total'],2,',','.'); ?></td>
                <td>
                  <?php
                    switch ($d['status']) {
                      case 1:
                        echo "<span style='font-size:15px' class='badge badge-success'>".$this->lang->line("verification_by_admin").' '.env("APP_NAME")."</span>";
                        break;
                      case 2:
                        echo "<span style='font-size:15px' class='badge badge-success'>".$this->lang->line("withdrawal_process")."</span>";
                        break;
                      case 3;
                        echo "<span style='font-size:15px' class='badge badge-success'>".$this->lang->line("done")."</span>";
                        break;
                    }
                  ?>
                </td>
              </tr>
            <?php
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Voucher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>lecturer/voucher-store" method="POST">
            <div class="form-group">
              <label for="">Kode</label>
              <input type="text" name="kode" class="form-control" required><br>

              <label for="">Discount (%)</label>
              <input type="number" name="discount" class="form-control" required><br>

              <label for="">Expired</label>
              <input type="date" min="<?= date('Y-m-d');?>" name="expired" class="form-control" required><br>

              <label for="">Kuota</label>
              <input type="number" name="kuota" class="form-control" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </form>
  </div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
		var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	= '<?php echo $this->session->flashdata("error");?>'
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}
	})

  $(document).on('click', '#btn_withdraw', function(){
    var confirm = "<?php echo $this->lang->line('confirm_withdrawal');?>"+"<?php echo "Rp " . number_format($withdrawalable,2,',','.'); ?> ?"
    bootbox.confirm(confirm, function(result){
      if(result == true) {
        var route = '<?php echo base_url();?>lecturer/withdrawal-request/'

        $.get(route, function(data, status){
          if (status)
          {
            toastr.success(data)
            setTimeout(function(){ location.reload() }, 1000);
          }
        }).fail(function(data, error) {
            var err = eval("(" + data.responseText + ")");
            toastr.error(err)
        })
        
      }
    });
  })

</script>
