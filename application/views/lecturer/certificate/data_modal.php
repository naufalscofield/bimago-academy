<?php echo form_open_multipart($simpan, array('name' => 'modal-course', 'id' => 'modal-course')); ?>
  <div class="modal-header">
    <h5 class="modal-title" id="ModalLabelLarge"><b>Certificate</b></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <input type="hidden" name="id" value="<?= !empty($data['id']) ? $data['id'] : '';?>">
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Course</label>
        <?php echo form_dropdown('id_course', $opt_modul, !empty($data['id_course']) ? $data['id_course'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Type Certificate</label>
        <?php echo form_dropdown('type', ['course'=>'Course', 'webinar'=> 'Webinar'], !empty($data['type']) ? $data['type'] : '', 'class="form-control form-control-user" onchange="hideinput(this)"');?>
      </div>
    </div>
    <div class="form-group row d-none">
      <div class="col-sm-12">
        <label>Certificate Prefix</label>
        <?php echo form_input('certificate_number', !empty($data['certificate_number']) ? $data['certificate_number'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Certificate Category</label>
        <?php echo form_input('certificate_category', !empty($data['certificate_category']) ? $data['certificate_category'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Duration</label>
        <?php echo form_input('duration', !empty($data['duration']) ? $data['duration'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#signature1" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="signature1">
          <h6 class="m-0 font-weight-bold text-primary">Signature 1</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse" id="signature1">
          <div class="card-body">
              <div class="form-group row" id="institution1">
                <div class="col-sm-12">
                  <label>Institution</label>
                  <?php echo form_input('institution1', !empty($data['institution1']) ? $data['institution1'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12">
                  <label>Signature Name</label>
                  <?php echo form_input('signature_name', !empty($data['signature_name']) ? $data['signature_name'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12">
                  <label>Signature Title</label>
                  <?php echo form_input('signature_title', !empty($data['signature_title']) ? $data['signature_title'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
              <?php if (isset($data['sign_pic1'])): ?>
              <div class="form-group row" style="background-color:#3258c9;" align="center">
                <div class="col-sm-12">
                  <img src="<?= base_url().$data['sign_pic1'];?>" alt="" height="140">
                </div>
              </div>
              <?php endif ?>
              <div class="form-group row">
                <div class="col-sm-12">
                  <label>File</label>
                  <?php echo form_upload('sign_pic1', '', 'class="form-control form-control-user"');?>
                  <?php echo form_input('sign_pic1_old', !empty($data['sign_pic1']) ? $data['sign_pic1'] : '', 'class="form-control form-control-user" hidden');?>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#signature2" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="signature2">
          <h6 class="m-0 font-weight-bold text-primary">Signature 2</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse" id="signature2">
          <div class="card-body">
            <div class="form-group row" id="institution2">
                <div class="col-sm-12">
                  <label>Institution</label>
                  <?php echo form_input('institution2', !empty($data['institution2']) ? $data['institution2'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
             <div class="form-group row">
                <div class="col-sm-12">
                  <label>Signature Name 2</label>
                  <?php echo form_input('signature_name2', !empty($data['signature_name2']) ? $data['signature_name2'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12">
                  <label>Signature Title 2</label>
                  <?php echo form_input('signature_title2', !empty($data['signature_title2']) ? $data['signature_title2'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
              <?php if (isset($data['sign_pic2'])): ?>
              <div class="form-group row" style="background-color:#3258c9;" align="center">
                <div class="col-sm-12">
                  <img src="<?= base_url().$data['sign_pic2'];?>" alt="" height="140">
                </div>
              </div>
              <?php endif ?>
              <div class="form-group row">
                <div class="col-sm-12">
                  <label>File</label>
                  <?php echo form_upload('sign_pic2', '', 'class="form-control form-control-user"');?>
                  <?php echo form_input('sign_pic2_old', !empty($data['sign_pic2']) ? $data['sign_pic2'] : '', 'class="form-control form-control-user" hidden');?>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div id="presented">
        <div class="card shadow mb-4">
          <!-- Card Header - Accordion -->
          <a href="#presented1" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="presented1">
            <h6 class="m-0 font-weight-bold text-primary">Presented 1</h6>
          </a>
          <!-- Card Content - Collapse -->
          <div class="collapse" id="presented1">
            <div class="card-body">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <label>Presented Name 1</label>
                    <?php echo form_input('presented_name1', !empty($data['presented_name1']) ? $data['presented_name1'] : '', 'class="form-control form-control-user"');?>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-12">
                    <label>Presented Title 1</label>
                    <?php echo form_input('presented_title1', !empty($data['presented_title1']) ? $data['presented_title1'] : '', 'class="form-control form-control-user"');?>
                  </div>
                </div>
            </div>
          </div>
        </div>

        <div class="card shadow mb-4">
          <!-- Card Header - Accordion -->
          <a href="#presented2" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="presented2">
            <h6 class="m-0 font-weight-bold text-primary">Presented 2</h6>
          </a>
          <!-- Card Content - Collapse -->
          <div class="collapse" id="presented2">
            <div class="card-body">
              <div class="form-group row">
                <div class="col-sm-12">
                  <label>Presented Name 2</label>
                  <?php echo form_input('presented_name2', !empty($data['presented_name2']) ? $data['presented_name2'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-12">
                  <label>Presented Title 2</label>
                  <?php echo form_input('presented_title2', !empty($data['presented_title2']) ? $data['presented_title2'] : '', 'class="form-control form-control-user"');?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="modal-footer">
  <button type="button" id="simpan" class="btn btn-dark">Simpan</button>
  <!-- <button type="button" id="simpan" class="btn btn-dark" <?=$disable;?>>Simpan</button> -->
<?php echo form_close() ?>

<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
  $( document ).ready(function() {
      var v =  $('select[name=type] option').filter(':selected').val();
      hideinput(null,v);
  });

  function hideinput(obj, value = ''){
    if (value != '') {
      var type =  value;
    }else{
      var type =  $('select[name=type] option').filter(':selected').val();
    }
    if (type == 'course') {
      $("#presented").hide();
      $("#institution1").hide();
      $("#institution2").hide();
    }else{
      $("#presented").show();
      $("#institution1").show();
      $("#institution2").show();
    }
  }

  $("#simpan").click(function(){
    for (instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].updateElement();
    }
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
    bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function(result){
      if(result == true) {
        $.ajax({
          type: form.attr("method"),
          url: form.attr("action"),
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          success: function (res) {
            var hasil = $.parseJSON(res);
            if (hasil["status"] == 200) {
               toastr.success(hasil["pesan"]);
               setTimeout(function () {
                 return hasil["status"];
               }, 1500);
               $('#ModalLarge').modal('hide');
               setTimeout(function () {
                $('#dataTable').DataTable().ajax.reload();
                 // location.reload(true);
               }, 1500);
             }else{
               toastr.error(hasil["pesan"]);
               result = false;
               return hasil["status"];
             }
           },
           error: function (res) {
             toastr.error("Data tidak dapat disimpan.");
             result = false;
             return false;
           },
        });
      }
    });
  });
</script>
