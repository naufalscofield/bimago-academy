<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?=$judul;?></h6>
    </div>
    <div class="card-body">
      <div align="right">
        <a href="#" id="" data-toggle="modal" data-target="#modal_add" class="btn ladda-button btn-success btn-icon-split getModal ladda-update_" style="align:right;">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="20%">Kode</th>
              <th width="10%">Discount (%)</th>
              <th width="10%">Expired</th>
              <th width="10%">Kuota</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php
            if ($data['status'] == 200)
            {
              foreach($data['data'] as $idx => $d)
              {
            ?>
              <tr>
                <td><?= $idx+1 ;?></td>
                <td><?= $d['kode'];?></td>
                <td><?= $d['discount'];?></td>
                <td><?= $d['expired'];?></td>
                <td><?= $d['kuota'];?></td>
                <!-- <td><?= $d['counter'];?></td> -->
                <td>
                  <button data-id="<?= $d['id'];?>" class="btn btn-info btn-sm btn-edit"><i class="fas fa-pen"></i></button>
                  <button data-id="<?= $d['id'];?>" class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash"></i></button>
                </td>
              </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Voucher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>lecturer/voucher-store" method="POST">
            <div class="form-group">
              <label for="">Kode</label>
              <input type="text" name="kode" class="form-control" required><br>

              <label for="">Discount (%)</label>
              <input type="number" name="discount" class="form-control" required><br>

              <label for="">Expired</label>
              <input type="date" min="<?= date('Y-m-d');?>" name="expired" class="form-control" required><br>

              <label for="">Kuota</label>
              <input type="number" name="kuota" class="form-control" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </form>
  </div>
</div>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Voucher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>lecturer/voucher-update" method="POST">
            <div class="form-group">
              <input type="hidden" name="edit_id" id="edit_id" class="form-control" required><br>

              <label for="">Kode</label>
              <input type="text" name="edit_kode" id="edit_kode" class="form-control" required><br>

              <label for="">Discount (%)</label>
              <input type="number" name="edit_discount" id="edit_discount" class="form-control" required><br>

              <label for="">Expired</label>
              <input type="date" min="<?= date('Y-m-d');?>" name="edit_expired" id="edit_expired" class="form-control" required><br>

              <label for="">Kuota</label>
              <input type="number" name="edit_kuota" id="edit_kuota" class="form-control" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
      </form>
  </div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
		var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	= '<?php echo $this->session->flashdata("error");?>'
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}
	})

  $(document).on('click', '.btn-delete', function(){
    var id    = $(this).data("id")
    bootbox.confirm("Apakah anda yakin akan mengahapus voucher ini?", function(result){
      if(result == true) {
        var route = '<?php echo base_url();?>admin/voucher-delete/'+id

        $.get(route, function(data, status){
          if (status == 'success')
          {
            window.location.href = "<?php echo base_url();?>admin/voucher"
          }
        })
        
      }
    });
  })

  $(document).on('click', '.btn-edit', function(){
    $("#modal_edit").modal("show")
    var id  = $(this).data("id")
    var route = '<?php echo base_url();?>lecturer/voucher-show/'+id

    $.get(route, function(data, status){
      if (status == 'success')
      {
        var data = JSON.parse(data)
        $("#edit_id").val(data.id)
        $("#edit_kode").val(data.kode)
        $("#edit_discount").val(data.discount)
        $("#edit_expired").val(data.expired)
        $("#edit_kuota").val(data.kuota)
      }
    })
  })
</script>
