<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        Monitoring <i class="fas fa-angle-double-right"></i>
        <a href="<?= base_url();?>lecturer/Lecturer_webinar">
          Webinar : <?= $judul;?> <i class="fas fa-angle-double-right"></i>
        </a>
        <a href="#">
          <?= $this->lang->line('member_list');?>
        </a>
      </h6>
    </div>
    <div class="card-body">
      <?php echo form_open_multipart($email, array('name' => 'form-member', 'id' => 'form-member')); ?>
      <input type="hidden" name="id_course" value="<?=$id_course;?>">
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%"></th>
              <th width="5%">No.</th>
              <th width="25%">Nama </th>
              <th width="30%">Email</th>
              <th width="10%">Status</th>
              <th width="25%">Tanggal Pembelian</th>
            </tr>
          </thead>
          <tbody class="row_position">
              <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
                <?php if ($data['status'] == 200) { ?>
                  <tr>
                    <td>
                      <input type="checkbox" name="member[<?= $value['id'];?>]" value="<?=$value['email'];?>">
                    </td>
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_depan'].' '.$value['nama_belakang'];?></td>
                    <td><?= $value['email'];?></td>
                    <td style="background-color: <?= $value['status'] == 1 ? '#cbf5cf;' : '#f5a2a7';?>">
                      <?= $value['status'] == 1 ? 'Lunas' : 'Menunggu';?>
                    </td>
                    <td><?= indo_date($value['tanggal_pembelian']);?></td>
                  </tr>
                <?php }else{ ?>
                  <td>Tidak Ada Data !</td>
                <?php } ?>
              <?php } ?>
          </tbody>
        </table>
      </div>
      <button type="button" id="simpan" class="btn btn-dark">Kirim</button>
      <?php echo form_close() ?>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
$("#simpan").click(function(){
  var form = $("#" + $(this).closest('form').attr('name'));
  var formdata = false;
  if (window.FormData) {
    formdata = new FormData(form[0]);
  }
  bootbox.confirm("Apakah anda yakin akan mengirim email?", function(result){
    if(result == true) {
      $.ajax({
        type: form.attr("method"),
        url: form.attr("action"),
        data: formdata ? formdata : form.serialize(),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["status"] == 200) {
             toastr.success(hasil["pesan"]);
             setTimeout(function () {
               location.reload(true);
             }, 1500);
           }else{
             toastr.error(hasil["pesan"]);
           }
         },
         error: function (res) {
           toastr.error("Data tidak dapat disimpan.");
         },
      });
    }
  });
});
</script>
