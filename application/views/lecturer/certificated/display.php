<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        Monitoring <i class="fas fa-angle-double-right"></i>
        <a href="#">
          <?= $this->lang->line('certificate_list');?>
        </a>
      </h6>
    </div>
    <div class="card-body">
      <!-- <div align="right" styl>
        <a href="#" class="btn btn-success btn-icon-split getModal" style="align:right;">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
      </div> -->
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="15%">Flier</th>
              <th width="30%">Judul Webinar</th>
              <th width="15%">Kategori</th>
              <th width="15%"><i class="fas fa-cogs"></th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
              <?php if ($data['status'] == 200) { ?>
                  <tr id="<?php echo $value['id'] ?>">
                    <td><?= $no;?>.</td>
                    <td>
                      <center>
                        <img src="<?=base_url().$value['image'];?>" alt="" style="width:250px;height:auto;">
                      </center>
                    </td>
                    <td><?= $value['judul'];?></td>
                    <td><?= $value['modul'];?></td>
                    <td>
                      <center>
                      <a href="<?=$member.$value['id'];?>" title="Member" class="btn btn-sm btn-info">
    										<i class="fas fa-user"></i>
    									</a>
                      <button class="btn btn-sm btn-primary" title="Generate Certificate To All Member Manually" type="button" value="Generate" onclick="onGenerate(<?= $value['id'];?>)"><i class="fa fa-cog"></i></button>
                    </center>
                    </td>
                  </tr>
              <?php }else{ ?>
                <td>Tidak Ada Data !</td>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
    $("#load_more").click(function(){
      $("#more").show();
      $("#less").hide();
    });
    $("#load_less").click(function(){
      $("#less").show();
      $("#more").hide();
    });


  });
  function onGenerate(id){
    $.ajax({
      url: "<?=$get_data_generate;?>",
      method: "POST",
      data: {id: id},
      success: function (res) {
        let r = JSON.parse(res)
          if (r["status"] == true) {
           toastr.success(r["msg"]);
          }else{
           toastr.error(r["msg"]);
          }
      },
      error: function (res) {
      },
  });
  }
</script>
