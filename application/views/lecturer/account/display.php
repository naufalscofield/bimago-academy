<style>
    .profil {
      border-radius: 60%;
    }

    .contact-us {
        position: relative;
        z-index: 43;
    }
    .contact-us .title{
        margin-bottom: 30px;
    }
    .contact-us .title h4 {
        font-size: 17px;
        font-weight: 500;
        margin-bottom: 5px;
        color: #D6AF40;
    }
    .contact-us .title h3 {
        font-size: 25px;
        text-transform: capitalize;
        font-weight: 600;
    }
    .contact-us .single-head {
        padding: 50px;
        box-shadow: 0px 0px 15px #0000001a;
        height: 100%;
    }
    .contact-us .single-info {
        text-align: left;
        margin-bottom:30px;
    }
    .contact-us .single-info i {
        color: #fff;
        font-size: 18px;
        display: inline-block;
        margin-bottom: 15px;
        height: 40px;
        width: 40px;
        display: block;
        text-align: center;
        border-radius: 3px;
        line-height: 40px;
        background:#D6AF40;
    }
    .contact-us .single-info ul
    .contact-us .single-info ul li{
        margin-bottom:5px;
    }
    .contact-us .single-info ul li:last-child{
        margin-bottom:0;
    }
    .contact-us .single-info ul li a{
        font-weight:400;
    }
    .contact-us .single-info ul li a:hover{
        color:#D6AF40;
    }
    .contact-us .single-info .title {
        margin-bottom: 10px;
        font-weight: 500;
        color: #333;
        font-size: 18px;
    }
    .contact-us .form-main {
        box-shadow: 0px 0px 15px #0000001a;
        padding: 50px;
    }
    .contact-us .form .form-group input {
        height: 48px;
        line-height: 48px;
        width: 100%;
        border: 1px solid #e6e2f5;
        padding: 0px 20px;
        color: #333;
        border-radius: 0px;
        font-weight: 400;
    }
    .contact-us .form .form-group textarea {
        height: 180px;
        width: 100%;
        border: 1px solid #e6e2f5;
        padding: 15px 20px;
        color: #333;
        border-radius: 0px;
        resize: none;
        font-weight:400;
    }
    .contact-us .form .form-group label {
        color: #333;
        position: relative;
    }
    .contact-us .form .form-group label span {
        color: #ff2c18;
        display: inline-block;
        position: absolute;
        right: -12px;
        top: 4px;
        font-size: 16px;
    }
    .contact-us .form .button {
        margin:0;
    }
    .contact-us .form .button .btn {
        height: 50px;
        border: none;
    }
    #myMap {
        height: 500px;
        width: 100%;
    }
</style>

<!-- Start Contact -->
<section id="contact-us" class="contact-us section">
    <div class="container">
            <div class="contact-head">
                <div class="row">
                <div class="col-lg-4 col-12 text-center">
                        <div class="single-head">
                            <div class="single-info">
                              <center>
                                <?php if ($this->session->userdata('login_via') == 'solmit') { ?>
                                    <?php if ($data->avatar == null || $data->avatar == '') { ?>
                                        <?php if ($data->jk == 'W') { ?>
                                            <img class="profil" src="<?= base_url();?>assets/default/female_avatar_s0lm1t.png" alt="" style="width:170px;height:170px;">
                                        <?php }else{ ?>
                                            <img class="profil" src="<?= base_url();?>assets/default/male_avatar_s0lm1t.png" alt="" style="width:170px;height:170px;">
                                        <?php } ?>
                                    <?php }else{ ?>
                                        <img class="profil" src="<?= base_url();?>assets/avatar/<?=$data->avatar;?>" alt="" style="width:170px;height:170px;">
                                  <?php } ?>
                                <?php }else{ ?>
                                  <img class="profil" src="<?=$data->avatar;?>" alt="" style="width:170px;height:170px;">
                                <?php } ?>
                              </center>
                              <br>
                              <div class="single-info text-center">
                                <button data-toggle="modal" data-target="#modalPicture" class="btn btn-primary"><?= $this->lang->line('change_picture'); ?></button>
                              </div>
                            </div>
                            <?php if ($this->session->userdata('login_via') == 'solmit') { ?>
                              <div class="single-info text-center">
                                <center>
                                  <i class="fa fa-key"></i>
                                </center>
                                <button data-toggle="modal" data-target="#modalPassword" class="btn btn-primary"><?= $this->lang->line('change_password') ?></button>
                              </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-8 col-12">
                        <div class="form-main">
                            <div class="title">
                                <h3><?= $this->lang->line('account') ?></h3>
                                <h4><?= $this->lang->line('acc_desc') ?></h4>
                            </div>
                            <form class="form" id="form-user" method="POST" action="<?= base_url();?>lecturer/update-account">
                                <div class="row">
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('first_name') ?></label>
                                            <input name="nama_depan" id="nama_depan" value="<?= $data->nama_depan;?>" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('last_name') ?></label>
                                            <input name="nama_belakang" id="nama_belakang" value="<?= $data->nama_belakang;?>"type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('your_email') ?></label>
                                            <input name="email" id="email" value="<?= $data->email;?>" type="email" <?= $this->session->userdata('login_via') == 'solmit' ? '' : 'disabled';?>>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('your_phone') ?></label>
                                            <input name="no_hp" id="no_hp" type="number" value="<?= $data->no_hp;?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                          <label><?= $this->lang->line('gender'); ?></label><br><br>
                                          <input style="width:20px;height:20px;" name="jk" id="jk" type="radio" value="P" <?= $data->jk == 'P' ? 'checked' : '' ;?>> Pria &emsp;
                                          <input style="width:20px;height:20px;" name="jk" id="jk" type="radio" value="W" <?= $data->jk == 'W' ? 'checked' : '' ;?>> Wanita
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('institution'); ?></label>
                                            <input name="institusi" id="institusi" type="text" value="<?= $data->institusi;?>">
                                        </div>
                                    </div>               
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                        <label><?= $this->lang->line('account_number'); ?></label>
                                        <input type="text" name="no_rek" value="<?= isset($data_detail->no_rek) ? $data_detail->no_rek : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12" >
                                        <div class="form-group">
                                        <label><?= $this->lang->line('bank_name');?></label>
                                        <select required class="form-control" name="nama_bank" id="">
                                            <option value="">--<?= $this->lang->line('select'); ?>--</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'mandiri') ? 'selected' : '' ;?>value="mandiri">Mandiri</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'cimb') ? 'selected' : '' ;?>value="cimb">CIMB Niaga</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'bni') ? 'selected' : '' ;?>value="bni">BNI</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'hsbc') ? 'selected' : '' ;?>value="hsbc">HSBC</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'bca') ? 'selected' : '' ;?>value="bca">BCA</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'citibank') ? 'selected' : '' ;?>value="citibank">Citibank</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'bri') ? 'selected' : '' ;?>value="bri">BRI</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'dbs') ? 'selected' : '' ;?>value="dbs">DBS</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'danamon') ? 'selected' : '' ;?>value="danamon">Danamon</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'btn') ? 'selected' : '' ;?>value="btn">BTN</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'ocbc') ? 'selected' : '' ;?>value="ocbc">OCBC NISP</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'muamalat') ? 'selected' : '' ;?>value="muamalat">Muamalat</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'panin') ? 'selected' : '' ;?>value="panin">Panin</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'bsi') ? 'selected' : '' ;?>value="bsi">BSI</option>
                                            <option <?= (isset($data_detail->nama_bank) && $data_detail->nama_bank  == 'bca_s') ? 'selected' : '' ;?>value="bca_s">BCA Syariah</option>
                                        </select>
                                        <!-- <input type="text" name="nama_bank" value="<?= isset($data_detail->nama_bank) ? $data_detail->nama_bank : ''; ?>"> -->
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <div class="form-group">
                                        <label><?= $this->lang->line('account_name');?></label>
                                        <input type="text" name="atas_nama_rek" value="<?= isset($data_detail->atas_nama_rek) ? $data_detail->atas_nama_rek : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('profile');?></label>
                                            <?php echo form_textarea('profil', !empty($data->profil) ? $data->profil : '', 'class="form-control form-control-user" id="ckeditor"');?>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group button">
                                            <button type="submit" data-style="expand-left" class="btn btn-primary ladda-button ladda-user"><?= $this->lang->line('update_profil');?></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!--/ End Contact -->

<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <form action="<?=base_url();?>lecturer/change-password" method="POST" id="form-password">
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="quickview-content">
                                <h2><?= $this->lang->line('change_password') ?></h2>
                                <div class="size">
                                    <div class="row">
                                            <div class="col-lg-6 col-6">
                                                <h5 class="title"><?= $this->lang->line('old_pass') ?></h5>
                                                <input type="password" required name="old_password" id="old_password" style="width:100%">
                                            </div>
                                            <div class="col-lg-6 col-6">
                                                <h5 class="title"><?= $this->lang->line('new_pass') ?></h5>
                                                <input type="password" required name="new_password" id="new_password" style="width:100%">
                                            </div>
                                        </div>
                                        <br>
                                        <center>
                                            <button class="btn btn-primary ladda-pass" style="width:100%" type="submit"><?= $this->lang->line('save') ?></button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<div class="modal fade" id="modalPicture" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <form action="<?=base_url();?>lecturer/change-picture" method="POST" enctype="multipart/form-data" id="form-picture">
                <div class="modal-body">
                    <div class="row no-gutters">
                        <div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="quickview-content">
                                <h2><?= $this->lang->line('change_picture') ?></h2>
                                <div class="size">
                                    <div class="row">
                                            <div class="col-lg-6 col-6">
                                                <h5 class="title"><?= $this->lang->line('select_an_image') ?></h5>
                                                <input type='file' id="file" name="avatar"/>
                                            </div>
                                            <div class="col-lg-6 col-6">
                                                <h5 class="title"><?= $this->lang->line('preview_image') ?></h5>
                                                <img id="blah" src="#" style="display:none" alt="your image" style="width:150px;height:150px;"/>
                                            </div>
                                        </div>
                                        <br>
                                        <center>
                                            <button class="btn btn-primary ladda-pic" style="width:100%" type="submit"><?= $this->lang->line('save') ?></button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<script>

CKEDITOR.replace('ckeditor');

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#blah").show()
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#file").change(function(){
    $("#blah").hide()
    readURL(this);
});

    $("#form-password").submit(function (event) {
      var l = Ladda.create( document.querySelector( '.ladda-pass' ) );
        event.preventDefault();
        l.start();
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var formData = {
        new_password: $("#new_password").val(),
        old_password: $("#old_password").val(),
        };
      $.ajax({
        url: url,
        method: method,
        data: formData,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["proses"] == 'success') {
            l.stop();
                toastr.success(hasil["pesan"]);
            setTimeout(function () {
              location.reload(true);
            }, 1500);
          }else{
            l.stop();
            toastr.error(hasil["pesan"]);
          }
        },
        error: function (res) {
          l.stop();
          toastr.error("Oops, something went wrong, change password failed.");
        },
      });
    });


    $("#form-user").submit(function (event) {
      for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
      }
      var l = Ladda.create( document.querySelector( '.ladda-user' ) );
        event.preventDefault();
        l.start();
        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var formData = $('#form-user').serialize();
      $.ajax({
        url: url,
        method: method,
        data: formData,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["proses"] == 'success') {
            l.stop();
                toastr.success(hasil["pesan"]);
            setTimeout(function () {
              location.reload(true);
            }, 1500);
          }else{
            l.stop();
            toastr.error(hasil["pesan"]);
          }
        },
        error: function (res) {
          l.stop();
          toastr.error("Oops, something went wrong, change password failed.");
        },
      });
    });
</script>
