<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
	.blink_me {
	animation: blinker 1s linear infinite;
	}

	@keyframes blinker {
		50% {
			opacity: 0;
		}
	}
	.delete-media-answer{
		right:0px;
		position: absolute;
	}
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
		<?php
		if ($data_exam->tipe == 'exam'){
		?>
			<h6 class="m-0 font-weight-bold text-primary">
				Master <i class="fas fa-angle-double-right"></i>
				<a href="<?= base_url('lecturer/lecturer_course'); ?>"><?= $this->lang->line('courses').' : '.$course->judul; ?> <i class="fas fa-angle-double-right"></i></a> 
				<a href="<?= base_url('lecturer/lecturer_exam/index/'.$course->id); ?>"><?= $this->lang->line('exam');?> <i class="fas fa-angle-double-right"></i></a> 
				<a href="#">
					<?= $this->lang->line('question'); ?>
				</a>
			</h6>
		<?php
		} else
		{
			?>
      		<h6 class="m-0 font-weight-bold text-primary">
			  	Master <i class="fas fa-angle-double-right"></i>
				<a href="<?= base_url('lecturer/lecturer_course'); ?>"><?= $this->lang->line('courses').' : '.$course->judul; ?> <i class="fas fa-angle-double-right"></i></a> 
				<a href="<?= base_url('lecturer/lecturer_episodev2/index/'.$episode->id); ?>">Episode : <?=$episode->judul;?> <i class="fas fa-angle-double-right"></i></a> 
				<a href="<?= base_url('lecturer/quiz/'.$episode->id); ?>"><?= $this->lang->line('quiz');?> <i class="fas fa-angle-double-right"></i></a> 
				<a href="#">
					<?= $this->lang->line('question'); ?>
				</a>
			</h6>
		<?php
		}
		?>
		</div>
		<div class="card-body">
			<div class="row">
					<div class="col-md-8">
						<div class="col-md-4">
						<b><p id="totalSoal"><?= $this->lang->line('question_total'); ?> : <span class="badge badge-primary" style="font-size:15px"><?= $total_question->num_rows();?></span></p></b>
						<form action="<?= base_url();?>lecturer/store-question-used" method="POST">
						<input type="hidden" name="id_exam" value="<?= $id_exam;?>">
						<b><label><?= $this->lang->line('used_question'); ?> : 
							<div class="blink_me" style="display: <?= ($data_exam->jumlah_soal_dipakai == 0) ? 'block' : 'none';?>">
								<span class="badge badge-danger"><?= $this->lang->line('question_is_0');?></span>
							</div>
							<input style="width:103%" required class="form-control form-control-sm" max="<?= $total_question->num_rows();?>" name="jumlah_soal_dipakai" value="<?= $data_exam->jumlah_soal_dipakai;?>" type="number"></label>
						</b>
						<input type="submit" style="width:70%" class="btn btn-info btn-sm" value="<?= $this->lang->line('save');?>">
						</form>
						</div>
					</div>
				<div class="col-md-4">
					<div class="text-right">
						<button type="button" style="width:70%" class="btn btn-primary mb-1" data-toggle="modal" data-target="#modalAdd">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text"><?= $this->lang->line('add_single_question');?></span>
						</button>

						<a style="width:70%" class="btn btn-warning mb-1" href="<?= base_url();?>format_import_question_answers.xlsx" download>
							<span class="icon text-white-50">
								<i class="fas fa-download"></i>
							</span>
							<span class="text">Download Format Excel</span>
						</a>

						<form action="<?= base_url();?>lecturer/import-questions" method="POST" enctype="multipart/form-data">
							<input type="hidden" value="<?=$id_exam;?>" name="id_exam">
							<input required type="file" name="file_excel" style="width:70%" class="mb-1">
							<button type="submit" style="width:70%" class="btn btn-success">
								<span class="icon text-white-50">
									<i class="fas fa-file-excel"></i>
								</span>
								<span class="text"><?= $this->lang->line('import_question');?></span>
							</button>
						</form>
						<br>
					</div>
				</div>
			</div>
			<br>
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="dataTableQuestions" width="100%" cellspacing="0">
					<thead class="thead-dark">
						<tr>
							<th width="5%">No. </th>
							<th width="70%"><?= $this->lang->line('question');?></th>
							<th width="3%"><?= $this->lang->line('score');?></th>
							<th width="95%"><?= $this->lang->line('answer');?></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bd-example-modal-lg" id="modalAnswers" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"><?= $this->lang->line('answer');?></h5>
			<h6 id="namaSoal"></h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="d-flex justify-content-center">
               <div class="spinner-border text-primary text-center" id="spinnerAnswers" role="status">
                  <span class="sr-only"><?= $this->lang->line('loading');?>...</span>
               </div>
            </div>
		</div>
		<form action="<?= base_url();?>lecturer/store-answers" method="POST" id="formAnswers" style="display:none">
			<div class="ml-4 mr-4">
				<p style="color:red">*<?= $this->lang->line('select_correct_answer');?></p>
			</div>
			<input type="hidden" name="ans_id_question" id="ansIdQuestion">
			<input type="hidden" name="ans_id_exam" value="<?= $id_exam;?>" id="ansIdExam">
			<input type="hidden" name="ans_tipe_question" value="" id="ansTipeQuestion">
			<div id="listAnswers" class="ml-4 mr-4">

			</div>
			<div id="listAnswersAdd" class="ml-4 mr-4">

			</div>
			<div class="modal-footer" id="footerAnswers" style="display:none">
				<button class="btn btn-success btn-add-option" type="button"><?= $this->lang->line('add_option');?></button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close');?></button>
				<button type="submit" class="btn btn-primary"><?= $this->lang->line('save');?></button>
			</div>
		</form>
      </div>
   </div>
</div>
<div class="modal fade bd-example-modal-lg" id="modalAdd" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"><?= $this->lang->line('add');?> <?= $this->lang->line('question');?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
		 <br>
		 <form action="<?= base_url();?>lecturer/store-question" method="POST" enctype="multipart/form-data">
			<div class="form-group ml-4 mr-4">
				<label for=""> <span style="color:red">*</span> <?= $this->lang->line('question');?></label>
				<!-- <input type="text" class="form-control" required placeholder="" name="question"> -->
				<textarea name="question" class="form-control" cols="30" rows="10"></textarea>
			</div>
			<div class="form-group ml-4 mr-4">
				<a href="" data-toggle="modal" data-target="#modal_media"><img src="<?= base_url();?>assets/default/iconinfo.ico" style="width:17px;height:15px" alt=""></a>
				<label for="">Media</label>
				<input type="file" class="form-control" placeholder="" name="media">
			</div>
			<div class="form-group ml-4 mr-4">
				<label for=""><span style="color:red">*</span><?= $this->lang->line('score');?></label>
				<input type="number" class="form-control" required placeholder="" name="score">
			</div>
			<div class="form-group ml-4 mr-4">
				<label for=""><span style="color:red">*</span><?= $this->lang->line('type_');?></label>
				<select required class="form-control" id="add_tipe" required placeholder="" name="type">
				<option value="">--<?= $this->lang->line('select');?>--</option>
				<option value="option">Option (<?= $this->lang->line('select');?> 1 <?= $this->lang->line('answer');?>)</option>
				<option value="checkbox">Checkbox (<?= $this->lang->line('select_all_true_answer');?>)</option>
				<option value="boolean">True or False</option>
				</select>
			</div>
			<div class="form-group ml-4 mr-4" id="div_number_option" style="display:none" >
				<label for=""><?= $this->lang->line('number_option');?></label>
				<input type="number" class="form-control" id="add_number_option" placeholder="" value="0" name="number_option">
			</div>
			<div class="form-group ml-4 mr-4">
				<input type="hidden" class="form-control" placeholder="" value="<?= $id_exam;?>" name="id_exam">
			</div>
			<div class="form-group ml-4 mr-4">
				<span style="color:red">*</span> <?= $this->lang->line('required');?>
			</div>
			<input type="hidden" class="form-control" placeholder="" value="<?= $data_exam->tipe;?>" name="tipe_exam">
			<div class="modal-footer" id="footerQuestion" style="display:block">
				<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close');?></button>
				<button type="submit" id="btn_submit_single_question" class="btn btn-primary"><?= $this->lang->line('save');?></button>
			</div>
		</form>
      </div>
   </div>
</div>
<div class="modal fade bd-example-modal-lg" id="modalEdit" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit <?= $this->lang->line('question');?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
		 <br>
		 <form action="<?= base_url();?>lecturer/update-question" method="POST" enctype="multipart/form-data">
			<input type="hidden" class="form-control" id="edit_id_question" placeholder="" name="edit_id_question">
			<input type="hidden" class="form-control" placeholder="" value="<?= $id_exam;?>" name="edit_id_exam">
			<div class="form-group ml-4 mr-4">
				<label for=""><span style="color:red">*</span><?= $this->lang->line('question');?></label>
				<!-- <input type="text" class="form-control" id="edit_question" placeholder="" name="edit_question"> -->
				<textarea name="edit_question" id="edit_question" class="form-control" cols="30" rows="10"></textarea>
			</div>
			<div class="form-group ml-4 mr-4">
				<a href="" data-toggle="modal" data-target="#modal_media"><img src="<?= base_url();?>assets/default/iconinfo.ico" style="width:17px;height:15px" alt=""></a>
				<label for="">Media</label>
				<input type="file" class="form-control" placeholder="" name="edit_media">
			</div>
			<div class="form-group ml-4 mr-4">
				<label for=""><span style="color:red">*</span><?= $this->lang->line('score');?></label>
				<input type="number" class="form-control" id="edit_score" placeholder="" name="edit_score">
			</div>
			<div class="form-group ml-4 mr-4">
				<span style="color:red">*</span> <?= $this->lang->line('required');?>
			</div>
			<div class="modal-footer" id="footerQuestion" style="display:block">
				<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close');?></button>
				<button type="submit" class="btn btn-primary"><?= $this->lang->line('save');?></button>
			</div>
		</form>
      </div>
   </div>
</div>

<div class="modal fade" id="modal_media" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Media on Question</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>You can add media such as Image, Video or Sound on question.</p>
		<table class="table">
			<tr>
				<th>Type</th>
				<th>Supported Extension</th>
			</tr>
			<tr>
				<td>Image</td>
				<td>.jpeg .jpg .png</td>
			</tr>
			<tr>
				<td>Video</td>
				<td>.mp4 .mov</td>
			</tr>
			<tr>
				<td>Sound</td>
				<td>.mp3</td>
			</tr>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalMedia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Current Media</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="container">
			  <div class="text-center" id="div_media">

			  </div>
			  <div style="display:none" id="div_add_media">
			  	<form action="<?= base_url();?>lecturer/store-media" method="POST" enctype="multipart/form-data">
				  	<div class="form-group ml-4 mr-4">
						<a href="" data-toggle="modal" data-target="#modal_media"><img src="<?= base_url();?>assets/default/iconinfo.ico" style="width:17px;height:15px" alt=""></a>
						<input type="file" required class="form-control" placeholder="" name="inp_media">
						<input type="hidden" class="form-control" placeholder="" name="inp_id_question" id="inp_id_question">
						<input type="hidden" class="form-control" placeholder="" value="<?= $id_exam; ?>" name="inp_id_exam">
					</div>
					<div class="form-group ml-4 mr-4">
						<button type="submit" class="btn btn-success" style="width:100%">Save</button>
					</div>
				</form>
			  </div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
	$(document).ready(function(){

		$(document).on("change", ".upload_file_media_answer", function(){
			var id	= $(this).data("id")
			var tipe	= $(this).data("tipe")
			var question	= $(this).data("question")
			var fd = new FormData();
			var files = $('#upload_file_media_answer_'+id)[0].files[0];
			fd.append('file', files);
			fd.append('id', id);
			$.ajax({
				url: '<?php echo base_url();?>lecturer/upload-media-answer',
				type: 'POST',
				data: fd,
				contentType: false,
				processData: false,
				success: function(response){
					toastr.success("Media uploaded")
					showAnswers(question, tipe)
				},
				error: function(data, status, error)
				{
					var err = eval("(" + data.responseText + ")");
					toastr.error(err)
				}
			});
		});

		var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error		= '<?php echo $this->session->flashdata("error");?>'
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}
		
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		})

		$(document).on('change', '#add_tipe', function(){
			if ($(this).val() != 'boolean')
			{
				$('#div_number_option').show();
			} else {
				$('#div_number_option').hide();
			}
		})

		var statusStoreQuestion	= '<?php echo $this->session->flashdata("status_store_question");?>'
		var msgStoreQuestion	= '<?php echo $this->session->flashdata("msg_store_question");?>'
		var statusUpdateAnswer	= '<?php echo $this->session->flashdata("status_update_answer");?>'
		var msgUpdateAnswer		= '<?php echo $this->session->flashdata("msg_update_answer");?>'

		if (statusStoreQuestion)
		{
			show_toast(statusStoreQuestion, msgStoreQuestion)
		}

		if (statusUpdateAnswer)
		{
			show_toast(statusUpdateAnswer, msgUpdateAnswer)
		}

		var table = $('#dataTableQuestions').DataTable({
			"processing": true,
			"serverSide": true,
			"bStateSave": true,
			"fnStateSave": function (oSettings, oData) {
				localStorage.setItem('offersDataTables', JSON.stringify(oData));
			},
			"fnStateLoad": function (oSettings) {
				return JSON.parse(localStorage.getItem('offersDataTables'));
			},
			"order": [],
			"ajax": {
				"url" : '<?php echo $url_data;?>',
				"type": "POST",
				"async": true,
			},
			"columnDefs": [
				{
					"targets": [ 3 ],
					"orderable": false,
					"render": function(data, type, full)
					{
						return '<button data-toggle="tooltip" data-placement="top" title="answers" id="btnAnswer'+full[3]+'" type="button" data-id="'+full[3]+'" data-tipe="'+full[4]+'" class="btn btn-success btn-xs btnAnswer"><i class="fas fa-tasks"></i></button>&nbsp<button data-toggle="tooltip" data-placement="top" title="delete" id="btnDelete'+full[3]+'" type="button" data-id="'+full[3]+'" class="btn btn-danger btn-xs btnDeleteQ"><i class="fas fa-trash"></i></button>&nbsp<button data-toggle="tooltip" data-placement="top" data-id-question="'+full[3]+'" title="edit" type="button" class="btn btn-info btn-xs btnEditQuestion"><i class="fas fa-pen"></i></button>&nbsp<button data-toggle="tooltip" data-placement="top" data-id-question="'+full[3]+'" title="media" type="button" class="btn btn-warning btn-xs btnMediaQuestion"><i class="fas fa-photo-video"></i></button>';
					},
					"data": null
				},
			],
		});

		$(document).on("click", ".btnEditQuestion", function(){
			var id_question = $(this).data("id-question")
			var route		= "<?php echo base_url();?>lecturer/edit-question/"+id_question

			$.get(route, function(data, stat){
				$("#modalEdit").modal("show")
				var data = JSON.parse(data)
				$("#edit_question").val(data.pertanyaan)
				$("#edit_score").val(data.score)
				$("#edit_id_question").val(data.id)
			})
		})

		function showMediaQuestion(id_question)
		{
			$("#div_media").empty()
			var route		= "<?php echo base_url();?>lecturer/media-question/"+id_question
			$.get(route, function(data, stat){
				$("#inp_id_question").val(id_question)
				$("#modalMedia").modal("show")
				var data = JSON.parse(data)
				if (data.type == "image")
				{
					$("#div_media").append('<img style="width:50%" src="'+data.source+'"><br><br><button style="width:100%" class="btn btn-xs btn-danger btn-delete-media" data-id="'+id_question+'"><i class="fas fa-trash"></i> Delete</button>')
				} else if (data.type == "video")
				{
					$("#div_media").append('<video width="450" height="340" controls><source src="'+data.source+'" type="video/'+data.ext+'">Your browser does not support the video tag.</video><br><br><button style="width:100%" class="btn btn-xs btn-danger btn-delete-media" data-id="'+id_question+'"><i class="fas fa-trash"></i> Delete</button>')
				} else if (data.type == "sound")
				{
					$("#div_media").append('<audio controls><source src="'+data.source+'" type="audio/mp3"></audio><br><br><button style="width:100%" class="btn btn-xs btn-danger btn-delete-media" data-id="'+id_question+'"><i class="fas fa-trash"></i> Delete</button>')
				} else
				{
					$("#div_media").append('<h1>No Media</h1><br><button class="btn btn-info btn-xs btn-add-media">Add Media</button>')
				}
			})
		}

		$(document).on("click", ".btnMediaQuestion", function(){
			var id_question = $(this).data("id-question")
			showMediaQuestion(id_question)
		})

		$(document).on("click", ".btn-add-media", function(){
			$("#div_add_media").show()
		})

		$(document).on("click", ".btn-delete-media", function(){
			var id_question	= $(this).data('id')
			var id_exam		= "<?php echo $id_exam; ?>"
			var route = "<?php echo base_url();?>lecturer/delete-media/"+id_question+"/"+id_exam
			bootbox.confirm("Are you sure to delete this media?", function(result){
				if(result == true) {
					$.get(route, function(val, status){
						if (status)
						{
							toastr.success("Media updated")
							showMediaQuestion(id_question)
						} 
					}).catch(function(e){
						toastr.error("Something wrong, media failed update")
					})
				}
			})
		})

		$(document).on("click", ".btn-delete-media-answer", function(){
			var id_answer	= $(this).data("id")
			var id_question	= $(this).data("question")
			var id_exam		= "<?php echo $id_exam; ?>"
			var route = "<?php echo base_url();?>lecturer/delete-media-answer/"+id_answer+"/"+id_exam
			bootbox.confirm("Are you sure to delete this media?", function(result){
				if(result == true) {
					$.get(route, function(val, status){
						var val = JSON.parse(val)
						if (status)
						{
							toastr.success("Media updated")
							showAnswers(id_question, val.tipe)
						} 
					}).catch(function(e){
						toastr.error("Something wrong, media failed udpate")
					})
				}
			})
		})

		$(document).on("click", ".btnDeleteQ", function(){
	    var id = $(this).data('id');
	    bootbox.confirm("Apakah anda yakin akan menghapus pertanyaan ini?", function(result){
	      if(result == true) {
	        $.ajax({
              url: "<?=$url_delete;?>",
	            method: "POST",
	            data: {id:id},
	            success: function(res){
	              var hasil = $.parseJSON(res);
	              if (hasil["status"] == 200) {
	                 toastr.success(hasil["pesan"]);
	                 setTimeout(function () {
	                   return hasil["status"];
	                 }, 1500);
	                 setTimeout(function () {
	                   location.reload(true);
	                 }, 1500);
	               }else{
	                 toastr.error(hasil["pesan"]);
	                 result = false;
	                 return hasil["status"];
	               }
	            },
	            error: function (res) {
	              toastr.error("Data tidak dapat dihapus.");
	              result = false;
	              return false;
	            },
	        });
	      }
	    });
	  });

		$(document).on('click', '.btn-add-option', function(){
			$("#listAnswersAdd").empty()
			$("#listAnswersAdd").append('<form action="<?php echo base_url();?>lecturer/question/store-option" method="POST"><div class="row"><input type="hidden" name="id_question" value="'+$("#ansIdQuestion").val()+'"><input type="hidden" name="id_exam" value="'+$("#ansIdExam").val()+'"><input class="form-control" type="text" style="width:90%" name="answer"><button class="btn btn-success" type="submit">Save</button></div></form>')
		})

		function showAnswers(idQuestion, tipe)
		{
			$("#listAnswers").empty()
			$("#ansIdQuestion").val(idQuestion)
			$("#ansTipeQuestion").val(tipe)

			$("#modalAnswers").modal('show')
			$('#spinnerAnswers').show()
			$('#formAnswers').hide()
			$('#footerAnswers').hide()

			$.ajax({
				url: "<?=$url_answers;?>",
				method: "POST",
				data: {id:idQuestion},
				success: function(res){
					var hasil = $.parseJSON(res);
					if (hasil["status"] == 200) {
						$('#spinnerAnswers').hide()
						$('#formAnswers').show()
						$('#footerAnswers').show()

						if ($("#ansTipeQuestion").val() == 'checkbox' && hasil['kunjaw'] != null)
						{
							var arrKunjaw		= hasil["kunjaw"].id_answer.split(",")
							var indexNotNull	= 0

							for(i=0; i<hasil["data"].length; i++)
							{
								if (hasil["data"][i]['id_key'] != null)
								{
									indexNotNull = i
								}
							}
							for(i=1; i<hasil["data"].length; i++)
							{

								if (arrKunjaw.includes(hasil["data"][i]['id_answer']))
								{
									hasil["data"][i]['id_key'] = 	hasil["data"][indexNotNull]['id_key']
								}
							}
						}
						hasil["data"].forEach((val) =>{
							if (val.id_key !== null)
							{
								var checked		= 'checked'
								var classColor	= 'alert alert-success inp-answer'
							} else
							{
								var checked		= ''
								var classColor	= 'alert alert-danger inp-answer'
							}

							if (val.media_answer != null)
							{
								var media_answer = val.media_answer
								var ext		= media_answer.slice(media_answer.length - 3)
								var src		= "<?php echo base_url();?>uploads/media_answer/"+val.media_answer

								if (ext == "mp3")
								{
									var media = '<audio width="150px" height="150px" controls><source src="'+src+'" type="audio/mp3"></audio><br><button type="button" class="btn btn-xs btn-danger btn-delete-media-answer" data-id="'+val.id_answer+'" data-question="'+val.id+'" style="width:295px;height:35px">Delete</button>'
								} else if (ext == "mp4" || ext == "mov")
								{
									var media = '<video width="150px" height="150px" controls><source src="'+src+'" type="video/'+ext+'">Your browser does not support the video tag.</video><br><button type="button" class="btn btn-xs btn-danger btn-delete-media-answer" data-id="'+val.id_answer+'" data-question="'+val.id+'" style="width:150px;height:35px">Delete</button>'
								} else
								{
									var media = '<img style="width:150px;height:100px" src="'+src+'"><br><button type="button" class="btn btn-xs btn-danger btn-delete-media-answer" data-id="'+val.id_answer+'" data-question="'+val.id+'" style="width:150px;height:35px">Delete</button>' 
								}
							} else
							{
								// var media	= '<div class="ml-2"><form id="form_media_answer" method="POST" action="<?php echo base_url();?>lecturer/store-media-answer">'
								// media 		+= '<div class="row"><input type="file" style="width:80%" name="ans_media" class="form-control"><button class="btn btn-xs btn-primary" type="submit">Save</button></div>'
								// media		+= '</form></div>' 
								// var media = '<form>'
								var media	  	= '<button type="button" class="btn btn-warning btn-add-media-answer" data-id="'+val.id_answer+'">+ Add Media</button>'
								media	  		+= '<div style="display:none"><input type="file" class="upload_file_media_answer" name="file_media_answer" data-id="'+val.id_answer+'" data-tipe="'+val.tipe+'" data-question="'+val.id+'" id="upload_file_media_answer_'+val.id_answer+'"></div>'
								// media     += '</form>'
							}

							if ($("#ansTipeQuestion").val() == 'option')
							{
								$("#listAnswers").append('<div class="form-group"><b><input name="ans_id_answer[]" type="hidden" value="'+val.id_answer+'"><label><input name="opt" '+checked+' value="'+val.id_answer+'" required type="radio" class="optradio"> Option '+val.opt.toUpperCase()+'.&nbsp&nbsp</label></b><br><input id="answer'+val.id_answer+'" class="'+classColor+'" style="width:90%" type="text" name="ans_answer[]" value="'+val.answer+'"><button data-id="'+val.id_answer+'" type="button" class="btn-hapus-opt btn btn-danger btn-lg"><i class="fas fa-trash"></i></button><div class="ml-1" id="div_media_answer_'+val.id_answer+'">'+media+'</div><hr>')
							} else if ($("#ansTipeQuestion").val() == 'boolean')
							{
								$("#listAnswers").append('<div class="form-group"><b><input name="ans_id_answer[]" type="hidden" value="'+val.id_answer+'"><label><input name="opt" '+checked+' value="'+val.id_answer+'" required type="radio" class="optradio"> Option '+val.opt.toUpperCase()+'.&nbsp&nbsp</label></b><br><input disabled class="'+classColor+'" style="width:90%" type="text" name="ans_answer[]" value="'+val.answer+'"><input id="answer'+val.id_answer+'" class="'+classColor+'" style="width:100%" type="hidden" name="ans_answer[]" value="'+val.answer+'"><button data-id="'+val.id_answer+'" type="button" class="btn-hapus-opt btn btn-danger btn-lg"><i class="fas fa-trash"></i></button><div class="ml-1" id="div_media_answer_'+val.id_answer+'">'+media+'</div><hr>')
							} else if ($("#ansTipeQuestion").val() == 'checkbox')
							{
								$("#listAnswers").append('<div class="form-group"><b><input name="ans_id_answer[]" type="hidden" value="'+val.id_answer+'"><label><input name="opt[]" '+checked+' value="'+val.id_answer+'" type="checkbox" class="optradiocek"> Option '+val.opt.toUpperCase()+'.&nbsp&nbsp</label></b><br><input id="answer'+val.id_answer+'" class="'+classColor+'" style="width:90%" type="text" name="ans_answer[]" value="'+val.answer+'"><button data-id="'+val.id_answer+'" type="button" class="btn-hapus-opt btn btn-danger btn-lg"><i class="fas fa-trash"></i></button><div class="ml-1" id="div_media_answer_'+val.id_answer+'">'+media+'</div><hr>')
							}

						});

					}else{
						toastr.error(hasil["pesan"]);
						result = false;
						return hasil["status"];
					}
				},
				error: function (res) {
					toastr.error("Cannot get data");
					result = false;
					return false;
				},
			});
		}

		$(document).on('click', '.btn-add-media-answer', function(){
			var id_answer = $(this).data("id")
			$("#upload_file_media_answer_"+id_answer).trigger("click");
		})

		$(document).on('click', '.btnAnswer', function(){
			var idQuestion	= $(this).data('id')
			var tipe = $(this).data('tipe')
			showAnswers(idQuestion, tipe)
		})

		$(document).on('click', '.btn-hapus-opt', function(){
			var id_answer 	= $(this).data("id")
			var id_question	= $("#ansIdQuestion").val()
			var route		= '<?php echo base_url();?>lecturer/question/delete-option/'+id_answer+'/'+id_question
			$.get(route, function(val, stat){
				if (stat)
				{
					toastr.success("Answer deleted")
					location.reload(true);
				} else
				{
					toastr.error("Answer fail deleted")
				}
			})
		})
		
		$(document).on('click', '.optradio', function(){
			var idAnswer	= $(this).val()
			$('.inp-answer').removeClass("alert-success")
			$('.inp-answer').addClass("alert-danger")

			$('#answer'+idAnswer).removeClass("alert-danger")
			$('#answer'+idAnswer).addClass("alert-success")
		})

		$(document).on('click', '.optradiocek', function(){
			var idAnswer	= $(this).val()
			if($(this).is(":checked"))
			{
				$('#answer'+idAnswer).removeClass("alert-danger")
				$('#answer'+idAnswer).addClass("alert-success")
			} else
			{
				$('#answer'+idAnswer).removeClass("alert-success")
				$('#answer'+idAnswer).addClass("alert-danger")
			}
			// $('.inp-answer').removeClass("alert-success")
			// $('.inp-answer').addClass("alert-danger")

		})

	});
</script>
