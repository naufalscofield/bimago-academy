<script type="text/javascript"src="https://app.midtrans.com/snap/snap.js"data-client-key="Mid-client-NOEu7ZMgcw6aoRRF"></script>
<!-- Breadcrumbs -->
 <form id="payment-form" method="post" action="<?=site_url()?>users/users_checkout/finish">
  <input type="hidden" name="result_type" id="result-type" value=""></div>
  <input type="hidden" name="result_data" id="result-data" value=""></div>
</form>
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bread-inner">
							<ul class="bread-list">
								<li><a href="<?= base_url();?>">Home<i class="ti-arrow-right"></i></a></li>
								<li class="active"><a href="blog-single.html">Checkout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumbs -->

		<!-- Start Checkout -->
		<section class="shop checkout section">
			<div class="container">
				<form class="form" method="post" action="#">
					<div class="row">
						<div class="col-lg-7 col-12">
							<div class="checkout-form">
								<h2><?= $this->lang->line('make_checkout') ?></h2>
								<p><?= $this->lang->line('please_register') ?></p>
								<!-- Form -->
									<div class="">
										<!-- Order Widget -->
										<div class="single-widget">
											<h2><?= $this->lang->line('payments') ?></h2>
											<div class="content">
												<div class="checkbox d-none">
													<input onclick="manualPayment()" name="payment" type="radio" value="manual"> Manual Check Payment <br>
														<input onclick="otoPayment()" name="payment" type="radio" value="otomatic" checked> Otomatic Check Payment
												</div>
											</div>
										</div>
										<!--/ End Order Widget -->
										<!-- Payment Method Widget -->
										<div class="single-widget payement">
											<div class="content">
												<img src="<?= base_url();?>assets/eshop/eshop/images/payments-ready.png" alt="#">
											</div>
										</div>
										<!--/ End Payment Method Widget -->
									</div>
								<br>
								<br>
								<div class="single-widget">
									<h2><?= $this->lang->line('detail_order');?></h2>
									<div class="content">
										<br><br>
										<div class="row" style="margin-left:25px">
												<table>
													<?php foreach ($cart as $key => $value) { ?>
														<tr>
																<!-- <th width="20%" height="50px">
																	<center><img class="image" src="<?=base_url().$value['image'];?>" alt="#" style="width:65px;height:40px;"></center>
																</th> -->
																<th width="20%" height="50px">
																	<label><?=$value['name'];?></label>
																</th>
																<th width="40%" height="50px">
																	<label><?=$value['type_course'];?></label>
																</th>
																<th width="20%" height="50px">
																	<label><?=rupiah($value['price']);?></label>
																</th>
														</tr>
													<?php } ?>
												</table>
										</div>
									</div>
							</div>
								<!--/ End Form -->
							</div>
						</div>
						<div class="col-lg-5 col-12">
							<div class="order-details">
								<!-- Order Widget -->
								<div class="single-widget">
									<h2><?= $this->lang->line('cart_total') ?></h2>
									<div class="content">
										<ul>
											<li>Sub Total<span><?=$total_label;?></span></li>
											<br>
											<li class="last">Total<span><?=$total_label;?></span></li>
										</ul>
									</div>
								</div>
								<!--/ End Order Widget -->
								<!-- Button Widget -->
								<div class="single-widget get-button">
									<div class="content">
										<div class="button">
											<?php if ($this->session->userdata('token') != '') { ?>
													<a id="manual-button" url="<?=$checkout;?>" type="submit" data-style="expand-left" onclick="checkout(this);" class="btn ladda-button ladda"><?= $this->lang->line('proceed_to_checkout') ?></a>
													<a id="pay-button" class="btn ladda-button ladda"><?= $this->lang->line('proceed_to_checkout') ?></a>
											<?php }else{ ?>
												<a href="<?= base_url().'login';?>" class="btn"><?= $this->lang->line('proceed_to_checkout') ?></a>
											<?php } ?>
										</div>
									</div>
								</div>
								<!--/ End Button Widget -->
							</div>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!--/ End Checkout -->

  <script type="text/javascript">

		$(document).on("click", "#btn_submit_voucher", function(){
				var kode_voucher = $("#kode_voucher").val()

				$.ajax({
					url: '<?php echo base_url('users/checkout-voucher'); ?>',
					method: "POST",
					data: {kode_voucher : kode_voucher},
					success: function(data){
						location.reload()
					}
				});
		})

		function otoPayment(){
			var total = '<?= $total ?>';
			if (total != 0){
				console.log(total)
				$('#pay-button').show()
				$('#manual-button').hide()
			}else{
				$('#pay-button').hide()
			}
		}
		function manualPayment(){
				$('#pay-button').hide()
				$('#manual-button').show()
		}
  
    $('#pay-button').click(function (event) {
      event.preventDefault();
      $(this).attr("disabled", "disabled");
    
    $.ajax({
      url: '<?=site_url()?>users/users_checkout/token',
      cache: false,

      success: function(data) {
        //location = data;

        console.log('token = '+data);
        
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');

        function changeResult(type,data){
          $("#result-type").val(type);
          $("#result-data").val(JSON.stringify(data));
          //resultType.innerHTML = type;
          //resultData.innerHTML = JSON.stringify(data);
        }

        snap.pay(data, {
          
          onSuccess: function(result){
            changeResult('success', result);
            console.log(result.status_message);
            console.log(result);
            $("#payment-form").submit();
          },
          onPending: function(result){
            changeResult('pending', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          },
          onError: function(result){
            changeResult('error', result);
            console.log(result.status_message);
            $("#payment-form").submit();
          }
        });
      }
    });
  });
  $(document).ready(function(){
		var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	= '<?php echo $this->session->flashdata("error");?>'
		console.log(flashdata_error+'as')
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}
		otoPayment();
	})
  </script>
