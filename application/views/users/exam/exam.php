<!DOCTYPE html>
<html lang="zxx">
   <head>
      <style>
         * { box-sizing: border-box; }
         body {
         font: 16px Arial;
         }
         .autocomplete {
         /*the container must be positioned relative:*/
         position: relative;
         display: inline-block;
         }
         input {
         border: 1px solid transparent;
         background-color: #f1f1f1;
         padding: 10px;
         font-size: 16px;
         }
         input[type=text] {
         background-color: #f1f1f1;
         width: 100%;
         }
         input[type=submit] {
         background-color: DodgerBlue;
         color: #fff;
         }
         .autocomplete-items {
         position: absolute;
         border: 1px solid #d4d4d4;
         border-bottom: none;
         border-top: none;
         z-index: 99;
         /*position the autocomplete items to be the same width as the container:*/
         top: 100%;
         left: 0;
         right: 0;
         }
         .autocomplete-items div {
         padding: 10px;
         cursor: pointer;
         background-color: #fff;
         border-bottom: 1px solid #d4d4d4;
         }
         .autocomplete-items div:hover {
         /*when hovering an item:*/
         background-color: #e9e9e9;
         }
         .autocomplete-active {
         /*when navigating through the items using the arrow keys:*/
         background-color: DodgerBlue !important;
         color: #ffffff;
         }
         /*.pad-nav{
         padding-left: 100px;
         padding-right: 100px;
         }*/
         .checked {
         color: orange;
         }
         .love {
         color: red;
         }
         body {margin:0;height:2000px;}
         .icon-bar {
         position: fixed;
         top: 50%;
         -webkit-transform: translateY(-50%);
         -ms-transform: translateY(-50%);
         transform: translateY(-50%);
         z-index: 5;
         }
         .icon-bar a {
         display: block;
         text-align: center;
         padding: 16px;
         transition: all 0.3s ease;
         color: white;
         font-size: 20px;
         }
         .icon-bar a:hover {
         background-color: #000;
         }
         .mail-fb {
         background: #EA4335;
         color: white;
         }
         .telegram-fb {
         background: #0088cc;
         color: white;
         }
         .whatsapp-fb {
         background: #25D366;
         color: white;
         }
         .phone-fb {
         background: #FF8C00;
         color: white;
         }

         .sidenav {
         height: 100%;
         width: 0;
         position: fixed;
         z-index: 9999;
         top: 0;
         right: 0;
         background-color: #111;
         overflow-x: hidden;
         transition: 0.5s;
         padding-top: 60px;
         }

         .sidenav a {
         padding: 8px 8px 8px 32px;
         text-decoration: none;
         font-size: 18px;
         color: #818181;
         display: block;
         transition: 0.3s;
         }

         .sidenav a:hover {
            background-color: #818181
         /* color: #818181; */
         }

         .sidenav .closebtn {
         position: absolute;
         top: 25px;
         right: 25px;
         font-size: 36px;
         margin-left: 50px;
         }


      </style>
      <!-- Meta Tag -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name='copyright' content=''>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Title Tag  -->
      <title><?= env('APP_NAME') ?></title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="<?= getIco();?>" />
      <!-- Web Font -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
      <!-- StyleSheet -->
      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/bootstrap-v5.css">
      <!-- Magnific Popup -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/magnific-popup.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/font-awesome.css">
      <!-- Fancybox -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery.fancybox.min.css">
      <!-- Themify Icons -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/themify-icons.css">
      <!-- Jquery Ui -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery-ui.css">
      <!-- Nice Select CSS -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/niceselect.css">
      <!-- Animate CSS -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/animate.css">
      <!-- Flex Slider CSS -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/flex-slider.min.css">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/owl-carousel.css">
      <!-- Slicknav -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/slicknav.min.css">
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/custom.css">
      <!-- Eshop StyleSheet -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/reset-v5.css">
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/style-v5.css">
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/responsive-v5.css">
      <link href="<?=base_url();?>assets/select2/select2.min.css" rel="stylesheet" />
      <script src="<?= base_url();?>assets/eshop/eshop/js/jquery.min.js"></script>
      <!-- Ladda -->
      <link rel="stylesheet" href="<?=base_url();?>assets/ladda/ladda-themeless.min.css">
      <script src="<?=base_url();?>assets/ladda/spin.min.js"></script>
      <script src="<?=base_url();?>assets/ladda/ladda.min.js"></script>
   </head>
   <body class="js" oncontextmenu="return false">
      <!-- Preloader -->
      <div class="preloader">
         <div class="preloader-inner">
            <div class="preloader-icon">
               <span></span>
               <span></span>
            </div>
         </div>
      </div>
      <!-- Header -->
      <header class="header shop">
         <div class="middle-inner">
            <div class="pad-nav">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-6 col-lg-2">
                        <div class="logo">
                           <a href="<?=base_url();?>"><img id="logo_atas" src="<?=base_url();?>solmit-academy.png" alt="logo"></a>
                        </div>
                        <div class="mobile-nav"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <script src="<?=base_url();?>assets/swal/sweetalert2.min.js"></script>
      <link rel="stylesheet" href="<?=base_url();?>assets/swal/sweetalert2.min.css">
      <style>
         .underline {
         border-bottom: 2px solid currentColor;
         }
         /* .checked {
         color: orange;
         }
         .love {
         color: red;
         } */
         /* Card styles */
         .btn-blue {
         background-color: #0c60af;
         color: white;
         border-radius: 2px
         }
         .btn-red {
         background-color: red;
         color: white;
         border-radius: 2px
         }
         .btn-green {
         background-color: green;
         color: white;
         border-radius: 2px
         }
         .card{
         background-color: #fff;
         height: auto;
         width: auto;
         overflow: hidden;
         margin: 12px;
         border-radius: 5px;
         box-shadow: 9px 17px 45px -29px
         rgba(0, 0, 0, 0.44);
         }
         /* Card image loading */
         .card__image img {
         width: 100%;
         height: 200%;
         }
         .card__image.loading {
         height: 200px;
         width: 100%;
         }
         /* Card title */
         .card__title {
         padding: 8px;
         font-size: 22px;
         font-weight: 700;
         }
         .card__title.loading {
         height: 1rem;
         width: 100%;
         /* margin: 1rem; */
         border-radius: 3px;
         }
         /* Card description */
         .card__description {
         padding: 8px;
         font-size: 16px;
         }
         .card__description.loading {
         height: 3rem;
         margin: 1rem;
         border-radius: 3px;
         }
         /* The loading Class */
         .loading {
         position: relative;
         background-color: #e2e2e2;
         }
         /* .skeleton {
         margin: 0px !important;
         } */
         /* The moving element */
         .loading::after {
         display: block;
         content: "";
         position: absolute;
         width: 100%;
         height: 100%;
         transform: translateX(-100%);
         background: -webkit-gradient(linear, left top,
         right top, from(transparent),
         color-stop(rgba(255, 255, 255, 0.2)),
         to(transparent));
         background: linear-gradient(90deg, transparent,
         rgba(255, 255, 255, 0.2), transparent);
         /* Adding animation */
         animation: loading 0.8s infinite;
         }
         /* Loading Animation */
         @keyframes loading {
         100% {
         transform: translateX(100%);
         }
         }
         /* Pagination links */
         .pagination a {
         color: black;
         float: center;
         padding: 8px 16px;
         text-decoration: none;
         transition: background-color .3s;
         }
         /* Style the active/current link */
         .pagination a.active {
         background-color: dodgerblue;
         color: white;
         }
         /* Add a grey background color on mouse-over */
         .pagination a:hover:not(.active) {background-color: #ddd;}
         .nobreak {
         page-break-inside: avoid;
         }
         .vertical-center {
         margin: 0;
         position: absolute;
         top: 50%;
         -ms-transform: translateY(-50%);
         transform: translateY(-50%);
         }

      </style>
      <!-- Breadcrumbs -->
      <br>
      <div class="breadcrumbs" style="<?= ($tipe == 'exam') ? 'background-color:#c81c1d' : 'background-color:#0c60af';?>;height:50px">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="bread-inner vertical-center">
                     <ul class="bread-list" style="color: #fff">
                        <?php
                        if ($tipe == 'exam')
                        {
                        ?>
                           <h6><?= $this->lang->line('exam');?> : <i><?= $data_exam->judul;?></i></h6>
                        <?php
                        }
                        else
                        {
                        ?>
                           <h6 id="h6_atas"><?= $this->lang->line('quiz');?> : <i><?= $data_exam->judul;?> - Modul : <?= $episode->judul;?></i></h6>
                        <?php
                        }
                        ?>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <section class="product-area shop-sidebar shop section">
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-4 col-12" id="divTimer1">
                  <div class="shop-sidebar">
                     <div class="single-widget category text-center">
                        <ul class="check-box-list">
                           <div class="row" style="margin-left:25px">
                              <h6><i class="fa fa-clock-o"></i>&nbsp&nbsp&nbsp<?= $data_exam->waktu;?> <?= $this->lang->line('minutes') ?>&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp</h6><h6 style="color:black" class="timer-tick" id="timer"></h6>
                           </div>
                        </ul>
                        <br>
                        <form id="formFinishExam" action="<?= base_url();?>finish-exam" method="POST" style="display:none">
                           <input type="hidden" value="" name="finish_status" id="finish_status">
                           <input type="hidden" name="tipe_exam" value="<?= $tipe ?>">
                           <input type="hidden" value="<?= $id_exam;?>" name="finish_id_exam" id="finish_id_exam">
                           <button class="btn btn-green" type="button" id="btnSubmitExam" style="width:100%"><?= ($tipe == 'exam') ? $this->lang->line('submit_exam') : $this->lang->line('submit_quiz') ?></button>
                        </form>
                     </div>
                  </div>
               </div>
               <div class="card skeleton col-lg-6 col-md-6 col-12" style="display:block; margin:0px !important;">
                  <div class="card__image loading"></div>
                  <div class="card__title loading"></div>
                  <div class="card__description loading"></div>
               </div>
               <!-- <div class="col-lg-3 col-md-4 col-12" id="divTimer2" style="display:none">
                  <div class="shop-sidebar">
                     <div class="single-widget category" style="width:100% !important">
                        <h3 class="title">Timer</h3>
                        <ul class="check-box-list">
                           <?= $data_exam->waktu;?> <?= $this->lang->line('minutes') ?>
                           <h6 style="color:black" class="timer-tick" id="timer"></h6>
                        </ul>
                        <br>
                        <form id="formFinishExam" action="<?= base_url();?>finish-exam" method="POST">
                           <input type="hidden" value="" name="finish_status" id="finish_status">
                           <input type="hidden" name="tipe_exam" value="<?= $tipe ?>">
                           <input type="hidden" value="<?= $id_exam;?>" name="finish_id_exam" id="finish_id_exam">
                           <button class="btn btn-green" type="button" class="btnSubmitExam" style="width:100%"><?= ($tipe == 'exam') ? $this->lang->line('submit_exam') : $this->lang->line('submit_quiz') ?></button>
                        </form>
                     </div>
                  </div>
               </div> -->
               <br>
               <div class="col-lg-6 col-md-6 col-12">
                  <div class="form-group" id="questionField">
                     <i>
                        <h6 style="color:grey" id="questionNumber"></h6>
                     </i>
                     <hr id="questionHr">
                     <div id="divQuestionMedia">

                     </div><br>
                     <span id="questionTitle"><?= $this->lang->line('question') ?></span><br>
                     <label id="questionScore" for=""><?= $this->lang->line('scores') ?>()</label><br>
                     <form id="formSingleQuestion" action="<?= base_url();?>store-user-answer" method="POST">
                        <input type="hidden" value="<?=$id_exam;?>" name="id_exam" id="inputIdExam">
                        <input type="hidden" value="" name="input_tipe_exam" id="inputTipeExam">
                        <input type="hidden" value="" name="input_no_question" id="inputQuestionNumber">
                        <div id="listAnswers">
                        </div>
                        <input type="hidden" name="id_question" id="inputIdQuestion" value="">
                        <input type="hidden" name="" id="inputIdQuestionNext" value="">
                        <input type="hidden" name="" id="inputQuestionNumberNext" value="">
                        <input type="hidden" name="id_user_answer" id="inputIdUserAnswer" value="">
                        <br>
                        <div class="text-center">
                           <button id="" type="button" onclick="" style="max-width:100px;max-height:50px" class="btn btn-blue ladda-button btnPrevQuestion"><< <?= $this->lang->line('prev_question') ?></button>
                           <button id="btnSubmitAnswer" onclick="" style="max-width:100px;max-height:50px" class="btn btn-green ladda-button " data-style="expand-left">>> <?= $this->lang->line('next_question') ?></button>
                           <!-- <button id="" type="button" onclick="" style="max-width:100px;max-height:50px" class="btn btn-blue ladda-button btnNextQuestion">>> <?= $this->lang->line('next_question') ?></button> -->
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-lg-3 col-md-6 col-12" style="" id="open_questionNavigation">
                  <div class="single-widget category" style="word-wrap: break-word">
                     <h3 class="title"><?= $this->lang->line('open_question_navigation') ?></h3>
                     <div class="ro text-center">
                        <button style="width:100%" class="btn btn-blue" onclick="openNav()">☰</button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="mySidenav" class="sidenav" style="background-color:#F6F7FB;box-shadow: 0px 0px 20%;">
            <div class="row text-center">
               <h5 class="title" style="margin-left:30px;margin-top:5px"><?= $this->lang->line('question_navigation') ?></h5>
               <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            </div>
            <hr>
            <div class="single-widget category">
               <div class="row">
                  <?php
                     $no = 0;
                     for ($i=0; $i<count($questions); $i++) {
                        $no++
                        ?>
                  <div class="col-lg-4">
                     <button type="button" style="max-width:30px" class="btn btnNavQuestion" data-number="<?= $no;?>" style="color:white;width:100px" data-idQuestion="<?= $questions[$i]['id_question'] ;?>"><?= $no;?></button>
                  </div>
                  <?php
                     }
                     ?>
               </div>
            </div>
         </div>
      </section>

      <script src="<?= base_url();?>assets/eshop/eshop/js/jquery-migrate-3.0.0.js"></script>
      <script src="<?= base_url();?>assets/eshop/eshop/js/jquery-ui.min.js"></script>
      <!-- Popper JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/popper.min.js"></script>
      <!-- Bootstrap JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/bootstrap.min.js"></script>
      <!-- Color JS -->
      <!-- <script src="<?= base_url();?>assets/eshop/eshop/js/colors.js"></script> -->
      <!-- Slicknav JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/slicknav.min.js"></script>
      <!-- Owl Carousel JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/owl-carousel.js"></script>
      <!-- Magnific Popup JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/magnific-popup.js"></script>
      <!-- Waypoints JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/waypoints.min.js"></script>
      <!-- Countdown JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/finalcountdown.min.js"></script>
      <!-- Nice Select JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/nicesellect.js"></script>
      <!-- Flex Slider JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/flex-slider.js"></script>
      <!-- ScrollUp JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/scrollup.js"></script>
      <!-- Onepage Nav JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/onepage-nav.min.js"></script>
      <!-- Easing JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/easing.js"></script>
      <!-- Active JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/active.js"></script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="<?= base_url();?>assets/admin/vendor/jquery/bootbox.min.js"></script>
      <script src="<?= base_url();?>assets/eshop/eshop/js/custom.js"></script>
      
      <script>
         var l = Ladda.create( document.querySelector( '.ladda-button' ) );

         function save_checkpoint(st)
         {
            $.ajax({
               type: 'POST',
               url: '<?php echo base_url();?>exam-update-checkpoint',
               data: { 
                     id_user  : '<?php echo $id_user;?>', 
                     id_exam  : '<?php echo $id_exam;?>', 
                     minute   : $('.timer-tick').html(),
                     status   : st
                  }
            });
         }

         $(window).on('unload', function() {
           save_checkpoint('ul')
         });

         window.onbeforeunload = function() { return "Your work will be lost."; };

         setInterval(function(){
            save_checkpoint('cp')
         }, 30000)

         function openNav() {
            var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
            if (isMobile) {
               document.getElementById("mySidenav").style.width = "70%";
               $(".btnNavQuestion").attr("style", "width:20px")
            } else {
               document.getElementById("mySidenav").style.width = "20%";
                  $('.blur-background').css('display', 'block')
            }
         }

         function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
               $('.blur-background').css('display', 'none')
         }

         $(document).ready(function(){

            document.addEventListener('contextmenu', function(e) {
               e.preventDefault();
            });
            document.onkeydown = function(e) {
               if(event.keyCode == 123) {
                  return false;
               }
               if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
                  return false;
               }
               if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
                  return false;
               }
               if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
                  return false;
               }
               if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
                  return false;
               }
               if(e.altKey && e.keyCode == 'U'.charCodeAt(0)) {
                  return false;
               }
            }

            window.onbeforeunload = null;
      
            var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
            if (isMobile) {
               $("#logo_atas").attr("style", "width:60%")
               $("#h6_atas").attr("style", "font-size:12px")
            }

            function timeout()
            {
               if ('<?php echo $data_exam->aksi_waktu_habis; ?>' == 'auto_submit')
               {
                  var word = 'Your exam will be submitted!'
               } else if ('<?php echo $data_exam->aksi_waktu_habis; ?>' == 'auto_failed')
               {
                  $('#finish_status').val('timeout')
                  var word = 'You failed this exam.'
               }

               Swal.fire({
                  icon : 'warning',
                  title: 'Timeout',
                  text: word,
                  showCancelButton: false,
                  showConfirmButton: false
               })

               setTimeout(() => {  $('#formFinishExam').submit(); }, 5000);
            }

            getAnswered()
            var countDownTime =  window.sessionStorage.getItem('timerexam') || '<?php echo $data_room;?>';
            timer(countDownTime)

            function timer(sessTimer)
            {
               var timer2 = sessTimer+":01";
               var interval = setInterval(function() {
               var timer = timer2.split(':');
               var minutes = parseInt(timer[0], 10);
               var seconds = parseInt(timer[1], 10);
               --seconds;
               minutes = (seconds < 0) ? --minutes : minutes;
               sessionStorage.setItem("timerexam", $('.timer-tick').html());
               if (minutes < 1)
               {
                  $(".timer-tick").css("color","red");
               }
               if (minutes < 0 && seconds < 0)
               {
                  sessionStorage.clear()
                  clearInterval(interval);
                  timeout()
               }
               seconds = (seconds < 0) ? 59 : seconds;
               seconds = (seconds < 10) ? '0' + seconds : seconds;
               minutes = (minutes < 10) ?  minutes : minutes;
               $('.timer-tick').html(minutes + ':' + seconds);
               timer2 = minutes + ':' + seconds;
               }, 1000);
            }

            getSingleQuestion('<?php echo $questions[0]['id_question'];?>', 1)

            const queryString   = window.location.search;
            const urlParams     = new URLSearchParams(queryString);
            const keywordJudul  = urlParams.get('judul')

            $(document).on("click", ".btnNavQuestion", function(){
               var idQuestion      = $(this).data("idquestion")
               var questionNumber  = $(this).data("number")
               var idQuestionNext      = $(this).data("idquestionnext")
               var questionNumberNext  = $(this).data("numbernext")
               var idQuestionPrev      = $(this).data("idquestionprev")
               var questionNumberPrev  = $(this).data("numberprev")
               getSingleQuestion(idQuestion, questionNumber)
            })

            $(document).on("click", ".btnNextQuestion", function(){
               var idQuestion      = $(this).data("idQuestion")
               var questionNumber  = $(this).data("numberQuestion")
               getSingleQuestion(idQuestion, questionNumber)
            })

            $(document).on("click", ".btnPrevQuestion", function(){
               var idQuestion      = $(this).data("idQuestion")
               var questionNumber  = $(this).data("numberQuestion")
               getSingleQuestion(idQuestion, questionNumber)
            })

            function getSingleQuestion(idQuestion, questionNumber)
            {  
               var arrQuestion   = <?php echo json_encode($questions );?>;
               var arrNext       = [];
               var arrPrev       = [];
               
               var next       = arrQuestion.map(function(value, index, elements) {
                  arrNext.push(elements[index+1])
               });
               var prev       = arrQuestion.map(function(value, index, elements) {
                  arrPrev.push(elements[index-1])
               });

               var idx        = parseInt(questionNumber)-1
               
               if (arrPrev[idx] == undefined) {
                  var prevIq  = idQuestion
                  var prevNq  = questionNumber
               } else
               {
                  var prevIq  = arrPrev[idx].id_question
                  var prevNq  = parseInt(questionNumber) - 1
               }

               if (arrNext[idx] == undefined) {
                  var nextIq  = idQuestion
                  var nextNq  = questionNumber
               } else
               {
                  var nextIq  = arrNext[idx].id_question
                  var nextNq  = parseInt(questionNumber) + 1
               }

               
               $(".btnNextQuestion").data("idQuestion", nextIq)
               $(".btnNextQuestion").data("numberQuestion", nextNq)

               $(".btnPrevQuestion").data("idQuestion", prevIq)
               $(".btnPrevQuestion").data("numberQuestion", prevNq)

               $("#questionNumber").hide()
               $("#questionHr").hide()
               $('.btnNavQuestion').removeClass("btn-blue")
               $('.btnNavQuestion[data-idQuestion='+idQuestion+']').addClass("btn btn-blue")

               $("#questionTitle").text('')
               $("#divQuestionMedia").hide()
               $("#questionScore").text('')
               $("#listAnswers").empty()
               $("#btnSubmitAnswer").hide()
               $(".btnNextQuestion").hide()
               $(".btnPrevQuestion").hide()
               $('.skeleton').show()
               $('#questionNavigation').hide()
               $('#open_questionNavigation').hide()
               $.ajax({
                  url: "<?=$url_single_question;?>",
                  method: "POST",
                  data: {id: idQuestion},
                  success: function(data){
                     $("#questionNumber").show()
                     var res = JSON.parse(data)
                     $("#questionHr").show()
                     $('.skeleton').hide()
                     $('#questionNavigation').show()
                     $('#open_questionNavigation').show()
                     $(".btnNextQuestion").show()
                     $(".btnPrevQuestion").show()
                     $("#questionNumber").text('Question Number '+questionNumber+' of <?php echo $total_question;?>')
                     $("#questionTitle").text(res['data']['question'].pertanyaan)
                     if (res['data']['question'].media != undefined)
                     {
                        $("#divQuestionMedia").show()

                        var media   = res['data']['question'].media
                        var ext     = media.slice(media.length - 3)

                        const video = ["mp4", "mov"]
                        const audio = ["mp3"]
                        var media   = "<?php echo base_url();?>uploads/media_question/"+res['data']['question'].media

                        if (video.includes(ext))
                        {
            					$("#divQuestionMedia").append('<video width="450" height="340" controls><source src="'+media+'" type="video/'+ext+'">Your browser does not support the video tag.</video>')
                        } else if (audio.includes(ext))
                        {
                           $("#divQuestionMedia").append('<audio controls><source src="'+media+'" type="audio/mp3"></audio>')
                        } else
                        {
                           $("#divQuestionMedia").append('<img src="'+media+'" style="width:300px;height:200px">')
                        }
                     }
                     $("#questionScore").text("Score("+res['data']['question'].score+")")
                     $("#questionScore").attr("style", "color:red")
                     $("#questionScore").append("<hr>")
                     if (res["data"]["question"].tipe == 'checkbox')
                     {
                        var arrKunjaw		= res["data"]["kunjaw"].id_answer.split(",")
                        var arrUAnswer		= res["data"]["userAnswer"].id_answer.split(",")
                        var indexNotNull	= 0

                        for(i=0; i<res["data"]["answers"].length; i++)
                        {
                           if (res["data"]["answers"][i]['id_key'] != null)
                           {
                              indexNotNull = i
                           }
                        }

                        for(i=1; i<res["data"]["answers"].length; i++)
                        {

                           if (arrKunjaw.includes(res["data"]["answers"][i]['id_answer']))
                           {
                              res["data"]["answers"][i]['id_key'] = 	res["data"]["answers"][indexNotNull]['id_key']
                           }
                        }

                        res["data"]["answers"].forEach((aw) => {

                           if (aw.media_answer != undefined)
                           {
                              var media   = aw.media_answer
                              var ext     = media.slice(media.length - 3)

                              const video = ["mp4", "mov"]
                              const audio = ["mp3"]
                              var media   = "<?php echo base_url();?>uploads/media_answer/"+aw.media_answer

                              if (video.includes(ext))
                              {
                                 var appMedia = '<video width="250" height="140" controls><source src="'+media+'" type="video/'+ext+'">Your browser does not support the video tag.</video>'
                              } else if (audio.includes(ext))
                              {
                                 var appMedia = '<audio controls><source src="'+media+'" type="audio/mp3"></audio>'
                              } else
                              {
                                 var appMedia = '<img src="'+media+'" style="width:300px;height:200px">'
                              }
                           } else
                           {
                              var appMedia = ''
                           }

                           $("#listAnswers").append('<table><tbody><tr><td style="width:5px"><b><label style="margin-top;5px" id="answerOpt" for="">'+aw['opt'].toUpperCase()+'. </b></td><td style="padding: 0px 10px;width:5px;"><input type="checkbox" style="margin-top:0px" class="answerValue" name="id_answer[]" id="answerValue'+aw['id_answer']+'" value="'+aw['id_answer']+'"></td><td><label for="" id="answerText">'+aw['answer']+'</label></td></tr></tbody></table>'+appMedia+'<hr>')
                        });

                        if (res["data"]["userAnswer"].id_answer !== '')
                        {
                           for(i=0; i<res["data"]["answers"].length; i++)
                           {
                              if (arrUAnswer.includes(res["data"]["answers"][i]['id_answer']))
                              {
                                 $('#answerValue'+res["data"]["answers"][i]['id_answer']).attr("checked", "checked")
                              }
                           }
                        }
                     } else
                     {
                        res["data"]["answers"].forEach((aw) => {
                           if (aw.media_answer != undefined)
                           {
                              var media   = aw.media_answer
                              var ext     = media.slice(media.length - 3)

                              const video = ["mp4", "mov"]
                              const audio = ["mp3"]
                              var media   = "<?php echo base_url();?>uploads/media_answer/"+aw.media_answer

                              if (video.includes(ext))
                              {
                                 var appMedia = '<video width="250" height="140" controls><source src="'+media+'" type="video/'+ext+'">Your browser does not support the video tag.</video>'
                              } else if (audio.includes(ext))
                              {
                                 var appMedia = '<audio controls><source src="'+media+'" type="audio/mp3"></audio>'
                              } else
                              {
                                 var appMedia = '<img src="'+media+'" style="width:300px;height:200px">'
                              }
                           } else
                           {
                              var appMedia = ''
                           }

                           $("#listAnswers").append('<table><tbody><tr><td style="width:5px"><b><label id="answerOpt" for="">'+aw['opt'].toUpperCase()+'. </label></b></td><td style="padding: 0px 10px;width:5px;"><input style="padding-top;0px" type="radio" class="answerValue" name="id_answer" id="answerValue'+aw['id_answer']+'" value="'+aw['id_answer']+'"></td><td><label for="" id="answerText">'+aw['answer']+'</label></td></tr></tbody></table>'+appMedia+'<hr>')
                        });

                        if (res["data"]["userAnswer"].id_answer !== '')
                        {
                           $('#answerValue'+res["data"]["userAnswer"].id_answer).attr("checked", "checked")
                        }
                     }



                     $("#inputIdQuestion").val(idQuestion)
                     $("#inputIdQuestionNext").val(nextIq)
                     $("#inputQuestionNumberNext").val(nextNq)
                     $("#inputTipeExam").val(res["data"]["question"].tipe)

                     $("#btnSubmitAnswer").show()
                  }
               });
            }

            $("#formSingleQuestion").submit(function (event) {
               event.preventDefault();
               l.start();
               var url = $(this).attr('action');
               var method = $(this).attr('method');

               if ($("#inputTipeExam").val() == 'checkbox')
               {
                  var formData = {
                     id_exam     : $("#inputIdExam").val(),
                     id_question : $("#inputIdQuestion").val(),
                     id_answer   : $('input[name="id_answer[]"]:checked').map(function(){return $(this).val();}).get(),
                     tipe_exam   : $("#inputTipeExam").val()
                  };
               } else
               {
                  var formData = {
                     id_exam     : $("#inputIdExam").val(),
                     id_question : $("#inputIdQuestion").val(),
                     id_answer   : $('input[name="id_answer"]:checked').val(),
                     tipe_exam   : $("#inputTipeExam").val()
                  };
               }
               $.ajax({
                  url: url,
                  method: method,
                  data: formData,
                  success: function (res) {
                     l.stop();
                     var hasil = $.parseJSON(res);
                     if (hasil["status"] == 200) {
                        if(hasil["act"] == 'insert')
                        {
                           toastr.success(hasil["msg"]);
                        } else
                        {
                           toastr.info(hasil["msg"]);
                        }

                        if (hasil['is_answer'] == 'true')
                        {
                           $('.btnNavQuestion[data-idQuestion='+$("#inputIdQuestion").val()+']').removeClass("btn btn-blue")
                           $('.btnNavQuestion[data-idQuestion='+$("#inputIdQuestion").val()+']').addClass("btn btn-green")
                        } else
                        {
                           $('.btnNavQuestion[data-idQuestion='+$("#inputIdQuestion").val()+']').removeClass("btn btn-green")
                           $('.btnNavQuestion[data-idQuestion='+$("#inputIdQuestion").val()+']').addClass("btn btn-blue")
                        }

                        if (hasil['all_answered'] == 'true')
                        {
                           toastr.success('All question is answered!')
                           $("#formFinishExam").show()
                        } else
                        {
                           $("#formFinishExam").hide()
                        }

                        if (parseInt($("#inputQuestionNumberNext").val()) <= '<?php echo $total_question; ?>')
                        {
                              var idq  = parseInt($("#inputIdQuestionNext").val())
                              var qn   = parseInt($("#inputQuestionNumberNext").val())
                              getSingleQuestion(idq, qn)
                        }

                     }else{
                        toastr.error(hasil["msg"]);
                     }
                  },
                  error: function (res) {
                     toastr.error("Server error");
                  },
               });
            });

            $("#btnSubmitExam").click(function () {
               if ('<?php echo $tipe;?>' == 'exam')
               {
                  Swal.fire({
                  title: 'Are you sure want to end and submit this exam?',
                  text: "You cannot undo the answer!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, submit'
                  }).then((result) => {
                     if (result.isConfirmed) {
                        $('#formFinishExam').submit()
                     }
                  })
               } else {
                  Swal.fire({
                  title: 'Are you sure want to end and submit this quiz?',
                  text: "You cannot undo the answer!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, submit'
                  }).then((result) => {
                     if (result.isConfirmed) {
                        $('#formFinishExam').submit()
                     }
                  })
               }
            });

            function getAnswered()
            {
               var idExam  = '<?php echo $id_exam;?>'
               var idUser  = '<?php echo $id_user;?>'
               var url     = '<?php echo $url_get_answered;?>'

               $.ajax({
                  url: url,
                  method: 'post',
                  data: {id_exam:idExam, id_user:idUser},
                  success: function (res) {
                     var hasil = $.parseJSON(res);
                     if (hasil["status"] == 200) {
                        if (hasil['all_answered'] == 'true')
                        {
                           toastr.success('All question is answered!')
                           $("#formFinishExam").show()
                        } else
                        {
                           $("#formFinishExam").hide()
                        }
                        hasil["data"].forEach((res) =>{
                           $('.btnNavQuestion[data-idQuestion='+res['id_question']+']').removeClass("btn")
                           $('.btnNavQuestion[data-idQuestion='+res['id_question']+']').addClass("btn btn-green")
                        })
                     }else{
                        toastr.error(hasil["msg"]);
                     }
                  },
                  error: function (res) {
                     toastr.error("Server error");
                  },
               });
            }

         });
      </script>
   </body>
</html>
