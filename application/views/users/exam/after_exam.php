<!DOCTYPE html>
<html lang="zxx">
   <head>
      <style>
         * { box-sizing: border-box; }
         body {
         font: 16px Arial;
         }
         .autocomplete {
         /*the container must be positioned relative:*/
         position: relative;
         display: inline-block;
         }
         input {
         border: 1px solid transparent;
         background-color: #f1f1f1;
         padding: 10px;
         font-size: 16px;
         }
         input[type=text] {
         background-color: #f1f1f1;
         width: 100%;
         }
         input[type=submit] {
         background-color: DodgerBlue;
         color: #fff;
         }
         .autocomplete-items {
         position: absolute;
         border: 1px solid #d4d4d4;
         border-bottom: none;
         border-top: none;
         z-index: 99;
         /*position the autocomplete items to be the same width as the container:*/
         top: 100%;
         left: 0;
         right: 0;
         }
         .autocomplete-items div {
         padding: 10px;
         cursor: pointer;
         background-color: #fff;
         border-bottom: 1px solid #d4d4d4;
         }
         .autocomplete-items div:hover {
         /*when hovering an item:*/
         background-color: #e9e9e9;
         }
         .autocomplete-active {
         /*when navigating through the items using the arrow keys:*/
         background-color: DodgerBlue !important;
         color: #ffffff;
         }
         /*.pad-nav{
         padding-left: 100px;
         padding-right: 100px;
         }*/
         .checked {
         color: orange;
         }
         .love {
         color: red;
         }
         body {margin:0;height:2000px;}
         .icon-bar {
         position: fixed;
         top: 50%;
         -webkit-transform: translateY(-50%);
         -ms-transform: translateY(-50%);
         transform: translateY(-50%);
         z-index: 5;
         }
         .icon-bar a {
         display: block;
         text-align: center;
         padding: 16px;
         transition: all 0.3s ease;
         color: white;
         font-size: 20px;
         }
         .icon-bar a:hover {
         background-color: #000;
         }
         .mail-fb {
         background: #EA4335;
         color: white;
         }
         .telegram-fb {
         background: #0088cc;
         color: white;
         }
         .whatsapp-fb {
         background: #25D366;
         color: white;
         }
         .phone-fb {
         background: #FF8C00;
         color: white;
         }
      </style>
      <!-- Meta Tag -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name='copyright' content=''>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Title Tag  -->
      <title><?= env('APP_NAME') ?></title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="<?= getIco();?>" />
      <!-- Web Font -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
      <!-- StyleSheet -->
      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/bootstrap-v5.css">
      <!-- Magnific Popup -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/magnific-popup.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/font-awesome.css">
      <!-- Fancybox -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery.fancybox.min.css">
      <!-- Themify Icons -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/themify-icons.css">
      <!-- Jquery Ui -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery-ui.css">
      <!-- Nice Select CSS -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/niceselect.css">
      <!-- Animate CSS -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/animate.css">
      <!-- Flex Slider CSS -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/flex-slider.min.css">
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/owl-carousel.css">
      <!-- Slicknav -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/slicknav.min.css">
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/custom.css">
      <!-- Eshop StyleSheet -->
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/reset-v5.css">
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/style-v5.css">
      <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/responsive-v5.css">
      <link href="<?=base_url();?>assets/select2/select2.min.css" rel="stylesheet" />
      <script src="<?= base_url();?>assets/eshop/eshop/js/jquery.min.js"></script>
      <!-- Ladda -->
      <link rel="stylesheet" href="<?=base_url();?>assets/ladda/ladda-themeless.min.css">
      <script src="<?=base_url();?>assets/ladda/spin.min.js"></script>
      <script src="<?=base_url();?>assets/ladda/ladda.min.js"></script>
   </head>
   <body class="js">
      <!-- Preloader -->
      <div class="preloader">
         <div class="preloader-inner">
            <div class="preloader-icon">
               <span></span>
               <span></span>
            </div>
         </div>
      </div>
      <!-- Header -->
      <header class="header shop">
         <div class="middle-inner">
            <div class="pad-nav">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-6 col-lg-2">
                        <div class="logo">
                           <a href="<?=base_url();?>"><img src="<?=base_url();?>solmit-academy.png" alt="logo"></a>
                        </div>
                        <div class="mobile-nav"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <script src="<?=base_url();?>assets/swal/sweetalert2.min.js"></script>
      <link rel="stylesheet" href="<?=base_url();?>assets/swal/sweetalert2.min.css">
      <style>
         .underline {
         border-bottom: 2px solid currentColor;
         }
         /* .checked {
         color: orange;
         }
         .love {
         color: red;
         } */
         /* Card styles */
         .btn-blue {
         background-color: #0c60af;
         color: white;
         border-radius: 2px
         }
         .btn-red {
         background-color: red;
         color: white;
         border-radius: 2px
         }
         .btn-green {
         background-color: green;
         color: white;
         border-radius: 2px
         }
         .card{
         background-color: #fff;
         height: auto;
         width: auto;
         overflow: hidden;
         margin: 12px;
         border-radius: 5px;
         box-shadow: 9px 17px 45px -29px
         rgba(0, 0, 0, 0.44);
         }
         /* Card image loading */
         .card__image img {
         width: 100%;
         height: 200%;
         }
         .card__image.loading {
         height: 200px;
         width: 100%;
         }
         /* Card title */
         .card__title {
         padding: 8px;
         font-size: 22px;
         font-weight: 700;
         }
         .card__title.loading {
         height: 1rem;
         width: 100%;
         /* margin: 1rem; */
         border-radius: 3px;
         }
         /* Card description */
         .card__description {
         padding: 8px;
         font-size: 16px;
         }
         .card__description.loading {
         height: 3rem;
         margin: 1rem;
         border-radius: 3px;
         }
         /* The loading Class */
         .loading {
         position: relative;
         background-color: #e2e2e2;
         }
         /* .skeleton {
         margin: 0px !important;
         } */
         /* The moving element */
         .loading::after {
         display: block;
         content: "";
         position: absolute;
         width: 100%;
         height: 100%;
         transform: translateX(-100%);
         background: -webkit-gradient(linear, left top,
         right top, from(transparent),
         color-stop(rgba(255, 255, 255, 0.2)),
         to(transparent));
         background: linear-gradient(90deg, transparent,
         rgba(255, 255, 255, 0.2), transparent);
         /* Adding animation */
         animation: loading 0.8s infinite;
         }
         /* Loading Animation */
         @keyframes loading {
         100% {
         transform: translateX(100%);
         }
         }
         /* Pagination links */
         .pagination a {
         color: black;
         float: center;
         padding: 8px 16px;
         text-decoration: none;
         transition: background-color .3s;
         }
         /* Style the active/current link */
         .pagination a.active {
         background-color: dodgerblue;
         color: white;
         }
         /* Add a grey background color on mouse-over */
         .pagination a:hover:not(.active) {background-color: #ddd;}
         .nobreak {
         page-break-inside: avoid;
         }
      </style>
      <!-- Breadcrumbs -->
      <div class="breadcrumbs">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="bread-inner">
                     <ul class="bread-list">
                        <h5><?= $this->lang->line('exam_result') ?></h5>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>

    <section class="product-area shop-sidebar shop section">
        <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 text-center">
               <?php if ($this->session->flashdata('status_timeout'))
               {
               ?>
                <h4><?= $this->lang->line('you_are_running') ?></h4>
                <u><i><h4 style="color:gray">-<?=$data->judul;?>-</h4></i></u><br>
                <h4 style="color:#c81c1d"><?= $this->lang->line('failed') ?></h4>
                <img src="<?= base_url();?>assets/default/timeout.png" style="width:75%" alt="">
               <?php 
               } else {
                  if ($data->status == 'passed')
                  {
               ?>
                     <h4><?= $this->lang->line('congratulations') ?></h4>
                     <h5><?= $this->lang->line('you_have_finish') ?></h5>
                     <u><i><h4 style="color:gray">-<?=$data->judul;?>-</h4></i></u><br>
                     <h5><?= $this->lang->line('with_score') ?> <?= $data->score;?> <?= $this->lang->line('from_total') ?> <?= $data->passing_grade;?></h5>
                     <h4 style="color:<?=($data->status == 'passed') ? '#0c60af' : '#c81c1d';?>"><?= strtoupper($data->status);?></h4>
               <?php
                  } else
                  {
               ?>
                     <h5><?= $this->lang->line('you_have_finish2') ?></h5>
                     <u><i><h4 style="color:gray">-<?=$data->judul;?>-</h4></i></u><br>
                     <h5><?= $this->lang->line('with_score') ?> <?= $data->score;?> <?= $this->lang->line('from_total') ?> <?= $data->passing_grade;?></h5>
                     <h4 style="color:<?=($data->status == 'passed') ? '#0c60af' : '#c81c1d';?>"><?= strtoupper($data->status);?></h4>
               <?php
                  }
               ?>
                <?php if ($data->status == 'passed') { ?>
                     <u><i><a href="<?= base_url('certificate_exam'); ?>"> <?= $this->lang->line('view_certificate') ?></a></i></u><br>
                     <img src="<?= base_url();?>assets/default/after_exam.png" style="width:50%" alt="">
                <?php } 
                else
                {
                ?>
                   <img src="<?= base_url();?>assets/default/failed.png" style="width:40%" alt="">
                <?php 
                }
               }
               ?>
               <br>
               <a href="<?= base_url();?>users/exam" class="btn btn-blue" style="color:white"><?= $this->lang->line('back_to_exam_page') ?></a>
            </div>
        </div>
        </div>
        <!-- <h1 style="font-family:Arial, Helvetica, sans-serif; font-size: 15px;">Closing window in <label id="count">15</label> seconds</h1> -->
    </section>

      <script src="<?= base_url();?>assets/eshop/eshop/js/jquery-migrate-3.0.0.js"></script>
      <script src="<?= base_url();?>assets/eshop/eshop/js/jquery-ui.min.js"></script>
      <!-- Popper JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/popper.min.js"></script>
      <!-- Bootstrap JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/bootstrap.min.js"></script>
      <!-- Color JS -->
      <!-- <script src="<?= base_url();?>assets/eshop/eshop/js/colors.js"></script> -->
      <!-- Slicknav JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/slicknav.min.js"></script>
      <!-- Owl Carousel JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/owl-carousel.js"></script>
      <!-- Magnific Popup JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/magnific-popup.js"></script>
      <!-- Waypoints JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/waypoints.min.js"></script>
      <!-- Countdown JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/finalcountdown.min.js"></script>
      <!-- Nice Select JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/nicesellect.js"></script>
      <!-- Flex Slider JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/flex-slider.js"></script>
      <!-- ScrollUp JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/scrollup.js"></script>
      <!-- Onepage Nav JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/onepage-nav.min.js"></script>
      <!-- Easing JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/easing.js"></script>
      <!-- Active JS -->
      <script src="<?= base_url();?>assets/eshop/eshop/js/active.js"></script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script src="<?= base_url();?>assets/admin/vendor/jquery/bootbox.min.js"></script>
      <script src="<?= base_url();?>assets/eshop/eshop/js/custom.js"></script>
      <!-- <script type="text/javascript">
        var milisec = 0
        var seconds = 6
        document.getElementById("count").innerHTML = '15';

        function display() {
            if (milisec <= 0) {
                milisec = 9
                seconds -= 1
            }
            if (seconds <= -1) {
                milisec = 0
                seconds += 1
            }
            else
                milisec -= 1

            document.getElementById("count").innerHTML = seconds;
            
            if (seconds == 0)
            {
                window.close()
            } else
            {
                setTimeout("display()", 100)
            }
        }
        display() 

        </script> -->
   </body>
</html>