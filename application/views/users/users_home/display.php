	<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet'>
	<style>
		#div_materi {
			padding: 0px 100px;
		}
		#div_features {
			padding: 0px 100px;
		}
		.div-gallery1 {
			padding: 20px 120px;
		}
		.div-gallery2 {
			padding: 0px 120px;
		}
		#card_video {
			height: 200px;
		}

		@media (max-width: 480px) {
			.banner_title1 {
				font-size: 20px !important;
			}
			.banner_title2 {
				font-size: 30px !important;
			}
			.banner_title3 {
				font-size: 10px !important;
			}
			#br_title {
				display: none !important;
			}
			#div_materi {
				padding: 0px;
			}
			#div_features {
				padding: 0px;
			}
			#card_video {
				height: 200px;
			}
			.div-gallery1 {
				padding: 5px 0px;
			}
			.div-gallery2 {
				padding: 0px;
			}
			
		}
		@media (max-width: 768px) {
			.banner_title1 {
				font-size: 15px !important;
			}
			.banner_title2 {
				font-size: 25px !important;
			}
			.banner_title3 {
				font-size: 10px !important;
			}
			#br_title {
				display: none !important;
			}
			#div_materi {
				padding: 0px;
			}
			#div_features {
				padding: 0px;
			}
			#card_video {
				height: 200px;
			}
			.div-gallery1 {
				padding: 5px 0px;
			}
			.div-gallery2 {
				padding: 0px;
			}
		}
	</style>
	<!-- Slider Area -->
	<section class="hero-slider">
		<!-- Single Slider -->
		<div class="single-slider">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-lg-9 col-12">
						<div class="text-inner">
							<div class="col-lg-12 col-lg-12" style="">
									<div class="hero-text">
										<div id="br_title">
											<br><br><br><br>
										</div>
										<h1 class="banner_title1" style="font-size:40px;display:inline-block">Menyiapkan</h1>
										<span class="banner_title2" style="color:#000;font-size:60px;font-family:Pacifico">Calon Santri!</span>
										<br>
										<span class="banner_title3" style="font-size:20px">Solusi belajar untuk calon santri mempersiapkan<br>diri menghadapi ujian masuk Gontor</span>
										<div class="button ml-0">
											<br>
											<a href="<?=$login_page;?>/registrasi" class="btn mb-2 ml-0" style="background-color:#D6AF40;color:black;border-radius:20px;color:#fff"><?= $this->lang->line('register_now'); ?></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Single Slider -->
	</section>
	<!--/ End Slider Area -->
	<section class="shop-services section home" style="background-color:#E0E0E0 !important">
		<div class="container">
			<!-- <div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2><?= $this->lang->line('features') ?></h2>
					</div>
				</div>
			</div> -->
			<div class="row" id="">
				<div class="col-lg-12 col-md-12 col-12 text-center">
					<?php
					$link_yt = get_link_youtube_home_page();
					if ($link_yt != NULL)
					{
					?>
                    <iframe src="<?= getYoutubeEmbedUrl($link_yt) ?>?showinfo=0&rel=0&iv_load_policy=3&fs=0&disablekb=1" width="950" height="460" frameborder="0"></iframe>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="shop-services section home">
		<div class="product-area most-popular section" style="padding-bottom:0px !important">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="section-title">
							<h2><?= $this->lang->line('material_video') ?></h2>
							<p><?=$this->lang->line('material_video2');?></p>
						</div>
					</div>
				</div>
				<div class="row" id="div_materi">
					<?php for ($i=0; $i<3; $i++): ?>
						<div class="col-lg-4 mb-3" style="">
							<div class="card" style="background-color:#E0E0E0;height:auto">
								<img class="card-img-top" src="<?=base_url().$top_viewed[$i]['image'];?>" alt="Card image cap">
								<div class="card-body">
									<!-- <div class="container"> -->
										<div class="row" style="position:relative">
											<!-- <div class="col-md-2 text-center" style="padding-top:15px"> -->
												<!-- <label style="font-size:16px;color:#DB7093;display:inline" class="" title="<?= $top_viewed[$i]['tot_wish'];?> <?= $this->lang->line('wishlist_tooltip');?>"><i class="fa fa-heart"></i></label> -->
												<!-- <label style=""><?= $top_viewed[$i]['tot_wish']; ?></label> -->
												<!-- <br> -->
												<!-- <label style="font-size:16px;color:#4682B4;display:inline" title="<?= $top_viewed[$i]['tot_com'];?> <?= $this->lang->line('comment_tooltip');?>"><i class="fa fa-comment"></i></label> -->
												<!-- <label style=""><?= $top_viewed[$i]['tot_com']; ?></label> -->
											<!-- </div>	 -->
											<div class="col-md-12" id="card_video" style="">
												<h5 style="font-size:18px" class="card-title"><?=$top_viewed[$i]['judul'];?></h5>
												<p style="font-size:10px" class="card-text"><?=substr($top_viewed[$i]['deskripsi_singkat'], 0, 130).'...';?></p>
												<a style="position: absolute;bottom: 0;color:#4682B4" href="<?= base_url();?>users/users_course/detail/<?=$top_viewed[$i]['id'];?>">Yuk Belajar  <i class="fa fa-angle-double-right"></i></a>
											</div>	
										</div>
									<!-- </div> -->
								</div>
							</div>
						</div>
					<?php endfor ?>
				</div>
			</div>
			<div class="product-area most-popular section" style="padding:0px !important">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="section-title">
								<a style="border-radius:20px;background-color:#D6AF40;color:#fff;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px;margin-bottom:5px" href="<?=base_url();?>users/users_course"><?= $this->lang->line('find_out_more');?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="shop-services section home" style="background-color:#E0E0E0 !important">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2><?= $this->lang->line('features') ?></h2>
					</div>
				</div>
			</div>
			<div class="row" id="div_features">
				<div class="col-lg-3 col-md-6 col-12 text-center">
					<!-- Start Single Service -->
					<img src="<?= base_url();?>assets/default/f1.png" alt="">
					<div class="single-service p-0 mt-3">
						<h4><?=$this->lang->line('interactive_videos');?></h4>
						<br>
						<p><?=$this->lang->line('interactive_videos2');?></p>
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-3 col-md-6 col-12 text-center">
					<!-- Start Single Service -->
					<img src="<?= base_url();?>assets/default/f2.png" alt="">
					<div class="single-service p-0 mt-3">
						<h4><?=$this->lang->line('sharing_via_zoom');?></h4>
						<br>
						<p><?=$this->lang->line('sharing_via_zoom2');?></p>
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-3 col-md-6 col-12 text-center">
					<!-- Start Single Service -->
					<img src="<?= base_url();?>assets/default/f3.png" alt="">
					<div class="single-service p-0 mt-3">
						<h4><?=$this->lang->line('1x_payment');?></h4>
						<br>
						<p><?=$this->lang->line('1x_payment2');?></p>
					</div>
					<!-- End Single Service -->
				</div>
				<div class="col-lg-3 col-md-6 col-12 text-center">
					<!-- Start Single Service -->
					<img src="<?= base_url();?>assets/default/f4.png" alt="">
					<div class="single-service p-0 mt-3">
						<h4><?=$this->lang->line('pahala_infaq');?></h4>
						<br>
						<p><?=$this->lang->line('pahala_infaq2');?></p>
					</div>
					<!-- End Single Service -->
				</div>
			</div>
		</div>
	</section>

	<div class="product-area most-popular section" style="">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title">
						<h2><?= $this->lang->line('gallery') ?></h2>
					</div>
				</div>
			</div>
			<div class="row" id="">
				<div class="text-center" style="height:auto">
					<div class="row div-gallery1">
						<div class="col-lg-12">
							<img class="card-img-top" src="<?=base_url();?>uploads/gallery/1.jpeg" alt="">
						</div>
					</div>
					<div class="row div-gallery2">
						<div class="col-lg-4">
							<img class="card-img-top" style="height:245px;width:327px" src="<?=base_url();?>uploads/gallery/bimago_santri1.png" alt="">
						</div>
						<div class="col-lg-4">
							<img class="card-img-top" style="height:245px;width:327px" src="<?=base_url();?>uploads/gallery/bimago_santri2.png" alt="">
						</div>
						<div class="col-lg-4">
							<img class="card-img-top" style="height:245px;width:327px" src="<?=base_url();?>uploads/gallery/bimago_santri3.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>