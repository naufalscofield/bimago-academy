<div class="container py-5">
  <form action="<?= base_url('users/users_sertification') ?>" method="get">
    <div class="row">
      <div class="col-12 col-md-3">
        <div class="form-group">
          <label><?= $this->lang->line('name') ?></label>
          <input type="text" id="name_certificate" class="form-control" name="name" placeholder="<?= $this->lang->line('name') ?>">
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="form-group">
          <label><?= $this->lang->line('no_sertificate') ?></label>
          <input type="text" id="no_certificate" class="form-control" name="no" placeholder="<?= $this->lang->line('no_sertificate') ?>">
        </div>
      </div>
      <div class="col-12 col-md-2 d-flex align-items-md-end">
        <div class="form-group">
          <button type="submit" id="find_sertificate" class="btn btn-primary"><?= $this->lang->line('search') ?></button>
        </div>
      </div>
    </div>
  </form>

  <div class="row">
    <div class="col-12">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col"><?= $this->lang->line('name') ?></th>
              <th scope="col"><?= $this->lang->line('no_sertificate') ?></th>
              <th scope="col"><?= $this->lang->line('course') ?></th>
            </tr>
          </thead>
          <tbody>
            <?php if (count($data) > 0) : ?>
              <?php foreach ($data as $index => $row) : ?>
                  <tr>
                    <th scope="row"><?= $index + 1 ?></th>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->certificate_number ?></td>
                    <td><?= $row->course_name ?></td>
                  </tr>
              <?php endforeach; ?>
            <?php else : ?>
              <tr>
                <td colspan="100" class="text-center"><?= $this->lang->line('no_data_try_again') ?></td>
              </tr>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>