<!DOCTYPE html>
<html lang="zxx">
<head>
<style>
	#form3 {
	width: 100%;
	display: flex;
	flex-direction: row;
	border: 1px solid grey;
	padding: 2px;
	border-color: #D6AF40;
	border-radius: 50px; you can remove this to avoid rounded corners on the form
	}

	i.fa-footer {
		display: inline-block;
		border-radius: 60px;
		box-shadow: 0px 0px 2px #888;
		border: 1px solid #000;
		padding: 0.5em 0.6em;
		background-color:#000;
	}

	#prim_footer {
		padding:0px 100px
	}
	
	@media (max-width: 480px) {
		#prim_footer {
			padding:0px
		}
		.title-footer {
			font-size:120% !important
		}
		#div_part_footer1 {
			display: none;
		}
		#div_part_footer1x {
			display: block !important;
		}
		#div_part_footer2 {
			display: none;
		}
		#div_part_footer2x {
			display: block !important;
		}
	}

	@media (max-width: 768px) {
		#prim_footer {
			padding:0px
		}
		.title-footer {
			font-size:120% !important
		}
		#div_part_footer1 {
			display: none;
		}
		#div_part_footer1x {
			display: block !important;
		}
		#div_part_footer2 {
			display: none;
		}
		#div_part_footer2x {
			display: block !important;
		}
	}

	.input3 {
	flex-grow: 2;
	border: none;
	border-radius: 50px;
	}

	.input4 {
	flex-grow: 2;
	border: none;
	border-radius: 50px;
	/* width:20px; */
	font-family: FontAwesome;
	}

	textarea:focus, input:focus{
		outline: none;
	}


	* { box-sizing: border-box; }
	body {
	font: 16px Arial;
	transition: background-color .5s;
	}

	.sidenav {
	height: 100%;
	width: 0;
	position: fixed;
	z-index: 9999;
	top: 0;
	right: 0;
	background-color: #111;
	overflow-x: hidden;
	transition: 0.5s;
	padding-top: 60px;
	}

	.sidenav a {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 18px;
	color: #818181;
	display: block;
	transition: 0.3s;
	}

	.sidenav a:hover {
		background-color: #818181
	/* color: #818181; */
	}

	.btn-green {
		background-color: green;
		color: white;
		border-radius: 2px
	}

	.sidenav .closebtn {
	position: absolute;
	top: 25px;
	right: 25px;
	font-size: 36px;
	margin-left: 50px;
	}

	#main {
	transition: margin-left .5s;
	padding: 16px;
	}

	@media screen and (max-height: 450px) {
	.sidenav {padding-top: 55px;}
	.sidenav a {font-size: 18px;}
	}

	.autocomplete {
	/*the container must be positioned relative:*/
	position: relative;
	display: inline-block;
	}
	input {
	border: 1px solid transparent;
	background-color: #f1f1f1;
	padding: 10px;
	font-size: 16px;
	}
	input[type=text] {
	background-color: #f1f1f1;
	width: 100%;
	}
	input[type=submit] {
	background-color: DodgerBlue;
	color: #fff;
	}
	.autocomplete-items {
	position: absolute;
	border: 1px solid #d4d4d4;
	border-bottom: none;
	border-top: none;
	z-index: 99;
	/*position the autocomplete items to be the same width as the container:*/
	top: 100%;
	left: 0;
	right: 0;
	}
	.autocomplete-items div {
	padding: 10px;
	cursor: pointer;
	background-color: #fff;
	border-bottom: 1px solid #d4d4d4;
	}
	.autocomplete-items div:hover {
	/*when hovering an item:*/
	background-color: #e9e9e9;
	}
	.autocomplete-active {
	/*when navigating through the items using the arrow keys:*/
	background-color: DodgerBlue !important;
	color: #ffffff;
	}

	/*.pad-nav{
		padding-left: 100px;
		padding-right: 100px;
	}*/
	.checked {
	color: orange;
    }
    .love {
      color: red;
    }
	body {margin:0;height:2000px;}

    .icon-bar {
      position: fixed;
	  right: 0px;
      bottom: -100px;
      -webkit-transform: translateY(-50%);
      -ms-transform: translateY(-50%);
      transform: translateY(-50%);
      z-index: 5;
    }

    .icon-bar a {
      display: block;
      text-align: center;
      padding: 16px;
      transition: all 0.3s ease;
      color: white;
      font-size: 20px;
    }

    .icon-bar a:hover {
      background-color: #000;
    }

    .mail-fb {
      background: #EA4335;
      color: white;
    }

    .whatsapp-fb {
      background: #25D366;
      color: white;
    }

    .phone-fb {
      background: #FF8C00;
      color: white;
    }

	.cart-mod {
		width:50%
	}

	.header-inner .nav.main-menu .fixed-menu {
		background-color: #424646;
	}

	.header.shop.sticky .header-inner .nav.main-menu .fixed-menu,
	.header-inner .nav.main-menu .fixed-menu a {
		color: #fff !important;
	}

	::-webkit-scrollbar {
		display: none;
	}

	.header-inner .nav.main-menu .scrolling-menu {
		width: 80%;
		height: 54px;
		overflow: auto;
		white-space: nowrap;
	}

	.header-inner .nav.main-menu .scrolling-menu a {
		display: inline-block;
		text-align: center;
		color: #fff;
		padding: 14px;
		text-decoration: none;
	}

	.header.shop.sticky .header-inner .nav.main-menu .scrolling-menu a {
		color: #424646;
	}

	.header.shop .nav li .dropdown li a {
		color: #424646 !important;
	}

	.logo-utama {
		height:100px;
		width:300px;
	}

	@media only screen and (max-width: 992px) {
		.logo-utama {
			height:50px;
			width:150px;
		}
	}
	@media only screen and (min-width: 992px) {
		.header-inner .nav.main-menu .fixed-menu,
		.header-inner .nav.main-menu .fixed-menu li {
			padding: 0px;
			height: 54px;
		}

		.header-inner .nav.main-menu .fixed-menu li a {
			padding: 15px !important;
		}
	}
	</style>
	<!-- Meta Tag -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php if (isset($dinamic_meta) && $dinamic_meta == 'on'): ?>
		<meta property="og:url" content="<?=base_url().'users/users_course/detail/'.encode_url_share_id($data['data'][0]['id']);?>" />
		<meta property="og:title" content="<?= env('APP_NAME') ?> |  <?= $data['data'][0]['judul'];?>" />
		<meta property="og:description" content="<?= env('APP_NAME') ?>, IT Solution, Consultan, Solution of Management and Information Technology" />
		<meta property="og:site_name" content="<?= env('APP_NAME') ?>" />
		<meta property="og:image" content="<?= base_url($data['data'][0]['image']);?>" />
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:type" content="article" />
		<meta property="article:author" content="" />
		<meta property="article:publisher" content="" />
		<meta property="article:published_time" content="<?= $data['data'][0]['created_at'];?>" />
	<?php else: ?>
		<meta name="image" itemprop="image" property="og:image" content="<?= base_url('assets/share.png');  ?>" />
	<?php endif ?>

	<!-- Title Tag  -->
    <title><?= env('APP_NAME') ?></title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?= getIco(); ?>" />
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

	<!-- StyleSheet -->
	<link rel="stylesheet" href="<?=base_url();?>assets/swal/sweetalert2.min.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/bootstrap-v5.css">
	<!-- Magnific Popup -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/magnific-popup.min.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/font-awesome.css">
	<!-- Fancybox -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery.fancybox.min.css">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/themify-icons.css">
	<!-- Jquery Ui -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery-ui.css">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/niceselect.css">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/animate.css">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/flex-slider.min.css">
	<!-- Owl Carousel -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/owl-carousel.css">
	<!-- Slicknav -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/slicknav.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/custom.css">

	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/reset-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/style-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/responsive-v5.css">
	<link href="<?=base_url();?>assets/select2/select2.min.css" rel="stylesheet" />

	<script src="<?= base_url();?>assets/eshop/eshop/js/jquery.min.js"></script>

	<!-- Ladda -->
	<link rel="stylesheet" href="<?=base_url();?>assets/ladda/ladda-themeless.min.css">
	<script src="<?=base_url();?>assets/ladda/spin.min.js"></script>
	<script src="<?=base_url();?>assets/ladda/ladda.min.js"></script>
	<script src="<?=base_url();?>assets/swal/sweetalert2.min.js"></script>

</head>
<body class="js">
	<div class="blur-background" style="background-color: rgba(0,0,0,0.4); position: fixed; top: 0px; right: 0px; left: 0px; bottom: 0px; z-index: 9999; display: none"></div>

	<div id="mySidenav" class="sidenav" style="background-color:white;box-shadow: 0px 0px 10px">
  	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	<?php if (!$this->session->userdata('id')) { ?>
		<a href="<?=$login_page;?>" style="color:#0c60af;"><?= $this->lang->line('login');?></a>
		<a href="<?=$register_page;?>"style="color:#0c60af;border-bottom: 2px solid #bbbbbb;padding-bottom: 10px;margin-bottom: 10px;">Sign Up</a>
	<?php } ?>
	<a href="<?=base_url();?>" style="margin-bottom: 10px"><?= $this->lang->line('home'); ?></a>
	<a href="<?=base_url().'users/users_course';?>"><?= $this->lang->line('courses'); ?></a>
	<a href="<?=base_url().'users/users_course?type=4';?>">Webinar</a>
	<a href="https://santri.bimago.academy">Santri</a>
	<a href="<?=$contact;?>" style="margin-bottom: 10px"><?= $this->lang->line('about_us')  ?></a>
	<a target="_blank" href="<?= base_url();?>assets/user_manual/user_manual_student.pdf"><?= $this->lang->line('um_student') ?></a>

	<?php if ($this->session->userdata('id')) { ?>
		<?php
		if ($this->session->userdata('login_via') == 'solmit') {
			if ($user->avatar == null || $user->avatar == '') {
				if ($user->jk == 'W') {
					$image = '<img class="profil" src="'.base_url().'assets/default/female_avatar_s0lm1t.png" alt="" style="width:25px;height:25px;border-radius: 50%;">';
				}else{
					$image = '<img class="profil" src="'.base_url().'assets/default/male_avatar_s0lm1t.png" alt="" style="width:25px;height:25px;border-radius: 50%;">';
				}
			}else{
				$image = '<img class="profil" src="'.base_url().'assets/avatar/'.$user->avatar.'" alt="" style="width:25px;height:25px;border-radius: 50%;">';
			}
		}else{
			$image = '<img class="profil" src="'.$user->avatar.'" alt="" style="width:25px;height:25px;border-radius: 50%;">';
		} ?>
		<a style="cursor:pointer;" data-toggle="collapse" data-target="#demo">Hello <?= $user->nama_depan; ?> <?=$image;?> <i class="ti-angle-down"></i></a>
		<div id="demo" class="collapse">
			<ul class="dropdown" style="left: auto; right: 0px;">
				<a href="<?=$mycourse;?>"><i class="ti-ruler-pencil"></i> <?= $this->lang->line('my_course') ?></a>
				<a href="<?=base_url();?>users/users_mycourse/index/exam"><i class="fa fa-newspaper-o"></i> <?= $this->lang->line('take_an_exam') ?></a>
				<a href="<?=base_url();?>users/users_mycourse/index/wishlist"><i class="ti-heart" aria-hidden="true" alt="Wishlist"></i> Whislist</a>
				<a href="<?=base_url();?>users/users_mycourse/index/history"><i class="ti-wallet" aria-hidden="true"></i> Invoice</a>
				<a href="<?=base_url();?>users/users_mycourse/index/certificate"><i class="ti-medall" aria-hidden="true"></i> Certificate</a>
				<a href="<?=$my_account;?>"><i class="ti-user"></i> <?= $this->lang->line('my_account') ?></a>
				<a href="<?= base_url();?>users/logout" url=""><i class="ti-share-alt"></i> Logout</a>
			</ul>
		</div>
	<?php } ?>
</div>

	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>

	<!-- End Preloader -->
		<!-- <div class="icon-bar">
      <a target="_blank" href="https://wa.me/6282214316638" class="whatsapp-fb"><i class="fa fa-whatsapp"></i></a>
      <a target="_blank" href="mailto:solmitacademy@gmail.com" class="mail-fb"><i class="fa fa-envelope"></i></a>
      <a target="_blank" href="tel:+62227237667" class="phone-fb"><i class="fa fa-phone"></i></a>
      <a target="_blank" href="https://telegram.me/<?= env('APP_NAME') ?>" class="telegram-fb"><i class="fa fa-telegram"></i></a>
    </div> -->

    <style>
    	* {
  box-sizing: border-box;
}

.fab-wrapper {
  position: fixed;
  bottom: 5rem;
  right: 3rem;
  z-index: 9999;
}
.fab-checkbox {
  display: none;
}
.fab {
  position: absolute;
  bottom: -1rem;
  right: -1rem;
  width: 4rem;
  height: 4rem;
  text-align: center;
  background: blue;
  border-radius: 50%;
  background: #126ee2;
  box-shadow: 0px 5px 20px #81a4f1;
  transition: all 0.3s ease;
  z-index: 1;
  border-bottom-right-radius: 6px;
  border: 1px solid #0c50a7;
}

.fab i {
	position: absolute;
	top: 30%;
	left:  32%;
	color: white;
	font-size: 25px;
	display: inline-block;
}

.fab:before {
  content: "";
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  border-radius: 50%;
  background-color: rgba(255, 255, 255, 0.1);
}
.fab-checkbox:checked ~ .fab:before {
  width: 90%;
  height: 90%;
  left: 5%;
  top: 5%;
  background-color: rgba(255, 255, 255, 0.2);
}
.fab:hover {
  background: #2c87e8;
  box-shadow: 0px 5px 20px 5px #81a4f1;
}

.fab-checkbox:checked ~ .fab .fab-dots {
  height: 6px;
}

.fab .fab-dots-2 {
  transform: translateX(-50%) translateY(-50%) rotate(0deg);
}

.fab-checkbox:checked ~ .fab .fab-dots-1 {
  width: 32px;
  border-radius: 10px;
  left: 50%;
  transform: translateX(-50%) translateY(-50%) rotate(45deg);
}
.fab-checkbox:checked ~ .fab .fab-dots-3 {
  width: 32px;
  border-radius: 10px;
  right: 50%;
  transform: translateX(50%) translateY(-50%) rotate(-45deg);
}

@keyframes blink {
  50% {
    opacity: 0.25;
  }
}

.fab-checkbox:checked ~ .fab .fab-dots {
  animation: none;
}

.fab-wheel {
  position: absolute;
  bottom: 0;
  right: 0;
  border: 1px solid #;
  width: 10rem;
  height: 10rem;
  transition: all 0.3s ease;
  transform-origin: bottom right;
  transform: scale(0);
}

.fab-checkbox:checked ~ .fab-wheel {
  transform: scale(1);
}

.fab-action {
	color: white;
  position: absolute;
  background: white;
  width: 3rem;
  height: 3rem;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: White;
  box-shadow: 0 0.1rem 1rem rgba(24, 66, 154, 0.82);
  transition: all 1s ease;
  opacity: 0;
}

.fab-checkbox:checked ~ .fab-wheel .fab-action {
  opacity: 1;
}

.fab-action:hover {
  background-color: #f16100;
}

.fab-wheel .fab-action-1 {
  right: -1rem;
  top: 2.4rem;
}

.fab-wheel .fab-action-2 {
  right: 3.4rem;
  top: 4.2rem;
}
.fab-wheel .fab-action-3 {
  left: 3.1rem;
  bottom: -1.6rem;
}
.fab-wheel .fab-action-4 {
  left: 0;
  bottom: -1rem;
}

.slicknav_nav {
	overflow: scroll;
}

.slicknav_parent ul {
	background-color: #fff;
	left: -5px !important;
	right: auto;
}

.search-responsive {
	transform: scale(0);
	position: fixed;
	top: 0px;
	left: 0px;
	right: 0px;
	z-index: 999;
	background: white;
	padding: 5px 10px;
	transition: 500ms;
}

.search-responsive.show {
	transform: scale(1);
	top: 83px;
}
    </style>

<div class="fab-wrapper">
  <input id="fabCheckbox" type="checkbox" class="fab-checkbox" />
  <label class="fab" for="fabCheckbox">
    <i class="fa fa-bars"></i>
  </label>

  <div class="fab-wheel">
     <a target="_blank" href="https://wa.me/6281220487074" class="fab-action fab-action-1 text-dark"><i class="fa fa-whatsapp"></i></a>
     <a target="_blank" href="tel:+6281220487074" class="fab-action fab-action-2 text-dark"><i class="fa fa-phone"></i></a>
     <a target="_blank" href="mailto:info@Solmit.academy" class="fab-action fab-action-3 text-dark"><i class="fa fa-envelope"></i></a>
  </div>
</div>

<div class="search-responsive">
	<form action="<?=$search;?>" method="GET">
		<div class="input-group mb-2">
			<input id="searchBox" type="search" class="form-control input-block" name="judul" placeholder="<?= $this->lang->line('search_course_here'); ?>">
			<div class="input-group-append">
				<button class="btn btn-primary input-group-text" id="btnSearch" type="button">Search</button>
			</div>
		</div>
	</form>
	<div class="clear"></div>
</div>
	<!-- </div> -->

		<!-- Header -->
		<header class="header shop">
			<div class="middle-inner">
				<div class="row" style="padding-top:20px">
					<div class="col-lg-4 col-md-2 col-12" style="">
						<!-- Logo -->
						<!-- <div class="logo logo-utama" style="height: 80px;line-height: 70px;text-align: center;border: 2px #f69c55;padding-top:5px"> -->
						<div class="logo" style="height: 80px;line-height: 70px;text-align: center;border: 2px #f69c55;padding-top:5px">
							<a href="<?=base_url();?>"><img class="mq logo-utama" style="margin-left:25px" src="<?= base_url();?>uploads/lms_config/<?= $info->logo_utama;?>" alt="logo"></a>
						</div>
						<!--/ End Logo -->

						<!-- Search Form -->
						<div class="search-top" style="margin-right:25px">

							<a style="font-size:30px;cursor:pointer;color:#0c60af;" onclick="openNav()" class="float-right ml-3">☰</a>
							<a href="javascript:;" class="btn-search-responsive float-right ml-3"><i class="ti-search"></i></a>
							<a href="<?=$cart_page;?>" class="mb-2 float-right">
								<i class="fa fa-shopping-cart"></i> 
								<!-- <span class="badge total-count" style="background-color:#0c60af;color:white" id="total-count-2">0</span> -->
							</a>

							<!-- Search Form -->
							<form class="search-form" action="<?=$search;?>" method="GET" autocomplete="off">
								<input name="judul" placeholder="<?= $this->lang->line('search_course_here'); ?>" id="searchBox" type="search">
								<button class="btnn" id="btnSearch" type="button"><i class="ti-search"></i></button>
							</form>
							<!--/ End Search Form -->
						</div>

						<!--/ End Search Form -->
						<!-- <div class="mobile-nav"></div> -->
					</div>
					<div class="col-8 col-md-8 col-12">
						<div class="row">
							<div class="col-lg-7 col-md-7 col-12 my-auto">
								<div class="search-bar-top">
									<div class="search-bar">
										<form action="<?=$search;?>" method="GET" autocomplete="off" id="form3">
											<input name="judul" placeholder="<?= $this->lang->line('search_course_here'); ?>" class="input3" type="search" id="searchBox2"/>
											<input class="input4" id="btnSearch2" type="button" style="width:30px !important;color:#D6AF40;font-size:15px" value="&#xf002;" />
										</form>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-3 col-12 my-auto">
								<div class="right-bar">
									<div class="sinlge-bar shopping" url="<?=base_url();?>users/users_cart/get_modal" onclick="shopping(this);">
										<a href="#" class="single-icon">
											<div class="row">
												<i style="font-size:30px"class="fa fa-shopping-cart"></i> &nbsp
												<!-- <span class="total-count" id="total-count">0</span> -->
												<label style="font-size:15px;color:grey"><?= $this->lang->line('my_cart'); ?></label>
											</div>
										</a>
										<div id="list-cart"></div>
									</div>
									<div class="sinlge-bar">
										<?php $active = !empty($this->session->userdata('site_lang')) ? $this->session->userdata('site_lang'):''; ?>
										<form id="formLang" name="" method="post" action="<?= base_url('changelang');?>">
											<div class="btn-group btn-group-toggle btn-sm" data-toggle="buttons">
												<label class="btn btn-secondary btn-sm p-1 <?= $active == 'english' || empty($active)  ? 'active':''; ?>">
													<input  value="english" type="radio" name="lang" id="option1"> <img src="<?=base_url().'assets/default/eng-flag.jpg';?>" style="width:30px;height:19px" alt="">
												</label>
												<label class="btn btn-secondary btn-sm p-1 <?= $active == 'indo' ? 'active':''; ?>">
													<input value="indo" type="radio" name="lang" id="option2"> <img src="<?=base_url().'assets/default/ind-flag.png';?>" style="width:30px;height:19px" alt="">
												</label>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="header-inner">
								<div class="container">
									<div class="cat-nav-head">
										<div class="row">
											<div class="col-lg-12 col-12">
												<div class="menu-area">
													<!-- Main Menu -->
													<nav class="navbar navbar-expand-lg">
														<div class="navbar-collapse">
															<div class="nav-inner" style="width: 800%">
																<div class="nav main-menu menu navbar-nav" style="width: 100%">
																	<li><a style="background-color:#fff;color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url();?>"><?= $this->lang->line('home'); ?></a></li>
																	<li><a style="background-color:#fff;color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url().'users/users_course';?>"><?= $this->lang->line('courses'); ?></a></li>
																	<li><a style="background-color:#fff;color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url().'users/users_course?type=4';?>">Webinar</a></li>
																	<li><a style="background-color:#fff;color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="https://santri.bimago.academy">Santri</a></li>
																	<li><a style="background-color:#fff;color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="https://santri.bimago.academy"><?= $this->lang->line('about_us');?></a></li>
																	<li><a style="background-color:#fff;color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="#">User Manual <i class="ti-angle-down"></i></a>
																		<ul class="dropdown" style="left: auto; right: 0px;">
																			<li><a target="_blank" href="<?= base_url();?>assets/user_manual/user_manual_student.pdf"><?= $this->lang->line('um_student') ?></a></li>
																		</ul>
																	</li>
																	<?php if ($this->session->userdata('id')) { ?>
																		<?php
																		if ($this->session->userdata('login_via') == 'solmit') {
																			if ($user->avatar == null || $user->avatar == '') {
																				if ($user->jk == 'W') {
																					$image = '<img class="profil" src="'.base_url().'assets/default/female_avatar_s0lm1t.png" alt="" style="width:28px;height:28px;border-radius: 50%;">';
																				}else{
																					$image = '<img class="profil" src="'.base_url().'assets/default/male_avatar_s0lm1t.png" alt="" style="width:28px;height:28px;border-radius: 50%;">';
																				}
																			}else{
																				$image = '<img class="profil" src="'.base_url().'assets/avatar/'.$user->avatar.'" alt="" style="width:28px;height:28px;border-radius: 50%;">';
																			}
																		}else{
																			$image = '<img class="profil" src="'.$user->avatar.'" alt="" style="width:28px;height:28px;border-radius: 50%;">';
																		} ?>
																		<li><a href="#">Hello <?= $user->nama_depan; ?> <?=$image;?> <i class="ti-angle-down"></i></a>
																			<ul class="dropdown" style="left: auto; right: 0px;">
																				<li><a href="<?=$mycourse;?>"><?= $this->lang->line('my_course') ?><i class="ti-ruler-pencil"></i></a></li>
																				<li><a href="<?=base_url();?>users/users_mycourse/index/exam"><?= $this->lang->line('take_an_exam') ?> <i class="fa fa-newspaper-o"></i></a></li>
																				<li><a href="<?=base_url();?>users/users_mycourse/index/wishlist">Wishlist <i class="ti-heart" aria-hidden="true" alt="Wishlist"></i></a></li>
																				<li><a href="<?=base_url();?>users/users_mycourse/interest"><?= $this->lang->line('interest'); ?> <i class="ti-heart" aria-hidden="true" alt="Interest"></i></a></li>
																				<li><a href="<?=base_url();?>users/users_mycourse/index/history"><?= $this->lang->line('invoice'); ?> <i class="ti-wallet" aria-hidden="true"></i></a></li>
																				<li><a href="<?=base_url();?>users/users_mycourse/index/certificate"><?= $this->lang->line('certificate'); ?> <i class="ti-medall" aria-hidden="true"></i></a></li>
																				<li><a href="<?=$my_account;?>"><?= $this->lang->line('my_account') ?> <i class="ti-user"></i></a></li>
																				<li><a href="<?= base_url();?>users/logout" url=""><?= $this->lang->line('logout'); ?> <i class="ti-share-alt"></i></a></li>
																			</ul>
																		</li>
																	<?php }else{ ?>
																		<li>
																			<a style="border-radius:20px;border:1px solid #D6AF40;background-color:#fff;color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px;margin-bottom:5px" href="<?=$login_page;?>"><?= $this->lang->line('login');?></a>
																		</li>
																		<li>
																			<a style="border-radius:20px;background-color:#D6AF40;color:#fff;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px;margin-bottom:5px" href="<?=$login_page;?>/registrasi"><?= $this->lang->line('register');?></a>
																		</li>
																	<?php } ?>
																</div>
															</div>
														</div>
													</nav>
													<!--/ End Main Menu -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		

<script type="text/javascript">
$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if (scroll > 0 && scroll < 210) {
			$('.search-responsive').removeClass('show');
		}
});

	$(function() {
		$('.btn-search-responsive').click(function() {
			if ($('.search-responsive').hasClass('show')) {
				$('.search-responsive').removeClass('show');
			} else {
				$('.search-responsive').addClass('show');
			}
		});
	});
</script>

  <?= $content ;?>

	<!-- Modal -->
    <div class="modal fade" id="cartModal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-sm dialog" role="document">
        <div class="modal-content content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <div class="cart-body">
            </div>
        </div>
      </div>
    </div>
    <!-- Modal end -->

		<!-- Modal -->
	    <div class="modal fade" id="tutorModal" tabindex="-1" role="dialog">
	      <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
	            </div>
	            <div class="tutor-body">
	            </div>
	        </div>
	      </div>
	    </div>
	    <!-- Modal end -->

	<!-- Start Footer Area -->
	<footer class="footer" id="prim_footer" style="background-color:#D6AF40 !important">
		<!-- Footer Top -->
		<div class="footer-top section">
			<div class="container">
				<div class="row" id="div_part_footer1">
					<div class="col-lg-12 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
							<div class="logo">
								<h3 class="title-footer" style="color:#000">Siapkan calon santri hadapi</h3>
								<h3 class="title-footer" style="color:#000;padding-bottom:5px">ujian masuk Gontor!</h3>
								<p style="color:#000">Mari belajar bersama bimago.academy</p>
								<br>
								<a href="<?=$login_page;?>/registrasi" class="btn mb-2 ml-0" style="background-color:#fff;color:black;border-radius:20px"><?= $this->lang->line('register_now'); ?>!</a>
								<br>
								<br>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url();?>"><?= $this->lang->line('home'); ?></a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url().'users/users_course';?>"><?= $this->lang->line('courses'); ?></a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url().'users/users_course?type=4';?>">Webinar</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="https://santri.bimago.academy">Santri</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="https://santri.bimago.academy"><?= $this->lang->line('about_us');?></a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" target="_blank" href="<?= base_url();?>assets/user_manual/user_manual_student.pdf">User Manual</a>
								<div class="pull-right">
									<a target="_blank" style="color:#fff" href="<?= strip_tags($info->facebook);?>"><i class="fa-footer fa fa-twitter"></i></a>
									<a target="_blank" style="color:#fff" href="<?= strip_tags($info->facebook);?>"><i class="fa-footer ti-facebook"></i></a>
									<a target="_blank" style="color:#fff" href="<?= strip_tags($info->linkedin);?>"><i class="fa-footer fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row text-center" id="div_part_footer1x" style="display:none">
					<div class="col-lg-12 col-md-6 col-12">
						<!-- Single Widget -->
						<div class="single-footer about">
							<div class="logo">
								<h3 class="title-footer" style="color:#000">Siapkan calon santri hadapi</h3>
								<h3 class="title-footer" style="color:#000;padding-bottom:5px">ujian masuk Gontor!</h3>
								<p style="color:#000">Mari belajar bersama bimago.academy</p>
								<br>
								<a href="<?=$login_page;?>/registrasi" class="btn mb-2 ml-0" style="background-color:#fff;color:black;border-radius:20px"><?= $this->lang->line('register_now'); ?>!</a>
								<br>
								<br>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url();?>"><?= $this->lang->line('home'); ?></a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url().'users/users_course';?>"><?= $this->lang->line('courses'); ?></a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="<?=base_url().'users/users_course?type=4';?>">Webinar</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="https://santri.bimago.academy">Santri</a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" href="https://santri.bimago.academy"><?= $this->lang->line('about_us');?></a>
								<a style="color:#000;padding-top:5px;padding-bottom:5px;padding-left:15px;padding-right:15px;margin-top:15px" target="_blank" href="<?= base_url();?>assets/user_manual/user_manual_student.pdf">User Manual</a>
								<br>
								<br>
								<div class="right">
									<a target="_blank" style="color:#fff" href="<?= strip_tags($info->facebook);?>"><i class="fa-footer fa fa-twitter"></i></a>
									<a target="_blank" style="color:#fff" href="<?= strip_tags($info->facebook);?>"><i class="fa-footer ti-facebook"></i></a>
									<a target="_blank" style="color:#fff" href="<?= strip_tags($info->linkedin);?>"><i class="fa-footer fa fa-instagram"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer Top -->
		<div class="copyright">
			<div class="container">
				<div class="inner">
					<div class="row" id="div_part_footer2">
						<div class="col-lg-2">
							<div class="left">
								<img src="<?= base_url();?>assets/default/bimago_logo_black.png" alt="">
								<!-- <p>Copyright © <a href="http://www.solmit.com" target="_blank"><img style="width:20px;height:20px;" src="<?=base_url().'logo-footer.png';?>" alt=""> BeeLMS</a>  -  2021</p> -->
							</div>
						</div>
						<div class="col-lg-6">
							<div class="pull-left">
								<p style="color:#000;font-size:70%">Copyright © <?= date('Y'); ?> bimago.academy. All Rights Reserved.</p>
								<p style="color:#000;font-size:70%">The use of this website requires acceptance of the Terms and Conditions and Privacy Policy.</p>
								<!-- <p>Copyright © <a href="http://www.solmit.com" target="_blank"><img style="width:20px;height:20px;" src="<?=base_url().'logo-footer.png';?>" alt=""> BeeLMS</a>  -  2021</p> -->
							</div>
						</div>
						<div class="col-lg-2">
							<div class="right">
								<b><p style="margin-bottom:1px;color:#000;font-size:70%">Need help?</p></b>
								<p style="padding-top:0px;color:#000;font-size:70%">Call our support</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="right">
								<p style="color:#000;font-size:100%"><i style="background-color:transparent !important" class="fa fa-footer fa-whatsapp"></i> 0812 2048 7074</p>
							</div>
						</div>
					</div>
					<div class="row nowrap" id="div_part_footer2x" style="display:none">
						<div class="col-lg-2">
							<div class="left">
								<img src="<?= base_url();?>assets/default/bimago_logo_black.png" alt="">
								<!-- <p>Copyright © <a href="http://www.solmit.com" target="_blank"><img style="width:20px;height:20px;" src="<?=base_url().'logo-footer.png';?>" alt=""> BeeLMS</a>  -  2021</p> -->
							</div>
						</div>
						<br>
						<div class="col-lg-6">
							<div class="pull-left text-center">
								<p style="margin-bottom:-10px;color:#000;font-size:50%">Copyright © <?= date('Y'); ?> bimago.academy. All Rights Reserved.</p>
								<p style="color:#000;font-size:50%">The use of this website requires acceptance of the Terms and Conditions and Privacy Policy.</p>
							</div>
						</div>
						<br>
						<br>
						<div class="col-lg-2">
							<div class="right">
								<b><p style="margin-bottom:-10px;color:#000;font-size:60%">Need help?</p></b>
								<p style="padding-top:0px;color:#000;font-size:60%">Call our support</p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="right">
								<p style="color:#000;font-size:100%"><i style="background-color:transparent !important" class="fa fa-footer fa-whatsapp"></i> 0812 2048 7074</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- /End Footer Area -->

	<!-- Jquery -->
  <script src="<?= base_url();?>assets/eshop/eshop/js/jquery-migrate-3.0.0.js"></script>
	<script src="<?= base_url();?>assets/eshop/eshop/js/jquery-ui.min.js"></script>
	<!-- Popper JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/popper.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/bootstrap.min.js"></script>
	<!-- Color JS -->
	<!-- <script src="<?= base_url();?>assets/eshop/eshop/js/colors.js"></script> -->
	<!-- Slicknav JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/slicknav.min.js"></script>
	<!-- Owl Carousel JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/owl-carousel.js"></script>
	<!-- Magnific Popup JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/magnific-popup.js"></script>
	<!-- Waypoints JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/waypoints.min.js"></script>
	<!-- Countdown JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/finalcountdown.min.js"></script>
	<!-- Nice Select JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/nicesellect.js"></script>
	<!-- Flex Slider JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/flex-slider.js"></script>
	<!-- ScrollUp JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/scrollup.js"></script>
	<!-- Onepage Nav JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/onepage-nav.min.js"></script>
	<!-- Easing JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/easing.js"></script>
	<!-- Active JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/active.js"></script>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script src="<?= base_url();?>assets/eshop/eshop/js/custom.js"></script>

	<script>
		$(document).ready(function() {
			var listCourse	= []
			var baseUrl		= '<?php echo base_url();?>'

			$("#searchBox").keyup(function(){
				var	text	= $("#searchBox").val()

				$.get(baseUrl+'search/'+text, function(data, status){
					listCourse	=	[]
					var set	= JSON.parse(data)
					for (var i = 0; i < set.length; i++) {
						listCourse.push(set[i]['judul'])
					}
				});

				autocomplete(document.getElementById("searchBox"), listCourse);
			})

			$('.total-count').load("<?=$count_cart;?>");
			<?php
			$uri = $this->uri->segment(2);
			if (in_array($uri, ['','users_home']) == false) { ?>
				$(".main-category").hide();
				$(window).on("scroll", function() {
					var scrollPos = $(window).scrollTop();
					if (scrollPos <= 230) {
						$(".main-category").hide();
					} else {
						$(".main-category").show();
					}
				});
				$('.cat-heading').click(function() {
				   $(".main-category").toggle();
				});
			<?php } ?>
		});

		$(document).on("click", "#btnSearch", function(){
			var base_url	= '<?php echo base_url();?>'

			var keyword	= $("#searchBox").val()
			console.log(keyword)
			$("#keyword").val(keyword)

			const currUrl		= window.location.href
			const lastSegment 	= currUrl.split("/").pop()

			if (lastSegment.includes("users_course"))
			{
				load_content('table_content_<?php echo $class; ?>','form-table','html');
				if (keyword != '')
				{
					window.history.pushState("users_course", "Title", base_url+"users/users_course?judul="+keyword);
				} else
				{
					window.history.pushState("users_course", "Title", base_url+"users/users_course");
				}
			} else
			{
				window.location.href= base_url+"users/users_course?judul="+keyword
			}

		})

		$(document).on("click", "#btnSearch2", function(){
			var base_url	= '<?php echo base_url();?>'

			var keyword	= $("#searchBox2").val()
			console.log(keyword)
			$("#keyword").val(keyword)

			const currUrl		= window.location.href
			const lastSegment 	= currUrl.split("/").pop()

			if (lastSegment.includes("users_course"))
			{
				load_content('table_content_<?php echo $class; ?>','form-table','html');
				if (keyword != '')
				{
					window.history.pushState("users_course", "Title", base_url+"users/users_course?judul="+keyword);
				} else
				{
					window.history.pushState("users_course", "Title", base_url+"users/users_course");
				}
			} else
			{
				window.location.href= base_url+"users/users_course?judul="+keyword
			}

		})

		$('input[type=radio]').on('change', function() {
		    $(this).closest("#formLang").submit();
		});

		function openNav() {
		  document.getElementById("mySidenav").style.width = "70%";
		  // document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
			$('.blur-background').css('display', 'block')
		}

		function closeNav() {
		  document.getElementById("mySidenav").style.width = "0";
		  // document.body.style.backgroundColor = "white";
			$('.blur-background').css('display', 'none')
		}

	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-H9GN9YG0KB"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-H9GN9YG0KB');
	</script>

</body>
</html>
