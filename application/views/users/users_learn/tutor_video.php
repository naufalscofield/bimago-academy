<br>
<div id="all" style="height:500px; overflow:scroll;">
  <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="comments" id="comments">
            <div class="row comment">
              <div class="col-md-3 col-lg-2 text-center text-md-center">
                <p><img src="<?=base_url().'assets/avatar/'.$tutor['avatar'];?>" alt=""></p>
                <br>
              </div>
              <div class="col-md-9 col-lg-10">
                <h5><?=$tutor['nama_depan'].' '.$tutor['nama_belakang'];?></h5>
                <br>
                <p align="justify"><?= $tutor['profil'];?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
