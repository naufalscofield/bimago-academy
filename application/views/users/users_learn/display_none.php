<!DOCTYPE html>
<html lang="zxx">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
    <title><?= env('APP_NAME') ?></title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?= getIco(); ?>" />
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

	<!-- StyleSheet -->

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/bootstrap-v5.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/font-awesome.css">
	<!-- Fancybox -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery.fancybox.min.css">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/themify-icons.css">
	<!-- Jquery Ui -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery-ui.css">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/niceselect.css">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/animate.css">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/flex-slider.min.css">
	<!-- Slicknav -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/slicknav.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/custom.css">

	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/reset-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/style-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/responsive-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/videojs/css/video-js.css">

	<script src="<?= base_url();?>assets/eshop/eshop/js/jquery.min.js"></script>
	<script src="<?= base_url();?>assets/videojs/js/videojs-ie8.min.js"></script>
	<!-- <script src="<?= base_url();?>assets/devtools/index.js"></script> -->

  <!-- Ladda -->
  <link rel="stylesheet" href="<?=base_url();?>assets/ladda/ladda-themeless.min.css">
  <script src="<?=base_url();?>assets/ladda/spin.min.js"></script>
  <script src="<?=base_url();?>assets/ladda/ladda.min.js"></script>
  <script src="<?=base_url();?>assets/swal/sweetalert2.min.js"></script>
	<style>
		.header.shop .nav li a{
			color: #fff;
			text-transform: capitalize;
			font-size: 15px;
			padding: 20px 15px;
			font-weight: 500;
			display: block;
			position: relative;
			}
			.header.shop .nav li{
			color: red;
			}
			.header.sticky .header-inner{
			position:fixed;
			top:0;
			left:0;
			background:#333;
			animation: fadeInDown 1s both 0.2s;
			-webkit-box-shadow:0px 0px 10px rgba(0, 0, 0, 0.3);
			-moz-box-shadow:0px 0px 10px rgba(0, 0, 0, 0.3);
			box-shadow:0px 0px 10px rgba(0, 0, 0, 0.3);
			z-index:999;
			}
			.header.shop .nav > li > a:hover {
			/*text-decoration: none;
			font-weight: bold;
			border-radius:0px;
			border-bottom-width: 4px;
			border-bottom-style: solid;
			border-bottom-color: #FF6600;
			color: #000 !important;*/
			background-color: transparent !important;

			}
			.header.shop .logo {
			-webkit-box-shadow: 10px 0px 0px -9px #e0e0e0;
				-moz-box-shadow: 10px 0px 0px -9px #e0e0e0;
			box-shadow: 10px 0px 0px -9px #e0e0e0;
			}
			@media only screen and (max-width: 1400px) {
			.hide-logo{
				display: none;
			}
			}
			.header.sticky .header-inner .nav li a {
			color:white;
			}
			.header.shop .search-bar input {
			width: 470px;
			}
			#sidebar-wrapper {
			border-style: dotted;
			margin-left: -250px;
			left: 0;
			width: 250px;
			background: rgb(255 255 255);
			position: absolute;
			height: 70%;
			overflow-y: auto;
			z-index: 1000;
			transition: all 0.5s ease-in 0s;
			-webkit-transition: all 0.5s ease-in 0s;
			-moz-transition: all 0.5s ease-in 0s;
			-ms-transition: all 0.5s ease-in 0s;
			-o-transition: all 0.5s ease-in 0s;
			}
			#sidebar-wrapper-right {
			border-style: dotted;
			margin-right: -250px;
			right: 0;
			width: 250px;
			background: rgb(255 255 255);
			position: absolute;
			height: 70%;
			overflow-y: auto;
			z-index: 1000;
			transition: all 0.5s ease-in 0s;
			-webkit-transition: all 0.5s ease-in 0s;
			-moz-transition: all 0.5s ease-in 0s;
			-ms-transition: all 0.5s ease-in 0s;
			-o-transition: all 0.5s ease-in 0s;
			}

			.sidebar-nav {
			position: absolute;
			top: 0;
			width: 250px;
			list-style: none;
			margin: 0;
			padding: 0;
			}

			.sidebar-nav li {
			line-height: 50px;
			text-indent: 20px;
			}

			.sidebar-nav li a {
			color: #999999;
			display: block;
			text-decoration: none;
			}

			.sidebar-nav li a:hover {
			color: #fff;
			background: rgb(255 255 255);
			text-decoration: none;
			}

			.sidebar-nav li a:active, .sidebar-nav li a:focus {
			text-decoration: none;
			}

			.sidebar-nav > .sidebar-brand {
			height: 55px;
			line-height: 55px;
			font-size: 18px;
			}

			.sidebar-nav > .sidebar-brand a {
			color: #999999;
			}

			.sidebar-nav > .sidebar-brand a:hover {
			color: #fff;
			background: none;
			}

			#sidebar-wrapper.active {
			left: 250px;
			width: 250px;
			transition: all 0.5s ease-out 0s;
			-webkit-transition: all 0.5s ease-out 0s;
			-moz-transition: all 0.5s ease-out 0s;
			-ms-transition: all 0.5s ease-out 0s;
			-o-transition: all 0.5s ease-out 0s;
			}
			#sidebar-wrapper-right.active {
			right: 250px;
			width: 250px;
			transition: all 0.5s ease-out 0s;
			-webkit-transition: all 0.5s ease-out 0s;
			-moz-transition: all 0.5s ease-out 0s;
			-ms-transition: all 0.5s ease-out 0s;
			-o-transition: all 0.5s ease-out 0s;
			}

			.toggle {
			margin: 5px 5px 0 0;
			}
			#menu-toggle {
			position: absolute;
			z-index: 1;
			}
			#menu-toggle-right {
			position: absolute;
			z-index: 1;
			right: 0;
			}
			.full-height {
			    height: 100vh;
			    background-color: black;
			}
			.flex-center {
			    align-items: center;
			    display: flex;
			    justify-content: center;
			}
			.position-ref {
			    position: relative;
			}
			.top-right {
			    position: absolute;
			    right: 10px;
			    top: 18px;
			}
			.content {
			    text-align: center;
			}
			.title {
			    font-size: 84px;
			}
			.links > a {
			    color: #636b6f;
			    padding: 0 25px;
			    font-size: 12px;
			    font-weight: 600;
			    letter-spacing: .1rem;
			    text-decoration: none;
			    text-transform: uppercase;
			}
			.m-b-md {
			    margin-bottom: 30px;
			}

      .back-col {
        background-color: white;
      }

      /*
*
* ==========================================
* CUSTOM UTIL CLASSES
* ==========================================
*
*/
.progress {
  width: 150px;
  height: 150px;
  background: none;
  position: relative;
}

.progress::after {
  content: "";
  width: 100%;
  height: 100%;
  border-radius: 50%;
  border: 6px solid #eee;
  position: absolute;
  top: 0;
  left: 0;
}

.progress>span {
  width: 50%;
  height: 100%;
  overflow: hidden;
  position: absolute;
  top: 0;
  z-index: 1;
}

.progress .progress-left {
  left: 0;
}

.progress .progress-bar {
  width: 100%;
  height: 100%;
  background: none;
  border-width: 6px;
  border-style: solid;
  position: absolute;
  top: 0;
}

.progress .progress-left .progress-bar {
  left: 100%;
  border-top-right-radius: 80px;
  border-bottom-right-radius: 80px;
  border-left: 0;
  -webkit-transform-origin: center left;
  transform-origin: center left;
}

.progress .progress-right {
  right: 0;
}

.progress .progress-right .progress-bar {
  left: -100%;
  border-top-left-radius: 80px;
  border-bottom-left-radius: 80px;
  border-right: 0;
  -webkit-transform-origin: center right;
  transform-origin: center right;
}

.progress .progress-value {
  position: absolute;
  top: 0;
  left: 0;
}
.rounded-lg {
  border-radius: 1rem;
}

.text-gray {
  color: #aaa;
}

div.h4 {
  line-height: 1rem;
}

	</style>
</head>
<body class="js" style="background:#ebeef0;">
	<!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->

		<!-- Header -->
		<header class="header shop">
				<!-- Header Inner -->
				<div class="header-inner">
					<div class="">
						<div class="cat-nav-head">
							<div class="row no-gutters">
								<div class="col-lg-2 col-md-3 col-sm-3 hide-logo">
									<div class="all-category">
										<div class="logo pl-2 pr-0">
			                				<a href="<?= base_url(); ?>"><img src="<?= base_url();?>uploads/lms_config/<?= $info->logo_putih;?>" alt="logo" style="max-width: 90%"></a>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-5">
									<div class="menu-area">
										<!-- Main Menu -->
										<nav class="navbar navbar-expand-lg">
											<div class="navbar-collapse">
												<div class="nav-inner">
													<ul class="nav main-menu menu navbar-nav">
													<!-- <li style="width:500px"><a href="<?= base_url('users/users_course/detail/'.$course->id); ?>"><?= isset($course->judul) ? $course->judul : '';  ?></a></li> -->
													</ul>
												</div>
											</div>
										</nav>
										<!--/ End Main Menu -->
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="menu-area">
										<!-- Main Menu -->
										<nav class="navbar navbar-expand-lg">
											<div class="navbar-collapse">
												<!-- <div class="nav-inner">
											        <span>All Progress</span>
											        <div class="progress">
													 	 <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
											      </div>

												</div> -->
											</div>
										</nav>
										<!--/ End Main Menu -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ End Header Inner -->
		</header>
		<!--/ End Header -->
		<!-- Breadcrumbs -->
		<div class="breadcrumbs pt-1 pb-1" style="padding-bottom:1rem!important; padding-top:1rem!important; background-color:#b3b3b5;">
		  <div class="container">
		    <div class="row">
		      <div class="col-12 pl-2">
		        <div class="bread-inner text-left">
		            <a style="font-size:15px"><?=$course.' ('.$type.') ';?></a>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<!-- End Breadcrumbs -->
		<section style="overflow:hidden;">
		<div>
			<div class="container">
			  <div class="row">
          <div class="col-lg-3 col-md-12" align="center">
    			</div>
    			<div class="col-lg-6 col-md-12" align="center">
              <img src="<?=base_url('assets/default/empty_video.png');?>" style="width:100%;height:auto; margin-top:100px;" alt=""><br><br>
              <span style="font-size:25px;"><?= $this->lang->line('video_not_yet') ?></span><br><br><br><br>
    			</div>
          <div class="col-lg-3 col-md-12" align="center">
    			</div>
			  </div>
			</div>
		</div>
		</section>
    <br>
    <!-- Start Footer Area -->
  	<footer class="footer">
  		<!-- Footer Top -->
  		<div class="footer-top section">
  			<div class="container">
  				<div class="row">
  					<div class="col-lg-8 col-md-6 col-12">
  						<!-- Single Widget -->
  						<div class="single-footer about">
  							<div class="logo">
  								<a href="<?=base_url();?>"><img src="<?= base_url();?>uploads/lms_config/<?= $info->logo_putih;?>"  alt="logo" style="width: 260px;"></a>
  							</div>
  							<p class="text"><?= $this->lang->line('comdesc') ?></p>
  							<p class="call"><?= $this->lang->line('got_question') ?><span><a href="tel:123456789">(+6222) 723 7667</a></span></p>
  						</div>
  						<!-- End Single Widget -->
  					</div>
  					<div class="col-lg-4 col-md-12 col-12">
  						<!-- Single Widget -->
  						<div class="single-footer social">
  							<h4><?= $this->lang->line('get_in_touch') ?></h4>
  							<!-- Single Widget -->
  							<div class="contact">
  								<ul>
  									<li>Surapati Core Blok J No 2, Pasirlayung, Kec. Cibeunying Kidul.</li>
  									<li>Kota Bandung, Jawa Barat 40192.</li>
  									<li>academy@solmit.com</li>
  								</ul>
  							</div>
  							<!-- End Single Widget -->
  							<ul>
  								<li><a href="#"><i class="ti-facebook"></i></a></li>
  								<li><a href="#"><i class="ti-twitter"></i></a></li>
  								<li><a href="#"><i class="ti-flickr"></i></a></li>
  								<li><a href="#"><i class="ti-instagram"></i></a></li>
  							</ul>
  						</div>
  						<!-- End Single Widget -->
  					</div>
  				</div>
  			</div>
  		</div>
  		<!-- End Footer Top -->
  		<div class="copyright">
  			<div class="container">
  				<div class="inner">
  					<div class="row">
  						<div class="col-lg-6 col-12">
  							<div class="left">
<<<<<<< HEAD
  								<p>Copyright © 2020 <a href="http://www.wpthemesgrid.com" target="_blank">Bimago Academy</a>  -  All Rights Reserved.</p>
=======
  								<p>Copyright © 2020 <a href="http://www.wpthemesgrid.com" target="_blank"><?= env('APP_NAME') ?></a>  -  All Rights Reserved.</p>
>>>>>>> ecc8ace965d0bcabf6493637165cc2ca2a45be1b
  							</div>
  						</div>
  						<div class="col-lg-6 col-12">
  							<div class="right">
  								<img src="<?= base_url();?>assets/eshop/eshop/images/payments.png" alt="#">
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</footer>
  	<!-- /End Footer Area -->

	<!-- Jquery -->
  <script src="<?= base_url();?>assets/eshop/eshop/js/jquery-migrate-3.0.0.js"></script>
	<script src="<?= base_url();?>assets/eshop/eshop/js/jquery-ui.min.js"></script>
	<!-- Popper JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/popper.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/bootstrap.min.js"></script>
	<!-- Color JS -->
	<!-- <script src="<?= base_url();?>assets/eshop/eshop/js/colors.js"></script> -->
	<!-- Slicknav JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/slicknav.min.js"></script>
	<!-- Owl Carousel JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/owl-carousel.js"></script>
	<!-- Magnific Popup JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/magnific-popup.js"></script>
	<!-- Waypoints JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/waypoints.min.js"></script>
	<!-- Countdown JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/finalcountdown.min.js"></script>
	<!-- Nice Select JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/nicesellect.js"></script>
	<!-- Flex Slider JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/flex-slider.js"></script>
	<!-- ScrollUp JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/scrollup.js"></script>
	<!-- Onepage Nav JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/onepage-nav.min.js"></script>
	<!-- Easing JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/easing.js"></script>
	<!-- Active JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/active.js"></script>

	<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
	<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
	<script src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
	<script src="<?= base_url();?>assets/eshop/eshop/js/custom.js"></script>

	<script src="<?= base_url();?>assets/videojs/js/video.js"></script>
	<script src="<?= base_url();?>assets/devtools/index.js"></script>

</body>
</html>
