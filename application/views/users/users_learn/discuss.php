<style media="screen">
.navbar-nav {
  width: 100%
}

@media(min-width:568px) {
  .end {
      margin-left: auto
  }
}

@media(max-width:768px) {
  #post {
      width: 100%
  }
}

.checked {
  color: orange;
}

#clicked {
  padding-top: 1px;
  padding-bottom: 1px;
  text-align: center;
  width: 100%;
  background-color: #ecb21f;
  border-color: #a88734 #9c7e31 #846a29;
  color: black;
  border-width: 1px;
  border-style: solid;
  border-radius: 13px
}

#profile {
  background-color: unset
}

#post {
  margin: 10px;
  padding: 6px;
  padding-top: 2px;
  padding-bottom: 2px;
  text-align: center;
  background-color: #ecb21f;
  border-color: #a88734 #9c7e31 #846a29;
  color: black;
  border-width: 1px;
  border-style: solid;
  border-radius: 13px;
  width: 50%
}

.back-col {
  background-color: white;
}

#nav-items li a,
#profile {
  text-decoration: none;
  color: rgb(224, 219, 219);
  background-color: black
}

.comments {
  margin-top: 5%;
  margin-left: 20px
}

.darker {
  border: 1px solid #ecb21f;
  background-color: black;
  float: right;
  width: 100%;
  border-radius: 5px;
  padding-left: 40px;
  padding-right: 30px;
  padding-top: 10px
}

.comment {
  border: 1px solid rgba(16, 46, 46, 1);
  background-color: rgba(16, 46, 46, 0.973);
  float: left;
  width: 100%;
  border-radius: 5px;
  padding-left: 40px;
  padding-right: 30px;
  padding-top: 10px
}

.comment h6,
.comment span,
.darker h6,
.darker span {
  display: inline
}

.comment p,
.comment span,
.darker p,
.darker span {
  color: rgb(184, 183, 183)
}

h2 {
  color: black;
  font-weight: bold
}

h6 {
  color: white;
  font-weight: bold
}

label {
  color: rgb(212, 208, 208)
}

#align-form {
  margin-top: 20px
}

.form-group p a {
  color: white
}

#checkbx {
  background-color: black
}

#darker img {
  margin-right: 15px;
  position: static
}

.form-group input,
.form-group textarea {
  background-color: black;
  border: 1px solid rgba(16, 46, 46, 1);
  border-radius: 12px
}

form {
  border: 1px solid rgba(16, 46, 46, 1);
  background-color: rgba(16, 46, 46, 0.973);
  border-radius: 5px;
  padding: 20px
}

.rating {
    border: none;
    float: left;
}

.rating>input {
    display: none;
}

.rating>label::before {
    margin: 5px;
    font-size: 1.5em;
    font-family: FontAwesome;
    display: inline-block;
    content: "\f005";
}

.rating>label {
    color: #ddd;
    float: right;
}

.rating>input:checked~label,
.rating:not(:checked)>label:hover,
.rating:not(:checked)>label:hover~label {
    color: #f7d106;
}

.rating>input:checked+label:hover,
.rating>input:checked~label:hover,
.rating>label:hover~input:checked~label,
.rating>input:checked~label:hover~label {
    color: #fce873;
}

</style>
<!-- Main Body -->
<section>
    <div class="container back-col">
        <div class="row">
          <div class="col-sm-8 col-md-6 col-12">
            <div class="col-12">
              <h2>Comment</h2>
            </div>
            <div class="col-12" style="height:500px; overflow:scroll;">
              <?php $no = 0; foreach ($komentar as $key => $value){ $no++; ?>
                <?php if ($no % 2 == 0){
                  $class = 'text-justify darker mt-4 float-left';
                }else{
                  $class = 'comment mt-4 text-justify float-left';
                } ?>
                <?php if ($this->session->userdata('login_via') == 'solmit') {
                  if ($value['avatar'] == null || $value['avatar'] == '') {
                    if ($value['jk'] == 'W') {
                      $image = base_url().'assets/default/female_avatar_s0lm1t.png';
                    }else{
                      $image = base_url().'assets/default/male_avatar_s0lm1t.png';
                    }
                  }else{
                    $image = base_url().'assets/avatar/'.$value['avatar'];
                  }
                }else{
                  $image = $value['avatar'];
                } ?>
                <div class="<?=$class;?>"> <img src="<?=$image;?>" alt="" class="rounded-circle" width="40" height="40">
                  <h6><?=$value['nama_depan'].' '.$value['nama_belakang'];?></h6><br>
                  <span>
                    <?=indo_date($value['tanggal']);?> <br>
                    <?php $not = 5 - $value['nilai'];
                    for ($i=0; $i < $value['nilai']; $i++) { ?>
                      <span class="fa fa-star checked"></span>
                    <?php } ?>
                    <?php for ($i=0; $i < $not; $i++) { ?>
                      <span class="fa fa-star-o"></span>
                    <?php } ?>
                  </span> <br>
                  <p><?=$value['komentar'];?></p>
                </div>
              <?php } ?>
            </div>
          </div>
            <div class="col-lg-4 col-md-5 col-sm-4 offset-md-1 offset-sm-1 col-12 mt-4">
              <div class="col-12">
                <?php echo form_open_multipart($save_rating, array('name' => 'form-rating', 'id' => 'form-rating', 'style' => 'padding:15px;width:310px;margin-left:-2rem;')); ?>

                <h6>Leave a comment</h6>
                <br>
                <div class="form-group">
                  <input type="hidden" name="id_episode" value="<?=$id_episode;?>">
                  <?php if ($akses_komen == FALSE) { ?>
                    <div class="rating">
                      <input type="radio" class="rate" id="star5" name="rating" value="5"/>
                      <label for="star5" title="5 Star"></label>

                      <input type="radio" class="rate" id="star4" name="rating" value="4"/>
                      <label for="star4" title="4 Star"></label>

                      <input type="radio" class="rate" id="star3" name="rating" value="3"/>
                      <label for="star3" title="3 Star"></label>

                      <input type="radio" class="rate" id="star2" name="rating" value="2"/>
                      <label for="star2" title="2 Star"></label>

                      <input type="radio" class="rate" id="star1" name="rating" value="1"/>
                      <label for="star1" title="1 Star"></label>
                    </div>
                  </div>
                <?php } ?>
                <div class="form-group">
                  <textarea name="msg" id="" msg cols="30" rows="5" class="form-control" style="background-color: white;"></textarea>
                </div>

                <div class="form-group" style="width:100%"> <button type="button" id="save_rating" class="btn">Post Comment</button> </div>
                <?php echo form_close();?>
              </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
  $("#save_rating").click(function(){
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
      $.ajax({
        type: form.attr("method"),
        url: form.attr("action"),
        data: formdata ? formdata : form.serialize(),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["proses"] == 'success') {
             toastr.success(hasil["pesan"]);
             setTimeout(function () {
               location.reload(true);
             }, 2000);
           }else{
             toastr.error(hasil["pesan"]);
           }
         },
         error: function (res) {
           toastr.error("Komentar tidak dapat disimpan.");
         },
      });
  });
</script>
