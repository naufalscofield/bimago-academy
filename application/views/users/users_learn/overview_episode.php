<h5><?= $this->lang->line('about_course');?></h5>
<hr>
<div class="container p-1" style="overflow:scroll;height:300px">
  <div class="nice_view" >
    <h6><?=$this->lang->line('description');?></h6>
    <?= $overview; ?>
  </div>
</div>
