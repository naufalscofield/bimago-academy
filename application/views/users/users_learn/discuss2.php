<style media="screen">

.img-sm {
  width: 46px;
  height: 46px;
}

.panel {
  box-shadow: 0 2px 0 rgba(0,0,0,0.075);
  border-radius: 0;
  border: 0;
  margin-bottom: 15px;
}

.panel .panel-footer, .panel>:last-child {
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
}

.panel .panel-heading, .panel>:first-child {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

.panel-body {
  padding: 20px 10px;
}


.media-block .media-left {
  display: block;
  float: left
}

.media-block .media-right {
  float: right
}

.media-block .media-body {
  display: block;
  overflow: hidden;
  width: auto
}

.middle .media-left,
.middle .media-right,
.middle .media-body {
  vertical-align: middle
}

.thumbnail {
  border-radius: 0;
  border-color: #e9e9e9
}

.tag.tag-sm, .btn-group-sm>.tag {
  padding: 5px 10px;
}

.tag:not(.label) {
  background-color: #fff;
  padding: 6px 12px;
  border-radius: 2px;
  border: 1px solid #cdd6e1;
  font-size: 12px;
  line-height: 1.42857;
  vertical-align: middle;
  -webkit-transition: all .15s;
  transition: all .15s;
}
.text-muted, a.text-muted:hover, a.text-muted:focus {
  color: #acacac;
}
.text-sm {
  font-size: 0.9em;
}
.text-5x, .text-4x, .text-5x, .text-2x, .text-lg, .text-sm, .text-xs {
  line-height: 1.25;
}

.btn-trans {
  background-color: transparent;
  border-color: transparent;
  color: #929292;
}

.btn-icon {
  padding-left: 9px;
  padding-right: 9px;
}

.btn-sm, .btn-group-sm>.btn, .btn-icon.btn-sm {
  padding: 5px 10px !important;
}

.mar-top {
  margin-top: 15px;
}

.rating {
    border: none;
    float: left;
}

.rating>input {
    display: none;
}

.rating>label::before {
    margin: 5px;
    font-size: 1.5em;
    font-family: FontAwesome;
    display: inline-block;
    content: "\f005";
}

.rating>label {
    color: #ddd;
    float: right;
}

.rating>input:checked~label,
.rating:not(:checked)>label:hover,
.rating:not(:checked)>label:hover~label {
    color: #f7d106;
}

.rating>input:checked+label:hover,
.rating>input:checked~label:hover,
.rating>label:hover~input:checked~label,
.rating>input:checked~label:hover~label {
    color: #fce873;
}
</style>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container bootdey" style="padding-left:1px;padding-right:1px;">
  <div class="col-md-12 bootstrap snippets" style="padding-left:1px;padding-right:1px;">
    <div class="col-12">
      <div class="panel">
        <div class="panel-body" style="margin-top:-20px">
          <?php echo form_open_multipart($save_rating, array('name' => 'form-rating', 'id' => 'form-rating')); ?>
          <textarea class="form-control" rows="2" name="msg" placeholder="What are you thinking?"></textarea>
          <div class="mar-top clearfix">
            <button class="btn btn-sm btn-primary pull-right" id="save_rating" type="button"><i class="fa fa-pencil fa-fw"></i> Comment</button>
            <div class="form-group">
              <input type="hidden" name="id_episode" value="<?=$id_episode;?>">
              <?php if ($akses_komen == FALSE) { ?>
                <div class="rating">
                  <input type="radio" class="rate" id="star5" name="rating" value="5"/>
                  <label for="star5" title="5 Star"></label>

                  <input type="radio" class="rate" id="star4" name="rating" value="4"/>
                  <label for="star4" title="4 Star"></label>

                  <input type="radio" class="rate" id="star3" name="rating" value="3"/>
                  <label for="star3" title="3 Star"></label>

                  <input type="radio" class="rate" id="star2" name="rating" value="2"/>
                  <label for="star2" title="2 Star"></label>

                  <input type="radio" class="rate" id="star1" name="rating" value="1"/>
                  <label for="star1" title="1 Star"></label>
                </div>
              </div>
            <?php } ?>
          </div>
          <?php echo form_close();?>
        </div>
      </div>
    </div>
    <div class="col-12" style="height:250px; overflow:scroll;">
      <div class="panel">
          <div class="panel-body">
          <!-- Newsfeed Content -->
          <?php foreach ($komentar as $key => $value) { ?>
            <!--===================================================-->
            <div class="media-block">
              <?php if ($this->session->userdata('login_via') == 'solmit') {
                if ($value['avatar'] == null || $value['avatar'] == '') {
                  if ($value['jk'] == 'W') {
                    $image = base_url().'assets/default/female_avatar_s0lm1t.png';
                  }else{
                    $image = base_url().'assets/default/male_avatar_s0lm1t.png';
                  }
                }else{
                  $image = base_url().'assets/avatar/'.$value['avatar'];
                }
              }else{
                $image = $value['avatar'];
              } ?>
              <a class="media-left" href="#"><img class="img-circle img-sm" alt="Profile Picture" src="<?=$image;?>"></a>
              <div class="media-body">
                <div class="mar-btm" style="margin-left:10px;">
                  <a class="btn-link text-semibold media-heading box-inline"><?=$value['nama_depan'].' '.$value['nama_belakang'];?></a>
                  <p class="text-muted text-sm">
                    <?php $not = 5 - $value['nilai'];
                    for ($i=0; $i < $value['nilai']; $i++) { ?>
                      <span class="fa fa-star checked"></span>
                    <?php } ?>
                    <?php for ($i=0; $i < $not; $i++) { ?>
                      <span class="fa fa-star-o"></span>
                    <?php } ?>
                  </p>
                </div>
                <p style="margin: revert">
                  <?=$value['komentar'];?>
                </p>
                <div class="pad-ver">
                  <div class="btn-group">
                    <span><i class="fa fa-calendar"></i> <?=indo_date($value['tanggal']);?></span>
                  </div>
                </div>
                <hr>
              </div>
            </div>
            <!--===================================================-->
          <?php } ?>
          <!-- End Newsfeed Content -->
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#save_rating").click(function(){
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
      $.ajax({
        type: form.attr("method"),
        url: form.attr("action"),
        data: formdata ? formdata : form.serialize(),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["proses"] == 'success') {
             toastr.success(hasil["pesan"]);
             setTimeout(function () {
               location.reload(true);
             }, 2000);
           }else{
             toastr.error(hasil["pesan"]);
           }
         },
         error: function (res) {
           toastr.error("Komentar tidak dapat disimpan.");
         },
      });
  });
</script>
