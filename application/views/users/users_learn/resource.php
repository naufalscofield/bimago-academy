<section class="shop-home-list section" style="height:280px; overflow:scroll; margin-top:-20px">
	<br>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12" >
					<!-- Start Single List  -->
						<table class="table table-hover table-striped">
							<tr>
								<th width="5%" style="background:#b9b7b7">No</th>
								<th width="75%" style="background:#b9b7b7"><?= $this->lang->line('resource');?></th>
								<th width="20%" style="background:#b9b7b7"><?= $this->lang->line('download');?></th>
							</tr>
								<?php if ($resource->num_rows()): ?>
									<?php foreach ($resource->result() as $key => $value): ?>
										<tr>
											<td><?= $key+1 ?></td>
											<td><?= $value->nama_file; ?></td>
											<td>
												<a style="color:white;" id='<?= $value->id ?>' class="btn btn-sm onPreview ladda-button ladda-button_<?= $value->id ?>" data-style="expand-left"><i class="fa fa-download"></i></a>
											</td>
										</tr>
									<?php endforeach ?>
							<?php else: ?>
									<tr>
										<td colspan="4" class="text-center">Materi / resource belum di isi.</td>
									</tr>
							<?php endif ?>
						</table>
					<!-- End Single List  -->
				</div>
			</div>
		</div>
	</section>

	<script>
	$(".onPreview").click(function(event){
		var id = $(this).attr('id');
		var l = Ladda.create( document.querySelector('.ladda-button_'+id) );
		l.start();
		$.ajax({
			url: "<?=$get_pdf_preview;?>",
			method: "POST",
			data: {id:id},
			success: function(data){
				l.stop();
				var hasil = $.parseJSON(data);
				window.open(hasil["url"], '_blank');
		}
	});
});
</script>
