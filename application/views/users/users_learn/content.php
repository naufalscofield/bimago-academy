  <style>
  	no-bullet.ul {
	  list-style-type: none;
	}

    .circle-green {
      width: 40px;
      height: 40px;
      line-height: 40px;
      border-radius: 50%;
      font-size: 15px;
      background: #228B22;
      text-align: center;
      color: #fff
    }
    .circle-red {
      width: 40px;
      height: 40px;
      line-height: 40px;
      border-radius: 50%;
      font-size: 15px;
      background: #c81c1d;
      text-align: center;
      color: #fff
    }

    .btn-blue2 {
    background-color: #0c60af;
    border-color: #0c60af;
    color: white;
    border-radius: 2px;
    }
    .btn-blue2:hover {
    background-color: #87CEFA;
    border-color: #87CEFA;
    }
    .btn-gray2 {
    background-color: #C0C0C0;
    border-color: #C0C0C0;
    color: white;
    border-radius: 2px;
    }
    .btn-gray2:hover {
    background-color: #C0C0C0;
    border-color: #C0C0C0;
    }
    .btn-red2 {
    background-color: #c81c1d;
    border-color: #c81c1d;
    color: white;
    border-radius: 2px;
    }
    .btn-red2:hover {
    background-color: #c81c1d;
    border-color: #c81c1d;
    }
    .btn-green2 {
    background-color: #228B22;
    border-color: #228B22;
    color: white;
    border-radius: 2px;
    }
    .btn-green2:hover {
    background-color: #3CB371;
    border-color: #3CB371;
    }
    .btn-yellow2 {
    background-color: #FFD700;
    color: white;
    border-radius: 2px;
    border-color: #FFD700;
    }
    .btn-yellow2:hover {
    background-color: #F0E68C;
    border-color: #F0E68C;
    }

    .card{
    background-color: #fff;
    height: auto;
    width: auto;
    overflow: hidden;
    margin: 12px;
    border-radius: 5px;
    box-shadow: 9px 17px 45px -29px
    rgba(0, 0, 0, 0.44);
    }
    .card__image img {
    width: 100%;
    height: 100%;
    }
    .card__image.loading {
    height: 200px;
    width: 400px;
    }
    .card__title {
    padding: 8px;
    font-size: 22px;
    font-weight: 700;
    }
    .card__title.loading {
    height: 1rem;
    width: 50%;
    margin: 1rem;
    border-radius: 3px;
    }
    .card__description {
    padding: 8px;
    font-size: 16px;
    }
    .card__description.loading {
    height: 3rem;
    margin: 1rem;
    border-radius: 3px;
    }
    .loading {
    position: relative;
    background-color: #e2e2e2;
    }
    .loading::after {
    display: block;
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    transform: translateX(-100%);
    background: -webkit-gradient(linear, left top,
    right top, from(transparent),
    color-stop(rgba(255, 255, 255, 0.2)),
    to(transparent));
    background: linear-gradient(90deg, transparent,
    rgba(255, 255, 255, 0.2), transparent);
    animation: loading 0.8s infinite;
    }
    @keyframes loading {
    100% {
    transform: translateX(100%);
    }
    }
    .pagination a {
    color: black;
    float: center;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    }
    .pagination a.active {
    background-color: dodgerblue;
    color: white;
    }
    .pagination a:hover:not(.active) {background-color: #ddd;}
    .modal-dialog {
    width:100%
    }
    .modal-content {
    height: 100%;
    width: 100%;
    }
    .modal-body {
    overflow-y: scroll;
    }
  </style>
  <!-- <br> -->
  <!-- <center><span><?=$judul_course;?></span> <center> -->
  <!-- Accordion card -->
  <div class="card">
    <!-- Card header -->
    <div class="card-header" role="tab" id="headingProgres2">
      <a data-toggle="collapse" data-parent="#accordionExProgres2" href="#collapseProgres2" aria-expanded="true"
        aria-controls="collapseProgres2">
        <h6 class="mb-0">
          Modules <i class="fa fa-angle-down rotate-icon"></i>
          <br>
        </h6>
      </a>
    </div>

    <!-- Card body -->
    <div id="collapseProgres2" class="collapse show" role="tabpanel" aria-labelledby="headingProgres2"
      data-parent="#accordionExProgres2" style="height:300px; overflow-y:scroll;">
      <div class="card-body">
        <span>Overall Progress</span>
        <div class="progress">
          <div class="progress-bar" id="progress-bar-video-cont" role="progressbar" style="width: <?=$progres_all;?>%;" aria-valuenow="<?=$progres_all;?>" aria-valuemin="0" aria-valuemax="100"><span id="progres-view-cont"><?=$progres_all;?></span>%</div>
          <!-- <div class="progress-bar" id="progress-bar-video" role="progressbar" style="width: <?=$progres_all;?>%;" aria-valuenow="<?=$progres_all;?>" aria-valuemin="0" aria-valuemax="100"><span id="progres-all-view"><?=$progres_all;?></span>%</div> -->
        </div><br>

        <table class="table table-hover">
          <?php if ($num_episode > 0): ?>
            <?php $no=0; foreach ($episode as $key => $value) { $no++; ?>
            	<tr>
            		<!-- <td width="5px"><input type="checkbox"></td> -->
            		<td>
                  <a href="<?= base_url('users/users_learn/learn/'.encode_url($id_course).'/'.encode_url($id_type_course).'/'.encode_url($value['id']));  ?>"><i class="fa fa-play-circle <?= $id_episode == $value['id'] ? 'text-success' : ''; ?>"></i> <?=$no;?>. <?=$value['judul'] ?> (<span id="persen-view-cont_<?=$value['id'];?>"><?=$value['progres_vid'];?></span>%)</a><br>

                  <?php if (!empty($value['nilai'])) { ?>
                    <?php if ($value['rating_belakang'] == '') { ?>
                      <?php $not = 5 - $value['nilai'];
                      for ($i=0; $i < $value['nilai']; $i++) { ?>
                        <span class="fa fa-star checked"></span>
                      <?php } ?>
                      <?php for ($i=0; $i < $not; $i++) { ?>
                        <span class="fa fa-star-o"></span>
                      <?php } ?>
                    <?php }else{ ?>
                      <?php $not = 4 - $value['rating_depan'];
                      for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                        <span class="fa fa-star checked"></span>
                      <?php } ?>
                      <span class="fa fa-star-half-o checked"></span>
                      <?php for ($i=0; $i < $not; $i++) { ?>
                        <span class="fa fa-star-o"></span>
                      <?php } ?>
                    <?php } ?>
                  <?php }else{ ?>
                    <?php for ($i=0; $i < 5 ; $i++) { ?>
                      <span class="fa fa-star-o"></span>
                    <?php } ?>
                  <?php } ?>
                  (<?=$value['reviewer'];?>)<br>
                  <!-- Video : <br>
                  <div class="progress">
                    <div class="progress-bar" id="progress-bar-video_<?=$value['id'];?>" role="progressbar" style="width: <?=$value['progres_vid'];?>%;" aria-valuenow="<?=$value['progres_vid'];?>" aria-valuemin="0" aria-valuemax="100"><span id="persen-view_<?=$value['id'];?>"><?=$value['progres_vid'];?></span>%</div>
                  </div>
                  Quiz : <br>
                    <?php if ($value['quiz'] == '') {
                      $quiz = '<i class="fa fa-hourglass"></i> Not yet';
                    }elseif($value['quiz'] == 'passed'){
                      $quiz = '<i class="fa fa-check-circle"></i> Passed';
                    }elseif($value['quiz'] == 'failed'){
                      $quiz = '<i class="fa fa-times-circle"></i> Failed';
                    }elseif($value['quiz'] == 'timeout'){
                      $quiz = '<i class="fa fa-times-circle"></i> Timeout';
                    } ?>
                    <span><?=$quiz;?></span> -->
            		</td>
            	</tr>
            <?php } ?>
          <?php endif ?>
          <td>
            <?php if ($exam->num_rows() > 0){ ?>
              <?php foreach ($exam->result_array() as $key => $course) { ?>
                    <?php
                    if ($course['id_exam'] != NULL) {
                       if ($course['is_running'] == "0" || $course['is_running'] == 0) {
                          if ($course['status_lulus_terakhir'] == NULL) { ?>
                            <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room/<?= $course['id'];?>/n" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue2 btnToUjroh2"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_exam') ?></button></center>
                          <?php }
                          else if ($course['status_lulus_terakhir'] == 'passed') { ?>
                            <center><button disabled class="btn btn-green2"><i class="fa fa-check"></i> <?= $this->lang->line('passed') ?></button></center>
                          <?php }
                          else if ($course['status_lulus_terakhir'] == 'failed' && $course['jumlah_kesempatan'] <= $course['percobaan']) { ?>
                            <center><button disabled class="btn btn-red2"><i class="fa fa-times"></i> <?= $this->lang->line('failed') ?></button></center>
                          <?php }
                          else if ($course['status_lulus_terakhir'] == 'timeout' && $course['jumlah_kesempatan'] <= $course['percobaan']) { ?>
                            <center><button disabled class="btn btn-red2"><i class="fa fa-times"></i> <?= $this->lang->line('timeout') ?></button></center>
                          <?php }
                          else if ( ($course['status_lulus_terakhir'] == 'failed' && $course['jumlah_kesempatan'] > $course['percobaan']) ) { ?>
                            <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room/<?= $course['id'];?>/n" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue2 btnToUjroh2"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_exam') ?></button></center>
                          <?php }
                          else if ( ($course['status_lulus_terakhir'] == 'timeout' && $course['jumlah_kesempatan'] > $course['percobaan']) ) { ?>
                            <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room/<?= $course['id'];?>/n" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue2 btnToUjroh2"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_exam') ?></button></center>
                          <?php }
                        } else { ?>
                             <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="yes" data-href="<?=base_url();?>create-room/<?=$course['id'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-yellow2 btnToUjroh2" onclick="window.open('<?= base_url();?>create-room/<?= $course['id'];?>/y','_blank','name','width=600,height=400;')"><i class="fa fa-flag"></i> <?= $this->lang->line('continue_exam') ?></button></center>
                       <?php }
                    } else { ?>
                         <center>
                            <button disabled class="btn btn-gray2">
                               <i class="fa fa-warning"></i> <?= $this->lang->line('exam_not_avaiable') ?></a>
                         </center>
                   <?php } ?>
              <?php } ?>
            <?php } ?>
          </td>
        </table>

      </div>
    </div>
  </div>

  <div class="modal fade" id="modal" tabindex="-1" role="dialog">
     <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
           </div>
           <div class="modal-body">
              <div class="row no-gutters">
                 <div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="quickview-content">
                       <h2><?= $this->lang->line('exam_rules') ?></h2>
                       <hr>
                       <div class="size">
                          <div class="row">
                             <div class="col-lg-4 col-4">
                                <h5 class="title"><?= $this->lang->line('time') ?></h5>
                                <label for="" id="exam_time2">...</label>
                             </div>
                             <div class="col-lg-4 col-4">
                                <h5 class="title">Passing Grade</h5>
                                <label for="" id="exam_pg2">...</label>
                             </div>
                             <div class="col-lg-4 col-4">
                                <h5 class="title"><?= $this->lang->line('timeout_act') ?></h5>
                                <label for="" id="exam_timeout2">...</label>
                             </div>
                          </div>
                          <br>
                          <div class="row">
                             <div class="col-lg-12 col-12">
                                <p><?= $this->lang->line('connection_desc') ?></p>
                             </div>
                          </div>
                          <br>
                          <label for="" style="color:red"><?= $this->lang->line('by_clicking') ?></label>
                          <br>
                          <br>
                          <center>
                             <a id="hrefToExam2" href="" target="popup" onclick="" class="btn btn-blue2" style="color:white">OK</a>
                          </center>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div>

  <script type="text/javascript">
  $(document).ready(function(){

    $(document).on('click', '.btnToUjroh2', function(){
      var currRoom  = '<?php echo $room;?>'
      var idExam    = $(this).data('id_exam')
      var isContinue  = $(this).data('continue')
      if (isContinue == 'yes')
      {
        window.location.replace('<?php echo base_url();?>')
      }
      if (currRoom == 'none')
      {
        $('#modal').modal('show')
        $('#exam_time2').html($(this).data('time')+' Minutes')
        $('#exam_pg2').html($(this).data('pg')+' Score')
        var act = $(this).data('timeout').replace('_',' ')
        act = act.toUpperCase()
        $('#exam_timeout2').html(act)
        var link = $(this).data('href')+"','_blank"
        var href = "window.open('"+link+")"
        $('#hrefToExam2').attr('href', $(this).data('href'))
        $('#hrefToExam2').attr('target', 'popup')
        $('#hrefToExam2').attr('onclick', "window.open('"+link+"','name','width=600,height=400');window.location.replace('<?php echo base_url();?>')")
        // location.reload()
      } else
      {
        if (currRoom != idExam)
        {
          Swal.fire({
            icon: 'info',
            title: 'Oops...',
            text: 'You have unfinished exam, finish first exam to take this exam!',
          })
        } else
        {
          // var isContinue  = $(this).data('continue')
          // if (isContinue == 'no')
          // {
          //   sessionStorage.clear()
          // }
          $('#modal').modal('show')
          $('#exam_time2').html($(this).data('time')+' Minutes')
          $('#exam_pg2').html($(this).data('pg')+' Score')
          var act = $(this).data('timeout').replace('_',' ')
          act = act.toUpperCase()
          $('#exam_timeout2').html(act)
          var link = $(this).data('href')+"','_blank"
          var href = "window.open('"+link+")"
          $('#hrefToExam2').attr('href', $(this).data('href'))
          $('#hrefToExam2').attr('target', 'popup')
          $('#hrefToExam2').attr('onclick', "window.open('"+link+"','name','width=600,height=400');window.location.replace('<?php echo base_url();?>')")
          // window.focus();
          // location.reload();
        }
      }
    })
  })

  // var myPlayer = videojs('my-player');
  // $(document).ready(function(){
  //   <?php if(!empty($waktu)){ ?>
  //    myPlayer.currentTime(<?=$waktu;?>);
  //   <?php } ?>
  // })
  // var watchPoints = [];
  // $("#my-player").bind("timeupdate", function(){
  //   var currentTimePlay = this.currentTime();
  //   var watchPoint = Math.floor((currentTimePlay/this.duration()) * 100);
  //   if(watchPoints.indexOf(watchPoint) == -1){
  //     if( watchPoint % 3 == 0 ) {
  //       $.ajax({
  //         type: 'POST',
  //         url: '<?= base_url('users/users_learn/persen_video')?>',
  //         data: { id_course : '<?=$id_course;?>', id_episode : '<?= $id_episode ?>', id_type_course : '<?=$id_type_course;?>', persentase : watchPoint, waktu : currentTimePlay.toFixed(2) },
  //         success: function(res) {
  //           var hasil = $.parseJSON(res);
  //           $('#persen-view-cont_'+'<?=$id_episode;?>').html(hasil["persentase"]);
  //           // $('#progress-bar-video_'+'<?=$id_episode;?>').attr('aria-valuenow', hasil["persentase"]);
  //           // $('#progress-bar-video_'+'<?=$id_episode;?>').attr('style', 'width:'+hasil["persentase"]+'%');
  //           $('#progres-view-cont').html(hasil["persentase_all"]);
  //           $('#progress-bar-video-cont').attr('aria-valuenow', hasil["persentase_all"]);
  //           $('#progress-bar-video-cont').attr('style', 'width:'+hasil["persentase_all"]+'%');
  //         },
  //       });
  //     }
  //     watchPoints.push(Math.floor(watchPoint))
  //   }
  // });
  </script>
