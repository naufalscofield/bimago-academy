<style>
	.header.shop .search-bar {
		width: 473px;
	}
</style>
<header class="header shop">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-7 col-12">
				<div class="search-bar-top">
					<div class="search-bar">
						<form>
							<input name="search" placeholder="Search Course Here....." type="search">
							<button class="btnn"><i class="ti-search"></i></button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-7 col-12 text-center pt-5 pb-5">
				<h4><?= $this->lang->line('start_search') ?></h4>
				<p><?= $this->lang->line('to_find') ?></p>
			</div>
		</div>
	</div>
</div>
</header>