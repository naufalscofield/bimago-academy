<style>
   .underline {
   border-bottom: 2px solid currentColor;
   }
   .btn-blue {
   background-color: #0c60af;
   border-color: #0c60af;
   color: white;
   border-radius: 2px;
   width: 270px;
   }
   .btn-blue:hover {
   background-color: #87CEFA;
   border-color: #87CEFA;
   }
   .btn-gray {
   background-color: #C0C0C0;
   border-color: #C0C0C0;
   color: white;
   border-radius: 2px;
   width: 270px;
   }
   .btn-gray:hover {
   background-color: #C0C0C0;
   border-color: #C0C0C0;
   }
   .btn-red {
   background-color: #c81c1d;
   border-color: #c81c1d;
   color: white;
   border-radius: 2px;
   width: 270px;
   }
   .btn-red:hover {
   background-color: #c81c1d;
   border-color: #c81c1d;
   }
   .btn-green {
   background-color: #228B22;
   border-color: #228B22;
   color: white;
   border-radius: 2px;
   width: 270px;
   }
   .btn-green:hover {
   background-color: #3CB371;
   border-color: #3CB371;
   }
   .btn-yellow {
   background-color: #FFD700;
   color: white;
   border-radius: 2px;
   width: 270px;
   border-color: #FFD700;
   }
   .btn-yellow:hover {
   background-color: #F0E68C;
   border-color: #F0E68C;
   }
   .card{
   background-color: #fff;
   height: auto;
   width: auto;
   overflow: hidden;
   margin: 12px;
   border-radius: 5px;
   box-shadow: 9px 17px 45px -29px
   rgba(0, 0, 0, 0.44);
   }
   .card__image img {
   width: 100%;
   height: 100%;
   }
   .card__image.loading {
   height: 200px;
   width: 400px;
   }
   .card__title {
   padding: 8px;
   font-size: 22px;
   font-weight: 700;
   }
   .card__title.loading {
   height: 1rem;
   width: 50%;
   margin: 1rem;
   border-radius: 3px;
   }
   .card__description {
   padding: 8px;
   font-size: 16px;
   }
   .card__description.loading {
   height: 3rem;
   margin: 1rem;
   border-radius: 3px;
   }
   .loading {
   position: relative;
   background-color: #e2e2e2;
   }
   .loading::after {
   display: block;
   content: "";
   position: absolute;
   width: 100%;
   height: 100%;
   transform: translateX(-100%);
   background: -webkit-gradient(linear, left top,
   right top, from(transparent),
   color-stop(rgba(255, 255, 255, 0.2)),
   to(transparent));
   background: linear-gradient(90deg, transparent,
   rgba(255, 255, 255, 0.2), transparent);
   animation: loading 0.8s infinite;
   }
   @keyframes loading {
   100% {
   transform: translateX(100%);
   }
   }
   .pagination a {
   color: black;
   float: center;
   padding: 8px 16px;
   text-decoration: none;
   transition: background-color .3s;
   }
   .pagination a.active {
   background-color: dodgerblue;
   color: white;
   }
   .pagination a:hover:not(.active) {background-color: #ddd;}
   .modal-dialog {
   width:100%
   }
   .modal-content {
   height: 100%;
   width: 100%;
   }
   .modal-body {
   overflow-y: scroll;
   }
   .circle-green {
   width: 40px;
   height: 40px;
   line-height: 40px;
   border-radius: 50%;
   font-size: 15px;
   background: #228B22;
   text-align: center;
   color: #fff
   }
   .circle-red {
   width: 40px;
   height: 40px;
   line-height: 40px;
   border-radius: 50%;
   font-size: 15px;
   background: #c81c1d;
   text-align: center;
   color: #fff
   }
</style>
<!-- End Breadcrumbs -->
<!-- Product Style -->
<section class="product-area shop-sidebar shop section" style="height:280px; overflow:scroll; margin-top:-10px">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-12">
         <div class="table-responsive">
            <table class="table">
               <thead>
                  <tr>
                     <th style="background:#b9b7b7">
                        <center>No</center>
                     </th>
                     <th style="background:#b9b7b7">
                        <center><?= $this->lang->line('episode_title') ?></center>
                     </th>
                     <th style="background:#b9b7b7">
                        <center><?= $this->lang->line('chance') ?></center>
                     </th style="background:#b9b7b7">
                     <th style="background:#b9b7b7">
                        <center><?= $this->lang->line('time_remaining') ?></center>
                     </th>
                     <th style="background:#b9b7b7">
                        <center><i class="fa fa-pencil"></i></center>
                     </th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                     $no = 0;
                     foreach ($courses as $course)
                     {
                        $no++;
                  ?>
                  <tr>
                     <td>
                        <center><?= $no;?></center>
                     </td>
                     <td>
                        <?= $course['judul_episode'];?>
                     </td>
                     <?php
                        if ($course['jumlah_kesempatan'] != 0 && $course['status_lulus_terakhir'] != 'passed')
                        {
                     ?>
                     <td>
                        <center>
                           <div class="<?= ($course['jumlah_kesempatan'] - $course['percobaan'] <= 1) ? 'circle-red' : 'circle-green';?>"><?= $course['jumlah_kesempatan'] - $course['percobaan'];?></div>
                        </center>
                     </td>
                     <?php
                        } else if ($course['jumlah_kesempatan'] == 0)
                        {
                     ?>
                     <td>
                        <center>
                           <div class="circle-green"><img style="color:#fff; width:70%" src="<?= base_url();?>assets/default/infinity.png" alt=""></div>
                        </center>
                     </td>
                     <?php
                        } else
                        {
                        ?>
                     <td>
                     </td>
                     <?php
                        }
                        if ($course['time_remaining'] != NULL || isset($course['time_remaining']))
                        {
                     ?>
                     <td>
                        <center><?= $course['time_remaining'];?></center>
                     </td>
                     <?php
                        } else
                        {
                        ?>
                     <td>
                        <center>--:--</center>
                     </td>
                     <?php
                        }
                        if ($course['id_exam'] != NULL)
                        {
                           if ($course['is_running'] == "0" || $course['is_running'] == 0)
                           {
                              if ($course['status_lulus_terakhir'] == NULL)
                              {
                     ?>
                              <td>
                                 <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room-quiz/<?= $course['id'];?>/n/<?=$course['id_episode'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue btnToUjroh"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_quiz') ?></button></center>
                              </td>
                              <?php
                              }
                                 else if ($course['status_lulus_terakhir'] == 'passed')
                              {
                              ?>
                              <td>
                                 <center><button disabled class="btn btn-green"><i class="fa fa-check"></i> <?= $this->lang->line('passed') ?></button></center>
                              </td>
                              <?php
                                 } else if ($course['status_lulus_terakhir'] == 'failed' && $course['jumlah_kesempatan'] <= $course['percobaan'])
                                 {
                                 ?>
                              <td>
                                 <center><button disabled class="btn btn-red"><i class="fa fa-times"></i> <?= $this->lang->line('failed') ?></button></center>
                              </td>
                              <?php
                                 } else if ($course['status_lulus_terakhir'] == 'timeout' && $course['jumlah_kesempatan'] <= $course['percobaan'])
                                 {
                              ?>
                              <td>
                                 <center><button disabled class="btn btn-red"><i class="fa fa-times"></i> <?= $this->lang->line('timeout') ?></button></center>
                              </td>
                              <?php
                                 } else if ( ($course['status_lulus_terakhir'] == 'failed' && $course['jumlah_kesempatan'] > $course['percobaan']) )
                                 {
                              ?>
                              <td>
                                 <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room-quiz/<?= $course['id'];?>/n/<?=$course['id_episode'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue btnToUjroh"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_quiz') ?></button></center>
                              </td>
                              <?php
                                 } else if ( ($course['status_lulus_terakhir'] == 'timeout' && $course['jumlah_kesempatan'] > $course['percobaan']) )
                                 {
                              ?>
                              <td>
                                 <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room-quiz/<?= $course['id'];?>/n/<?=$course['id_episode'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue btnToUjroh"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_quiz') ?></button></center>
                              </td>
                              <?php
                                 }
                           } else
                           {
                              ?>
                              <td>
                                 <!-- <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="yes" data-href="<?=base_url();?>create-room-quiz/<?=$course['id'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-yellow btnToUjroh" onclick="window.location.href = '<?= base_url();?>create-room-quiz/<?= $course['id'];?>/y/<?=$course['id_episode'];?>'"><i class="fa fa-flag"></i> <?= $this->lang->line('continue_quiz') ?></button></center> -->
                                 <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="yes" data-href="<?=base_url();?>create-room-quiz/<?=$course['id'];?>/y/<?=$course['id_episode'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-yellow btnToUjroh"><i class="fa fa-flag"></i> <?= $this->lang->line('continue_quiz') ?></button></center>
                                 <!-- <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room-quiz/<?= $course['id'];?>/n/<?=$course['id_episode'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue btnToUjroh"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_quiz') ?></button></center> -->

                              </td>
                           <?php
                           }
                        } else {
                        ?>
                           <td>
                              <center>
                                 <button disabled class="btn btn-gray">
                                    <i class="fa fa-warning"></i> <?= $this->lang->line('quiz_not_avaiable') ?></a>
                              </center>
                           </td>
                        <?php
                        }
                        ?>
                  </tr>
                  <?php
                     }
                  ?>
               </tbody>
            </table>
         </div>
         </div>
      </div>
   </div>
</section>
<div class="modal fade" id="modalUjroh" tabindex="-1" role="dialog">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
</div>
<div class="modal-body">
<div class="row no-gutters">
<div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
<div class="quickview-content">
<h2><?= $this->lang->line('quiz_rules') ?></h2>
<hr>
<div class="size">
<div class="row">
<div class="col-lg-4 col-4">
<h5 class="title"><?= $this->lang->line('time') ?></h5>
<label for="" id="exam_time">...</label>
</div>
<div class="col-lg-4 col-4">
<h5 class="title">Passing Grade</h5>
<label for="" id="exam_pg">...</label>
</div>
<div class="col-lg-4 col-4">
<h5 class="title"><?= $this->lang->line('timeout_act') ?></h5>
<label for="" id="exam_timeout">...</label>
</div>
</div>
<br>
<div class="row">
<div class="col-lg-12 col-12">
<p><?= $this->lang->line('connection_desc_quiz') ?></p>
</div>
</div>
<br>
<label for="" style="color:red"><?= $this->lang->line('by_clicking_quiz') ?></label>
<br>
<br>
<center>
<a id="hrefToExam" href="" onclick="" class="btn btn-blue" style="color:white">OK</a>
</center>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--/ End Product Style 1  -->
<script>
   $(document).ready(function(){

     $(document).on('click', '.btnToUjroh', function(){
       var currRoom  = '<?php echo $room;?>'
       var idExam    = $(this).data('id_exam')
       var isContinue  = $(this).data('continue')
      //  if (isContinue == 'yes')
      //  {
      //    window.location.replace('<?=base_url();?>')
      //  }
       if (currRoom == 'none')
       {
         $('#modalUjroh').modal('show')
         $('#exam_time').html($(this).data('time')+' Minutes')
         $('#exam_pg').html($(this).data('pg')+' Score')
         var act = $(this).data('timeout').replace('_',' ')
         act = act.toUpperCase()
         $('#exam_timeout').html(act)
         var link = $(this).data('href')
         var href = "window.open('"+link+")"
         $('#hrefToExam').attr('href', $(this).data('href'))
         // $('#hrefToExam').attr('target', 'popup')
         // $('#hrefToExam').attr('onclick', "window.open('"+link+"','name','width=600,height=400');window.location.replace('<?=base_url();?>')")
         // location.reload()
       } else
       {
         if (currRoom != idExam)
         {
           Swal.fire({
             icon: 'info',
             title: 'Oops...',
             text: 'You have unfinished exam or quiz, finish first exam or quiz to take this quiz!',
           })
         } else
         {
           // var isContinue  = $(this).data('continue')
           // if (isContinue == 'no')
           // {
           //   sessionStorage.clear()
           // }
           $('#modalUjroh').modal('show')
           $('#exam_time').html($(this).data('time')+' Minutes')
           $('#exam_pg').html($(this).data('pg')+' Score')
           var act = $(this).data('timeout').replace('_',' ')
           act = act.toUpperCase()
           $('#exam_timeout').html(act)
           var link = $(this).data('href')
           var href = "window.open('"+link+")"
           $('#hrefToExam').attr('href', $(this).data('href'))
           // $('#hrefToExam').attr('target', 'popup')
         //   $('#hrefToExam').attr('onclick', "window.open('"+link+"','name','width=600,height=400');window.location.replace('<?=base_url();?>')")
           // window.focus();
           // location.reload();
         }
       }
     })

     function goToExam()
     {

     }



   })
</script>
