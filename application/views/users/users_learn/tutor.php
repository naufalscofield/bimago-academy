<br>
  <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="comments" id="comments">
            <div class="row comment">
              <div class="col-md-3 col-lg-2 text-center text-md-center">
                <p><img src="<?=base_url().'assets/avatar/'.$tutor['avatar'];?>" alt="" class="img-fluid rounded-circle"></p>
              </div>
              <div class="col-md-9 col-lg-10">
                <h5><?=$tutor['nama_depan'].' '.$tutor['nama_belakang'];?></h5>
              </div>
              <br>
              <br>
              <br>
              <div id="all" style="height:800px; overflow:scroll;">
              <div class="col-md-12 col-lg-12" style="margin-top:20px">
                <p align="justify">
                  <blockquote> <i class="fa fa-quote-left"></i>
                    <?= $tutor['profil'];?>
                  </blockquote>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
