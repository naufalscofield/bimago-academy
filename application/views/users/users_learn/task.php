<section class="shop-home-list section" style="height:280px; overflow:scroll;margin-top:-20px">
	<br>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<!-- Start Single List  -->
						<table class="table table-hover table-striped">
							<tr>
								<th width="5%" style="background:#b9b7b7">No</th>
								<th width="35%" style="background:#b9b7b7"><?= $this->lang->line('task');?></th>
								<th width="20%" style="background:#b9b7b7"><?= $this->lang->line('download');?></th>
								<th width="20%" style="background:#b9b7b7"><?= $this->lang->line('upload_assignment');?></th>
								<th width="20%" style="background:#b9b7b7"><?= $this->lang->line('score');?></th>
							</tr>
							<?php if ($task->num_rows()): ?>
								<?php foreach ($task->result() as $key => $value): ?>
									<tr>
										<td><?= $key+1 ?></td>
										<td><?= $value->nama_file; ?></td>
										<td>
											<a style="color:white;" id='<?= $value->id ?>' class="btn btn-sm onPreview ladda-button ladda-button_<?= $value->id ?>" data-style="expand-left"><i class="fa fa-download"></i></a>
										</td>
										<td>

											<?php if (isset($value->id_user_task) || $value->id_user_task != NULL)
										{
											?>
												<label for="" style="color:green"><?= $this->lang->line('has_upload_task'); ?></label>
												<!-- <button class="btn btn-detail-done" style="width:100%">Detail</button> -->
												<button title="Detail" style="width:100%" id='<?= $value->id_user_task ?>' class="btn btn-sm onPreviewTask ladda-button ladda-button_<?= $value->id_user_task ?>" data-style="expand-left"><i class="fa fa-eye"></i> Detail</button>
											<?php
										} else
										{
											?>
											<form action="<?= base_url();?>users/user-upload-task" method="POST" enctype="multipart/form-data">
												<input type="hidden" name="no" value="<?= $key+1; ?>">
												<input type="hidden" name="id_tugas" value="<?= $value->id; ?>">
												<input type="hidden" name="id_course" value="<?= $id_course; ?>">
												<input type="hidden" name="id_type_course" value="<?= $id_type_course; ?>">
												<input type="hidden" name="id_episode" value="<?= $id_episode; ?>">
												<input required type="file" class="form-control" name="file">
												<input type="submit" style="width:100%" class="btn btn-success" name="Upload">
											</form>
											<?php
										}
										?>
										</td>
										<td>
											<?php 
												if (isset($value->id_user_task) || $value->id_user_task != NULL)
												{
													if (!empty($value->nilai))
													{
														echo '<span class="badge badge-success" style="font-size:150%">'.$value->nilai.'</span>';
													} else
													{
														echo $this->lang->line('lecturer_hasnt_score');
													}
												} 
												else
												{
													echo '-';
												}
											?>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
									<tr>
										<td colspan="4" class="text-center">Modul ini tidak terdapat tugas yang harus diselesaikan</td>
									</tr>
							<?php endif ?>
						</table>
					<!-- End Single List  -->
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_detail" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="row p-3">
					<div class="col-md-9 col-10">
					</div>
						<div class="col-md-3 col-2">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
						</div>
					</div> <hr>
					<div class="modal-body" style="height: 250px;">
						<div class="col-lg-12 col-md-12 col-12">
							
						</div>
				</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" onclick="report();">Submit</button>
					</div>
				</div>
			</div>
    	</div>
	</section>

	<script>
	
	$(".onPreview").click(function(event){
		var id = $(this).attr('id');
		var l = Ladda.create( document.querySelector('.ladda-button_'+id) );
		l.start();
		$.ajax({
			url: "<?=$get_pdf_preview;?>",
			method: "POST",
			data: {id:id},
			success: function(data){
				l.stop();
				var hasil = $.parseJSON(data);
				window.open(hasil["url"], '_blank');
			}
		});
	});
	$(".onPreviewTask").click(function(event){
		var id = $(this).attr('id');
		var l = Ladda.create( document.querySelector('.ladda-button_'+id) );
		l.start();
		$.ajax({
			url: "<?=$get_pdf_preview_user;?>",
			method: "POST",
			data: {id:id},
			success: function(data){
				l.stop();
				var hasil = $.parseJSON(data);
				window.open(hasil["url"], '_blank');
			}
		});
	});

	$(document).on('click', '.btn-detail-done', function(){
		$("#modal_detail").modal('show')
	})
</script>
