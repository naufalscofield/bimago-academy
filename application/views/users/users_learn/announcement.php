<div class="col-lg-12 text-center p-5">
<h4>
	<!-- Belum ada posting pengumuman -->
	<?= $this->lang->line('announcement') ?>
</h4>
<p>
	<!-- Instruktur belum menambahkan pengumuman apa pun ke kursus ini. Pengumuman berfungsi untuk memberi tahu Anda tentang pembaruan atau penambahan pada kursus. -->
	<?= $this->lang->line('instructor') ?>
</p>
</div>