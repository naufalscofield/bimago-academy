<!DOCTYPE html>
<html lang="zxx">
<head>
	<!-- Meta Tag -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name='copyright' content=''>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title Tag  -->
    <title><?= env('APP_NAME') ?></title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?= getIco(); ?>" />
	<!-- Web Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

	<!-- StyleSheet -->

  <!-- StyleSheet -->
	<link rel="stylesheet" href="<?=base_url();?>assets/swal/sweetalert2.min.css">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/bootstrap-v5.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/font-awesome.css">
	<!-- Fancybox -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery.fancybox.min.css">
	<!-- Themify Icons -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/themify-icons.css">
	<!-- Jquery Ui -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/jquery-ui.css">
	<!-- Nice Select CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/niceselect.css">
	<!-- Animate CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/animate.css">
	<!-- Flex Slider CSS -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/flex-slider.min.css">
	<!-- Slicknav -->
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/slicknav.min.css">
    <link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/custom.css">

	<!-- Eshop StyleSheet -->
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/reset-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/style-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/eshop/eshop/css/responsive-v5.css">
	<link rel="stylesheet" href="<?= base_url();?>assets/videojs/css/video-js.css">

	<script src="<?= base_url();?>assets/eshop/eshop/js/jquery.min.js"></script>
	<script src="<?= base_url();?>assets/videojs/js/videojs-ie8.min.js"></script>
	<script src="<?= base_url();?>assets/devtools/index.js"></script>

  <!-- Ladda -->
  <link rel="stylesheet" href="<?=base_url();?>assets/ladda/ladda-themeless.min.css">
  <script src="<?=base_url();?>assets/ladda/spin.min.js"></script>
  <script src="<?=base_url();?>assets/ladda/ladda.min.js"></script>
  <script src="<?=base_url();?>assets/swal/sweetalert2.min.js"></script>
	<style>
    .nav-link{
      /* background-color: #b9b7b7; */
      margin-bottom: 6px;
      margin-top: 6px;
    }

    /* width */
    .nice_view
    ::-webkit-scrollbar {
    width: 10px;
    }

    /* Track */
    .nice_view
    ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
    }

    /* Handle */
    .nice_view
    ::-webkit-scrollbar-thumb {
    background: #0c60af;
    border-radius: 10px;
    }

		.header.shop .nav li a{
			color: #fff;
			text-transform: capitalize;
			font-size: 15px;
			padding: 20px 15px;
			/* font-weight: 500; */
			display: block;
			position: relative;
			}
			.header.shop .nav li{
			color: red;
			}
			.header.sticky .header-inner{
			position:fixed;
			top:0;
			left:0;
			background:#333;
			animation: fadeInDown 1s both 0.2s;
			-webkit-box-shadow:0px 0px 10px rgba(0, 0, 0, 0.3);
			-moz-box-shadow:0px 0px 10px rgba(0, 0, 0, 0.3);
			box-shadow:0px 0px 10px rgba(0, 0, 0, 0.3);
			z-index:999;
			}
			.header.shop .nav > li > a:hover {
			/*text-decoration: none;
			font-weight: bold;
			border-radius:0px;
			border-bottom-width: 4px;
			border-bottom-style: solid;
			border-bottom-color: #FF6600;
			color: #000 !important;*/
			background-color: transparent !important;

			}
			.header.shop .logo {
			-webkit-box-shadow: 10px 0px 0px -9px #e0e0e0;
				-moz-box-shadow: 10px 0px 0px -9px #e0e0e0;
			box-shadow: 10px 0px 0px -9px #e0e0e0;
			}
      .btn-back-home{
				display: none;
			}
      .modules-mob {
        display: none;
      }

      @media only screen and (max-width: 450px) {
  			.modules-mob{
  				display: block;
  			}
        .modules-web{
  				display: none;
  			}
			}

			@media only screen and (max-width: 1400px) {
			.hide-logo{
				display: none;
			}
      .btn-back-home{
				display: inline-block;
			}
			}
			.header.sticky .header-inner .nav li a {
			color:white;
			}
			.header.shop .search-bar input {
			width: 470px;
			}
			#sidebar-wrapper {
			border-style: solid;
			margin-left: -250px;
			left: 0;
			width: 250px;
			background: rgb(255 255 255);
			position: absolute;
			height: 70%;
			overflow-y: auto;
			z-index: 1000;
			transition: all 0.5s ease-in 0s;
			-webkit-transition: all 0.5s ease-in 0s;
			-moz-transition: all 0.5s ease-in 0s;
			-ms-transition: all 0.5s ease-in 0s;
			-o-transition: all 0.5s ease-in 0s;
			}
			#sidebar-wrapper-right {
			border-style: dotted;
			margin-right: -250px;
			right: 0;
			width: 250px;
			background: rgb(255 255 255);
			position: absolute;
			height: 70%;
			overflow-y: auto;
			z-index: 1000;
			transition: all 0.5s ease-in 0s;
			-webkit-transition: all 0.5s ease-in 0s;
			-moz-transition: all 0.5s ease-in 0s;
			-ms-transition: all 0.5s ease-in 0s;
			-o-transition: all 0.5s ease-in 0s;
			}

			.sidebar-nav {
			position: absolute;
			top: 0;
			width: 250px;
			list-style: none;
			margin: 0;
			padding: 0;
			}

			.sidebar-nav li {
			line-height: 50px;
			text-indent: 20px;
			}

			.sidebar-nav li a {
			color: #999999;
			display: block;
			text-decoration: none;
			}

			.sidebar-nav li a:hover {
			color: #fff;
			background: rgb(255 255 255);
			text-decoration: none;
			}

			.sidebar-nav li a:active, .sidebar-nav li a:focus {
			text-decoration: none;
			}

			.sidebar-nav > .sidebar-brand {
			height: 55px;
			line-height: 55px;
			font-size: 18px;
			}

			.sidebar-nav > .sidebar-brand a {
			color: #999999;
			}

			.sidebar-nav > .sidebar-brand a:hover {
			color: #fff;
			background: none;
			}

			#sidebar-wrapper.active {
			left: 250px;
			width: 350px;
			transition: all 0.5s ease-out 0s;
			-webkit-transition: all 0.5s ease-out 0s;
			-moz-transition: all 0.5s ease-out 0s;
			-ms-transition: all 0.5s ease-out 0s;
			-o-transition: all 0.5s ease-out 0s;
			}
			#sidebar-wrapper-right.active {
			right: 250px;
			width: 300px;
			transition: all 0.5s ease-out 0s;
			-webkit-transition: all 0.5s ease-out 0s;
			-moz-transition: all 0.5s ease-out 0s;
			-ms-transition: all 0.5s ease-out 0s;
			-o-transition: all 0.5s ease-out 0s;
			}

			.toggle {
			margin: 2px -42px 0 0;
			}
      .toggle-modules {
			margin: 17px -38px 0 0;
			}
			#menu-toggle {
			position: absolute;
			z-index: 1;
			}
			#menu-toggle-right {
			position: absolute;
			z-index: 1;
			right: 0;
			}
			.full-height {
			    height: 100vh;
			    background-color: black;
			}
			.flex-center {
			    align-items: center;
			    display: flex;
			    justify-content: center;
			}
			.position-ref {
			    position: relative;
			}
			.top-right {
			    position: absolute;
			    right: 10px;
			    top: 18px;
			}
			.content {
			    text-align: center;
			}
			.title {
			    font-size: 84px;
			}
			.links > a {
			    color: #636b6f;
			    padding: 0 25px;
			    font-size: 12px;
			    font-weight: 600;
			    letter-spacing: .1rem;
			    text-decoration: none;
			    text-transform: uppercase;
			}
			.m-b-md {
			    margin-bottom: 30px;
			}

      .back-col {
        background-color: white;
      }

      .rounded-lg {
        border-radius: 1rem;
      }

      .text-gray {
        color: #aaa;
      }

      div.h4 {
        line-height: 1rem;
      }

	</style>
</head>
<body class="js" style="background:white;">
	<!-- Breadcrumbs -->
	<div class="breadcrumbs pt-3 pb-3" style="background-color:#28303b;">
    <div class="row no-gutters">
      <div class="col-lg-2 hide-logo" style="width:">
        <div class="all-category">
          <div class="logo pl-2 pr-0" style="border-right: 1px solid grey;margin-left:25px">
              <a href="<?= base_url(); ?>"><img src="<?= base_url();?>uploads/lms_config/<?= $info->logo_putih;?>" alt="logo" style="max-width: 200px"></a>
          </div>
        </div>
      </div>

      <div class="col-lg-8 col-md-8 col-sm-5 my-auto">
        <div class="container">
          <div class="row">
            <div class="col-2 my-auto btn-back-home" style="color:white;border-right: 1px solid grey;">
              <center>
                <a alt="Back to my course" class="mr-2" href="<?= base_url().'users/users_mycourse';?>" style="font-size: 23px;"><i style="size:30px;color:white" class="fa fa-arrow-left"></i></a>
              </center>
            </div>
            <div class="col-10">
              <div class="menu-area">
                <div class="navbar-collapse">
                  <div class="nav-inner">
                    <ul class="nav main-menu menu navbar-nav">
                      <li>
                        <a class="" style="color:white;"><?= isset($course->judul) ? $course->judul : '';  ?></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>

	<!-- End Breadcrumbs -->

<div class="row no-gutters">
  <div class="modal" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
        </div>
        <div class="modal-body">
          <div class="row no-gutters">
            <div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
              <div class="quickview-content">
                <h2><?= $this->lang->line('exam_rules') ?></h2>
                <hr>
                <div class="size">
                  <div class="row">
                    <div class="col-lg-4 col-4">
                      <h5 class="title"><?= $this->lang->line('time') ?></h5>
                      <label for="" id="exam_time2">...</label>
                    </div>
                    <div class="col-lg-4 col-4">
                      <h5 class="title">Passing Grade</h5>
                      <label for="" id="exam_pg2">...</label>
                    </div>
                    <div class="col-lg-4 col-4">
                      <h5 class="title"><?= $this->lang->line('timeout_act') ?></h5>
                      <label for="" id="exam_timeout2">...</label>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-lg-12 col-12">
                      <p><?= $this->lang->line('connection_desc') ?></p>
                    </div>
                  </div>
                  <br>
                  <label for="" style="color:red"><?= $this->lang->line('by_clicking') ?></label>
                  <br>
                  <br>
                  <center>
                    <a id="hrefToExamModal" href="" onclick="" class="btn btn-blue2" style="color:white">OKE</a>
                  </center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="reportModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row p-3">
                  <div class="col-md-9 col-10">
                  <h4><i class="fa fa-flag"></i> <?= $this->lang->line('report'); ?></h4>
                  </div>
                  <div class="col-md-3 col-2">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
                  </div>
                </div> <hr>
                <div class="modal-body" style="height: 250px;">
                  <div class="col-lg-12 col-md-12 col-12">
                    <div class="mb-1">
                      <input class="reportCbx" type="checkbox" name="report[]" value="copyright"> <b><?= $this->lang->line('copyright'); ?></b> ( <?= $this->lang->line('copyright_detail'); ?>) <br>
                    </div>
                    <div class="mb-1">
                      <input class="reportCbx" type="checkbox" name="report[]" value="pornography"> <b><?= $this->lang->line('pornography'); ?></b> ( <?= $this->lang->line('pornography_detail'); ?>)<br>
                    </div>
                    <div class="mb-1">
                      <input class="reportCbx" type="checkbox" name="report[]" value="plagiarism"> <b><?= $this->lang->line('plagiarism'); ?></b> ( <?= $this->lang->line('plagiarism_detail'); ?>)<br>
                    </div>
                    <div class="mb-1">
                      <input class="reportCbx" type="checkbox" name="report[]" value="violence"> <b><?= $this->lang->line('violence'); ?></b> ( <?= $this->lang->line('violence_detail'); ?>)<br>
                    </div>
                    <div class="mb-1">
                      <input class="reportCbx" type="checkbox" name="report[]" value="broken_video"> <b>Broken Video</b> ( Video Cannot be load)<br>
                    </div>
                  </div>
                  <br>
                  <div class="mb-1 ml-3 mr-3">
                    <label for=""><b><?= $this->lang->line('description'); ?></b></label>
                  	<textarea name="report_desc" cols="10" rows="4"></textarea>
                  </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="report();">Submit</button>
                </div>
            </div>
        </div>
    </div>
  <!-- ==================================================================================LEFT -->
  <div class="col-md-3 modules-web" style="background: #e4e4e4; padding-right:0px;">

    <style>
      no-bullet.ul {
      list-style-type: none;
    }

      .circle-green {
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 50%;
        font-size: 15px;
        background: #228B22;
        text-align: center;
        color: #fff
      }
      .circle-red {
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 50%;
        font-size: 15px;
        background: #c81c1d;
        text-align: center;
        color: #fff
      }

      .btn-blue2 {
      background-color: #0c60af;
      border-color: #0c60af;
      color: white;
      border-radius: 2px;
      }
      .btn-blue2:hover {
      background-color: #87CEFA;
      border-color: #87CEFA;
      }
      .btn-gray2 {
      background-color: #C0C0C0;
      border-color: #C0C0C0;
      color: white;
      border-radius: 2px;
      }
      .btn-gray2:hover {
      background-color: #C0C0C0;
      border-color: #C0C0C0;
      }
      .btn-red2 {
      background-color: #c81c1d;
      border-color: #c81c1d;
      color: white;
      border-radius: 2px;
      }
      .btn-red2:hover {
      background-color: #c81c1d;
      border-color: #c81c1d;
      }
      .btn-green2 {
      background-color: #228B22;
      border-color: #228B22;
      color: white;
      border-radius: 2px;
      }
      .btn-green2:hover {
      background-color: #3CB371;
      border-color: #3CB371;
      }
      .btn-yellow2 {
      background-color: #FFD700;
      color: white;
      border-radius: 2px;
      border-color: #FFD700;
      }
      .btn-yellow2:hover {
      background-color: #F0E68C;
      border-color: #F0E68C;
      }

      .card{
      background-color: #fff;
      height: auto;
      width: auto;
      overflow: hidden;
      margin: 12px;
      border-radius: 5px;
      box-shadow: 9px 17px 45px -29px
      rgba(0, 0, 0, 0.44);
      }
      .card__image img {
      width: 100%;
      height: 100%;
      }
      .card__image.loading {
      height: 200px;
      width: 400px;
      }
      .card__title {
      padding: 8px;
      font-size: 22px;
      font-weight: 700;
      }
      .card__title.loading {
      height: 1rem;
      width: 50%;
      margin: 1rem;
      border-radius: 3px;
      }
      .card__description {
      padding: 8px;
      font-size: 16px;
      }
      .card__description.loading {
      height: 3rem;
      margin: 1rem;
      border-radius: 3px;
      }
      .loading {
      position: relative;
      background-color: #e2e2e2;
      }
      .loading::after {
      display: block;
      content: "";
      position: absolute;
      width: 100%;
      height: 100%;
      transform: translateX(-100%);
      background: -webkit-gradient(linear, left top,
      right top, from(transparent),
      color-stop(rgba(255, 255, 255, 0.2)),
      to(transparent));
      background: linear-gradient(90deg, transparent,
      rgba(255, 255, 255, 0.2), transparent);
      animation: loading 0.8s infinite;
      }
      @keyframes loading {
      100% {
      transform: translateX(100%);
      }
      }
      .pagination a {
      color: black;
      float: center;
      padding: 8px 16px;
      text-decoration: none;
      transition: background-color .3s;
      }
      .pagination a.active {
      background-color: dodgerblue;
      color: white;
      }
      .pagination a:hover:not(.active) {background-color: #ddd;}
      .modal-dialog {
      width:100%
      }
      .modal-content {
      height: 100%;
      width: 100%;
      }
      .modal-body {
      overflow-y: scroll;
      }

      .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
        color: #fff !important;
      }
    </style>
    <center>
    <!-- Accordion card -->
    <div class="card">
      <!-- Card header -->
      <div class="card-header" role="tab" id="headingProgres">
        <a data-toggle="collapse" data-parent="#accordionExProgres" href="#collapseProgres" aria-expanded="true"
          aria-controls="collapseProgres">
          <h6 class="mb-0">
            <?= $this->lang->line('modules');?> <i class="fa fa-angle-down rotate-icon"></i>
            <br>
          </h6>
        </a>
      </div>

      <!-- Card body -->
      <div id="collapseProgres" class="collapse show" role="tabpanel" aria-labelledby="headingProgres"
        data-parent="#accordionExProgres">
        <div class="card-body">
          <span><?= $this->lang->line('overall_prog');?></span>
          <div class="progress">
            <div class="progress-bar" id="progress-bar-video" role="progressbar" style="width: <?=$progres_all;?>%;" aria-valuenow="<?=$progres_all;?>" aria-valuemin="0" aria-valuemax="100"><span id="progres-all-view"><?=$progres_all;?></span>%</div>
          </div><br>

          <table class="table table-hover">
            <?php if ($num_episode > 0): ?>
              <?php $no=0; foreach ($episode as $key => $value) { $no++; ?>
                <tr>
                  <!-- <td width="5px"><input type="checkbox"></td> -->
                  <td style="height:100px">
                    <a href="<?= base_url('users/users_learn/learn/'.encode_url($id_course).'/'.encode_url($id_type_course).'/'.encode_url($value['id']));  ?>"><i class="fa fa-play-circle <?= $id_episode == $value['id'] ? 'text-success' : ''; ?>"></i> <?=$no;?>. <?=$value['judul'] ?> (<span id="persen-view_<?=$value['id'];?>"><?=$value['progres_vid'];?></span>%)</a><br>

                    <?php if (!empty($value['nilai'])) { ?>
                      <?php if ($value['rating_belakang'] == '') { ?>
                        <?php $not = 5 - $value['nilai'];
                        for ($i=0; $i < $value['nilai']; $i++) { ?>
                          <span class="fa fa-star checked"></span>
                        <?php } ?>
                        <?php for ($i=0; $i < $not; $i++) { ?>
                          <span class="fa fa-star-o"></span>
                        <?php } ?>
                      <?php }else{ ?>
                        <?php $not = 4 - $value['rating_depan'];
                        for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                          <span class="fa fa-star checked"></span>
                        <?php } ?>
                        <span class="fa fa-star-half-o checked"></span>
                        <?php for ($i=0; $i < $not; $i++) { ?>
                          <span class="fa fa-star-o"></span>
                        <?php } ?>
                      <?php } ?>
                    <?php }else{ ?>
                      <?php for ($i=0; $i < 5 ; $i++) { ?>
                        <span class="fa fa-star-o"></span>
                      <?php } ?>
                    <?php } ?>
                    (<?=$value['reviewer'];?>)<br>
                    <!-- Video : <br>
                    <div class="progress">
                      <div class="progress-bar" id="progress-bar-video_<?=$value['id'];?>" role="progressbar" style="width: <?=$value['progres_vid'];?>%;" aria-valuenow="<?=$value['progres_vid'];?>" aria-valuemin="0" aria-valuemax="100"><span id="persen-view_<?=$value['id'];?>"><?=$value['progres_vid'];?></span>%</div>
                    </div> -->
                    <!-- Quiz : <br>
                      <?php if ($value['quiz'] == '') {
                        $quiz = '<i class="fa fa-hourglass"></i> Not yet';
                      }elseif($value['quiz'] == 'passed'){
                        $quiz = '<i class="fa fa-check-circle"></i> Passed';
                      }elseif($value['quiz'] == 'failed'){
                        $quiz = '<i class="fa fa-times-circle"></i> Failed';
                      }elseif($value['quiz'] == 'timeout'){
                        $quiz = '<i class="fa fa-times-circle"></i> Timeout';
                      } ?>
                      <span><?=$quiz;?></span> -->
                  </td>
                </tr>
              <?php } ?>
            <?php endif ?>
            <td style="height:100%">
              <?php if ($exam->num_rows() > 0){ ?>
                <?php foreach ($exam->result_array() as $key => $course) { ?>
                      <?php
                      if ($course['id_exam'] != NULL) {
                         if ($course['is_running'] == "0" || $course['is_running'] == 0) {
                            if ($course['status_lulus_terakhir'] == NULL) { ?>
                              <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room/<?= $course['id'];?>/n" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue2 btnToUjroh2"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_exam') ?></button></center>
                            <?php }
                            else if ($course['status_lulus_terakhir'] == 'passed') { ?>
                              <center><button disabled class="btn btn-green2"><i class="fa fa-check"></i> <?= $this->lang->line('passed') ?></button></center>
                            <?php }
                            else if ($course['status_lulus_terakhir'] == 'failed' && $course['jumlah_kesempatan'] <= $course['percobaan']) { ?>
                              <center><button disabled class="btn btn-red2"><i class="fa fa-times"></i> <?= $this->lang->line('failed') ?></button></center>
                            <?php }
                            else if ($course['status_lulus_terakhir'] == 'timeout' && $course['jumlah_kesempatan'] <= $course['percobaan']) { ?>
                              <center><button disabled class="btn btn-red2"><i class="fa fa-times"></i> <?= $this->lang->line('timeout') ?></button></center>
                            <?php }
                            else if ( ($course['status_lulus_terakhir'] == 'failed' && $course['jumlah_kesempatan'] > $course['percobaan']) ) { ?>
                              <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room/<?= $course['id'];?>/n" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue2 btnToUjroh2"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_exam') ?></button></center>
                            <?php }
                            else if ( ($course['status_lulus_terakhir'] == 'timeout' && $course['jumlah_kesempatan'] > $course['percobaan']) ) { ?>
                              <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="no" data-href="<?=base_url();?>create-room/<?= $course['id'];?>/n" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-blue2 btnToUjroh2"><i class="fa fa-pencil"></i> <?= $this->lang->line('take_exam') ?></button></center>
                            <?php }
                          } else { ?>
                               <center><button data-id_exam="<?=$course['id_exam'];?>" data-continue="yes" data-href="<?=base_url();?>create-room/<?=$course['id'];?>" data-time="<?=$course['waktu'];?>" data-pg="<?=$course['passing_grade'];?>" data-timeout="<?=$course['aksi_waktu_habis'];?>" style="color:white" class="btn btn-yellow2 btnToUjroh2" onclick="window.open('<?= base_url();?>create-room/<?= $course['id'];?>/y','_blank','name','width=600,height=400;')"><i class="fa fa-flag"></i> <?= $this->lang->line('continue_exam') ?></button></center>
                         <?php }
                      } else { ?>
                           <center>
                              <button disabled class="btn btn-gray2">
                                 <i class="fa fa-warning"></i> <?= $this->lang->line('exam_not_avaiable') ?></a>
                           </center>
                     <?php } ?>
                <?php } ?>
              <?php } ?>
            </td>
          </table>

        </div>
      </div>
    </div>
  </center>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog">
       <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
             </div>
             <div class="modal-body">
                <div class="row no-gutters">
                   <div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
                      <div class="quickview-content">
                         <h2><?= $this->lang->line('exam_rules') ?></h2>
                         <hr>
                         <div class="size">
                            <div class="row">
                               <div class="col-lg-4 col-4">
                                  <h5 class="title"><?= $this->lang->line('time') ?></h5>
                                  <label for="" id="exam_time2">...</label>
                               </div>
                               <div class="col-lg-4 col-4">
                                  <h5 class="title">Passing Grade</h5>
                                  <label for="" id="exam_pg2">...</label>
                               </div>
                               <div class="col-lg-4 col-4">
                                  <h5 class="title"><?= $this->lang->line('timeout_act') ?></h5>
                                  <label for="" id="exam_timeout2">...</label>
                               </div>
                            </div>
                            <br>
                            <div class="row">
                               <div class="col-lg-12 col-12">
                                  <p><?= $this->lang->line('connection_desc') ?></p>
                               </div>
                            </div>
                            <br>
                            <label for="" style="color:red"><?= $this->lang->line('by_clicking') ?></label>
                            <br>
                            <br>
                            <center>
                               <a id="" href="" target="" onclick="" class="btn btn-blue2" style="color:white">OK</a>
                            </center>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>

    <script type="text/javascript">
    $(document).ready(function(){

      $(document).on('click', '.btnToUjroh2', function(){
        var currRoom  = '<?php echo $room;?>'
        var idExam    = $(this).data('id_exam')
        var isContinue  = $(this).data('continue')
        if (isContinue == 'yes')
        {
          window.location.replace('<?php echo base_url();?>')
        }
        if (currRoom == 'none')
        {
          $('#modal').modal('show')
          $('#exam_time2').html($(this).data('time')+' Minutes')
          $('#exam_pg2').html($(this).data('pg')+' Score')
          var act = $(this).data('timeout').replace('_',' ')
          act = act.toUpperCase()
          $('#exam_timeout2').html(act)
          var link = $(this).data('href')+"','_blank"
          var href = "window.open('"+link+")"
          $('#hrefToExamModal').attr('href', $(this).data('href'))

        } else
        {
          if (currRoom != idExam)
          {
            Swal.fire({
              icon: 'info',
              title: 'Oops...',
              text: 'You have unfinished exam, finish first exam to take this exam!',
            })
          } else
          {
            // var isContinue  = $(this).data('continue')
            // if (isContinue == 'no')
            // {
            //   sessionStorage.clear()
            // }
            $('#modal').modal('show')
            $('#exam_time2').html($(this).data('time')+' Minutes')
            $('#exam_pg2').html($(this).data('pg')+' Score')
            var act = $(this).data('timeout').replace('_',' ')
            act = act.toUpperCase()
            $('#exam_timeout2').html(act)
            var link = $(this).data('href')+"','_blank"
            var href = "window.open('"+link+")"
            $('#hrefToExamModal').attr('href', $(this).data('href'))

          }
        }
      })
    })
    </script>

    <div class="card">
    	<div class="card-header" role="tab" id="recomendCourse">
        <a data-toggle="collapse" data-parent="#accordionRecomendations" href="#collapseRecomendCourse" aria-expanded="true"
          aria-controls="collapseRecomendCourse">
          <h6 class="mb-0">
            <?= $this->lang->line('related_course'); ?> <i class="fa fa-angle-down rotate-icon"></i>
            <br>
          </h6>
          <?php
            if ($status_recommend == 'cb')
            {
          ?>
              <p><?= $this->lang->line('cb_related');?></p>
          <?php
            } else
            {
          ?>
              <p><?= $this->lang->line('ct_related');?></p>
          <?php
            }
          ?>
        </a>
      </div>

      <div id="collapseRecomendCourse" class="collapse show" role="tabpanel" aria-labelledby="headingProgres"
        data-parent="#accordionRecomendations">
        <div class="card-body" style="max-height: 500px; overflow-x: scroll; ">
        	<?php foreach ($recomedations as $rec): ?>
        	<a target="_blank" href="<?= base_url('users/users_course/detail/'.$rec['id']); ?>">
        	<div class="text-center pb-2">
	        	<img src="<?= base_url($rec['image']); ?>" style="height:200px; margin-left:-15px;" />
        	</div>
           <h6><?= $rec['judul']; ?></h6>
           <br>  
           <p style="font-size:5px;color:red"><?= $rec['deskripsi_singkat']; ?></p>
         </a>
        	<hr>
        	<?php endforeach ?>
       	</div>
       </div>
       
     </div>
  </div>

  <!-- ==================================================================================RIGHT -->
  <div class="col-md-9" style="padding-left:0px;">
    <section style="overflow:hidden;background-color:black;">
    <!-- <div style="background-color: black;"> -->
      <!-- <a id="menu-toggle" href="#" class="btn btn-primary btn-lg toggle modules"><i class="fa fa-list"></i></a> -->
      <!-- <a id="menu-toggle-right" href="#" class="btn btn-primary btn-lg toggle mr-0"><i class="fa fa-file-text"></i></a> -->
      <div class="container">
        <div class="row" style="margin-bottom:-22px;">
          <div class="col-xl-12 col-lg-12 col-md-12 mb-4 p-0 m-0" align="center">
            <?php if ($url_video != '' || $type != '') { ?>
              <?php if ($type == 'gdrive'): ?>
                <div class="wrapper">
	                <div style="max-width: 800px; max-height:auto">
	                  <video id='my-player' class='video-js vjs-default-skin vjs-big-play-centered' width='950' height='460'
	                  poster="" data-setup='{"controls" : true, "preload" : "auto" ,"fluid": true}'>
	                   <?php if (!empty($url_video)): ?>
	                    <source src='<?= getGDriveEmbedUrl($url_video);?>' type='video/mp4'>
	                    <?php elseif (!empty($video)): ?>
	                    <source src='<?= base_url($video);?>' type='video/mp4'>
	                  <?php endif ?>
	                    <p class='vjs-no-js'>
	                      To view this video please enable JavaScript, and consider upgrading to a web browser that
	                      <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
	                    </p>
	                  </video>
	                </div>
                </div>
              <?php elseif($type == 'pdf'): ?>
              <?php else: ?>
                <div>
                	<video style="width: 100%; height: 460px;left: 0px;top: 0px;"
								    id="my-player"
								    class='video-js vjs-default-skin vjs-big-play-centered'
								    controls
								    data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "<?= getYoutubeEmbedUrl($url_video); ?>"}] }'
								  >
								  </video>
                  <!-- <iframe src="<?= getYoutubeEmbedUrl($url_video); ?>?showinfo=0&rel=0&iv_load_policy=3&disablekb=1" width="950" height="460" allowfullscreen="allowfullscreen" frameborder="0" scrolling="no" seamless=""></iframe> -->
                </div>
              <?php endif ?>
            <?php }else{ ?>
              <img src="<?=base_url('assets/default/empty_video.png');?>" style="width:600px;height:400px" alt=""><br>
              <?= $this->lang->line('video_not_yet') ?>
            <?php } ?>
          </div>
          </div>
        </div>
    </section>

    <section style="background-color:white;">
      <div class="row" style="margin-right:0px">
        <div class="col-<?= !empty($urlPag['prevUrl']) && !empty($urlPag['nextUrl']) ? '7' : '9';?> my-auto" style="padding-right:0px;">
          <div class="text-left" style="padding-left:10px;padding-bottom: 10px;padding-top:10px;">
            <h5 class="d-inline"> <i class="fa fa-play-circle text-success pr-1"></i><?= $judul;?> </h5>
          </div>
          <div class="text-left" style="padding-left:10px;padding-bottom: 10px;">
           <a data-toggle="modal" data-target="#reportModal" style="cursor:pointer;">
              <i class="fa fa-flag"> </i> <?= $this->lang->line('report');?>
            </a>
          </div>
        </div>
        <div class="col-<?= !empty($urlPag['prevUrl']) && !empty($urlPag['nextUrl']) ? '5' : '3';?> my-auto" style="padding-left:0px">
          <div class="text-right" style="padding-right:0px; padding-left: 0px;padding-bottom: 10px; padding-top:10px;">
            <?php if (!empty($urlPag['prevUrl'])): ?>
              <a class="btn btn-danger" style="font-size:12px;border-radius:7%; padding: 5px 10px;" data-toggle="tooltip" title="<?= $urlPag['prevTitle'] ?>" data-placement="bottom" class="pr-1" href="<?= $urlPag['prevUrl']; ?>"> <i class="fa fa-angle-left"></i> <?= $this->lang->line('prev') ?></a>
            <?php endif ?>
            <?php if (!empty($urlPag['nextUrl'])): ?>
              <a class="btn btn-primary" style="font-size:12px; border-radius:7%;padding: 5px 10px;" data-toggle="tooltip" title="<?= $urlPag['nextTitle'] ?>" data-placement="bottom" href="<?= $urlPag['nextUrl']; ?>">&nbsp;<?= $this->lang->line('next') ?> <i class="fa fa-angle-right"></i></a>
            <?php endif ?>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row" style="background-color:#e4e4e4; padding-left:10px; padding-right:10px;">
          <div id="nav" class="tabsMain pt-3" style="overflow:auto">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="width:700px">
              <li class="nav-item">
                <a id="content" style="cursor:pointer;" class="nav-link modules-mob">Modules</a>
              </li>
              <li class="nav-item">
                <a id="overview_episode" style="cursor:pointer;" class="nav-link active"><?= $this->lang->line('overview') ?></a>
              </li>
              <li class="nav-item">
                <a id="tutor_video" style="cursor:pointer;" class="nav-link"><?= $this->lang->line('instructure');?></a>
              </li>
              <li class="nav-item">
                <a id="resource" style="cursor:pointer;" class="nav-link"><?= $this->lang->line('resource');?></a>
              </li>
              <li class="nav-item">
                <a id="task" style="cursor:pointer;" class="nav-link"><?= $this->lang->line('task');?></a>
              </li>
              <li class="nav-item">
                <a id="discuss" style="cursor:pointer;" class="nav-link"><?= $this->lang->line('comment');?></a>
              </li>
              <li class="nav-item">
                   <a id="quiz" style="cursor:pointer;" class="nav-link"><?= $this->lang->line('quiz') ?></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div id="content" class="pt-3 pb-3 rounded-lg" style="width: 100%; min-width: 300px; padding-top:0rem!important; padding:1rem!important;"></div>
        </div>
      </div>
    </section>
  </div>
</div>

<!-- Start Footer Area -->
<footer class="footer">
  <!-- Footer Top -->
  <div class="footer-top section">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-6 col-12">
          <!-- Single Widget -->
          <div class="single-footer about">
            <div class="logo">
              <a href="<?=base_url();?>"><img src="<?= base_url();?>uploads/lms_config/<?= $info->logo_putih;?>"  alt="logo" style="width: 260px;"></a>
            </div>
            <p class="text" style="font-size:12px"><?= $this->lang->line('comdesc') ?></p>
          </div>
          <!-- End Single Widget -->
        </div>
        <div class="col-lg-4 col-md-12 col-12">
          <!-- Single Widget -->
          <div class="single-footer social">
            <h4><?= $this->lang->line('get_in_touch') ?></h4>
            <!-- Single Widget -->
            <div class="contact">
              <ul>
                <li style="font-size:12px">Surapati Core J2, Jl. PHH Mustofa 39. <?=$this->lang->line('city');?></li>
                <li style="font-size:12px">info@solmit.academy</li>
              </ul>
            </div>
            <!-- End Single Widget -->
            <ul>
              <li><a href="https://web.facebook.com/solmitdotcom/?_rdc=1&_rdr"><i class="ti-facebook"></i></a></li>
              <li><a href="https://www.instagram.com/beeandants/"><i class="ti-instagram"></i></a></li>
              <li><a href="https://www.linkedin.com/company/pt.-solmit-bangun-indonesia/"><i class="ti-linkedin"></i></a></li>
              <li><a href="https://www.youtube.com/c/SOLMITACADEMY/featured"><i class="ti-youtube"></i></a></li>
            </ul>
          </div>
          <!-- End Single Widget -->
        </div>
      </div>
    </div>
  </div>
  		<!-- End Footer Top -->
  		<div class="copyright">
  			<div class="container">
  				<div class="inner">
  					<div class="row">
  						<div class="col-lg-6 col-12">
  							<div class="left">
  								<p>Copyright © <a href="http://www.solmit.com" target="_blank"><img style="width:20px;height:20px;" src="<?=base_url().'logo-footer.png';?>" alt=""> Bee & Ants Know</a>  -  2021</p>
  							</div>
  						</div>
  						<div class="col-lg-6 col-12">
  							<div class="right">
  							</div>
  						</div>
  					</div>
  				</div>
  			</div>
  		</div>
  	</footer>
  	<!-- /End Footer Area -->

	<!-- Jquery -->
  	<script src="<?= base_url();?>assets/eshop/eshop/js/jquery-migrate-3.0.0.js"></script>
	<script src="<?= base_url();?>assets/eshop/eshop/js/jquery-ui.min.js"></script>
	<!-- Popper JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/popper.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/bootstrap.min.js"></script>
	<!-- Color JS -->
	<!-- <script src="<?= base_url();?>assets/eshop/eshop/js/colors.js"></script> -->
	<!-- Slicknav JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/slicknav.min.js"></script>
	<!-- Owl Carousel JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/owl-carousel.js"></script>
	<!-- Magnific Popup JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/magnific-popup.js"></script>
	<!-- Waypoints JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/waypoints.min.js"></script>
	<!-- Countdown JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/finalcountdown.min.js"></script>
	<!-- Nice Select JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/nicesellect.js"></script>
	<!-- Flex Slider JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/flex-slider.js"></script>
	<!-- ScrollUp JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/scrollup.js"></script>
	<!-- Onepage Nav JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/onepage-nav.min.js"></script>
	<!-- Easing JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/easing.js"></script>
	<!-- Active JS -->
	<script src="<?= base_url();?>assets/eshop/eshop/js/active.js"></script>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
	<script src="<?= base_url();?>assets/eshop/eshop/js/custom.js"></script>

	<script src="<?= base_url();?>assets/videojs/js/video.js"></script>
	<script src="<?= base_url();?>assets/videojs/js/Youtube.js"></script>
	<script src="<?= base_url();?>assets/devtools/index.js"></script>

	<script>
    $(document).ready(function(){
		var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	= '<?php echo $this->session->flashdata("error");?>'
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}
	})

  var myPlayer = videojs('my-player');
  $(document).ready(function(){
    <?php if(!empty($waktu)){ ?>
     myPlayer.currentTime(<?=$waktu;?>);
    <?php } ?>
    myPlayer.controlBar.progressControl.disable();
  })
  var watchPoints = [];
  videojs('my-player').on("timeupdate", function(){
  var currentTimePlay = myPlayer.currentTime();
  var watchPoint = Math.floor((currentTimePlay/myPlayer.duration()) * 100);
  if(watchPoints.indexOf(watchPoint) == -1){
    if( watchPoint % 3 == 0 ) {
      $.ajax({
        type: 'POST',
        url: '<?= base_url('users/users_learn/persen_video')?>',
        data: { id_course : '<?=$id;?>', id_episode : '<?= $id_episode ?>', id_type_course : '<?= $id_type_course ?>', persentase : watchPoint, waktu : currentTimePlay.toFixed(2) },
        success: function(res) {
          var hasil = $.parseJSON(res);
          $('#persen-view_'+'<?=$id_episode;?>').html(hasil["persentase"]);
          $('#progres-all-view').html(hasil["persentase_all"]);
          $('#progress-bar-video').attr('aria-valuenow', hasil["persentase_all"]);
          $('#progress-bar-video').attr('style', 'width:'+hasil["persentase_all"]+'%');

          $('#persen-view-cont_'+'<?=$id_episode;?>').html(hasil["persentase"]);
          // $('#progress-bar-video_'+'<?=$id_episode;?>').attr('aria-valuenow', hasil["persentase"]);
          // $('#progress-bar-video_'+'<?=$id_episode;?>').attr('style', 'width:'+hasil["persentase"]+'%');
          $('#progres-view-cont').html(hasil["persentase_all"]);
          $('#progress-bar-video-cont').attr('aria-valuenow', hasil["persentase_all"]);
          $('#progress-bar-video-cont').attr('style', 'width:'+hasil["persentase_all"]+'%');
        },
      });
    }
    watchPoints.push(Math.floor(watchPoint))
  }
  });

// ====================================

	$("#nav li a").click(function() {
	    $("#nav li a").removeClass('active');
	    $(this).addClass('active');
	    $.ajax({
	      type: 'POST',
	      url: '<?= base_url('users/users_learn/tab')?>',
	      data: { id : this.id, id_course : '<?= $id ?>',id_episode : '<?= $id_episode ?>', id_type_course : '<?= $id_type_course ?>' },
	      dataType: "json",
	      success: function(html) {
	        $("div#content").html(html);
	      },
	    });
 	});
	$.ajax({
	      type: 'POST',
	      url: '<?= base_url('users/users_learn/tab')?>',
	      data: { id : 'overview_episode', id_course : '<?= $id ?>',id_episode : '<?= $id_episode ?>', id_type_course : '<?= $id_type_course ?>' },
	      dataType: "json",
	      success: function(html) {
	        $("div#content").html(html);
	      },
	    });
	document.addEventListener("contextmenu", function (e) {
        e.preventDefault();
    }, false);
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }

  // var stateElement = '';
	// stateElement = window.devtools.isOpen ? 'yes' : 'no';
	// if(stateElement == 'yes'){
	// 	$('body').empty();
	//     $('body').append('<div class="flex-center position-ref full-height"> <div class="top-right links"> </div> <div class="content"> <div class="title m-b-md"> <img style="width: 500px; height:auto;" src="<?= base_url('assets/devtools/not-allowed.png');?>" id="forbidden" alt=""> </div> <div class="links"> <a href="" class="text-danger">NOT ALLOWED TO OPEN !! PLEASE CLOSE</a> </div> </div> </div>');
	// }
	// window.addEventListener('devtoolschange', event => {
	// 	stateElement = event.detail.isOpen ? 'yes' : 'no';
	// 	if(stateElement == 'yes'){
	// 		$('body').empty();
	// 	    $('body').append('<div class="flex-center position-ref full-height"> <div class="top-right links"> </div> <div class="content"> <div class="title m-b-md"> <img style="width: 500px; height:auto;" src="<?= base_url('assets/devtools/not-allowed.png');?>" id="forbidden" alt=""> </div> <div class="links"> <a href="" class="text-danger">NOT ALLOWED TO OPEN !! PLEASE CLOSE</a> </div> </div> </div>');
	// 	}else{
	// 	}
  
	// });

	 // $("#menu-close").click(function(e) {
	 //    e.preventDefault();
	 //    $("#sidebar-wrapper").toggleClass("active");
	 //  });
	 //  $("#menu-toggle").click(function(e) {
	 //    e.preventDefault();
	 //    $("#sidebar-wrapper").toggleClass("active");
	 //  });
   //
	 //  $("#menu-close-right").click(function(e) {
	 //    e.preventDefault();
	 //    $("#sidebar-wrapper-right").toggleClass("active");
	 //  });
	 //  $("#menu-toggle-right").click(function(e) {
	 //    e.preventDefault();
	 //    $("#sidebar-wrapper-right").toggleClass("active");
	 //  });
	 //  $('[data-toggle="tooltip"]').tooltip();

   function report(){   	
   		var report_desc = $('textarea[name="report_desc').val();
      var formdata = { 'report[]' : [],'id_course': '<?= $id_course ?>','id_episode': '<?= $id_episode ?>','report_desc' : report_desc};
      $(".reportCbx:checked").each(function() {
        formdata['report[]'].push($(this).val());
      });
      //console.log(formdata);

      $.ajax({
        url: '<?= base_url("users/users_learn/report") ?>',
        method: "POST",
        data: formdata,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["proses"] == 'success') {
          	 console.log(hasil["pesan"]);
             toastr.success(hasil["pesan"]);
             setTimeout(function () {
               return hasil["proses"];
             }, 1500);
             $('#reportModal').modal('hide');
             setTimeout(function () {
               location.reload(true);
             }, 1500);
           }else{
             toastr.error(hasil["pesan"]);
             result = false;
             return hasil["proses"];
           }
         },
         error: function (res) {
           toastr.error("Server Errror.");
           result = false;
           return false;
         },
      });
   }

	</script>
</body>
</html>
