<style>
	.checkbox-group {
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	width: 90%;
	margin-left: auto;
	margin-right: auto;
	user-select: none;
}

.btn-green {
	background-color: green;
	color: white;
	border-radius: 2px
}

.checkbox-group-legend {
	font-size: 1.5rem;
	font-weight: 700;
	color: black;
	text-align: center;
	padding-top: 20px;
	line-height: 1.125;
	margin-bottom: 1.25rem;
}

.checkbox-input {
	// Code to hide the input
	clip: rect(0 0 0 0);
	clip-path: inset(100%);
	height: 1px;
	overflow: hidden;
	position: absolute;
	white-space: nowrap;
	width: 1px;
}

.checkbox-tile {
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	padding: 5px 15px 5px 15px;
	border-radius: 0.5rem;
	border: 2px solid #b5bfd9;
	background-color: #fff;
	box-shadow: 0 5px 10px rgba(#000, 0.1);
	transition: 0.15s ease;
	cursor: pointer;
	position: relative;
	margin: 2px;
}
.checked > .checkbox-tile {
	color: white;
  	background-color: #2166af;
}

.checkbox-input:checked + .checkbox-tile {
	color: white;
  	background-color: #2166af;
}
.btn {
	position: relative;
    font-weight: 500;
    font-size: 13px;
    color: #fff;
    background: #333;
    display: inline-block;
    -webkit-transition: all 0.4s ease;
    -moz-transition: all 0.4s ease;
    transition: all 0.4s ease;
    z-index: 5;
    display: inline-block;
    padding: 13px 26px;
    border-radius: 0px;
    text-transform: uppercase;
}

</style>
<div class="container">
  	<div class="row" style="min-height:350px">
		<div class="col-lg-12 col-md-12">
			<form id="form_interest">
			<div class="checkbox-group">
				<legend class="checkbox-group-legend"><?= $this->lang->line('choose_your_interest'); ?></legend>
				<?php if ($data->num_rows() > 0): ?>
					<?php foreach ($data->result() as $value): ?>
						<div class="checkbox">
							<label class="checkbox-wrapper <?= !empty($value->id_exist) ? 'checked' : ''  ?>">
								<input name="interest[]" value="<?= $value->id  ?>" type="checkbox" class="checkbox-input" <?= !empty($value->id_exist) ? 'checked' : ''  ?> />
								<span class="checkbox-tile">
									<span class="checkbox-icon">
										<!-- <i class="ti-facebook"></i> -->
										<?= $value->nama_kategori  ?>
									</span>
								</span>
							</label>
						</div>
					<?php endforeach ?>
				<?php endif ?>
			</div>
			</form>
		</div>
	</div>
	<hr>
	<div class="row text-center">
		<div class="col-lg-12 col-md-12 mb-3 ">
			<!-- <input onclick="subInterest()" class="btn float-right btn-green" type="button" value="SUMBIT"> -->
			<button onclick="subInterest()" style="background-color:green;width:100%" class="btn btn-green" type="button"><?= $this->lang->line('save');?></button>
		</div>
	</div>
</div>
<script>
	function subInterest(){
		$.ajax({
        type: 'POST',
        url: '<?= $link?>',
        data: $("#form_interest").serialize(),
        dataType: "json",
        success: function(hasil) {
        	console.log(hasil)
        	if (hasil['status'] == 'success') {
	          	toastr.success(hasil["pesan"])
        	}else if(hasil['status'] == 'warning'){
	          	toastr.warning(hasil["pesan"])
        	}else{
	          	toastr.error(hasil["pesan"])
        	}
        },
      });
	}
</script>