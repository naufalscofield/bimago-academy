<?php if (!empty($invoice['data'])) { ?>
<div class="product-area section">
  <div class="container">

    <a href="#" class="btn2 ladda-button payment" url="<?=$payment;?>" onclick="openModal(this);" data-style="expand-left" style="background-color:#c81c1d"><p style="color:white;"><i class="fa fa-exclamation-circle"
      ></i> <?= $this->lang->line('procedure_of_payment');?></p></a>
      <br><br>
      <div class="row">
        <div class="col-12">
          <div class="product-info">
            <div class="nav-main">
              <!-- Tab Nav -->
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#invoice" role="tab"><?= $this->lang->line('invoice_history');?></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#purchase" role="tab"><?= $this->lang->line('purchase_history');?></a></li>
              </ul>
              <!--/ End Tab Nav -->
            </div>
            <br>
            <div class="tab-content" id="myTabContent">
              <!-- Start Single Tab -->
              <div class="tab-pane fade show active" id="invoice" role="tabpanel">
                <div class="tab-single">
                  <div class="shopping-cart section">
                		<div class="container">
                      <div class="row">
                				<div class="col-12">
                						<!-- Shopping Summery -->
                						<table class="table shopping-summery">
                							<thead>
                								<tr class="main-hading">
                                  <th width="5%"></th>
                                  <th width="16%"><?= $this->lang->line('no_invoice');?></th>
                                  <th width="16%"><?= $this->lang->line('method');?></th>
                                  <th width="19%">TOTAL</th>
                                  <th width="14%">STATUS</th>
                                  <th width="25%" class="text-center"><?= $this->lang->line('purchase_date');?></th>
                								</tr>
                							</thead>
                              <tbody>
                                <?php
                                $total = 0;
                                foreach ($invoice['data'] as $key => $value) {
                                  ?>
                                  <tr>
                                    <td class="price" style="<?= $value['status'] == 1 ? 'background-color:#cbf5cf;' : 'background-color:#f5a2a7;'?>" data-title="Indikator"><i class="ti-shopping-cart-full"></i></td>
                                    <td class="price" data-title="Invoice"><span><?=$value['no_pesanan'];?></span></td>
                                    <td class="price" data-title="Method"><span><?=$value['metode_pembayaran'];?></span></td>
                                    <td class="price" data-title="Total"><span><?=rupiah($value['total']);?></span></td>
                                    <td class="price" data-title="Status">
                                      <span>
                                        <?php if ($value['status'] == 1) { ?>
                                          PAID OFF
                                        <?php }else if($value['status'] == 2){ ?>
                                          EXPIRE
                                        <?php }else{ ?>
                                          NOT YET PAID
                                        <?php } ?>
                                      </span>
                                    </td>
                                    <td class="price" data-title="Date"><span><?=indo_date($value['tanggal_pembelian']);?></span></td>
                                  </tr>
                                <?php } ?>
                              </tbody>
                						</table>
                						<!--/ End Shopping Summery -->
                				</div>
                			</div>
                    </div>
                  </div>
                </div>
              </div>
              <!--/ End Single Tab -->
              <!-- Start Single Tab -->
              <div class="tab-pane fade" id="purchase" role="tabpanel">
                <div class="tab-single">
                  <div class="shopping-cart section">
                		<div class="container">
                      <div class="row">
                				<div class="col-12">
                						<!-- Shopping Summery -->
                						<table class="table shopping-summery">
                							<thead>
                								<tr class="main-hading">
                                  <th width="10%"><?= strtoupper($this->lang->line('invoice_history'));?></th>
                                  <th width="15%"><?= strtoupper($this->lang->line('no_invoice'));?></th>
                                  <th width="30%"><?= strtoupper($this->lang->line('courses'));?></th>
                                  <th width="15%"><?= strtoupper($this->lang->line('categories'));?></th>
                                  <th width="15%"><?= strtoupper($this->lang->line('type_course'));?></th>
                                  <th width="15%" class="text-center"><?= strtoupper($this->lang->line('price'));?></th>
                								</tr>
                							</thead>
                              <tbody>
                               <?php
                                $total = 0;
                                 foreach ($purchase['data'] as $key => $value) {
                                   ?>
                                  <tr>
                                    <td class="image" data-title="Product"><center><img src="<?=base_url().$value['image'];?>" alt="#" style="width:100%;height:100%;"></center></td>
                                    <td class="price" data-title="Invoice"><span><?=$value['no_pesanan'];?></span></td>
                                    <td class="price" data-title="Course"><span><?=$value['judul'];?></span></td>
                                    <td class="price" data-title="Category"><span><?=$value['modul'];?></span></td>
                                    <td class="price" data-title="Type"><span><?=$value['type_course'];?></span></td>
                                    <?php if ($value['harga'] != 0) { ?>
                                      <td class="price" data-title="Price"><span><?= rupiah($value['harga']);?></span></td>
                                    <?php }else{ ?>
                                      <td class="price" data-title="Price"><span><?= $this->lang->line('free') ?></span></td>
                                    <?php } ?>
                                  </tr>
                                <?php } ?>
                              </tbody>
                						</table>
                						<!--/ End Shopping Summery -->
                				</div>
                			</div>
                    </div>
                  </div>
                </div>
              </div>
              <!--/ End Single Tab -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php }else{ ?>
  <style media="screen">
  @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

  body {
    background-color: #eee;
  }

  .card {
    margin-bottom: 30px;
    border: 0;
    -webkit-transition: all .3s ease;
    transition: all .3s ease;
    letter-spacing: .5px;
    border-radius: 8px;
    -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
    box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05)
  }

  .card .card-header {
    background-color: #fff;
    border-bottom: none;
    padding: 24px;
    border-bottom: 1px solid #f6f7fb;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px
    } */

    .btn-primary,
    .btn-primary.disabled,
    .btn-primary:disabled {
      background-color: #4466f2 !important;
      border-color: #4466f2 !important
    }
    </style>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body cart">
              <div class="col-sm-12 empty-cart-cls text-center"> <img src="<?=base_url().'assets/default/empty-invoice2.png';?>" width="500" height="500" class="img-fluid mb-4 mr-3">
                <h3><strong>You don't have shopping history!</strong></h3>
                <h4>Immediately make a purchase.</h4> <a href="<?=$course;?>" class="btn btn-primary cart-btn-transform m-3" style="color:white;" data-abc="true">continue shopping</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>

  <!-- Modal -->
    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <div class="payment-body">
            </div>
        </div>
      </div>
    </div>
    <!-- Modal end -->

<script type="text/javascript">
function openModal(obj) {
// var id = $(this).attr('id');
var url = $(obj).attr('url');
var l = Ladda.create( document.querySelector( '.payment') );
l.start();
  $.ajax({
      url: url,
      method: "POST",
      // data: {id:id},
      success: function(data){
        l.stop();
        $('.payment-body').html(data);
        $('#paymentModal').modal('show');
      }
  });
}
</script>
