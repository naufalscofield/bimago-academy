<style>
  .underline {
    border-bottom: 2px solid currentColor;
  }
  .checked {
    color: orange;
  }
  .love {
    color: red;
  }
  /* Card styles */
  .card{
      background-color: #fff;
      height: auto;
      width: auto;
      overflow: hidden;
      margin: 12px;
      border-radius: 5px;
      box-shadow: 9px 17px 45px -29px
                  rgba(0, 0, 0, 0.44);
  }

  /* Card image loading */
  .card__image img {
      width: 100%;
      height: 100%;
  }

  .card__image.loading {
      height: 200px;
      width: 400px;
  }

  /* Card title */
  .card__title {
      padding: 8px;
      font-size: 22px;
      font-weight: 700;
  }

  .card__title.loading {
      height: 1rem;
      width: 50%;
      margin: 1rem;
      border-radius: 3px;
  }

  /* Card description */
  .card__description {
      padding: 8px;
      font-size: 16px;
  }

  .card__description.loading {
      height: 3rem;
      margin: 1rem;
      border-radius: 3px;
  }

  /* The loading Class */
  .loading {
      position: relative;
      background-color: #e2e2e2;
  }

  /* The moving element */
  .loading::after {
      display: block;
      content: "";
      position: absolute;
      width: 100%;
      height: 100%;
      transform: translateX(-100%);
      background: -webkit-gradient(linear, left top,
                  right top, from(transparent),
                  color-stop(rgba(255, 255, 255, 0.2)),
                  to(transparent));

      background: linear-gradient(90deg, transparent,
              rgba(255, 255, 255, 0.2), transparent);

      /* Adding animation */
      animation: loading 0.8s infinite;
  }

  /* Loading Animation */
  @keyframes loading {
      100% {
          transform: translateX(100%);
      }
  }
</style>
<!-- Breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-12" style="overflow:auto;">
        <!-- <div class="bread-inner">
          <ul class="bread-list">
            <li><a href="index1.html">Home<i class="ti-arrow-right"></i></a></li>
            <li class="active"><a href="blog-single.html"> My Course</a></li>
          </ul>
        </div> -->
        <h4><?= $this->lang->line('my_course') ?></h4>
        <div id="nav" class="tabsMain text-left">
            <ul class="nav nav-tabs" style="width:500px;">
              <li class="nav-item">
                <a id="all" class="nav-link"><?= $this->lang->line('all_course') ?></a>
              </li>
               <li class="nav-item">
                <a id="wishlist" class="nav-link"><?= $this->lang->line('wishlist') ?> </a>
              </li>
               <li class="nav-item">
                <a id="history" class="nav-link"><?= $this->lang->line('history') ?></a>
              </li>
              <li class="nav-item">
               <a id="exam" class="nav-link"><?= $this->lang->line('exam');?></a>
             </li>
               <li class="nav-item">
                <a id="certificate" class="nav-link"><?= $this->lang->line('certificate') ?></a>
              </li>
            </ul>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- End Breadcrumbs -->
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-3 col-3">
        <div class="card skeleton">
          <div class="card__image loading"></div>
          <div class="card__title loading"></div>
          <div class="card__description loading"></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-3">
        <div class="card skeleton">
          <div class="card__image loading"></div>
          <div class="card__title loading"></div>
          <div class="card__description loading"></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-3">
        <div class="card skeleton">
          <div class="card__image loading"></div>
          <div class="card__title loading"></div>
          <div class="card__description loading"></div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-3">
        <div class="card skeleton">
          <div class="card__image loading"></div>
          <div class="card__title loading"></div>
          <div class="card__description loading"></div>
        </div>
    </div>
    <div id="content" style="width: 100%; min-width: 300px; display:none"></div>
    </div>
  </div>
</div>
<!--/ End Product Style 1  -->
<script>
  $("#nav li a").click(function() {
      $("#nav li a").removeClass('active');
      $(this).addClass('active');
      $.ajax({
        type: 'POST',
        url: '<?= $link?>',
        data: { id : this.id },
        dataType: "json",
        beforeSend: function () {
            $("div#content").hide();
            $('.skeleton').show()
            $('#loader').removeClass('display-none')
          },
          success: function(html) {
          $('.skeleton').hide()
          $("div#content").html(html);
          $("div#content").show();
        },
      });
  });
  $.ajax({
      type: 'POST',
      url: '<?= $link?>',
      data: { id : '<?=$list;?>' },
      dataType: "json",
      beforeSend: function () {
        $("div#content").hide();
        $(".skeleton").show()
      },
      success: function(html) {
        $('.skeleton').hide()
        $("div#content").html(html);
        $("div#content").show();
      },
    });
</script>
