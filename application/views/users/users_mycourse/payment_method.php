<br>
<div id="all">
  <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="contact" class="box">
            <h1>Procedure of Payments</h1>
            <p>We Provide Instructions For Your Payment.</p>
            <hr>
            <div id="accordion">
              <div class="card border-primary mb-3">
                <div id="headingOne" class="card-header p-0 border-0">
                  <h4 class="mb-0"><a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="btn btn-primary d-block text-left rounded-0" style="color:white;">1. Manual Payment?</a></h4>
                </div>
                <div id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion" class="collapse show">
                  <div class="card-body">
                    <ul>
                      <li>
                        1. Lakukan transfer ke rekening 0123456 dengan nominal yang tertera pada struk pembelian kemudian bukti transfer di foto atau screenshot.
                        <img src="<?= base_url('assets/default/payment/manual-1.png');?>" alt="">
                      </li>
                      <br>
                      <li>
                        2. Hubungi admin kami setelah selesai melakukan pembayaran.<br>
                        <img src="<?= base_url('assets/default/payment/manual-2.png');?>" style="width:350px;height:120px;" alt="">
                        &emsp; atau &emsp;
                        <img src="<?= base_url('assets/default/payment/manual-2.1.png');?>" style="width:100px;height:150px;" alt="">
                      </li>
                      <br>
                      <li>
                        3. Kirimkan bukti transfer tadi dan beritahukan nomor struk yang anda bayar.<br>
                        <img src="<?= base_url('assets/default/payment/manual-3.png');?>" alt="">
                      </li>
                      <br>
                      <li>4. Tunggu admin kami selesai melakukan pengecekan.</li>
                      <br>
                      <li>
                        5. Apabila semua proses sudah selesai maka admin kami akan mengubah status pembayaran menjadi lunas.
                        <img src="<?= base_url('assets/default/payment/manual-5.png');?>" alt="">
                      </li>
                      <br>
                      <li>
                        6. Pelatihan yang sudah di beli dapat anda nikmati di menu My Course.
                        <img src="<?= base_url('assets/default/payment/manual-6.png');?>" alt="">
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="card border-primary mb-3">
                <div id="headingTwo" class="card-header p-0 border-0">
                  <h4 class="mb-0"><a href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="btn btn-primary d-block text-left rounded-0" style="color:white;">2. Virtual Account Payment?</a></h4>
                </div>
                <div id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion" class="collapse">
                  <div class="card-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                </div>
              </div>
            </div>
            <!-- /.accordion-->
          </div>
        </div>
        <!-- /.col-lg-9-->
      </div>
    </div>
  </div>
</div>
<br>
