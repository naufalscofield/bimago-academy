<style>
@media only screen and (max-width: 450px){
  .shop-home-list .single-list .content {
    padding: 5px;
    text-align: center;
  }
  hr{
    margin-bottom: 0.5rem;
    margin-top: 0rem;
  }
  .mobile{
    height: 125px !important;
  }
}

@media only screen and (min-width: 451px) and (max-width: 992px){
  .mobile{
    height: 100px !important;
  }
  .shop-home-list .single-list .content {
    padding: 5px;
    text-align: center;
  }
  hr{
    margin-bottom: 0.5rem;
    margin-top: 0rem;
  }
  .responsive-img {
    min-height: 272px !important;
  }
}

.responsive-img {
  min-height: 166px;
}
.mobile{
  height: 170px;
}

hr{
  margin-bottom: 0.5rem;
  margin-top: 0rem;
}

.searchMyCourse {
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
}
.modal-dialog .modal-content .modal-body {
    padding: 0px;
    overflow-y: auto;
    max-height: 510px;
    height: 200px;
}
.for-cat{
  background-color: #ed1b24;
	display: inline-block;
	font-size: 11px;
	color: #fff;
	right: 10px;
	top: 20px;
	padding: 1px 16px;
	font-weight: 700;
	border-radius: 0;
	text-align: center;
	position: absolute;
	text-transform: uppercase;
	border-radius: 30px;
	height: 26px;
	line-height: 24px;
}

.moree {
  display: none;
}
</style>
<style>
  no-bullet.ul {
  list-style-type: none;
}

  .btn-blue2 {
  background-color: #0c60af;
  border-color: #0c60af;
  color: white;
  border-radius: 2px;
  }
  .btn-blue2:hover {
  background-color: #87CEFA;
  border-color: #87CEFA;
  }
  .btn-gray2 {
  background-color: #C0C0C0;
  border-color: #C0C0C0;
  color: white;
  border-radius: 2px;
  }
  .btn-gray2:hover {
  background-color: #C0C0C0;
  border-color: #C0C0C0;
  }
  .btn-red2 {
  background-color: #c81c1d;
  border-color: #c81c1d;
  color: white;
  border-radius: 2px;
  }
  .btn-red2:hover {
  background-color: #c81c1d;
  border-color: #c81c1d;
  }
  .btn-green2 {
  background-color: #228B22;
  border-color: #228B22;
  color: white;
  border-radius: 2px;
  }
  .btn-green2:hover {
  background-color: #3CB371;
  border-color: #3CB371;
  }
  .btn-yellow2 {
  background-color: #FFD700;
  color: white;
  border-radius: 2px;
  border-color: #FFD700;
  }
  .btn-yellow2:hover {
  background-color: #F0E68C;
  border-color: #F0E68C;
  }
  .modal-content {
  height: 100%;
  width: 100%;
  }
  .modal-body-exam {
  overflow-y: scroll;
  }

  .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: #fff !important;
  }
</style>
<section class="shop-home-list section">
<br>
<input type="text" class="searchMyCourse" id="searchMyCourse" placeholder="&#xF002; <?= $this->lang->line('search_my_course'); ?>">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<!-- Start Single List  -->
					<div class="row contentOri">
						<?php foreach ($owned as $key => $value) { $button = ''; ?>
              <?php if (!empty($value['type_course'])) { ?>
                <?php if ($value['id_type'] == 1) {
                  $href = '<a href="'.$course.encode_url($value['id']).'/'.encode_url($value['id_type']).'" class="buy" target="_blank"><i class="fa fa-play-circle"></i></a>';
                  $target = 'href="'.$course.encode_url($value['id']).'/'.encode_url($value['id_type']).'"'; ?>
                  <div class="col-lg-3 col-md-6 col-12 row-list">
                    <div class="single-list">
                      <div class="list-image overlay">
                        <img src="<?=base_url().$value['image'];?>" alt="#" class="responsive-img" style="width:100%;height:100%">
                        <?=$href;?>
                        <span class="for-cat"><?=$value['nama_type'];?></span>
                      </div>
                      <div class="content">
                        <div class="mobile">
                          <h4 class="title"><a target="_blank" <?=$target;?>><?=$value['judul'];?></a></h4>
                          <p><?= $this->lang->line('instructure').' : '.$value['nama_depan'].' '.$value['nama_belakang'];?></p>
                          <p><i class="ti-shopping-cart-full"></i> <?=eng_date($value['tanggal_pembelian']);?></p>
                        </div>
                        <hr>
                        <div style="height:50px">
                          <center>
                            <a style="color:white" class="btn btn-blue2" target="_blank" <?=$target;?>><i class="fa fa-pencil"></i> <?= $this->lang->line('start_course');?></a>
                          </center>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php }elseif ($value['id_type'] == 2) {
                  if ($value['pelaksanaan_led'] == date('Y-m-d') || $value['pelaksanaan_led'] > date('Y-m-d')){
                    $href = '';
                    $live = '<p>Live on '.eng_date($value['pelaksanaan_led']).'</p>';
                    $target = ''; ?>
                    <div class="col-lg-3 col-md-6 col-12 row-list">
                      <div class="single-list">
                        <div class="list-image overlay">
                          <img src="<?=base_url().$value['image'];?>" alt="#" class="responsive-img" style="width:100%;height:100%">
                          <?=$href;?>
                          <span class="for-cat"><?=$value['nama_type'];?></span>
                        </div>
                        <div class="content">
                          <div class="mobile">
                            <h4 class="title"><a target="_blank" <?=$target;?>><?=$value['judul'];?></a></h4>
                            <p><?='Instructure : '.$value['nama_depan'].' '.$value['nama_belakang'];?></p>
                            <?=$live;?>
                          </div>
                          <hr>
                        </div>
                      </div>
                    </div>
                  <?php }else{
                    $href = '<a href="'.$course.encode_url($value['id']).'/'.encode_url($value['id_type']).'" class="buy" target="_blank"><i class="fa fa-play-circle"></i></a>';
                    $live = '<i class="fa fa-check-circle"></i> '.$this->lang->line('already_done_on').' : '.eng_date($value['pelaksanaan_webinar']);
                    $target = 'href="'.$course.encode_url($value['id']).'/'.encode_url($value['id_type']).'"'; ?>
                    <div class="col-lg-3 col-md-6 col-12 row-list">
                      <div class="single-list">
                        <div class="list-image overlay">
                          <img src="<?=base_url().$value['image'];?>" alt="#" class="responsive-img" style="width:100%;height:100%">
                          <?=$href;?>
                          <span class="for-cat"><?=$value['nama_type'];?></span>
                        </div>
                        <div class="content">
                          <div class="mobile">
                            <h4 class="title"><a target="_blank" <?=$target;?>><?=$value['judul'];?></a></h4>
                            <p><?='Instructure : '.$value['nama_depan'].' '.$value['nama_belakang'];?></p>
                            <?=$live;?>
                            <p><i class="ti-shopping-cart-full"></i> <?=eng_date($value['tanggal_pembelian']);?></p>
                          </div>
                          <hr>

                        </div>
                      </div>
                    </div>
                  <?php }
                }elseif ($value['id_type'] == 3) {
                  $href = '<a style="cursor: pointer;"  data-id="'.$value['id_type'].'" data-course="'.$value['id'].'" onclick="showDesc(this)" class="buy"><i class="fa fa-play-circle"></i></a>';
                  $target = 'style="cursor: pointer;"  data-id="'.$value['id_type'].'" data-course="'.$value['id'].'" onclick="showDesc(this)"';
                  if ($value['pelaksanaan_training'] == date('Y-m-d') || $value['pelaksanaan_training'] > date('Y-m-d')){
                    $live = '<p>Start on '.eng_date($value['pelaksanaan_training']).'</p>';
                  }else{
                    $live = '<p><i class="fa fa-check-circle"></i> '.$this->lang->line('already_done_on').' : '.eng_date($value['pelaksanaan_training']).'</p>';
                  } ?>
                  <div class="col-lg-3 col-md-6 col-12 row-list">
                    <div class="single-list">
                      <div class="list-image overlay">
                        <img src="<?=base_url().$value['image'];?>" alt="#" class="responsive-img" style="width:100%;height:100%">
                        <?=$href;?>
                        <span class="for-cat"><?=$value['nama_type'];?></span>
                      </div>
                      <div class="content">
                        <div class="mobile">
                          <h4 class="title"><a target="_blank" <?=$target;?>><?=$value['judul'];?></a></h4>
                          <p><?='Instructure : '.$value['nama_depan'].' '.$value['nama_belakang'];?></p>
                          <?=$live;?>
                          <p><i class="ti-shopping-cart-full"></i> <?=eng_date($value['tanggal_pembelian']);?></p>
                        </div>
                        <hr>
                      </div>
                    </div>
                  </div>
                <?php }elseif ($value['id_type'] == 4) {
                  if ($value['pelaksanaan_webinar'] == date('Y-m-d') || $value['pelaksanaan_webinar'] > date('Y-m-d')){
                    $href = '';
                    $live = '<p>Live on '.eng_date($value['pelaksanaan_webinar']).'</p>';
                    $target = ''; ?>
                    <div class="col-lg-3 col-md-6 col-12 row-list">
                      <div class="single-list">
                        <div class="list-image overlay">
                          <img src="<?=base_url().$value['image'];?>" alt="#" class="responsive-img" style="width:100%;height:100%">
                          <?=$href;?>
                          <span class="for-cat"><?=$value['nama_type'];?></span>
                        </div>
                        <div class="content">
                          <div class="mobile">
                            <h4 class="title"><a target="_blank" <?=$target;?>><?=$value['judul'];?></a></h4>
                            <?=$live;?>
                            <p><i class="ti-shopping-cart-full"></i> <?=eng_date($value['tanggal_pembelian']);?></p>
                            ID Zoom : <?=$value['id_zoom_webinar'];?><br>
                            Pass Zoom : <?=$value['pass_zoom_webinar'];?><br>
                            <div class="">
                              Speakers : <br>
                              <span class="less_<?=$value['id'];?>"><?=substr($value['pembicara_webinar'], 0, 50);?>...</span>
                              <span class="more_<?=$value['id'];?> moree"><?=$value['pembicara_webinar'];?></span>
                              <a style="cursor:pointer;" onclick="read(this)" id="<?=$value['id'];?>" class="myBtn_<?=$value['id'];?>">Read more</a>
                            </div>
                          </div>
                          <hr>
                        </div>
                      </div>
                    </div>
                  <?php }else{
                    $href = '<a href="'.$course.encode_url($value['id']).'/'.encode_url($value['id_type']).'" class="buy" target="_blank"><i class="fa fa-play-circle"></i></a>';
                    $live = '<i class="fa fa-check-circle"></i> '.$this->lang->line('already_done_on').' : '.eng_date($value['pelaksanaan_webinar']);
                    $target = 'href="'.$course.encode_url($value['id']).'/'.encode_url($value['id_type']).'"'; ?>
                    <div class="col-lg-3 col-md-6 col-12 row-list">
                      <div class="single-list">
                        <div class="list-image overlay">
                          <img src="<?=base_url().$value['image'];?>" alt="#" class="responsive-img" style="width:100%;height:100%">
                          <?=$href;?>
                          <span class="for-cat"><?=$value['nama_type'];?></span>
                        </div>
                        <div class="content">
                          <div class="mobile">
                            <h4 class="title"><a target="_blank" <?=$target;?>><?=$value['judul'];?></a></h4>
                            <?=$live;?>
                            <p><i class="ti-shopping-cart-full"></i> <?=eng_date($value['tanggal_pembelian']);?></p>
                          </div>
                          <hr>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                <?php } ?>
              <?php } ?>
						<?php } ?>
					</div>
					<!-- End Single List  -->
				</div>
			</div>
		</div>
	</section>

   <!-- Modal -->
    <div class="modal fade" id="descModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row p-3">
                  <div class="col-md-9 col-10">
                  <h4>Info Class Training</h4>
                  <!-- <p>deskripsi singkat</p> -->
                  </div>
                  <div class="col-md-3 col-2">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
                  </div>
                </div>
                <div class="modal-body">
                  <div class="col-lg-12 col-md-12 col-12">
                      <div id="content-desc"></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
  <!-- Modal end -->

<script>
$(document).ready(function(){
	let contentOri  = [];

	var nodes = document.getElementsByClassName('row-list');
	for (i = 0; i < nodes.length; i++) {
		contentOri[i] = nodes[i].outerHTML;
	}

	$("#searchMyCourse").keyup(function(){
		var input = document.getElementById("searchMyCourse");
		var filter = input.value.toLowerCase();

		arr = jQuery.grep(contentOri, function( n, i ) {
			var reg = new RegExp(filter, "ig");
       		return reg.test(n)
		});

		console.log(arr)

		$('.contentOri').html('');
		for (i = 0; i < arr.length; i++) {
			$('.contentOri').append(arr[i]);
		}
	})

})
  function showDesc(obj){
  var id = $(obj).data('id');
  var id_course = $(obj).data('course');
   $.ajax({
      type: 'POST',
      url: '<?= base_url('users/users_mycourse/desc')?>',
      data: {id_course : id_course, id_type : id },
      dataType: "json",
      success: function(html) {
        $("#content-desc").html(html);
        $('#descModal').modal('toggle');
      },
    });
  }

  function read(obj) {

  var id = $(obj).attr('id');
  var less = document.querySelector( '.less_' + id );
  var more = document.querySelector( '.more_' + id );
  var btnText = document.querySelector( '.myBtn_' + id );
  console.log(less+' '+more)
  if (less.style.display === "none") {
      less.style.display = "inline";
      btnText.innerHTML = "Read more";
      more.style.display = "none";
    } else {
      less.style.display = "none";
      btnText.innerHTML = "Read less";
      more.style.display = "inline";
    }
  }
</script>
