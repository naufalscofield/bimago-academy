<?php if (!empty($wishlisted)) { ?>
<section class="shop-home-list section">
		<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<!-- Start Single List  -->
						<div class="row">
							<?php foreach ($wishlisted as $key => $value) { ?>
								<div class="col-lg-3 col-md-6 col-12" id="mycourse_<?=$value['id'];?>">
									<div class="single-list">
										<div class="list-image overlay">
											<img src="<?=base_url().$value['image']?>" alt="#" class="responsive-img" style="width:100%;height:100%">
											<a href="<?=$detail.$value['id'];?>" class="buy"><i class="fa fa-play-circle"></i></a>
										</div>
										<div class="content">
											<div style="height:80px">
												<h4 class="title"><a href="<?=$detail.encode_url_share_id($value['id']);?>"><?=$value['judul'];?></a></h4>
												<p style="color:grey;font-size:12px;"><?=$value['institusi'].' ('.$value['nama_depan'].' '.$value['nama_belakang'].')';?></p>
												<div>
		                      <span style="color:orange;"><?= $value['rating'];?></span>
		                      <?php if ($value['rating_belakang'] == '' || $value['rating_belakang'] == 0) { ?>
		                        <?php if ($value['rating_depan'] == '' || $value['rating_depan'] == 0) { ?>
		                          <?php $not = 5; ?>
		                          <?php for ($i=0; $i < $not; $i++) { ?>
		                            <span class="fa fa-star-o"></span>
		                          <?php } ?>
		                        <?php }else{ ?>
		                          <?php $not = 5 - $value['rating_depan'];
		                          for ($i=0; $i < $value['rating_depan']; $i++) { ?>
		                            <span class="fa fa-star checked"></span>
		                          <?php } ?>
		                          <?php for ($i=0; $i < $not; $i++) { ?>
		                            <span class="fa fa-star-o"></span>
		                          <?php } ?>
		                        <?php } ?>
		                      <?php }else{ ?>
		                        <?php $not = 4 - $value['rating_depan'];
		                        for ($i=0; $i < $value['rating_depan']; $i++) { ?>
		                          <span class="fa fa-star checked"></span>
		                        <?php } ?>
		                        <span class="fa fa-star-half-o checked"></span>
		                        <?php for ($i=0; $i < $not; $i++) { ?>
		                          <span class="fa fa-star-o"></span>
		                        <?php } ?>
		                      <?php } ?>
		                      (<?= $value['reviewer'];?>)
		                    </div>
												<?php if ($value['harga'] == 0) { ?>
													<p style="color:grey;font-size:12px;"><?=indo_date($value['pelaksanaan']);?></p>
												<?php }else{ ?>
													<br>
												<?php } ?>
						          </div>
											<hr>
												<div>
													<a class="btn2 ladda-button cart_<?=$value['id'];?>" data-style="expand-left" url="<?=$detail_cart;?>" id="<?=$value['id'];?>" onclick="openModalCart(this)"><p style="color:white;"><?= $this->lang->line('add_to_cart') ?></p></a>
													<a id="<?=$value['id'];?>" url="<?=$wishlist;?>" onclick="add_remove_wishlist(this);" class="btn2 ladda-button ladda-wish<?=$value['id'];?>"><i id="wish_<?=$value['id'];?>" class="fa fa-heart love"></i></a>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
						<!-- End Single List  -->
					</div>
				</div>
		</div>
	</section>
<?php }else{ ?>
	<style media="screen">
	@import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

	body {
		background-color: #eee;
	}

	.card {
		margin-bottom: 30px;
		border: 0;
		-webkit-transition: all .3s ease;
		transition: all .3s ease;
		letter-spacing: .5px;
		border-radius: 8px;
		-webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
		box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05)
	}

	.card .card-header {
		background-color: #fff;
		border-bottom: none;
		padding: 24px;
		border-bottom: 1px solid #f6f7fb;
		border-top-left-radius: 8px;
		border-top-right-radius: 8px
		} */

		.btn-primary,
		.btn-primary.disabled,
		.btn-primary:disabled {
			background-color: #4466f2 !important;
			border-color: #4466f2 !important
		}
		</style>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-body cart">
						    <div class="col-sm-12 empty-cart-cls text-center"> <img src="<?=base_url().'assets/default/empty-wishlist2.png';?>" width="500" height="500" class="img-fluid mb-4 mr-3">
						        <h3><strong><?= $this->lang->line('you_dont_have_wishlist'); ?></strong></h3>
						        <a href="<?=$course;?>" class="btn btn-primary cart-btn-transform m-3" style="color:white;" data-abc="true"><?= $this->lang->line('continue'); ?></a>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
