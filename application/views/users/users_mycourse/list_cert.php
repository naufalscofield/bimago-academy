<section class="shop-home-list section">
	<br>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<!-- Start Single List  -->
						<table class="table table-hover table-striped">
							<tr>
								<th width="2%">No</th>
								<th width="50%"><?= $this->lang->line('course') ?></th>
								<th width="30%"><?= $this->lang->line('certificate_number') ?></th>
								<th width="18%"><?= $this->lang->line('download'); ?></th>
							</tr>
							<?php if ($list_cert->num_rows()): ?>
								<?php foreach ($list_cert->result() as $key => $value): ?>
									<tr>
										<td><?= $key+1 ?></td>
										<td><?= $value->judul.' ('. (!empty($value->type_course) ? $value->type_course : 'Webinar') . ')' ;?></td>
										<td><?= $value->certificate_number; ?></td>
										<td>
											<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> <i class="fa fa-search"></i> </button> -->
											<a target="_blank" id='<?= $value->id ?>' onclick="generateCertificate(this)" class="btn btn-sm btn-warning ladda-button ladda-button<?= $value->id ?> m-1" data-style="expand-left"><i class="fa fa-download"></i></a>
											<a target="_blank" id='pdf<?= $value->id ?>' class="btn btn-sm btn-dangger ladda-button ladda-button-pdf<?= $value->id ?> m-1" data-style="expand-left" href="<?= base_url('users/users_mycourse/certpdf/'. encode_url($value->id)) ?>" ><i class="fa fa-file-pdf-o"></i></a>
										</td>
									</tr>
								<?php endforeach ?>
							<?php else: ?>
									<tr>
										<td colspan="4" class="text-center"><?= $this->lang->line('certificate_exist') ?></td>
									</tr>
							<?php endif ?>
						</table>
					<!-- End Single List  -->
				</div>
			</div>
		</div>
	</section>

	<script>
		function generateCertificate(obj){
  		var id = $(obj).attr('id');
		var l = Ladda.create( document.querySelector('.ladda-button'+id) );
		l.start();
		$.ajax({
		    url: '<?= base_url('users/users_mycourse/certificate'); ?>',
		    method: "POST",
		    data: {id:id},
		    success: function (res) {
		  		l.stop();
		    	let r = JSON.parse(res)
		    	window.open(r.url, '_blank');
		    	// location.reload();
		    },
		    error: function (res) {
		      
		    },
		});
		}
	</script>

<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="col-lg-12 col-md-12 col-12">
        <img src="<?php //echo base_url('assets/certificate/certificate.png'); ?>">
      </div>
    </div>
    </div>
  </div>
</div> -->
