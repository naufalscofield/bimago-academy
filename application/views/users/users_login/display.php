<!-- <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'> -->
<link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous">
</script>
<style>

	/* .logo {
		width: 200px;
		height: 100px;
		margin-top: 20px;
		margin-left: 35px
	} */

	.chkbx {
    /* -webkit-appearance:none; */
    width:15px;
    height:15px;
    background:white;
    border-radius:5px;
    border:2px solid #555;
	}

	.image {
		width: 400px;
		height: 320px
	}

	.border-line {
		border-right: 1px solid #EEEEEE
	}

	.facebook {
		background-color: #3b5998;
		color: #fff;
		font-size: 18px;
		padding-top: 5px;
		border-radius: 50%;
		width: 35px;
		height: 35px;
		cursor: pointer
	}

	.twitter {
		background-color: #1DA1F2;
		color: #fff;
		font-size: 18px;
		padding-top: 5px;
		border-radius: 50%;
		width: 35px;
		height: 35px;
		cursor: pointer
	}

	.linkedin {
		background-color: #2867B2;
		color: #fff;
		padding-top: 5px;
		border-radius: 50%;
		width: 35px;
		height: 35px;
		cursor: pointer
	}
	.google {
		background-color: #D6AF4045;
		color: #fff;
		padding-top: 5px;
		border-radius: 50%;
		width: 35px;
		height: 35px;
		cursor: pointer
	}

	.line {
		height: 1px;
		width: 45%;
		background-color: #E0E0E0;
		margin-top: 10px
	}

	.or {
		width: 10%;
		font-weight: bold
	}

	.text-sm {
		font-size: 14px !important
	}

	::placeholder {
		color: #BDBDBD;
		opacity: 1;
		font-weight: 300
	}

	:-ms-input-placeholder {
		color: #BDBDBD;
		font-weight: 300
	}

	::-ms-input-placeholder {
		color: #BDBDBD;
		font-weight: 300
	}

	input,
	textarea {
		padding: 10px 12px 10px 12px;
		border: 1px solid lightgrey;
		border-radius: 2px;
		margin-bottom: 5px;
		margin-top: 2px;
		width: 100%;
		box-sizing: border-box;
		color: #2C3E50;
		font-size: 14px;
		letter-spacing: 1px
	}

	input:focus,
	textarea:focus {
		-moz-box-shadow: none !important;
		-webkit-box-shadow: none !important;
		box-shadow: none !important;
		border: 1px solid #304FFE;
		outline-width: 0
	}

	button:focus {
		-moz-box-shadow: none !important;
		-webkit-box-shadow: none !important;
		box-shadow: none !important;
		outline-width: 0
	}

	a {
		color: inherit;
		cursor: pointer
	}

	.btn-blue {
		background-color: #1A237E;
		width: 150px;
		color: #fff;
		border-radius: 2px
	}

	.btn-blue:hover {
		background-color: #4285f4;
		cursor: pointer
	}

	@media screen and (max-width: 1250px) {
		/* .logo {
			margin-left: 0px
		} */

		.image {
			width: 390px;
			height: 310px
		}

		.border-line {
			border-right: none
		}

		.card2 {
			border-top: 1px solid #EEEEEE !important;
			margin: 0px 15px
		}
	}
/* =====================GOOGLE */
</style>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<body oncontextmenu='return false' class='snippet-body'>
	<!-- Breadcrumbs -->
	<div class="breadcrumbs">
	<div class="container">
		<div class="row">
		<div class="col-12">
			<div class="bread-inner">
			<ul class="bread-list">
				<li><a href="<?=$login_page;?>"><?= $this->lang->line('login'); ?></a></li>
			</ul>
			</div>
		</div>
		</div>
	</div>
	</div>
	<!-- End Breadcrumbs -->
	<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
		<div class="card card0 border-0">
			<div class="row d-flex">
				<div class="col-lg-3">
				</div>
				<div class="col-lg-6">
					<div class="card2 card border-0 px-4 py-5">
						<div class="row mb-4 px-3">
							<h6 class="mb-0 mr-4 mt-2"><?= $this->lang->line('sign_in_with') ?></h6>
							<!-- <div class="google text-center mr-3">
								<a class="buttonText" href="<?= $loginGoogle  ?>">
									<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                        <path d="M19.9895 10.1871C19.9895 9.36767 19.9214 8.76973 19.7742 8.14966H10.1992V11.848H15.8195C15.7062 12.7671 15.0943 14.1512 13.7346 15.0813L13.7155 15.2051L16.7429 17.4969L16.9527 17.5174C18.879 15.7789 19.9895 13.221 19.9895 10.1871Z" fill="#4285F4"></path>
                        <path d="M10.1993 19.9313C12.9527 19.9313 15.2643 19.0454 16.9527 17.5174L13.7346 15.0813C12.8734 15.6682 11.7176 16.0779 10.1993 16.0779C7.50243 16.0779 5.21352 14.3395 4.39759 11.9366L4.27799 11.9466L1.13003 14.3273L1.08887 14.4391C2.76588 17.6945 6.21061 19.9313 10.1993 19.9313Z" fill="#34A853"></path>
                        <path d="M4.39748 11.9366C4.18219 11.3166 4.05759 10.6521 4.05759 9.96565C4.05759 9.27909 4.18219 8.61473 4.38615 7.99466L4.38045 7.8626L1.19304 5.44366L1.08875 5.49214C0.397576 6.84305 0.000976562 8.36008 0.000976562 9.96565C0.000976562 11.5712 0.397576 13.0882 1.08875 14.4391L4.39748 11.9366Z" fill="#FBBC05"></path>
                        <path d="M10.1993 3.85336C12.1142 3.85336 13.406 4.66168 14.1425 5.33717L17.0207 2.59107C15.253 0.985496 12.9527 0 10.1993 0C6.2106 0 2.76588 2.23672 1.08887 5.49214L4.38626 7.99466C5.21352 5.59183 7.50242 3.85336 10.1993 3.85336Z" fill="#EB4335"></path>
                    </svg>
										<span>Google</span>
								</a>
							</div> -->
							<!-- <div class="row"> -->
							  <div class="mr-3">
							    <a class="btn" href="<?= $loginGoogle  ?>" role="button" style="border-radius:5px;border: 0px;text-transform:none;background-color:#D6AF4045;">
							      <img class="google-icon-wrapper" width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
							      Google
							    </a>
							  </div>
							<!-- </div> -->
						<!-- 	<div class="facebook text-center mr-3">
								<div class="fa fa-facebook"></div>
							</div>
							<div class="twitter text-center mr-3">
								<div class="fa fa-twitter"></div>
							</div>
							<div class="linkedin text-center mr-3">
								<a href="<?= $loginLinkedin  ?>" class="fa fa-linkedin"></a>
							</div> -->
						</div>
						<div class="row px-3 mb-4">
							<div class="line"></div> <small class="or text-center">Or</small>
							<div class="line"></div>
						</div>
						<form action="<?=$check_login;?>" method="POST" id="formLogin">
							<input type="hidden" name="redirect" value="<?= $current_url; ?>" />
							<div class="row px-3"> <label class="mb-1">
									<h6 class="mb-0 text-sm">Email</h6>
								</label> <input class="mb-4" type="text" id="email" name="email" placeholder=""> </div>
							<div class="row px-3"> <label class="mb-1">
									<h6 class="mb-0 text-sm"><?= $this->lang->line('password') ?></h6>
								</label> <input type="password" id="password" name="password" placeholder=""> </div>
							<div class="row px-3 mb-4">
								<!-- <div class="custom-control custom-checkbox custom-control-inline"> <input id="chk1" type="checkbox" name="chk" class="custom-control chkbx"> <label for="chk1" class="custom-control-label text-sm"><?= $this->lang->line('remember_me') ?></label> </div> <a href="#" class="ml-auto mb-0 text-sm"><?= $this->lang->line('forgot') ?></a> -->
								<div class="custom-control custom-checkbox custom-control-inline" style="padding-left:0rem;margin-right:0.5rem;min-height:1.25rem;">
                  <input id="showPassworda" type="checkbox" onclick="showPassword()" name="chk" class="custom-control chkbx"> <label for="chk1" class="custom-control-label text-sm"><?= $this->lang->line('show_password') ?></label>
                </div>
                <a href="#forgotPassword" data-toggle="modal" class="ml-auto mb-0 text-sm"><?= $this->lang->line('forgot') ?></a>
							</div>
							<div class="row px-3 mb-4">
								<div class="g-recaptcha" data-sitekey="6Le2G_caAAAAAMAUWCuFUb_2ODn-EZSC6I_-BGZP"></div>
							</div>
							<div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center ladda-button" data-style="expand-left"><?= $this->lang->line('login') ?></button> </div>
							<div class="row mb-4 px-3"> <small class="font-weight-bold"><?= $this->lang->line('not_have_account') ?> <a class="text-danger" href="<?= $registrasi;?>"><?= $this->lang->line('registration') ?></a></small> </div>
						</form>
					</div>
				</div>
				<div class="col-lg-3">
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="ti-close" aria-hidden="true"></span>
				</button>
			</div>
			<div class="modal-body" style="height:auto;">
				<div class="row no-gutters">
					<div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
						<div class="quickview-content">
							<h2>
								<?= $this->lang->line('forgot') ?>
							</h2>
							<hr>
							<form action="<?= base_url();?>forgot-password" method="post">
								<div class="size">
									<div class="row">
										<div class="col-lg-12 col-12">
											<h5 class="title">
												Email
											</h5>
											<input type="email" placeholder="ex. academy@Bimago.com" name="forgot_email" required class="form-control">
											<span style="color:red; font-size:12px;">*<?=$this->lang->line('forgot_pass_email');?></span>
										</div>
									</div>
									<br>
									<center>
										<button class="btn btn-blue" style="width:100%" type="submit">Send Email Change Password</button>
									</center>
								</div>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

<script>
function showPassword() {
	var x = document.getElementById("password");
	if (x.type === "password") {
	x.type = "text";
	} else {
	x.type = "password";
	}
}

var l = Ladda.create( document.querySelector( '.ladda-button' ) );
$("#formLogin").submit(function (event) {
	event.preventDefault();
	console.log(l)
	l.start();
	var url = $(this).attr('action');
	var method = $(this).attr('method');
	// var formData = {
 //    email: $("#email").val(),
 //    password: $("#password").val(),
 //  	};
  $.ajax({
    url: url,
    method: method,
    data: $('#formLogin').serialize(),
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'success') {
		toastr.success(hasil["pesan"]);
		l.stop();
		setTimeout(function () {
              window.location = hasil["url"];
            }, 1500);
      }else{
        toastr.error(hasil["pesan"]);
  			grecaptcha.reset();
		l.stop();
      }
    },
    error: function (res) {
      toastr.error("Tidak bisa melakukan login.");
		l.stop();
    },
  });
});

$(document).ready(function(){
		var pesan   = '<?= $this->session->flashdata('pesan');?>'
    var proses  = '<?= $this->session->flashdata('proses');?>'

    if (pesan)
    {
        show_toast(proses, pesan)
    }
});

function show_toast(status, msg)
{
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  Command: toastr[status](msg)

}

</script>
