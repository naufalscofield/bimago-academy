<!-- <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' rel='stylesheet'> -->
<link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous">
</script>
<style>

/* tooltip */


.tool-tip{
	color: #fff;
	background-color: rgba( 0, 0, 0, .7);
	text-shadow: none;
	font-size: .8em;
	visibility: hidden;
	-webkit-border-radius: 7px;
	-moz-border-radius: 7px;
	-o-border-radius: 7px;
	border-radius: 7px;
	text-align: center;
	opacity: 0;
	z-index: 999;
	padding: 3px 8px;
	position: absolute;
	cursor: default;
	-webkit-transition: all 240ms ease-in-out;
	-moz-transition: all 240ms ease-in-out;
	-ms-transition: all 240ms ease-in-out;
	-o-transition: all 240ms ease-in-out;
	transition: all 240ms ease-in-out;
}

.tool-tip,
.tool-tip.top{
	top: auto;
	bottom: 114%;
	left: 50%;
}

.tool-tip.top:after,
.tool-tip:after{
	position: absolute;
	bottom: -12px;
	left: 50%;
	margin-left: -7px;
	content: ' ';
	height: 0px;
	width: 0px;
	border: 6px solid transparent;
    border-top-color: rgba( 0, 0, 0, .7);
}

.tool-tip,
.tool-tip.top{
	width: 250px;
	height: 80px;
	margin-left: -43px;
}

*:not(.on-focus):hover > .tool-tip,
.on-focus input:focus + .tool-tip{
	visibility: visible;
	opacity: 1;
	-webkit-transition: all 240ms ease-in-out;
	-moz-transition: all 240ms ease-in-out;
	-ms-transition: all 240ms ease-in-out;
	-o-transition: all 240ms ease-in-out;
	transition: all 240ms ease-in-out;
}

/* tool tip slide out */

*:not(.on-focus) > .tool-tip.slideIn,
.on-focus > .tool-tip{
	display: block;
}

.on-focus > .tool-tip.slideIn{
	z-index: -1;
}

.on-focus > input:focus + .tool-tip.slideIn{
	z-index: 1;
}

/* top slideIn */

*:not(.on-focus) > .tool-tip.slideIn,
*:not(.on-focus) > .tool-tip.slideIn.top,
.on-focus > .tool-tip.slideIn,
.on-focus > .tool-tip.slideIn.top{
	bottom: 50%;
}

*:not(.on-focus):hover > .tool-tip.slideIn,
*:not(.on-focus):hover > .tool-tip.slideIn.top,
.on-focus > input:focus + .tool-tip.slideIn,
.on-focus > input:focus + .tool-tip.slideIn.top{
	bottom: 110%;
}

.chkbx {
	/* -webkit-appearance:none; */
	width:15px;
	height:15px;
	background:white;
	border-radius:5px;
	border:2px solid #555;
}

.image {
	width: 400px;
	height: 320px
}

.border-line {
	border-right: 1px solid #EEEEEE
}

.facebook {
	background-color: #3b5998;
	color: #fff;
	font-size: 18px;
	padding-top: 5px;
	border-radius: 50%;
	width: 35px;
	height: 35px;
	cursor: pointer
}

.twitter {
	background-color: #1DA1F2;
	color: #fff;
	font-size: 18px;
	padding-top: 5px;
	border-radius: 50%;
	width: 35px;
	height: 35px;
	cursor: pointer
}

.linkedin {
	background-color: #2867B2;
	color: #fff;
	font-size: 18px;
	padding-top: 5px;
	border-radius: 50%;
	width: 35px;
	height: 35px;
	cursor: pointer
}
.google {
	background-color: #e23131;
	color: #fff;
	padding-top: 5px;
	border-radius: 50%;
	width: 35px;
	height: 35px;
	cursor: pointer
}

.line {
	height: 1px;
	width: 45%;
	background-color: #E0E0E0;
	margin-top: 10px
}

.or {
	width: 10%;
	font-weight: bold
}

.text-sm {
	font-size: 14px !important
}

::placeholder {
	color: #BDBDBD;
	opacity: 1;
	font-weight: 300
}

:-ms-input-placeholder {
	color: #BDBDBD;
	font-weight: 300
}

::-ms-input-placeholder {
	color: #BDBDBD;
	font-weight: 300
}

input,
textarea {
	padding: 10px 12px 10px 12px;
	border: 1px solid lightgrey;
	border-radius: 2px;
	margin-bottom: 5px;
	margin-top: 2px;
	width: 100%;
	box-sizing: border-box;
	color: #2C3E50;
	font-size: 14px;
	letter-spacing: 1px
}

input:focus,
textarea:focus {
	-moz-box-shadow: none !important;
	-webkit-box-shadow: none !important;
	box-shadow: none !important;
	border: 1px solid #304FFE;
	outline-width: 0
}

button:focus {
	-moz-box-shadow: none !important;
	-webkit-box-shadow: none !important;
	box-shadow: none !important;
	outline-width: 0
}

a {
	color: inherit;
	cursor: pointer
}

.btn-blue {
	background-color: #1A237E;
	width: 150px;
	color: #fff;
	border-radius: 2px
}

.btn-blue:hover {
	background-color: #9d9ea0;
	cursor: pointer
}

@media screen and (max-width: 1250px) {
	/* .logo {
		margin-left: 0px
	} */

	.image {
		width: 390px;
		height: 310px
	}

	.border-line {
		border-right: none
	}

	.card2 {
		border-top: 1px solid #EEEEEE !important;
		margin: 0px 15px
	}
}
</style>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'></script>
<script type='text/javascript'></script>
<body oncontextmenu='return false' class='snippet-body'>
	<!-- Breadcrumbs -->
	<div class="breadcrumbs">
	<div class="container">
		<div class="row">
		<div class="col-12">
			<div class="bread-inner">
			<ul class="bread-list">
				<!-- <li><a href="index1.html">Login</a></li> -->
				<li><a href="<?=$login_page;?>"><?= $this->lang->line('login') ?><i class="ti-arrow-right"></i></a></li>
				<li class="active"><a href="<?=$register_page;?>"><?= $this->lang->line('registration') ?></a></li>
			</ul>
			</div>
		</div>
		</div>
	</div>
	</div>
	<!-- End Breadcrumbs -->
	<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
		<div class="card card0 border-0">
			<div class="row d-flex">
				<div class="col-lg-2">

				</div>
				<div class="col-lg-8">
					<div class="card2 card border-0 px-4 py-5">
						<div class="row mb-4 px-3">
							<h6 class="mb-0 mr-4 mt-2"><?= $this->lang->line('sign_in_with') ?></h6>
							<!-- <div class="google text-center mr-3">
								<a href="<?= $loginGoogle  ?>"><i class="fa fa-google"></i></a>
							</div> -->
							<div class="mr-3">
								<a class="btn" href="<?= $loginGoogle  ?>" role="button" style="border-radius:5px;border: 0px;text-transform:none;background-color:#D6AF4045;">
									<img class="google-icon-wrapper" width="20px" style="margin-bottom:3px; margin-right:5px" alt="Google sign-in" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png" />
									Google
								</a>
							</div>
							<!-- <div class="facebook text-center mr-3">
								<div class="fa fa-facebook"></div>
							</div>
							<div class="twitter text-center mr-3">
								<div class="fa fa-twitter"></div>
							</div>
							<div class="linkedin text-center mr-3">
								<div class="fa fa-linkedin"></div>
							</div> -->
						</div>
						<div class="row px-3 mb-4">
							<div class="line"></div> <small class="or text-center">Or</small>
							<div class="line"></div>
						</div>
						<form action="<?=$registrasi;?>" method="POST">
							<div class="row">
								<div class="col-md-6">
									<h6 class="mb-0 text-sm"><?= $this->lang->line('first_name') ?> <span style="color:red;">*</span></h6>
									<input class="mb-2" type="text" id="nama_depan" name="nama_depan">
								</div>
								<div class="col-md-6">
									<h6 class="mb-0 text-sm"><?= $this->lang->line('last_name') ?> <span style="color:red;">*</span></h6>
									<input class="mb-2" type="text" id="nama_belakang" name="nama_belakang">
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<h6 class="mb-0 text-sm">Email <span style="color:red;">*</span></h6>
									<input class="mb-2" type="email" id="email" name="email">
								</div>
								<div class="col-md-6">
									<h6 class="mb-0 text-sm">Role <span style="color:red;">*</span></h6>
									<select class="form-control" style="width:100%" id="role" name="role">
										<?php foreach ($role as $key => $value) { ?>
											<?php if ($value['id'] == 2) {
												$option = 'Lecturer';
											}elseif ($value['id'] == 3) {
												$option = 'Student';
											} ?>
											<option style="width:100%" value="<?= $value['id']; ?>" <?= $value['id']== 3 ? 'selected' : ''; ?> ><?=$option;?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="row">
								<div style="clear:both;"></div>
								<div class="col-md-6 on-focus clearfix">
									<h6 class="mb-0 text-sm"><?= $this->lang->line('password') ?> <span style="color:red;">*</span></h6>
									<input class="mb-2" type="password" id="password" name="password">
									<div class="tool-tip  slideIn">Password must be at least 6 characters and must contain lower case letter, one upper case letter and one digit</div>
								</div>
								<div style="clear:both;"></div>
								<div class="col-md-6 on-focus clearfix">
									<h6 class="mb-0 text-sm"><?= $this->lang->line('confirm_password') ?> <span style="color:red;">*</span></h6>
									<input class="mb-2" type="password" id="confirm_password" name="confirm_password">
									<div class="tool-tip  slideIn">Password must be at least 6 characters and must contain lower case letter, one upper case letter and one digit</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<span style="color:red;">* <?= $this->lang->line('required') ?></span>
								</div>
								<div class="col-md-6">
									<span id='message'></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label for="" style="color:blue"><a data-toggle="modal" data-target="#modal_terms"><?= $this->lang->line('agree').' '.$this->lang->line('title_terms') ?></a></label>
								</div>
							</div>
							<br>

							<div class="row mb-4 px-3"> <button style="width:200px" type="submit" class="btn btn-blue text-center ladda-button" data-style="expand-left"><span class="ladda-label"><?= $this->lang->line('registration') ?></span></button> </div>
							<div class="row mb-4 px-3"> <small class="font-weight-bold"><?= $this->lang->line('back_to') ?> <a class="text-danger" href="<?=$login_page;?>">Login</a></small> </div>
						</form>
					</div>
				</div>
				<div class="col-lg-2">

				</div>
			</div>
		</div>
	</div>
</body>
<div class="modal fade" id="modal_terms" tabindex="-1" role="dialog">
     <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
           </div>
           <div class="modal-body">
				<div class="row no-gutters">
					<div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
						<div class="quickview-content">
						<h2><?= $this->lang->line('title_terms') ?></h2>
						<hr>
						<div class="size">
							<?= $terms->terms;?>
						</div>
						</div>
					</div>
				</div>
           </div>
        </div>
     </div>
  </div>
</div>	
<script>
var l = Ladda.create( document.querySelector( '.ladda-button' ) );
$("form").submit(function (event) {
	event.preventDefault();
	l.start();
	var url = $(this).attr('action');
	var method = $(this).attr('method');
	var formData = {
    nama_depan: $("#nama_depan").val(),
    nama_belakang: $("#nama_belakang").val(),
    email: $("#email").val(),
    role: $("#role").val(),
    institusi: $("#institusi").val(),
    password: $("#password").val(),
    confirm_password: $("#confirm_password").val(),
  	};
  $.ajax({
    url: url,
    method: method,
    data: formData,
    success: function (res) {
      var hasil = $.parseJSON(res);
      if (hasil["proses"] == 'success') {
		toastr.success(hasil["pesan"]);
		l.stop();
		setTimeout(function () {
              window.location = hasil["url"];
            }, 2500);
      }else{
        toastr.error(hasil["pesan"]);
		l.stop();
      }
    },
    error: function (res) {
      	toastr.error("Tidak bisa melakukan pendaftaran.");
		l.stop();
    },
  });
});

$('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#confirm_password').html('Matching').css('backgroundColor', 'green');
} else
	$('#confirm_password').html('Not Matching').css('backgroundColor', 'red');
});

function AllowOnlyNumbers(e) {
	e = (e) ? e : window.event;
	var clipboardData = e.clipboardData ? e.clipboardData : window.clipboardData;
	var key = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	var str = (e.type && e.type == "paste") ? clipboardData.getData('Text') : String.fromCharCode(key);

	return (/^\d+$/.test(str));
}
</script>
