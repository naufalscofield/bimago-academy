<style>
    /* .modal-dialog,
    .modal-content {
        height: 70%;
    } */

    /* .modal-body {
        max-height: calc(100% - 120px);
        overflow-y: scroll;
    } */
    .profil {
      border-radius: 60%;
    }
</style>
<div class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="bread-inner">
                    <ul class="bread-list">
                        <li><a href="<?=base_url();?>"><?= $this->lang->line('home');?><i class="ti-arrow-right"></i></a></li>
                        <li class="active"><a href="<?=base_url().'users/users_account';?>"><?= $this->lang->line('my_account');?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumbs -->

<!-- Start Contact -->
<section id="contact-us" class="contact-us section">
    <div class="container">
            <div class="contact-head">
                <div class="row">
                <div class="col-lg-4 col-12 text-center">
                        <div class="single-head">
                            <div class="single-info">
                              <center>
                                <?php if ($this->session->userdata('login_via') == 'solmit') { ?>
                                  <?php if ($data->avatar == null || $data->avatar == '') { ?>
                                    <?php if ($data->jk == 'W') { ?>
                                      <img class="profil" src="<?= base_url();?>assets/default/female_avatar_s0lm1t.png" alt="" style="width:170px;height:170px;">
                                    <?php }else{ ?>
                                      <img class="profil" src="<?= base_url();?>assets/default/male_avatar_s0lm1t.png" alt="" style="width:170px;height:170px;">
                                    <?php } ?>
                                  <?php }else{ ?>
                                    <img class="profil" src="<?= base_url();?>assets/avatar/<?=$data->avatar;?>" alt="" style="width:170px;height:170px;">
                                  <?php } ?>
                                <?php }else{ ?>
                                  <img class="profil" src="<?=$data->avatar;?>" alt="" style="width:170px;height:170px;">
                                <?php } ?>
                              </center>
                              <br>
                              <div class="single-info text-center">
                                <button data-toggle="modal" data-target="#modalPicture" class="btn btn-primary"><?= $this->lang->line('change_picture');?></button>
                              </div>
                            </div>
                            <?php if ($this->session->userdata('login_via') == 'solmit') { ?>
                              <div class="single-info text-center">
                                <center>
                                  <i class="fa fa-key"></i>
                                </center>
                                <button data-toggle="modal" data-target="#modalPassword" class="btn btn-primary"><?= $this->lang->line('change_password') ?></button>
                              </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-8 col-12">
                        <div class="form-main">
                            <div class="title">
                                <h3><?= $this->lang->line('account') ?></h3>
                                <h4><?= $this->lang->line('acc_desc') ?></h4>
                            </div>
                            <form class="form" id="form-user" method="POST" action="<?= base_url();?>users/update-account">
                                <div class="row">
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('first_name') ?></label> <span class="text-danger">*</span>
                                            <input name="nama_depan" value="<?= $data->nama_depan;?>" type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('last_name') ?></label> <span class="text-danger">*</span>
                                            <input name="nama_belakang" value="<?= $data->nama_belakang;?>"type="text">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('your_email') ?></label> <span class="text-danger">*</span>
                                            <?php // $this->session->userdata('login_via') == 'solmit' ? '' : 'disabled';?>
                                            <input name="email" value="<?= $data->email;?>" type="email" readonly="true">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('your_phone') ?></label> <span class="text-danger">*</span>
                                            <input name="no_hp" type="number" value="<?= $data->no_hp;?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('institution');?></label> <span class="text-danger">*</span>
                                            <input name="institusi" type="text" value="<?= $data->institusi;?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <div class="form-group">
                                          <label><?= $this->lang->line('gender');?></label> <span class="text-danger">*</span><br><br>
                                          <input style="width:20px;height:20px;" name="jk" type="radio" value="P" <?= $data->jk == 'P' ? 'checked' : '' ;?>> Pria &emsp;
                                          <input style="width:20px;height:20px;" name="jk" type="radio" value="W" <?= $data->jk == 'W' ? 'checked' : '' ;?>> Wanita
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-12">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('certificate_name');?></label> <span class="text-danger">*</span>
                                            <input name="certificate_name" type="text" value="<?= $data->nama_sertifikat;?>">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group button">
                                            <button type="submit" data-style="expand-left" class="btn btn-primary ladda-button ladda-user"><?= $this->lang->line('update_profil');?></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!--/ End Contact -->

<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <form action="<?=base_url();?>users/change-password" method="POST" id="form-password">
                <div class="modal-body" style="height:auto;">
                    <div class="row no-gutters">
                        <div class="col-md-6 col-md-12 col-sm-12 col-xs-12">
                            <div class="quickview-content">
                                <h2><?= $this->lang->line('change_password') ?></h2>
                                <div class="size">
                                    <div class="row">
                                            <div class="col-lg-6 col-6">
                                                <h5 class="title"><?= $this->lang->line('old_pass') ?></h5>
                                                <input type="password" required name="old_password" id="old_password" style="width:100%">
                                            </div>
                                            <div class="col-lg-6 col-6">
                                                <h5 class="title"><?= $this->lang->line('new_pass') ?></h5>
                                                <input type="password" required name="new_password" id="new_password" style="width:100%">
                                            </div>
                                        </div>
                                        <br>
                                        <center>
                                            <button class="btn btn-primary ladda-pass" style="width:100%" type="submit"><?= $this->lang->line('save') ?></button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPicture" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
            </div>
            <form action="<?=base_url();?>users/change-picture" method="POST" enctype="multipart/form-data" id="form-picture">
                <div class="modal-body" style="height:auto;">
                    <div class="row no-gutters">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="quickview-content">
                                <h2><?= $this->lang->line('change_picture') ?></h2>
                                <div class="size">
                                    <div class="row">
                                            <div class="col-lg-6">
                                                <h5 class="title"><?= $this->lang->line('select_an_image') ?></h5>
                                                <input type='file' id="file" name="avatar"/>
                                            </div>
                                            <div class="col-lg-6">
                                                <center>
                                                <img id="blah" src="#" alt="your image" style="width:150px;height:150px;"/>
                                              </center>
                                            </div>
                                        </div>
                                        <br>
                                        <center>
                                            <button class="btn btn-primary ladda-pic" style="width:100%" type="submit"><?= $this->lang->line('save') ?></button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= base_url()?>application/views/users/users_account/main.js"></script>
<script>

$(document).ready(function(){
  test()
  $('#blah').hide()
})
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').show()
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#file").change(function(){
    readURL(this);
});


    $("#form-password").submit(function (event) {
      var l = Ladda.create( document.querySelector( '.ladda-pass' ) );
    	event.preventDefault();
    	l.start();
    	var url = $(this).attr('action');
    	var method = $(this).attr('method');
    	var formData = {
        new_password: $("#new_password").val(),
        old_password: $("#old_password").val(),
      	};
      $.ajax({
        url: url,
        method: method,
        data: formData,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["proses"] == 'success') {
            l.stop();
        		toastr.success(hasil["pesan"]);
            setTimeout(function () {
              location.reload(true);
            }, 1500);
          }else{
            l.stop();
            toastr.error(hasil["pesan"]);
          }
        },
        error: function (res) {
          l.stop();
          toastr.error("Oops, something went wrong, change password failed.");
        },
      });
    });


    $("#form-user").submit(function (event) {
      var l = Ladda.create( document.querySelector( '.ladda-user' ) );
    	event.preventDefault();
    	l.start();
    	var url = $(this).attr('action');
    	var method = $(this).attr('method');
      var formData = $(this).serialize();
      $.ajax({
        url: url,
        method: method,
        data: formData,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["proses"] == 'success') {
            l.stop();
        		toastr.success(hasil["pesan"]);
            setTimeout(function () {
              location.reload(true);
            }, 1500);
          }else{
            l.stop();
            toastr.error(hasil["pesan"]);
          }
        },
        error: function (res) {
          l.stop();
          toastr.error("Oops, something went wrong, change password failed.");
        },
      });
    });
</script>
