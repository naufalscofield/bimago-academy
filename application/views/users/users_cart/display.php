<style media="screen">
  @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

  body {
    background-color: #eee;
  }

  /* .mt-100 {
    margin-top: 100px
  } */

  .card {
    margin-bottom: 30px;
    border: 0;
    -webkit-transition: all .3s ease;
    transition: all .3s ease;
    letter-spacing: .5px;
    border-radius: 8px;
    -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
    box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05)
  }

  .card .card-header {
    background-color: #fff;
    border-bottom: none;
    padding: 24px;
    border-bottom: 1px solid #f6f7fb;
    border-top-left-radius: 8px;
    border-top-right-radius: 8px
  } */

  /* .card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
  }

  .card .card-body {
    padding: 30px;
    background-color: transparent
  } */

  .btn-primary,
  .btn-primary.disabled,
  .btn-primary:disabled {
    background-color: #4466f2 !important;
    border-color: #4466f2 !important
  }
</style>
<!-- Breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="bread-inner">
						<ul class="bread-list">
							<li><a href="index1.html"><?= $this->lang->line('home'); ?><i class="ti-arrow-right"></i></a></li>
							<li class="active"><a href="blog-single.html"><?= $this->lang->line('cart'); ?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Breadcrumbs -->

	<!-- Shopping Cart -->
	<div class="shopping-cart section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if (!empty($cart)) { ?>
						<!-- Shopping Summery -->
						<table class="table shopping-summery">
							<thead>
								<tr class="main-hading">
									<th width="15%"><?= $this->lang->line('product') ?></th>
									<th width="40%"><?= $this->lang->line('name') ?></th>
									<th width="25%"><?= $this->lang->line('type') ?></th>
									<th width="15%" class="text-center">TOTAL</th>
									<th width="5%" class="text-center"><i class="ti-trash remove-icon"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$total = 0;
								foreach ($cart as $key => $value) {
									//  $total += $value['price'];
									?>
									<tr id="row_<?=$value['id_course'];?>">
										<td class="image" data-title="Product"><center><img src="<?=base_url().$value['image'];?>" alt="#" style="width:100%;height:100%;"></center></td>
										<td class="product-des" data-title="Name">
											<p class="product-name"><a href="#"><?=$value['name']?></a></p>
										</td>
										<td class="price" data-title="Type"><span><?= $value['type_course'];?></span></td>
										<td class="price" data-title="Price"><span><?= rupiah($value['price']);?></span></td>
										<td class="action" data-title="Remove"><a href="#" id="<?= $value['id_course'];?>" url="<?=$delete_row_cart;?>" onclick="delete_row_cart(this);"><i class="ti-trash remove-icon"></i></a></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<!--/ End Shopping Summery -->
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<!-- Total Amount -->
					<div class="total-amount" id="exist-cart" style="display:<?= (!empty($cart)) ? 'block' : 'none'; ?>">
						<div class="row">
							<div class="col-lg-8 col-md-5 col-12">
								<div class="left">
									<!-- <div class="coupon">
										<form action="#" target="_blank">
											<input name="Coupon" placeholder="Enter Your Coupon">
											<button class="btn"><?= $this->lang->line('apply') ?></button>
										</form>
									</div> -->
								</div>
							</div>
							<div class="col-lg-4 col-md-7 col-12">
								<div class="right">
									<ul>
										<li><?= $this->lang->line('cart_subtotal') ?><span id="total-amount"><?=$cart_total;?></span></li>
										<!-- <li>You Save<span>$20.00</span></li> -->
										<li class="last"><?= $this->lang->line('you_pay') ?><span id="total-amount-2"><?=$cart_total;?></span></li>
									</ul>
									<div class="button5">
										<a href="<?= $checkout;?>" class="btn">Checkout</a>
										<a href="<?= $course;?>" class="btn"><?= $this->lang->line('continue') ?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/ End Total Amount -->
				<?php } ?>
					<div class="container-fluid" id="empty-cart" style="display:<?= (!empty($cart)) ? 'none' : 'block'; ?>">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-body cart">
										<div class="col-sm-12 empty-cart-cls text-center"> <img src="<?=base_url().'assets/default/empty-cart.png';?>" width="220" height="220" class="img-fluid mb-4 mr-3">
											<h3><strong><?= $this->lang->line('your_shopping_cart') ?>!</strong></h3>
											<h4><?= $this->lang->line('immediately') ?></h4> <a href="<?=$course;?>" class="btn btn-primary cart-btn-transform m-3" style="color:white;" data-abc="true"><?= $this->lang->line('continue') ?></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ End Shopping Cart -->
