<!-- Shopping Item -->
<div class="shopping-item">
    <div class="dropdown-cart-header">
        <span id="total-count-2"><?= count($cart);?></span> <span> Item</span>
        <a href="<?=$cart_page;?>"><?= $this->lang->line('view_cart') ?></a>
    </div>
    <ul class="shopping-list">
    <?php foreach ($cart as $key => $value) { ?>
        <li id="row_<?=$value['id'];?>">
        <a href="#" class="remove" title="Remove this item" id="<?= $value['id_course'];?>" url="<?=$delete_row_cart;?>" onclick="delete_row_cart(this);"><i class="fa fa-remove"></i></a>
        <a class="cart-img" href="#"><img src="<?= base_url().$value['image'];?>" alt="#"></a>
        <h4><a href="#"><?= $value['name'];?></a></h4>
        <p><?= $value['type_course'];?></p>
        <p class="quantity"><span class="amount"><?= rupiah($value['price']);?></span></p>
        </li>
    <?php } ?>
    </ul>
    <div class="bottom">
        <div class="total">
            <span>Total</span>
            <span class="total-amount" id="total-amount"><?= $cart_total;?></span>
        </div>
        <a href="<?=$checkout;?>" class="btn animate">Checkout</a>
    </div>
</div>
