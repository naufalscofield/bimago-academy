<style>

.style-1 {
  del {
    color: rgba(red, 0.5);
    text-decoration: none;
    position: relative;
    &:before {
      content: " ";
      display: block;
      width: 100%;
      border-top: 2px solid rgba(red, 0.8);
      height: 12px;
      position: absolute;
      bottom: 0;
      left: 0;
      transform: rotate(-7deg);
    }
  }
  ins {
    color: green;
    font-size: 32px;
    text-decoration: none;
    padding: 1em 1em 1em .5em;
  }
}

}
</style>
<br>
<div id="all">
  <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="contact" class="box">
            <h3><?=$judul;?></h3>
            <p><?= $this->lang->line('types_of_courses') ?></p>
            <hr>
            <div id="accordion">
              <form action="<?=$add_cart;?>" method="post" name="form-add_cart" id="form-add_cart">
                <input type="checkbox" name="select_type[<?=$id;?>][0]" class="choice d-none" value="0" checked>
                <?php
                $tanggal = $disabled_led = $disabled_training = $disabled_webinar = '';
                foreach ($type_course as $key2 => $value2): ?>
                  <?php
                  if (in_array($value2['id_type_course'],[4,3,2])) {
                    if ($pelaksanaan_led == date('Y-m-d')){
                      $disabled_led = 'disabled';
                    }
                    if ($pelaksanaan_training < date('Y-m-d')){
                      $disabled_training = 'disabled';
                    }
                    if ($pelaksanaan_webinar == date('Y-m-d')){
                      $disabled_webinar = 'disabled';
                    }
                    if ($value2['id_type_course'] == 3) {
                      $tanggal = $pelaksanaan_training;
                    }elseif ($value2['id_type_course'] == 2) {
                      $tanggal = $pelaksanaan_led;
                    }elseif ($value2['id_type_course'] == 4) {
                      $tanggal = $pelaksanaan_webinar;
                    }
                  }
                  ?>
                  <?php if (in_array($value2['id_type_course'],$purchased)) { ?>
                    <input type="checkbox" name="select_type[<?=$id;?>][<?=$value2['id_type_course'];?>]" class="choice" value="<?=$value2['id_type_course'];?>" disabled checked>
                  <?php }else{ ?>
                    <?php if (in_array($value2['id_type_course'],$in_cart)) { ?>
                      <input type="checkbox" name="select_type[<?=$id;?>][<?=$value2['id_type_course'];?>]" class="choice" value="<?=$value2['id_type_course'];?>" checked>
                    <?php }else{ ?>
                      <?php if ($value2['id_type_course'] == 2): ?>
                         <input type="checkbox" name="select_type[<?=$id;?>][<?=$value2['id_type_course'];?>]" class="choice" value="<?=$value2['id_type_course'];?>" <?= $disabled_led ?> >
                      <?php elseif($value2['id_type_course'] == 3): ?>
                        <input type="checkbox" name="select_type[<?=$id;?>][<?=$value2['id_type_course'];?>]" class="choice" value="<?=$value2['id_type_course'];?>" <?= $disabled_training ?>>
                      <?php elseif($value2['id_type_course'] == 4): ?>
                        <input type="checkbox" name="select_type[<?=$id;?>][<?=$value2['id_type_course'];?>]" class="choice" value="<?=$value2['id_type_course'];?>" <?= $disabled_webinar ?> >
                      <?php else: ?>
                        <input type="checkbox" name="select_type[<?=$id;?>][<?=$value2['id_type_course'];?>]" class="choice" value="<?=$value2['id_type_course'];?>">
                      <?php endif ?>
                    <?php } ?>
                  <?php } ?>
                  <?php
                    if ($value2['id_type_course'] == 2) {
                      $view_tgl = !empty($tanggal) ? ' (Live on '.eng_date($tanggal) . ')' : '';
                    }elseif ($value2['id_type_course'] == 3) {
                      $view_tgl = !empty($tanggal) ? ' ('.eng_date($tanggal) . ')' : '';
                    }elseif ($value2['id_type_course'] == 4) {
                      $view_tgl = !empty($tanggal) ? ' ('.eng_date($tanggal) . ')' : '';
                    }elseif ($value2['id_type_course'] == 1){
                      $view_tgl = '';
                    }
                  ?>
                  <span><?= $value2['type_course'].' '.$view_tgl;?></span><br>
                  <?php
                    if ($value2['discount'] != NULL || $value2['discount'] != 0)
                    {
                  ?>
                    <div class="style-1">
                      <del>
                        <span class="amount"><?= rupiah($value2['harga']);?></span>
                      </del>
                      <ins>
                        <?php
                          $discount = ($value2['discount']/100) * $value2['harga'];
                          $new_price= $value2['harga']  - $discount;
                        ?>
                        <span class="amount"><?= rupiah($new_price);?></span>
                      </ins>
                    </div>
                  <?php
                    } else
                    {
                  ?>
                      <span><?= rupiah($value2['harga']);?></span><br><br>
                  <?php
                    }
                  ?>
                <?php endforeach; ?>

              </form>
            </div>
            <!-- /.accordion-->
          </div>
        </div>
        <!-- /.col-lg-9-->
      </div>
    </div>
  </div>
</div>
<br>
<script type="text/javascript">
$(document).ready(function(){
  $(".choice").click(function(){
    var form = $("#" + $(this).closest('form').attr('name'));
    var formData = $('#form-add_cart').serialize();
    var method = form.attr("method");
    var action = form.attr("action");
    add_cart(form, formData, method, action);
  });
});
</script>
