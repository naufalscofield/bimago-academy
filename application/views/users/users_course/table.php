<br>
<?php if ($data['status'] == 200) { ?>
  <!-- <form action="<?=$add_cart;?>" method="post" name="form-add_cart" id="form-add_cart"> -->
  <?php foreach ($data['data'] as $key => $value) { ?>
  <div class="row">
    <div class="col-lg-4 col-md-6 col-12">
      <div class="single-product">
        <div class="product-img">
          <a href="<?= $detail.encode_url_share_id($value['id']);?>">
            <img class="default-img" src="<?= base_url().$value['image'];?>" alt="#">
            <!-- <img class="hover-img" src="https://via.placeholder.com/550x750" alt="#"> -->
            <span class="new"><?=$value['modul'];?></span>
            <?php if ($value['have_free'] == TRUE) { ?>
              <span class="out-of-stock"><?= $this->lang->line('free') ?></span>
            <?php } ?>
          </a>
        </div>

      </div>
    </div>
    <div class="col-lg-8 col-md-6 col-12">
      <div class="single-product">
        <div class="product-content">
          <h3><a href="<?= $detail.encode_url_share_id($value['id']);?>"><?=$value['judul'];?></a></h3>
          <h6><a><p align="justify" style="font-size:13px;line-height: 0.5cm;"><?=$value['deskripsi_singkat'];?></p></a></h6>
          <p style="color:grey;font-size:12px;"><?= $value['nama_depan'].' '.$value['nama_belakang'].' ('.$value['institusi'].')'; ?></p>
          <div>
          <div class="product-price">
                      <?php if ($value['discount'] != null && $value['discount'] != 0 ):
                          $discount = ($value['discount']/100) * $value['harga'];
                          $new_price= $value['harga']  - $discount;
                    ?>
            <span class="old"> <?= rupiah($value['harga']); ?></span>
            <span> <?= rupiah($new_price); ?></span>
                    <?php else: ?>
            <span> <?= rupiah($value['harga']); ?></span>
          <?php endif ?>
          </div>
            <span style="color:orange;"><?= $value['rating'];?></span>
            <?php if ($value['rating_belakang'] == '' || $value['rating_belakang'] == 0) { ?>
              <?php if ($value['rating_depan'] == '' || $value['rating_depan'] == 0) { ?>
                <?php $not = 5; ?>
                <?php for ($i=0; $i < $not; $i++) { ?>
                  <span class="fa fa-star-o"></span>
                <?php } ?>
              <?php }else{ ?>
                <?php $not = 5 - $value['rating_depan'];
                for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                  <span class="fa fa-star checked"></span>
                <?php } ?>
                <?php for ($i=0; $i < $not; $i++) { ?>
                  <span class="fa fa-star-o"></span>
                <?php } ?>
              <?php } ?>
            <?php }else{ ?>
              <?php $not = 4 - $value['rating_depan'];
              for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                <span class="fa fa-star checked"></span>
              <?php } ?>
              <span class="fa fa-star-half-o checked"></span>
              <?php for ($i=0; $i < $not; $i++) { ?>
                <span class="fa fa-star-o"></span>
              <?php } ?>
            <?php } ?>
            (<?= $value['reviewer'];?>)
          </div>
          <div class="default-social">
            <h4 class="share-now"><?= $this->lang->line('share') ?>:</h4>
            <ul>
              <li><a class="facebook" href="https://www.facebook.com/share.php?u=<?=base_url().'users/users_course/detail/'.encode_url_share_id($value['id']);?>"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="http://www.linkedin.com/shareArticle?mini=true&url=<?=base_url().'users/users_course/detail/'.encode_url_share_id($value['id']);?>"><i class="fa fa-linkedin-square"></i></a></li>
              <li><a class="youtube" href="whatsapp://send?text=<?=base_url().'users/users_course/detail/'.encode_url_share_id($value['id']);?>"><i class="fa fa-whatsapp"></i></a></li>
            </ul>
          </div>
          <br>

          <div class="add-to-cart">
            <a class="capsule ladda-button cart_<?=$value['id'];?>" data-style="expand-left" url="<?=$detail_cart;?>" id="<?=$value['id'];?>" onclick="openModalCart(this)"><p style="color:white;"><?= $this->lang->line('add_to_cart') ?></p></a>
            <a href="<?= $detail.encode_url_share_id($value['id']);?>" class="capsule"><p style="color:white;">Detail</p></a>

            <?php if ($this->session->userdata('token') != '') { ?>
              <?php if (in_array($value['id'], $wishlisted)) { ?>
                <a id="<?=$value['id'];?>" url="<?=$wishlist;?>" onclick="add_remove_wishlist(this);" data-style="expand-left" class="capsule ladda-button ladda-wish<?=$value['id'];?>"><i id="wish_<?=$value['id'];?>" class="fa fa-heart love"></i></a>
              <?php }else{ ?>
                <a id="<?=$value['id'];?>" url="<?=$wishlist;?>" onclick="add_remove_wishlist(this);" data-style="expand-left" class="capsule ladda-button ladda-wish<?=$value['id'];?>"><i id="wish_<?=$value['id'];?>" class="fa fa-heart"></i></a>
              <?php } ?>
            <?php }else{ ?>
              <a href="<?=$login_page;?>" class="capsule min"><i class="fa fa-heart"></i></a>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <?php } ?>
<!-- </form> -->
<?php }else{ ?>
  <style media="screen">
    @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

    body {
      background-color: #eee;
    }

    /* .mt-100 {
      margin-top: 100px
    } */

    .card {
      margin-bottom: 30px;
      border: 0;
      -webkit-transition: all .3s ease;
      transition: all .3s ease;
      letter-spacing: .5px;
      border-radius: 8px;
      -webkit-box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05);
      box-shadow: 1px 5px 24px 0 rgba(68, 102, 242, .05)
    }

    .card .card-header {
      background-color: #fff;
      border-bottom: none;
      padding: 24px;
      border-bottom: 1px solid #f6f7fb;
      border-top-left-radius: 8px;
      border-top-right-radius: 8px
    } */

    /* .card-header:first-child {
      border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
    }

    .card .card-body {
      padding: 30px;
      background-color: transparent
    } */

    .btn-primary,
    .btn-primary.disabled,
    .btn-primary:disabled {
      background-color: #4466f2 !important;
      border-color: #4466f2 !important
    }
  </style>
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="card-body cart">
                      <div class="col-sm-12 empty-cart-cls text-center"> <img src="<?=base_url().'assets/default/empty_course.png';?>" width="500" height="500" class="img-fluid mb-4 mr-3">
                          <h3><strong>Course you want is empty!</strong></h3>
                          <h4><?= $this->lang->line('immediately') ?></h4> <a href="<?=$course;?>" class="btn btn-primary cart-btn-transform m-3" style="color:white;" data-abc="true"><?= $this->lang->line('continue') ?></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

<?php } ?>
<div class="pagination text-center">
  <a class="page-link-num" href="#">&laquo;</a>
  <?php
    for ($i = 1; $i <= $data['total_page']; $i++)
    {
  ?>
      <a class="<?= ($pagenum == $i) ? 'active' : '';?> page-link-num" data-id="<?= $i;?>" id="movePage" href="#"><?= $i; ?></a>
  <?php
    }
  ?>
  <a class="page-link-num" href="#">&raquo;</a>
</div>

<script>
$(document).on("click", ".page-link-num", function(event){
  event.stopPropagation();
  event.stopImmediatePropagation();
  var pagenum = $(this).attr('data-id')
  $("#pagenum").val(pagenum)
  $('#table_content_users_course').empty()
  load_content('table_content_<?php echo $class; ?>','form-table','html');

});

</script>
