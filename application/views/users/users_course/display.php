<style>
  .underline {
    border-bottom: 2px solid currentColor;
  }
  /* .checked {
    color: orange;
  }
  .love {
    color: red;
  } */
  /* Card styles */
  .card{
      background-color: #fff;
      height: auto;
      width: auto;
      overflow: hidden;
      margin: 12px;
      border-radius: 5px;
      box-shadow: 9px 17px 45px -29px
                  rgba(0, 0, 0, 0.44);
  }

  /* Card image loading */
  .card__image img {
      width: 100%;
      height: 100%;
  }

  .card__image.loading {
      height: 200px;
      width: 400px;
  }

  /* Card title */
  .card__title {
      padding: 8px;
      font-size: 22px;
      font-weight: 700;
  }

  .card__title.loading {
      height: 1rem;
      width: 50%;
      margin: 1rem;
      border-radius: 3px;
  }

  /* Card description */
  .card__description {
      padding: 8px;
      font-size: 16px;
  }

  .card__description.loading {
      height: 3rem;
      margin: 1rem;
      border-radius: 3px;
  }

  /* The loading Class */
  .loading {
      position: relative;
      background-color: #e2e2e2;
  }

  /* The moving element */
  .loading::after {
      display: block;
      content: "";
      position: absolute;
      width: 100%;
      height: 100%;
      transform: translateX(-100%);
      background: -webkit-gradient(linear, left top,
                  right top, from(transparent),
                  color-stop(rgba(255, 255, 255, 0.2)),
                  to(transparent));

      background: linear-gradient(90deg, transparent,
              rgba(255, 255, 255, 0.2), transparent);

      /* Adding animation */
      animation: loading 0.8s infinite;
  }

  /* Loading Animation */
  @keyframes loading {
      100% {
          transform: translateX(100%);
      }
  }

  /* Pagination links */
  .pagination a {
    color: black;
    float: center;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
  }

  /* Style the active/current link */
  .pagination a.active {
    background-color: dodgerblue;
    color: white;
  }

  /* Add a grey background color on mouse-over */
  .pagination a:hover:not(.active) {background-color: #ddd;}
</style>

<!-- Breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="bread-inner">
          <ul class="bread-list">
            <li><a href="<?=base_url();?>"><?= $this->lang->line('home'); ?><i class="ti-arrow-right"></i></a></li>
            <li class="active"><a href="<?=base_url().'users/users_course';?>"><?= $this->lang->line('course_list'); ?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Breadcrumbs -->

<!-- Product Style -->
<form id="form-table" name="form-table" data-url="<?= $get_data_url; ?>/">
  <section class="product-area shop-sidebar shop section">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-12">
          <div class="shop-sidebar">
              <!-- Single Widget -->
              <div class="single-widget category">
                <h3 class="title"><?= $this->lang->line('categories') ?></h3>
                <ul class="check-box-list">
                  <input type="hidden" name="filter" value="1">
                  <?php foreach ($modul['data'] as $key => $value) { ?>
                    <li><input class="checkbox-inline cbxModul" id="modul" type="checkbox" name="modul[]" value="<?=$value['id'];?>">&emsp;<a><?=$value['modul'];?></a></li>
                  <?php } ?>
                </ul>
              </div>
              <!--/ End Single Widget -->
              <!-- Single Widget -->
              <div class="single-widget category">
                <h3 class="title"><?= $this->lang->line('type_course'); ?></h3>
                <ul class="check-box-list">
                  <?php foreach ($type_course as $key => $value) { ?>
                    <li><input class="checkbox-inline cbxType" id="type" type="checkbox" name="type[]" value="<?=$value['id'];?>">&emsp;<a><?=$value['type_course'];?></a></li>
                  <?php } ?>
                </ul>
              </div>
              <!--/ End Single Widget -->
              <!-- Single Widget -->
              <div class="single-widget category">
                <h3 class="title"><?= $this->lang->line('rating_course'); ?></h3>
                <ul class="check-box-list">
                  <li>
                    <input class="checkbox-inline" type="checkbox" name="rating[]" id="rating" value="5"> &nbsp;
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    &nbsp;& UP
                  </li>
                  <li>
                    <input class="checkbox-inline" type="checkbox" name="rating[]" id="rating" value="4"> &nbsp;
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star-o"></span>
                    &nbsp;& UP
                  </li>
                  <li>
                    <input class="checkbox-inline" type="checkbox" name="rating[]" id="rating" value="3"> &nbsp;
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star-o"></span>
                    <span class="fa fa-star-o"></span>
                    &nbsp;& UP
                  </li>
                  <li>
                    <input class="checkbox-inline" type="checkbox" name="rating[]" id="rating" value="2"> &nbsp;
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star-o"></span>
                    <span class="fa fa-star-o"></span>
                    <span class="fa fa-star-o"></span>
                    &nbsp;& UP
                  </li>
                  <li>
                    <input class="checkbox-inline" type="checkbox" name="rating[]" id="rating" value="1"> &nbsp;
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star-o"></span>
                    <span class="fa fa-star-o"></span>
                    <span class="fa fa-star-o"></span>
                    <span class="fa fa-star-o"></span>
                    &nbsp;& UP
                  </li>
                </ul>
              </div>
              <!--/ End Single Widget -->
              <!-- Shop By Price -->
                <!-- <div class="single-widget range">
                  <h3 class="title">
                    <input type="checkbox" name="use_price" value="1">
                    Shop by Price
                  </h3>
                  <div class="price-filter">
                    <div class="price-filter-inner">
                      <div id="slider-range"></div>
                        <div class="price_slider_amount">
                        <div class="label-input">
                          <span>Range (Ribu Rupiah):</span>
                          <input type="text" id="amount" name="price" placeholder="Add Your Price"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <!--/ End Shop By Price -->
                <!-- <br>
                <center>
                  <div class="add-to-cart">
                    <a onclick="load_content('table_content_<?php echo $class; ?>','form-table','html')" class="btn"><p style="color:white;"><?= $this->lang->line('apply_filter') ?></p></a>
                  </div>
                </center>
                <br> -->
          </div>
        </div>
        <div class="col-lg-9 col-md-8 col-12">
          <div class="row">
            <div class="col-12">
              <!-- Shop Top -->
              <div class="shop-top">
                <div class="shop-shorter">
                  <div class="single-shorter">
                    <label><?= $this->lang->line('show') ?> :</label>
                    <select name="limit" id="limit">
                      <option selected="selected">5</option>
                      <option>10</option>
                      <option>15</option>
                      <option>20</option>
                    </select>
                  </div>
                  <div class="single-shorter">
                    <label><?= $this->lang->line('sort_by') ?> :</label>
                    <select id="sort" name="sort">
                      <option selected="selected" value="name"><?= $this->lang->line('name') ?></option>
                      <option value="price" ><?= $this->lang->line('price') ?></option>
                      <option value="date" ><?= $this->lang->line('date') ?></option>
                    </select>
                  </div>
                </div>
              </div>
              <!--/ End Shop Top -->
            </div>
          </div>
            <div class="card skeleton">
              <div class="card__image loading"></div>
              <div class="card__title loading"></div>
              <div class="card__description loading"></div>
            </div>
            <div class="card skeleton">
              <div class="card__image loading"></div>
              <div class="card__title loading"></div>
              <div class="card__description loading"></div>
            </div>
            <div class="card skeleton">
              <div class="card__image loading"></div>
              <div class="card__title loading"></div>
              <div class="card__description loading"></div>
            </div>
            <div class="card skeleton">
              <div class="card__image loading"></div>
              <div class="card__title loading"></div>
              <div class="card__description loading"></div>
            </div>
            <div class="card skeleton">
              <div class="card__image loading"></div>
              <div class="card__title loading"></div>
              <div class="card__description loading"></div>
            </div>
            <div id='table_content_<?php echo $class; ?>' data-source="<?php echo $data_source; ?>" data-filter="#form_tabel" data-type="json">
            </div>
            <div>
            <input type="hidden" value="" name="keyword" id="keyword">
            <input type="hidden" value="1" name="pagenum" id="pagenum">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</form>
<!--/ End Product Style 1  -->


<script>
$(document).ready(function(){
  const queryString   = window.location.search;
  const urlParams     = new URLSearchParams(queryString);
  const keywordJudul  = urlParams.get('judul')
  const keywordModul  = urlParams.get('modul')
  const keywordType  = urlParams.get('type')

  if (keywordJudul != '' || (keywordModul != '') || (keywordType != ''))
  {
    $("#keyword").val(keywordJudul)
    $("#searchBox").val(keywordJudul)
    $(".cbxModul[value='"+keywordModul+"']").prop("checked","true");
    $(".cbxType[value='"+keywordType+"']").prop("checked","true");
    load_content('table_content_<?php echo $class; ?>','form-table','html');
  } else
  {
    load_content('table_content_<?php echo $class; ?>','form-table','html');
  }

  $(document).on("change", "#limit", function(){
    load_content('table_content_<?php echo $class; ?>','form-table','html');
  })

  $(document).on("change", "#sort", function(){
    load_content('table_content_<?php echo $class; ?>','form-table','html');
  })

  $(document).on("change", "#modul", function(){
    load_content('table_content_<?php echo $class; ?>','form-table','html');
  })

  $(document).on("change", "#type", function(){
    load_content('table_content_<?php echo $class; ?>','form-table','html');
  })

  $(document).on("change", "#rating", function(){
    load_content('table_content_<?php echo $class; ?>','form-table','html');
  })

  $(".getModal").click(function(event){
    var id = $(this).attr('id');
      $.ajax({
          url: "<?=$data_modal;?>",
          method: "POST",
          data: {id:id},
          success: function(data){
            $('#dataCourseModal').html(data);
            $('#courseModal').modal('show');
          }
      });
    });


  });
</script>
