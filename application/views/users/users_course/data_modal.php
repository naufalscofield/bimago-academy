<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
</div>
<div class="modal-body">
  <div class="row no-gutters">
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
      <!-- Product Slider -->
        <div class="product-gallery">
          <div class="quickview-slider-active">
            <div class="single-slider">
              <!-- <img src="https://via.placeholder.com/569x528"  alt="#"> -->
              <img src="<?= base_url().$data['gambar'];?>" style="width:569px;height:510px;" alt="#">
            </div>

          </div>
        </div>
      <!-- End Product slider -->
    </div>
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
      <div class="quickview-content">
        <h2><?= $data['judul'];?></h2>
        <div class="quickview-ratting-review">
          <div class="quickview-ratting-wrap">
            <div class="quickview-ratting">
              <i class="yellow fa fa-star"></i>
              <i class="yellow fa fa-star"></i>
              <i class="yellow fa fa-star"></i>
              <i class="yellow fa fa-star"></i>
              <i class="fa fa-star"></i>
            </div>
            <a href="#"> (1 customer review)</a>
          </div>
          <div class="quickview-stock">
            <span><i class="fa fa-check-circle-o"></i> <?= $this->lang->line('in_stock') ?></span>
          </div>
        </div>
        <h3><?= rupiah($data['harga']);?></h3>
        <div class="quickview-peragraph">
          <?= $data['deskripsi_singkat'];?>
        </div>
        <br>
        <div class="add-to-cart">
          <a href="#" class="btn" url="<?=$add_cart;?>" id="<?=$data['id'];?>" onclick="add_cart(this)"><?= $this->lang->line('add_to_cart') ?></a>
          <a href="#" class="btn min"><i class="ti-heart"></i></a>
          <a href="#" class="btn min"><i class="fa fa-compress"></i></a>
        </div>
        <div class="default-social">
          <h4 class="share-now"></i> <?= $this->lang->line('share') ?>:</h4>
          <ul>
            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a class="youtube" href="#"><i class="fa fa-pinterest-p"></i></a></li>
            <li><a class="dribbble" href="#"><i class="fa fa-google-plus"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script> -->
<!-- <script src="<?= base_url();?>assets/eshop/eshop/js/custom.js"></script> -->
<script type="text/javascript">
  $("#simpan").click(function(){
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
    bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function(result){
      if(result == true) {
        $.ajax({
          type: form.attr("method"),
          url: form.attr("action"),
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          success: function (res) {
            var hasil = $.parseJSON(res);
            if (hasil["status"] == 200) {
               toastr.success(hasil["pesan"]);
               setTimeout(function () {
                 return hasil["status"];
               }, 1500);
               $('#ModalLarge').modal('hide');
               setTimeout(function () {
                 location.reload(true);
               }, 1500);
             }else{
               toastr.error(hasil["pesan"]);
               result = false;
               return hasil["status"];
             }
           },
           error: function (res) {
             toastr.error("Data tidak dapat disimpan.");
             result = false;
             return false;
           },
        });
      }
    });
  });
</script>
