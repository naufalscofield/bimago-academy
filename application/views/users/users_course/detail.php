<style>
  div.modal-dialog {
    width: 800px;
    margin: 5% auto 8%;
  }
  div.modal-dialog2 {
    width: 400px;
    height: 200px;
    margin: 5% auto 8%;
  }
  .trailer-icon {
    color: white;
    font-size: 70px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
  }
  img.detail-image {
    width:950px;height:460px;
  }
  div.layer {
    /* background: linear-gradient(180deg ,rgba(30,30,28,0) 0%,rgba(30,30,28,0.9) 100%); */
    color: white;
    font-size: 20px;
    position: absolute;
    top: 60%;
    left: 50%;
    height: 200px;
    transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    text-align: center;
  }
  .checked {
    color: orange;
  }
  .love {
    color: red;
  }

  .rating {
      border: none;
      float: left;
  }

  .rating>input {
      display: none;
  }

  .rating>label::before {
      margin: 5px;
      font-size: 2.25em;
      font-family: FontAwesome;
      display: inline-block;
      content: "\f005";
  }

  .rating>label {
      color: #ddd;
      float: right;
  }

  .rating>input:checked~label,
  .rating:not(:checked)>label:hover,
  .rating:not(:checked)>label:hover~label {
      color: #f7d106;
  }

  .rating>input:checked+label:hover,
  .rating>input:checked~label:hover,
  .rating>label:hover~input:checked~label,
  .rating>input:checked~label:hover~label {
      color: #fce873;
  }

</style>
<!-- Breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="bread-inner">
          <ul class="bread-list">
            <li><a href="<?=base_url();?>"><?= $this->lang->line('home');?><i class="ti-arrow-right"></i></a></li>
            <li class="active"><a href="#top"><?= $this->lang->line('detail_course');?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Breadcrumbs -->
  <!-- Start Blog Single -->
  <section class="blog-single section" style="padding: 0px;">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-12">
          <div class="blog-single-main">
            <div class="row">
              <?php foreach ($data['data'] as $key => $value) { ?>
              <div class="col-12">
                <div class="image">
                    <a data-toggle="modal" data-target="#exampleModal" title="Pratinjau Video" href="#">
                      <img class="detail-image" src="<?=base_url().$value['image'];?>" alt="#" style="width:100%;height:100%">
                        <div class="trailer-icon">
                        <i class="fa fa-play-circle"></i>
                        </div>
                    <!-- <div class="layer col-md-12 m-0 p-0"> -->
                      <!-- <span>
                        Lihat Pratinjau Kursus Ini
                      </span> -->
                    <!-- </div> -->
                    </a>
                </div>
                <div class="blog-detail">
                  <h2 class="blog-title"><?=$value['judul'];?></h2>
                  <!-- <div class="blog-meta"> -->
                    <span class="author">
                      <span><i class="fa fa-user"></i>&nbsp; <?= $this->lang->line('by').' : '.$value['nama_depan'].' '.$value['nama_belakang'];?></span>&nbsp;|&nbsp;
                        <span style="color:orange;"><?= $value['rating'];?></span>
                        <?php if ($value['rating_belakang'] == '' || $value['rating_belakang'] == 0) { ?>
                          <?php if ($value['rating_depan'] == '' || $value['rating_depan'] == 0) { ?>
                            <?php $not = 5; ?>
                            <?php for ($i=0; $i < $not; $i++) { ?>
                              <a class="fa fa-star-o"></a>
                            <?php } ?>
                          <?php }else{ ?>
                            <?php $not = 5 - $value['rating_depan'];
                            for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                              <a class="fa fa-star checked"></a>
                            <?php } ?>
                            <?php for ($i=0; $i < $not; $i++) { ?>
                              <a class="fa fa-star-o"></a>
                            <?php } ?>
                          <?php } ?>
                        <?php }else{ ?>
                          <?php $not = 4 - $value['rating_depan'];
                          for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                            <a class="fa fa-star checked"></a>
                          <?php } ?>
                          <a class="fa fa-star-half-o checked"></a>
                          <?php for ($i=0; $i < $not; $i++) { ?>
                            <a class="fa fa-star-o"></a>
                          <?php } ?>
                        <?php } ?>
                        (<?= $value['reviewer'];?>)
                    </span>
                    <div class="share-social">
                      <div class="row">
                        <div class="col-12" style="margin-top:-12px; margin-bottom:12px;">
                          <div class="content-tags">
                            <h4><?= $this->lang->line('share');?></h4>
                            <ul class="tag-inner" style="padding-left:55px;">
                              <li style="margin-right:0px"><a style="color:white;background-color:#3a5997;" href="https://www.facebook.com/share.php?u=<?=base_url().'users/users_course/detail/'.encode_url_share_id($value['id']);?>"><i class="fa fa-facebook"></i></a></li>
                              <li style="margin-right:0px"><a style="color:white;background-color:#0177b5;" href="http://www.linkedin.com/shareArticle?mini=true&url=<?=base_url().'users/users_course/detail/'.encode_url_share_id($value['id']);?>"><i class="fa fa-linkedin-square"></i></a></li>
                              <li style="margin-right:0px"><a style="color:white;background-color:#45c052;" href="whatsapp://send?text=<?=base_url().'users/users_course/detail/'.encode_url_share_id($value['id']);?>"><i class="fa fa-whatsapp"></i></a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  <!-- </div> -->
                  <!-- <center> -->
                    <div class="add-to-cart">
                      <a class="capsule ladda-button cart_<?=$value['id'];?> prim_add_to_cart" data-style="expand-left" url="<?=$detail_cart;?>" id="<?=$value['id'];?>" onclick="openModalCart(this)"><p style="color:white;"><?= $this->lang->line('add_to_cart') ?></p></a>

                      <?php if ($this->session->userdata('token') != '') { ?>
                        <?php if (in_array($value['id'], $wishlisted)) { ?>
                          <a id="<?=$value['id'];?>" url="<?=$wishlist;?>" onclick="add_remove_wishlist(this);" class="capsule min ladda-button ladda-wish<?=$value['id'];?>"><i id="wish_<?=$value['id'];?>" class="fa fa-heart love"></i></a>
                        <?php }else{ ?>
                          <a id="<?=$value['id'];?>" url="<?=$wishlist;?>" onclick="add_remove_wishlist(this);" class="capsule min ladda-button ladda-wish<?=$value['id'];?>"><i id="wish_<?=$value['id'];?>" class="fa fa-heart"></i></a>
                        <?php } ?>
                      <?php }else{ ?>
                        <a href="<?=$login_page;?>" class="capsule min"><i class="fa fa-heart"></i></a>
                      <?php } ?>
                    </div>
                  <!-- </center> -->
                  <div id="nav" class="tabsMain pt-3">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                      <li class="nav-item ">
                        <a id="overview" class="nav-link active"><?= $this->lang->line('overview') ?></a>
                      </li>
                       <li class="nav-item">
                        <a id="tutor" class="nav-link"><?= $this->lang->line('instructure'); ?></a>
                      </li>
                       <li class="nav-item">
                        <a id="pricing" class="nav-link"><?= $this->lang->line('price');?></a>
                      </li>
                    </ul>
                  </div>
                  <div class="container">
                    <div class="row">
                      <div id="content" class="pt-3 pb-3" style="width: 100%; min-width: 300px">
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            <?php } ?>
              <?php if (count($comment) == 0) {
                $height = '0px';
              }else{
                $height = '350px';
              } ?>
              <div class="col-12" style="height:<?=$height?>px; overflow:scroll;">
                <div class="comments">
                  <h3 class="comment-title"><?= $this->lang->line('comments') ?> (<?= count($comment);?>)</h3>
                  <?php foreach ($comment as $key => $value) { ?>
                    <!-- Single Comment -->
                    <div class="single-comment">
                      <!-- <img src="https://via.placeholder.com/80x80" alt="#"> -->
                      <?php if ($value['login_via'] == 'solmit') { ?>
                        <?php if ($value['avatar'] == null || $value['avatar'] == '') { ?>
                          <?php if ($value['jk'] == 'W') { ?>
                            <img src="<?= base_url();?>assets/default/female_avatar_s0lm1t.png" alt="">
                          <?php }else{ ?>
                            <img src="<?= base_url();?>assets/default/male_avatar_s0lm1t.png" alt="">
                          <?php } ?>
                        <?php }else{ ?>
                          <img src="<?= base_url();?>assets/avatar/<?=$value['avatar'];?>" alt="">
                        <?php } ?>
                      <?php }else{ ?>
                        <img src="<?=$value['avatar'];?>" alt="">
                      <?php } ?>
                      <div class="content">
                        <h4><?=$value['nama_depan'];?> <?=$value['nama_belakang'];?> <span><?= eng_date($value['tanggal_komentar']);?></span></h4>
                        <p><?= $value['comment'];?></p>
                        (<span style="color:orange;"><?= $value['score'];?></span>) &nbsp;
                        <?php $not = 5 - $value['score'];
                        for ($i=0; $i < $value['score']; $i++) { ?>
                          <span class="fa fa-star checked"></span>
                        <?php } ?>
                        <?php for ($i=0; $i < $not; $i++) { ?>
                          <span class="fa fa-star-o"></span>
                        <?php } ?>
                    </div>
                  </div>
                  <!-- End Single Comment -->
                  <?php } ?>
                </div>
              </div>


              <?php if ($this->session->userdata('token') != '') { ?>
                <?php if ($type_comment) { ?>
                  <div class="col-12">
                    <div class="reply">
                      <div class="reply-head">
                        <h2 class="reply-title"><?= $this->lang->line('leave_a_comment') ?></h2>
                        <!-- Comment Form -->
                        <?php echo form_open_multipart($save_rating, array('name' => 'form-rating', 'id' => 'form-rating')); ?>
                        <div class="row">
                          <div class="col-12">
                            <div class="rating">
                              <p style="color:black;"><?= $this->lang->line('give_your_rating') ?> <span style="color:red;">*</span></p>
                              <input type="radio" class="rate" id="star5" name="rating" value="5"/>
                              <label for="star5" title="5 Star"></label>

                              <input type="radio" class="rate" id="star4" name="rating" value="4"/>
                              <label for="star4" title="4 Star"></label>

                              <input type="radio" class="rate" id="star3" name="rating" value="3"/>
                              <label for="star3" title="3 Star"></label>

                              <input type="radio" class="rate" id="star2" name="rating" value="2"/>
                              <label for="star2" title="2 Star"></label>

                              <input type="radio" class="rate" id="star1" name="rating" value="1"/>
                              <label for="star1" title="1 Star"></label>
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="form-group">
                              <label>Your Message<span>*</span></label>
                              <textarea name="message" placeholder=""></textarea>
                              <input type="hidden" name="id_course" value="<?=$id_course;?>">
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="form-group button">
                              <button type="button" id="save_rating" class="btn"><?= $this->lang->line('post_comment') ?></button>
                            </div>
                          </div>
                        </div>
                        <?php echo form_close() ?>
                        <!-- End Comment Form -->
                      </div>
                    </div>
                  </div>
                <?php } ?>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="main-sidebar">
            <!-- Single Widget -->
            <div class="single-widget category">
              <h3 class="title"><?= $this->lang->line('other_modules') ?></h3>
              <ul class="categor-list">
                <?php foreach ($modul as $key => $value) { ?>
                  <li><a href="<?= base_url();?>users/users_course?modul=<?=$value['id'];?>"><?= $value['modul'];?></a></li>
                <?php } ?>
              </ul>
            </div>
            <!--/ End Single Widget -->
            <!-- Single Widget -->
            <div class="single-widget recent-post">
              <h3 class="title"><?= $this->lang->line('related_course') ?></h3>
              <?php foreach ($terkait as $key => $value) { ?>
                <!-- Single Post -->
                <div class="single-product">
                  <div class="product-img">
                    <a href="<?= $detail.$value['id'];?>">
                      <img class="default-img" src="<?=base_url().$value['image'];?>" style="width:100%;height:100%;" alt="#">
                      <span class="new"><?=$value['modul'];?></span>
                      <?php if ($value['have_free'] == TRUE) { ?>
                        <span class="out-of-stock"><?= $this->lang->line('free') ?></span>
                      <?php } ?>
                      <!-- <span class="out-of-stock">Hot</span> -->
                    </a>
                  </div>
                  <div class="product-content" style="height:100px">
                    <h3><a href="<?= $detail.encode_url_share_id($value['id']);?>"><?=$value['judul'];?></a></h3>
                    <p style="color:grey;font-size:12px;"><?= $value['nama_depan'].' '.$value['nama_belakang'].' ('.$value['institusi'].')'; ?></p>
                    <div>
                      <span style="color:orange;"><?= $value['rating'];?></span>
                      <?php if ($value['rating_belakang'] == '' || $value['rating_belakang'] == 0) { ?>
                        <?php if ($value['rating_depan'] == '' || $value['rating_depan'] == 0) { ?>
                          <?php $not = 5; ?>
                          <?php for ($i=0; $i < $not; $i++) { ?>
                            <span class="fa fa-star-o"></span>
                          <?php } ?>
                        <?php }else{ ?>
                          <?php $not = 5 - $value['rating_depan'];
                          for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                            <span class="fa fa-star checked"></span>
                          <?php } ?>
                          <?php for ($i=0; $i < $not; $i++) { ?>
                            <span class="fa fa-star-o"></span>
                          <?php } ?>
                        <?php } ?>
                      <?php }else{ ?>
                        <?php $not = 4 - $value['rating_depan'];
                        for ($i=0; $i < $value['rating_depan']; $i++) { ?>
                          <span class="fa fa-star checked"></span>
                        <?php } ?>
                        <span class="fa fa-star-half-o checked"></span>
                        <?php for ($i=0; $i < $not; $i++) { ?>
                          <span class="fa fa-star-o"></span>
                        <?php } ?>
                      <?php } ?>
                      (<?= $value['reviewer'];?>)
                    </div>
                    <?php if ($value['harga'] == 0) { ?>
                      <?php if ($value['pelaksanaan'] > date('Y-m-d')) { ?>
                        <p style="color:grey;font-size:12px;"><?= $this->lang->line('held_on') ?> <?=eng_date($value['pelaksanaan']);?></p>
                      <?php }else{ ?>
                        <p style="color:grey;font-size:12px;"><?= $this->lang->line('already_done_on') ?> <?=eng_date($value['pelaksanaan']);?></p>
                      <?php } ?>
                    <?php }else{ ?>
                      <br>
                    <?php } ?>
                  </div>

                  <div class="product-price">
                    <a class="capsule ladda-button cart_<?=$value['id'];?>" data-style="expand-left" url="<?=$detail_cart;?>" id="<?=$value['id'];?>" onclick="openModalCart(this)"><p style="color:white;"><?= $this->lang->line('add_to_cart') ?></p></a>
                    <?php if ($this->session->userdata('token') != '') { ?>
                      <?php if (in_array($value['id'], $wishlisted)) { ?>
                        <a id="<?=$value['id'];?>" url="<?=$wishlist;?>" onclick="add_remove_wishlist(this);" data-style="expand-left" class="capsule ladda-button ladda-wish<?=$value['id'];?>"><i id="wish_<?=$value['id'];?>" class="fa fa-heart love"></i></a>
                      <?php }else{ ?>
                        <a id="<?=$value['id'];?>" url="<?=$wishlist;?>" onclick="add_remove_wishlist(this);" data-style="expand-left" class="capsule ladda-button ladda-wish<?=$value['id'];?>"><i id="wish_<?=$value['id'];?>" class="fa fa-heart"></i></a>
                      <?php } ?>
                    <?php }else{ ?>
                      <a href="<?=$login_page;?>" class="capsule min"><i class="fa fa-heart"></i></a>
                    <?php } ?>
                  </div>
                </div>


                <!-- End Single Post -->
                <hr>
              <?php } ?>
            </div>
            <!--/ End Single Widget -->
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row p-3">
                  <div class="col-md-9 col-10">
                  <h1><?= $this->lang->line('course_review') ?></h1>
                  <p><?=$data['data'][0]['judul'];?></p>
                  </div>
                  <div class="col-md-3 col-2">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ti-close" aria-hidden="true"></span></button>
                  </div>
                </div>
                <div class="modal-body">
                  <div class="trailer col-lg-12 col-md-12 col-12">
                    <?php if (!empty($data['data'][0]['url_youtube'])): ?>
                    <div id="trailer">
                    </div>
                    <?php else: ?>
                      <h6><?= $this->lang->line('preview_avaiable') ?> ..</h6>
                    <?php endif ?>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modal_interested">
        <div class="modal-dialog2" role="document">
            <div class="modal-content">
            <div class="row p-3">
              <div class="col-md-9 col-10">
              </div>
              <div class="col-md-3 col-2">
              </div>
            </div>
              <div class="modal-body text-center">
                <h1 style="font-size:18px"><?= $this->lang->line('interested');?></h1>
                <br>
                <br>
                <button style="font-size:10px;background-color:Navy" class="btn btn-blue" id="btn_yes_interested"><?= $this->lang->line('interested2');?></button>
                <button style="font-size:10px;background-color:Crimson" class="btn btn-red" id="btn_maybe_later"><?= $this->lang->line('interested3');?></button>
              </div>
            </div>
        </div>
    </div>
  <!-- Modal end -->
  <script src="http://www.youtube.com/player_api"></script>
  <script type="text/javascript">

    $(document).on("click", "#btn_maybe_later", function(){
      $("#modal_interested").modal("hide")
    })

    $(document).on("click", "#btn_yes_interested", function(){
      $("#modal_interested").modal("hide")
      $(".prim_add_to_cart").trigger("click");
    })

    // create youtube player
    var player;
    function onYouTubePlayerAPIReady() {
        player = new YT.Player('trailer', {
          width: '950',
          height: '460',
          videoId: '<?php echo getYoutubeEmbedId($data['data'][0]['url_youtube']) ?>',
          events: {
            onReady: onPlayerReady,
            onStateChange: onPlayerStateChange
          }
        });
    }

    // autoplay video
    function onPlayerReady(event) {
        event.target.playVideo();
    }

    // when video ends
    function onPlayerStateChange(event) {        
        if(event.data === 0) {          
            $("#exampleModal").modal("hide")
            $("#modal_interested").modal("show")
        }
    }

    $("#save_rating").click(function(){
      var form = $("#" + $(this).closest('form').attr('name'));
      var formdata = false;
      if (window.FormData) {
        formdata = new FormData(form[0]);
      }
        $.ajax({
          type: form.attr("method"),
          url: form.attr("action"),
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          success: function (res) {
            var hasil = $.parseJSON(res);
            if (hasil["proses"] == 'success') {
               toastr.success(hasil["pesan"]);
               setTimeout(function () {
                 location.reload(true);
               }, 2000);
             }else{
               toastr.error(hasil["pesan"]);
             }
           },
           error: function (res) {
             toastr.error("Komentar tidak dapat disimpan.");
           },
        });
    });
    $("#nav li a").click(function() {
        $("#nav li a").removeClass('active');
        $(this).addClass('active');
        $.ajax({
          type: 'POST',
          url: '<?= base_url('users/users_course/tab')?>',
          data: { id : this.id, id_course : '<?= $id_course ?>'},
          dataType: "json",
          success: function(html) {
            $("div#content").html(html);
          },
        });
    });
    $.ajax({
      type: 'POST',
      url: '<?= base_url('users/users_course/tab')?>',
      data: { id : 'overview', id_course : '<?= $id_course ?>'},
      dataType: "json",
      success: function(html) {
        $("div#content").html(html);
      },
    });
  </script>
