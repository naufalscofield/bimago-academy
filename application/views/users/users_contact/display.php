<style media="screen">
.map-container-6{
overflow:hidden;
padding-bottom:56.25%;
position:relative;
height:0;
}
.map-container-6 iframe{
left:0;
top:0;
height:100%;
width:100%;
position:absolute;
}
</style>

<!-- Breadcrumbs -->
<div class="breadcrumbs">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="bread-inner">
          <ul class="bread-list">
            <li><a href="<?=base_url();?>"><?= $this->lang->line('home');?><i class="ti-arrow-right"></i></a></li>
            <li class="active"><a href="#"><?= $this->lang->line('find_us');?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Breadcrumbs -->

<!--Section: Contact v.1-->
<section class="section pb-5">
<div class="container">

  <div class="row">

    <!-- <div class="col-lg-5 mb-4">
      <div class="card">
        <div class="card-body">
          <div class="form-header blue accent-1">
            <h3><i class="fa fa-envelope"></i> Write to us:</h3>
          </div>

          <p>We'll write rarely, but with only the best content.</p>
          <br>

          <?php echo form_open_multipart($simpan, array('name' => 'form-message', 'id' => 'form-message', 'class' => 'form')); ?>
            <?php if ($this->session->userdata('token') != '') { ?>
              <input name="nama" type="hidden" value="<?=$data->nama_depan.' '.$data->nama_belakang;?>">
              <input name="email" type="hidden" value="<?=$data->email;?>">
              <input name="no_hp" type="hidden" value="<?=$data->no_hp;?>">
            <?php }else{ ?>
              <div class="md-form">
                <i class="fa fa-user prefix grey-text"></i> <label for="form-name">Your name</label>
                <input type="text" id="form-name" name="nama" class="form-control">
              </div>

              <div class="md-form">
                <i class="fa fa-envelope prefix grey-text"></i> <label for="form-email">Your email</label>
                <input type="text" id="form-email" name="email" class="form-control">
              </div>

              <div class="md-form">
                <i class="fa fa-phone prefix grey-text"></i> <label for="form-Subject">Phone Number</label>
                <input type="text" id="form-Subject" name="no_hp" class="form-control">
              </div>
            <?php } ?>

            <div class="md-form">
              <i class="fa fa-pencil prefix grey-text"></i> <label for="form-text">Message</label>
              <textarea id="form-text" class="form-control md-textarea" name="pesan" rows="3"></textarea>
            </div>

            <div class="text-center mt-4">
              <button type="button" id="simpan" class="btn btn-light-blue">Submit</button>
            </div>
          <?php echo form_close() ?>
        </div>
      </div>
    </div> -->

    <!--Grid column-->
    <div class="col-lg-12">

      <!--Google map-->
      <div id="map-container-google-11" class="z-depth-1-half map-container-6" style="height: 400px">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.918463013185!2d107.65260132035915!3d-6.900354706253837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e791685ec42d%3A0xf6156f6a3663477c!2sPT.%20Solmit%20Bangun%20Indonesia!5e0!3m2!1sid!2sid!4v1620614316359!5m2!1sid!2sid"
          frameborder="0" style="border:1" allowfullscreen></iframe>
      </div>

      <br>
      <!--Buttons-->
      <div class="row text-center">
        <div class="col-md-4">
          <a class="btn-floating blue accent-1"><i class="fa fa-map-marker"></i></a>
          <p><?= strip_tags($info->alamat);?></p>
        </div>

        <div class="col-md-4">
          <a class="btn-floating blue accent-1"><i class="fa fa-phone"></i></a>
          <p><?= strip_tags($info->telepon);?></p>
          <p><?= $info->hari_kerja.' , '.strip_tags($info->jam_kerja);?></p>
        </div>

        <div class="col-md-4">
          <a class="btn-floating blue accent-1"><i class="fa fa-envelope"></i></a>
          <p>info@solmit.com</p>
          <p><?= strip_tags($info->email);?></p>
        </div>
      </div>

    </div>
    <!--Grid column-->

  </div>
</div>

</section>
<!--Section: Contact v.1-->
