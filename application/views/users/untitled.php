<div class="row">
		<div class="col-sm-6 col-md-3 col-lg-2">
			<!-- Logo -->
			<div class="logo">
	<a href="<?=base_url();?>"><img src="<?=base_url();?>solmit-academy.png" alt="logo"></a>
			</div>
			<!--/ End Logo -->
			<!-- Search Form -->
			<div class="search-top d-sm-none">
				<!-- <div class="top-search"><a href="#0"><i class="ti-search"></i></a></div> -->
				<!-- Search Form -->
				<div class="search-top">
					<form class="search-form" action="<?=base_url();?>users/users_course" method="POST">
						<!-- <input type="text" placeholder="Search here..." name="judul">
						<button value="search" type="submit"><i class="ti-search"></i></button> -->
					</form>
				</div>
				<!--/ End Search Form -->
			</div>
			<!--/ End Search Form -->
			<div class="mobile-nav"></div>
		</div>
		<div class="col-6 col-md-3 d-none d-lg-block d-xl-block">
			<div class="search-bar-top">
				<div class="search-bar">
					<form action="<?=$search;?>" method="GET" autocomplete="off">
						<!-- <input name="judul" placeholder="<?= $this->lang->line('search_course_here') ?>" id="searchBox" type="search">
						<button class="btnn"id="btnSearch" type="button"><i class="ti-search"></i></button> -->
						<div class="row">
					   <div class="col-sm-10 pr-0">
					       <input name="judul" placeholder="Search Course Here....." id="searchBox" type="search">
					    </div>
					    <div class="col-sm-2 pr-0">
					        <button class="btnn" id="btnSearch" type="button"><i class="ti-search"></i></button>
					    </div>
			       </div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-6 col-md-6">
			<div class="d-sm-block d-md-none d-xl-none pull-right">
				<div class="btn-group">
				  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> </button>
				  <div class="dropdown-menu dropdown-menu-right">
					    <a href="<?=base_url();?>users/users_course/paid_webinar" class="dropdown-item">List <?= $this->lang->line('course_certification') ?></a>
					   <?php if ($this->session->userdata('token') != '') { ?>
							<a href="<?=base_url();?>users/exam" class="dropdown-item"><?= $this->lang->line('take_an_exam') ?></a>
							<a href="<?=base_url();?>users/users_mycourse/index/wishlist" class="dropdown-item">Wishlist</a>
							<a href="<?=base_url();?>users/users_mycourse/index/history" class="dropdown-item">History</a>
						<?php } ?>
							<a href="<?=base_url();?>users/users_mycourse/users/users_checkout" class="dropdown-item">Cart</a>
				  </div>
				</div>
			</div>
			<div class="right-bar d-sm-none d-md-block d-xl-block">
				<!-- Search Form -->
					<!-- <a href="<?=base_url();?>users/users_course/free_webinar" class="p-1"><?= $this->lang->line('free_webminar') ?></a> -->
					<a href="<?=base_url();?>users/users_course" class="p-2">List <?= $this->lang->line('course_certification') ?></a>
					<?php if ($this->session->userdata('token') != '') { ?>
						<a href="<?=base_url();?>users/exam" class="p-2"><?= $this->lang->line('take_an_exam') ?></a>
						<div class="sinlge-bar">
							<a href="<?=base_url();?>users/users_mycourse/index/wishlist" class="single-icon"><i class="fa fa-heart" aria-hidden="true" alt="Wishlist"></i></a>
						</div>
						<div class="sinlge-bar">
							<a href="<?=base_url();?>users/users_mycourse/index/history" class="single-icon"><i class="fa fa-file" aria-hidden="true"></i></a>
						</div>
					<?php } ?>
					<div class="sinlge-bar shopping" url="<?=base_url();?>users/users_cart/get_modal" onclick="shopping(this);">
						<a href="#" class="single-icon"><i class="fa fa-shopping-cart"></i> <span class="total-count" id="total-count">0</span></a>
					<div id="list-cart"></div>
				</div>
			</div>
		</div>
	</div>