<?php echo form_open_multipart($simpan, array('name' => 'modal-course', 'id' => 'modal-course')); ?>
  <div class="modal-header">
    <h5 class="modal-title" id="ModalLabelLarge"><b>Upload Certificate</b></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="form-group row">
    <div class="container">
      <div class="col-sm-12">
        <input type="hidden"  name="id" value="<?= isset($data['id']) ? $data['id'] : '' ; ?>">
        <input type="hidden"  name="id_user" value="<?= $id_user; ?>">
        <input type="hidden"  name="id_certificate" value="<?= $id_certificate; ?>">

        <label>Upload</label>
        <?php echo form_upload('url', '', 'class="form-control form-control-user"');?>
        <?php echo form_input('url_old', !empty($data['url']) ? $data['url'] : '', 'class="form-control form-control-user" hidden');?>
      </div>
      <?php if (!empty($data['url'])):
        $filename = explode('/',$data['url']);
      ?>
      <div class="col-sm-12 pull-right">
        <br>
        <table class="table table-striped"> 
          <tr>
            <th>Filename</th>
            <th>Download</th>
          </tr>
          <tr>
            <td><?= $filename[3]; ?></td>
            <td>
              <a class=" btn btn-primary pull-right" target="_blank" href="<?= base_url($data['url']) ?>"><i class="fa fa-download"></i></a>
              <a class="btn btn-danger" onclick="onDelete('<?= $data['id'];?>')"> <i class="fa fa-trash"></i></a>
            </td>
          </tr>
        </table>
      </div>
    <?php endif ?>
    </div>
  </div>
  </div>
  <div class="modal-footer">
  <button type="button" id="simpan" class="btn btn-dark">Simpan</button>
<?php echo form_close() ?>

<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
  $("#simpan").click(function(){
    for (instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].updateElement();
    }
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
    bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function(result){
      if(result == true) {
        $.ajax({
          type: form.attr("method"),
          url: form.attr("action"),
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          success: function (res) {
            var hasil = $.parseJSON(res);
            if (hasil["status"] == 200) {
               toastr.success(hasil["pesan"]);
               setTimeout(function () {
                 return hasil["status"];
               }, 1500);
               $('#ModalLarge').modal('hide');
               setTimeout(function () {
                 location.reload(true);
               }, 1500);
             }else{
               toastr.error(hasil["pesan"]);
               result = false;
               return hasil["status"];
             }
           },
           error: function (res) {
             toastr.error("Data tidak dapat disimpan.");
             result = false;
             return false;
           },
        });
      }
    });
  });
</script>
