<div class="modal-header">
  <h5 class="modal-title" id="ModalLabelLarge"><b>Detail Progres <?=$nama;?></b></h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="card-body">
  <div class="table-responsive">
    <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
      <thead class="thead-dark">
        <tr>
          <th width="5%">No.</th>
          <th width="55%">Title Module </th>
          <th width="40%">Persentase</th>
        </tr>
      </thead>
      <tbody class="row_position">
        <?php if (count($data) > 0 || !empty($data)) { ?>
          <?php $no=0; foreach ($data as $key => $value) { $no++; ?>
              <tr>
                <td><?= $no;?>.</td>
                <td><?= $value['judul'];?></td>
                <td>
                  <?= $value['persentase'] == 0 || empty($value['persentase']) ? '0' : $value['persentase'];?>%<br>
                  <div class="progress">
                    <div class="progress-bar" id="progress-bar-video" role="progressbar" style="width: <?=$value['persentase'];?>%;" aria-valuenow="<?=$value['persentase'];?>" aria-valuemin="0" aria-valuemax="100"><span id="progres-all-view"></div>
                  </div>
                </td>
              </tr>
          <?php } ?>
        <?php }else{ ?>
          <td colspan="3"><center>Tidak Ada Data Video !</center></td>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
