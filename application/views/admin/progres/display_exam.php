<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?=$judul;?></h6>
    </div>
    <div class="card-body">
      <input type="hidden" name="id_course" value="<?=$id_course;?>">
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No.</th>
              <th width="25%">Nama Pelajar </th>
              <th width="15%">Nilai </th>
              <th width="15%">Keterangan </th>
            </tr>
          </thead>
          <tbody class="row_position">
              <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
                <?php if ($data['status'] == 200) { ?>
                  <tr>
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_depan'].' '.$value['nama_belakang'];?></td>
                    <?php foreach ($value['data'] as $key2 => $value2) { ?>
                      <td><?= $value2['score_final'];?></td>
                      <td style="<?= $value2['status'] == 'passed' ? 'background-color:#cbf5cf;' : 'background-color:#f5a2a7';?>"><?= $value2['status'];?></td>
                    <?php } ?>
                  </tr>
                <?php }else{ ?>
                  <td>Tidak Ada Data !</td>
                <?php } ?>
              <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
function onEdit(id,id_user){
  $.ajax({
      url: '<?= base_url('admin/certificate_monitoring/uploadcertificate'); ?>',
      method: "POST",
      data: {id:id,id_user:id_user},
      success: function(data){
          $('#dataModalLarge').html(data);
          $('#ModalLarge').modal('show');
      }
  });
}
function onDelete(id,id_user){
  $.ajax({
      url: '<?= base_url('admin/certificate_monitoring/delete'); ?>',
      method: "POST",
      data: {id:id,id_user:id_user},
      success: function(res){
        var hasil = $.parseJSON(res);
        if (hasil["status"] == 200) {
           toastr.success(hasil["pesan"]);
           setTimeout(function () {
             return hasil["status"];
           }, 1500);
           $('#ModalLarge').modal('hide');
           setTimeout(function () {
             location.reload(true);
           }, 1500);
         }else{
           toastr.error(hasil["pesan"]);
           result = false;
           return hasil["status"];
         }
      }
  });
}
function onGenerate(id,id_user){
    $.ajax({
        url: '<?= base_url('admin/certificate_monitoring/certificate'); ?>',
        method: "POST",
        data: {id:id,id_user:id_user},
        success: function (res) {
          let r = JSON.parse(res)
          window.open(r.url, '_blank');
          // location.reload();
        },
        error: function (res) {

        },
    });
}
// $("#simpan").click(function(){
//   var form = $("#" + $(this).closest('form').attr('name'));
//   var formdata = false;
//   if (window.FormData) {
//     formdata = new FormData(form[0]);
//   }
//   bootbox.confirm("Apakah anda yakin akan mengirim email?", function(result){
//     if(result == true) {
//       $.ajax({
//         type: form.attr("method"),
//         url: form.attr("action"),
//         data: formdata ? formdata : form.serialize(),
//         processData: false,
//         contentType: false,
//         cache: false,
//         async: false,
//         success: function (res) {
//           var hasil = $.parseJSON(res);
//           if (hasil["status"] == 200) {
//              toastr.success(hasil["pesan"]);
//              setTimeout(function () {
//                location.reload(true);
//              }, 1500);
//            }else{
//              toastr.error(hasil["pesan"]);
//            }
//          },
//          error: function (res) {
//            toastr.error("Data tidak dapat disimpan.");
//          },
//       });
//     }
//   });
// });
</script>
