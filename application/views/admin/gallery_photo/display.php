<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        <?= $this->lang->line('gallery'); ?>
      </h6>
    </div>
    <div class="card-body">
      <div align="right">
        <button class="btn btn-success" data-target="#modal_add" data-toggle="modal" id="btn_add">+ <?= $this->lang->line('add'); ?></button>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No</th>
              <th width="80%"><i class="fas fa-image"></i></th>
              <th width="5%"><i class="fa fa-cogs"></i></th>
            </tr>
          </thead>
          <tbody class="row_position">
              <?php
                foreach ($gallery as $idx => $g) {
              ?>
                <tr>
                  <td>
                    <center>
                      <?= $idx + 1;?>
                    </center>
                  </td>
                  <td>
                    <center>
                      <img style="width:35%;height:35%" src="<?= base_url();?>/uploads/gallery/<?= $g['file'];?>" alt="">
                    </center>
                  </td>
                  <td>
                    <button class="btn btn-danger btn-delete" data-id="<?= $g['id'];?>" title="<?= $this->lang->line('delete');?>"><i class="fas fa-trash"></i></button>
                  </td>
                </tr>
              <?php
                }
              ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= $this->lang->line('add_photo'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>admin/gallery-photo-store" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <label for=""><?= $this->lang->line('photo'); ?></label><br>
              <span style="color:red">*<?= $this->lang->line('suggest_gallery');?></span>
              <input type="file" required class="form-control" name="file">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
            <button type="submit" class="btn btn-primary"><?= $this->lang->line('save'); ?></button>
          </div>
        </div>
      </form>
  </div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
    var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	  = '<?php echo $this->session->flashdata("error");?>'
    
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}

    CKEDITOR.replace('ckeditor');
  })

  $(document).on('click', '.btn-delete', function(){
    var id_photo  =  $(this).data("id")
    var route     = "<?php echo base_url();?>admin/gallery-photo-delete/"+id_photo
    bootbox.confirm("Apakah anda yakin akan mengahapus foto ini?", function(result){
      if(result == true) {
        $.get(route, function(data, status){
          var data = JSON.parse(data)
          if (data.code == 200)
          {
            toastr.success("Photo deleted")
          } else
          {
            toastr.success("Photo failed deleted")
          }
          setTimeout(function(){ 
            window.location.href = "<?php echo base_url();?>admin/gallery-photo"
          }, 1000);
        })
      }
    })
  })
</script>
