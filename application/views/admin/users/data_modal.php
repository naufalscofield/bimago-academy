<?php echo form_open_multipart($simpan, array('name' => 'modal-biodata', 'id' => 'modal-biodata')); ?>
  <div class="modal-header">
    <h5 class="modal-title" id="ModalLabelLarge"><b>Master Users</b></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <input type="hidden" name="id" value="<?= !empty($data['id']) ? $data['id'] : '';?>">
    <input type="hidden" name="role" value="<?= !empty($data['role']) ? $data['role'] : '';?>">
    <input type="hidden" name="login_via" value="<?= !empty($data['login_via']) ? $data['login_via'] : '';?>">
    <input type="hidden" name="avatar_old" value="<?= !empty($data['avatar']) ? $data['avatar'] : '';?>">
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Avatar</label>
          <center>
            <?php if ($this->session->userdata('login_via') == 'solmit') { ?>
              <?php if ($data['avatar'] == null || $data['avatar'] == '') { ?>
                <?php if ($data['jk'] == 'W') { ?>
                  <img src="<?= base_url();?>assets/default/female_avatar_s0lm1t.png" alt="" style="width:250px;height:250px;">
                <?php }else{ ?>
                  <img src="<?= base_url();?>assets/default/male_avatar_s0lm1t.png" alt="" style="width:250px;height:250px;">
                <?php } ?>
              <?php }else{ ?>
                <img src="<?= base_url();?>assets/avatar/<?=$data['avatar'];?>" alt="" style="width:250px;height:250px;">
              <?php } ?>
            <?php }else{ ?>
              <img src="<?=$data['avatar'];?>" alt="" style="width:200px;height:200px;">
            <?php } ?>
        </center>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Role</label>
        <?php echo form_input('', !empty($data['nama_role']) ? $data['nama_role'] : '', 'class="form-control form-control-user" disabled');?>
      </div>
      <div class="col-sm-6">
        <label>Login Via</label>
        <?php echo form_input('', !empty($data['login_via']) ? $data['login_via'] : '', 'class="form-control form-control-user" disabled');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Nama Depan</label>
        <?php echo form_input('nama_depan', !empty($data['nama_depan']) ? $data['nama_depan'] : '', 'class="form-control form-control-user"');?>
      </div>
      <div class="col-sm-6">
        <label>Nama Belakang</label>
        <?php echo form_input('nama_belakang', !empty($data['nama_belakang']) ? $data['nama_belakang'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Email</label>
        <input class="form-control form-control-user" type="text" name="email" value="<?=!empty($data['email']) ? $data['email'] : '';?>" <?=$data['login_via'] == 'gmail' ? 'disabled' : '';?>>
      </div>
      <div class="col-sm-6">
        <label>No. Handphone</label>
        <?php echo form_input('no_hp', !empty($data['no_hp']) ? $data['no_hp'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Institusi</label>
        <?php echo form_input('institusi', !empty($data['institusi']) ? $data['institusi'] : '', 'class="form-control form-control-user"');?>
      </div>
      <div class="col-sm-6">
        <label>Jenis Kelamin</label>
        <select class="form-control form-control-user" name="jk">
          <?php
          $jk = ['Pria' => 'P', 'Wanita' => 'W'];
          foreach ($jk as $key => $value) { ?>
            <option value="<?=$value;?>" <?= $data['jk'] == $value ? 'selected' : '' ;?>><?=$key;?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <?php if ($data['role'] == '3') { ?>
      <div class="form-group row">
        <?php if ($data['login_via'] == 'solmit') { ?>
          <div class="col-sm-6">
          <label>Pilih Photo</label>
          <select class="form-control form-control-user" name="avatar">
          <?php
          $choice = ['Photo Tersedia' => 0, 'Photo Default' => 1];
          foreach ($choice as $key => $value) { ?>
            <option value="<?=$value;?>"><?=$key;?></option>
            <?php } ?>
            </select>
            </div>
        <?php } ?>
        <div class="col-sm-6">
          <label>Nama Sertifikat</label>
          <?php echo form_input('nama_sertifikat', !empty($data['nama_sertifikat']) ? $data['nama_sertifikat'] : '', 'class="form-control form-control-user"');?>
        </div>
      </div>
    <?php } ?>

    <?php if ($data['role'] == 2) { ?>
      <div class="form-group row">
        <div class="col-sm-12">
          <label>Profil</label>
          <?php echo form_textarea('profil', !empty($data['profil']) ? $data['profil'] : '', 'class="form-control form-control-user" id="ckeditor"');?>
        </div>
      </div>
    <?php } ?>
  </div>
  <div class="modal-footer">
  <button type="button" id="simpan" class="btn btn-dark" <?=$disable;?>>Simpan</button>
<?php echo form_close() ?>

<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script type="text/javascript">
  CKEDITOR.replace('ckeditor');

  $("#simpan").click(function(){
    for (instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].updateElement();
    }
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
    bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function(result){
      if(result == true) {
        $.ajax({
          type: form.attr("method"),
          url: form.attr("action"),
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          success: function (res) {
            var hasil = $.parseJSON(res);
            if (hasil["status"] == 200) {
               toastr.success(hasil["pesan"]);
               setTimeout(function () {
                 return hasil["status"];
               }, 1500);
               $('#ModalLarge').modal('hide');
               setTimeout(function () {
                 location.reload(true);
               }, 1500);
             }else{
               toastr.error(hasil["pesan"]);
               result = false;
               return hasil["status"];
             }
           },
           error: function (res) {
             toastr.error("Data tidak dapat disimpan.");
             result = false;
             return false;
           },
        });
      }
    });
  });
</script>
