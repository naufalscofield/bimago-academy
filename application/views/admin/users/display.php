<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?=$judul;?></h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered dt-responsive" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th>Nama Lengkap</th>
              <th>Email</th>
              <th>Role</th>
              <th>Institusi</th>
              <th>No Handphone</th>
              <th>Email</th>
              <th>Status</th>
              <th>Login Via</th>
              <th width="15%">Aksi</th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
              <?php if ($data['status'] == 200) { ?>
                  <tr id="<?php echo $value['id'] ?>">
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_depan'].' '.$value['nama_belakang'];?></td>
                    <td><?= $value['email'];?></td>
                    <td><?= $value['nama_role'];?></td>
                    <td><?= $value['institusi'];?></td>
                    <td><?= $value['no_hp'];?></td>
                      <?php if ($value['verifikasi'] == 1): ?>
                        <td style="background-color:#cbf5cf;">
                          <span>Ya</span>
                        </td>
                      <?php else: ?>
                        <td style="background-color:#f5a2a7;">
                          <span>Tidak</span>
                        </td>
                      <?php endif; ?>
                      <?php if ($value['status'] == 2): ?>
                        <td style="background-color:#cbf5cf;">
                          <span>Ya</span>
                        </td>
                      <?php else: ?>
                        <td style="background-color:#f5a2a7;">
                          <span>Tidak</span>
                        </td>
                      <?php endif; ?>
                    <td><?= $value['login_via'];?></td>
                    <td>
                      <?php if ($value['login_via'] == 'solmit'): ?>
                        <a id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-info btn-sm resetPassword ladda-reset_<?=$value['id'];?>">
                          <span style="color:white">Reset Password</span>
                        </a>
                      <?php endif; ?>
                      <?php if ($value['role'] == 2) { ?>
                        <a id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-warning btn-sm updateStatus ladda-status_<?=$value['id'];?>">
                        <?= $this->lang->line('verify') ?>
                          <!-- <i style="color:white" class="fas fa-exclamation-triangle"></i> -->
                        </a>
                      <?php } ?>
                      <a id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-info btn-sm getModal ladda-update_<?=$value['id'];?>">
                        <i style="color:white" class="fas fa-info-circle"></i>
                      </a>
                      <a id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-danger btn-sm deleteData ladda-delete_<?=$value['id'];?>">
                        <i style="color:white" class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
              <?php }else{ ?>
                <td>Tidak Ada Data !</td>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
    $(".resetPassword").click(function(event){
      var id = $(this).attr('id');
      var l = Ladda.create( document.querySelector( '.ladda-reset_' + id));
      l.start();
      bootbox.confirm("Apakah anda yakin akan me reset password dengan username ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_reset_password;?>",
              method: "POST",
              data: {id:id},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                   l.stop();
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   l.stop();
                   toastr.error(hasil["pesan"]);
                 }
              },
              error: function (res) {
                l.stop();
                toastr.error("Password tidak dapat diubah.");
              },
          });
        }else{
          l.stop();
        }
      });
    });

    $(".getModal").click(function(event){
      var id = $(this).attr('id');
      var l = Ladda.create( document.querySelector( '.ladda-update_' + id));
      l.start();
        $.ajax({
            url: "<?=$get_data_edit;?>",
            method: "POST",
            data: {id:id},
            success: function(data){
              l.stop();
              $('#dataModalLarge').html(data);
              $('#ModalLarge').modal('show');
            },
            error: function (data) {
              l.stop();
              toastr.error("Data tidak dapat dibuka, coba kembali.");
            },
        });
      });

    $(".deleteData").click(function(event){
      var id = $(this).attr('id');
      var l = Ladda.create( document.querySelector( '.ladda-delete_' + id));
      l.start();
      bootbox.confirm("Apakah anda yakin akan menghapus data ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_data_delete;?>",
              method: "POST",
              data: {id:id},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                  l.stop();
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   l.stop();
                   toastr.error(hasil["pesan"]);
                 }
              },
              error: function (res) {
                l.stop();
                toastr.error("Data tidak dapat dihapus.");
              },
          });
        }else{
          l.stop();
        }
      });
    });

    $(".updateStatus").click(function(event){
  		var id = $(this).attr('id');
  		var l = Ladda.create( document.querySelector( '.ladda-status_' + id));
  		l.start();
  		bootbox.confirm("Ubah status verifikasi lecturer?", function(result){
  			if(result == true) {
  				$.ajax({
  						url: "<?=$update_status;?>",
  						method: "POST",
  						data: {id:id},
  						success: function(res){
  							var hasil = $.parseJSON(res);
  							if (hasil["status"] == 200) {
  								l.stop();
  								 toastr.success(hasil["pesan"]);
  								 setTimeout(function () {
  									 location.reload(true);
  								 }, 1500);
  							 }else{
  								 l.stop();
  								 toastr.error(hasil["pesan"]);
  							 }
  						},
  						error: function (res) {
  							l.stop();
  							toastr.error("Data tidak dapat diubah.");
  						},
  				});
  			}else{
  				l.stop();
  			}
  		});
  	});

  });
</script>
