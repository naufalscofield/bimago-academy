<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
		<?php
		if ($data_exam->tipe == 'exam'){
		?>
		<a class="m-0 font-weight-bold text-primary" href="<?= base_url();?>admin/exam">Exam</a>&nbsp<i class="fas fa-angle-double-right"></i>&nbsp<a href="#" class="m-0 font-weight-bold text-primary"><?=$title;?></a>&nbsp<i class="fas fa-angle-double-right"></i>&nbsp<a class="m-0 font-weight-bold" style="color:#191970">Exam Course : <?= $data_exam->judul;?></a>
		<?php
		} else
		{
			?>
			<a class="m-0 font-weight-bold text-primary" href="<?= base_url();?>admin/quiz/<?=$data_exam->id_episode;?>">Quiz</a>&nbsp<i class="fas fa-angle-double-right"></i>&nbsp<a href="#" class="m-0 font-weight-bold text-primary"><?=$title;?></a>&nbsp<i class="fas fa-angle-double-right"></i>&nbsp<a class="m-0 font-weight-bold" style="color:#191970">Quiz Course : <?= $data_exam->judul;?> - Episode : <?= $episode->judul;?></a>
		<?php
		}
		?>
		</div>
		<div class="card-body">
			<div class="row">
					<div class="col-md-6">
						<div class="col-md-4">
						<b><p id="totalSoal">Total Soal : <?= $total_question->num_rows();?></p></b>
						<form action="<?= base_url();?>admin/store-question-used" method="POST">
						<input type="hidden" name="id_exam" value="<?= $id_exam;?>">
						<b><label id="">Soal Yang Dipakai : <input style="width:45%" required class="form-control form-control-sm" max="<?= $total_question->num_rows();?>" name="jumlah_soal_dipakai" value="<?= $data_exam->jumlah_soal_dipakai;?>" type="number"></label></b>
						<input type="submit" class="btn btn-info btn-sm" value="Save">
						</form>
						</div>
					</div>
				<div class="col-md-6">
					<div class="text-right">
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text">Add Single Question</span>
						</button>

						<form action="<?= base_url();?>admin/import-questions" method="POST" enctype="multipart/form-data">
							<input type="hidden" value="<?=$id_exam;?>" name="id_exam">
							<input required type="file" name="file_excel" class="form-control-sm">
							<button type="submit" class="btn btn-success">
								<span class="icon text-white-50">
									<i class="fas fa-file-excel"></i>
								</span>
								<span class="text">Import Questions</span>
							</button>
						</form>
						<br>

						<a class="btn btn-warning" href="<?= base_url();?>format_import_question_answers.xlsx" download>
							<span class="icon text-white-50">
								<i class="fas fa-download"></i>
							</span>
							<span class="text">Download Format Excel</span>
						</a>

					</div>
				</div>
			</div>
			<br>
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="dataTableQuestions" width="100%" cellspacing="0">
					<thead class="thead-dark">
						<tr>
							<th width="5%">No. </th>
							<th width="80%">Question</th>
							<th width="3%">Score</th>
							<th width="80%">Answers</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bd-example-modal-lg" id="modalAnswers" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Answers</h5>
			<h6 id="namaSoal"></h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="d-flex justify-content-center">
               <div class="spinner-border text-primary text-center" id="spinnerAnswers" role="status">
                  <span class="sr-only">Loading...</span>
               </div>
            </div>
		</div>
		<form action="<?= base_url();?>admin/store-answers" method="POST" id="formAnswers" style="display:none">
			<div class="ml-4 mr-4">
				<p style="color:red">*Select correct answer</p>
			</div>
			<input type="hidden" name="ans_id_question" id="ansIdQuestion">
			<input type="hidden" name="ans_id_exam" value="<?= $id_exam;?>" id="ansIdExam">
			<input type="hidden" name="ans_tipe_question" value="" id="ansTipeQuestion">
			<div id="listAnswers" class="ml-4 mr-4">

			</div>
			<div class="modal-footer" id="footerAnswers" style="display:none">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</form>
      </div>
   </div>
</div>
<div class="modal fade bd-example-modal-lg" id="modalAdd" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add Question</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
		 <br>
		 <form action="<?= base_url();?>admin/store-question" method="POST">
			<div class="form-group ml-4 mr-4">
				<label for="">Question</label>
				<input type="text" class="form-control" id="" placeholder="" name="question">
			</div>
			<div class="form-group ml-4 mr-4">
				<label for="">Score</label>
				<input type="number" class="form-control" id="" placeholder="" name="score">
			</div>
			<div class="form-group ml-4 mr-4">
				<label for="">Type</label>
				<select required class="form-control" id="add_tipe" placeholder="" name="type">
				<option value="">--Select--</option>
				<option value="option">Option (Select 1 answer)</option>
				<option value="checkbox">Checkbox (Select all true answer)</option>
				<option value="boolean">True or False</option>
				</select>
			</div>
			<div class="form-group ml-4 mr-4" id="div_number_option" style="display:none" >
				<label for="">Number of Option/Checkboxes</label>
				<input type="number" class="form-control" id="add_number_option" placeholder="" value="0" name="number_option">
			</div>
			<div class="form-group ml-4 mr-4">
				<input type="hidden" class="form-control" id="" placeholder="" value="<?= $id_exam;?>" name="id_exam">
			</div>
			<input type="hidden" class="form-control" id="" placeholder="" value="<?= $data_exam->tipe;?>" name="tipe_exam">
			<div class="modal-footer" id="footerQuestion" style="display:block">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
		</form>
      </div>
   </div>
</div>

<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
	$(document).ready(function(){

		$(document).on('change', '#add_tipe', function(){
			if ($(this).val() != 'boolean')
			{
				$('#div_number_option').show();
			} else {
				$('#div_number_option').hide();
			}
		})

		var statusStoreQuestion	= '<?php echo $this->session->flashdata("status_store_question");?>'
		var msgStoreQuestion	= '<?php echo $this->session->flashdata("msg_store_question");?>'
		var statusUpdateAnswer	= '<?php echo $this->session->flashdata("status_update_answer");?>'
		var msgUpdateAnswer		= '<?php echo $this->session->flashdata("msg_update_answer");?>'

		if (statusStoreQuestion)
		{
			show_toast(statusStoreQuestion, msgStoreQuestion)
		}

		if (statusUpdateAnswer)
		{
			show_toast(statusUpdateAnswer, msgUpdateAnswer)
		}

		var table = $('#dataTableQuestions').DataTable({
			"processing": true,
			"serverSide": true,
			"bStateSave": true,
			"fnStateSave": function (oSettings, oData) {
				localStorage.setItem('offersDataTables', JSON.stringify(oData));
			},
			"fnStateLoad": function (oSettings) {
				return JSON.parse(localStorage.getItem('offersDataTables'));
			},
			"order": [],
			"ajax": {
				"url" : '<?php echo $url_data;?>',
				"type": "POST",
				"async": true,
				// "success": function(data) {
				// 	var countData = data.data.length;
				// 	console.log(data.data);
				// 	console.log(countData);
				// },
			},
			"columnDefs": [
				{
					"targets": [ 3 ],
					"orderable": false,
					"render": function(data, type, full)
					{
						return '<button id="btnAnswer'+full[3]+'" type="button" data-id="'+full[3]+'" data-tipe="'+full[4]+'" class="btn btn-success btn-xs btnAnswer"><i class="fas fa-tasks"></i></button>&nbsp<button id="btnDelete'+full[3]+'" type="button" data-id="'+full[3]+'" class="btn btn-danger btn-xs btnDeleteQ"><i class="fas fa-trash"></i></button>';
					},
					"data": null
				},
			],
		});

		$(document).on("click", ".btnDeleteQ", function(){
	    var id = $(this).data('id');
	    bootbox.confirm("Apakah anda yakin akan menghapus pertanyaan ini?", function(result){
	      if(result == true) {
	        $.ajax({
              url: "<?=$url_delete;?>",
	            method: "POST",
	            data: {id:id},
	            success: function(res){
	              var hasil = $.parseJSON(res);
	              if (hasil["status"] == 200) {
	                 toastr.success(hasil["pesan"]);
	                 setTimeout(function () {
	                   return hasil["status"];
	                 }, 1500);
	                 setTimeout(function () {
	                   location.reload(true);
	                 }, 1500);
	               }else{
	                 toastr.error(hasil["pesan"]);
	                 result = false;
	                 return hasil["status"];
	               }
	            },
	            error: function (res) {
	              toastr.error("Data tidak dapat dihapus.");
	              result = false;
	              return false;
	            },
	        });
	      }
	    });
	  });

		$(document).on('click', '.btnAnswer', function(){
			var idQuestion	= $(this).data('id')
			$("#listAnswers").empty()
			$("#ansIdQuestion").val(idQuestion)
			$("#ansTipeQuestion").val($(this).data('tipe'))

			$("#modalAnswers").modal('show')
			$('#spinnerAnswers').show()
			$('#formAnswers').hide()
			$('#footerAnswers').hide()

			$.ajax({
				url: "<?=$url_answers;?>",
				method: "POST",
				data: {id:idQuestion},
				success: function(res){
					var hasil = $.parseJSON(res);
					if (hasil["status"] == 200) {
						$('#spinnerAnswers').hide()
						$('#formAnswers').show()
						$('#footerAnswers').show()
						console.log(hasil['kunjaw'])
						if ($("#ansTipeQuestion").val() == 'checkbox' && hasil['kunjaw'] != null)
						{
							var arrKunjaw		= hasil["kunjaw"].id_answer.split(",")
							var indexNotNull	= 0

							for(i=0; i<hasil["data"].length; i++)
							{
								if (hasil["data"][i]['id_key'] != null)
								{
									indexNotNull = i
								}
							}
							for(i=1; i<hasil["data"].length; i++)
							{

								if (arrKunjaw.includes(hasil["data"][i]['id_answer']))
								{
									hasil["data"][i]['id_key'] = 	hasil["data"][indexNotNull]['id_key']
								}
							}
						}

						hasil["data"].forEach((val) =>{
							console.log(val)
							if (val.id_key !== null)
							{
								var checked		= 'checked'
								var classColor	= 'alert alert-success inp-answer'
							} else
							{
								var checked		= ''
								var classColor	= 'alert alert-danger inp-answer'
							}

							// console.log($(this).data('tipe'))
							if ($("#ansTipeQuestion").val() == 'option')
							{
								$("#listAnswers").append('<div class="form-group"><b><input name="ans_id_answer[]" type="hidden" value="'+val.id_answer+'"><label><input name="opt" '+checked+' value="'+val.id_answer+'" required type="radio" class="optradio"> Option '+val.opt.toUpperCase()+'.&nbsp&nbsp</label></b><input id="answer'+val.id_answer+'" class="'+classColor+'" style="width:100%" type="text" name="ans_answer[]" value="'+val.answer+'">')
							} else if ($("#ansTipeQuestion").val() == 'boolean')
							{
								$("#listAnswers").append('<div class="form-group"><b><input name="ans_id_answer[]" type="hidden" value="'+val.id_answer+'"><label><input name="opt" '+checked+' value="'+val.id_answer+'" required type="radio" class="optradio"> Option '+val.opt.toUpperCase()+'.&nbsp&nbsp</label></b><input disabled class="'+classColor+'" style="width:100%" type="text" name="ans_answer[]" value="'+val.answer+'"><input id="answer'+val.id_answer+'" class="'+classColor+'" style="width:100%" type="hidden" name="ans_answer[]" value="'+val.answer+'">')
							} else if ($("#ansTipeQuestion").val() == 'checkbox')
							{
								$("#listAnswers").append('<div class="form-group"><b><input name="ans_id_answer[]" type="hidden" value="'+val.id_answer+'"><label><input name="opt[]" '+checked+' value="'+val.id_answer+'" type="checkbox" class="optradiocek"> Option '+val.opt.toUpperCase()+'.&nbsp&nbsp</label></b><input id="answer'+val.id_answer+'" class="'+classColor+'" style="width:100%" type="text" name="ans_answer[]" value="'+val.answer+'">')
							}

						});

					}else{
						toastr.error(hasil["pesan"]);
						result = false;
						return hasil["status"];
					}
				},
				error: function (res) {
					toastr.error("Cannot get data");
					result = false;
					return false;
				},
			});
		})

		$(document).on('click', '.optradio', function(){
			var idAnswer	= $(this).val()
			$('.inp-answer').removeClass("alert-success")
			$('.inp-answer').addClass("alert-danger")

			$('#answer'+idAnswer).removeClass("alert-danger")
			$('#answer'+idAnswer).addClass("alert-success")
		})

		$(document).on('click', '.optradiocek', function(){
			var idAnswer	= $(this).val()
			if($(this).is(":checked"))
			{
				$('#answer'+idAnswer).removeClass("alert-danger")
				$('#answer'+idAnswer).addClass("alert-success")
			} else
			{
				$('#answer'+idAnswer).removeClass("alert-success")
				$('#answer'+idAnswer).addClass("alert-danger")
			}
			// $('.inp-answer').removeClass("alert-success")
			// $('.inp-answer').addClass("alert-danger")

		})

	});
</script>
