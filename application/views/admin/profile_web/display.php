<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <?php
        foreach($breadcrumbs as $br)
        {
      ?>
        <a href="<?= $br['link']; ?>"><h6 class="m-0 font-weight-bold text-primary"><?=$br['text'];?></h6></a>
      <?php
        }
      ?>
    </div>
    <div class="card-body">
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="60%">Description Web Profile</th>
              <th width="35%">Image Web Profile</th>
              <th width="5%"><i class="fa fa-cogs"></i></th>
            </tr>
          </thead>
          <tbody class="row_position">
              <tr>
                <td><?= substr($data->description, 0, 100);?>..</td>
                <td><img src="<?= base_url();?>uploads/profile-web/<?= $data->image;?>" style="width:50%" alt=""></td>
                <td>
                  <button data-toggle="modal" data-target="#modal_edit" class="btn btn-info btn-sm btn-edit"><i class="fas fa-pen"></i></button>
                </td>
              </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Description Web Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>admin/profile-web-update" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <label for="">Description Web Profile</label>
              <textarea name="description" class="form-control" id="ckeditor" required cols="30" rows="10"><?= $data->description; ?></textarea>
            </div>
            <div class="form-group">
              <label for="">Image Web Profile</label>
              <input type="file" name="image" class="form-control">
              <a href="<?= base_url();?>uploads/profile-web/<?= $data->image;?>" target="_blank"><i class="fas fa-eye">Current Image</i></a>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update</button>
          </div>
        </div>
      </form>
  </div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
    var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	  = '<?php echo $this->session->flashdata("error");?>'
    
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}

    CKEDITOR.replace('ckeditor');
  })
</script>
