<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin - <?= env('APP_NAME') ?></title>

  <link rel="shortcut icon" href="<?= getIco(); ?>" />

  <!-- Custom fonts for this template -->
  <link href="<?= base_url();?>assets/adminnew/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?= base_url();?>assets/adminnew/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?= base_url();?>assets/adminnew/css/custom.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="<?= base_url();?>assets/adminnew/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <!-- Select2 -->
  <link href="<?= base_url();?>assets/select2/select2.min.css" rel="stylesheet">

  <!-- Toast -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


  <script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/jquery.min.js"></script>

	<!-- Ladda -->
  <link rel="stylesheet" href="<?=base_url();?>assets/ladda/ladda-themeless.min.css">
	<script src="<?=base_url();?>assets/ladda/spin.min.js"></script>
	<script src="<?=base_url();?>assets/ladda/ladda.min.js"></script>
	<script src="<?=base_url();?>assets/swal/sweetalert2.min.js"></script>

  <!-- CKEDITOR -->
  <script src="<?= base_url();?>assets/ckeditor/ckeditor.js"></script>

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=$dashboard;?>">
        <div class="sidebar-brand-icon">
          <!-- <i class="fas fa-laugh-wink"></i> -->
          <img style="width:80%" src="<?= base_url(); ?>uploads/lms_config/<?= $info->logo_utama;?>" alt="">
        </div>
        <!-- <div class="sidebar-brand-text mx-3"><?= env('APP_NAME_FIRST') ?> <br><?= env('APP_NAME_LAST') ?> <sup></sup></div> -->
      </a>

      <?php foreach ($list_menu as $key => $value) { ?>
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Heading -->
        <div class="sidebar-heading">
          <?= $value->nama_menu;?>
        </div>
        <?php if (!empty($value->child)) { ?>
          <?php foreach ($value->child as $key2 => $value2) { ?>
            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item <?= ($value2->nama_menu == $menu) ? 'active' : '' ;?>">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse<?=$value2->target;?>" aria-expanded="true" aria-controls="collapse<?=$value2->target;?>">
                <i class="<?=$value2->icon;?>"></i>
                <span><?=$value2->nama_menu;?></span>
              </a>
              <div id="collapse<?=$value2->target;?>" class="collapse <?= ($value2->nama_menu == $menu) ? 'show' : '' ;?>" aria-labelledby="heading<?=$value2->target;?>" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Custom <?=$value2->nama_menu;?>:</h6>
                  <?php if (!empty($value2->child)) { ?>
                    <?php foreach ($value2->child as $key3 => $value3) { ?>
                      <a class="collapse-item <?= ($value3->nama_menu == $sub_menu) ? 'active' : '' ;?>" href="<?=base_url().'admin/'.$value3->url;?>"><?=$value3->nama_menu;?></a>
                    <?php } ?>
                  <?php } ?>
                </div>
              </div>
            </li>
          <?php } ?>
        <?php } ?>
      <?php } ?>


      <hr class="sidebar-divider my-0">
      <br>
      <div class="sidebar-heading">
        Setting
      </div>
      <!-- Nav Item - Tables -->
      <li class="nav-item <?= ($sub_menu == 'menu') ? 'active' : '' ;?>">
        <a class="nav-link" href="<?=$config_menu;?>">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Konfigurasi Menu</span></a>
        </li>
      <li class="nav-item <?= ($this->uri->segment(2) == 'terms') ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url();?>admin/terms">
          <i class="fas fa-fw fa-info-circle"></i>
          <span>Terms & Conditions</span>
        </a>
      </li>
      <li class="nav-item <?= ($this->uri->segment(2) == 'cut-off') ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url();?>admin/cut-off">
          <i class="fas fa-fw fa-tag"></i>
          <span>Fee</span>
        </a>
      </li>

      <li class="nav-item <?= ($this->uri->segment(2) == 'profile-web') ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url();?>admin/profile-web">
          <i class="fas fa-fw fa-id-card"></i>
          <span>Profile Web</span>
        </a>
      </li>

      <li class="nav-item <?= ($this->uri->segment(2) == 'lms-config') ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url();?>admin/lms-config">
          <i class="fas fa-fw fa-info"></i>
          <span>LMS Config</span>
        </a>
      </li>

      <li class="nav-item <?= ($this->uri->segment(2) == 'gallery') ? 'active' : '' ;?>">
        <a class="nav-link" href="<?= base_url();?>admin/gallery-photo">
          <i class="fas fa-fw fa-image"></i>
          <span><?= $this->lang->line('gallery'); ?></span>
        </a>
      </li>
      <!-- Sidebar Toggler (Sidebar) -->
      <hr class="sidebar-divider d-none d-md-block">


      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <?php 
              $getUnreadNotif = getUnreadNotification();
              $countUnreadNotif = count($getUnreadNotif);
            ?>

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <?php if ($countUnreadNotif > 0) : ?>
                  <span class="badge badge-danger badge-counter"><?= $countUnreadNotif ?></span>
                <?php endif; ?>
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  <?= $this->lang->line('notification') ?>
                </h6>
                <?php if ($countUnreadNotif > 0) : ?>
                  <?php foreach ($getUnreadNotif as $row) : ?>
                  <a class="dropdown-item d-flex align-items-center" <?= $row->st_viewed == 0 ? 'style="background-color: #eaeaea"' : null ?> href="<?= base_url('common/read_notif/' . $row->id) ?>">
                    <div class="mr-3">
                      <div class="icon-circle bg-primary">
                        <i class="fas fa-file-alt text-white"></i>
                      </div>
                    </div>
                    <div>
                      <div class="small text-gray-500"><?= date('d/m/Y H:i:s', strtotime($row->created_at)) ?></div>
                      <span class="font-weight-bold"><?= $row->message ?></span>
                    </div>
                  </a>
                  <?php endforeach; ?>
                <?php else : ?>
                  <span class="dropdown-item d-flex align-items-center">
                    <div class="mr-3">
                      <div class="icon-circle bg-primary">
                        <i class="fas fa-file-alt text-white"></i>
                      </div>
                    </div>
                    <div>
                      <span class="font-weight-bold"><?= $this->lang->line('no_notification') ?></span>
                    </div>
                  </span>
                <?php endif; ?>
                <!-- <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a> -->
              </div>
            </li>
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$nama['nama_depan'].' '.$nama['nama_belakang'].' ('.$nama['institusi'].')';?></span>
                  <?php if ($nama['avatar'] == null || $nama['avatar'] == '') { ?>
                    <?php if ($nama['jk'] == 'W') { ?>
                      <img class="img-profile rounded-circle" src="<?= base_url();?>assets/default/female_avatar_s0lm1t.png">
                    <?php }else{ ?>
                      <img class="img-profile rounded-circle" src="<?= base_url();?>assets/default/male_avatar_s0lm1t.png">
                    <?php } ?>
                  <?php }else{ ?>
                    <img class="img-profile rounded-circle" src="<?= base_url();?>assets/avatar/<?=$nama['avatar'];?>">
                  <?php } ?>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <?= $content; ?>

        <div class="modal fade" id="ModalSmall" tabindex="-1" role="dialog" aria-labelledby="ModalLabelSmall" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div id="dataModalSmall"></div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="ModalLarge" tabindex="-1" role="dialog" aria-labelledby="ModalLabelLarge" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div id="dataModalLarge"></div>
            </div>
          </div>
        </div>

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright &copy; <?= env('APP_NAME') ?> <?= date("Y"); ?></span>
            </div>
          </div>
        </footer>
        <!-- End of Footer -->

      </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="<?=$logout;?>" url="">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <script src="<?= base_url();?>assets/eshop/eshop/js/custom.js"></script>

    <!-- Toast -->
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- Select2 -->
    <script src="<?= base_url();?>assets/select2/select2.min.js"></script>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url();?>assets/adminnew/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url();?>assets/adminnew/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url();?>assets/adminnew/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="<?= base_url();?>assets/adminnew/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url();?>assets/adminnew/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="<?= base_url();?>assets/adminnew/js/demo/datatables-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript">
    function logout(obj) {
      var url = $(obj).attr('url');
      $.ajax({
        url: url,
        success: function (res) {
          var hasil = $.parseJSON(res);
          toastr.success(hasil["pesan"]);
          setTimeout(function () {
              window.location = hasil["url"];
            }, 1500);
        },
        error: function (res) {
          toastr.error("Tidak bisa logout.");
        },
      });
    }
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-H9GN9YG0KB"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-H9GN9YG0KB');
    </script>


  </body>

  </html>
