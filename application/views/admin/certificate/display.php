<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('admin/course'); ?>">Course <i class="fas fa-angle-double-right"></i></a> Certificate <i class="fas fa-angle-double-right"></i> <?=$title;?> </h6>
		</div>
		<div class="card-body">
			<div align="right">
		        <a href="#" class="btn btn-success btn-icon-split getModal" style="align:right;">
		          <span class="icon text-white-50">
		            <i class="fas fa-plus"></i>
		          </span>
		          <span class="text">Tambah</span>
		        </a>
		      </div>
			<br>
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead class="thead-dark">
						<tr>
							<th width="5%">No. </th>
							<th width="80%">Course Name</th>
							<!-- <th width="3%">Certificate Number</th> -->
							<th width="80%">#</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
	function onEdit(data, row, tr) {
        $.ajax({
            url: "<?=$get_data_edit;?>",
            method: "POST",
            data: {id: data.id_course},
            success: function(data){
                $('#dataModalLarge').html(data);
                $('#ModalLarge').modal('show');
            }
        });
	}
	function onPreview(data, row, tr) {
		$.ajax({
		    url: "<?=$get_data_preview;?>",
		    method: "POST",
		    data: {id: data.id_course},
		    success: function (res) {
		    	let r = JSON.parse(res)
		    	window.open(r.url, '_blank');
		    },
		    error: function (res) {
		      
		    },
		});
	}
	function onGenerate(data, row, tr) {
		$.ajax({
		    url: "<?=$get_data_generate;?>",
		    method: "POST",
		    data: {id: data.id_course},
		    success: function (res) {
		    	let r = JSON.parse(res)
						if (r["status"] == true) {
						 toastr.success(r["msg"]);
						}else{
						 toastr.error(r["msg"]);
						}
		    },
		    error: function (res) {
		    },
		});
	}
	function onDelete(data, row, tr) {
      bootbox.confirm("Apakah anda yakin akan menghapus data ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_data_delete;?>",
              method: "POST",
              data: {id: data.id},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     return hasil["status"];
                   }, 1500);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   toastr.error(hasil["pesan"]);
                   result = false;
                   return hasil["status"];
                 }
              },
              error: function (res) {
                toastr.error("Data tidak dapat dihapus.");
                result = false;
                return false;
              },
          });
        }
      });
	}
	$(document).ready(function(){
        var table = $('#dataTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "order": [], 
				rowId: 'id',
            "ajax": {
                "url" : '<?php echo $url_data;?>',
                "type": "POST"
            },
            columns: [
				{
					label: 'No',
					width: 2,
					data: "no",
					searchable: false,
		            orderable: false,
				},
				{
					label: 'Course Name',
					data: "judul",
					width: 80,
					sortable: true,
					searchable: true,
					defaultOrder: 'desc'
				},
				// {
				// 	label: 'Certificate Number',
				// 	data: "certificate_number",
				// 	width: 10,
				// 	sortable: true,
				// 	searchable: true,
				// 	defaultOrder: 'desc'
				// },
				{
					isLocal: true,
					label: '#',
					searchable: false,
					sortable: false,
					width: 10,
					defaultContent: '<button class="btn btn-sm btn-success" type="button" value="Edit" data-action="onEdit"><i class="fas fa-edit"></i></button> ' +
						'<button class="btn btn-sm btn-danger" type="button" value="Delete" data-action="onDelete"><i class="fa fa-trash"></i></button> ' 
						// '<button class="btn btn-sm btn-primary" type="button" value="Preview" data-action="onPreview"><i class="fa fa-search"></i></button> '+
						// '<button class="btn btn-sm btn-primary" type="button" value="Generate" data-action="onGenerate"><i class="fa fa-cog"></i></button>'
				},
			],
        });

	    $('#dataTable').on('click', '[data-action]', (e) => {
	        e.preventDefault()

	        var target = $(e.target);
	        var action = target.attr('data-action');

	        var tr = target.closest('tr');
	        var row = table.row(tr);
	        var data = row.data();

	        if (typeof window[action] === 'function') {
	            window[action](data, row, tr);
	        } else {
	            console.log('function ' + action + ' not found ');
	        }
	    });

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	});

	$(".getModal").click(function(event){
		$.ajax({
		    url: "<?=$get_data_edit;?>",
		    method: "POST",
		    data: {id:''},
		    success: function(data){
		        $('#dataModalLarge').html(data);
		        $('#ModalLarge').modal('show');
		    }
		});
	});
</script>