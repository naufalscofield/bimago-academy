<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?=$judul;?></h6>
    </div>
    <div class="card-body">
      <div align="right">
        <a href="#" id="" class="btn ladda-button btn-success btn-icon-split getModal ladda-update_" style="align:right;">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="25%">Modul</th>
              <th width="10%">Aksi</th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
              <?php if ($data['status'] == 200) { ?>
                  <tr id="<?php echo $value['id'] ?>">
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_kategori'];?></td>
                    <td>
                      <a id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-info getModal ladda-update_<?=$value['id'];?>">
                        <i style="color:white" class="fas fa-info-circle"></i>
                      </a>
                      <a id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-danger deleteData ladda-delete_<?=$value['id'];?>">
                        <i style="color:white" class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
              <?php }else{ ?>
                <td>Tidak Ada Data !</td>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){

    $(".getModal").click(function(event){
      var id = $(this).attr('id');
      var l = Ladda.create( document.querySelector( '.ladda-update_' + id));
      l.start();
        $.ajax({
            url: "<?=$get_data_edit;?>",
            method: "POST",
            data: {id:id},
            success: function(data){
              l.stop();
              $('#dataModalLarge').html(data);
              $('#ModalLarge').modal('show');
            },
            error: function (data) {
              l.stop();
              toastr.error("Data tidak dapat dibuka, coba kembali.");
            },
        });
      });

    $(".deleteData").click(function(event){
      var id = $(this).attr('id');
      var l = Ladda.create( document.querySelector( '.ladda-delete_' + id));
  		l.start();
      bootbox.confirm("Apakah anda yakin akan menghapus data ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_data_delete;?>",
              method: "POST",
              data: {id:id},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                  l.stop();
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   l.stop();
                   toastr.error(hasil["pesan"]);
                 }
              },
              error: function (res) {
                l.stop();
                toastr.error("Data tidak dapat dihapus.");
              },
          });
        }else{
          l.stop();
        }
      });
    });

  });
</script>
