<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <?php 
        foreach ($breadcrumbs as $idx => $br)
        {
      ?>
        <a href="<?= base_url();?>lecturer/<?= $br['link'];?>">
          <?= $br['title'];?> <?= (isset($br['akhir'])) ? '' : '<i class="fas fa fa-angle-double-right"></i>';?>
        </a>
      <?php
        }
      ?>
    </div>
    <div class="card-body">

      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%"><center>No. </center></th>
              <th width="20%"><center><?= $this->lang->line('name_');?></center></th>
              <th width="10%"><center><?= $this->lang->line('date');?></center></th>
              <th width="10%"><center>Nominal (Rp.)</center></th>
              <th width="20%"><center>Status</center></th>
              <th width="10%"><center><i class="fas fa fa-cogs"></i></center></th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php
              foreach($withdrawal as $idx => $d)
              {
            ?>
              <tr>
                <td><center><?= $idx+1 ;?></center></td>
                <td><center><?= $d['nama_depan'].' '.$d['nama_belakang'];?></center></td>
                <td><center><?= date("d F Y - H:i:s", strtotime($d['created_at']));?></center></td>
                <td><center><?= "Rp " . number_format($d['total'],2,',','.'); ?></center></td>
                <td>
                  <center>
                  <?php
                    switch ($d['status']) {
                      case 1:
                        echo "<span style='font-size:15px' class='badge badge-success'>".$this->lang->line("verification_by_admin").' '.env("APP_NAME")."</span>";
                        break;
                      case 2:
                        echo "<span style='font-size:15px' class='badge badge-success'>".$this->lang->line("withdrawal_process")."</span>";
                        break;
                      case 3;
                        echo "<span style='font-size:15px' class='badge badge-success'>".$this->lang->line("done")."</span>";
                        break;
                    }
                  ?>
                  </center>
                </td>
                <td>

                  <center>
                  <?php
                    switch ($d['status']) {
                      case 1:
                        ?><button class="btn btn-danger btn-xs btn-verify" data-id="<?= $d['id'];?>"><i class="fas fa fa-check"></i> <?= $this->lang->line('verify');?></button><?php
                        break;
                      case 3:
                        ?><button class="btn btn-success btn-xs btn-receipt" data-id="<?= $d['id'];?>"><i class="fas fa fa-eye"></i> <?= $this->lang->line('see_receipt');?></button><?php
                        break;
                    }
                  ?>
                  </center>
                </td>
              </tr>
            <?php
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<!-- Modal -->
<div class="modal fade" id="modal_receipt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= $this->lang->line('see_receipt'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="" id="img_receipt" alt="" style="width:100%;height:100%">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
		var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	= '<?php echo $this->session->flashdata("error");?>'
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}
	})

  $(document).on('click', '.btn-receipt', function(){
    var id = $(this).data("id")
    var route = '<?php echo base_url();?>admin/show-withdrawal-request/'+id
    $("#modal_receipt").modal("show")
    $.get(route, function(data, status){
      if (status)
      {
        var data = JSON.parse(data)
        console.log(data)
        $("#img_receipt").attr("src", "<?php echo base_url();?>uploads/withdrawal_receipt/"+data.data.receipt)
      }
    }).fail(function(data, error) {
        var err = eval("(" + data.responseText + ")");
        toastr.error(err)
    })
  })
  $(document).on('click', '.btn-verify', function(){
    var confirm = "<?php echo $this->lang->line('confirm_withdrawal_request');?>"
    var id = $(this).data("id")
    bootbox.confirm(confirm, function(result){
      if(result == true) {
        var route = '<?php echo base_url();?>admin/verify-withdrawal-request/'+id

        $.get(route, function(data, status){
          if (status)
          {
            toastr.success(data)
            setTimeout(function(){ location.reload() }, 1000);
          }
        }).fail(function(data, error) {
            var err = eval("(" + data.responseText + ")");
            toastr.error(err)
        })
        
      }
    });
  })

</script>
