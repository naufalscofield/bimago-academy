<style>
  .select2-container{
 width: 100%!important;
 }
 .select2-search--dropdown .select2-search__field {
 width: 98%;
 }
</style>
<?php echo form_open_multipart($simpan, array('name' => 'modal-course', 'id' => 'modal-course')); ?>
  <div class="modal-header">
    <h5 class="modal-title" id="ModalLabelLarge"><b>Master Course</b></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <input type="hidden" name="id" value="<?= !empty($data['id']) ? $data['id'] : '';?>">
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Pengajar</label>
        <?php echo form_dropdown('kontributor', $opt_lecturer, !empty($data['kontributor']) ? $data['kontributor'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Modul</label>
        <?php echo form_dropdown('id_modul', $opt_modul, !empty($data['id_modul']) ? $data['id_modul'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Judul</label>
        <?php echo form_input('judul', !empty($data['judul']) ? $data['judul'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Kategori</label>
        <?php array_shift($opt_kategori); ?>
        <?php echo form_dropdown('kategori[]', $opt_kategori, $kategori, ' class="form-control form-control-user select-multiple" multiple="multiple"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Deskripsi Singkat</label>
        <?php echo form_textarea('deskripsi_singkat', !empty($data['deskripsi_singkat']) ? $data['deskripsi_singkat'] : '', 'class="form-control form-control-user" id="ckeditor"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Deskripsi Lengkap</label>
        <?php echo form_textarea('deskripsi', !empty($data['deskripsi']) ? $data['deskripsi'] : '', 'class="form-control form-control-user" id="ckeditor2"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Image</label>
        <?php echo form_upload('image', '', 'class="form-control form-control-user"');?>
        <?php echo form_input('image_old', !empty($data['image']) ? $data['image'] : '', 'class="form-control form-control-user" hidden');?>
        <p style="color:red; font-size:13px">Pastikan gambar memiliki lebar dan panjang (800x500)px</p>
      </div>
      <div class="col-sm-6">
        <label>Trailer</label>
        <?php echo form_input('url_youtube', !empty($data['url_youtube']) ? $data['url_youtube'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
    <div class="form-group row">
      <?php
      foreach ($type as $key => $value) { ?>
        <div class="col-sm-3">
          <?php if (isset($data['type_course'])) { ?>
            <input type="radio" name="type_course[]" id="type_<?=$value['id'];?>" value="<?=$value['id'];?>" <?= in_array($value['id'], $data['type_course']) ? 'checked' : '' ;?>> <b><?=$value['type_course'];?></b>
          <?php }else{ ?>
            <input type="radio" name="type_course[]" id="type_<?=$value['id'];?>" value="<?=$value['id'];?>"> <b><?=$value['type_course'];?></b>
          <?php } ?>
        </div>
      <?php } ?>
    </div>
    <div class="form-group row" id="type_content_1">
      <div class="col-sm-6">
        <label>Harga Self Paced Study</label>
        <input class="form-control form-control-user" type="number" name="harga_1" value="<?=!empty($data['harga'][1]['harga']) ? $data['harga'][1]['harga'] : 0;?>">
      </div>
      <div class="col-sm-6">
        <label>Discount (%)</label>
        <input class="form-control form-control-user" type="number" name="discount_1" value="<?=!empty($data['harga'][1]['discount']) ? $data['harga'][1]['discount'] : 0;?>">
      </div>
    </div>
    <div id="type_content_2">
      <div class="form-group row">
        <div class="col-sm-4">
          <label>Harga Online - Intructure Led</label>
          <input class="form-control form-control-user" type="number" name="harga_2" value="<?=!empty($data['harga'][2]['harga']) ? $data['harga'][2]['harga'] : 0;?>">
        </div>
        <div class="col-sm-4">
          <label>Discount (%)</label>
          <input class="form-control form-control-user" type="number" name="discount_2" value="<?=!empty($data['harga'][2]['discount']) ? $data['harga'][2]['discount'] : 0;?>">
        </div>
        <div class="col-sm-4">
          <label>Dimulainya Instructure Led</label>
          <input type="date" name="pelaksanaan_led" value="<?=isset($data['pelaksanaan_led']) ? $data['pelaksanaan_led'] : date('Y-m-d');?>" class="form-control form-control-user">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-6">
          <label>ID Zoom</label>
          <?php echo form_input('link_zoom', !empty($data['link_zoom']) ? $data['link_zoom'] : '', 'class="form-control form-control-user"');?>
        </div>
        <div class="col-sm-6">
          <label>Password Zoom</label>
          <?php echo form_input('pass_zoom', !empty($data['pass_zoom']) ? $data['pass_zoom'] : '', 'class="form-control form-control-user"');?>
        </div>
      </div>
    </div>
    <div id="type_content_3">
      <div class="form-group row">
        <div class="col-sm-4">
          <label>Harga Class Training</label>
          <input class="form-control form-control-user" type="number" name="harga_3" value="<?=!empty($data['harga'][3]['harga']) ? $data['harga'][3]['harga'] : 0;?>">
        </div>
        <div class="col-sm-4">
          <label>Discount (%)</label>
          <input class="form-control form-control-user" type="number" name="discount_3" value="<?=!empty($data['harga'][3]['discount']) ? $data['harga'][3]['discount'] : 0;?>">
        </div>
        <div class="col-sm-4">
          <label>Pendaftaran Terakhir Training</label>
          <input type="date" name="pelaksanaan_training" value="<?=isset($data['pelaksanaan_training']) ? $data['pelaksanaan_training'] : date('Y-m-d');?>" class="form-control form-control-user">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-12">
          <label>Info Class Training (Dikirim pada pembeli training)</label>
          <?php echo form_textarea('deskripsi_training', !empty($data['deskripsi_training']) ? $data['deskripsi_training'] : '', 'class="form-control form-control-user" id="ckeditor3"');?>
        </div>
      </div>
    </div>
    <div id="type_content_4">
      <div class="form-group row">
        <div class="col-sm-6">
          <label>Harga Webinar</label>
          <input class="form-control form-control-user" type="number" name="harga_4" value="<?=!empty($data['harga'][4]['harga']) ? $data['harga'][4]['harga'] : 0;?>">
        </div>
        <div class="col-sm-4">
          <label>Discount (%)</label>
          <input class="form-control form-control-user" type="number" name="discount_4" value="<?=!empty($data['harga'][4]['discount']) ? $data['harga'][4]['discount'] : 0;?>">
        </div>
        <div class="col-sm-6">
          <label>Pelaksanaan Webinar</label>
          <input type="datetime-local" min="<?=date('Y-m-d');?>" name="pelaksanaan_webinar" value="<?=isset($data['pelaksanaan_webinar']) ? $data['pelaksanaan_webinar'] : date('Y-m-d');?>" class="form-control form-control-user">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-6">
          <label>ID Zoom Webinar</label>
          <?php echo form_input('id_zoom_webinar', !empty($data['id_zoom_webinar']) ? $data['id_zoom_webinar'] : '', 'class="form-control form-control-user"');?>
        </div>
        <div class="col-sm-6">
          <label>Password Zoom Webinar</label>
          <?php echo form_input('pass_zoom_webinar', !empty($data['pass_zoom_webinar']) ? $data['pass_zoom_webinar'] : '', 'class="form-control form-control-user"');?>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-sm-6">
          <label>Background Webinar</label>
          <?php echo form_upload('background_webinar', '', 'class="form-control form-control-user"');?>
          <?php echo form_input('background_webinar_old', !empty($data['background_webinar']) ? $data['background_webinar'] : '', 'class="form-control form-control-user" hidden');?>
        </div>
        <div class="col-sm-6">
          <label>.</label>
          <img src="<?= !empty($data['background_webinar']) ? base_url().$data['background_webinar'] : '';?>" style="width:100%;height:auto;" alt="">
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-12">
          <label>Pembicara / Speakers</label>
          <?php echo form_textarea('pembicara_webinar', !empty($data['pembicara_webinar']) ? $data['pembicara_webinar'] : '', 'class="form-control form-control-user" id="ckeditor4"');?>
        </div>
      </div>
    </div>

  </div>
  <div class="modal-footer">
  <button type="button" id="simpan" class="btn btn-dark" <?=$disable;?>>Simpan</button>
<?php echo form_close() ?>

<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script type="text/javascript">

  CKEDITOR.replace('ckeditor');
  CKEDITOR.replace('ckeditor2');
  CKEDITOR.replace('ckeditor3');
  CKEDITOR.replace('ckeditor4');

  $( document ).ready(function() {
    $('.select-multiple').select2();

    if ($("#type_1").is(":checked")) {
        $("#type_content_1").show();
        $("#type_content_2").hide();
        $("#type_content_3").hide();
        $("#type_content_4").hide();
    }
    if ($("#type_2").is(":checked")) {
        $("#type_content_2").show();
        $("#type_content_1").hide();
        $("#type_content_3").hide();
        $("#type_content_4").hide();
    }

    if ($("#type_3").is(":checked")) {
        $("#type_content_3").show();
        $("#type_content_1").hide();
        $("#type_content_2").hide();
        $("#type_content_4").hide();
    }

    if ($("#type_4").is(":checked")) {
        $("#type_content_4").show();
        $("#type_content_1").hide();
        $("#type_content_2").hide();
        $("#type_content_3").hide();
    }
  });

  $(function () {
    $("#type_1").click(function () {
      if ($("#type_1").is(":checked")) {
          $("#type_content_1").show();
          $("#type_content_2").hide();
          $("#type_content_3").hide();
          $("#type_content_4").hide();
      }
    });
    $("#type_2").click(function () {
      if ($("#type_2").is(":checked")) {
          $("#type_content_2").show();
          $("#type_content_1").hide();
          $("#type_content_3").hide();
          $("#type_content_4").hide();
      }
    });
    $("#type_3").click(function () {
      if ($("#type_3").is(":checked")) {
          $("#type_content_3").show();
          $("#type_content_1").hide();
          $("#type_content_2").hide();
          $("#type_content_4").hide();
      }
    });
    $("#type_4").click(function () {
      if ($("#type_4").is(":checked")) {
          $("#type_content_4").show();
          $("#type_content_1").hide();
          $("#type_content_2").hide();
          $("#type_content_3").hide();
      } 
    });
  });

  $("#simpan").click(function(){
    for (instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].updateElement();
    }
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
    bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function(result){
      if(result == true) {
        $.ajax({
          type: form.attr("method"),
          url: form.attr("action"),
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          success: function (res) {
            var hasil = $.parseJSON(res);
            if (hasil["status"] == 200) {
               toastr.success(hasil["pesan"]);
               setTimeout(function () {
                 return hasil["status"];
               }, 1500);
               $('#ModalLarge').modal('hide');
               setTimeout(function () {
                 location.reload(true);
               }, 1500);
             }else{
               toastr.error(hasil["pesan"]);
               result = false;
               return hasil["status"];
             }
           },
           error: function (res) {
             toastr.error("Data tidak dapat disimpan.");
             result = false;
             return false;
           },
        });
      }
    });
  });
</script>
