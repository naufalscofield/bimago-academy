<style>
  /* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  -webkit-animation: spinner 150ms infinite linear;
  -moz-animation: spinner 150ms infinite linear;
  -ms-animation: spinner 150ms infinite linear;
  -o-animation: spinner 150ms infinite linear;
  animation: spinner 150ms infinite linear;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
box-shadow: rgba(255,255,255, 0.75) 1.5em 0 0 0, rgba(255,255,255, 0.75) 1.1em 1.1em 0 0, rgba(255,255,255, 0.75) 0 1.5em 0 0, rgba(255,255,255, 0.75) -1.1em 1.1em 0 0, rgba(255,255,255, 0.75) -1.5em 0 0 0, rgba(255,255,255, 0.75) -1.1em -1.1em 0 0, rgba(255,255,255, 0.75) 0 -1.5em 0 0, rgba(255,255,255, 0.75) 1.1em -1.1em 0 0;
}

/* Animation */

@-webkit-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-moz-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@-o-keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes spinner {
  0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<div class="loading">Loading&#8230;</div>
<?php echo form_open_multipart($simpan, array('name' => 'modal-course', 'id' => 'modal-course')); ?>
  <div class="modal-header">
    <h5 class="modal-title" id="ModalLabelLarge"><b>Master Episode</b></h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <input type="hidden" name="id" value="<?= !empty($data['id']) ? $data['id'] : '';?>">
    <input type="hidden" id="id_type_hidden" value="<?= !empty($data['id_type_course']) ? $data['id_type_course'] : '';?>">
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Course</label>
        <?php echo form_dropdown('id_course', $opt_modul, !empty($data['id_course']) ? $data['id_course'] : '', 'class="form-control form-control-user" id="id_course"');?>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Type Course</label>
        <select class="form-control" name="id_type_course" id="id_type_course" required>
          <option value="">-</option>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Judul</label>
        <?php echo form_input('judul', !empty($data['judul']) ? $data['judul'] : '', 'class="form-control form-control-user"');?>
      </div>
    </div>
     <div class="form-group row">
      <div class="col-sm-10">
        <label>Video Upload</label>
        <?php echo form_upload('video', '', 'class="form-control form-control-user" accept=".mov,.mp4"');?>
      </div>
      <div class="col-sm-2">
        <label>Urutan</label><span style="color:red;font-size:13px"> * </span>
        <input class="form-control form-control-user" type="number" name="order" value="<?=!empty($data['order']) ? $data['order'] : '';?>">
      </div>
      <div class="col-sm-4 d-none">
        <label>Upload to</label>
        <?php echo form_dropdown('type', ['gdrive'=> 'Google Drive ','youtube'=> 'Youtube'], !empty($data['type']) ? $data['type'] : '', 'class="form-control form-control-user"');?>
      </div>
      <span style="color:red;font-size:13px" class="pl-3 pr-3">File upload harus kurang dari 200mb, dan lebih disarankan lebih kecil (dibawah 100MB) agar proses upload lebih cepat.</span>
    </div>
   <div class="form-group row d-none">
    <div class="col-sm-12">
        <label>Video Location</label>
        <?php echo form_input('video_old', !empty($data['video']) ? $data['video'] : '', 'class="form-control form-control-user" readonly="true"');?>
        <!-- <p style="color:red;font-size:13px">Video yang di pilih harus file berformat (.mov) untuk kebutuhan upload googledrive.</p> -->
      </div>
    </div>
    <div class="form-group row">
      <!-- <div class="col-sm-4">
        <label>Durasi Video (Menit)</label>
        <input class="form-control form-control-user" type="number" name="length" value="<?=!empty($data['length']) ? $data['length'] : '';?>">
      </div> -->
      <div class="col-sm-10 d-none">
        <label>URL</label>
        <?php echo form_input('url', !empty($data['url']) ? $data['url'] : '', 'class="form-control form-control-user"');?>
        <p style="color:red;font-size:13px">Video dengan format (mov/mp4) akan diupload ke google drive / youtube dan url akan tergenerate otomatis.</p>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-12">
        <label>Deskripsi</label>
        <?php echo form_textarea('deskripsi', !empty($data['deskripsi']) ? $data['deskripsi'] : '', 'class="form-control form-control-user" id="ckeditor"');?>
      </div>
    </div>
  </div>
  <div class="modal-footer">
  <button type="button" id="simpan" class="btn btn-dark" <?=$disable;?>>Simpan</button>
<?php echo form_close() ?>

<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">
  $('.loading').hide();
  CKEDITOR.replace('ckeditor');
  $("#simpan").click(function(){
    for (instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].updateElement();
    }
    var form = $("#" + $(this).closest('form').attr('name'));
    var formdata = false;
    if (window.FormData) {
      formdata = new FormData(form[0]);
    }
    bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function(result){
      if(result == true) {
        $('.loading').show();
        $.ajax({
          type: form.attr("method"),
          url: form.attr("action"),
          data: formdata ? formdata : form.serialize(),
          processData: false,
          contentType: false,
          cache: false,
          // async: false,
          success: function (res) {
            var hasil = $.parseJSON(res);
            if (hasil["status"] == 200) {
               $('.loading').hide();
               toastr.success(hasil["pesan"]);
               setTimeout(function () {
                 return hasil["status"];
               }, 1500);
               $('#ModalLarge').modal('hide');
               setTimeout(function () {
                 location.reload(true);
               }, 1500);
             }else{
               $('.loading').hide();
               toastr.error(hasil["pesan"]);
               result = false;
               return hasil["status"];
             }
           },
           error: function (res) {
             $('.loading').hide();
             toastr.error(eval('('+res.message+')'));
             result = false;
             return false;
           },
        });
      }
    });
  });

  $("#id_course").change(function(event){
    $('#id_type_course')
    .find('option')
    .remove()
    .end()

    var idCourse  = $(this).val();
    $.ajax({
      url: "<?=$url_get_tipe_course;?>",
      method: "POST",
      data: {id:idCourse},
      success: function(res){
        var hasil = $.parseJSON(res);
        if (hasil["status"] == 200) {
          $("#id_type_course").html(hasil["type"]);
        }else{
          toastr.error(hasil["pesan"]);
        }
      },
      error: function (res) {
        toastr.error("Cannot get data");
      },
    });
  })

  $(document).ready(function() {
    var idCourse  = $('#id_course').val();
    var idType  = $('#id_type_hidden').val();
    $.ajax({
      url: "<?=$url_get_tipe_course;?>",
      method: "POST",
      data: {id:idCourse,id_type_course:idType},
      success: function(res){
        var hasil = $.parseJSON(res);
        if (hasil["status"] == 200) {
          $("#id_type_course").html(hasil["type"]);
        }else{
          toastr.error(hasil["pesan"]);
        }
      },
      error: function (res) {
        toastr.error("Cannot get data");
      },
    });
  });
  
</script>
