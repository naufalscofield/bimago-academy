<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('admin/course'); ?>">Course <i class="fas fa-angle-double-right"></i></a> Episode <i class="fas fa-angle-double-right"></i> <?=$judul;?> </h6>
    </div>
    <div class="card-body">
      <div align="right">
        <a href="#" class="btn btn-success btn-icon-split getModal" style="align:right;">
          <span class="icon text-white-50">
            <i class="fas fa-plus"></i>
          </span>
          <span class="text">Tambah</span>
        </a>
      </div>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="5%">No. </th>
              <th width="20%">Judul Episode</th>
              <th width="18%">Course</th>
              <th width="10%">Type Course</th>
              <th width="10%">Urutan</th>
              <th width="5%">Status</th>
              <th width="5%">Materi</th>
              <th width="5%">Tugas</th>
              <th width="7%">Quiz</th>
              <th width="5%">Verifikasi</th>
              <th width="15%">Aksi</th>
            </tr>
          </thead>
          <tbody class="row_position">
            <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
              <?php if ($data['status'] == 200) { ?>
                  <tr id="<?php echo $value['id'] ?>">
                    <td><?= $no;?>.</td>
                    <td><?= $value['judul'];?></td>
                    <td><?= $value['judul_course'];?></td>
                    <td><?= $value['type_course'];?></td>
                    <td><?= $value['order'];?></td>
                    <?php if ($value['status'] == 2) { ?>
                      <td style="background-color:#cbf5cf;">
                        <span>Ya</span>
                      </td>
                    <?php }else{ ?>
                      <td style="background-color:#f5a2a7;">
                        <span>Tidak</span>
                      </td>
                    <?php } ?>
                    <td>
                      <a href="<?=$materi.$value['id'];?>" class="btn btn-success btn-sm">
                        List <i class="fas fa-book"></i>
                      </a>
                    </td>
                    <td>
                      <a href="<?=$tugas.$value['id'];?>" class="btn btn-success btn-sm">
                        List <i class="fas fa-tasks"></i>
                      </a>
                    </td>
                    <td>
                      <a href="<?= base_url();?>admin/quiz/<?= $value['id'];?>" class="btn btn-success btn-sm">
                        List <i class="fas fa-pencil-ruler"></i>
                      </a>
                    </td>
                      <td>
                        <a id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-warning updateStatus ladda-status_<?=$value['id'];?>">
                          Update
                        </a>
                      </td>
                      <td>
                      <a href="#" id="<?=$value['id'];?>" class="btn btn-info btn-circle btn-sm getModal">
                        <i class="fas fa-info-circle"></i>
                      </a>
                       <a href="#" id="<?=$value['id'];?>" class="btn btn-danger btn-circle btn-sm videoData">
                        <i class="fas fa-play"></i>
                      </a>
                      <a href="#" id="<?=$value['id'];?>" class="btn btn-danger btn-circle btn-sm deleteData">
                        <i class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
              <?php }else{ ?>
                <td>Tidak Ada Data !</td>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){

    $(".getModal").click(function(event){
      var id = $(this).attr('id');
        $.ajax({
            url: "<?=$get_data_edit;?>",
            method: "POST",
            data: {id:id},
            success: function(data){
                $('#dataModalLarge').html(data);
                $('#ModalLarge').modal('show');
            }
        });
      });

      $(".videoData").click(function(event){
        var id = $(this).attr('id');
          $.ajax({
              url: "<?=$get_video;?>",
              method: "POST",
              data: {id:id},
              success: function(data){
                  $('#dataModalLarge').html(data);
                  $('#ModalLarge').modal('show');
              }
          });
        });

    $(".deleteData").click(function(event){
      var id = $(this).attr('id');
      bootbox.confirm("Apakah anda yakin akan menghapus data ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_data_delete;?>",
              method: "POST",
              data: {id:id},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     return hasil["status"];
                   }, 1500);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   toastr.error(hasil["pesan"]);
                   result = false;
                   return hasil["status"];
                 }
              },
              error: function (res) {
                toastr.error("Data tidak dapat dihapus.");
                result = false;
                return false;
              },
          });
        }
      });
    });

    $(".updateStatus").click(function(event){
  		var id = $(this).attr('id');
  		var l = Ladda.create( document.querySelector( '.ladda-status_' + id));
  		l.start();
  		bootbox.confirm("Ubah status verifikasi pelatihan?", function(result){
  			if(result == true) {
  				$.ajax({
  						url: "<?=$update_status;?>",
  						method: "POST",
  						data: {id:id},
  						success: function(res){
  							var hasil = $.parseJSON(res);
  							if (hasil["status"] == 200) {
  								l.stop();
  								 toastr.success(hasil["pesan"]);
  								 setTimeout(function () {
  									 location.reload(true);
  								 }, 1500);
  							 }else{
  								 l.stop();
  								 toastr.error(hasil["pesan"]);
  							 }
  						},
  						error: function (res) {
  							l.stop();
  							toastr.error("Data tidak dapat diubah.");
  						},
  				});
  			}else{
  				l.stop();
  			}
  		});
  	});

  });
</script>
