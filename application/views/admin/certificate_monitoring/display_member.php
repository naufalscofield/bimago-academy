<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?=$judul;?></h6>
    </div>
    <div class="card-body">
      <?php echo form_open_multipart($email, array('name' => 'form-member', 'id' => 'form-member')); ?>
      <input type="hidden" name="id_course" value="<?=$id_course;?>">
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="2%">#</th>
              <th width="5%">No.</th>
              <th width="25%">Name </th>
              <th width="25%">Certificate Number</th>
              <th width="10%">#</th>
            </tr>
          </thead>
          <tbody class="row_position">
              <?php $no=0; foreach ($data['data'] as $key => $value) { $no++; ?>
                <?php if ($data['status'] == 200) { ?>
                  <tr>
                    <td><input type="checkbox" name="id[]" value="<?= $value['id'];?>"></td>
                    <td><?= $no;?>.</td>
                    <td><?= $value['nama_depan'].' '.$value['nama_belakang'];?></td>
                    <td><?= $value['certificate_number'] .' ('. (!empty($value['type_course']) ? $value['type_course'] : 'Webinar') . ')';?></td>
                    <td>
                      <button type="button" class="btn btn-primary btn-sm" onclick="onEdit('<?= $value['id'];?>','<?= $value['id_user'];?>')"> <i class="fa fa-edit"></i></button>
                      <button type="button" class="btn btn-primary btn-sm" onclick="onGenerate('<?= $value['id'];?>','<?= $value['id_user'];?>')"> <i class="fa fa-cog"></i></button>
                    </td>
                  </tr>
                <?php }else{ ?>
                  <td>Tidak Ada Data !</td>
                <?php } ?>
              <?php } ?>
          </tbody>
        </table>
      <button type="button" id="simpan" class="btn btn-dark">Kirim</button>
      </div>
    </div>
      <?php echo form_close() ?>
  </div>

</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
function onEdit(id,id_user){
  $.ajax({
      url: '<?= base_url('admin/certificate_monitoring/uploadcertificate'); ?>',
      method: "POST",
      data: {id:id,id_user:id_user},
      success: function(data){
          $('#dataModalLarge').html(data);
          $('#ModalLarge').modal('show');
      }
  });
}
function onDelete(id,id_user){

  $.ajax({
      url: '<?= base_url('admin/certificate_monitoring/delete'); ?>',
      method: "POST",
      data: {id:id,id_user:id_user},
      success: function(res){
        var hasil = $.parseJSON(res);
        if (hasil["status"] == 200) {
           toastr.success(hasil["pesan"]);
           setTimeout(function () {
             return hasil["status"];
           }, 1500);
           $('#ModalLarge').modal('hide');
           setTimeout(function () {
             location.reload(true);
           }, 1500);
         }else{
           toastr.error(hasil["pesan"]);
           result = false;
           return hasil["status"];
         }
      }
  });
}
function onGenerate(id,id_user){
    $.ajax({
        url: '<?= base_url('admin/certificate_monitoring/certificate'); ?>',
        method: "POST",
        data: {id:id,id_user:id_user},
        success: function (res) {
          let r = JSON.parse(res)
          window.open(r.url, '_blank');
          // location.reload();
        },
        error: function (res) {

        },
    });
}
$("#simpan").click(function(){
  var form = $("#" + $(this).closest('form').attr('name'));
  var formdata = false;
  if (window.FormData) {
    formdata = new FormData(form[0]);
  }
  bootbox.confirm("Apakah anda yakin akan mengirim email?", function(result){
    if(result == true) {
      $.ajax({
        type: form.attr("method"),
        url: form.attr("action"),
        data: formdata ? formdata : form.serialize(),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function (res) {
          var hasil = $.parseJSON(res);
          if (hasil["status"] == 200) {
             toastr.success(hasil["pesan"]);
             setTimeout(function () {
               location.reload(true);
             }, 1500);
           }else{
             toastr.error(hasil["pesan"]);
           }
         },
         error: function (res) {
           toastr.error("Data tidak dapat disimpan.");
         },
      });
    }
  });
});
</script>
