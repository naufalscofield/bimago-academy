<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary"><?=$title;?></h6>
		</div>
		<div class="card-body">
			<br>
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="dataTableQuestions" width="100%" cellspacing="0">
					<thead class="thead-dark">
						<tr>
							<th width="5%">No. </th>
							<th width="15%">Nama Pembeli</th>
							<th width="10%">No. Handphone</th>
							<th width="10%">No. Pesanan</th>
							<th width="10%">Pembelian</th>
							<th width="10%">Metode</th>
							<th width="15%">Total</th>
							<th width="15%">Status</th>
							<th width="10%">Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php $no=0; foreach ($data as $key => $value) { $no++; ?>
							<tr>
								<td style="<?= $value['status'] == 1 ? 'background-color:#cbf5cf;' : 'background-color:#f5a2a7;'?>"><?=$no;?>.</td>
								<td><?=$value['nama_depan'].' '.$value['nama_belakang'];?></td>
								<td><?=$value['no_hp'];?></td>
								<td><?=$value['no_pesanan'];?></td>
								<td><?=indo_date($value['tanggal_pembelian']);?></td>
								<td><?=$value['metode_pembayaran'];?></td>
								<td><?=rupiah($value['total']);?></td>
								<td style="<?= $value['status'] == 1 ? 'background-color:#cbf5cf;' : 'background-color:#f5a2a7;'?>">
									<?php if ($value['status'] == 1) { ?>
										Lunas
									<?php }else if ($value['status'] == 2) { ?>
										Expire
									<?php }else { ?>
										Belum Lunas
									<?php } ?>
								</td>
								<td>
									<a href="#" id="<?=$value['id'];?>" data-style="zoom-out" class="btn ladda-button btn-info btn-circle updateData ladda_<?=$value['id'];?>">
										<i class="fas fa-info-circle"></i>
									</a>
									 <?php if ($value['status'] != 1) { ?>
										<a href="#" data-id="<?=$value['id'];?>" id="<?=$value['no_pesanan'];?>" data-style="zoom-out" class="btn ladda-button btn-warning btn-circle getMidtrans laddaMid_<?=$value['no_pesanan'];?>">
											<i class="fas fa-info-circle"></i>
										</a>
									 <?php } ?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	$(".updateData").click(function(event){
		var id = $(this).attr('id');
		var l = Ladda.create( document.querySelector( '.ladda_' + id));
		l.start();
		bootbox.confirm("Ubah status pembayaran?", function(result){
			if(result == true) {
				$.ajax({
						url: "<?=$update;?>",
						method: "POST",
						data: {id:id},
						success: function(res){
							var hasil = $.parseJSON(res);
							if (hasil["status"] == 200) {
								l.stop();
								 toastr.success(hasil["pesan"]);
								 setTimeout(function () {
									 location.reload(true);
								 }, 1500);
							 }else{
								 l.stop();
								 toastr.error(hasil["pesan"]);
							 }
						},
						error: function (res) {
							l.stop();
							toastr.error("Data tidak dapat diubah.");
						},
				});
			}else{
				l.stop();
			}
		});
	});
	$(".getMidtrans").click(function(event){
			var id_order = $(this).attr('id');
			var id = $(this).attr('data-id');
			var l = Ladda.create( document.querySelector( '.laddaMid_' + id_order));
			l.start();
			$.ajax({
				url: "<?= base_url('midtrans/transaction/process'); ?>",
				method: "POST",
				data: {order_id:id_order , action:'status'},
				success: function(res){
					var hasil = $.parseJSON(res);
					toastr.success(hasil['pesan']);
					l.stop();
					setTimeout(function () {
						 location.reload(true);
					}, 1500);
				},
				error: function (res) {
					l.stop();
					toastr.error("Data tidak dapat cek.");
				},
			});
		});
	});

</script>
