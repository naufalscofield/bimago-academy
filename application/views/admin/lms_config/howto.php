<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= env('APP_NAME');?></title>
</head>
<body>
    <center>
        <div>
            <h1>1. <?= $this->lang->line('howto1');?></h1>
            <img src="<?= base_url();?>assets/default/howto2.png" style="width:35%;height:35%" alt="">
        </div>
        <div>
            <h1>2. <?= $this->lang->line('howto2');?></h1>
            <img src="<?= base_url();?>assets/default/howto1.png" style="width:35%;height:35%" alt="">
        </div>
        <div>
            <h1>3. <?= $this->lang->line('howto3');?></h1>
            <img src="<?= base_url();?>assets/default/howto3.png" style="width:35%;height:35%" alt="">
        </div>
    </center>
</body>
</html>