<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">
        <?= $this->lang->line('lms_config'); ?>
      </h6>
    </div>
    <div class="card-body">
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="thead-dark">
            <tr>
              <th width="60%"><?= $this->lang->line('description'); ?></th>
              <th width="35%"><?= $this->lang->line('main_logo'); ?></th>
              <th width="35%"><?= $this->lang->line('white_logo'); ?></th>
              <th width="35%">Icon</th>
              <th width="35%"><?= $this->lang->line('address'); ?></th>
              <th width="35%">Email</th>
              <th width="35%">Facebook</th>
              <th width="35%">Linkedin</th>
              <th width="35%"><?= $this->lang->line('phone'); ?></th>
              <th width="35%"><?= $this->lang->line('working_days'); ?></th>
              <th width="35%"><?= $this->lang->line('working_hours'); ?></th>
              <th width="5%"><i class="fa fa-cogs"></i></th>
            </tr>
          </thead>
          <tbody class="row_position">
              <tr>
                <td><?= substr($data->footer, 0, 100);?>..</td>
                <td><img src="<?= base_url();?>uploads/lms_config/<?= $data->logo_utama;?>" style="width:100%" alt=""></td>
                <td style="background-color:grey"><img src="<?= base_url();?>uploads/lms_config/<?= $data->logo_putih;?>" style="width:100%" alt=""></td>
                <td><img src="<?= base_url();?>uploads/lms_config/<?= $data->icon;?>" style="width:100%" alt=""></td>
                <td><?= substr($data->alamat, 0, 100);?>..</td>
                <td><?= $data->email;?></td>
                <td><?= $data->facebook;?></td>
                <td><?= $data->linkedin;?></td>
                <td><?= $data->telepon;?></td>
                <td><?= $data->hari_kerja;?></td>
                <td><?= $data->jam_kerja;?></td>
                <td>
                  <button data-toggle="modal" data-target="#modal_edit" title="Edit" class="btn btn-info btn-sm btn-edit"><i class="fas fa-pen"></i></button>
                </td>
              </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= $this->lang->line('edit_lms_config'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>admin/lms-config-update" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <label for=""><?= $this->lang->line('description'); ?></label>
              <textarea name="footer" class="form-control" id="ckeditor" required cols="30" rows="10"><?= $data->footer; ?></textarea>
            </div>
            <div class="form-group">
              <label for=""><?= $this->lang->line('main_logo'); ?></label>
              <input type="file" name="logo_utama" class="form-control">
              <a href="<?= base_url();?>uploads/lms_config/<?= $data->logo_utama;?>" target="_blank"><i class="fas fa-eye"><?= $this->lang->line('current_image'); ?></i></a>
            </div>
            <div class="form-group">
              <label for=""><?= $this->lang->line('white_logo'); ?></label>
              <input type="file" name="logo_putih" class="form-control">
              <a href="<?= base_url();?>uploads/lms_config/<?= $data->logo_putih;?>" target="_blank"><i class="fas fa-eye"><?= $this->lang->line('current_image'); ?></i></a>
            </div>
            <div class="form-group">
              <label for="">Icon</label>
              <input type="file" name="icon" class="form-control">
              <a href="<?= base_url();?>uploads/lms_config/<?= $data->icon;?>" target="_blank"><i class="fas fa-eye"><?= $this->lang->line('current_image'); ?></i></a>
            </div>
            <div class="form-group">
              <label for="">Link Youtube Video Home Page</label>
              <input type="text" name="link_youtube" value="<?= $data->link_youtube;?>" class="form-control">
              <a href="<?= $data->link_youtube;?>" target="_blank"><i class="fas fa-eye"><?= $this->lang->line('current_video'); ?></i></a>
            </div>
            <div class="form-group">
              <label for=""><?= $this->lang->line('address'); ?></label>
              <textarea name="alamat" class="form-control" id="ckeditore" required cols="30" rows="10"><?= $data->alamat; ?></textarea>
            </div>
            <div class="form-group">
              <label for="">Email</label>
              <input type="email" name="email" value="<?= $data->email;?>" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Facebook</label>
              <input type="text" name="facebook" value="<?= $data->facebook;?>" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Linkedin</label>
              <input type="text" name="linkedin" value="<?= $data->linkedin;?>" class="form-control">
            </div>
            <div class="form-group">
              <label for=""><?= $this->lang->line('phone'); ?></label>
              <input type="text" name="telepon" value="<?= $data->telepon;?>" class="form-control">
            </div>
            <div class="form-group">
              <label for=""><?= $this->lang->line('working_days'); ?></label>
              <input type="text" name="hari_kerja" value="<?= $data->hari_kerja;?>"  placeholder="Senin - Jumat" class="form-control">
            </div>
            <div class="form-group">
              <label for=""><?= $this->lang->line('working_hours'); ?></label>
              <input type="text" name="jam_kerja" value="<?= $data->jam_kerja;?>" placeholder="08:00-17:00" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Link Google Maps</label><br>
              <a href="<?= base_url();?>admin/howto-link-google-maps" target="_blank"><i class="fas fa-info"></i> <?= $this->lang->line('howto'); ?></a>
              <textarea name="link_google_map" class="form-control" id="" cols="30" rows="10"><?= (string)$data->link_google_maps; ?></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $this->lang->line('close'); ?></button>
            <button type="submit" class="btn btn-primary"><?= $this->lang->line('save'); ?></button>
          </div>
        </div>
      </form>
  </div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
  $(document).ready(function(){
    var flashdata_success	= '<?php echo $this->session->flashdata("success");?>'
		var flashdata_error	  = '<?php echo $this->session->flashdata("error");?>'
    
		if (flashdata_success)
		{
			toastr.success(flashdata_success);
		} else if (flashdata_error)
		{
			toastr.error(flashdata_error);
		}

    CKEDITOR.replace('ckeditor');
  })
</script>
