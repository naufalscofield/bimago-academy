<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('admin/course'); ?>">Course <i class="fas fa-angle-double-right"></i></a> Exam <i class="fas fa-angle-double-right"></i> <?=$judul;?> </h6>
		</div>
		<div class="card-body">
			<div align="right">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalAdd">
				<span class="icon text-white-50">
				<i class="fas fa-plus"></i>
				</span>
				<span class="text">Tambah</span>
				</button>
			</div>
			<br>
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead class="thead-dark">
						<tr>
							<th width="5%">No. </th>
							<th width="25%">Course</th>
							<th width="15%">Type</th>
							<th width="8%">Waktu</th>
							<th width="5%">Total Soal</th>
							<th width="5%">Passing Grade</th>
							<th width="13%">Timeout</th>
							<th width="12%">Kesempatan</th>
							<th width="15%">Aksi</th>
						</tr>
					</thead>
					<tbody class="row_position">
						<?php $no=0; foreach ($data as $key => $value) { $no++; ?>
						<tr id="<?php echo $value['id'] ?>">
							<td><?= $no;?>.</td>
							<td><?= $value['judul'];?></td>
							<td><?= $value['type_course'];?></td>
							<td><?= $value['waktu'].' Menit'; ?></td>
							<td><?= $value['total_soal'];?> Soal</td>
							<td><?= $value['passing_grade'];?> Score</td>
							<td><?= strtoupper(str_replace("_"," ",$value['aksi_waktu_habis']));?></td>
							<td><?= ($value['jumlah_kesempatan'] == 0) ? 'Tidak terbatas' : $value['jumlah_kesempatan'].' Kali';?></td>
							<td>
								<a href="<?= base_url();?>admin/exam-questions/<?= $value['id_exam'];?>" id="<?=$value['id_exam'];?>" data-toggle="tooltip" data-placement="top" title="Questions" class="btn btn-success btn-circle btn-sm">
								<i class="fas fa-list"></i>
								</a>
								<a href="#modalEdit" data-toggle="modal" id="<?= $value['id_exam'];?>" class="btn btn-info btn-circle btn-sm editData">
								<i class="fas fa-pencil-alt"></i>
								</a>
								<a href="#" id="<?=$value['id_exam'];?>" data-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-danger btn-circle btn-sm deleteData">
								<i class="fas fa-trash"></i>
								</a>
							</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="modalAdd" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Tambah Exam</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?= base_url();?>admin/store-exam" method="POST">
					<div class="form-group">
						<label for="">Course</label>
						<?php echo form_dropdown('id_course', $opt_course, '', ' id="id_course" required class="form-control"');?>
					</div>
					<div class="form-group">
						<label for="">Tipe Course</label>
						<select class="form-control" name="id_type_course" id="id_type_course" required>
							<option value="">-</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Waktu (Menit)</label>
						<input name="waktu" required type="number" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Passing Grade</label>
						<input name="passing_grade" required type="number" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Jumlah Kesempatan</label>
						<input name="jumlah_kesempatan" required type="number" class="form-control">
						<p style="color:red">*Ketik 0 untuk kesempatan tidak terbatas.</p>
					</div>
					<div class="form-group">
						<label for="">Aksi Saat Waktu Habis</label>
						<select class="form-control" name="aksi_waktu_habis" id="" required>
							<option value="">--Pilih--</option>
							<option value="auto_submit">Auto Submit</option>
							<option value="auto_failed">Auto Failed</option>
						</select>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
    </form>
		</div>
	</div>
</div>
<!-- Modal Edit-->
<div class="modal fade bd-example-modal-lg" id="modalEdit" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Edit Exam</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-primary text-center" id="spinnerEdit" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
		<form action="<?= base_url();?>admin/update-exam" method="POST" id="formEdit" style="display:none">
			<div class="form-group">
				<label for="">Course</label>
				<input class="form-control" disabled id="edit_judul_course">
				<input type="hidden" name="edit_id_course" id="edit_id_course">
				<input type="hidden" name="edit_id" id="edit_id">
			</div>
			<div class="form-group">
				<label for="">Tipe Course</label>
				<input class="form-control" disabled id="edit_type_course">
				<input type="hidden" name="edit_id_type_course" id="edit_id_type_course">
			</div>
          <div class="form-group">
            <label for="">Waktu (Menit)</label>
            <input id="edit_waktu" name="edit_waktu" required type="number" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Passing Grade</label>
            <input id="edit_passing_grade" name="edit_passing_grade" required type="number" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Jumlah Kesempatan</label>
            <input id="edit_jumlah_kesempatan" name="edit_jumlah_kesempatan" required type="number" class="form-control">
          </div>
          <div class="form-group">
            <label for="">Aksi Saat Waktu Habis</label>
            <select class="form-control" name="edit_aksi_waktu_habis" id="edit_aksi_waktu_habis" required>
                <option value="">--Pilih--</option>
                <option value="auto_submit">Auto Submit</option>
                <option value="auto_failed">Auto Failed</option>
            </select>
          </div>
			</div>
			<div class="modal-footer" id="footerEdit" style="display:none">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Update</button>
			</div>
      </form>
		</div>
	</div>
</div>
<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
$(document).ready(function(){

    $('.select2').select2();

    var status_store_exam   = '<?php echo $this->session->flashdata('status_store_exam');?>'
    var msg_store_exam      = '<?php echo $this->session->flashdata('msg_store_exam');?>'
    var status_update_exam  = '<?php echo $this->session->flashdata('status_update_exam');?>'
    var msg_update_exam     = '<?php echo $this->session->flashdata('msg_update_exam');?>'

    if (status_store_exam)
    {
        show_toast(status_store_exam, msg_store_exam)
    }
    if (status_update_exam)
    {
        show_toast(status_update_exam, msg_update_exam)
    }

	  $(".deleteData").click(function(event){
	    var id = $(this).attr('id');
	    bootbox.confirm("Apakah anda yakin akan menghapus data ini?", function(result){
	      if(result == true) {
	        $.ajax({
              url: "<?=$url_delete;?>",
	            method: "POST",
	            data: {id:id},
	            success: function(res){
	              var hasil = $.parseJSON(res);
	              if (hasil["status"] == 200) {
	                 toastr.success(hasil["pesan"]);
	                 setTimeout(function () {
	                   return hasil["status"];
	                 }, 1500);
	                 setTimeout(function () {
	                   location.reload(true);
	                 }, 1500);
	               }else{
	                 toastr.error(hasil["pesan"]);
	                 result = false;
	                 return hasil["status"];
	               }
	            },
	            error: function (res) {
	              toastr.error("Data tidak dapat dihapus.");
	              result = false;
	              return false;
	            },
	        });
	      }
	    });
	  });

	  	$(".editData").click(function(event){
			$('#spinnerEdit').show()
			$('#formEdit').hide()
			$('#footerEdit').hide()
			var id = $(this).attr('id');
			$.ajax({
				url: "<?=$url_edit;?>",
				method: "POST",
				data: {id:id},
				success: function(res){
					var hasil = $.parseJSON(res);
					if (hasil["status"] == 200) {
						$('#spinnerEdit').hide()
						$('#formEdit').show()
						$('#footerEdit').show()
						$('#edit_id').val(hasil["data"].id_exam)
						$('#edit_judul_course').val(hasil["data"].judul)
						$('#edit_type_course').val(hasil["data"].type_course)
						$('#edit_id_course').val(hasil["data"].id)
						$('#edit_id_type_course').val(hasil["data"].id_type_course)
						$('#edit_waktu').val(hasil["data"].waktu)
						$('#edit_passing_grade').val(hasil["data"].passing_grade)
						$('#edit_jumlah_kesempatan').val(hasil["data"].jumlah_kesempatan)
						$('#edit_aksi_waktu_habis').val(hasil["data"].aksi_waktu_habis)
					}else{
						toastr.error(hasil["pesan"]);
						result = false;
						return hasil["status"];
					}
				},
				error: function (res) {
					toastr.error("Cannot get data");
					result = false;
					return false;
				},
			});
	  	});

	$("#id_course").change(function(event){
		$('#id_type_course')
		.find('option')
		.remove()
		.end()

		var idCourse	= $(this).val();
		$.ajax({
			url: "<?=$url_get_tipe_course;?>",
			method: "POST",
			data: {id:idCourse},
			success: function(res){
				var hasil = $.parseJSON(res);
				if (hasil["status"] == 200) {

					$('#id_type_course')
					.append($("<option></option>")
					.text("--Pilih Tipe--"));

					$.each(hasil["data"], function(key, value) {
						$('#id_type_course')
						.append($("<option></option>")
						.attr("value", value['id'])
						.text(value['type_course']));
					});
				}else{
					toastr.error(hasil["pesan"]);
				}
			},
			error: function (res) {
				toastr.error("Cannot get data");
			},
		});
	})

});
</script>
