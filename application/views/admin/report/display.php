<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?=$title;?> </h6>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead class="thead-dark">
						<tr>
							<th width="5%">No. </th>
							<th width="20%">Course</th>
							<th width="20%">Module</th>
							<th width="20%">Report</th>
							<th width="20%">Report Desc</th>
							<th width="20%">From</th>
							<th width="20%">Created At</th>
							<th width="80%">#</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- /.container-fluid -->
<script type="text/javascript" src="<?= base_url();?>assets/adminnew/vendor/jquery/bootbox.min.js"></script>
<link href="<?= base_url();?>assets/toastr-js/toastr.scss" rel="stylesheet"/>
<script src="<?= base_url();?>assets/toastr-js/toastr.js"></script>
<script>
	function onDelete(data, row, tr) {
      bootbox.confirm("Apakah anda yakin akan menonaktifkan course ini?", function(result){
        if(result == true) {
          $.ajax({
              url: "<?=$get_data_delete;?>",
              method: "POST",
              data: {id: data.id_course},
              success: function(res){
                var hasil = $.parseJSON(res);
                if (hasil["status"] == 200) {
                   toastr.success(hasil["pesan"]);
                   setTimeout(function () {
                     return hasil["status"];
                   }, 1500);
                   setTimeout(function () {
                     location.reload(true);
                   }, 1500);
                 }else{
                   toastr.error(hasil["pesan"]);
                   result = false;
                   return hasil["status"];
                 }
              },
              error: function (res) {
                toastr.error("Data tidak dapat dihapus.");
                result = false;
                return false;
              },
          });
        }
      });
	}
	$(document).ready(function(){
        var table = $('#dataTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "order": [], 
				rowId: 'id',
            "ajax": {
                "url" : '<?php echo $url_data;?>',
                "type": "POST"
            },
            columns: [
				{
					label: 'No',
					width: 2,
					data: "no",
					searchable: false,
		            orderable: false,
				},
				{
					label: 'Course',
					data: "judul_course",
					width: 80,
					sortable: true,
					searchable: true,
					defaultOrder: 'judul_course'
				},
				{
					label: 'Module',
					data: "judul_episode",
					width: 80,
					sortable: true,
					searchable: true,
					defaultOrder: 'judul_course'
				},
				{
					label: 'Report',
					data: "report",
					render: function (data, type, row ) {
						var ret = '';
						var report = JSON.parse(data)
					 	report.forEach((entry) => {
						    ret += '<li>'+entry+'</li>';
						});
					return ret;
					},
					width: 80,
					sortable: true,
					searchable: true,
					defaultOrder: 'judul_course'
				},
				{
					label: 'Report Desc',
					data: "report_desc",
					width: 80,
					sortable: true,
					searchable: true,
					defaultOrder: 'judul_course'
				},
				{
					label: 'From',
					data: "nama",
					width: 80,
					sortable: true,
					searchable: true,
					defaultOrder: 'judul_course'
				},
				{
					label: 'Created At',
					data: "created_at",
					width: 80,
					sortable: true,
					searchable: true,
					defaultOrder: 'judul_course'
				},
				{
					isLocal: true,
					label: '#',
					searchable: false,
					sortable: false,
					width: 10,
					defaultContent: '<button class="btn btn-sm btn-danger text-light" type="button" value="Edit" data-action="onDelete"><i class="fas fa-power-off"></i></button> '
				},
			],
        });

	    $('#dataTable').on('click', '[data-action]', (e) => {
	        e.preventDefault()

	        var target = $(e.target);
	        var action = target.attr('data-action');

	        var tr = target.closest('tr');
	        var row = table.row(tr);
	        var data = row.data();

	        if (typeof window[action] === 'function') {
	            window[action](data, row, tr);
	        } else {
	            console.log('function ' + action + ' not found ');
	        }
	    });

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = i+1;
	        } );
	    } ).draw();

	});
</script>